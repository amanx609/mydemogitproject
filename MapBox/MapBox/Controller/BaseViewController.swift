//
//  BaseViewController.swift
//  MapBox
//
//  Created by APPLE on 24/02/22.
//

import UIKit

enum UINavigationBarButtonType: Int {
    case back
    case save
    case cross
    case hamburgerWhite
    case hamburgerBlack
    case filter
    case backWhite
    case filterWhite
    case notification
    case reset
    case apply
    
    var iconImage: UIImage? {
        switch self {
        case .back: return #imageLiteral(resourceName: "incorrect_visual")
        case .cross: return #imageLiteral(resourceName: "close")
        default: return nil
        }
    }
    
    var title: String? {
        switch self {
        case .filter, .filterWhite: return "Filter"
        case .reset: return "Reset"
        case .apply: return "Apply"
        default: return nil
        }
    }
}


protocol BaseViewCDelegate {
    func navigationBarButtonDidTap(_ buttonType: UINavigationBarButtonType)
}

class BaseViewController: UIViewController {

    @IBOutlet weak var navigationTitleLabel: UILabel!
    var baseDelegate: BaseViewCDelegate?

    
    
    private enum NavBarConstants {
        static let navButtonWidth: CGFloat = 35.0
        static let edgeInset: CGFloat = 10.0
        static let titleButtonWidth: CGFloat = 55.0
        static let titleButtonHeight: CGFloat = 25.0
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    deinit {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    private func setup() {
        if navigationTitleLabel.text != nil {
            navigationTitleLabel.font = UIFont.systemFont(ofSize: 15)
        }
    }
    
    @objc func navigationButtonTapped(_ sender: AnyObject) {
        guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
        switch buttonType {
        case .back, .backWhite: backButtonTapped()
        case .hamburgerWhite: hamburgerButtonTapped(sender)
        case .hamburgerBlack: hamburgerButtonTapped(sender)
        case .cross: crossButtonTapped()
        case .filterWhite: filterButtonTapped()
           
        default: break
        }
        self.baseDelegate?.navigationBarButtonDidTap(buttonType)
    }
    
    func filterButtonTapped() {
        
    }
    
    func crossButtonTapped() {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func hamburgerButtonTapped(_ sender: AnyObject) {
        if self.navigationController!.viewControllers.count > 1 {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func backButtonTapped() {
        if self.navigationController!.viewControllers.count > 1 {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    func setupNavigationBarTitle(title : String, barColor : UIColor = .white, titleColor : UIColor = .black, leftBarButtontype : [UINavigationBarButtonType], rightBarButtontype : [UINavigationBarButtonType]) {
        
        self.navigationController?.navigationBar.barTintColor = barColor
        self.navigationController?.navigationBar.backgroundColor = barColor
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: titleColor]
        
        if !title.isEmpty {
            self.navigationItem.title = title
        }

        var rightBarButtonItems = [UIBarButtonItem]()
        for rightButtonType in rightBarButtontype {
            let rightButtonItem = getBarButtonItem(for: rightButtonType, isLeftBarButtonItem: false)
            rightBarButtonItems.append(rightButtonItem)
        }
        if rightBarButtonItems.count > 0 {
            self.navigationItem.rightBarButtonItems = rightBarButtonItems
        }
        
        var leftBarButtonItems = [UIBarButtonItem]()
        for leftBarButtonItem in leftBarButtontype {
            let leftButtonItem = getBarButtonItem(for: leftBarButtonItem, isLeftBarButtonItem: true)
            leftBarButtonItems.append(leftButtonItem)
        }
        if leftBarButtonItems.count > 0 {
            self.navigationItem.leftBarButtonItems = leftBarButtonItems
        }
        
        
    }
        
        func getBarButtonItem(for type: UINavigationBarButtonType, isLeftBarButtonItem: Bool) -> UIBarButtonItem {
            var barButtonHeight = 40
            if let height = self.navigationController?.navigationBar.frame.size.height {
                barButtonHeight = Int(height)
            }
            let button = UIButton(frame: CGRect(x: 30, y: 0, width: Int(NavBarConstants.navButtonWidth), height: barButtonHeight))
            //SetAttributes
            button.titleLabel?.font = UIFont.font(name: .AirbnbCerealApp, weight: .Bold, size: .size_10)
            button.titleLabel?.textAlignment = .left
            button.tag = type.rawValue
            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: isLeftBarButtonItem ? -NavBarConstants.edgeInset : NavBarConstants.edgeInset, bottom: 0, right: isLeftBarButtonItem ? NavBarConstants.edgeInset : -NavBarConstants.edgeInset)
            button.frame.size.width = Constants.Devices.ScreenHeight == 568.0 ? 40.0 : 60.0
            button.addTarget(self, action: #selector(BaseViewController.navigationButtonTapped(_:)), for: .touchUpInside)
            //SetImage
            if let iconImage = type.iconImage {
                button.imageView?.contentMode = .scaleAspectFit
                button.setImage(iconImage, for: UIControl.State())
                return UIBarButtonItem(customView: button)
            }
            //SetTitle
            if let title = type.title {
                button.setTitle(title, for: UIControl.State())
                if type == .filter {
                    button.setTitleColor(UIColor.white, for: .normal)
                    button.backgroundColor = UIColor.red
                    button.roundCorners(Constants.UIConstants.sizeRadius_3half)
                    let barButton = UIBarButtonItem(customView: button)
                    let currHeight = barButton.customView?.heightAnchor.constraint(equalToConstant: NavBarConstants.titleButtonHeight)
                    currHeight?.isActive = true
                    let currWidth = barButton.customView?.widthAnchor.constraint(equalToConstant: NavBarConstants.titleButtonWidth)
                    currWidth?.isActive = true
                    return barButton
                } else if type == .filterWhite {
                    button.setTitleColor(UIColor.red, for: .normal)
                    button.backgroundColor = UIColor.white
                    button.roundCorners(Constants.UIConstants.sizeRadius_3half)
                    button.titleLabel?.font = UIFont.font(name: .AirbnbCerealApp, weight: .Medium, size: .size_10)
                    let barButton = UIBarButtonItem(customView: button)
                    let currHeight = barButton.customView?.heightAnchor.constraint(equalToConstant: NavBarConstants.titleButtonHeight)
                    currHeight?.isActive = true
                    let currWidth = barButton.customView?.widthAnchor.constraint(equalToConstant: NavBarConstants.titleButtonWidth)
                    currWidth?.isActive = true
                    return barButton
                } else if type == .reset {
                    button.setTitleColor(UIColor.white, for: .normal)
                    button.backgroundColor = UIColor.red
                    button.roundCorners(Constants.UIConstants.sizeRadius_3half)
                    let barButton = UIBarButtonItem(customView: button)
                    let currHeight = barButton.customView?.heightAnchor.constraint(equalToConstant: NavBarConstants.titleButtonHeight)
                    currHeight?.isActive = true
                    let currWidth = barButton.customView?.widthAnchor.constraint(equalToConstant: NavBarConstants.titleButtonWidth)
                    currWidth?.isActive = true
                    return barButton
                } else if type == .apply {
                    button.setTitleColor(UIColor.white, for: .normal)
                    button.backgroundColor = UIColor.red
                    button.roundCorners(Constants.UIConstants.sizeRadius_3half)
                    let barButton = UIBarButtonItem(customView: button)
                    let currHeight = barButton.customView?.heightAnchor.constraint(equalToConstant: NavBarConstants.titleButtonHeight)
                    currHeight?.isActive = true
                    let currWidth = barButton.customView?.widthAnchor.constraint(equalToConstant: NavBarConstants.titleButtonWidth)
                    currWidth?.isActive = true
                    return barButton
                }
            }
            return UIBarButtonItem()
        }
        
    }
