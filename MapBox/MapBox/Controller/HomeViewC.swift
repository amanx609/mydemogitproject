//
//  ViewController.swift
//  MapBox
//
//  Created by APPLE on 22/02/22.
//

import UIKit
import MapboxMaps
import CoreLocation
import MapboxDirections
import MapboxCoreNavigation
import MapboxNavigation



class HomeViewC: BaseViewController, NavigationServiceDelegate, NavigationMapViewDelegate {
 
    var mapView : MapView!
    var navView : NavigationMapView!
   
    private var routeResponse : RouteResponse!
    

    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName : UITextField!
    @IBOutlet weak var email : UITextField!
    @IBOutlet weak var password : UITextField!
    @IBOutlet weak var confirmPassword : UITextField!
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var btn: UIButton!
    
    
    var buttonText = 0 {
        
        didSet {
            btn.setTitle("\(buttonText)", for: .normal)
            btn.isHidden = buttonHide
        }
        willSet{
            btn.setTitle("value", for: .normal)
        }
    }
    
    
    var buttonHide : Bool {
        get {
            if buttonText == 10 {
                return true
            }
            return false
        }
        set {
            self.buttonHide = newValue
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let myResourceOptions = ResourceOptions(accessToken: Constants.mapViewPublicKey)
        let myMapInitOptions = MapInitOptions(resourceOptions: myResourceOptions)
        mapView = MapView(frame: view.bounds, mapInitOptions: myMapInitOptions)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.navView = NavigationMapView(frame: view.bounds)
        self.navView.moveUserLocation(to: .init(latitude: Constants.s.latitude, longitude: Constants.s.longitude))
        self.navView.delegate = self
//        self.moveToCurrentLocation() // set camera to fit source and destination in map
        self.navView.routeCasingColor = .clear
        self.navView.trafficHeavyColor =  Constants.routeColor
        self.navView.trafficLowColor = Constants.routeColor
        self.navView.trafficModerateColor = Constants.routeColor
        self.navView.trafficSevereColor = Constants.routeColor
        self.navView.trafficUnknownColor = Constants.routeColor
        self.navView.traversedRouteColor = Constants.coveredRouteColor
        self.navView.mapView.ornaments.options.scaleBar.visibility = .hidden // hide scaleBar
        self.navView.mapView.mapboxMap.onNext(.mapLoaded) { [weak self] _ in
            // Add source and destination annotation
        }
//        view.addSubview(navView)
//        main()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Home Screen", leftBarButtontype: [.filter], rightBarButtontype: [.apply])
    }
    
    
    @IBAction func submitButton(_ sender: UIButton) {
        makeRequest()
    }
    
    func makeRequest() {
        do {
            let name = try ValidationServices.validate(name: self.firstName.text)
            let lastName = try ValidationServices.validate(lastName: self.lastName.text)
            let email = try ValidationServices.validate(email: self.email.text)
            let password = try ValidationServices.validate(password: self.password.text)
            let matchPassword = try ValidationServices.validate(newPassword: password, confirmPassword: self.confirmPassword.text)
            print(name , lastName , email , password , matchPassword)
            self.showError("")
        } catch {
            self.showError(error.localizedDescription)
        }
        buttonText += 1
    }
    
    
     func shareAll() {
      let text = "This is the text...."
      let image = UIImage(named: "tickGreen")
      let myWebsite = NSURL(string:"https://stackoverflow.com/users/4600136/mr-javed-multani?tab=profile")
         let shareAll = [text , image! , myWebsite as Any] as [Any]
      let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
      activityViewController.popoverPresentationController?.sourceView = self.view
      self.present(activityViewController, animated: true, completion: nil)
     }
    
    
    func showError(_ message: String){
        DispatchQueue.main.async {
            self.responseLabel.text = message
        }
    }
}

















class Rectrangle {
    var height : Int
    var width : Int
    
    init(width : Int , height : Int){
        self.width = width
        self.height = height
    }
    
    func area() -> Int {
        return height * width
    }
}

class Square : Rectrangle {
    
    override var height: Int {
        didSet {
            super.width = height
        }
    }
    
    override var width: Int {
        didSet {
            super.height = width
        }
    }
}

func main(){
    let square = Square(width: 10, height: 10)
    let rectrangle : Rectrangle = square
    
    rectrangle.height = 7
    rectrangle.width = 5
    
    print(rectrangle.area())
}

class Shape {
    func doSomething() {
        // do something relate to shape that is irrelevant to this example, actually
    }
}

class SquareNew: Shape {
    func drawSquare() {
        // draw the square
    }
}

class Circle: Shape {
    func drawCircle() {
        // draw the circle
    }
}


func draw(shape: Shape) {
    if let square = shape as? SquareNew {
        square.drawSquare()
    } else if let circle = shape as? Circle {
        circle.drawCircle()
    }
}


func Newdraw() {
    
}
