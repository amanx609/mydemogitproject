//
//  DIConfigurator.swift
//  MapBox
//
//  Created by APPLE on 24/02/22.
//

import Foundation
import UIKit

enum StoryboardType: String {
    
    case LaunchScreen
    case Login
    case Store
    case Settings
    case Home
    case Main
    case SupplierMain
    case Account
    var storyboardName: String {
        return rawValue
    }
}


class DIConfigurator: NSObject {
    
    // MARK: - Singleton Instantiation
    private static let _sharedInstance: DIConfigurator = DIConfigurator()
    static var sharedInstance: DIConfigurator {
        return _sharedInstance
    }
    
    //MARK: - Initialization Method
    private override init() {
        
    }
    
    func getViewControler(storyBoard: StoryboardType, indentifier: String) -> UIViewController {
        let storyB = UIStoryboard(name: storyBoard.rawValue, bundle: nil)
        return storyB.instantiateViewController(withIdentifier: indentifier)
    }
    
    func getHomeScreen() -> HomeViewC {
        if let homeScreen = self.getViewControler(storyBoard: .Home, indentifier: HomeViewC.className) as? HomeViewC {
            return homeScreen
        } else {
            fatalError("Not able to initialize LoginTypeViewC")
        }
    }
    
    func setRootViewController(window : UIWindow? = AppDelegate.getAppDelegate()?.window) {
       let home = self.getHomeScreen()
        let nav = UINavigationController(rootViewController: home)
        nav.setNavigationBarHidden(true, animated: false)
        window?.rootViewController = nav
        window?.overrideUserInterfaceStyle = .light
        window?.makeKeyAndVisible()
    }
    
    
    func setRootViewControllerWithTab(window : UIWindow? = AppDelegate.getAppDelegate()?.window) {
        let storyBaord = UIStoryboard(name: "Main", bundle: nil)
//        let navController = storyBaord.instantiateViewController(withIdentifier: "NavigationController") as? UINavigationController
        let tabbarController = storyBaord.instantiateViewController(withIdentifier: TabBarController.className) as? TabBarController
//        navController.
//        guard let nav = navController else {return}
//        nav.setViewControllers([tabbarController], animated: true)
        window?.rootViewController = tabbarController
        window?.overrideUserInterfaceStyle = .light
        window?.makeKeyAndVisible()
    }
    
    
    func showHome() {
        let homeVC = DIConfigurator.sharedInstance.getHomeScreen()
        let nav = UINavigationController()
        nav.setViewControllers([homeVC], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.overrideUserInterfaceStyle = .light
        window.rootViewController = nav
    }
    
    
    }



extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
    
}
