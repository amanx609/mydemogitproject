//
//  AppDelegates+Addition.swift
//  MapBox
//
//  Created by APPLE on 24/02/22.
//


import Foundation
import UIKit

extension AppDelegate {
    
    static func getAppDelegate() -> AppDelegate? {
        if Thread.isMainThread {
            return UIApplication.shared.delegate as? AppDelegate
        }
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        DispatchQueue.main.async {
            realDelegate = UIApplication.shared.delegate as? AppDelegate
            dispatchGroup.leave()
        }
        dispatchGroup.wait()
        return realDelegate
    }
    
    
    func showHome() {
        let homeVC = DIConfigurator.sharedInstance.getHomeScreen()
        let nav = UINavigationController(rootViewController: homeVC)
        self.window?.rootViewController = nav
    }
}
