//
//  ValidationSeervices.swift
//  MapBox
//
//  Created by APPLE on 23/02/22.
//

import Foundation

enum ValidationError : Error {
    case FirstName, invalidName, LastName, emptyEmail, invalidEmail, ConfirmPassword, emptyConfrimPassword, validConfirmPassword, noMatchPassword, CreatePassword, PhoneNumber
}

extension ValidationError : LocalizedError {
    var errorDescription: String? {
        switch self {
        case .FirstName:
            return Constants.FirstName
        case .invalidName:
            return Constants.invalidName
        case .LastName:
            return Constants.LastName
        case .emptyEmail:
            return Constants.emptyEmail
        case .invalidEmail:
            return Constants.invalidEmail
        case .ConfirmPassword:
            return Constants.ConfirmPassword
        case .emptyConfrimPassword:
            return Constants.emptyConfrimPassword
        case .validConfirmPassword:
            return Constants.validConfirmPassword
        case .noMatchPassword:
            return Constants.noMatchPassword
        case .CreatePassword:
            return Constants.CreatePassword
        case .PhoneNumber:
            return Constants.PhoneNumber
        }
    }
}

struct ValidationServices {
    
    static func validate(name : String?) throws -> String {
        guard let firstName = name, !firstName.isEmpty else {
            throw ValidationError.FirstName
        }
        return firstName
    }
    
    static func validate(lastName : String?) throws -> String {
        guard let lastName = lastName, !lastName.isEmpty else {
            throw ValidationError.LastName
        }
      return lastName
    }
    
    static func validate(email : String?) throws -> String {
        guard let email = email, !email.isEmpty else {
            throw ValidationError.emptyEmail
        }
        guard email.isValidEmail() else {
            throw ValidationError.invalidEmail
        }
      return email
    }
    
    static func validate(password : String?) throws -> String {
        guard let password = password, !password.isEmpty else {
            throw ValidationError.ConfirmPassword
        }
        guard password.count > 7 && password.count < 21 else {
            throw ValidationError.validConfirmPassword
        }
      return password
    }
    
    static func validate(newPassword: String, confirmPassword: String?) throws -> String {
          guard let confirmPassword = confirmPassword, !confirmPassword.isEmpty else {
              throw ValidationError.emptyConfrimPassword
          }
          guard newPassword == confirmPassword else {
              throw ValidationError.noMatchPassword
          }
          return newPassword
      }
}

extension String {

    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
}
