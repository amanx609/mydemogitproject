//
//  Constant.swift
//  MapBox
//
//  Created by APPLE on 22/02/22.
//

import Foundation
import UIKit
import CoreLocation

enum CoordinateType {
    case source
    case destination
}


struct Constants {
    static var mapViewPublicKey = "pk.eyJ1IjoiaWFtanVuaW9uIiwiYSI6ImNrenhyMTBmYTA0NXcydXFmcHN3aGV2dncifQ._DcQDxnfdzk8rgFJXVw0pg"
    static var s = CLLocationCoordinate2DMake(30.83682725082924, 78.45031499221531)
    static var d = CLLocationCoordinate2DMake(30.89808600498695, 78.52521156667208)
    static var routeColor : UIColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
    static var coveredRouteColor : UIColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
   
    static var FirstName = "Enter your first name"
    static var invalidName = "Enter valid name"
    static var LastName = "Enter your last name"
    static var emptyEmail = "Enter your mail id"
    static var invalidEmail = "Enter your valid mail id"
    static var ConfirmPassword = "Enter your password"
    static var emptyConfrimPassword = "Confirm password field is empty"
    static var validConfirmPassword = "Password characters between 8 to 20"
    static var noMatchPassword = "Password not match"
    static var CreatePassword = "Create password"
    static var PhoneNumber = "Enter your phone number"
    
    struct Devices {
      static let ScreenWidth = UIScreen.main.bounds.width
      static let ScreenHeight = UIScreen.main.bounds.height
      static let ScreenMaxLength = (max(ScreenWidth, ScreenHeight))
      static let ScreenMinLength = (min(ScreenWidth, ScreenHeight))
      // static let StatusBarHeight = UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
      static let NavigationBarHeight = CGFloat(44.0)
      //   static let NavigationBarHeightWithStatusBar = NavigationBarHeight+StatusBarHeight
      
      static let IsiPhone = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone
      static let IsiPhone4OrLess = (IsiPhone && ScreenMaxLength < 568.0)
      static let IsiPhone5 = (IsiPhone && ScreenMaxLength == 568.0)
      static let IsiPhone6 = (IsiPhone && ScreenMaxLength == 667.0)
      static let IsiPhone6P = (IsiPhone && ScreenMaxLength == 736.0)
      static let IsiPhoneXOr11Pro = (IsiPhone && ScreenMaxLength == 812.0)
      static let IsiPhone11 = (IsiPhone && ScreenMaxLength == 896.0)
      
      static let deviceType = 2 //ForiOS
      static var statusBarOrientation: UIInterfaceOrientation? {
        get {
          if #available(iOS 13.0, *) {
            guard let orientation = UIApplication.shared.windows.first?.windowScene?.interfaceOrientation else {
              #if DEBUG
              fatalError("Could not obtain UIInterfaceOrientation from a valid windowScene")
              #else
              return nil
              #endif
            }
             return orientation
          } else {
            // Fallback on earlier versions
            let orientation = UIApplication.shared.statusBarOrientation
            return orientation
          }
        }
      }
      
      @available(iOS 13.0, *)
      static let keyWindow = UIApplication.shared.connectedScenes
        .filter({$0.activationState == .foregroundActive})
        .map({$0 as? UIWindowScene})
        .compactMap({$0})
        .first?.windows
        .filter({$0.isKeyWindow}).first
    }
    
    struct UIConstants {
      static let sizeRadius_3half: CGFloat = 3.5
      static let sizeRadius_2: CGFloat = 2.0
      static let sizeRadius_4: CGFloat = 4.0
      static let sizeRadius_5: CGFloat = 5.0
      static let sizeRadius_7: CGFloat = 7.0
      static let sizeRadius_10: CGFloat = 10.0
      static let sizeRadius_12: CGFloat = 12.0
      static let sizeRadius_13: CGFloat = 13.0
      static let sizeRadius_14: CGFloat = 14.0
      static let sizeRadius_16: CGFloat = 16.0
      static let sizeRadius_18: CGFloat = 18.0
      static let sizeRadius_20: CGFloat = 20.0
      static let sizeRadius_40: CGFloat = 40.0
      static let sizeRadius_48: CGFloat = 48.0
      static let shadowOpacity: Float = 0.08
      static let shadowOpacity_15: Float = 0.15
      static let borderWidth_1: CGFloat = 1.0
      static let borderWidth_07: CGFloat = 0.7
      static let borderWidth_17: CGFloat = 1.7
      static let sizeRadius_6: CGFloat = 6.0
      static let sizeHeight_14: CGFloat = 14.0
      static let sizeHeight_23: CGFloat = 24.0
      static let borderWidth_2half: CGFloat = 2.5
      static let sizeRadius_30: CGFloat = 30.0
    }
    
    struct CellHeightConstants {
      static let height_0: CGFloat = 0.0
      static let height_80: CGFloat = 80.0
      static let height_100: CGFloat = 100.0
      static let height_50: CGFloat = 50.0
      static let height_156: CGFloat = 156.0
      static let height_150: CGFloat = 150.0
      static let height_40: CGFloat = 40.0
      static let height_30: CGFloat = 30.0
      static let height_120: CGFloat = 120.0
      static let height_110: CGFloat = 110.0
      static let height_130: CGFloat = 130.0
      static let height_60: CGFloat = 60.0
      static let height_170: CGFloat = 170.0
      static let height_116: CGFloat = 116.0
      static let height_25: CGFloat = 25.0
      static let height_35: CGFloat = 35.0
      static let height_45: CGFloat = 45.0
      static let height_135: CGFloat = 135.0
      static let height_84: CGFloat = 84.0
      static let height_56: CGFloat = 56.0
      static let height_168: CGFloat = 168.0
      static let height_97: CGFloat = 97.0
      static let height_65: CGFloat = 65.0
      static let height_118: CGFloat = 118.0
      static let height_53: CGFloat = 53.0
      static let height_220: CGFloat = 220.0
      static let height_106: CGFloat = 106.0
      static let height_310: CGFloat = 310.0
      static let height_125: CGFloat = 125.0
      static let height_20: CGFloat = 20.0
      static let height_15: CGFloat = 15.0
      static let height_70: CGFloat = 70.0
      static let height_185: CGFloat = 185.0
      static let height_95: CGFloat = 95.0
      static let height_75: CGFloat = 75.0
      static let height_137: CGFloat = 137.0
      static let height_115: CGFloat = 115.0
      static let height_175: CGFloat = 175.0
      static let height_450: CGFloat = 450.0
      static let height_140: CGFloat = 140.0
      static let height_160: CGFloat = 160.0
      static let height_180: CGFloat = 180.0
      static let height_230: CGFloat = 230.0
      static let height_260: CGFloat = 260.0
      static let height_270: CGFloat = 270.0
      static let height_240: CGFloat = 240.0
       static let height_250: CGFloat = 250.0
      static let height_200: CGFloat = 200.0
      static let height_165: CGFloat = 165.0
      static let height_55: CGFloat = 55.0
      static let height_210: CGFloat = 210.0
      static let height_265: CGFloat = 265.0
      static let height_121: CGFloat = 121.0
      static let height_320: CGFloat = 320.0
      static let height_198: CGFloat = 198.0
      static let height_90: CGFloat = 90.0
      static let height_360: CGFloat = 360.0
      static let height_245: CGFloat = 245.0
      static let height_190: CGFloat = 190.0
      static let height_128: CGFloat = 128.0
      static let height_192: CGFloat = 192.0
    }
    
}
    
    
