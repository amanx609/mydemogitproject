//
//  UIView+Additions.swift
//  MapBox
//
//  Created by APPLE on 24/02/22.
//

import UIKit


public extension UIView {
    
    func initializeFromNib(nibNamed: String) {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibNamed, bundle: bundle)
        if let view = nib.instantiate(withOwner: self, options: nil).first as? UIView {
            view.frame = bounds
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            addSubview(view)
        }
    }
    
    func initializeFromNibAndGetView(nibNamed: String)-> UIView?
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibNamed, bundle: bundle)
        if let view = nib.instantiate(withOwner:nil, options: nil).first as? UIView {
            view.frame = bounds
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            return view
        }
        
        return nil
    }
    
    
    func roundCorners(_ cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
        self.layer.masksToBounds = true
    }
    
    func addShadow(_ shadowRadius: CGFloat = 5.0, shadowOpacity: Float = 0.2) {
        self.layer.masksToBounds = false
        self.clipsToBounds = false
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = shadowOpacity
    }
    
    func addShadow(ofColor color: UIColor = UIColor.black,radius: CGFloat = 3,
                   offset: CGSize = CGSize.zero,
                   opacity: Float = 0.5)
    {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
        self.layer.masksToBounds = false
        self.clipsToBounds = false
    }
    
    func addShadow(_ sides: UIRectEdge, shadowSize: CGFloat, shadowColor: UIColor) {
        let shadowPath = UIBezierPath(rect: CGRect(x: shadowSize ,
                                                   y: shadowSize ,
                                                   width: self.frame.size.width + shadowSize,
                                                   height: self.frame.size.height + shadowSize))
        self.layer.masksToBounds = false
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = 0.5
        self.layer.shadowPath = shadowPath.cgPath
        
    }
    
   func addShadow(shadowColor: UIColor, offSet: CGSize, opacity: Float, shadowRadius: CGFloat, cornerRadius: CGFloat, corners: UIRectCorner, fillColor: UIColor = .white) {
        
        let shadowLayer = CAShapeLayer()
        let size = CGSize(width: cornerRadius, height: cornerRadius)
        let cgPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: size).cgPath //1
        shadowLayer.path = cgPath //2
        shadowLayer.fillColor = fillColor.cgColor //3
        shadowLayer.shadowColor = shadowColor.cgColor //4
        shadowLayer.shadowPath = cgPath
        shadowLayer.shadowOffset = offSet //5
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius = shadowRadius
        self.layer.addSublayer(shadowLayer)
    }
    
    func makeLayer( color: UIColor, boarderWidth: CGFloat, round:CGFloat) -> Void {
        self.layer.borderWidth = boarderWidth;
        self.layer.cornerRadius = round;
        self.layer.masksToBounds =  true;
        self.layer.borderColor = color.cgColor
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        self.layer.mask = shape
    }
        
    func animation(view:UIView)
    {
        view.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5.0, options: .allowUserInteraction , animations: {
            view.transform = CGAffineTransform.identity
        }) { (fininsh) in
            
        }
    }
    
    func animateView()
    {
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
        }) { (true) in
            UIView.animate(withDuration: 1.0, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.layoutIfNeeded()
            }) { (true) in
            }
        }
    }
    
    func fadeOut() {
        UIView.animate(withDuration: 0.35, animations: {
            //            self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        }) { (finished : Bool) in
            if finished {
                self.removeFromSuperview()
            }
        }
    }
    
    func drawGradient(startColor: UIColor, endColor: UIColor) {
        self.layer.sublayers = nil
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [startColor.cgColor, endColor.cgColor]
        gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.5, y: 1.0)
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func drawGradientLeftToRight(startColor: UIColor, endColor: UIColor) {
        self.layer.sublayers = nil
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [startColor.cgColor, endColor.cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.layer.insertSublayer(gradient, at: 0)
    }
}

extension UIView {
    func addDashedLine(color: UIColor = UIColor.lightGray,horizontal: Bool = true ) {
        layer.sublayers?.filter({ $0.name == "DashedTopLine" }).map({ $0.removeFromSuperlayer() })
        self.backgroundColor = UIColor.clear
        let cgColor = color.cgColor
        
        let shapeLayer: CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.name = "DashedTopLine"
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width / 2, y: frameSize.height / 2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [4, 4]
        
        let path = CGMutablePath()
        path.move(to: CGPoint.zero)
        
        if horizontal {
            path.addLine(to: CGPoint(x: frame.width, y: 0))
        } else {
            path.addLine(to: CGPoint(x: 0, y: frame.height))
        }
        
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
    
    func addRoundDashedLine(color: UIColor = UIColor.lightGray, backgroundColor: UIColor = UIColor.redButtonColor ) {
        
        let shapeLayer = CAShapeLayer()
        let frameSize = self.bounds.size
        let rect = CGRect(x:0 , y: 0, width: frameSize.width, height: frameSize.height)
//        shapeLayer.frame = shapeRect
//        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
//        shapeLayer.fillColor = UIColor.clear.cgColor
//        shapeLayer.strokeColor = color.cgColor
//        shapeLayer.lineWidth = 2
//        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
//        shapeLayer.lineDashPattern = [6,6]
//        shapeLayer.path = UIBezierPath(roundedRect: self.frame, cornerRadius: 3.5).cgPath
//        self.backgroundColor = backgroundColor
//        self.layer.addSublayer(shapeLayer)
        
        layer.sublayers?.filter({ $0.name == "DashedTopLine" }).map({ $0.removeFromSuperlayer() })
        self.backgroundColor = UIColor.clear
        let layer = CAShapeLayer.init()
        layer.name = "DashedTopLine"
        let path = UIBezierPath(roundedRect: rect, cornerRadius: 3.5)
        layer.path = path.cgPath;
        layer.strokeColor = color.cgColor
        layer.lineDashPattern = [6,6]
        layer.lineWidth = 2
        //layer.backgroundColor = UIColor.clear.cgColor;
        layer.fillColor = UIColor.clear.cgColor;
        self.layer.addSublayer(layer);
    }
}
