//
//  TopBar.swift
//  MapBox
//
//  Created by APPLE on 23/02/22.
//

import UIKit

protocol TopBarDelegate : AnyObject {
    func leftButtonAction(_ sender: TopBar)
    func rightButtonAction(_ sender: TopBar)
}

extension TopBarDelegate where Self : BaseViewController {
    func leftButtonAction(_ sender: TopBar) {}
    func rightButtonAction(_ sender: TopBar) {}
}


final class TopBar: UIView {

    var tempLabel : UILabel?
    
    @IBOutlet weak var rightbutton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    private weak var actionDelegate : TopBarDelegate?
    
    @IBOutlet public weak var delegate : AnyObject? {
        didSet {
            self.actionDelegate = delegate as? TopBarDelegate
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    @IBInspectable var inheritFont : String?
    @IBInspectable var rightTintColor : UIColor?
    
    @IBInspectable var titleColor : UIColor? {
        didSet {
            self.titleLabel.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
    }
    
    @IBInspectable var labelText : String? {
        didSet {
            if self.labelText == "" && titleLabel.text == "" {
                titleLabel.isHidden = true
            } else {
                titleLabel.isHidden = false
                titleLabel.text = labelText
            }
        }
    }
    
    @IBInspectable var rightTitle : String? {
        didSet {
            self.rightbutton.setTitle(rightTitle, for: .normal)
        }
    }
    
    
    
    
    

    func commonInit() {
        Bundle.main.loadNibNamed("TopBar", owner: self)
        if let label = self.subviews.first as? UILabel {
//            self.tempLabel = label.font
        }
        self.subviews.forEach({$0.removeFromSuperview()})
        self.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            contentView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0)
        ])
    }
}
