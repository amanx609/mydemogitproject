//
//  AlertHandler.swift
//  Wadooni
//
//  Created by appl on 23/11/19.
//  Copyright © 2019 com.ripenapps.wadooni. All rights reserved.
//

import Foundation
import UIKit

//class AlertHandler: NSObject {
//    static func makeAlertWithSingleAction(withTitle title:String, withMessage message:String, withButtonTitle buttonTitle:String, withHandler handler:@escaping () -> Void) -> UIAlertController{
//        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        let firstAction = UIAlertAction(title: buttonTitle, style: .default, handler: {
//            action in
//            handler()
//        })
//        alert.addAction(firstAction)
//        return alert
//    }
//
//}
class CustomUITextField: UITextField {

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
                if action == "paste:" {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
