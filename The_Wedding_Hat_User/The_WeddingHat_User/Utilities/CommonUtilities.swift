//
//  CommanUtilities.swift
//  The_WeddingHat_User
//
//  Created by apple on 20/05/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//


import UIKit
import Alamofire
import Foundation
import MBProgressHUD
import AlamofireImage
import SlideMenuControllerSwift


class CommonUtilities{
    
    var window : UIWindow?
    let preferences = UserDefaults.standard
    var isLogout = Bool()
    static let shared = CommonUtilities()
    
    func getuserdata() -> Userinfo?
    {
        let objectData = UserDefaults.standard.object(forKey: "User_info") as? Data?
        if let storedObject = objectData as? NSData
        {
            let fetchedObject: Userinfo = Userinfo(data: storedObject)!
            return fetchedObject
        }
        else
        {
            return nil
        }
    }
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func setHomeScreen()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objTabbar = storyboard.instantiateViewController(withIdentifier: "Tabbar") as! Tabbar
        let navObj = UINavigationController(rootViewController: objTabbar)
        UIApplication.shared.windows.first?.rootViewController = navObj
    }
    func setupinitialView()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navcontroller = storyboard.instantiateViewController(withIdentifier: "homenav") as! UINavigationController
        UIApplication.shared.windows.first?.rootViewController = navcontroller
    }
    
    func setLoginScreen()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objTabbar = storyboard.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
        let navObj = UINavigationController(rootViewController: objTabbar)
        navObj.isNavigationBarHidden = true
        
        UIApplication.shared.windows.first?.rootViewController = navObj
    }
    
    
    
    func sidemenu()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navcontroller = storyboard.instantiateViewController(withIdentifier: "Tabbar") as! Tabbar
        let sidemenuvc = storyboard.instantiateViewController(withIdentifier: "SidemenuVC") as! SidemenuVC
        let sideMenuViewController: SlideMenuController = SlideMenuController(mainViewController: navcontroller, leftMenuViewController: sidemenuvc)
        SlideMenuOptions.contentViewScale = 1.0
        let topnavigation = UINavigationController(rootViewController: sideMenuViewController)
        topnavigation.isNavigationBarHidden = true
        UIApplication.shared.windows.first?.rootViewController = topnavigation
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    func getselectedLanguage() -> Int{
        let objectData = UserDefaults.standard.object(forKey: "selectedcountryLanguage") as? Int?
        if let storedObject = objectData as? Int
        {
            return storedObject
        }
        return 0
    }
}


extension CommonUtilities{
    func hitServiceCountryList()
    {
        ServiceManager.instance.request(method: .get, URLString:"countryList", parameters:nil, encoding: JSONEncoding.default, headers: nil)
        { (success, dictionary, error) in
            if (success)!{
                GlobalData.shared.countryArray.removeAll()
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if let dict = dictionary?["data"] as? [String:Any]
                    {
                        if let countryList = dict["countryList"] as? [[String:Any]]
                        {
                            if statuscode == 200{
                                for dic in countryList
                                {
                                    var obj = CountryModel()
                                    if let id = dic["id"] as? Int
                                    {
                                        obj.id = id
                                    }
                                    if let country_name = dic["country_name"] as? String
                                    {
                                        obj.country_name = country_name
                                    }
                                    if let cities = dic["cities"] as? [[String:Any]]
                                    {
                                        for city in cities{
                                            var cityObj = ModelCities()
                                            if let id = city["id"] as? Int
                                            {
                                                cityObj.id = String(id)
                                            }
                                            if let city_name = city["city_name"] as? String
                                            {
                                                cityObj.city_name = city_name
                                            }
                                            if let country_id = city["country_id"] as? Int
                                            {
                                                cityObj.country_id = String(country_id)
                                            }
                                            obj.cities.append(cityObj)
                                        }
                                    }
                                    GlobalData.shared.countryArray.append(obj)
                                }
                            }
                        }
                    }
                }
                else{
                    //print(parameters)
                }
            }
        }
    }
    
    
    func hitServicegetEventType()
    {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        ServiceManager.instance.request(method: .get, URLString: "eventType", parameters:nil, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            if (success)!{
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        GlobalData.shared.eventTypeArray.removeAll()
                        if let dict = dictionary?["data"] as? [String:Any]
                        {
                            if let eventtype = dict["eventType"] as? [[String:Any]]
                            {
                                for dic in eventtype
                                {
                                    var obj = EventTypeMoel()
                                    if let id = dic["id"] as? Int
                                    {
                                        obj.id = id
                                    }
                                    if let event_type = dic["event_type"] as? String
                                    {
                                        obj.event_type = event_type
                                    }
                                    GlobalData.shared.eventTypeArray.append(obj)
                                }
                            }
                        }
                    }
                    else{
                        //self.createAlert(title: Constant.string.appname, message: msg)
                    }
                }
            }
            else{
                // print(parameters)
            }
        }
    }
}


struct  GlobalData {
    static var shared = GlobalData()
    var countryArray = [CountryModel]()
    var eventTypeArray = [EventTypeMoel]()
}
class CountryModel
{
    var id = Int()
    var cities = [ModelCities]()
    var country_name = ""
}
class EventTypeMoel{
    var id = Int()
    var event_type = ""
}
class ModelCities{
    var id = ""
    var city_name = ""
    var country_id = ""
}

class RealWeddingList{
    var name         = ""
    var partner_name = ""
    var country      = ""
    var city         = ""
    var wedding_date = ""
    var description = ""
    var hired_all_vendors = ""
    var images = [String]()
    var vendorArray = [Vendors]()
    var questionAnsArray = [QuestionAnswer]()
    var weddingImageArray = [Weddingimages]()
}

class Vendors{
    var vendor_id = ""
    var vendor_name = ""
}
//MARK:- Poll Question Model Class
class QuestionAnswer{
    var que_Id = ""
    var question = ""
    var is_answer = Bool()
    var answer = [Answer]()
}

class Weddingimages{
    var image = ""
}
//MARK:- Poll Answer Model Class
class Answer
{
    var option_id = ""
    var option = ""
    var percentage = ""
    var is_selected = Bool()
}


