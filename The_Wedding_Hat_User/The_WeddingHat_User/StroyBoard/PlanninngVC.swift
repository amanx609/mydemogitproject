//
//  PlanninngVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 05/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class PlanninngVC: UIViewController {
    
    @IBOutlet weak var headerVIew: UIView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var planningColl: UICollectionView!
    var planningArray = [["image":"register",
                          "text":Constant.string.checklist.localized()],
                         ["image":"wallet 12",
                          "text":Constant.string.budgeter.localized()],
                         ["image":"guest",
                          "text":Constant.string.guest.localized()],
                         ["image":"vendor",
                          "text":Constant.string.vendorTeam.localized()],
                         ["image":"heart",
                          "text":Constant.string.dream.localized()],
                         ["image":"Journey",
                          "text":Constant.string.journey.localized()]]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerVIew.dropShadow()
        self.headerLbl.text = Constant.string.plan.localized()
    }
}

extension PlanninngVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return planningArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlanningCell", for: indexPath) as! PlanningCell
        cell.titleLbl.text = planningArray[indexPath.row]["text"]
        cell.imgView.image =  UIImage(named: planningArray[indexPath.row]["image"]!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        if(UIScreen.main.bounds.width > 375){
            return CGSize(width: (width / 2) - 20, height: 145)
        }
        return CGSize(width: (width / 2) - 20, height: 135)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if (indexPath.row == 2)
        {
            let storyboard = UIStoryboard(name: "IdeasAndInspire", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "GuestListVC") as! GuestListVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            self.createAlert(message: Constant.string.underdevlopment)
        }
        
    }
}
