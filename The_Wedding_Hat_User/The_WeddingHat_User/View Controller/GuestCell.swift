//
//  GuestCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 04/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class GuestCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var guestLogo: UIImageView!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var poinstlbl: UILabel!
    @IBOutlet weak var totalguestlbl: UILabel!
    @IBOutlet weak var pendinglbl: UILabel!
    @IBOutlet weak var pendingview: UIView!
    @IBOutlet weak var pendingGuestLbl: UILabel!
    @IBOutlet weak var declinedView: UIView!
    @IBOutlet weak var declinedguestLbl: UILabel!
    @IBOutlet weak var decliendLbl: UILabel!
    override func awakeFromNib() {
    super.awakeFromNib()
        self.mainView.makeRoundCorner(10)
        self.mainView.dropShadow()
        self.guestLogo.makeRounded()
        self.pendingview.makeRoundCorner(6)
        self.declinedView.makeRoundCorner(6)
        self.headingLbl.text = Constant.string.guest.localized()
        self.descLbl.text = Constant.string.confirmedGuest.localized()
    }

    

}
