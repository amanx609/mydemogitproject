//
//  WeddingPartyVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 20/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class WeddingPartyVC: UIViewController {

    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var addMemberButton: UIButton!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var memberTblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpDesign()

    }
   
    
}
extension WeddingPartyVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeddingPartyMemberCell", for: indexPath) as! WeddingPartyMemberCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    
}

extension WeddingPartyVC{
    func setUpDesign(){
        self.headView.dropShadowBlueColor()
        self.buttonView.makeRounded()
 
    }
}
extension WeddingPartyVC{
   @IBAction func addMember(_ sender: Any) {
//    let stryBrd : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//    let vc = stryBrd.instantiateViewController(withIdentifier: "NewAddMemberVC") as! NewAddMemberVC
//    vc.modalPresentationStyle = .fullScreen
//    self.present(vc, animated: true, completion: nil)
    }
    @IBAction func backButtonTap(_ sender: Any) {
        
    }
}
