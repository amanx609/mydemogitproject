//
//  SendInvitationVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 22/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import KRProgressHUD
import Alamofire
import AlamofireImage

class SendInvitationVC: UIViewController {
    
    @IBOutlet weak var headerIVew: UIView!
    @IBOutlet weak var img_view: UIImageView!
    @IBOutlet weak var preview_btn: UIButton!
    @IBOutlet weak var groomName_txt: UITextField!
    @IBOutlet weak var eventDate_txt: UITextField!
    @IBOutlet weak var eventTime_txt: UITextField!
    @IBOutlet weak var brideName_txt: UITextField!
    @IBOutlet weak var InviteTitle_txt: UITextField!
    @IBOutlet weak var eventLocation_txt: UITextField!
    @IBOutlet weak var invitation_message_txt: UITextField!
    @IBOutlet weak var changePhotoButtonView: UIView!
    @IBOutlet weak var changePhotoButton: UIButton!
    var selectedImage: UIImage?
    var params = [String:Any]()
    var parent_guest_id = [Int]()
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setUI()
        self.picker()
    }
    
    @IBAction func tapOnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnSend(_ sender: Any)
    {
        self.validation()
    }
    
    @IBAction func changePhotoButton(_ sender: Any)
    {
        self.presentImagePickerController(allowEditing: true) { (image, nil) in
            if image != nil
            {
                self.img_view.contentMode = .scaleAspectFill
                self.img_view.image = image
                self.selectedImage = image
            }
        }
    }
    
    func setUI()
    {
        self.headerIVew.dropShadow()
        self.img_view.makeRoundCorner(8)
        self.preview_btn.makeRoundCorner(8)
        self.brideName_txt.colorbluePlaceHolder(placeholder: "Bride name")
        self.groomName_txt.colorbluePlaceHolder(placeholder: "Groom name")
        self.eventDate_txt.colorbluePlaceHolder(placeholder: "Event date")
        self.eventTime_txt.colorbluePlaceHolder(placeholder: "Event time")
        self.InviteTitle_txt.colorbluePlaceHolder(placeholder: "Invitation title")
        self.eventLocation_txt.colorbluePlaceHolder(placeholder: "Event location")
    }
    
    func picker(){
        self.eventDate_txt.setDatePickerWithDateFormate(dateFormate: "dd MMM yyyy", defaultDate: nil, isPrefilledDate: false) { (date) in
        }
        self.eventDate_txt.setDatePickerMode(mode: .date)
        self.eventDate_txt.setMinimumDate(minDate:  NSDate() as Date)
        
        self.eventTime_txt.setDatePickerMode(mode: .time)
        self.eventTime_txt.setDatePickerWithDateFormate(dateFormate: "HH:mm a", defaultDate: nil, isPrefilledDate: false) { (date) in
            self.eventTime_txt.text = date.toString(dateFormat: "hh:mm a")
        }
    }
    
}

extension SendInvitationVC{
    
    func validation() -> Bool{
        
        guard let Bridename = brideName_txt.text , !Bridename.isEmpty else{
            createAlert(message: "Bride name can't be empty")
            return false
        }
        
        guard let groomName = groomName_txt.text , !groomName.isEmpty else{
            createAlert(message: "Groom name can't be empty")
            return false
        }
        
        guard let InvitationTitle = InviteTitle_txt.text , !InvitationTitle.isEmpty else{
            createAlert(message: "Invitationtitle  can't be empty")
            return false
        }
        guard let EventDate = eventDate_txt.text , !EventDate.isEmpty else{
            createAlert(message: "Event date can't be empty")
            return false
        }
        guard let Eventtime = eventTime_txt.text , !Eventtime.isEmpty else{
            createAlert(message: "Event time can't be empty")
            return false
        }
        
        guard let eventMsg = invitation_message_txt.text , !eventMsg.isEmpty else{
            createAlert(message: "Event message can;t be empty")
            return false
        }
        guard let location = eventLocation_txt.text , !location.isEmpty else{
            createAlert(message: "Location can't be empty")
            return false
        }
        if (selectedImage == nil){
            createAlert(message: "Uplaod a image")
            return false
        }

        self.dataSet()
        return true
        
    }
    
    func dataSet(){
        params["user_id"] = CommonUtilities.shared.getuserdata()?.id ?? ""
        params["event_id"]  = UserDefaults.standard.object(forKey: "EventIDFromCreateEVent") as? String ?? ""
        params["invitation_title"]  =  self.InviteTitle_txt.text!
        params["groom_name"]       = self.groomName_txt.text!
        params["bride_name"]       = self.brideName_txt.text!
        params["event_date"]       = self.eventDate_txt.text!
        params["event_time"]       = self.eventTime_txt.text!
        params["event_location"]   = self.eventLocation_txt.text!
        params["invitation_message"]   = self.invitation_message_txt.text!
        self.PushToVC()
    }
    func PushToVC(){
        let storyboard = UIStoryboard(name: "IdeasAndInspire", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "InvitationVC") as! InvitationVC
        vc.DictParams = params
        vc.selectedImage = self.selectedImage
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


