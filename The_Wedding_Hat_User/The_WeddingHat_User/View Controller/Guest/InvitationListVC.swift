//
//  InvitationListVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 23/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

class InvitationListVC: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var send_btn: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var selectAllImgView: UIImageView!
    @IBOutlet weak var invitationListTable: UITableView!
    @IBOutlet weak var selectAllButton: UIButton!
    
    var selectedListID = [String]()
    var selectAll = false
    var headerSection = ["abc","xyz","cfb"]
    var objGuestListMOdel = [GuestListMOdel]()
    var searchArray = [GuestListMOdel]()
    var shouldShowSearchResults = false
    var guestArray = [String]()
    var invitationParams = [String:Any]()
    var image : UIImage?
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.txtSearch.delegate = self
        self.setUI()
        self.HitServiceInvitationList(search: "")
    }
    
    @IBAction func tapOnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnSelectAll(_ sender: Any)
    {
        
    }
    
    @IBAction func tapOnSendInvitation(_ sender: Any)
    {
        self.hitSendInvitation()
    }
    
    @IBAction func selectAllButtonTap(_ sender: Any)
    {
        if selectAll == false
        {
            selectAll = true
            selectAllImgView.image = #imageLiteral(resourceName: "checkbox_selected")
        }
        else
        {
            selectAllImgView.image = #imageLiteral(resourceName: "checkbox_unselected")
             selectAll = false
        }
        self.invitationListTable.reloadData()
    }
    

    func setUI()
    {
        self.headerView.dropShadow()
        self.searchView.makeRoundCorner(8)
        self.searchView.dropShadow()
        self.send_btn.makeRoundCorner(8)
        self.invitationListTable.separatorColor = .clear
        let nibName = UINib(nibName: "InvitationHeader", bundle: nil)
        self.invitationListTable.register(nibName, forHeaderFooterViewReuseIdentifier: "InvitationHeader")
    }
}

extension InvitationListVC: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
//        return shouldShowSearchResults == true ? searchArray.count : objGuestListMOdel.count
        return objGuestListMOdel.count
//        return searchArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
//        return shouldShowSearchResults == true ? searchArray[section].groupArray.count : objGuestListMOdel[section].groupArray.count
        return objGuestListMOdel[section].groupArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvitationListCell", for: indexPath) as! InvitationListCell
        
        
        if objGuestListMOdel[indexPath.section].groupArray[indexPath.row].guest_email == ""
        {
            cell.select_icon.image = #imageLiteral(resourceName: "circle_tick_unselected")
        }
        else
        {
            if selectAll == true
            {
                objGuestListMOdel[indexPath.section].groupArray[indexPath.row].isSelected = true
                cell.select_icon.image = #imageLiteral(resourceName: "circle_tick_selected")
            }
            else
            {
                if objGuestListMOdel[indexPath.section].groupArray[indexPath.row].isSelected == true
                {
                    cell.select_icon.image = #imageLiteral(resourceName: "circle_tick_selected")
                }
                else
                {
                    cell.select_icon.image = #imageLiteral(resourceName: "outline_cirlce_selected")
                }
            }
        }
        cell.selectionStyle = .none
        cell.name_lbl.text = objGuestListMOdel[indexPath.section].groupArray[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        guestArray.removeAll()
        if objGuestListMOdel[indexPath.section].groupArray[indexPath.row].isSelected == false
        {
            guestArray.append(objGuestListMOdel[indexPath.section].groupArray[indexPath.row].id)
            objGuestListMOdel[indexPath.section].groupArray[indexPath.row].isSelected = true
        }
        else
        {
             objGuestListMOdel[indexPath.section].groupArray[indexPath.row].isSelected = false
            guestArray.remove(at: indexPath.row)
        }
         self.invitationListTable.reloadData()
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "InvitationHeader") as! InvitationHeader
        header.name_lbl.text = objGuestListMOdel[section].groupsName
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 30
    }
}

extension InvitationListVC: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
          {
                if txtSearch.text! != ""
                         {
                             self.searchArray.removeAll()
                             for i in objGuestListMOdel
                             {
                                if (i.groupsName).contains(txtSearch.text!)
                                 {
                                     self.searchArray.append(i)
                                 }
                             }
                         }
                         else
                         {
                             self.searchArray = self.objGuestListMOdel
                         }
                         self.invitationListTable.reloadData()
                         return true
          }

          func textFieldDidEndEditing(_ textField: UITextField)
          {
            if txtSearch.text! != ""
            {
                self.searchArray.removeAll()
                for i in objGuestListMOdel
                {
                    if (i.groupsName).contains(txtSearch.text!)
                    {
                        self.searchArray.append(i)
                    }
                }
            }
            else
            {
                self.searchArray = self.objGuestListMOdel
            }
            self.invitationListTable.reloadData()

          }
}

extension InvitationListVC
{
    func HitServiceInvitationList(search : String)
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = ["Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Accept": "application/json"]
        
        let parameters = ["user_id": CommonUtilities.shared.getuserdata()?.id ?? "",
                          "event_id": UserDefaults.standard.object(forKey: "EventIDFromCreateEVent") as? String ?? "",
            "search" : search] as [String:Any]
        

        
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "listForEventInvitation", parameters:parameters, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = dictionary?["data"] as? [String:Any]
                        {
                            if let eventGuestList = data["eventGuestList"] as? [[String:Any]]
                            {
                                self.objGuestListMOdel.removeAll()
                                for list in eventGuestList
                                {
                                    let obj = GuestListMOdel()
                                    if let groupsName = list["groupsName"] as? String
                                    {
                                        obj.groupsName = groupsName
                                    }
                                    if let groupArray = list["groupArray"] as? [[String:Any]]
                                    {
                                        obj.groupArray.removeAll()
                                        for group in groupArray
                                        {
                                            let objGroup = Model_groupArray()
                                            if let id = group["id"] as? Int
                                            {
                                                objGroup.id = String(id)
                                            }
                                            if let flag = group["flag"] as? Bool
                                            {
                                                objGroup.flag = flag
                                            }
                                            if let groups = group["groups"] as? String
                                            {
                                                objGroup.groups = groups
                                            }
                                            if let name = group["name"] as? String
                                            {
                                                objGroup.name = name
                                            }
                                            if let guest_email = group["guest_email"] as? String
                                            {
                                                objGroup.guest_email = guest_email
                                            }
                                            if let companion = group["companion"] as? [[String:Any]]
                                            {
                                                objGroup.companion.removeAll()
                                                for champ in companion
                                                {
                                                    let objcamp = companionModel()
                                                    if let id = champ["id"] as? Int
                                                    {
                                                        objcamp.id = String(id)
                                                    }
                                                    if let flag = champ["flag"] as? Bool
                                                    {
                                                        objcamp.flag = flag
                                                    }
                                                    if let name = champ["name"] as? String
                                                    {
                                                        objcamp.name = name
                                                    }
                                                    if let age_group = champ["age_group"] as? String
                                                    {
                                                        objcamp.age_group = age_group
                                                    }
                                                    if let first_name = champ["first_name"] as? String
                                                    {
                                                        objcamp.first_name = first_name
                                                    }
                                                    if let last_name = champ["last_name"] as? String
                                                    {
                                                        objcamp.last_name = last_name
                                                    }
                                                    if let event_guest_id = champ["event_guest_id"] as? Int
                                                    {
                                                        objcamp.event_guest_id = String(event_guest_id)
                                                    }
                                                    objGroup.companion.append(objcamp)
                                                }
                                            }
                                            obj.groupArray.append(objGroup)
                                        }
                                    }
                                    self.objGuestListMOdel.append(obj)
                                }
                                self.invitationListTable.reloadData()
                            }
                        }
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                    }
                }
            }
            
        }
    }
    
    func hitSendInvitation(){
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        let dataArea = try! JSONSerialization.data(withJSONObject: guestArray, options: .prettyPrinted)
        let jsonTagString = String(data: dataArea, encoding: .utf8)!
//        print(invitationParams)
       self.invitationParams["invitation_mail_send_to"] = jsonTagString
        print(self.invitationParams)
        Alamofire.upload(multipartFormData: { multipartFormData in
            if self.image != nil {
                if self.image!.size.width != 0{
                    let imageData1 =
                        self.image!.jpegData(compressionQuality: 0.5)!
                    multipartFormData.append(imageData1, withName: "event_image", fileName: "\(imageData1)images.jpg", mimeType: "image/jpeg")
                }
            }
            
            
            for (key, value) in self.invitationParams{
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: BASEURL + "addEventInvitation", headers: headers,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    KRProgressHUD.dismiss()
                    let result = response.result.value
                    let responseDic = result as! [String : Any]
                    print(responseDic)
                    
                    if let statuscode = responseDic["status"] as? NSNumber
                    {
                        if statuscode == 200
                        {
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: GuestListVC.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }
                        else
                        {
                            if let msg = responseDic["message"] as? String
                            {
                                self.createAlert(message: msg)
                            }
                        }
                    }
                }
            case .failure(let error):
                print(error)
                KRProgressHUD.dismiss()
            }
        })
    }
    
    
}

class Model_groupArray
{
    var isSelected = false
    var id = ""
    var name = ""
    var flag = Bool()
    var groups = ""
    var guest_email = ""
    var companion = [companionModel]()
}

class companionModel
{
    var id = ""
    var name = ""
    var flag = Bool()
    var first_name = ""
    var last_name = ""
    var age_group = ""
    var event_guest_id = ""
}

class GuestListMOdel
{
    var groupsName = ""
    var groupArray = [Model_groupArray]()
}

