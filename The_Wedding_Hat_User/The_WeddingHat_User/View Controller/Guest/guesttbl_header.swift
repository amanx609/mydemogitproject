//
//  guesttbl_header.swift
//  The_WeddingHat_User
//
//  Created by apple on 29/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import Foundation
import UIKit

protocol HeaderButtonTap
{
    func guestGroupButtonTap(section : Int)
}

class guesttbl_header: UITableViewHeaderFooterView
{
    @IBOutlet weak var guestGroupLabel : UILabel!
    @IBOutlet weak var guestGroupNumberLbl : UILabel!
    @IBOutlet weak var guestGroupButton : UIButton!
    var currentsection = 0
    var headerDelegate : HeaderButtonTap?
    @IBAction func taponheader(sender : UIButton)
    {
        headerDelegate?.guestGroupButtonTap(section: currentsection)
    }
}
