//
//  GuestListVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 22/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

class GuestListVC: UIViewController , HeaderButtonTap, ExpandCompanionMember{
    
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var stackViewOfButtons: UIView!
    @IBOutlet weak var totalGuestNumberTextlbl: UILabel!
    @IBOutlet weak var totalGuestNumber: UILabel!
    @IBOutlet weak var guestListTableView: UITableView!
    @IBOutlet weak var savebuttonView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var saveButtonLbl: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var pendingView: UIView!
    @IBOutlet weak var pendingLbl: UILabel!
    @IBOutlet weak var pendingButton: UIButton!
    @IBOutlet weak var respondView: UIView!
    @IBOutlet weak var responseLbl: UILabel!
    @IBOutlet weak var addGuestBtn: UIButton!
    @IBOutlet weak var responseButton: UIButton!
    @IBOutlet weak var sendInviteBtn: UIButton!
    @IBOutlet var footer_view: UIView!
    @IBOutlet weak var addGuestView : UIView!
    @IBOutlet weak var filterView : UIView!
    
    var isEdit = true
    var isPending = true
    var isResponded = false
    var isExpand = false
    var section = Int()
    var indexPath1 = Int()
    var obj = EventGuestData()
    var isselected = true
    var parent_id = [Int]()
    var parent_section = Int()
    var update = false

    
    var guestGroupData = [EventGuestList]()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setUI()
        let nibName = UINib(nibName: "guesttbl_header", bundle: nil)
        self.guestListTableView.register(nibName, forHeaderFooterViewReuseIdentifier: "guesttbl_header")
        self.guestListTableView.estimatedRowHeight = 1000
        self.guestListTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.SaveButton()
    }
    
    
}


extension GuestListVC : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return guestGroupData.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "guesttbl_header") as! guesttbl_header
        header.currentsection = section
        header.guestGroupLabel.text = guestGroupData[section].groupName
        header.guestGroupNumberLbl.text = "\(guestGroupData[section].group_count)"
        header.headerDelegate = self
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return guestGroupData[section].expand ? guestGroupData[section].objGroupArr.count  : 0
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        self.parent_section = indexPath.section
        let cell_identifire = guestGroupData[indexPath.section].objGroupArr[indexPath.row].objCompanionDetails.count > 0 ? "GuestNameListCellwithcomp" : "GuestNameListCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cell_identifire, for: indexPath) as! GuestNameListCell 
        cell.index = indexPath
        cell.expandCompanionDelegate = self
        cell.populatedata(guest_obj: guestGroupData[indexPath.section].objGroupArr[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        if self.isselected == true
        {
            return tableView.numberOfSections - 1 == section ?  footer_view : UIView()
        }
        else
        {
            return UIView()
        }
         
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if self.isselected == true
        {
            return tableView.numberOfSections - 1 == section ? 60 : 0.1
        }
        else
        {
            return 0.1
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "IdeasAndInspire", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddGuestVC") as! AddGuestVC
        vc.isEdit = true
        vc.editingOff = self.isEdit
        vc.isResponded = self.isResponded
        vc.txtFIrstName.text = guestGroupData[indexPath.section].objGroupArr[indexPath.row].first_name
        vc.txtEmailAddress.text = guestGroupData[indexPath.section].objGroupArr[indexPath.row].guest_email
        vc.txtLastName.text = guestGroupData[indexPath.section].objGroupArr[indexPath.row].Last_name
        vc.txtSelectGroup.text = guestGroupData[indexPath.section].objGroupArr[indexPath.row].groups
        vc.txtTableNo.text = "\(guestGroupData[indexPath.section].objGroupArr[indexPath.row].table_no)"
        vc.age_group = guestGroupData[indexPath.section].objGroupArr[indexPath.row].age_group
        if(guestGroupData[indexPath.section].objGroupArr[indexPath.row].age_group == "child")
        {
            vc.childBtn()
        }
        else
        {
            vc.adultBtn()
        }
        vc.campaninData = guestGroupData[indexPath.section].objGroupArr[indexPath.row].objCompanionDetails
        vc.guest_id = guestGroupData[indexPath.section].objGroupArr[indexPath.row].id
        vc.flag = guestGroupData[indexPath.section].objGroupArr[indexPath.row].flag
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func guestGroupButtonTap(section: Int)
    {
        guestGroupData[section].expand =  !guestGroupData[section].expand
        guestListTableView.reloadSections([section], with: .automatic)
    }
}

extension GuestListVC{
    
    func guestListServicehit(flag : Int){
        
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        
        let params : Parameters =  ["user_id":CommonUtilities.shared.getuserdata()?.id ?? "",
                                    "event_id": UserDefaults.standard.object(forKey: "EventIDFromCreateEVent") ?? "",
                                    "flag":flag ]
        ServiceManager.instance.request(method: .post, URLString: "eventGuestList", parameters: params, encoding: JSONEncoding.default, headers: headers) { (sucess, response, error) in
            KRProgressHUD.dismiss()
            print(response ?? "")
            if(sucess)!
            {
                if let statuscode = response?["status"] as? NSNumber{
                    print(statuscode)
                    if(statuscode == 200)
                    {
                        if let dict = response?["data"] as? [String:Any]
                        {
                            let objEventGuestData = EventGuestData()
                            if let totalSaveCount = dict["totalSavedCount"] as? Int
                            {
                                objEventGuestData.totalCount = String(totalSaveCount)
                                self.totalGuestNumber.text = String(totalSaveCount)
                            }
                            if let eventList = dict["eventGuestList"] as? [[String:Any]]
                            {
                                self.guestGroupData.removeAll()
                                for arraydata in eventList
                                {
                                    let objEventGuestList = EventGuestList()
                                    if let groupsName = arraydata["groupsName"] as? String
                                    {
                                        objEventGuestList.groupName = groupsName
                                    }
                                    if let groupsCount = arraydata["groupsCount"] as? Int
                                    {
                                        objEventGuestList.group_count = groupsCount
                                    }
                                    if let groupsArray = arraydata["groupArray"] as? [[String:Any]]
                                    {
                                        for groupData in groupsArray
                                        {
                                            let groupsArray = Guest()
                                            print(groupData)
                                            if let id = groupData["id"] as? Int
                                            {
                                                groupsArray.id = id
                                                self.parent_id.append(id)
                                            }
                                            if let name = groupData["name"] as? String
                                            {
                                                groupsArray.name = name
                                            }
                                            if let first_name = groupData["first_name"] as? String
                                            {
                                                groupsArray.first_name = first_name
                                            }
                                            if let last_name = groupData["last_name"] as? String
                                            {
                                                groupsArray.Last_name = last_name
                                            }
                                            if let guest_email = groupData["guest_email"] as? String
                                            {
                                                groupsArray.guest_email = guest_email
                                            }
                                            if let groups = groupData["groups"] as? String
                                            {
                                                groupsArray.groups = groups
                                            }
                                            if let table_no = groupData["table_no"] as? Int
                                            {
                                                groupsArray.table_no = table_no
                                                
                                            }
                                            if let age_group = groupData["age_group"] as? String
                                            {
                                                groupsArray.age_group = age_group
                                                
                                            }
                                            if let flag = groupData["flag"] as? Int
                                            {
                                                groupsArray.flag = flag
                                                
                                            }
                                            if let companionCount = groupData["companionCount"] as? Int
                                            {
                                                groupsArray.companion_count = companionCount
                                                
                                            }
                                            if let companionArray = groupData["companion"] as? [[String:Any]]
                                            {
                                                for i in companionArray
                                                {
                                                    let objCompanion = Companion()
                                                    if let first_name = i["first_name"] as? String
                                                    {
                                                        objCompanion.first_name = first_name
                                                    }
                                                    if let last_name = i["last_name"] as? String
                                                    {
                                                        objCompanion.last_name = last_name
                                                        
                                                    }
                                                    if let age_group = i["age_group"] as? String
                                                    {
                                                        objCompanion.age_group = age_group
                                                        
                                                    }
                                                    if let name = i["name"] as? String
                                                    {
                                                        objCompanion.name = name
                                                        
                                                    }
                                                    if let id = i["id"] as? Int
                                                    {
                                                        objCompanion.id = id
                                                        
                                                    }
                                                    if let flag = i["flag"] as? Int
                                                    {
                                                        objCompanion.flag = flag
                                                        
                                                    }
                                                    if let event_guest_id = i["event_guest_id"] as? Int
                                                    {
                                                        objCompanion.event_guest_id = event_guest_id
                                                        
                                                    }
                                                    groupsArray.objCompanionDetails.append(objCompanion)
                                                    // Append eventGusestList --> group Array --> Companion
                                                }
                                            }
                                            objEventGuestList.objGroupArr.append(groupsArray)
                                            //Append eventGusestList --> group Array
                                        }
                                    }
                                    self.guestGroupData.append(objEventGuestList)
                                    // objEventGuestData.objEventGuestList.append(objEventGuestList)
                                    //Append Event Guest List
                                }
                            }
                            self.obj = objEventGuestData
                            self.guestListTableView.reloadData()
                        }
                        
                    }
                    else
                    {
                        if let msg = response?["message"] as? String
                        {
                            self.createAlert(message: msg)
                        }
                    }
                    
                }
            }
        }
}
    

    
    func setUI()
    {
        self.stackViewOfButtons.makeRoundCorner(20)
        self.stackViewOfButtons.layer.borderWidth = 1
        self.stackViewOfButtons.layer.borderColor = #colorLiteral(red: 0.6500751972, green: 0.650090754, blue: 0.6500824094, alpha: 1)
        self.sendInviteBtn.layer.borderWidth = 2
        self.savebuttonView.makeRoundCorner(20)
        self.pendingView.makeRoundCorner(20)
        self.respondView.makeRoundCorner(20)
        self.sendInviteBtn.makeRoundCorner(20)
        self.sendInviteBtn.layer.borderWidth = 1
        self.sendInviteBtn.layer.borderColor = #colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)
        self.addGuestView.makeRoundCorner(20)
        self.headView.dropShadow()
        self.addGuestBtn.makeRoundCorner(20)
    }
    
    
    func SaveButton()
    {
        saveButton.setTitle("Saved", for: .normal)
        saveButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        saveButton.backgroundColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        saveButton.makeRoundCorner(20)
        pendingButton.setTitle("Pending", for: .normal)
        pendingButton.setTitleColor(#colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1), for: .normal)
        pendingButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        responseButton.setTitle("Responded", for: .normal)
        responseButton.setTitleColor(#colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1), for: .normal)
        responseButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.totalGuestNumber.text = "\(obj.totalCount)"
        self.guestListServicehit(flag: 0)
        self.sendInviteBtn.isHidden = false
        self.addGuestBtn.isHidden = false
        self.totalGuestNumberTextlbl.text = "Total Saved Guests: "
        self.totalGuestNumber.text = "\(obj.totalCount)"
        self.isEdit = true
    }
    
    func PendingButton()
    {
        pendingButton.setTitle("Pending", for: .normal)
        pendingButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        pendingButton.backgroundColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        pendingButton.makeRoundCorner(20)
        responseButton.setTitle("Responded", for: .normal)
        responseButton.setTitleColor(#colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1), for: .normal)
        responseButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        saveButton.setTitle("Saved", for: .normal)
        saveButton.setTitleColor(#colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1), for: .normal)
        saveButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.totalGuestNumber.text = "\(obj.totalCount)"
        self.guestListServicehit(flag: 1)
        self.totalGuestNumberTextlbl.text = "Total Pending Guests: "
        self.totalGuestNumber.text = "\(obj.totalCount)"
        self.isResponded = true
    }
    
    func respondButton()
    {
        responseButton.setTitle("Responded", for: .normal)
        responseButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        responseButton.backgroundColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        responseButton.makeRoundCorner(20)
        saveButton.setTitle("Saved", for: .normal)
        saveButton.setTitleColor(#colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1), for: .normal)
        saveButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        pendingButton.setTitle("Pending", for: .normal)
        pendingButton.setTitleColor(#colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1), for: .normal)
        pendingButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.totalGuestNumberTextlbl.text = "Total Confirmed Guests: "
        self.totalGuestNumber.text =  "\(obj.totalCount)"
        self.guestListServicehit(flag: 2)
    }
}

extension GuestListVC
{
    @IBAction func tapOnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonAction(_ sender: Any)
    {
        self.SaveButton()
        self.isselected = true
        self.isEdit = true
        self.guestListTableView.reloadData()
    }
    
    @IBAction func pendingButtonAction(_ sender: Any)
    {
        self.PendingButton()
        self.isEdit = false
        self.isselected = false
        self.guestListTableView.reloadData()
    }
    @IBAction func responseButtonAction(_ sender: Any)
    {
        self.respondButton()
        self.isEdit = false
        self.isselected = false
        self.guestListTableView.reloadData()
    }
    
    @IBAction func tapOnAddGuest(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "IdeasAndInspire", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddGuestVC") as! AddGuestVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tapOnSendInvite(_ sender: Any)
    {
        let storyBoard = UIStoryboard(name: "IdeasAndInspire", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SendInvitationVC") as! SendInvitationVC
        vc.parent_guest_id = self.parent_id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tapOnBackButton(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func CompanionMemberTableExpand(selectedIndex:IndexPath)
    {
        self.guestGroupData[self.parent_section].objGroupArr[selectedIndex.row].isCompanionExpand = !self.guestGroupData[self.parent_section].objGroupArr[selectedIndex.row].isCompanionExpand
        //self.guestListTableView.reloadRows(at: [selectedIndex + 1], with: .automatic)
        self.guestListTableView.reloadData()
        
    }

}



class EventGuestData
{
    var totalCount = ""
    var objEventGuestList = [EventGuestList]()
    
}

class EventGuestList
{
    var expand = false
    var groupName = ""
    var group_count = Int()
    var objGroupArr = [Guest]()
}

class Guest
{
    var id = Int()
    var name = ""
    var first_name = ""
    var Last_name = ""
    var guest_email = ""
    var groups = ""
    var table_no = -1
    var age_group = ""
    var flag = -1
    var companion_count = Int()
    var objCompanionDetails = [Companion]()
    //var isMainExpand = false
    var isCompanionExpand = false
}

class Companion{
    var camp_expand = false
    var id = Int()
    var event_guest_id = Int()
    var first_name =  ""
    var last_name = ""
    var name = ""
    var age_group = ""
    var flag  = Int()
}
