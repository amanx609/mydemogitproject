//
//  GuestNameListCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 23/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

protocol ExpandCompanionMember
{
    func CompanionMemberTableExpand(selectedIndex:IndexPath)
}

class GuestNameListCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var guestName: UILabel!
    @IBOutlet weak var tableNoView: UIView!
    @IBOutlet weak var tableNoLabel: UILabel!
    @IBOutlet weak var emailId: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusType: UILabel!
    @IBOutlet weak var tblcomoanion: UITableView!
    
    @IBOutlet weak var companionView: UIView!
    @IBOutlet weak var topIncompanionView: UIView!
    @IBOutlet weak var companionTotalMemeberLbl: UILabel!
    @IBOutlet weak var guestNameCellButton: UIButton!
    var expandCompanionDelegate : ExpandCompanionMember?
    var index = IndexPath()
    var guestdata = Guest()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tableNoView.layer.cornerRadius = 10.0
        self.tableNoView.layer.masksToBounds = true
        // self.companionView.isHidden = false
    }
    
    
    func populatedata(guest_obj : Guest)
    {
        self.guestdata = guest_obj
        self.guestName.text = guest_obj.name
        self.emailId.text = guest_obj.guest_email
//        self.tableNoLabel.text = String(guest_obj.table_no)
        if(guest_obj.table_no == -1) 
        {
             self.tableNoLabel.text = "--"
        }
        else
        {
            self.tableNoLabel.text = "\(guest_obj.table_no)"
        }
        let flag = guest_obj.flag
        self.statusType.text = flag == 0 ? "Saved" : (flag == 1 ? "Pending" : "Responded")
        if(guest_obj.objCompanionDetails.count > 0)
        {
            self.companionTotalMemeberLbl.text = "\(guest_obj.companion_count.description)" +  " Companion members"
            self.tblcomoanion.reloadData()
        }
    }
    
    @IBAction func expandCompanionMemButton(_ sender: Any)
    {
        self.expandCompanionDelegate?.CompanionMemberTableExpand(selectedIndex: self.index)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    //Prankur Sir code
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize
    {
        contentView.layoutIfNeeded()
        let topConstraintConstant = contentView.constraint(byIdentifier: "topAnchor")?.constant ?? 0
        let bottomConstraintConstant = contentView.constraint(byIdentifier: "bottomAnchor")?.constant ?? 0
        let trailingConstraintConstant = contentView.constraint(byIdentifier: "trailingAnchor")?.constant ?? 0
        let leadingConstraintConstant = contentView.constraint(byIdentifier: "leadingAnchor")?.constant ?? 0
        var size = CGSize()
        if(self.guestdata.objCompanionDetails.count > 0)
        {
        tblcomoanion.frame = CGRect(x: 0, y: 0, width: targetSize.width + trailingConstraintConstant - leadingConstraintConstant, height: 1)
        size = tblcomoanion.contentSize
        let newSize = CGSize(width: size.width, height: size.height + mainView.frame.height + 50 + bottomConstraintConstant)
            return newSize
        }
        else
        {
            let newSize = CGSize(width: mainView.frame.width, height: mainView.frame.height + 20)
            return newSize
        }
    }
}



extension GuestNameListCell : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  self.guestdata.isCompanionExpand ? self.guestdata.objCompanionDetails.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompanionListTableViewCell") as! CompanionListTableViewCell
        cell.populatedata(guest_obj: self.guestdata.objCompanionDetails[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    //    cell.layoutIfNeeded()
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
