//
//  GuestCompanionListCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 23/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit


class GuestCompanionListCell: UITableViewCell{
    
    @IBOutlet weak var guestComapnionTableView: UITableView!
    @IBOutlet weak var heightOfTableView: NSLayoutConstraint!
    var data = [Companion]()
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
        
    func populatedata(guest_obj : [Companion])
    {
        self.data = guest_obj
        self.guestComapnionTableView.reloadData()
    }
    
    
}

//extension GuestCompanionListCell
//{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//    {
//        return self.data.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//    {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "CompanionListTableViewCell") as! CompanionListTableViewCell
//        cell.populatedata(guest_obj: self.data[indexPath.row])
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return UITableView.automaticDimension
//    }
//}

//deleted delegate datasource , outlets of tableview and its height constraints
