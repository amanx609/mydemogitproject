//
//  CompanionListTableViewCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 30/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class CompanionListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var declineConfirmView: UIView!
    @IBOutlet weak var declineAcceptLAbel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func populatedata(guest_obj : Companion)
    {
        self.nameLabel.text = guest_obj.name
//        self.declineAcceptLAbel.text = "\(guest_obj.event_guest_id)"
    }
    
}
