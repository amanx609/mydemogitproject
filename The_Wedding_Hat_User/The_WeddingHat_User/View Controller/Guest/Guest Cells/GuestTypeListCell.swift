//
//  GuestTypeListCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 23/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class GuestTypeListCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var groupName: UILabel!
    @IBOutlet weak var totalNumber: UILabel!
    @IBOutlet weak var downIcon: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var guestTypeButton: UIButton!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.mainView.layer.cornerRadius = 8
        self.mainView.layer.masksToBounds = true
        self.mainView.layer.maskedCorners = [.layerMinXMinYCorner , .layerMaxXMinYCorner]
        self.bottomView.isHidden = true
    
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//        self.mainView.layer.cornerRadius = 8
//        self.mainView.layer.masksToBounds = true
//        self.mainView.layer.maskedCorners = [.layerMinXMinYCorner , .layerMaxXMinYCorner]
//        self.bottomView.isHidden = true
//    }

}
