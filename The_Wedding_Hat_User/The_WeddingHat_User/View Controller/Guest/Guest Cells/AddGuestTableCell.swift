//
//  AddGuestTableCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 22/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class AddGuestTableCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var userFirstName: UITextField!
    @IBOutlet weak var userLastName: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
