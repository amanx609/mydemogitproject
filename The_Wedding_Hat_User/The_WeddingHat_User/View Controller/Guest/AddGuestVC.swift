//
//  AddGuestVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 17/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import KRProgressHUD


class AddGuestVC: UIViewController{
    
    
    @IBOutlet weak var headView: UIView!
    @IBOutlet var footer_view: UIView!
    @IBOutlet var header_view: UIView!
    @IBOutlet weak var age_view: UIView!
    @IBOutlet weak var txtFIrstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var txtSelectGroup: UITextField!
    @IBOutlet weak var txtTableNo: UITextField!
    @IBOutlet weak var guestTable: UITableView!
    @IBOutlet weak var familyMemberLbl: UILabel!
    @IBOutlet weak var addMoreView: UIView!
    @IBOutlet weak var addMoreButton: UIButton!
    @IBOutlet weak var addCompanionButton: UIButton!
    @IBOutlet weak var addPlusicon: UIImageView!
    @IBOutlet weak var adultButton: UIButton!
    @IBOutlet weak var childButton: UIButton!
    @IBOutlet weak var selectGroupBtn: UIButton!
    @IBOutlet weak var addCompanionView: UIView!
    @IBOutlet weak var addMoreMemberView: UIView!
    @IBOutlet weak var deleteGuest: UIButton!
    @IBOutlet weak var updateGuest: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var confrmDeclineView: UIView!
    @IBOutlet weak var confrmButton : UIButton!
    @IBOutlet weak var declneButton : UIButton!
    @IBOutlet weak var updateButtonView : UIView!
    @IBOutlet weak var updateConfirmDeclineButton : UIButton!
    
    
    var guestclass = GuestListVC()
    var cellcount = 0
    var addMore = 0
    var age_group = ""
    var cellArr = [Int]()
    var ageGroupSelect = false
    var champainsList = [[String:Any]]()
    var champainsData = [[String:Any]]()
    var campaninData = [Companion]()
    var groupArr = ["Bride family" , "Groom family" , "Groom friends" , "Mutual  friends", "Groom colleagues" , "Bride colleagues"]
    let dropdown = DropDown()
    var isEdit = false
    var isPending = false
    var isResponded = false
    var guest_id = Int()
    var flag = Int()
    var editingOff = false
    var pendingOff = false
    var responseOff = false
    var id_for_Confirm = Int()
    var isButtonTap = true
    var updateStatus = Bool()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupUI()
        
    }
}

//MARK:- TableView

extension AddGuestVC: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return champainsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddGuestCell", for: indexPath) as! AddGuestCell
        cell.selectionStyle = .none
        cell.cross_btn.addTarget(self, action: #selector(crossButtonTap), for: .touchUpInside)
        cell.confirmButton.addTarget(self, action: #selector(confirmButtonTap(_:)), for: .touchUpInside)
        cell.declineButton.addTarget(self, action: #selector(declineButtonTap(_:)), for: .touchUpInside)
        cell.updateConfirmDeclineButton.addTarget(self, action: #selector(updateConfrimDeclineButtonTap(_:)), for: .touchUpInside)
        
        if self.isEdit == true
        {
            cell.firstName.text = self.champainsList[indexPath.row]["first_name"] as? String ?? ""
            cell.lastName.text = self.champainsList[indexPath.row]["last_name"] as? String ?? ""
            if (cell.cell_age_group == "")
            {
                
            }
            else
            {
                if (self.champainsList[indexPath.row]["age_group"] != nil)
                {
                    
                }
//                if self.champainsList[indexPath.row]["age_group"] as? String ?? "" == "adult"
//                {
//                cell.selctedAdult()
//                }
//                else
//                {
//                  cell.selctedChild()
//                 }
            }
            if self.editingOff == false
            {
                
                cell.firstName.isUserInteractionEnabled = false
                cell.lastName.isUserInteractionEnabled = false
                cell.adultBtn.isUserInteractionEnabled = false
                cell.childBtn.isUserInteractionEnabled = false
                cell.cross_btn.isUserInteractionEnabled = false
                cell.cross_btn.isHidden = true
                cell.confirmButton.tag = indexPath.row
                cell.declineButton.tag = indexPath.row
                if self.isResponded == true
                {
                    if(self.champainsList[indexPath.row][""] as? Int == 2)
                     {
                        self.confrmDeclineView.isHidden = true
                        self.updateButtonView.isHidden = false
                        self.UpdateConfrim()
                     }
                     else if(self.champainsList[indexPath.row]["flag"] as? Int == 1)
                     {
                        self.updateButtonView.isHidden = true
                        self.confrmDeclineView.isHidden = false
                     }
                     else
                     {
                        self.confrmDeclineView.isHidden = true
                        self.updateButtonView.isHidden = false
                        self.UpdateDecline()
                    }
                }
                else
                {
                    print("--------------")
                }
            }
            else
            {
                cell.firstName.isUserInteractionEnabled = true
                cell.lastName.isUserInteractionEnabled = true
                cell.adultBtn.isUserInteractionEnabled = true
                cell.childBtn.isUserInteractionEnabled = true
                //------------------------------------Change---------------------------
                cell.ConfirmDeclineView.isHidden = true
                cell.ConfrimDeclineViewHeightConstraint.constant = 0
            }
            
        }
        else
        {
            cell.ConfirmDeclineView.isHidden = true
            cell.ConfrimDeclineViewHeightConstraint.constant = 0
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.id_for_Confirm = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return header_view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 362
    }
    
    //MARK:- Cell cross button function
    
    @objc func crossButtonTap(_ sender : UIButton)
    {
        if champainsList.count != 0
        {
            self.champainsList.remove(at: sender.tag)
            if  champainsList.count == 0
            {
                self.addCompanionView.isHidden = false
                self.addMoreMemberView.isHidden = true
            }
            self.guestTable.reloadData()
        }
    }
    //MARK:- Cell Confirm button function
    
    @objc func confirmButtonTap(_ sender : UIButton)
    {
        if let cell = sender.superview?.superview?.superview?.superview as? AddGuestCell
        {
            if let indexpath = guestTable.indexPath(for: cell )
            {
                let row = indexpath.row
                cell.UpdateAsConfrim()
                self.HitServiceConfirm(flag: 2, updated_for: 2 , id: campaninData[row].id)
            }
        }
    }
    //MARK:- Cell Decline button function
    
    @objc func declineButtonTap(_ sender : UIButton)
    {
        if let cell = sender.superview?.superview?.superview?.superview as? AddGuestCell
        {
            if let indexpath = guestTable.indexPath(for: cell )
            {
                let row = indexpath.row
                self.hitServiceDecline(flag: 3, updated_for: 2, Id: campaninData[row].id)
                cell.UpdateAsDecline()
            }
        }
    }
    //MARK:- Cell Update for Confirm or Decline button function
    
    
    @objc func updateConfrimDeclineButtonTap(_ sender : UIButton)
    {
        if let cell = sender.superview?.superview?.superview?.superview as? AddGuestCell
        {
            if let indexpath = guestTable.indexPath(for: cell )
            {
                let row = indexpath.row
                if(cell.updateStatus == true)
                {
                    self.hitServiceDecline(flag: 3, updated_for: 2, Id: campaninData[row].id)
                    cell.UpdateAsDecline()
                    
                }
                else
                {
                    self.HitServiceConfirm(flag: 2, updated_for: 2 , id: campaninData[row].id)
                    cell.UpdateAsConfrim()
                }
            }
        }
    }
    
}
    



extension AddGuestVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if textField == txtTableNo{
            let maxLength = 3
            let currentString: NSString = txtTableNo!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return false
    }
    
}

extension AddGuestVC
{
    //MARK:- ADD Guest API
    func HitServiceAddGuest()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = ["Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Accept": "application/json"]
        let parameters = ["user_id": CommonUtilities.shared.getuserdata()?.id ?? "",
                          "event_id": UserDefaults.standard.object(forKey: "EventIDFromCreateEVent") as? String ?? "",
                          "first_name": self.txtFIrstName.text!,
                          "last_name": self.txtLastName.text!,
                          "guest_email": self.txtEmailAddress.text!,
                          "group": self.txtSelectGroup.text!,
                          "table_no": self.txtTableNo.text!,
                          "age_group": self.age_group,
                          "companions":self.champainsData ] as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "addEventGuest", parameters:parameters, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                    }
                    
                }
            }
            
        }
    }
    
    //MARK:- UPDATE Guest API
    
    func HitServiceUpdateAddGuest()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = ["Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Accept": "application/json"]
        let parameters = ["user_id": CommonUtilities.shared.getuserdata()?.id ?? "",
                          "event_id": UserDefaults.standard.object(forKey: "EventIDFromCreateEVent") as? String ?? "",
                          "first_name": self.txtFIrstName.text!,
                          "last_name": self.txtLastName.text!,
                          "guest_id" : guest_id ,
                          "guest_email": self.txtEmailAddress.text!,
                          "group": self.txtSelectGroup.text!,
                          "table_no": self.txtTableNo.text!,
                          "age_group": self.age_group,
                          "companions":self.champainsData ] as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "updateEventGuest", parameters:parameters, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        //                        self.guestclass.guestListServicehit(flag: self.flag)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                    }
                }
            }
            
        }
    }
    
      //MARK:- DELETE Guest API

    
    func HitServiceDeleteAddGuest()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = ["Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Accept": "application/json"]
        let parameters = [
            "guest_id" : guest_id
            ] as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "deleteEventGuest", parameters:parameters, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        self.guestTable.reloadData()
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                    }
                }
            }
            
        }
    }
    
 //MARK:- DECLINE Guest API
    func hitServiceDecline(flag: Int , updated_for : Int , Id: Int)
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = ["Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Accept": "application/json"]
        let parameters = [
            "id" : Id,
            "flag" : flag,
            "updateFor" : updated_for
            ] as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "changeInvitationStatus", parameters:parameters, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        self.guestTable.reloadData()
//                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                    }
                }
            }
            
            
            
        }
    }
    
    //MARK:- Confrim Guest API

    func HitServiceConfirm(flag: Int , updated_for : Int , id : Int)
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = ["Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Accept": "application/json"]
        let parameters = [
            "id" : id,
            "flag" : flag,
            "updateFor" : updated_for
            ] as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "changeInvitationStatus", parameters:parameters, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        self.guestTable.reloadData()
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                    }
                }
            }
        }
    }
}

extension AddGuestVC{
    
    func setupUI()
      {
          self.updateButtonView.isHidden = true
          self.txtTableNo.delegate = self
          self.addCompanionView.isHidden = false
          self.addMoreMemberView.isHidden = true
          self.addMoreMemberView.makeRoundCorner(20)
          self.addCompanionView.makeRoundCorner(20)
          self.addCompanionButton.makeRoundCorner(20)
          self.addCompanionButton.dropShadowBlueColor()
          self.addMoreView.makeRoundCorner(17)
          self.headView.dropShadowBlueColor()
          self.age_view.makeRoundCorner(8)
          self.age_view.layer.borderWidth = 1
          self.age_view.layer.borderColor = #colorLiteral(red: 0.5923900604, green: 0.5958476067, blue: 0.5888268948, alpha: 1)
          self.guestTable.separatorColor = .clear
          self.adultButton.addTarget(self, action: #selector(adultbtnTap(_:)), for: .touchUpInside)
          self.childButton.addTarget(self, action: #selector(childBtnTap(_:)), for: .touchUpInside)
          self.setAttributedata()
          self.buttonSetUI()
          if self.isEdit == true
          {
              self.saveButton.isHidden = true
              self.deleteGuest.isHidden = false
              self.updateGuest.isHidden = false
              if self.editingOff == false //edit is off
              {
                  self.txtFIrstName.isUserInteractionEnabled = false
                  self.txtLastName.isUserInteractionEnabled = false
                  self.txtEmailAddress.isUserInteractionEnabled = false
                  self.txtEmailAddress.isUserInteractionEnabled = false
                  self.selectGroupBtn.isUserInteractionEnabled = false
                  self.txtTableNo.isUserInteractionEnabled = false
                  self.txtSelectGroup.isUserInteractionEnabled = false
                  self.adultButton.isUserInteractionEnabled = false
                  self.childButton.isUserInteractionEnabled = false
                  self.addMoreMemberView.isHidden = true
                  self.updateGuest.isHidden = true
                  self.deleteGuest.isHidden = true
                  self.saveButton.isHidden = true
                  
                if self.isResponded == true
                {
                     if(self.flag == 2)
                     {
                        self.confrmDeclineView.isHidden = true
                        self.updateButtonView.isHidden = false
                        self.UpdateConfrim()
                     }
                     else if(self.flag == 1)
                     {
                        self.updateButtonView.isHidden = true
                        self.confrmDeclineView.isHidden = false
                     }
                     else
                     {
                        self.confrmDeclineView.isHidden = true
                        self.updateButtonView.isHidden = false
                        self.UpdateDecline()
                    }
                }
                else
                {
                    print("--------------")
                }
              }
              else //edit is on
              {
                  self.txtFIrstName.isUserInteractionEnabled = true
                  self.txtLastName.isUserInteractionEnabled = true
                  self.txtEmailAddress.isUserInteractionEnabled = true
                  self.txtEmailAddress.isUserInteractionEnabled = true
                  self.selectGroupBtn.isUserInteractionEnabled = true
                  self.txtTableNo.isUserInteractionEnabled = true
                  self.adultButton.isUserInteractionEnabled = true
                  self.childButton.isUserInteractionEnabled = true
                  self.addCompanionView.isHidden = false
                  self.addMoreMemberView.isHidden = false
                  self.addMoreButton.isUserInteractionEnabled = true
                  self.addCompanionButton.isUserInteractionEnabled = true
                  self.confrmDeclineView.isHidden = true
                  
              }
              if(campaninData.count == 0)
              {
                  if editingOff == false
                  {
                      self.addMoreMemberView.isHidden = true
                      self.addCompanionView.isHidden = true
                      self.confrmDeclineView.isHidden = false
                  }
                  else
                  {
                      self.addCompanionView.isHidden = false
                      self.addMoreMemberView.isHidden = true
                      self.confrmDeclineView.isHidden = true
                      
                  }
              }
              else
              {
                  if editingOff == false
                  {
                      self.addMoreMemberView.isHidden = true
                      self.addCompanionView.isHidden = true
                      self.confrmDeclineView.isHidden = false
                  }
                  else
                  {
                      self.addCompanionView.isHidden = true
                      self.addMoreMemberView.isHidden = false
                      self.confrmDeclineView.isHidden = true
                      
                  }
                  var dict = [String:Any]()
                  for i in campaninData
                  {
                      dict["first_name"] = i.first_name
                      dict["last_name"] = i.last_name
                      dict["age_group"] = i.age_group
                      self.champainsList.append(dict)
                  }
                  self.guestTable.reloadData()
              }
          }
          else
          {
              self.saveButton.isHidden = false
              self.deleteGuest.isHidden = true
              self.updateGuest.isHidden = true
              if(campaninData.count == 0)
              {
                  self.addCompanionView.isHidden = false
                  self.addMoreMemberView.isHidden = true
                  self.confrmDeclineView.isHidden = true
              }
              var dict = [String:Any]()
              for i in campaninData
              {
                  
                  dict["first_name"] = i.first_name
                  dict["last_name"] = i.last_name
                  dict["age_group"] = i.age_group
                  self.champainsList.append(dict)
              }
              self.guestTable.reloadData()
          }
          
      }
      
      
      func buttonSetUI()
      {
          self.deleteGuest.layer.borderWidth = 1
          self.deleteGuest.layer.borderColor = #colorLiteral(red: 0.8, green: 0.5803921569, blue: 0.5568627451, alpha: 1)
          self.updateGuest.backgroundColor = #colorLiteral(red: 0.8, green: 0.5803921569, blue: 0.5568627451, alpha: 1)
          self.confrmButton.layer.cornerRadius = 6
          self.confrmButton.layer.borderWidth = 2
          self.confrmButton.layer.borderColor = #colorLiteral(red: 0.4274509804, green: 0.8235294118, blue: 0.07843137255, alpha: 1)
          self.declneButton.layer.cornerRadius = 6
          self.declneButton.layer.borderWidth = 2
          self.declneButton.layer.borderColor = #colorLiteral(red: 0.9812672734, green: 0.4039103091, blue: 0.2982072234, alpha: 1)
      }
      
      
      func setAttributedata()
      {
          let info1 = [
              NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 14.0)! , NSAttributedString.Key.foregroundColor : UIColor(hexString: "#00375F")
          ]
          let info2 = [
              NSAttributedString.Key.font: UIFont(name: "Montserrat-Light", size: 13.0)! , NSAttributedString.Key.foregroundColor : UIColor(hexString: "#00375F")
          ]
          
          
          let Text1 = NSAttributedString(string: "Email address ", attributes: info1)
          let Text2 = NSAttributedString(string: "(optional)", attributes: info2)
          
          let regularText2 = NSAttributedString(string: "Table no. ", attributes: info1)
          let regularText3 = NSAttributedString(string: "(optional)", attributes: info2)
          
          let newString = NSMutableAttributedString()
          newString.append(Text1)
          newString.append(Text2)
          
          let newStr = NSMutableAttributedString()
          newStr.append(regularText2)
          newStr.append(regularText3)
          
          
          txtEmailAddress.attributedPlaceholder = newString
          txtTableNo.attributedPlaceholder = newStr
          txtFIrstName.placeholder = "First name"
          txtLastName.placeholder = "Last name"
          txtSelectGroup.placeholder = "Select group"
          
      }
      
      func validateGuestDetails() -> Bool
      {
          guard let firstName = txtFIrstName.text, !firstName.isEmpty  else
          {
              self.createAlert(message: "Please enter the first name")
              return false
          }
          guard let lastName = txtLastName.text, !lastName.isEmpty  else{
              self.createAlert(message: "Please enter the last name")
              return false
          }
          if self.txtEmailAddress.text != ""
          {
              guard CommonUtilities.shared.isValidEmail(testStr: txtEmailAddress.text!) else{
                  self.createAlert(message: Constant.string.validEmail.localized())
                  return false
              }
          }
          if (ageGroupSelect == false)
          {
              self.createAlert(message: "Please select the age group")
              return false
          }
          
          if(self.txtTableNo.text == "") || (self.txtTableNo.text == "0"){
              self.txtTableNo.text = ""
          }
          
          guard let groupTxt = txtSelectGroup.text , !groupTxt.isEmpty else
          {
              self.createAlert(message: "Please select the group type")
              return false
          }
          //        var dict = [String:Any]()
          //        dict["first_name"] = ""
          //        dict["last_name"] = ""
          //        dict["age_group"] = ""
          //        self.champainsList.append(dict)
          //        self.guestTable.reloadData()
          
          if self.isEdit == false
          {
              if self.champainsData.count == 0
              {
                  for i in 0..<champainsList.count
                  {
                      let indexPath = IndexPath.init(row:i, section: 0)
                      let cell = self.guestTable.cellForRow(at:indexPath) as! AddGuestCell
                      if cell.firstName.text == "" || cell.lastName.text == "" || self.age_group == ""
                      {
                          return false
                      }
                      else
                      {
                          var dict = [String: Any]()
                          dict = ["age_group" : self.age_group, "first_name": cell.firstName.text!, "last_name": cell.lastName.text!]
                          self.champainsData.append(dict)
                      }
                  }
                  print(self.champainsData)
              }
          }
          self.HitServiceAddGuest()
          return true
      }
      
      func validation() -> Bool
      {
          guard let firstName = txtFIrstName.text, !firstName.isEmpty  else
          {
              self.createAlert(message: "Please enter the first name")
              return false
          }
          guard let lastName = txtLastName.text, !lastName.isEmpty  else{
              self.createAlert(message: "Please enter the last name")
              return false
          }
          if self.txtEmailAddress.text != ""
          {
              guard CommonUtilities.shared.isValidEmail(testStr: txtEmailAddress.text!) else
              {
                  self.createAlert(message: Constant.string.validEmail.localized())
                  return false
              }
          }
          if (ageGroupSelect == false)
          {
              self.createAlert(message: "Please select the age group")
              return false
          }
          
          if(self.txtTableNo.text == ""){
              self.txtTableNo.text = "-1"
          }
          
          guard let groupTxt = txtSelectGroup.text , !groupTxt.isEmpty else
          {
              self.createAlert(message: "Please select the group type")
              return false
          }
          self.addMoreMemberView.isHidden = false
          self.addCompanionView.isHidden = true
          var dict = [String:Any]()
          dict["first_name"] = ""
          dict["last_name"] = ""
          dict["age_group"] = ""
          self.champainsList.append(dict)
          self.guestTable.reloadData()
          return true
      }
      
      @IBAction func tapOnSave(_ sender: Any)
       {
           if self.isEdit == true
           {
               self.champainsData.removeAll()
               if self.champainsData.count > 0
               {
                   for i in 0..<champainsList.count
                   {
                       let indexPath = IndexPath.init(row:i, section: 0)
                       let cell = self.guestTable.cellForRow(at:indexPath) as! AddGuestCell
                       if cell.firstName.text == "" || cell.lastName.text == "" || self.age_group == ""
                       {
                           return
                       }
                       else
                       {
                           var dict = [String: Any]()
                           dict = ["age_group" : self.age_group, "first_name": cell.firstName.text!, "last_name": cell.lastName.text!]
                           self.champainsData.append(dict)
                       }
                   }
                   print(self.champainsData)
                   self.HitServiceAddGuest()
               }
           }
           else
           {
               self.validateGuestDetails()
               print("----------------------")
           }
           
       }
       
       @IBAction func tapONBack(_ sender: Any)
       {
           self.navigationController?.popViewController(animated: true)
       }
       
       @IBAction func tapOnCompanoin(_ sender: Any)
       {
           self.validation()
           
       }
       
       @IBAction func addMoreMember(_ sender : Any)
       {
           var dict = [String:Any]()
           dict["first_name"] = ""
           dict["last_name"] = ""
           dict["age_group"] = ""
           self.champainsList.append(dict)
           self.guestTable.reloadData()
       }
       
       @IBAction func selectGroup(_ sender: Any)
       {
           dropdown.textColor = #colorLiteral(red: 0, green: 0.2837521732, blue: 0.44864434, alpha: 1)
           dropdown.dataSource = groupArr
           dropdown.anchorView = txtSelectGroup
           dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
               print("Selected item: \(item) at index: \(index)")
               self.txtSelectGroup.text = item
           }
           dropdown.animationduration = 0.20
           dropdown.show()
       }
       
       
       @IBAction func confirmButtonVC(_ sender: UIButton)
       {
           self.UpdateConfrim()
       }
       
       @IBAction func declineButtonVC(_ sender: UIButton)
       {
           self.UpdateDecline()
       }
       
       @IBAction func updateConfirmDeclineButton(_ sender : UIButton)
       {
           if self.updateStatus == true
           {
               self.UpdateDecline()
           }
           else
           {
               self.UpdateConfrim()
           }
       }
       
       func UpdateConfrim()
       {
        self.updateStatus = true
        self.HitServiceConfirm(flag: 2, updated_for: 1, id: guest_id)
//        self.updateConfirmDeclineButton.setTitle("Mark as Decline", for: .normal)
//        self.updateConfirmDeclineButton.setTitleColor(#colorLiteral(red: 0.9812672734, green: 0.4039103091, blue: 0.2982072234, alpha: 1), for: .normal)
//        self.updateConfirmDeclineButton.layer.borderWidth = 2
//        self.updateConfirmDeclineButton.layer.borderColor = #colorLiteral(red: 0.9812672734, green: 0.4039103091, blue: 0.2982072234, alpha: 1)
//        self.updateConfirmDeclineButton.layer.cornerRadius = 6
        self.confrmButton.isHidden = true
        self.declneButton.isHidden = false
//        self.confrmDeclineView.isHidden = true
//        self.updateButtonView.isHidden = false
    }
       func UpdateDecline()
       {
        self.updateStatus = false
        self.hitServiceDecline(flag: 3, updated_for: 1, Id: guest_id)
//        self.updateConfirmDeclineButton.setTitle("Mark as Confirm", for: .normal)
//        self.updateConfirmDeclineButton.setTitleColor(#colorLiteral(red: 0.4882465601, green: 0.8395010233, blue: 0.09232813865, alpha: 1), for: .normal)
//        self.updateConfirmDeclineButton.layer.borderWidth = 2
//        self.updateConfirmDeclineButton.layer.borderColor = #colorLiteral(red: 0.4882465601, green: 0.8395010233, blue: 0.09232813865, alpha: 1)
//        self.updateConfirmDeclineButton.layer.cornerRadius = 6
        self.confrmButton.isHidden = false
        self.declneButton.isHidden = true
//        self.confrmDeclineView.isHidden = true
//        self.updateButtonView.isHidden = false
    }
      @objc func adultbtnTap(_ sender : UIButton)
      {
          self.adultBtn()
      }
      
      @objc func childBtnTap(_ sender : UIButton)
      {
          self.childBtn()
      }

      @IBAction func deleteButtonTap(_ sender: UIButton){
          self.HitServiceDeleteAddGuest()
      }
      
      @IBAction func updaterBtnTap(_ sender: UIButton){
          print(champainsList)
          
          self.champainsData.removeAll()
          if self.champainsData.count == 0
          {
              for i in 0..<champainsList.count
              {
                  let indexPath = IndexPath.init(row:i, section: 0)
                  let cell = self.guestTable.cellForRow(at:indexPath) as! AddGuestCell
                  if cell.firstName.text == "" || cell.lastName.text == "" || self.age_group == ""
                  {
                      return
                  }
                  else
                  {
                      var dict = [String: Any]()
                      dict = ["age_group" : self.age_group, "first_name": cell.firstName.text!, "last_name": cell.lastName.text!]
                      self.champainsData.append(dict)
                  }
              }
              print(self.champainsData)
              self.HitServiceUpdateAddGuest()
          }
      }
    func childBtn(){
            self.ageGroupSelect = true
            self.childButton.makeRoundCorner(5)
            self.adultButton.setTitle("Adult", for: .normal)
            self.childButton.setTitle("Child", for: .normal)
            self.childButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            self.childButton.backgroundColor = #colorLiteral(red: 0, green: 0.2837521732, blue: 0.44864434, alpha: 1)
            self.adultButton.setTitleColor(#colorLiteral(red: 0, green: 0.2837521732, blue: 0.44864434, alpha: 1), for: .normal)
            self.adultButton.backgroundColor = .white
            self.age_group = "child"
        }
        func adultBtn(){
            self.ageGroupSelect = true
            self.adultButton.makeRoundCorner(5)
            self.adultButton.setTitle("Adult", for: .normal)
            self.childButton.setTitle("Child", for: .normal)
            self.adultButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            self.adultButton.backgroundColor = #colorLiteral(red: 0, green: 0.2837521732, blue: 0.44864434, alpha: 1)
            self.childButton.setTitleColor(#colorLiteral(red: 0, green: 0.2837521732, blue: 0.44864434, alpha: 1), for: .normal)
            self.childButton.backgroundColor = .white
            self.age_group = "adult"
        }
}
