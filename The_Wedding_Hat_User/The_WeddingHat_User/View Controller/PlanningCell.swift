//
//  PlanningCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 05/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class PlanningCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        self.mainView.makeRoundCorner(8)
        self.contentView.planningDropShadow()
//        self.mainView.dropShadow(shadowColor: UIColor(hexString: "#054D821A")!, borderColor: .white, opacity: 0.5, offSet: CGSize(width: 0, height: 0), radius: 5, scale: true)
        self.imgView.makeRounded()
    }
}
