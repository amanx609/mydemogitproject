//
//  RecoverLinkVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 10/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class RecoverLinkVC: UIViewController {

    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var resetMsgLbl: UILabel!
    @IBOutlet weak var finalMsgLbl: UILabel!
    var email = ""
    var emailLabel = ""
    var myrange = NSRange(location: 4, length: 7)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emailLbl.text = self.email.maskingEmail(email: email)
        self.signInBtn.makeRoundCorner(14)
        self.resetMsgLbl.text = Constant.string.resetMsg.localized()
        self.finalMsgLbl.text = Constant.string.kindlyreset.localized()
     }
    @IBAction func tapOnSignIn(_ sender: Any) {
        self.navigationController?.pop_To_ViewController(viewController: LogInVC(), animated: true)
    }
    
    @IBAction func tapOnBack(_ sender: Any) {
    
        self.navigationController?.popToRootViewController(animated: true)
    }
}
