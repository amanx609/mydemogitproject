//
//  VendorImagestblcell.swift
//  The_WeddingHat_User
//
//  Created by admin on 06/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire

class VendorImagestblcell: UITableViewCell
{
    @IBOutlet weak var collctionview_vendor : UICollectionView!
    @IBOutlet weak var pagecontrol: UIPageControl!
    
    var imagesArr = [String]()
    var dotsCount = 5
    var currentPage : Int?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        collctionview_vendor.collectionViewLayout = {
            let flowLayout = UICollectionViewFlowLayout()
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0.0
            return flowLayout
        }()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func populatedData(images_arr: [String])
    {
        self.imagesArr = images_arr
        self.collctionview_vendor.reloadData()
    }
    
}

extension VendorImagestblcell : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        if self.imagesArr.count < dotsCount
        {
          self.pagecontrol.numberOfPages = self.imagesArr.count
        }
        else
        {
             self.pagecontrol.numberOfPages = dotsCount
        }
        return self.imagesArr.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "vendorimage_collectioncell", for: indexPath) as! vendorimage_collectioncell
        
        if let img_url = URL(string:self.imagesArr[indexPath.row])
        {
            cell.imgVIew.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "Group 389"))
        }
        
        //        let main_view = cell.viewWithTag(101)!
        //        main_view.layer.cornerRadius = 8.0
        //        main_view.layer.masksToBounds = true
        //        main_view.layer.borderColor = UIColor(hexString: "#00375F")?.cgColor
        //        main_view.layer.borderWidth = 1.0
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let pageWidth = scrollView.frame.width
        self.currentPage = Int(scrollView.contentOffset.x + pageWidth / 2) / Int(scrollView.frame.width)
        self.pagecontrol.currentPage = self.currentPage! % dotsCount
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        self.pagecontrol?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
        self.pagecontrol?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}


class vendorimage_collectioncell: UICollectionViewCell
{
    @IBOutlet weak var mainVIew: UIView!
    @IBOutlet weak var imgVIew: UIImageView!

    override  func awakeFromNib()
    {
        
    }
    
    
}
