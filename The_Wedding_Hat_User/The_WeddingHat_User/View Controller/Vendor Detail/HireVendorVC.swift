//
//  HireVendorVC.swift
//  The_WeddingHat_User
//
//  Created by admin on 30/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD
import AlamofireImage


class HireVendorVC: UIViewController
{
   
//MARK:- main view
    @IBOutlet weak var btn_cancel: UIButton!
    @IBOutlet weak var btn_send: UIButton!
    @IBOutlet weak var btn_adddeal: UIButton!
    @IBOutlet weak var vw_mainview: UIView!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var categoryColl: UICollectionView!
    @IBOutlet weak var finalCategoryColl: UICollectionView!
    @IBOutlet weak var desireVenuePackage: NSLayoutConstraint!
    
    
    //deal view
    @IBOutlet weak var vw_deal: UIView!
    @IBOutlet weak var vw_heading_deal: UIView!
    @IBOutlet weak var btn_deal_cancel: UIButton!
    @IBOutlet weak var btn_deal_send: UIButton!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var tbl_deals: UITableView!
    @IBOutlet weak var savingTxt: UILabel!
    @IBOutlet weak var savingAmount: UILabel!
    
    var selectedservice_id = [String]()
    var discount_id = ""
    var percent = ""
    var delegate: HireVendorProtocol?
        
    //thrid view - final review view
    @IBOutlet weak var vw_review: UIView!
    @IBOutlet weak var totalAmount: UILabel!
    @IBOutlet weak var cross_btn: UIButton!
    @IBOutlet weak var finalAmount: UILabel!
    @IBOutlet weak var btn_review_cancel: UIButton!
    @IBOutlet weak var reviewView_revisedCost: UILabel!
    @IBOutlet weak var btn_review_send: UIButton!
    @IBOutlet weak var reviewView_finalCost: UILabel!
    @IBOutlet weak var reviewView_savinglbl: UILabel!
    @IBOutlet weak var downView: UIView!
    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var reviewView_savingAmount: UILabel!
    
    @IBOutlet weak var savingOnTWHLbl: UILabel!
    @IBOutlet weak var saving_amount: UILabel!
    
    //fourth view - popUp view
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var cancelPopUp: UIButton!
    
    
    
    var Vendor_id = ""
    var tempIndex = -1
    var discountId = ""
    var BoxClick = false
    var objdiscount = [Deal_discount]()
    var objCategoryData = [CategoryData]()
    let defaults = UserDefaults.standard
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupUI()
        HitserviecForCateory()
        self.txtAmount.delegate = self
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
////        if let detailsVC = presentingViewController as? VendorDetailVC
////        {
////            detailsVC.tbl_vendor.reloadData()
////        }
//        self.delegate?.hireVendorDismiss()
//
//    }
    
   
    
    func setupUI()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100))
        {
            self.cancelPopUp.addDashBorder(color: UIColor(hexString: "#00375F")!)
            self.btn_cancel.addDashBorder(color: UIColor(hexString: "#00375F")!)
            self.btn_adddeal.addDashBorder(color: UIColor(hexString: "#CC948E")!)
            self.vw_mainview.dropShadow()
            self.popUpView.dropShadow()
            self.btn_deal_cancel.addDashBorder(color: UIColor(hexString: "#00375F")!)
            self.vw_deal.dropShadow()
            self.vw_heading_deal.layer.borderColor = UIColor(hexString: "#00375F")?.cgColor
            self.vw_heading_deal.layer.borderWidth = 1.0
            self.btn_review_cancel.addDashBorder(color: UIColor(hexString: "#00375F")!)
            self.vw_review.dropShadow()
            self.vw_mainview.isHidden = false
            self.popUpView.isHidden = true
            self.vw_review.isHidden = true
            self.vw_deal.isHidden = true
            self.txtAmount.colorPlaceHolder(placeholder:"Amount", color: "#00375F")
            
        }
        self.finalCategoryColl.delegate = self
        self.finalCategoryColl.dataSource = self
        if  vendorFulldata.shared.deals_discount.count != 0
        {
            self.btn_adddeal.isHidden = false
            self.desireVenuePackage.constant = 103
        }
        else
        {
            self.btn_adddeal.isHidden = true
            self.desireVenuePackage.constant = 45
        }
    }
    
    @IBAction func cancel(sender : UIButton)
    {
        self.dismiss(animated: true) {}
        
    }
    
    @IBAction func tapOnsendbtn(sender : UIButton)
    {
        if (self.BoxClick) == true
        {
            if self.txtAmount.text == ""
            {
                self.createAlert(message: "Please enter the amount")
                return
            }
            else
            {
                self.HitserviecForHireVendor()
                
            }
        }
        else
        {
            self.vw_mainview.isHidden = true
            self.vw_review.isHidden = true
            self.vw_deal.isHidden = true
            self.popUpView.isHidden = false
        }
    }
    
    @IBAction func tapOndiscountRemove(_ sender: Any)
    {
        self.discountId = ""
        self.discount.text = ""
        self.percent = ""
        self.tempIndex = -1
        self.finalAmount.text = self.totalAmount.text!
        //self.savingAmount.isHidden = true
       // self.savingTxt.isHidden = true
        self.tbl_deals.reloadData()
        self.cross_btn.isHidden = true
        self.popUpView.isHidden = true
        self.vw_mainview.isHidden = false
        self.vw_review.isHidden = true
        self.vw_deal.isHidden = true
        
    }
    
    @IBAction func tapondealbtn(sender : UIButton)
    {
        if self.txtAmount.text != ""
        {
            self.popUpView.isHidden = true
            self.vw_mainview.isHidden = true
            self.vw_review.isHidden = true
            self.vw_deal.isHidden = false
        }
        else
        {
            self.createAlert(message: "Please enter the amount")
        }
        
    }
    
    
    @IBAction func tapOnpopUpCancel(_ sender: Any)
    {
        self.popUpView.isHidden = true
        self.vw_mainview.isHidden = true
        self.vw_review.isHidden = false
        self.vw_deal.isHidden = true
    }
    
    @IBAction func tapOnpopUpSend(_ sender: Any)
    {
        if self.txtAmount.text == ""
        {
            self.createAlert(message: "Please enter the amount")
            return
        }
        else
        {
            self.HitserviecForHireVendor()
            
        }
    }
    
    @IBAction func tapOnDoNotShow(_ sender: Any)
    {
        if(self.BoxClick) == false
        {
            checkBtn.setImage(#imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle"), for: .normal)
            defaults.set(true, forKey: "state")
        }
        else
        {
            checkBtn.setImage(#imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle-outline"), for: .normal)
            defaults.set(false, forKey: "state")
        }
        self.BoxClick = !self.BoxClick
    }
    
    
}





//MARK:- offer view

extension HireVendorVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return vendorFulldata.shared.deals_discount.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dealcell", for: indexPath) as! dealcell
        if vendorFulldata.shared.deals_discount[indexPath.row].type == "normal"
        {
            cell.desc_lb.text = vendorFulldata.shared.deals_discount[indexPath.row].title
//                vendorFulldata.shared.deals_discount[indexPath.row].discount+"%"+vendorFulldata.shared.deals_discount[indexPath.row].description
        }
        else
        {
            cell.desc_lb.text = vendorFulldata.shared.deals_discount[indexPath.row].title
        }
        
        if self.tempIndex == indexPath.row
        {
            cell.checkIcon.image = #imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle")
        }
        else
        {
            cell.checkIcon.image = #imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle-outline")
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if self.tempIndex == indexPath.row
        {
            self.tempIndex = -1
            self.discountId  = ""
        }
        else
        {
            self.tempIndex = indexPath.row
            self.cross_btn.isHidden = false
            self.discountId = vendorFulldata.shared.deals_discount[indexPath.row].id
            if vendorFulldata.shared.deals_discount[indexPath.row].type == "normal"
            {
                self.percent = vendorFulldata.shared.deals_discount[indexPath.row].discount
                self.discount.text = self.percent+"% discount applied"
                self.savingOnTWHLbl.isHidden = false
                self.saving_amount.isHidden = false
            }
            else
            {
                self.discount.text = vendorFulldata.shared.deals_discount[indexPath.row].title+" applied"
//                self.saving_amount.isHidden = true
//                self.savingTxt.isHidden = true
                self.reviewView_finalCost.isHidden = true
                self.savingOnTWHLbl.isHidden = true
                self.saving_amount.isHidden = true
                self.reviewView_revisedCost.isHidden = true
                self.downView.isHidden = true
                
            }
        }
        self.tbl_deals.reloadData()
    }
    
    @IBAction func taponcancelbtn_deal(sender : UIButton)
    {
        vw_mainview.isHidden = false
        vw_review.isHidden = true
        vw_deal.isHidden = true
        self.discountId = ""
        self.discount.text = ""
        self.percent = ""
        self.tempIndex = -1
        self.tbl_deals.reloadData()
        
    }
    
    @IBAction func tapondealapplybtn_deal(sender : UIButton)
    {
        vw_mainview.isHidden = true
        vw_review.isHidden = false
        vw_deal.isHidden = true
        self.saving_amount.isHidden = false
        self.savingOnTWHLbl.isHidden = false
        self.finalCategoryColl.reloadData()
        self.totalAmount.text = self.txtAmount.text!
        let text = self.txtAmount.text!.replacingOccurrences(of: ",", with: "", options: NSString.CompareOptions.literal, range:nil)
        self.discountCalculate(totalAmount: text, percent: self.percent)
        
    }
}

//MARK:- Category Collection view
extension HireVendorVC : UICollectionViewDataSource,UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == self.finalCategoryColl
        {
            return self.objCategoryData.count
        }
        else
        {
            return self.objCategoryData.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollCell", for: indexPath) as! CategoryCollCell
        if  collectionView == self.finalCategoryColl
        {
            cell.cat_lbl.text = self.objCategoryData[indexPath.row].title
            if self.selectedservice_id.contains(self.objCategoryData[indexPath.row].id)
            {
                cell.checkIcon.image = #imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle")
            }
            else
            {
                cell.checkIcon.image = #imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle-outline")
            }
        }
        else
        {
            cell.cat_lbl.text = self.objCategoryData[indexPath.row].title
            if self.selectedservice_id.contains(self.objCategoryData[indexPath.row].id)
            {
                cell.checkIcon.image = #imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle")
            }
            else
            {
                cell.checkIcon.image = #imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle-outline")
            }
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if let index = selectedservice_id.index(where: {$0 == self.objCategoryData[indexPath.row].id})
        {
            self.selectedservice_id.remove(at: index)
            print(selectedservice_id)
        }
        else
        {
            self.selectedservice_id.append(self.objCategoryData[indexPath.row].id)
            print(selectedservice_id)
        }
        self.categoryColl.reloadData()
        self.finalCategoryColl.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if  collectionView == self.finalCategoryColl
        {
            return CGSize(width: 124 , height: 46)
        }
        else
        {
            return CGSize(width: 124 , height: 46)
        }
        
    }
}


//MARK:- review view
extension HireVendorVC
{
    @IBAction func taponcancelbtn_review(sender : UIButton)
    {
        vw_mainview.isHidden = false
        vw_review.isHidden = true
        vw_deal.isHidden = true
        self.dismiss(animated: true, completion: nil)
    }
    
    func discountCalculate(totalAmount:String,percent:String)
    {

        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 0
        let total_amount = Int(totalAmount) ?? 0
        let total_discount = Int(percent) ?? 0
        let discounted_amounnt = (total_amount * total_discount)/100
        let final_amount = total_amount - discounted_amounnt
        self.saving_amount.text = "-$\(formatter.string(from: NSNumber(value: discounted_amounnt)) ?? "0")"

        self.finalAmount.text = "$\(formatter.string(from: NSNumber(value: final_amount)) ?? "0")"
    }
    
    
    
    
    func HitserviecForHireVendor()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        let headers: HTTPHeaders =
            [
                "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
                "Content-Type": "application/json"
        ]
        
        let parameters = ["amount": self.txtAmount.text!,
                          "user_id" : CommonUtilities.shared.getuserdata()!.id,
                          "vendor_id" : Vendor_id,
                          "services" : selectedservice_id,
                          "discount_id": discountId,
                          "user_event_id": EventData.shared.eventID
            ] as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "vendorHire", parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        let alert = UIAlertController(title: Constant.string.appname.localized(), message: "Your request send successfully to the vendor", preferredStyle: .alert)
                        self.present(alert, animated: true, completion: nil)
                        let when = DispatchTime.now() + 2
                        DispatchQueue.main.asyncAfter(deadline: when){
                            self.delegate?.hireVendorDismiss()
                            alert.dismiss(animated: true, completion: nil)
                            self.dismiss(animated: true, completion: nil)
                            
                        }
                        
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                        
                    }
                }
            }
            else{
                print(parameters)
            }
        }
    }
    
    func HitserviecForCateory()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        let headers: HTTPHeaders =
            [
                "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
                "Content-Type": "application/json"
        ]
        
        ServiceManager.instance.request(method: .get, URLString: "getCategoryPackage", parameters: nil, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        self.objCategoryData.removeAll()
                        if let data = dictionary?["data"] as? [[String:Any]]
                        {
                            for i in data
                            {
                                let obj = CategoryData()
                                
                                obj.id = i.valueForString(key: "id")
                                obj.title = i.valueForString(key: "title")
                                
                                self.objCategoryData.append(obj)
                            }
                            self.categoryColl.reloadData()
                        }
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                        
                    }
                }
            }
        }
    }
}


extension HireVendorVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.locale = Locale.current
        formatter.maximumFractionDigits = 0
        
        // Uses the grouping separator corresponding to your Locale
        // e.g. "," in the US, a space in France, and so on
        if let groupingSeparator = formatter.groupingSeparator {
            
            if string == groupingSeparator {
                return true
            }
            if let textWithoutGroupingSeparator = textField.text?.replacingOccurrences(of: groupingSeparator, with: "") {
                var totalTextWithoutGroupingSeparators = textWithoutGroupingSeparator + string
                
                if string.isEmpty { // pressed Backspace key
                    totalTextWithoutGroupingSeparators.removeLast()
                }
                
                if let numberWithoutGroupingSeparator = formatter.number(from: totalTextWithoutGroupingSeparators),
                    let formattedText = formatter.string(from: numberWithoutGroupingSeparator) {
                    
                    textField.text = formattedText
                    return false
                }
            }
        }
        return true
        
    }
}



class CategoryCollCell: UICollectionViewCell
{
    @IBOutlet weak var cat_lbl: UILabel!
    @IBOutlet weak var checkIcon: UIImageView!
}

class dealcell: UITableViewCell
{
    @IBOutlet weak var checkIcon: UIImageView!
    @IBOutlet weak var desc_lb: UILabel!
}


class CategoryData
{
    var id = ""
    var title = ""
}

protocol HireVendorProtocol {
    func hireVendorDismiss()
}
