//
//  Socialmediacell.swift
//  The_WeddingHat_User
//
//  Created by admin on 16/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class Socialmediacell: UITableViewCell
{
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var social_link: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    

}
