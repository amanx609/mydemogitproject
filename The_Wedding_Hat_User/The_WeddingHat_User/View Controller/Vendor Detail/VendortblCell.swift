//
//  VendorCell.swift
//  The_WeddingHat_User
//
//  Created by admin on 22/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class VendortblCell: UITableViewCell
{

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collctionview_vendor : UICollectionView!

    var dotsCount = 5
    var currentPage : Int?
    var isShow = false
    var isSimilar = false 
    var objPreferedVendorData = [ModelVendorList]()
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        collctionview_vendor.collectionViewLayout = {
            let flowLayout = UICollectionViewFlowLayout()
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0.0
            return flowLayout
        }()
//        collctionview_review.collectionViewLayout = flowLayout
//        collctionview_review.contentInsetAdjustmentBehavior = .always
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    
    func populatedata(data : [ModelVendorList])
    {
        self.objPreferedVendorData = data
        self.collctionview_vendor.reloadData()
    }
    

}

extension VendortblCell : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if (objPreferedVendorData.count < dotsCount){
            self.pageControl.numberOfPages = self.objPreferedVendorData.count
        }else{
            pageControl.numberOfPages = dotsCount
        }
        

        
        
        return self.objPreferedVendorData.count
    }
    
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Vendorcollectioncell", for: indexPath) as! Vendorcollectioncell
      
        cell.vendorName.text = self.objPreferedVendorData[indexPath.row].name
        cell.descTxt.text = self.objPreferedVendorData[indexPath.row].business_description
        
        
        if self.objPreferedVendorData[indexPath.row].categoryArr.count != 0
        {
           cell.categoryLbl.text = self.objPreferedVendorData[indexPath.row].categoryArr.joined(separator: ",")
        }
        else
        {
           cell.categoryLbl.text = ""
        }
        if self.isShow == true
        {
            cell.similarReview.isHidden = true
            cell.ratingView.isHidden = false
            cell.ratingStarImg.isHidden = false
            cell.ratingLbl.isHidden = false
            cell.submitted_lbl.isHidden = false
            cell.dateLbl.isHidden = false
            cell.review.isHidden = true
            
        }
        else
        {
            if isSimilar == true
            {
                cell.similarReview.isHidden = false
                cell.similarReview.text = self.objPreferedVendorData[indexPath.row].reviews+" Reviews"
//                cell.review.isHidden = false
                cell.ratingStarImg.isHidden = false
                cell.ratingStarImg.isHidden = false
                cell.ratingLbl.isHidden = false
                cell.ratingView.isHidden = false
                cell.review.textColor = #colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)
                cell.submitted_lbl.isHidden = true
                cell.dateLbl.isHidden = true
            }
            else
            {
                cell.similarReview.isHidden = true
                cell.review.text = self.objPreferedVendorData[indexPath.row].reviews+" Reviews"
                cell.ratingStarImg.isHidden = true
                cell.ratingStarImg.isHidden = true
                cell.ratingLbl.isHidden = true
                cell.ratingView.isHidden = true
                cell.review.textColor = #colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)
                cell.submitted_lbl.isHidden = true
                cell.dateLbl.isHidden = true
//                cell.review.isHidden = false
            }
            
            
            
        }
        
        
        if self.objPreferedVendorData[indexPath.row].categoryArr.count != 0
        {
            cell.categoryLbl.text = "\(self.objPreferedVendorData[indexPath.row].categoryArr.joined(separator: ","))"

        }
        else
        {
            cell.categoryLbl.text = ""
            //cell.review.text = objPreferedVendorData[indexPath.row].created_at
        }
         
        if let img_url = URL(string: self.objPreferedVendorData[indexPath.row].image)
        {
            cell.vendorIcon.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "HeaderRing"))
        }
        
     
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let height = collectionView.frame.height
        return CGSize(width: collectionView.frame.width, height: 220)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let pageWidth = scrollView.frame.width
        self.currentPage = Int(scrollView.contentOffset.x + pageWidth / 2) / Int(scrollView.frame.width)
        self.pageControl.currentPage = self.currentPage! % dotsCount
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        self.pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
        self.pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    func dateChange(_ date: String) -> String{
          let dateFormatter = DateFormatter()
                  dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                  let date = dateFormatter.date(from: date)
                  dateFormatter.dateFormat = "yyyy-MM-dd"
                  return  dateFormatter.string(from: date!)
    }
}


