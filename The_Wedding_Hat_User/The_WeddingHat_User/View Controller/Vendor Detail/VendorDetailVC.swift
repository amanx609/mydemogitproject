//
//  VendorDetailVC.swift
//  The_WeddingHat_User
//
//  Created by admin on 16/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import Alamofire
import KRProgressHUD


enum Vendordetail_enum :String
{
    case vendor_images
    case vendor_address
    case vendor_header
    case About
   // case Social_Links
    case Services_Offered
    case AmenitiesCapacity
    case Deals_Discounts
    case Customer_Reviews
    case Preferred_vendor_team
    case Vendor_Recommendations
    case Real_Wedding
    case Similar_vendors
    case Location
    case pricing
    case Awards
    
}

class VendorDetailVC: UIViewController,HireVendorProtocol
{
    func hireVendorDismiss() {
        self.hitserviceForGetVendorDetail()
    }
    

    
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var name_lbl: UILabel!
    @IBOutlet weak var vendor_name: UILabel!
    @IBOutlet weak var tbl_vendor: UITableView!
    var vendor_id = ""
    var vendor_Name = ""
    
    var detailpage_sequesnce = [Vendordetail_enum]()
    
    var imgLocationOnMap: UIImage?
    
    var headersarr = [String]()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupUI()
        self.vendor_name.text = vendor_Name
        self.hitserviceForGetVendorDetail()
        self.tbl_vendor.estimatedSectionHeaderHeight = 0.1
        self.tbl_vendor.reloadData()
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupUI()
        self.vendor_name.text = vendor_Name
        self.hitserviceForGetVendorDetail()
        self.tbl_vendor.estimatedSectionHeaderHeight = 0.1
        self.tbl_vendor.reloadData()

    }
    
   
    
    func setupUI()
    {
        headerView.dropShadow()
        let nibName = UINib(nibName: "VendorDetail_Reusable_Header", bundle: nil)
        self.tbl_vendor.register(nibName, forHeaderFooterViewReuseIdentifier: "VendorDetail_Reusable_Header")
    }
    
    @IBAction func taponbackbtn(sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}

extension VendorDetailVC : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return detailpage_sequesnce.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch detailpage_sequesnce[section]
        {
            //case .Social_Links:
              //  return vendorFulldata.shared.vendor_Social_links.count
            case .Deals_Discounts:
                return vendorFulldata.shared.deals_discount.count
            default:
                return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch detailpage_sequesnce[indexPath.section]
        {
        case .vendor_images:
            let cell = tableView.dequeueReusableCell(withIdentifier: "VendorImagestblcell", for: indexPath) as! VendorImagestblcell
            cell.selectionStyle = .none
            cell.populatedData(images_arr:vendorFulldata.shared.vendor_Images)
            return cell
        case .vendor_address:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Addresscell", for: indexPath) as! Addresscell
            cell.selectionStyle = .none
            return cell
        case .vendor_header:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Headercell", for: indexPath) as! Headercell
            cell.selectionStyle = .none
            cell.viewMenu = vendorFulldata.shared.vendor_menu
            cell.hireBtn.addTarget(self, action: #selector(hiredButtonTap), for: .touchUpInside)
            
           // cell.isHired = vendorFulldata.shared.is_hired
            if vendorFulldata.shared.is_hired == "1"
            {
                cell.status_lbl.text = "Hired"
                cell.hireBtn.isUserInteractionEnabled = false
                cell.hired_View.backgroundColor = #colorLiteral(red: 0.8, green: 0.5803921569, blue: 0.5568627451, alpha: 1)
             }
            else if vendorFulldata.shared.is_hired == "2"
            {
                cell.status_lbl.text = "Pending"
                cell.hireBtn.isUserInteractionEnabled = false
            }
            else
            {
                cell.status_lbl.text = "Hire Vendor"
                cell.hireBtn.isUserInteractionEnabled = true
                
            }
            cell.isDressCat = vendorFulldata.shared.is_vendor_dress
            cell.dealsanddiscount = vendorFulldata.shared.deals_discount
            cell.own_lbl.text = vendorFulldata.shared.vendor_event_on_TWH
            cell.venddorID = self.vendor_id
            return cell
        case .About:
            let cell = tableView.dequeueReusableCell(withIdentifier: "tblcellwithsinglelbl", for: indexPath) as! tblcellwithsinglelbl
            cell.selectionStyle = .none
            let lbl_desc = cell.viewWithTag(110) as! UILabel
            lbl_desc.text = vendorFulldata.shared.vendor_about
            cell.populateData(data: vendorFulldata.shared.vendor_Social_links)
            
            
            return cell
//        case .Social_Links:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "Socialmediacell", for: indexPath) as! Socialmediacell
//            cell.selectionStyle = .none
//            if vendorFulldata.shared.vendor_Social_links[indexPath.row].type == "instagram"
//            {
//                cell.icon.image = #imageLiteral(resourceName: "instagram")
//            }
//            else if vendorFulldata.shared.vendor_Social_links[indexPath.row].type == "twitter"
//            {
//                cell.icon.image = #imageLiteral(resourceName: "twitter")
//            }
//            else if vendorFulldata.shared.vendor_Social_links[indexPath.row].type == "linked"
//            {
//                cell.icon.image = #imageLiteral(resourceName: "linkedin")
//            }
//            else  if vendorFulldata.shared.vendor_Social_links[indexPath.row].type == "facebook"
//            {
//                cell.icon.image = #imageLiteral(resourceName: "facebook")
//            }
//            else
//            {
//                cell.icon.image = #imageLiteral(resourceName: "web")
//            }
//            cell.social_link.text = vendorFulldata.shared.vendor_Social_links[indexPath.row].link
//
//            return cell
        case .Services_Offered:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceOfferedCell", for: indexPath) as! ServiceOfferedCell
            cell.selectionStyle = .none
            cell.serviceOfferedLbl.text =  vendorFulldata.shared.categoryArray.joined(separator: ",")
            return cell
        case .AmenitiesCapacity:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AmenitiesTblCell", for: indexPath) as! AmenitiesTblCell
            cell.selectionStyle = .none
            return cell
        case .pricing:
            let cell = tableView.dequeueReusableCell(withIdentifier: "pdftblcell", for: indexPath) as! pdftblcell
            cell.selectionStyle = .none
            if vendorFulldata.shared.vendor_Pricing == ""
            {
                cell.pdfTxt.text = "Price list is not avilable"
            }
            else
            {
                cell.pdfTxt.text = "View Price list"
                 cell.pdfLink = vendorFulldata.shared.vendor_Pricing
            }
           
            return cell
        case .Deals_Discounts:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Dealsanddiscountcell", for: indexPath) as! Dealsanddiscountcell
            cell.selectionStyle = .none
            if vendorFulldata.shared.deals_discount[indexPath.row].type == "normal"
            {
                cell.lbl_discount_title.text = vendorFulldata.shared.deals_discount[indexPath.row].title
                cell.lbl_discount_description.text = vendorFulldata.shared.deals_discount[indexPath.row].description
                cell.img_discount.image = #imageLiteral(resourceName: "icon_discount")
            }
            else
            {
                cell.lbl_discount_title.text = vendorFulldata.shared.deals_discount[indexPath.row].title
                cell.lbl_discount_description.text = vendorFulldata.shared.deals_discount[indexPath.row].description
                cell.img_discount.image = #imageLiteral(resourceName: "complimentary")

            }
            return cell
        case .Customer_Reviews:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustmorReviewCell", for: indexPath) as! CustmorReviewCell
            cell.selectionStyle = .none
            cell.populatedData(obj_ModelVendorList :  vendorFulldata.shared.vendor_Reviews)
            return cell
        case .Preferred_vendor_team:
            let cell = tableView.dequeueReusableCell(withIdentifier: "VendortblCell", for: indexPath) as! VendortblCell
            cell.selectionStyle = .none
            cell.populatedata(data : vendorFulldata.shared.vandor_preffered)
            return cell
        case .Vendor_Recommendations:
            let cell = tableView.dequeueReusableCell(withIdentifier: "VendortblCell", for: indexPath) as! VendortblCell
            cell.selectionStyle = .none
            cell.isShow = true
            cell.populatedata(data : vendorFulldata.shared.vendor_Recommandation)
            return cell
        case .Real_Wedding:
            let cell = tableView.dequeueReusableCell(withIdentifier: "VendortblCell", for: indexPath) as! VendortblCell
            cell.selectionStyle = .none
            return cell
        case .Similar_vendors:
            let cell = tableView.dequeueReusableCell(withIdentifier: "VendortblCell", for: indexPath) as! VendortblCell
            cell.selectionStyle = .none
            cell.isSimilar = true
            cell.populatedata(data: vendorFulldata.shared.vendor_similar)
            return cell
        case .Location:
            let cell = tableView.dequeueReusableCell(withIdentifier: "locationcell", for: indexPath) as! locationcell
            cell.selectionStyle = .none
            //let imgLocationOnMap = cell.viewWithTag(101) as? UIImageView
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        //        //        header.viewAllButton.isHidden = false
        //        //        header.viewButtonView.isHidden = false
        //        switch detailpage_sequesnce[section]
        //        {
        //        case .vendor_images,.vendor_header,.vendor_address:
        //            return nil
        //        default :
        //            //        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "VendorDetail_Reusable_Header") as! VendorDetail_Reusable_Header
        //            header.lbl_headername.text = headersarr[section - 3]
        //            return header
        //        }
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "VendorDetail_Reusable_Header") as! VendorDetail_Reusable_Header
        header.viewAllButton.isHidden = true
        header.viewButtonView.isHidden = true

        
        switch detailpage_sequesnce[section] {
        case .vendor_images,.vendor_header,.vendor_address:
            return nil
        case .About:
            header.lbl_headername.text = headersarr[0]
            return header
        case .Services_Offered:
            header.lbl_headername.text = headersarr[1]
            return header
        case .pricing:
            header.lbl_headername.text = headersarr[2]
            return header
        case .Location:
            header.lbl_headername.text = headersarr[6]
            return header
        default:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "VendorDetail_Reusable_Header") as! VendorDetail_Reusable_Header
            header.lbl_headername.text = headersarr[section - 3]
            header.viewAllButton.isHidden = false
            header.viewButtonView.isHidden = false
            return header
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        switch detailpage_sequesnce[section]
        {
        case .vendor_images,.vendor_header,.vendor_address:
            return 0.1
        default :
            return 30
        }
        
    }
    
    @objc func hiredButtonTap(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HireVendorVC") as! HireVendorVC
                vc.modalPresentationStyle = .fullScreen
           vc.Vendor_id = self.vendor_id
                vc.delegate = self
        //        vc.delegate?.hireVendorDismiss()
                self.viewController?.present(vc, animated: true, completion: {})
    }
}


extension VendorDetailVC
{
    
    func hitserviceForGetVendorDetail()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        let headers: HTTPHeaders = [ "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Content-Type": "application/json"]
        
        let parameters = ["vendor_id": vendor_id,"user_id": CommonUtilities.shared.getuserdata()?.id ?? "", "event_id": EventData.shared.eventID] as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "vendorDetails", parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let vendordata = dictionary?["data"]
                        {
                            self.detailpage_sequesnce.removeAll()
                            if let vendor_images = vendordata["vendor_Images"] as? [String]
                            {
                                self.detailpage_sequesnce.append(.vendor_images)
                                vendorFulldata.shared.vendor_Images.removeAll()
                                for images in vendor_images
                                {
                                    vendorFulldata.shared.vendor_Images.append(images)
                                }
                            }
                            self.detailpage_sequesnce.append(.vendor_address)
                            self.detailpage_sequesnce.append(.vendor_header)
                            
                            if let vendor_Mobile = vendordata["vendor_Mobile"] as? String
                            {
                                vendorFulldata.shared.vendor_Mobile = vendor_Mobile
                            }
                            if let vendor_Website = vendordata["vendor_Website"] as? String
                            {
                                vendorFulldata.shared.vendor_Website = vendor_Website
                            }
                            if let is_hired = vendordata["is_hired"] as? Int
                            {
                                vendorFulldata.shared.is_hired = String(is_hired)
                            }
                            if let is_vendor_dress = vendordata["is_vendor_dress"] as? Bool
                            {
                                vendorFulldata.shared.is_vendor_dress = is_vendor_dress
                            }
                            
                            
                            if let vendor_about = vendordata["vendor_about"] as? String
                            {
                                self.detailpage_sequesnce.append(.About)
                                self.headersarr.append("About")
                                vendorFulldata.shared.vendor_about = vendor_about
                            }
                            
                            if let vendor_event_on_TWH = vendordata["vendor_event_on_TWH"] as? Int
                            {
                                vendorFulldata.shared.vendor_event_on_TWH = String(vendor_event_on_TWH)
                            }
                            if let Vendor_Links = vendordata["Vendor_Social_links"] as? [Dictionary<String,Any>]
                            {
                                if(Vendor_Links.count > 0)
                                {
                                  //  self.detailpage_sequesnce.append(.Social_Links)
                                    //self.headersarr.append("Social Links")
                                }
                                vendorFulldata.shared.vendor_Social_links.removeAll()
                                for link in Vendor_Links
                                {
                                    let obj = SocialLinks()
                                    
                                    obj.type = link.valueForString(key: "type")
                                    obj.link = link.valueForString(key: "link")
                                    
                                    vendorFulldata.shared.vendor_Social_links.append(obj)
                                }
                            }
                            if let Vendor_service_offered = vendordata["Vendor_service_offered"] as? [[String:Any]]
                            {
                                self.detailpage_sequesnce.append(.Services_Offered)
                                self.headersarr.append("Service Offered")
                                vendorFulldata.shared.categoryArray.removeAll()
                                for ser in Vendor_service_offered
                                {
                                    if let category_name = ser["category_name"] as? String
                                    {
                                        vendorFulldata.shared.categoryArray.append(category_name)
                                    }
                                }
                            }
                            if let Vendor_Amenities = vendordata["Vendor_Amenities"] as? [Dictionary<String,Any>]
                            {
                                if(Vendor_Amenities.count > 0)
                                {
                                    self.detailpage_sequesnce.append(.AmenitiesCapacity)
                                    self.headersarr.append("Amenities & Capacity")
                                }
                            }
                            if let vendor_Pricing = vendordata["vendor_Pricing"] as? String
                            {
                                self.detailpage_sequesnce.append(.pricing)
                                self.headersarr.append("Pricing")
                                vendorFulldata.shared.vendor_Pricing = vendor_Pricing
                            }
                            if let vendor_menu = vendordata["vendor_menu"] as? String
                            {
                                vendorFulldata.shared.vendor_menu = vendor_menu
                            }
                            if let Vendor_deals = vendordata["Vendor_deals"] as? [Dictionary<String,Any>]
                            {
                                if Vendor_deals.count > 0
                                {
                                    self.detailpage_sequesnce.append(.Deals_Discounts)
                                    self.headersarr.append("Deals & Discounts")
                                }
                                vendorFulldata.shared.deals_discount.removeAll()
                                for deals in Vendor_deals
                                {
                                    let obj =  Deal_discount()
                                    
                                    obj.id = deals.valueForString(key: "id")
                                    obj.type = deals.valueForString(key: "type")
                                    obj.title = deals.valueForString(key: "title")
                                    obj.discount = deals.valueForString(key: "discount")
                                    obj.description = deals.valueForString(key: "description")
                                    
                                    vendorFulldata.shared.deals_discount.append(obj)
                                }
                            }
                            if let Vendor_awards = vendordata["Vendor_awards"] as? [Dictionary<String,Any>]
                            {
                                if Vendor_awards.count > 0
                                {
                                    self.detailpage_sequesnce.append(.Awards)
                                    self.headersarr.append("Awards")
                                }
                            }
                            if let vendor_review = vendordata["vendor_review"] as? [Dictionary<String,Any>]
                            {
                                if vendor_review.count > 0
                                {
                                    self.detailpage_sequesnce.append(.Customer_Reviews)
                                    self.headersarr.append("Customer Reviews")
                                }
                                for reviews in vendor_review
                                {
                                    let obj =  ModelVendorList()
                                    
                                    obj.reviews = reviews.valueForString(key: "review")
                                    obj.name = reviews.valueForString(key: "user_name")
                                    obj.image = reviews.valueForString(key: "user_image")
                                    obj.reviewRating = reviews.valueForString(key: "rating")
                                    obj.event_date = reviews.valueForString(key: "event_date")
                                    obj.created_at = reviews.valueForString(key: "created_at")
                                    obj.event_type_name = reviews.valueForString(key: "event_type_name")
                                    vendorFulldata.shared.vendor_Reviews.append(obj)
                                }
                            }
                            if let Vendor_Recommendations = vendordata["Preferred_vendor_team"] as? [Dictionary<String,Any>]
                            {
                                if Vendor_Recommendations.count > 0
                                {
                                    self.detailpage_sequesnce.append(.Preferred_vendor_team)
                                    self.headersarr.append("Preferred Vendor Team")
                                }
                                for vendor_item in Vendor_Recommendations
                                {
                                    let preffered_vendor = ModelVendorList()
                                    if let name = vendor_item["name"] as? String
                                    {
                                        preffered_vendor.name = name
                                    }
                                    if let business_description = vendor_item["business_description"] as? String
                                    {
                                        preffered_vendor.business_description = business_description
                                    }
                                    if let reviews = vendor_item["reviews"] as? Int
                                    {
                                        preffered_vendor.reviews = String(reviews)
                                    }
                                    if let vendor_id = vendor_item["vendor_id"] as? String
                                    {
                                        preffered_vendor.id = vendor_id
                                    }
                                    if let categories = vendor_item["categories"] as? [[String:Any]]
                                    {
                                        for cat in categories
                                        {
                                            if let category_name = cat["category_name"] as? String
                                            {
                                                preffered_vendor.categoryArr.append(category_name)
                                            }
                                        }
                                    }
                                    
                                    vendorFulldata.shared.vandor_preffered.append(preffered_vendor)
                                }
                            }
                            
                            if let Vendor_Recommendations = vendordata["Vendor_Recommendations"] as? [Dictionary<String,Any>]
                            {
                                if Vendor_Recommendations.count > 0
                                {
                                    self.detailpage_sequesnce.append(.Vendor_Recommendations)
                                    self.headersarr.append("Vendor Recommendations")
                                }
                                for vendor_item in Vendor_Recommendations
                                {
                                    let preffered_vendor = ModelVendorList()
                                    if let name = vendor_item["name"] as? String
                                    {
                                        preffered_vendor.name = name
                                    }
                                    if let business_description = vendor_item["business_description"] as? String
                                    {
                                        preffered_vendor.business_description = business_description
                                    }
                                    if let rating = vendor_item["rating"] as? Double
                                    {
                                        preffered_vendor.rating = (rating)
                                        
                                    }
                                    if let vendor_id = vendor_item["vendor_id"] as? String
                                    {
                                        preffered_vendor.id = vendor_id
                                    }
                                    if let created_at = vendor_item["created_at"] as? String{
                                        preffered_vendor.created_at =  created_at
                                        
                                    }
                                    if let categories = vendor_item["categories"] as? [[String : Any]]{
                                        for cat in categories{
                                            if let category_name = cat["category_name"] as? String
                                            {
                                                preffered_vendor.categoryArr.append(category_name)
                                            }
                                        }
                                    }
                                    vendorFulldata.shared.vendor_Recommandation.append(preffered_vendor)
                                }
                            }
                            if let Real_Wedding = vendordata["Real_Wedding"] as? [Dictionary<String,Any>]
                            {
                                if Real_Wedding.count > 0
                                {
                                    self.detailpage_sequesnce.append(.Real_Wedding)
                                    self.headersarr.append("Real Wedding")
                                }
                            }
                            if let Vendor_Recommendations = vendordata["Similar_vendors"] as? [Dictionary<String,Any>]
                            {
                                if Vendor_Recommendations.count > 0
                                {
                                    self.detailpage_sequesnce.append(.Similar_vendors)
                                    self.headersarr.append("Similar Vendors")
                                }
                                
                                for vendor_item in Vendor_Recommendations
                                {
                                    let preffered_vendor = ModelVendorList()
                                    if let name = vendor_item["name"] as? String
                                    {
                                        preffered_vendor.name = name
                                    }
                                    if let business_description = vendor_item["business_description"] as? String
                                    {
                                        preffered_vendor.business_description = business_description
                                    }
                                    if let reviews = vendor_item["reviews"] as? Int
                                    {
                                        preffered_vendor.reviews = String(reviews)
                                    }
                                    if let vendor_id = vendor_item["vendor_id"] as? String
                                    {
                                        preffered_vendor.id = vendor_id
                                    }
                                    if let categories = vendor_item["categories"] as? [[String : Any]]{
                                        for cat in categories{
                                            if let category_name = cat["category_name"] as? String
                                            {
                                                preffered_vendor.categoryArr.append(category_name)
                                            }
                                        }
                                    }
                                    vendorFulldata.shared.vendor_similar.append(preffered_vendor)
                                }
                            }
                            self.detailpage_sequesnce.append(.Location)
                            self.headersarr.append("Location")
                        }
                        self.tbl_vendor.reloadData()
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                    }
                }
            }
            else
            {
                print(parameters)
            }
        }
    }
//
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
            print("locations = \(locValue.latitude) \(locValue.longitude)")
            var latStr = ""
            var longStr = ""
            latStr = String(locValue.latitude)
            longStr = String(locValue.longitude)

            let staticMapUrl = "http://maps.google.com/maps/api/staticmap?markers=color:blue|\(latStr),\(longStr)&\("zoom=10&size=400x300")&sensor=true&key=AIzaSyBXAdCe4nJuapECudMeh4q-gGlU-yAMQX0"

            print(staticMapUrl)

            let url = URL(string: staticMapUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)

            do {
                let data = try NSData(contentsOf: url!, options: NSData.ReadingOptions())
                self.imgLocationOnMap = UIImage(data: data as Data)
            } catch {
                self.imgLocationOnMap = UIImage()
            }
    }
}



class Deal_discount
{
    var id = ""
    var title = ""
    var type = ""
    var description = ""
    var discount = ""
    
}

class Awards
{
    
}

class SocialLinks
{
    var link = ""
    var type = ""
}

class RealWedding
{
    
}

class Aminities
{
    
}



class vendorFulldata
{
    static var shared = vendorFulldata()
    
    var vandor_preffered = [ModelVendorList]()
    var vendor_Recommandation = [ModelVendorList]()
    var vendor_similar = [ModelVendorList]()
    
    var deals_discount = [Deal_discount]()
    var vendor_Awards = [Awards]()
    var vendor_Real_Weddings = [RealWedding]()
    var vendor_Social_links = [SocialLinks]()
    var vendor_Aminities = [ModelVendorList]()
    var vendor_Reviews = [ModelVendorList]()
    
    var vendor_Images = [String]()
    var vendor_Mobile = ""
    var vendor_menu = ""
    var vendor_Pricing = ""
    var vendor_Website = ""
    var vendor_about = ""
    var reviews_count = ""
    var vendor_event_on_TWH = ""
    var is_hired = ""
    var is_vendor_dress = Bool()
    var categoryArray = [String]()
}


class pdftblcell :UITableViewCell
{
    @IBOutlet weak var pdfTxt: UILabel!
    @IBOutlet weak var pdfBtn: UIButton!
    var pdfLink = ""
   
    @IBAction func tapOnPdf(_ sender: Any)
    {
        if self.pdfLink != ""
        {
            let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PDFViewVC") as! PDFViewVC
            vc.pdf = pdfLink
            self.viewController?.present(vc, animated: true, completion: nil)
        }
        
    }
}

class locationcell: UITableViewCell
{
   @IBOutlet weak var underDevelopmentView: UIView!
    
    override func awakeFromNib()
    {
        self.underDevelopmentView.makeRoundCorner(8)
        self.underDevelopmentView.dropShadow()
    }
}


