//
//  Headercell.swift
//  The_WeddingHat_User
//
//  Created by admin on 16/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

//protocol hireFlow
//{
//    func reload_status()
//}

class Headercell: UITableViewCell
{
    @IBOutlet weak var hired_View: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var own_lbl: UILabel!
    @IBOutlet weak var status_lbl: UILabel!
    @IBOutlet weak var hireBtn: UIButton!
    @IBOutlet weak var dressBtn: UIButton!
    var taponHirebtn : (() -> Void)?
    var venddorID = ""
    var viewMenu = ""
    var isHired = Bool()
    var isDressCat = Bool()
    var dealsanddiscount = [Deal_discount]()
    override func awakeFromNib()
    {
        super.awakeFromNib()
        setupUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func setupUI()
    {
        headerView.dropShadow()
        
        if self.isDressCat == true
        {
            self.dressBtn.setTitle("View Dresses", for: .normal)
        }
        else
        {
            self.dressBtn.setTitle("View menu", for: .normal)
        }
    }
}

extension Headercell
{
//    @IBAction func tapOnhirebtn(sender : UIButton)
//    {
//        let vc = self.viewController?.storyboard?.instantiateViewController(withIdentifier: "HireVendorVC") as! HireVendorVC
//        vc.modalPresentationStyle = .overCurrentContext
//        vc.Vendor_id = venddorID
////        vc.delegate = self
////        vc.delegate?.hireVendorDismiss()
//        self.viewController?.present(vc, animated: true, completion: {})
//    }
    
    @IBAction func tapOnopenwebsite(sender : UIButton)
    {
        self.viewController?.createAlert(message: "Under Development")
//        guard let url = URL(string: "http://www.google.com") else {
//            return
//        }
//
//        if #available(iOS 10.0, *)
//        {
//            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//        }
//        else
//        {
//            UIApplication.shared.openURL(url)
//        }
    }
        
    @IBAction func tapOnchatbtn(sender : UIButton)
    {
        self.viewController?.createAlert(message: "Under Development")
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
//        self.viewController?.present(vc, animated: true, completion: {})
    }
        
    @IBAction func tapOnopenDress(sender : UIButton)
    {
       if (self.isDressCat == true)
        {
            let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DressVC") as! DressVC
            vc.vendorID = self.venddorID
            self.viewController?.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            if (self.viewMenu != "")
            {
                let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "PDFViewVC") as! PDFViewVC
                vc.pdf = self.viewMenu
                self.viewController?.present(vc, animated: true, completion: nil)
            }
            else
            {
                self.viewController?.createAlert(message: "Menu is not avilable")
            }
        }
    }
    
    @IBAction func tapOncallbtn(sender : UIButton)
    {
        self.viewController?.createAlert(message: "Under Development")
//        let phoneNo = "+919891332556"
//            let urlStr = "tel://\(phoneNo)"
//            print(urlStr)
//            if let phoneCallURL = URL(string: urlStr) {
//                let application:UIApplication = UIApplication.shared
//                if (application.canOpenURL(phoneCallURL)) {
//                    application.open(phoneCallURL, options: [:], completionHandler: nil)
//                }
//            }
    }
}
