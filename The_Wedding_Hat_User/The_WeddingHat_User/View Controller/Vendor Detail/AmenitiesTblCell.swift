//
//  AmenitiesTblCell.swift
//  The_WeddingHat_User
//
//  Created by admin on 16/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class AmenitiesTblCell: UITableViewCell
{
    // MARK: Properties
    @IBOutlet weak var coll_amenities: UICollectionView! {
           didSet {
               collectionViewLayout?.estimatedItemSize = CGSize(width: 1, height: 1)
               selectionStyle = .none
           }
       }

       var collectionViewLayout: UICollectionViewFlowLayout? {
           return coll_amenities.collectionViewLayout as? UICollectionViewFlowLayout
       }

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: UIView functions
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize
    {
        coll_amenities.layoutIfNeeded()
        let topConstraintConstant = contentView.constraint(byIdentifier: "topAnchor")?.constant ?? 0
        let bottomConstraintConstant = contentView.constraint(byIdentifier: "bottomAnchor")?.constant ?? 0
        let trailingConstraintConstant = contentView.constraint(byIdentifier: "trailingAnchor")?.constant ?? 0
        let leadingConstraintConstant = contentView.constraint(byIdentifier: "leadingAnchor")?.constant ?? 0
        coll_amenities.frame = CGRect(x: 0, y: 0, width: targetSize.width - trailingConstraintConstant - leadingConstraintConstant, height: 1)
        let size = coll_amenities.collectionViewLayout.collectionViewContentSize
        let newSize = CGSize(width: size.width, height: size.height + topConstraintConstant + bottomConstraintConstant)
        return newSize
    }
}

extension AmenitiesTblCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 5
    }
    
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "amenitiescollectioncell", for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cell_width = (collectionView.frame.width / 2) - 8
        return CGSize(width: cell_width, height: 31)
    }
}


extension UIView
{
    func constraint(byIdentifier identifier: String) -> NSLayoutConstraint?
    {
        return constraints.first(where: { $0.identifier == identifier })
    }
}
