//
//  Vendorcollectioncell.swift
//  The_WeddingHat_User
//
//  Created by admin on 22/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class Vendorcollectioncell: UICollectionViewCell
{
    @IBOutlet weak var outerview : UIView!
    @IBOutlet weak var vendorIcon: UIImageView!
    @IBOutlet weak var vendorName: UILabel!
    @IBOutlet weak var descTxt: UILabel!
    @IBOutlet weak var review: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var ratingStarImg: UIImageView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var submitted_lbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var similarReview: UILabel!
    override func awakeFromNib()
    {
        self.outerview.dropShadow()
        self.ratingView.layer.cornerRadius = 5.0
    }
    
}
