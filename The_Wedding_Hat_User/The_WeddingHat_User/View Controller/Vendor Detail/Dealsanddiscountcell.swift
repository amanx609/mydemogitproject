//
//  Dealsanddiscountcell.swift
//  The_WeddingHat_User
//
//  Created by admin on 17/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class Dealsanddiscountcell: UITableViewCell
{
    
    @IBOutlet weak var content_vw : UIView!
    @IBOutlet weak var lbl_discount_title : UILabel!
    @IBOutlet weak var img_discount: UIImageView!
    @IBOutlet weak var lbl_discount_description : UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        content_vw.layer.borderColor = UIColor(hexString: "#00375F")?.cgColor
        content_vw.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
