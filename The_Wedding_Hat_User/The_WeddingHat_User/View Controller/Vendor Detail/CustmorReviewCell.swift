//
//  CustmorReviewCell.swift
//  The_WeddingHat_User
//
//  Created by admin on 17/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class CustmorReviewCell: UITableViewCell
{

    @IBOutlet weak var collctionview_review : UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var dotsCount = 5
    var currentPage : Int?
    
    
    
       var objData = [ModelVendorList]()
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        collctionview_review.collectionViewLayout = {
            let flowLayout = UICollectionViewFlowLayout()
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0.0
            return flowLayout
        }()
//        collctionview_review.collectionViewLayout = flowLayout
//        collctionview_review.contentInsetAdjustmentBehavior = .always
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

    func populatedData(obj_ModelVendorList : [ModelVendorList])
       {
           self.objData = obj_ModelVendorList
           self.collctionview_review.reloadData()
       }
}

extension CustmorReviewCell : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objData.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "review_collection_cell", for: indexPath) as! Reviewcollectioncell
        
        cell.nameLbl.text = self.objData[indexPath.row].name
        cell.descLbl.text = self.objData[indexPath.row].reviews
        cell.evnetName.text = self.objData[indexPath.row].event_type_name
        cell.heldDate.text = self.objData[indexPath.row].event_date
        cell.rating.text = self.objData[indexPath.row].reviewRating
        if let img_url = URL(string:self.objData[indexPath.row].image)
        {
            cell.imagView.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "muzica_icon"))
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let pageWidth = scrollView.frame.width
        self.currentPage = Int(scrollView.contentOffset.x + pageWidth / 2) / Int(scrollView.frame.width)
        self.pageControl.currentPage = self.currentPage! % dotsCount
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        self.pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
        self.pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}

//class ZoomAndSnapFlowLayout: UICollectionViewFlowLayout
//{
//    let activeDistance: CGFloat = 200
//    let zoomFactor: CGFloat = 0.3
//
//    override init() {
//        super.init()
//
//        scrollDirection = .horizontal
//        minimumLineSpacing = 40
//        itemSize = CGSize(width: 300, height: 150)
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//    override func prepare() {
//        guard let collectionView = collectionView else { fatalError() }
//        let verticalInsets = (collectionView.frame.height - collectionView.adjustedContentInset.top - collectionView.adjustedContentInset.bottom - itemSize.height) / 2
//        let horizontalInsets = (collectionView.frame.width - collectionView.adjustedContentInset.right - collectionView.adjustedContentInset.left - itemSize.width) / 2
//        sectionInset = UIEdgeInsets(top: verticalInsets, left: horizontalInsets, bottom: verticalInsets, right: horizontalInsets)
//
//        super.prepare()
//    }
//
//    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
//        guard let collectionView = collectionView else { return nil }
//        let rectAttributes = super.layoutAttributesForElements(in: rect)!.map { $0.copy() as! UICollectionViewLayoutAttributes }
//        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.frame.size)
//
//        // Make the cells be zoomed when they reach the center of the screen
//        for attributes in rectAttributes where attributes.frame.intersects(visibleRect) {
//            let distance = visibleRect.midX - attributes.center.x
//            let normalizedDistance = distance / activeDistance
//
//            if distance.magnitude < activeDistance {
//                let zoom = 1 + zoomFactor * (1 - normalizedDistance.magnitude)
//                attributes.transform3D = CATransform3DMakeScale(zoom, zoom, 1)
//                attributes.zIndex = Int(zoom.rounded())
//            }
//        }
//
//        return rectAttributes
//    }
//
//    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
//        guard let collectionView = collectionView else { return .zero }
//
//        // Add some snapping behaviour so that the zoomed cell is always centered
//        let targetRect = CGRect(x: proposedContentOffset.x, y: 0, width: collectionView.frame.width, height: collectionView.frame.height)
//        guard let rectAttributes = super.layoutAttributesForElements(in: targetRect) else { return .zero }
//
//        var offsetAdjustment = CGFloat.greatestFiniteMagnitude
//        let horizontalCenter = proposedContentOffset.x + collectionView.frame.width / 2
//
//        for layoutAttributes in rectAttributes {
//            let itemHorizontalCenter = layoutAttributes.center.x
//            if (itemHorizontalCenter - horizontalCenter).magnitude < offsetAdjustment.magnitude {
//                offsetAdjustment = itemHorizontalCenter - horizontalCenter
//            }
//        }
//
//        return CGPoint(x: proposedContentOffset.x + offsetAdjustment, y: proposedContentOffset.y)
//    }
//
//    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
//        // Invalidate layout so that every cell get a chance to be zoomed when it reaches the center of the screen
//        return true
//    }
//
//    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
//        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
//        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
//        return context
//    }
//
//}

//class Reviewcollectioncell: UICollectionViewCell {
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//
//        contentView.backgroundColor = .green
//    }
//
//
//}
