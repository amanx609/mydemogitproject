//
//  Reviewcollectioncell.swift
//  The_WeddingHat_User
//
//  Created by admin on 17/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class Reviewcollectioncell: UICollectionViewCell
{
    @IBOutlet weak var outerview : UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imagView: UIImageView!
    @IBOutlet weak var evnetName: UILabel!
    @IBOutlet weak var reviewdOnLbl: UILabel!
    @IBOutlet weak var heldOnLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var reply_btn: UIButton!
    @IBOutlet weak var btn_report: UIButton!
    @IBOutlet weak var reviedDate: UILabel!
    @IBOutlet weak var heldDate: UILabel!
    
    override func awakeFromNib()
    {
        self.outerview.dropShadow()
    }
    
}
