//
//  PDFViewVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 06/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import WebKit

class PDFViewVC: UIViewController {
    
    @IBOutlet weak var pdfView: WKWebView!
    
    var pdf = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.pdfView.isOpaque = false
        self.pdfView.backgroundColor = UIColor.white
        
        self.openPDF()
    }
    @IBAction func tapCancel(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func tapOnCancel(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func openPDF()
    {
        if pdf != ""
        {
            let url = URL(string: pdf)!
            pdfView.load(URLRequest(url: url))
            pdfView.allowsBackForwardNavigationGestures = false
        }
    }
}
