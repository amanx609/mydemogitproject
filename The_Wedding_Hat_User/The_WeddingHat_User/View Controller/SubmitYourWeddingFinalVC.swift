//
//  SubmitYourWeddingFinalVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 13/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class SubmitYourWeddingFinalVC: UIViewController,AllVendorYes,imagesArr
{
    
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var submitReviewBtn: UIButton!
    
    var vendor_hired = [vendorList]()
    var isSelectedYesorNo = Bool()
    var documentsPicArray = [UIImage]()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tblView.delegate = self
        tblView.dataSource = self
        self.uiDesign()
        
    }
    
    
    @IBAction func backBtnTap(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func isAnswer(isanswerKey:Bool)
    {
        self.isSelectedYesorNo = isanswerKey
        self.tblView.reloadData()
    }
   
    func imagesTransfer(imagesArr:[UIImage])
    {
        self.documentsPicArray = imagesArr
        self.tblView.reloadData()
    }
    
}

//MARK:- Collection Views
extension SubmitYourWeddingFinalVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadImageCollectionCell", for: indexPath) as! UploadImageCollectionCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 150 , height: 100)
    }
}

//MARK:- TableView

extension SubmitYourWeddingFinalVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return vendor_hired.count + 3
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubmitYourWeddingCell", for: indexPath) as! SubmitYourWeddingCell
            cell.delegate = self
            return cell
        }
        else if(indexPath.row == (tableView.numberOfRows(inSection: 0) - 2))
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UploadYourPhotosCell", for: indexPath) as! UploadYourPhotosCell
            cell.delegate = self
            return cell
        }
        else if(indexPath.row == (tableView.numberOfRows(inSection: 0) - 1))
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImagesTableCell", for: indexPath) as! ImagesTableCell
            cell.documentsPicArray = self.documentsPicArray
            cell.imageCollectionView.reloadData()
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VendorOptionCell", for: indexPath) as! VendorOptionCell
            cell.vendorNameLbl.text = self.vendor_hired[indexPath.row - 1].name
            return cell
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.isSelectedYesorNo == true
        {
            if(indexPath.row == 0)
            {
                return UITableView.automaticDimension
            }
            else if(indexPath.row == (tableView.numberOfRows(inSection: 0) - 2))
            {
                return UITableView.automaticDimension
            }
            else if(indexPath.row == (tableView.numberOfRows(inSection: 0) - 1))
            {
                return UITableView.automaticDimension
            }
            else
            {
                return 0.0
            }
        }
        else
        {
            return UITableView.automaticDimension
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 80
    }
    
}


extension SubmitYourWeddingFinalVC
{
    func uiDesign()
    {
        self.submitReviewBtn.makeRoundCorner(10.0)
        self.headView.dropShadow()
    }
}


