//
//  SubmitYourWeddingVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 13/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRActivityIndicatorView

class SubmitYourWeddingVC: UIViewController {
    
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var yourNameTextField: UITextField!
    @IBOutlet weak var yourPartnerTextField: UITextField!
    @IBOutlet weak var weddingDateTextField: UITextField!
    @IBOutlet weak var selectWeddingSeasonTxt: UITextField!
    @IBOutlet weak var selectStyleTxt: UITextField!
    
    @IBOutlet weak var selectColorThemeTxt: UITextField!
    @IBOutlet weak var selectCountryTxt: UITextField!
    @IBOutlet weak var selectCityTxt: UITextField!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    
    var countryId = ""
    var cityID = ""
    
    var getJourneyObj = AddJourneyDataModel()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.designUi()
        self.setPicker()
        self.getAddJourneyServiceHit()
        self.selectStyleTxt.delegate = self
        self.selectColorThemeTxt.delegate = self
        self.selectWeddingSeasonTxt.delegate = self
        self.selectCountryTxt.delegate = self
        self.selectCityTxt.delegate = self
        
        
    }
    
    
    
}



extension SubmitYourWeddingVC: UITextFieldDelegate
{
    func designUi()
    {
        headView.dropShadow()
        self.nextButton.makeRoundCorner(10.0)
        self.selectCountryTxt.addRightImageAsRightView(strImgName: "down-icon", rightPadding: 10)
        self.selectCityTxt.addRightImageAsRightView(strImgName: "down-icon", rightPadding: 10)
        self.selectStyleTxt.addRightImageAsRightView(strImgName: "down-icon", rightPadding: 10)
        self.selectColorThemeTxt.addRightImageAsRightView(strImgName: "down-icon", rightPadding: 10)
        self.selectWeddingSeasonTxt.addRightImageAsRightView(strImgName: "down-icon", rightPadding: 10)
    }
    
    func setPicker()
    {
        self.selectStyleTxt.setPickerData(arrPickerData:getJourneyObj.style_list, selectedPickerDataHandler: { (text, row, component) in
        }, defaultPlaceholder: nil)
        
        self.selectWeddingSeasonTxt.setPickerData(arrPickerData: getJourneyObj.season_list, selectedPickerDataHandler: { (text, row, component) in
            self.selectWeddingSeasonTxt.text = self.getJourneyObj.season_list[row]
        }, defaultPlaceholder: nil)
        
        self.selectColorThemeTxt.setPickerData(arrPickerData: getJourneyObj.theme_list, selectedPickerDataHandler: { (text, row, component) in
            self.selectColorThemeTxt.text = self.getJourneyObj.theme_list[row]
        }, defaultPlaceholder: nil)
        
        self.selectCountryTxt.setPickerData(arrPickerData: GlobalData.shared.countryArray.map({$0.country_name as String}), selectedPickerDataHandler: { (text, row, component) in
            var countryRow = Int()
            self.selectCountryTxt.text = GlobalData.shared.countryArray[row].country_name
            self.selectCityTxt.text = ""
            self.countryId = String(GlobalData.shared.countryArray[row].id)
            countryRow = row
            self.selectCityTxt.setPickerData(arrPickerData:
                (GlobalData.shared.countryArray[countryRow].cities.map({$0.city_name as String})), selectedPickerDataHandler: { (text, row, component) in
                    self.cityID = GlobalData.shared.countryArray[countryRow].cities[row].id
            }, defaultPlaceholder: nil)
        }, defaultPlaceholder: nil)
    }
    
    
    
    @IBAction func nextBtnTap(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "IdeasAndInspire", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SubmitYourWeddingFinalVC") as! SubmitYourWeddingFinalVC
        vc.vendor_hired = self.getJourneyObj.vendor_hired
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
//MARK:- API Services

extension SubmitYourWeddingVC{
    
    func getAddJourneyServiceHit(){
        
        let headers: HTTPHeaders = [ "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Content-Type": "application/json"]
        ServiceManager.instance.request(method: .get, URLString: "getAddJourney?user_id="+CommonUtilities.shared.getuserdata()!.id, parameters: nil, encoding: JSONEncoding.default, headers: headers) { (sucess, response, error) in
            if(sucess)!{
                if let statuscode = response?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = response?["data"] as? [String:Any]
                        {
                            let obj = AddJourneyDataModel()
                            if let season_list = data["season_list"] as? [String]
                            {
                                for season in season_list
                                {
                                    obj.season_list.append(season)
                                }
                            }
                            if let style_list = data["style_list"] as? [String]
                            {
                                for style in style_list
                                {
                                    obj.style_list.append(style)
                                }
                            }
                            if let theme_list = data["theme_list"] as? [String]
                            {
                                for theme in theme_list
                                {
                                    obj.theme_list.append(theme)
                                }
                            }
                            if let vendor_hired = data["vendor_hired"] as? [[String:Any]]
                            {
                                obj.vendor_hired.removeAll()
                                for vendor in vendor_hired
                                {
                                    let vendroList = vendorList()
                                    if let id = vendor["id"] as? Int
                                    {
                                        vendroList.id = String(id)
                                    }
                                    if let name = vendor["name"] as? String
                                    {
                                        vendroList.name = name
                                    }
                                    obj.vendor_hired.append(vendroList)
                                }
                            }
                            self.getJourneyObj = obj
                            self.setPicker()
                        }
                    }
                }
            }
        }
    }
}



class AddJourneyDataModel
{
    var season_list = [String]()
    var style_list = [String]()
    var vendor_hired = [vendorList]()
    var theme_list = [String]()
    
}

class vendorList
{
    var id = ""
    var name = ""
}
