//
//  FeaturedCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 04/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class FeaturedCell: UICollectionViewCell {
    
    @IBOutlet weak var superMainView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var vendorImg: UIImageView!
    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var featuredLbl: UILabel!
    @IBOutlet weak var featuredView: UIView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var reviewView: UIView!
    @IBOutlet weak var starIcon: UIImageView!
    override func awakeFromNib() {
        setupUI()
    }
    
}

extension FeaturedCell{
    func setupUI(){
        self.superMainView.makeRoundCorner(10)
        self.superMainView.dropShadow()
        
        self.featuredView.makeRoundCorner(8.0)
        self.reviewView.makeRoundCorner(5)
        self.ratingView.makeRoundCorner(5)
    }
}
extension FeaturedCell{
    @IBAction func viewMoreButtonTap(_ sender: UIButton) {
        
    }
}

