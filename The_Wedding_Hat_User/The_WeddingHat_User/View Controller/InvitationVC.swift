//
//  InvitationVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 23/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

class InvitationVC: UIViewController {

    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var main_view: UIView!
    @IBOutlet weak var img_view: UIImageView!
    @IBOutlet weak var welcome_lbl: UILabel!
    @IBOutlet weak var brideName_lbl: UILabel!
    @IBOutlet weak var dot_lbl: UILabel!
    @IBOutlet weak var groom_lbl: UILabel!
    @IBOutlet weak var date_lbl: UILabel!
    @IBOutlet weak var time_lbl: UILabel!
    @IBOutlet weak var locationHeading_lbl: UILabel!
    @IBOutlet weak var desc_lbl: UILabel!
    @IBOutlet weak var send_btn: UIButton!
    var DictParams = [String:Any]()
    var selectedImage : UIImage?
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setUI()
    }
    
    @IBAction func tapOnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnSend(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "IdeasAndInspire", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "InvitationListVC") as! InvitationListVC
        vc.invitationParams = self.DictParams
        vc.image = self.selectedImage
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setUI()
    {
        self.dot_lbl.makeRounded()
        self.header_view.dropShadow()
        self.main_view.makeRoundCorner(8)
        self.main_view.dropShadow()
        self.img_view.makeRoundCorner(8)
        self.send_btn.makeRoundCorner(8)
        
        self.brideName_lbl.text = (DictParams["bride_name"] as? String)?.uppercased()
        self.groom_lbl.text = (DictParams["groom_name"] as? String)?.uppercased()
        self.date_lbl.text = DictParams["event_date"] as? String
        self.time_lbl.text = DictParams["event_time"] as? String
        self.locationHeading_lbl.text = DictParams["event_location"] as? String
        self.desc_lbl.text = DictParams["invitation_message"] as? String
        self.welcome_lbl.text = DictParams["invitation_title"] as? String
        self.img_view.contentMode = .scaleToFill
        self.img_view.image = selectedImage
       
    }
}



