//
//  AnimationVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 15/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Localize_Swift

class AnimationVC: UIViewController {
    
    @IBOutlet weak var splashImg: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    let preference = UserDefaults.standard
    override func viewDidLoad()
    {
        super.viewDidLoad()
        lbl_title.alpha = 0.0
        self.splashImg.loadGif(name: "the-weddinghat-logo")
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute:
            {
                self.splashImg.image = UIImage(cgImage: UIImage.getlastframe(name: "the-weddinghat-logo"))
                UIView.animate(withDuration: 2.0, delay: 1.0, options: .curveEaseInOut, animations: {
                    
                    self.lbl_title.alpha = 1.0
                    
                }) { (status) in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute:
                        {
                            if(CommonUtilities.shared.getselectedLanguage() == 0)
                            {
                                let lanarr = ["en","ar"]
                                if let languages = UserDefaults.standard.object(forKey: "AppleLanguages") as? [String],let _ = languages.first(where:{!$0.hasPrefix("mn")})
                                {
                                    UserDefaults.standard.set(lanarr, forKey: "AppleLanguages")
                                }
                                UserDefaults.standard.set(1, forKey: "selectedcountryLanguage")
                                Localize.setCurrentLanguage("en")
                            }
                            if CommonUtilities.shared.getuserdata()?.id == "" || CommonUtilities.shared.getuserdata()?.id == nil
                            {
                                let logout = CommonUtilities.shared.preferences.object(forKey: "Logout")
                                if((logout) != nil)
                                {
                                    CommonUtilities.shared.setLoginScreen()
                                }
                                else
                                {
//                                    CommonUtilities.shared.setLoginScreen()

                                    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "OnBoardingScreen") as! OnBoardingScreen
                                    let navObj = UINavigationController(rootViewController: vc)
                                    navObj.isNavigationBarHidden = true
                                    UIApplication.shared.windows.first?.rootViewController = navObj
                                }
                                
                                
                                //                                    if self.isAppAlreadyLaunchedOnce() == false
                                //                                    {
                                //                                        let checkTime = self.preference.object(forKey: "Demo") as! String
                                //                                        if checkTime == "First"
                                //                                        {
                                //                                            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                                //                                            let vc = storyboard.instantiateViewController(withIdentifier: "OnBoardingScreen") as! OnBoardingScreen
                                //                                            let navObj = UINavigationController(rootViewController: vc)
                                //                                            navObj.isNavigationBarHidden = true
                                //                                            UIApplication.shared.windows.first?.rootViewController = navObj
                                //                                        }
                                //
                                //                                    }
                                                                
                            }
                            else
                            {
                                CommonUtilities.shared.sidemenu()
                            }
                            
                    })
                }
        })
    }
    
    func isAppAlreadyLaunchedOnce()->Bool
    {
        let defaults = UserDefaults.standard
        if let _ = defaults.string(forKey: "isAppAlreadyLaunchedOnce")
        {
            print("App already launched")
            preference.set("Done", forKey: "Demo")
            CommonUtilities.shared.setupinitialView()
            return true
        }
        else
        {
            defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
            print("App launched first time")
            preference.set("First", forKey: "Demo")
            return false
        }
    }
    
//    func isLogout() -> Bool
//    {
//        let defaults = UserDefaults.standard
//        if let _ = defaults.string(forKey: "isLogout")
//        {
//            preference.set("Done", forKey: "Logout")
//            CommonUtilities.shared.setLoginScreen()
//            return true
//        }
//        else
//        {
//            defaults.set(true, forKey: "isLogout")
//            preference.set("New", forKey: "Logout")
//            return false
//        }
//    }
    
}
