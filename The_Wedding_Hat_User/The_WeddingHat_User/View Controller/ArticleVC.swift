//
//  ArticleVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 26/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

class ArticleVC: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var articleTable: UITableView!
    
    var objArticelModel = [ArticelModel]()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.headerView.dropShadow()
        self.HitServiceget_Articles()
        self.articleTable.separatorColor = .clear
    }
    
    @IBAction func tapOnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ArticleVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.objArticelModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Article_Cell", for: indexPath) as! Article_Cell
        cell.selectionStyle = .none
        cell.desc_lbl.text = self.objArticelModel[indexPath.row].title
        if let img_url = URL(string: self.objArticelModel[indexPath.row].image)
        {
            cell.imgView.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "ss"))
        }
//        if self.objArticelModel[indexPath.row].post_date != ""
//        {
//            let dateString = self.objArticelModel[indexPath.row].post_date
//            cell.posted_lbl.text = self.convertDateFormater(dateString)
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PDFViewVC") as! PDFViewVC
        vc.pdf = self.objArticelModel[indexPath.row].postLink
        self.viewController?.present(vc, animated: true, completion: nil)
    }
    
    
}

extension ArticleVC
{
    func HitServiceget_Articles()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        
        let headers: HTTPHeaders = [ "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Content-Type": "application/json"]
        ServiceManager.instance.request(method: .get, URLString: "getArticles", parameters: nil, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!{
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = dictionary?["data"] as? [String:Any]
                        {
                            if let article = data["article"] as? [[String:Any]]
                            {
                                for articleList in article
                                {
                                    let obj = ArticelModel()
                                    
                                    if let author_name = articleList["author_name"] as? String
                                    {
                                        obj.author_name = author_name
                                    }
                                    if let postLink = articleList["postLink"] as? String
                                    {
                                        obj.postLink = postLink
                                    }
                                    if let post_date = articleList["post_date"] as? String
                                    {
                                        obj.post_date = post_date
                                    }
                                    if let image = articleList["image"] as? String
                                    {
                                        obj.image = image
                                    }
                                    if let title = articleList["title"] as? [String:Any]
                                    {
                                        if let rendered = title["rendered"] as? String
                                        {
                                            obj.title = rendered
                                        }
                                    }
                                    self.objArticelModel.append(obj)
                                }
                                self.articleTable.reloadData()
                            }
                            
                        }
                        
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                    }
                }
            }
        }
    }
}

class ArticelModel
{
    var author_name = ""
    var postLink = ""
    var post_date = ""
    var title = ""
    var image = ""
}
