//
//  IdeasandInspirationVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 26/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

class IdeasandInspirationVC: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var ideasTable: UITableView!
   
    var menuArray = [["image":"article",
                      "text":"Article"],
                     ["image":"Real wedding",
                      "text":"Real wedding"],
                     ["image":"poll",
                      "text":"Poll"]]
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.headerView.dropShadow()
        self.ideasTable.separatorColor = .clear
    }

}

extension IdeasandInspirationVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IdeasInspiritionCell", for: indexPath) as! IdeasInspiritionCell
        cell.selectionStyle = .none
        cell.name_lbl.text = menuArray[indexPath.row]["text"]
        cell.img_view.image =  UIImage(named: menuArray[indexPath.row]["image"]!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (indexPath.row == 0)
        {
            let storyboard = UIStoryboard(name: "IdeasAndInspire", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ArticleVC") as! ArticleVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if (indexPath.row == 1)
        {
            let storyboard = UIStoryboard(name: "IdeasAndInspire", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RealWeddingVC") as! RealWeddingVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let storyboard = UIStoryboard(name: "IdeasAndInspire", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PollVC") as! PollVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
}
}
