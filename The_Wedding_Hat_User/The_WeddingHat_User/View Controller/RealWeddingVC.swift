//
//  RealWeddingVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 26/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

class RealWeddingVC: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var submit_btn: UIButton!
    @IBOutlet weak var realWeddingTable: UITableView!
    @IBOutlet weak var tbl_constant: NSLayoutConstraint!
    @IBOutlet weak var submit_constant: NSLayoutConstraint!
    
    var realWeddingObj = [RealWeddingList]()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.headerView.dropShadow()
        self.hitServiceRealWeaddingList()
        self.submit_btn.makeRoundCorner(8)
        self.submit_constant.constant = 0.0
        self.tbl_constant.constant = 12.0
        self.realWeddingTable.separatorColor = .clear
    }
    
    @IBAction func taponback(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
  
    @IBAction func tapOnsubmit(_ sender: Any)
    {
        self.submit_constant.constant = 0.0
        self.tbl_constant.constant = 12.0
    }
}


extension RealWeddingVC: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.realWeddingObj.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RealWeddingCell", for: indexPath)as! RealWeddingCell
        cell.selectionStyle = .none
        cell.name_lbl.text = self.realWeddingObj[indexPath.row].name+" & "+self.realWeddingObj[indexPath.row].partner_name
        cell.wedding_lbl.text = self.realWeddingObj[indexPath.row].description
        cell.loc_lbl.text = self.realWeddingObj[indexPath.row].city+", "+self.realWeddingObj[indexPath.row].country
        if self.realWeddingObj[indexPath.row].images.count != 0
        {
            if let img_url = URL(string: self.realWeddingObj[indexPath.row].images[0])
            {
                cell.imgView.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "ss"))
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
}

extension RealWeddingVC
{
    func hitServiceRealWeaddingList()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        
        let headers: HTTPHeaders = [ "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Content-Type": "application/json"]
        ServiceManager.instance.request(method: .get, URLString: "realwedding/list?user_id="+CommonUtilities.shared.getuserdata()!.id, parameters: nil, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!{
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = dictionary?["data"] as? [[String:Any]]
                        {
                            print(data)
                            for list in data{
                                let obj = RealWeddingList()
                                
                                if let name = list["name"] as? String{
                                    obj.name = name
                                }
                                if let partner_name = list["partner_name"] as? String
                                {
                                    obj.partner_name = partner_name
                                }
                                if let country = list["country"] as? String
                                {
                                    obj.country = country
                                }
                                if let city = list["city"] as? String
                                {
                                    obj.city = city
                                }
                                if let description = list["description"] as? String
                                {
                                    obj.description = description
                                }
                                if let wedding_date = list["wedding_date"] as? String
                                {
                                    obj.wedding_date = wedding_date
                                }
                                if let city = list["city"] as? String
                                {
                                    obj.city = city
                                }
                                if let images = list["images"] as? [String]
                                {
                                    obj.images = images
                                }
                                self.realWeddingObj.append(obj)
                            }
                            self.realWeddingTable.reloadData()
                        }
                        
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                    }
                }
            }
        }
    }
}
