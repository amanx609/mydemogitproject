//
//  ForgotPasswordVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 10/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD
import AlamofireImage

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var forgotMsg: UILabel!
    @IBOutlet weak var sendLink: UIButton!
    @IBOutlet weak var emailTxt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sendLink.makeRoundCorner(10)
        self.emailTxt.colorPlaceHolder(placeholder:Constant.string.email.localized(), color: "#00375F")
    }
    @IBAction func tapOnSend(_ sender: Any) {
        self.validation()
    }
    
    
    
    func validation() -> Bool{
        guard let email = emailTxt.text, !email.isEmpty  else{
            self.createAlert(message: Constant.string.Empty_email.localized())
            return false
        }
        guard CommonUtilities.shared.isValidEmail(testStr: emailTxt.text!) else{
            self.createAlert(message: Constant.string.validEmail.localized())
            return false
        }
        self.hitServiceForgotPassword()
        return true
    }
    @IBAction func tapOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ForgotPasswordVC{
    func hitServiceForgotPassword(){
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let parameters = [
            "email" : self.emailTxt.text!
        ]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "forgotPassword", parameters: parameters as? [String : Any], encoding: JSONEncoding.default, headers: nil) { (sucess, dictionary, error) in
            if (sucess)!{
                KRProgressHUD.dismiss()
                if let statuscode = dictionary!["status"] as? NSNumber{
                    let message = dictionary!["message"] as? String
                    if statuscode == 200{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RecoverLinkVC") as! RecoverLinkVC
                        vc.email = self.emailTxt.text!
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else{
                        self.createAlert(message: message!)
                    }
                }
            }
        }
    }
}


// RecoverLinkVC.instantiate(fromAppStoryboard: .main)

//Use multiple storyboard
