//
//  LogInVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 29/05/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKLoginKit
import KRProgressHUD
import AlamofireImage

class LogInVC: UIViewController {
    
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var eyeBtn: UIButton!
    @IBOutlet weak var fbsignInButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var fbButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var haveAccount: UILabel!
    @IBOutlet weak var signInWithLabel: UILabel!
    @IBOutlet weak var signUpView: NSLayoutConstraint!
    var urlProfileImg = ""
    var iconClick = true
    var isTap = true
    var getImgData: UIImage?
    var socailLogin = [String:Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpDesigns()
    }
    func setUpDesigns(){
        if(UIScreen.main.bounds.width > 375){
            self.signUpView.constant = 85
        }else{
            self.signUpView.constant = 65
        }
        self.signInButton.makeRoundCorner(14)
        self.fbsignInButton.makeRoundCorner(14)
        self.emailTextField.colorPlaceHolder(placeholder:Constant.string.email.localized(),color:"#00375F")
        self.passwordTextField.colorPlaceHolder(placeholder:Constant.string.password.localized(),color:"#00375F")
        self.forgotPasswordBtn.setTitle(Constant.string.forgotPassword.localized(), for: .normal)
        self.signInButton.setTitle(Constant.string.signIn.localized(), for: .normal)
        self.fbsignInButton.setTitle(Constant.string.signInWith.localized(), for: .normal)
        self.signUpButton.setTitle(Constant.string.signUp.localized(), for: .normal)
        self.signInWithLabel.text = Constant.string.orWith.localized()
    }
    func validation() -> Bool
    {
        guard let email = emailTextField.text, !email.isEmpty  else{
            self.createAlert(message: Constant.string.Empty_email.localized())
            return false
        }
        guard CommonUtilities.shared.isValidEmail(testStr: emailTextField.text!) else{
            self.createAlert(message: Constant.string.validEmail.localized())
            return false
        }
        guard let password = passwordTextField.text, !password.isEmpty else{
            self.createAlert(message: Constant.string.enterPassword.localized())
            return false
        }
//        guard  let checkCount = passwordTextField.text?.count , checkCount >= 8 else {
//            self.createAlert(message: Constant.string.passwordLessThanEight.localized())
//            return false
//        }
        self.hitServiceForLogin()
        return true
    }
}

extension LogInVC{
    
    @IBAction func tapOnEye(_ sender: Any)
    {
        if(self.iconClick == true) {
            self.passwordTextField.isSecureTextEntry  = false
            self.eyeBtn.setImage(#imageLiteral(resourceName: "blue eye"), for: .normal)
        } else {
            self.passwordTextField.isSecureTextEntry = true
            self.eyeBtn.setImage(#imageLiteral(resourceName: "white eye"), for: .normal)
        }
        self.iconClick = !self.iconClick
    }
    @IBAction func forgotPasswordButtonTap(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signInButtonTap(_ sender: UIButton) {
        self.validation()
    }
    @IBAction func FbSignInButton(_ sender: UIButton) {
        if AccessToken.current != nil
        {
            getFBUserData()
            //            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController")
            //            self.navigationController?.pushViewController(vc!, animated: true)
            
        } else {
            let fbLoginManager : LoginManager = LoginManager()
            fbLoginManager.logIn(permissions:["email"], from: self) { (result, error) -> Void in
                if (error == nil)
                {
                    
                    let fbloginresult : LoginManagerLoginResult = result!
                    if (result?.isCancelled)! {
                        return
                    }
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        //                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController")
                        //                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                }
            }
        }
    }
    @IBAction func signUpButtonTap(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpPageVC")
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}

extension LogInVC{
    func getFBUserData()
    {
        if(AccessToken.current != nil) {
            KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
            KRProgressHUD.show()
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: {
                (connection, result, error) -> Void in
                KRProgressHUD.dismiss()
                if (error == nil)
                {
                    let dict = result as! [String : AnyObject]
                    self.socailLogin = dict
                    let fbID = dict["id"] as? String
                    let firstname = dict["first_name"] as? String ?? ""
                    let lastname = dict["last_name"] as? String ?? ""
                    let emailstr = dict["email"] as? String ?? ""
                    if let picture = dict["picture"] as? Dictionary<String, AnyObject> {
                        if let data = picture["data"] as? Dictionary<String, AnyObject> {
                            if let http = data["url"] as? String {
                                self.urlProfileImg = http
                                Alamofire.request(self.urlProfileImg, method: .get).responseImage { response in
                                    if let data = response.result.value {
                                        // handler(data)
                                        self.getImgData = data
                                    } else {
                                        // handler(nil)
                                    }
                                }
                            }
                            SocialLogin.shared.loginvia = true
                            self.hitServiceForFBLogin(name: firstname+" "+lastname, email: emailstr, socialID: fbID!)
                        }
                        
                    }
                    
                }
            })
        }
    }
}

extension LogInVC
{
    func hitServiceForLogin(){
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let parameters =
            [
                "email": self.emailTextField.text!,
                "password":self.passwordTextField.text!
                ]as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString:"login", parameters: parameters as [String : AnyObject] , encoding: JSONEncoding.default, headers: nil)
        { (success, dictionary, error) in
            print(dictionary ?? "")
            KRProgressHUD.dismiss()
            if let statuscode = dictionary?["status"] as? NSNumber
            {
                let msg = dictionary?["message"] as! String
                var userInfoObj = Userinfo()
                if let dict = dictionary?["data"] as? [String:Any]
                {
                    if statuscode == 200
                    {
                        if let active = dict["active"] as? Int
                        {
                            userInfoObj.active = String(active)
                        }
                        if let access_token = dict["access_token"] as? String
                        {
                            userInfoObj.access_token = access_token
                        }
                        if let email = dict["email"] as? String
                        {
                            userInfoObj.email = email
                        }
                        if let id = dict["id"] as? Int
                        {
                            userInfoObj.id = String(id)
                        }
                        if let name = dict["name"] as? String
                        {
                            userInfoObj.name = name
                        }
                        if let social_id = dict["social_id"] as? String
                        {
                            userInfoObj.social_id = social_id
                        }
                        if let social_type = dict["social_type"] as? String
                        {
                            userInfoObj.social_type = social_type
                        }
                        if let login_type = dict["login_type"] as? String
                        {
                            userInfoObj.login_type = login_type
                        }
                        if let country_id = dict["country_id"] as? Int
                        {
                            userInfoObj.country_id = String(country_id)
                        }
                        if let country = dict["country"] as? [String:Any]
                        {
                            if let country_name = country["country_name"] as? String
                            {
                                userInfoObj.country_name = country_name
                            }
                        }
                        let userDefaults = UserDefaults.standard
                        userDefaults.set(userInfoObj.encode(), forKey: "User_info")
                            CommonUtilities.shared.sidemenu()
                    }
                    else if statuscode == 400{
                        self.createAlert(message: Constant.string.verification.localized())
                    }
                    else{
                        self.createAlert(message: msg)
                    }
                }
            }
        }
    }
    
    func hitServiceForFBLogin(name:String,email:String,socialID:String){
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let parameters =
            [
                "name" : name,
                "email": email,
                "login_type":"facebook",
                "password":"",
                "country_id":0,   //int
                "social_id":socialID
                ]as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString:"signup", parameters: parameters as [String : AnyObject] , encoding: JSONEncoding.default, headers: nil)
        { (success, dictionary, error) in
            print(dictionary ?? "")
            KRProgressHUD.dismiss()
            if let statuscode = dictionary?["status"] as? NSNumber
            {
                let msg = dictionary?["message"] as! String
                var userInfoObj = Userinfo()
                if let dict = dictionary?["data"] as? [String:Any]
                {
                    if statuscode == 200
                    {
                        if let active = dict["active"] as? Int
                        {
                            userInfoObj.active = String(active)
                        }
                        if let access_token = dict["access_token"] as? String
                        {
                            userInfoObj.access_token = access_token
                        }
                        if let email = dict["email"] as? String
                        {
                            userInfoObj.email = email
                        }
                        if let id = dict["id"] as? Int
                        {
                            userInfoObj.id = String(id)
                        }
                        if let name = dict["name"] as? String
                        {
                            userInfoObj.name = name
                        }
                        if let social_id = dict["social_id"] as? String
                        {
                            userInfoObj.social_id = social_id
                        }
                        if let social_type = dict["social_type"] as? String
                        {
                            userInfoObj.social_type = social_type
                        }
                        if let login_type = dict["login_type"] as? String
                        {
                            userInfoObj.login_type = login_type
                        }
                        if let country = dict["country"] as? [String:Any]
                        {
                            if let country_id = country["id"] as? Int
                            {
                                userInfoObj.country_id = String(country_id)
                            }
                            if let country_name = country["country_name"] as? String
                            {
                                userInfoObj.country_name = country_name
                            }
                        }
                        let userDefaults = UserDefaults.standard
                        userDefaults.set(userInfoObj.encode(), forKey: "User_info")
                        print(CommonUtilities.shared.getuserdata()?.id)
                        CommonUtilities.shared.sidemenu()
                    }
                    else
                    {
                        self.createAlert(message: msg)
                    }
                }
                
            }
        }
    }
}

struct Userinfo
{
    var access_token = ""
    var active = ""
    var country_name = ""
    var country_id = ""
    var email = ""
    var id = ""
    var login_type = ""
    var name = ""
    var social_id = ""
    var social_type = ""
}

extension Userinfo
{
    init?(data: NSData)
    {
        if let coding = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? Encoding_user
        {
            access_token = coding.access_token
            active = coding.active
            country_name = coding.country_name
            email = coding.email
            id = coding.id
            login_type = coding.login_type
            name = coding.name
            social_id = coding.social_id
            social_type = coding.social_type
            country_id = coding.country_id
        }
        else
        {
            return nil
        }
    }
    
    func encode() -> NSData
    {
        return NSKeyedArchiver.archivedData(withRootObject: Encoding_user(self)) as NSData
    }
    
    @objc(_TtCV3XOC8UserinfoP33_32514F4DE9FDE7B9B50DBF8C5E6774B513Encoding_user) private class Encoding_user: NSObject, NSCoding
    {
        var access_token = ""
        var active = ""
        var country_name = ""
        var email = ""
        var id = ""
        var login_type = ""
        var name = ""
        var social_id = ""
        var social_type = ""
        var country_id = ""
        
        init(_ myObject: Userinfo)
        {
            access_token = myObject.access_token
            active = myObject.active
            country_name = myObject.country_name
            email = myObject.email
            id = myObject.id
            login_type = myObject.login_type
            name = myObject.name
            social_id = myObject.social_id
            social_type = myObject.social_type
            country_id = myObject.country_id
        }
        
        @objc required init?(coder aDecoder: NSCoder)
        {
            access_token =  (aDecoder.decodeObject(forKey: "access_token") as? String)!
            active =  (aDecoder.decodeObject(forKey: "active") as? String)!
            country_name = (aDecoder.decodeObject(forKey: "country_name") as? String)!
            email = (aDecoder.decodeObject(forKey: "email") as? String)!
            id = (aDecoder.decodeObject(forKey: "id") as? String)!
            login_type = (aDecoder.decodeObject(forKey: "login_type") as? String)!
            name = (aDecoder.decodeObject(forKey: "name") as? String)!
            social_id = (aDecoder.decodeObject(forKey: "social_id") as? String)!
            social_type = (aDecoder.decodeObject(forKey: "social_type") as? String)!
            country_id = (aDecoder.decodeObject(forKey: "country_id") as? String)!
            
        }
        @objc func encode(with aCoder: NSCoder)
        {
            aCoder.encode(access_token, forKey: "access_token")
            aCoder.encode(active, forKey: "active")
            aCoder.encode(country_name , forKey: "country_name")
            aCoder.encode(email , forKey: "email")
            aCoder.encode(id , forKey: "id")
            aCoder.encode(login_type , forKey: "login_type")
            aCoder.encode(name , forKey: "name")
            aCoder.encode(social_type , forKey: "social_type")
            aCoder.encode(social_id , forKey: "social_id")
            aCoder.encode(social_type , forKey: "social_type")
            aCoder.encode(social_type , forKey: "country_id")
        }
    }
}

struct SocialLogin {
    static var shared = SocialLogin()
    var loginvia = false
}
