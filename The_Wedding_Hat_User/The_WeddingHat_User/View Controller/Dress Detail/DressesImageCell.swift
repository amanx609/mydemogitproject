//
//  DressesImageCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 07/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class DressesImageCell: UITableViewCell {
    
    @IBOutlet weak var dress_pageController: UIPageControl!
    @IBOutlet weak var dressImg_collection: UICollectionView!
    
    var imageArr = [String]()
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.dressImg_collection.delegate = self
        self.dressImg_collection.dataSource = self
        self.dressImg_collection.collectionViewLayout = {
            let flowLayout = UICollectionViewFlowLayout()
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0.0
            return flowLayout
        }()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    
}

extension DressesImageCell : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return imageArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DressImagesCollCell", for: indexPath) as! DressImagesCollCell
        if self.imageArr.count > 0
        {
            if let img_url = URL(string: self.imageArr[0])
            {
                cell.img_view.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "add_images"))
            }
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}
