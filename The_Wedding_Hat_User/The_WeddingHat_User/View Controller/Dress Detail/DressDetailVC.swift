//
//  DressDetailVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 07/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

class DressDetailVC: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var header_lbl: UILabel!
    @IBOutlet weak var enquire_btn: UIButton!
    @IBOutlet weak var dressTable: UITableView!
    
    var product_id = ""
    var product_image = [String]()
    var objproductDetailModel = ProductDetailModel()
    var headingArr = ["Dress Code","Designer","Price","Neckline","Silhouette","Fabric","Style","Season","Waist","Accent"]
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.headerView.dropShadow()
        self.enquire_btn.makeRoundCorner(8)
        self.dressTable.separatorColor = .clear
        self.HitservicerProductDetail(productId: self.product_id)
        
    }
    @IBAction func tapOnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnEnquire(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SendMessageVC") as! SendMessageVC
        vc.vendor_id = self.objproductDetailModel.vendor_id
        vc.inventroy_id = self.objproductDetailModel.id
        self.present(vc, animated: true, completion: {})
    }
}

extension DressDetailVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 11
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (indexPath.row == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DressesImageCell", for: indexPath) as! DressesImageCell
            cell.selectionStyle = .none
            cell.imageArr = self.product_image
            cell.dressImg_collection.reloadData()
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DressMenuCell", for: indexPath) as! DressMenuCell
            cell.selectionStyle = .none
            if indexPath.row == 0
            {
                if self.objproductDetailModel.dress_code != ""
                {
                    cell.menuHeading_lbl.text = "Dress Code"
                    cell.menu_lbl.text = self.objproductDetailModel.dress_code
                }
            }
            if indexPath.row == 1
            {
                if self.objproductDetailModel.designer != ""
                {
                    cell.menuHeading_lbl.text = "Designer"
                    cell.menu_lbl.text = self.objproductDetailModel.designer
                }
            }
            if indexPath.row == 2
            {
                if self.objproductDetailModel.price != ""
                {
                    cell.menuHeading_lbl.text = "Price"
                    cell.menu_lbl.text = self.objproductDetailModel.price
                }
            }
            if indexPath.row == 3
            {
                if self.objproductDetailModel.neckline != ""
                {
                    cell.menuHeading_lbl.text = "Neckline"
                    cell.menu_lbl.text = self.objproductDetailModel.neckline
                }
            }
            if indexPath.row == 3
            {
                if self.objproductDetailModel.silhouette != ""
                {
                    cell.menuHeading_lbl.text = "Silhouette"
                    cell.menu_lbl.text = self.objproductDetailModel.silhouette
                }
            }
            if indexPath.row == 4
            {
                if self.objproductDetailModel.fabric != ""
                {
                    cell.menuHeading_lbl.text = "Fabric"
                    cell.menu_lbl.text = self.objproductDetailModel.fabric
                }
            }
            if indexPath.row == 5
            {
                if self.objproductDetailModel.style != ""
                {
                    cell.menuHeading_lbl.text = "Style"
                    cell.menu_lbl.text = self.objproductDetailModel.style
                }
            }
            if indexPath.row == 6
            {
                if self.objproductDetailModel.season != ""
                {
                    cell.menuHeading_lbl.text = "Season"
                    cell.menu_lbl.text = self.objproductDetailModel.season
                }
            }
            if indexPath.row == 7
            {
                if self.objproductDetailModel.waist != ""
                {
                    cell.menuHeading_lbl.text = "Waist"
                    cell.menu_lbl.text = self.objproductDetailModel.waist
                }
            }
            if indexPath.row == 8
            {
                if self.objproductDetailModel.accent != ""
                {
                    cell.menuHeading_lbl.text = "Accent"
                    cell.menu_lbl.text = self.objproductDetailModel.accent
                }
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
}


extension DressDetailVC
{
    func HitservicerProductDetail(productId:String)
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        let headers: HTTPHeaders =
            [
                "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
                "Content-Type": "application/json"
        ]
        ServiceManager.instance.request(method: .get, URLString: "productDetails/"+productId, parameters: nil, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = dictionary?["data"] as? [String:Any]
                        {
                            if let productDetails = data["productDetails"] as? [String:Any]
                            {
                                let obj = ProductDetailModel()
                                
                                obj.id = productDetails.valueForString(key: "id")
                                obj.vendor_id = productDetails.valueForString(key: "vendor_id")
                                obj.designer = productDetails.valueForString(key: "designer")
                                obj.dress_code = productDetails.valueForString(key: "dress_code")
                                obj.dress_name = productDetails.valueForString(key: "dress_name")
                                
                                if let accent = productDetails["accent"] as? [String:Any]
                                {
                                    obj.accent = accent.valueForString(key: "name")
                                }
                                if let dress_category = productDetails["dress_category"] as? [String:Any]
                                {
                                    obj.dress_category = dress_category.valueForString(key: "name")
                                }
                                if let fabric = productDetails["fabric"] as? [String:Any]
                                {
                                    obj.fabric = fabric.valueForString(key: "name")
                                }
                                if let neckline = productDetails["neckline"] as? [String:Any]
                                {
                                    obj.neckline = neckline.valueForString(key: "name")
                                }
                                if let neckline = productDetails["neckline"] as? [String:Any]
                                {
                                    obj.neckline = neckline.valueForString(key: "name")
                                }
                                if let season = productDetails["season"] as? [String:Any]
                                {
                                    obj.season = season.valueForString(key: "name")
                                }
                                if let silhouette = productDetails["silhouette"] as? [String:Any]
                                {
                                    obj.silhouette = silhouette.valueForString(key: "name")
                                }
                                if let style = productDetails["style"] as? [String:Any]
                                {
                                    obj.style = style.valueForString(key: "name")
                                }
                                if let waist = productDetails["waist"] as? [String:Any]
                                {
                                    obj.waist = waist.valueForString(key: "name")
                                }
                                if let price = productDetails["price"] as? [String:Any]
                                {
                                    obj.price = "$"+"\(price.valueForString(key: "low_price"))-$\(price.valueForString(key: "high_price"))"
                                }
                                self.objproductDetailModel = obj
                                self.dressTable.reloadData()
                            }
                        }
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                        
                    }
                }
            }
        }
    }
}



class ProductDetailModel
{
    var id = ""
    var accent = ""
    var designer = ""
    var dress_category = ""
    var dress_code = ""
    var dress_name = ""
    var fabric = ""
    var neckline = ""
    var price = ""
    var season = ""
    var silhouette = ""
    var waist = ""
    var style = ""
    var vendor_id = ""
}
