//
//  DressMenuCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 07/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class DressMenuCell: UITableViewCell {
    
    @IBOutlet weak var menu_lbl: UILabel!
    @IBOutlet weak var menuHeading_lbl: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
}
