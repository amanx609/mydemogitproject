//
//  DressImagesCollCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 07/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class DressImagesCollCell: UICollectionViewCell
{
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var img_view: UIImageView!
    
    override func awakeFromNib()
    {
        self.mainView.dropShadow()
        self.mainView.makeRoundCorner(8)
        self.img_view.makeRoundCorner(8)
    }
}
