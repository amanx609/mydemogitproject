//
//  IdeasVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 10/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

class IdeasVC: UIViewController {
    
    @IBOutlet weak var ideasCollectionView: UICollectionView!
    @IBOutlet weak var pollView: UIView!
    @IBOutlet weak var realWeddingView: UIView!
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var pollTblView: UITableView!
    @IBOutlet weak var realWeddingTblView: UITableView!
    @IBOutlet var tblHeaderView: UIView!
    @IBOutlet weak var articleView: UIView!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var filteredByLbl: UILabel!
    @IBOutlet weak var articleTable: UITableView!
    
    var glblIndex = 0
    
    var questionObj = [QuestionAnswer]()
    
    let colArr = ["Article","Poll","Real Weddings"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setDesign()
       // self.hitServiceget_Articles()
        self.articleTable.separatorColor = .clear
    }
    
    
    @IBAction func tapOnPlusBtn(_ sender: Any)
    {
        self.createAlert(message: "Under Development")
//        let storyboard = UIStoryboard(name: "IdeasAndInspire", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "SubmitYourWeddingVC") as! SubmitYourWeddingVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    internal func setUpDataModel()
    {
        self.articleView.isHidden = false
        self.pollView.isHidden = true
        self.realWeddingView.isHidden = true
        self.ideasCollectionView.reloadData()
    }
}


extension IdeasVC : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return colArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IdeasCollectionCell", for: indexPath) as! IdeasCollectionCell
        cell.cellLabel.text = self.colArr[indexPath.row]
        if(glblIndex == indexPath.row)
        {
            cell.cellLabel.textColor = .white
            cell.cellLabel.backgroundColor = UIColor(hexString: "#00375F")
            cell.cellView.backgroundColor = UIColor(hexString: "#00375F")
        }
        else
        {
            cell.cellLabel.textColor = UIColor(hexString: "#00375F")
            cell.cellLabel.backgroundColor  = .white
            cell.cellView.backgroundColor = .white
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 160, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        glblIndex = indexPath.row
        if(indexPath.row == 0)
        {
            self.articleView.isHidden = false
            self.pollView.isHidden = true
            self.plusBtn.isHidden = true
            self.realWeddingView.isHidden = true
        }
        else if(indexPath.row == 1)
        {
            self.articleView.isHidden = true
            self.pollView.isHidden = false
            self.plusBtn.isHidden = true
            self.realWeddingView.isHidden = true
        }
        else
        {
            self.articleView.isHidden = true
            self.pollView.isHidden = true
            self.plusBtn.isHidden = false
            self.realWeddingView.isHidden = false
        }
        
        self.ideasCollectionView.reloadData()
    }
}
//extension IdeasVC : UITableViewDelegate, UITableViewDataSource
//{
//    func numberOfSections(in tableView: UITableView) -> Int
//    {
//        if tableView == pollTblView
//        {
//            return questionObj.count
//        }
//        else
//        {
//            return 1
//        }
//    }
//
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if tableView == pollTblView
//        {
//            return questionObj[section].answer.count
//        }
//        else
//        {
////            return realWeddingObj.count
//            6
//        }
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        if tableView == pollTblView
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "PollCell", for: indexPath) as! PollCell
//            cell.selectionStyle = .none
//            cell.optionLabel.text = self.questionObj[indexPath.section].answer[indexPath.row].option
//            if self.questionObj[indexPath.section].is_answer == true
//            {
//                if self.questionObj[indexPath.section].answer[indexPath.row].is_selected == true
//                {
//                   cell.tickImage.image = #imageLiteral(resourceName: "checkMarkSelect")
//                    cell.createBorder()
//                    cell.optionLabel.textColor = UIColor(red: 204/255, green: 148/255, blue: 146/255, alpha: 1)
//                }
//                else
//                {
//                    cell.tickImage.image = #imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle-outline")
//                    cell.removeBorder()
//                    cell.optionLabel.textColor = UIColor(red: 0/255, green: 55/255, blue: 95/255, alpha: 1)
//                }
//            }
//            else
//            {
//                cell.tickImage.image = #imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle-outline")
//                cell.removeBorder()
//                cell.optionLabel.textColor = UIColor(red: 0/255, green: 55/255, blue: 95/255, alpha: 1)
//            }
//            return cell
//        }
//        else
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "RealWeddingCell", for: indexPath) as! RealWeddingCell
//            cell.mainView.backgroundColor = .white
//            cell.nameLbl.text = realWeddingObj[indexPath.row].name
//            cell.cityCountyLbl.text = realWeddingObj[indexPath.row].city + realWeddingObj[indexPath.row].country
//            cell.partnerNameLbl.text = realWeddingObj[indexPath.row].partner_name
//            return cell
//        }
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        if tableView == pollTblView
//        {
//            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width - 20, height: 60))
//            headerView.backgroundColor = UIColor(red: 204/255, green: 148/255, blue: 146/255, alpha: 0.1)
//            headerView.makeRoundCorner(10.0)
//            let label = UILabel()
//            label.frame = CGRect.init(x: 15, y: 0, width: headerView.frame.width-10, height: headerView.frame.height-10)
//            label.text = "Q. " + questionObj[section].question
//            label.textAlignment = .left
//            label.numberOfLines = 0
//            label.font = UIFont(name: "Montserrat,Semi-Bold", size: 16)
//            label.textColor = UIColor(hexString: "#CC948E")
//            headerView.addSubview(label)
//            return headerView
//        }
//        return UIView()
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if tableView == pollTblView
//        {
//            return 60
//        }
//        else if tableView == articleTable
//        {
//            return UITableView.automaticDimension
//        }
//        else
//        {
//            return .leastNormalMagnitude
//        }
//
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if tableView == pollTblView
//        {
//            return 70
//        }
//        else
//        {
//            return 250
//        }
//
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//    {
//        if tableView == pollTblView
//        {
//            if (self.questionObj[indexPath.section].is_answer == false)
//            {
//                self.hitPollAnswerService(question_Id: self.questionObj[indexPath.section].que_Id, optn_Id: self.questionObj[indexPath.section].answer[indexPath.row].option_id)
//            }
//        }
//        else if tableView == articleTable
//        {
//            let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
//            let vc = storyboard.instantiateViewController(withIdentifier: "PDFViewVC") as! PDFViewVC
//          //  vc.pdf = self.objArticelModel[indexPath.row].postLink
//            self.viewController?.present(vc, animated: true, completion: nil)
//        }
//        else
//        {
//
//        }
//
//    }
//}




extension IdeasVC
{
    func setDesign()
    {
        self.articleView.isHidden = false
        self.pollView.isHidden = true
        self.realWeddingView.isHidden = true
        self.headView.dropShadow()
        self.plusBtn.isHidden = true
        self.dropDownView.makeRoundCorner(10.0)
        self.ideasCollectionView.delegate = self
        self.ideasCollectionView.dataSource = self
        
        self.hitServicePoll()
        
        self.pollTblView.allowsSelection = true
        self.plusBtn.makeRounded()
        
    }
    
    func hitServicePoll()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2837521732, blue: 0.44864434, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [ "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Content-Type": "application/json"]
        ServiceManager.instance.request(method: .get, URLString: "question", parameters: nil, encoding: JSONEncoding.default, headers: headers) { (sucess, response, error) in
            KRProgressHUD.dismiss()
            if (sucess)!{
                if let statuscode = response?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = response?["data"] as? [String : Any]{
                            if let questionsArry = data["questions"] as? [[String:Any]]
                            {
                                self.questionObj.removeAll()
                                for allque in questionsArry
                                {
                                    let obj = QuestionAnswer()
                                    if let id = allque["id"] as? Int
                                    {
                                        obj.que_Id = String(id)
                                    }
                                    if let questions = allque["question"] as? String
                                    {
                                        obj.question = questions
                                    }
                                    if let is_selected = allque["is_answer"] as? Bool
                                    {
                                        obj.is_answer = is_selected
                                    }
                                    if let optionArr = allque["options"] as? [[String:Any]]
                                    {
                                        for option in optionArr
                                        {
                                            let ans = Answer()
                                            if let options = option["option"] as? String
                                            {
                                                ans.option = options
                                            }
                                            if let option_id = option["id"] as? Int
                                            {
                                                ans.option_id = String(option_id)
                                            }
                                            if let is_selected = option["is_selected"] as? Bool
                                            {
                                                ans.is_selected = is_selected
                                            }
                                            obj.answer.append(ans)
                                        }
                                    }
                                    self.questionObj.append(obj)
                                }
                                self.pollTblView.reloadData()
                            }
                        }
                    }
                    else
                    {
                        if let message = response?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                    }
                }
            }
            else{
                print("error")
            }
        }
        
        
        
    }
    
    func hitPollAnswerService(question_Id : String , optn_Id : String)
    {
        let headers: HTTPHeaders = [ "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Content-Type": "application/json"]
        let param =
            [
                "user_id": CommonUtilities.shared.getuserdata()?.id ,
                "question_id" : question_Id ,
                "option_id" : optn_Id
        ]
        ServiceManager.instance.request(method: .post, URLString: "pollAnswer", parameters: param as [String : Any], encoding: JSONEncoding.default, headers: headers) { (sucess, response, error) in
            if (sucess)!
            {
                if let statuscode = response?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        self.hitServicePoll()
                    }
                }
            }
            else
            {
                print(param)
            }
        }
    }
    
}


class NewDataModel
{
    var name : String!
    var isSelected = false
    
    init(_ dict : [String : String])
    {
        self.name = dict["data"]
    }
}




