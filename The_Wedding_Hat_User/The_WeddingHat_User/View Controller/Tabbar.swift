//
//  Tabbar.swift
//  The_WeddingHat_User
//
//  Created by appl on 08/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class Tabbar: UITabBarController,UITabBarControllerDelegate {
    
    var HomeBtn : UITabBarItem!
    var PlanningBtn : UITabBarItem!
    var VendorBtn : UITabBarItem!
    var PartyBtn : UITabBarItem!
    var IdeaBtn : UITabBarItem!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        HomeBtn = UITabBarItem()
        PlanningBtn = UITabBarItem()
        VendorBtn =  UITabBarItem()
        PartyBtn =  UITabBarItem()
        IdeaBtn =  UITabBarItem()
        setUpTabBarElements()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vendor = UIStoryboard(name: "Vendordetails", bundle: nil)
        let ideas_storyboard = UIStoryboard(name: "IdeasAndInspire", bundle: nil)
        let demo_stry = UIStoryboard(name: "Demo", bundle: nil)

        let VC1 = storyboard.instantiateViewController(withIdentifier: "HomeVC")
        let VC2 = storyboard.instantiateViewController(withIdentifier: "PlanninngVC")
        let VC3 = vendor.instantiateViewController(withIdentifier: "SelectServiceVC")
        let VC4 = demo_stry.instantiateViewController(withIdentifier: "WeddingPartyListVC")
        let VC5 = ideas_storyboard.instantiateViewController(withIdentifier: "IdeasandInspirationVC")
        VC1.tabBarItem = HomeBtn
        VC2.tabBarItem = PlanningBtn
        VC3.tabBarItem = VendorBtn
        VC4.tabBarItem = PartyBtn
        VC5.tabBarItem = IdeaBtn
        
        self.viewControllers = [VC1, VC2, VC3, VC4,VC5]
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(displayP3Red: 32/255, green: 86/229, blue: 249/255, alpha: 1.0)], for: .selected)
        UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -8)
        
    }
   
    override func viewWillLayoutSubviews()
    {
        var newTabBarFrame = tabBar.frame
        let newTabBarHeight: CGFloat = 60
        newTabBarFrame.size.height = newTabBarHeight
        newTabBarFrame.origin.y = self.view.frame.size.height - newTabBarHeight
        tabBar.bottomAnchor.constraint(equalToSystemSpacingBelow: view.bottomAnchor, multiplier:0)
        tabBar.layer.backgroundColor = UIColor.white.cgColor
        tabBar.layer.shadowColor = UIColor(red: 40.0/255.0, green: 150.0/255.0, blue: 252.0/255.0, alpha: 1.0).cgColor
        tabBar.layer.shadowOffset = CGSize(width: 0, height: 1)
        tabBar.layer.shadowOpacity = 0.3
        tabBar.layer.shadowRadius = 4.0
        tabBar.frame = newTabBarFrame
        
    }
   
    func setUpTabBarElements()
    {
        self.HomeBtn.imageInsets = UIEdgeInsets(top:0, left:10, bottom: 0, right: -10)
        self.IdeaBtn.imageInsets = UIEdgeInsets(top:0, left:-10, bottom: 0, right: 10)
        
        self.HomeBtn.image = #imageLiteral(resourceName: "home1").withRenderingMode(.alwaysOriginal)
        self.PlanningBtn.image = #imageLiteral(resourceName: "planning1").withRenderingMode(.alwaysOriginal)
        self.VendorBtn.image = #imageLiteral(resourceName: "vendor1").withRenderingMode(.alwaysOriginal)
        self.PartyBtn.image = #imageLiteral(resourceName: "party1").withRenderingMode(.alwaysOriginal)
        self.IdeaBtn.image = #imageLiteral(resourceName: "idea1").withRenderingMode(.alwaysOriginal)
        
        
        self.HomeBtn.selectedImage = #imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal)
        self.PlanningBtn.selectedImage = #imageLiteral(resourceName: "planning").withRenderingMode(.alwaysOriginal)
        self.VendorBtn.selectedImage = #imageLiteral(resourceName: "vendors").withRenderingMode(.alwaysOriginal)
        self.PartyBtn.selectedImage = #imageLiteral(resourceName: "wedding").withRenderingMode(.alwaysOriginal)
        self.IdeaBtn.selectedImage = #imageLiteral(resourceName: "ideas").withRenderingMode(.alwaysOriginal)
   }
    
}
