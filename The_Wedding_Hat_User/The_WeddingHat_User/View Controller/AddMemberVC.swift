//
//  AddMemberVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 06/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//
/*
import UIKit
import Alamofire
import KRProgressHUD
import AlamofireImage

protocol AddMember{
    func addNewMember(memData: [[String : Any]])
}
protocol AddMemberWeddingList {
    func addMember(memData : [[String:Any]])
}

class AddMemberVC: UIViewController {
    
    @IBOutlet weak var selectRoleText: UITextField!
    @IBOutlet weak var fullNameText: UITextField!
    @IBOutlet weak var emailIdText: UITextField!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var provideAccessView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveMember: UIButton!
    @IBOutlet var footerView: UIView!
    
    @IBOutlet weak var insideFooterView: UIView!
    let picker = UIPickerView()
    var indexPath = 0
    var selectedIndexPath  = Int()
    var delegate : AddMember?
    var weddingDelegate : AddMemberWeddingList?
    var  roleId = ""
    var memberAccessID = [String]()
    var memberData = [[String:Any]]()
    var objMemberAccessModel = [MemberAccessModel]()
    var objMemberRoleModel = [MemberRoleModel]()
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.picker.delegate = self
        self.picker.dataSource = self
        self.fullNameText.delegate = self
        self.setUpUI()
        self.hitServicegetMemberRole()
    }
    func setUpUI()
    {
        self.mainView.layer.cornerRadius = 10.0
        self.mainView.dropShadow()
        self.fullNameText.colorPlaceHolder(placeholder: Constant.string.fullname.localized(), color: "00375F")
        self.emailIdText.colorPlaceHolder(placeholder: Constant.string.email.localized(), color: "00375F")
        self.selectRoleText.colorPlaceHolder(placeholder: Constant.string.selectRole.localized(), color: "00375F")
        self.selectRoleText.inputView = picker
        self.selectRoleText.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneRoleButton))
        self.cancelButton.addDashBorder(color: UIColor.init(hexString: "#00375F")!)
        self.saveMember.makeRoundCorner(10.0)
        self.headerView.layer.cornerRadius = 10
        self.headerView.layer.maskedCorners = [.layerMaxXMaxYCorner , .layerMinXMinYCorner]
        self.insideFooterView.layer.cornerRadius = 10
        self.insideFooterView.layer.maskedCorners = [.layerMaxXMaxYCorner , .layerMinXMaxYCorner]
    }
}

extension AddMemberVC : UITableViewDelegate , UITableViewDataSource 
{
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.objMemberAccessModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddMoreTableCell", for: indexPath) as! AddMoreTableCell
        cell.cellLabel.text = self.objMemberAccessModel[indexPath.row].access
        if self.memberAccessID.contains(objMemberAccessModel[indexPath.row].id)
        {
            cell.checkButton.setImage(#imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle"), for: .normal)
        }
        else
        {
            cell.checkButton.setImage(#imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle-outline"), for: .normal)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 315
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let index = memberAccessID.index(where: {$0 == objMemberAccessModel[indexPath.row].id})
        {
            self.memberAccessID.remove(at: index)
            print(memberAccessID)
        }
        else
        {
            self.memberAccessID.append(objMemberAccessModel[indexPath.row].id)
            print(memberAccessID)
        }
        self.tblView.reloadData()
    }
}

extension AddMemberVC: UIPickerViewDelegate , UIPickerViewDataSource
{
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return objMemberRoleModel.count
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return objMemberRoleModel[row].role
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.indexPath = row
    }
    @objc func doneRoleButton(_ sender : Any){
        if objMemberRoleModel.count == 0{}else{
            self.selectRoleText.text = self.objMemberRoleModel[indexPath].role
            self.roleId = self.objMemberRoleModel[indexPath].id
        }
        
    }
}

extension AddMemberVC{
    @IBAction func saveMemberButton(_ sender: Any) {
        self.validation()
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func validation() -> Bool{
        guard let role = selectRoleText.text, !role.isEmpty  else{
            self.createAlert(message: Constant.string.slecetRole.localized())
            return false
        }
        guard let name = fullNameText.text, !name.isEmpty  else
        {
            self.createAlert(message: Constant.string.memberName.localized())
            return false
        }
        guard let email = emailIdText.text, !email.isEmpty  else
        {
            self.createAlert(message: Constant.string.memberEmail.localized())
            return false
        }
        guard CommonUtilities.shared.isValidEmail(testStr: emailIdText.text!) else
        {
            self.createAlert(message: Constant.string.validEmail.localized())
            return false
        }
        if self.memberAccessID.count == 0
        {
            self.createAlert(message: Constant.string.provideAccess.localized())
            return false
        }
        else
        {
            print(memberData)
            var MemberData: [String: Any] = ["user_member_role_id":self.roleId,
                                             "user_member_access_id": self.memberAccessID,
                                             "member_name":self.fullNameText.text!,
                                             "email":self.emailIdText.text!]
            self.memberData.append(MemberData)
            print(memberData)
            self.delegate?.addNewMember(memData: memberData)
            self.dismiss(animated: true, completion: nil)
            return true
        }
        return true
    }
    
    
}
extension AddMemberVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
         if textField == fullNameText
         {
                   let maxLength = 20
                   let currentString: NSString = fullNameText!.text as! NSString
                   let newString: NSString =
                       currentString.replacingCharacters(in: range, with: string) as NSString
                   return newString.length <= maxLength
           }
        return false
    }
   
}
extension AddMemberVC{
    func hitServicegetMemberRole()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        ServiceManager.instance.request(method: .get, URLString: "getMemberRoleAcesss", parameters:nil, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!{
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if let dict = dictionary?["data"] as? [String:Any]
                    {
                        if statuscode == 200
                        {
                            self.objMemberRoleModel.removeAll()
                            if let member_access = dict["member_access"] as? [[String:Any]]{
                                for member in member_access{
                                    var obj = MemberAccessModel()
                                    if let access = member["access"] as? String
                                    {
                                        obj.access = access
                                    }
                                    if let created_at = member["created_at"] as? String
                                    {
                                        obj.created_at = created_at
                                    }
                                    if let id = member["id"] as? Int
                                    {
                                        obj.id = String(id)
                                    }
                                    if let status = member["status"] as? Int
                                    {
                                        obj.status = String(status)
                                    }
                                    self.objMemberAccessModel.append(obj)
                                }
                                print(self.objMemberAccessModel.count)
                            }
                            if let member_role = dict["member_role"] as? [[String:Any]]{
                                for memberRole in member_role{
                                    var objRole = MemberRoleModel()
                                    if let role = memberRole["role"] as? String
                                    {
                                        objRole.role = role
                                    }
                                    if let created_at = memberRole["created_at"] as? String
                                    {
                                        objRole.created_at = created_at
                                    }
                                    if let id = memberRole["id"] as? Int
                                    {
                                        objRole.id = String(id)
                                    }
                                    self.objMemberRoleModel.append(objRole)
                                }
                                self.tblView.reloadData()
                            }
                        }
                        else{
                            //self.createAlert(title: Constant.string.appname, message: msg)
                        }
                    }


                }
            }
            else{
                // print(parameters)
            }
        }
    }
}

class MemberAccessModel{
    var access = ""
    var created_at = ""
    var id = ""
    var status = ""
}
class MemberRoleModel{
    var created_at = ""
    var id = ""
    var role = ""
 }*/
