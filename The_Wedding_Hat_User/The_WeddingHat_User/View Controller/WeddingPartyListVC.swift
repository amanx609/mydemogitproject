//
//  WeddingPartyListVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 04/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

protocol SaveMember {
    func saveWeddingPartyMember(wedMemdata : [[String:Any]])
}


class WeddingPartyListVC: UIViewController , HitAddWeddingPartyListApi{
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var addMemberView: UIView!
    @IBOutlet weak var addMemberTap: UIButton!
    var userMemberList = [MemberList]()
    var weddingDelegate : SaveMember?
    var memberData = [[String:Any]]()
    var userMemberCount = 0
    var memberCell = Int()
    var removeCell = Int()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.mainView.dropShadowBlueColor()
        self.memberListApiService()
     }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.memberListApiService()
    }
    
    
    
    @IBAction func addMemberTap(_ sender: Any) {
        let stryBrd = UIStoryboard(name: "Main", bundle: nil)
        let vc = stryBrd.instantiateViewController(withIdentifier: "NewAddMemberVC") as! NewAddMemberVC
        vc.memberCount = userMemberList.count
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self
        self.present(vc , animated: true , completion: nil)
    }
    
    func NewAddMember()
    {
        self.memberListApiService()
    }
    
    
}

extension WeddingPartyListVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userMemberList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeddingPartyCell", for: indexPath) as! WeddingPartyCell
        cell.name.text = userMemberList[indexPath.row].member_name
        cell.mail_id.text = userMemberList[indexPath.row].email
        cell.role.text = userMemberList[indexPath.row].event_member_role
        if(userMemberList[indexPath.row].payment_status == "0")
        {
            cell.freeOrPaidLbl.text = "Free"
            cell.freePaidView.backgroundColor = UIColor(hexString: "#FAF4F3")
            cell.freeOrPaidLbl.textColor = #colorLiteral(red: 0.8, green: 0.5803921569, blue: 0.5568627451, alpha: 1)
        }
        else
        {
            cell.freeOrPaidLbl.text = "Paid"
            cell.freeOrPaidLbl.textColor = UIColor(hexString: "#27527D")
            cell.freePaidView.backgroundColor = UIColor(hexString: "#EEF8FF")
        }
        cell.delete_btn.tag = indexPath.row
        cell.delete_btn.addTarget(self, action: #selector(connectedDelete(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
}


extension WeddingPartyListVC
{
    @objc func connectedDelete(sender: UIButton)
    {
        deleteMember(member_id: Int(userMemberList[sender.tag].user_member_id) ?? 0)
        
    }
    
    func memberListApiService()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        let params :Parameters  = ["user_id" : CommonUtilities.shared.getuserdata()?.id ?? "",
                                   "event_id": UserDefaults.standard.object(forKey: "EventIDFromCreateEVent") ?? "",
                                   "event_user_type":"Admin"
        ]
        ServiceManager.instance.request(method: .post, URLString: "memberList", parameters: params, encoding: JSONEncoding.default, headers: headers) { (sucess, response, error) in
            KRProgressHUD.dismiss()
            if(sucess)!{
                if let sucesscode = response?["status"] as? NSNumber{
                    print(sucesscode)
                    if(sucesscode == 200){
                        self.userMemberList.removeAll()
                        if let data = response?["data"] as? [String:Any]
                        {
                            if let memberList = data["memberList"] as? [[String:Any]]{
                                for data in memberList{
                                    let obj = MemberList()
                                    print(data)
                                    obj.event_member_id = data.valueForInt(key: "event_member_id") ?? -1
                                    obj.event_member_image = data.valueForString(key: "event_member_image")
                                    obj.event_member_role = data.valueForString(key: "event_member_role")
                                    obj.member_name = data.valueForString(key: "member_name")
                                    obj.email = data.valueForString(key: "email")
                                    obj.payment_status = data.valueForString(key: "payment_status")
                                    if let user_member_id = data["user_member_id"] as? Int
                                    {
                                        obj.user_member_id = String(user_member_id)
                                    }
                                    if let acess = data["access"] as? [Any]
                                    {
                                        for type in acess{
                                            obj.acessArr.removeAll()
                                            let objAcess = Acess()
                                            objAcess.acessName = type as? String ?? ""
                                            obj.acessArr.append(objAcess)
                                            print(type)
                                        }
                                    }
                                    self.userMemberList.append(obj)
                                }
                            }
                        }
                        self.tblView.reloadData()
                    }
                }
            }
        }
    }
    
    
    func deleteMember(member_id : Int)
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = ["Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,"Accept": "application/json"]
        let params :Parameters  = ["member_id" : member_id]
        print(params)
        ServiceManager.instance.request(method: .post, URLString: "deleteMember", parameters: params, encoding: JSONEncoding.default, headers: headers) { (sucess, response, error) in
            KRProgressHUD.dismiss()
            if(sucess)!{
                if let sucesscode = response?["status"] as? NSNumber
                {
                    if(sucesscode == 200)
                    {
                        self.memberListApiService()
                    }
                }
            }
        }
    }
    
    
}



class WeddingPartyCell : UITableViewCell
{
    
    @IBOutlet weak var viewOfimage: UIView!
    @IBOutlet weak var viewOfDesc: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var role: UILabel!
    @IBOutlet weak var mail_id: UILabel!
    @IBOutlet weak var delete_btn: UIButton!
    @IBOutlet weak var freeOrPaidLbl: UILabel!
    @IBOutlet weak var weddingPartyimages: UIImageView!
    @IBOutlet weak var newView: UIView!
    @IBOutlet weak var freePaidView: UIView!
    
    override func awakeFromNib()
    {
        viewOfimage.layer.cornerRadius = 10
        weddingPartyimages.layer.cornerRadius = 10
        viewOfDesc.dropShadowBlueColor()
        viewOfimage.dropShadowBlueColor()
        viewOfDesc.layer.cornerRadius = 10
        viewOfDesc.layer.maskedCorners = [.layerMaxXMinYCorner , .layerMaxXMaxYCorner]
     }
}

class MemberList
{
    var email = ""
    var event_member_id = Int()
    var event_member_image = ""
    var event_member_role = ""
    var member_name = ""
    var payment_status = ""
    var user_member_id = ""
    var acessArr = [Acess]()
}

class Acess
{
    var acessName = ""
}
