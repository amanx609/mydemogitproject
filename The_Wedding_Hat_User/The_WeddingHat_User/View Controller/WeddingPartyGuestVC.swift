//
//  CustomVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 19/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class WeddingPartyGuestVC: UIViewController{
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var memberLbl: UILabel!
    @IBOutlet weak var plusView: UIView!
    
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.plusView.layer.cornerRadius = 8
    }
  
}
extension WeddingPartyGuestVC:  UITableViewDelegate , UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return 10
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "GuestListCell", for: indexPath) as! GuestListCell
          return cell
      }
}

class GuestListCell : UITableViewCell{
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewOfImage: UIView!
    @IBOutlet weak var newView: UIView!
    @IBOutlet weak var descView: UIView!
    
    override func awakeFromNib() {
        self.mainView.layer.cornerRadius = 8
        self.mainView.dropShadowBlueColor()
        self.viewOfImage.layer.cornerRadius = 8
        self.newView.dropShadowBlueColor()
        self.descView.layer.cornerRadius = 8
//        self.newView.layer.shadowRadius = 5
//        self.newView.layer.shadowColor = #colorLiteral(red: 0.9333333333, green: 0.9725490196, blue: 0.9960784314, alpha: 1)
//        self.newView.layer.shadowOpacity = 1
//        self.newView.layer.shadowOffset = CGSize(width: 0, height: 0)
//        self.newView.layer.masksToBounds = false
//        self.newView.dropShadowBlueColor()
//        self.newView.layer.borderColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        
    }
}
