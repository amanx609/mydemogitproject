//
//  CommunityDetail.swift
//  The_WeddingHat_User
//
//  Created by appl on 30/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD
import AlamofireImage

class CommunityDetail: UIViewController , SwiftySwitchDelegate {
    
    
    
    @IBOutlet weak var privateOnView: SwiftySwitch!
    @IBOutlet weak var img_view: UIView!
    @IBOutlet weak var view_btn: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var badge_lbl: UILabel!
    @IBOutlet weak var badge_view: UIView!
    @IBOutlet var footer_view: UIView!
    @IBOutlet weak var comment_view: UIView!
    @IBOutlet weak var comment_btn: UIButton!
    @IBOutlet weak var discuss_btn: UIButton!
    @IBOutlet weak var message_btn: UIButton!
    @IBOutlet weak var badge_img: UIImageView!
    @IBOutlet weak var name_lbl: UILabel!
    @IBOutlet weak var discussion_view: UIView!
    @IBOutlet weak var profile_pic: UIImageView!
    @IBOutlet weak var vendroCOl: UICollectionView!
    @IBOutlet weak var communityTable: UITableView!
    @IBOutlet weak var expand_view: UIView!
    @IBOutlet weak var heightOfView: NSLayoutConstraint!
    @IBOutlet var tblHeaderView: UIView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var accountType: UILabel!
    @IBOutlet weak var questionIcon: UIImageView!
    @IBOutlet weak var badge_img_constraint: NSLayoutConstraint!
    
    var publicPrivate = Bool()
    var viaUser = Bool()
    var name = ""
    var user_Id = ""
    var objModel_Detail = CommunityDetailModel()
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        privateOnView.delegate = self
        privateOnView.isOn = false
        self.publicPrivate = false
        self.setUI()
    }
    
    @IBAction func tapOnViewLevel(_ sender: Any)
    {
        self.createAlert(message: "Under Develoment")
    }
    
    @IBAction func tapOnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnComments(_ sender: Any)
    {
        self.comment_view.backgroundColor = UIColor(hexString: "#00375F")
        self.comment_btn.setTitleColor(UIColor.white, for: .normal)
        self.discussion_view.backgroundColor = UIColor.white
        self.discuss_btn.setTitleColor(UIColor(hexString: "#00375F"), for: .normal)
        if (self.viaUser)
        {
            self.HitServiceMyProfile(flag: "2", user_id: CommonUtilities.shared.getuserdata()?.id ?? "")
        }
        else
        {
            self.HitServiceMyProfile(flag: "2", user_id: self.user_Id)
        }
    }
    
    @IBAction func tapOnMessage(_ sender: Any)
    {
        
    }
    
    @IBAction func tapOnDiscussion(_ sender: Any)
    {
        self.discussion_view.backgroundColor = UIColor(hexString: "#00375F")
        self.discuss_btn.setTitleColor(UIColor.white, for: .normal)
        self.comment_view.backgroundColor = UIColor.white
        self.comment_btn.setTitleColor(UIColor(hexString: "#00375F"), for: .normal)
        if (self.viaUser)
        {
            self.HitServiceMyProfile(flag: "1", user_id: CommonUtilities.shared.getuserdata()?.id ?? "")
        }
        else
        {
            self.HitServiceMyProfile(flag: "1", user_id: self.user_Id)
        }
    }
    func valueChanged(sender: SwiftySwitch) {
        if self.privateOnView.isOn == true{
            self.privateOnView.makeRoundCorner(8)
            self.privateOnView.layer.borderWidth = 1
            self.privateOnView.myColor = #colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)
            self.privateOnView.layer.borderColor = #colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)
            self.accountType.isHidden = false
            self.accountType.text = "Private"
            self.badge_lbl.isHidden = true
            self.badge_img.isHidden = true
            self.questionIcon.isHidden = false
            self.badge_lbl.isHidden = true
            self.badge_img.isHidden = true
            self.badge_img_constraint.constant = 0
            self.publicPrivate = false
            self.hitServiceChangeCommunityStatus(flag: 0, user_id: Int(CommonUtilities.shared.getuserdata()?.id ?? "-1")!)
            self.communityTable.reloadData()
        }else{
            self.privateOnView.makeRoundCorner(8)
            self.privateOnView.layer.borderWidth = 1
            self.privateOnView.myColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.privateOnView.layer.borderColor = #colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)
            self.badge_lbl.isHidden = true
            self.badge_img.isHidden = true
            self.accountType.isHidden = false
            self.questionIcon.isHidden = false
            self.accountType.text = "Public"
            self.badge_img_constraint.constant = 0
            self.publicPrivate = false
            self.hitServiceChangeCommunityStatus(flag: 1, user_id: Int(CommonUtilities.shared.getuserdata()?.id ?? "-1")!)
            self.communityTable.reloadData()
        }
    }
    
    func setUI()
    {
        self.badge_lbl.isHidden = true
        self.badge_img.isHidden = true
        self.messageView.makeRoundCorner(6)
        self.messageView.isHidden = true
        self.message_btn.isEnabled = false
        self.img_view.makeRounded()
        self.badge_img.makeRounded()
        self.headerView.dropShadow()
        self.badge_view.makeRounded()
        self.profile_pic.makeRounded()
        self.view_btn.makeRoundCorner(6)
        self.comment_btn.makeRoundCorner(4)
        self.discuss_btn.makeRoundCorner(4)
        self.comment_view.makeRoundCorner(8)
        self.comment_view.dropShadow()
        self.discussion_view.makeRoundCorner(8)
        self.discussion_view.dropShadow()
        self.communityTable.separatorColor = .clear
        self.accountType.isHidden = false
        self.questionIcon.isHidden = false
        self.discussion_view.backgroundColor = UIColor(hexString: "#00375F")
        self.discuss_btn.setTitleColor(UIColor.white, for: .normal)
        self.comment_view.backgroundColor = UIColor.white
        self.comment_btn.setTitleColor(UIColor(hexString: "#00375F"), for: .normal)
        self.badge_img_constraint.constant = 0
        self.privateOnView.makeRoundCorner(8)
        self.privateOnView.layer.borderWidth = 1
        self.privateOnView.myColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.privateOnView.layer.borderColor = #colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)
        if (self.viaUser)
        {
            self.expand_view.isHidden = false
            self.heightOfView.constant = 178.0
            self.name_lbl.text = CommonUtilities.shared.getuserdata()?.name
            self.HitServiceMyProfile(flag: "1", user_id: CommonUtilities.shared.getuserdata()?.id ?? "")
        }
        else
        {
            self.expand_view.isHidden = true
            self.heightOfView.constant = 0.0
            self.name_lbl.text = self.name
            self.HitServiceMyProfile(flag: "1", user_id: self.user_Id)
        }
    }
}

extension CommunityDetail:  UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //        return self.objModel_discussion.count
        return self.objModel_Detail.discussion.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommunityDetailCell", for: indexPath) as! CommunityDetailCell
        cell.selectionStyle = .none
        //        cell.nameLbl.text = self.objModel_discussion[indexPath.row].name
        cell.nameLbl.text = self.objModel_Detail.discussion[indexPath.row].name
        cell.question_lbl.text = self.objModel_Detail.discussion[indexPath.row].title
        if(self.objModel_Detail.discussion[indexPath.row].created_date == ""){
            cell.date_lbl.text = ""
        }else{
         cell.date_lbl.text = DateFormatter.shared().dateFromWebtoApp(self.objModel_Detail.discussion[indexPath.row].created_date)

        }
        cell.desc_lbl.text = self.objModel_Detail.discussion[indexPath.row].description
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return tblHeaderView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if(self.viaUser == false)
        {
            self.questionIcon.isHidden = true
            self.privateOnView.isHidden = true
            self.badge_lbl.isHidden = false
            self.badge_img.isHidden = false
            self.badge_img_constraint.constant = 30
            self.messageView.isHidden = false
            self.message_btn.isEnabled = true
            self.accountType.isHidden = true
            return 178
        }
        else if(publicPrivate == false)
        {
            return 326

//            if(publicPrivate == false){
//                return 326
//            }
            //else{
//                return 356
//            }
        }else{
            return 356
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return footer_view
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if(objModel_Detail.vendorTeam.count == 0){
            return 0
        }else{
            return 300

        }
    }
}





extension CommunityDetail
{
    func HitServiceMyProfile(flag: String,user_id:String)
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        let parameters = ["user_id":user_id,"flag":flag] as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "myCommunityForumDetails", parameters:parameters, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = dictionary?["data"] as? [String:Any]
                        {
                            if let discussion = data["discussion"] as? [[String:Any]]
                            {
                                self.objModel_Detail.discussion.removeAll()
                                for data in discussion
                                {
                                    let obj = Model_discussion()
                                    
                                    if let description = data["description"] as? String
                                    {
                                        obj.description = description
                                    }
                                    if let id = data["id"] as? Int
                                    {
                                        obj.id = String(id)
                                    }
                                    if let name = data["name"] as? String
                                    {
                                        obj.name = name
                                    }
                                    if let name = data["name"] as? String
                                    {
                                        obj.name = name
                                    }
                                    if let title = data["title"] as? String
                                    {
                                        obj.title = title
                                    }
                                    if let user_image = data["user_image"] as? String
                                    {
                                        obj.user_image = user_image
                                    }
                                    if let user_profile_status = data["user_profile_status"] as? Bool
                                    {
                                        obj.user_profile_status = user_profile_status
                                    }
                                    if let created_date = data["created_at"] as? String{
                                        obj.created_date = created_date
                                    }
                                    self.objModel_Detail.discussion.append(obj)
                                }
                                self.communityTable.reloadData()
                            }
                            if let vendorTeam = data["vendorTeam"] as? [[String:Any]]{
                                self.objModel_Detail.vendorTeam.removeAll()
                                for data in vendorTeam{
                                    print(data)
                                    let obj_vendor_team = VendorTeam()
                                    if let category_name = data["category_name"] as? String{
                                        obj_vendor_team.category_name = category_name
                                    }
                                    if let company_name = data["company_name"] as? String{
                                        obj_vendor_team.company_name = company_name
                                    }
                                    if let vendor_image = data["vendor_image"] as? String{
                                        obj_vendor_team.vendor_image = vendor_image
                                    }
                                    if let city_name = data["city_name"] as? String{
                                        obj_vendor_team.city_name = city_name
                                    }
                                    if let vendor_id = data["vendor_id"] as? Int{
                                        obj_vendor_team.vendor_id = vendor_id
                                    }
                                    if let country_name = data["country_name"] as? String{
                                        obj_vendor_team.country_name = country_name
                                    }
                                    if let vendorHire_id = data["vendorHire_id"] as? Int{
                                        obj_vendor_team.vendorHire_id = vendorHire_id
                                    }
                                    if let name = data["name"] as? String{
                                        obj_vendor_team.name = name
                                    }
                                    if let cat_id = data["cat_id"] as? Int{
                                        obj_vendor_team.cat_id = cat_id
                                    }
                                    if let user_id = data["user_id"] as? Int{
                                        obj_vendor_team.user_id = user_id
                                    }
                                    if let status = data["status"] as? Int{
                                        obj_vendor_team.status = status
                                    }
                                    self.objModel_Detail.vendorTeam.append(obj_vendor_team)
                                }
                                self.vendroCOl.reloadData()
                                self.communityTable.reloadData()
                            }
                        }
                    }
                    else{
                        
                    }
                }
            }
            
        }
    }
    
    
    func hitServiceChangeCommunityStatus(flag: Int,user_id:Int){
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        let parameters = ["user_id":user_id,"flag":flag] as [String : Int]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "changeCommunityStatus", parameters: parameters, encoding: JSONEncoding.default, headers: headers) { (sucess, response, error) in
            KRProgressHUD.dismiss()
            if(sucess)!{
                if let statusCode = response?["status"] as? NSNumber{
                    if(statusCode == 200){
                        if let data = response?["data"] as? [String:Any]{
                            print(data)
                        }
                    }else{
                        
                    }
                }
            }
        }
        
    }
    
}



extension CommunityDetail : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return objModel_Detail.vendorTeam.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VendorTeamColl", for: indexPath) as! VendorTeamColl
        cell.name_lbl.text = objModel_Detail.vendorTeam[indexPath.row].name
        cell.loc.text = "\(objModel_Detail.vendorTeam[indexPath.row].country_name), " + objModel_Detail.vendorTeam[indexPath.row].city_name
        cell.cat_lbl.text = objModel_Detail.vendorTeam[indexPath.row].category_name
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width = collectionView.frame.width
        return CGSize(width: width, height: 260)
    }
}

class CommunityDetailModel
{
    var discussion = [Model_discussion]()
    var vendorTeam = [VendorTeam]()
    
}

class Model_discussion
{
    var id = ""
    var name = ""
    var title = ""
    var user_image = ""
    var description = ""
    var user_profile_status = Bool()
    var created_date = ""
}

class VendorTeam
{
    var cat_id = Int()
    var category_name = ""
    var city_name = ""
    var company_name = ""
    var  country_name = ""
    var  name = ""
    var  status = Int()
    var  user_id = Int()
    var  vendorHire_id = Int()
    var   vendor_id = Int()
    var vendor_image = ""
}
