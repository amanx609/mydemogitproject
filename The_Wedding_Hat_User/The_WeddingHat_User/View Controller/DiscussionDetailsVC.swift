//
//  DiscussionDetailsVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 31/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD
import AlamofireImage

class DiscussionDetailsVC: UIViewController {
    
    @IBOutlet weak var add_btn: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var comment_view: UIView!
    @IBOutlet weak var commentTxt: UITextField!
    @IBOutlet weak var discussionTable: UITableView!
    @IBOutlet weak var totalComments: UILabel!
    
    var post_Id = ""
    var objModelCommunityDetail = ModelCommunityDetail()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.headerView.dropShadow()
        self.add_btn.makeRoundCorner(6)
        self.comment_view.makeRoundCorner(8)
        self.discussionTable.separatorColor = .clear
        self.HitServicegetDiscussionDetail(post_id: self.post_Id)
        self.commentTxt.colorbluePlaceHolder(placeholder: "Add your comment")
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.self.HitServicegetDiscussionDetail(post_id: self.post_Id)
    }
    
    @IBAction func tapOnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnAdd(_ sender: Any)
    {
        if self.commentTxt.text != ""
        {
            self.HitServicePostComment()
        }
    }
    
}

extension DiscussionDetailsVC : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.objModelCommunityDetail.post_comments.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (indexPath.row == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DiscussionPostCell", for: indexPath) as! DiscussionPostCell
            cell.selectionStyle = .none
            cell.name_lbl.text = self.objModelCommunityDetail.name
            cell.question_lbl.text = self.objModelCommunityDetail.title
            cell.post_lbl.text = self.objModelCommunityDetail.description
            cell.allCommentsNumber.text = "\(self.objModelCommunityDetail.post_comments.count)"
            
//            cell.allCommentsLbl.text = "All comments  (\(objModelCommunityDetail.post_comments.count))"
            if(self.objModelCommunityDetail.post_date == ""){
                cell.time_lbl.text = "25 Feb 2020"
            }else{
                cell.time_lbl.text = DateFormatter.shared().convertNewDateFormater(self.objModelCommunityDetail.post_date)
//                        dateFromWebtoApp(self.objModelCommunityDetail.post_date)
                
            }
            
            if self.objModelCommunityDetail.community_images.count != 0
            {
                if let img_url = URL(string: self.objModelCommunityDetail.community_images[indexPath.row].community_image)
                {
                    cell.post_img.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "ss"))
                }
            }
            if let img_url = URL(string: self.objModelCommunityDetail.user_image)
            {
                cell.profile_pic.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "ss"))
            }
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
            cell.selectionStyle = .none
            cell.name_lbl.text = self.objModelCommunityDetail.post_comments[indexPath.row - 1].name
            cell.post_lbl.text = self.objModelCommunityDetail.post_comments[indexPath.row - 1].discussion
            if( self.objModelCommunityDetail.post_comments[indexPath.row - 1].post_date == ""){
                cell.time_lbl.text = ""
            }else{
                if let date = self.objModelCommunityDetail.post_comments[indexPath.row - 1].post_date as? Any{
                    cell.time_lbl.text = DateFormatter.shared().convertNewDateFormater(date as! String)
                    print(date)
                }
            }
            
            if let img_url = URL(string: self.objModelCommunityDetail.post_comments[indexPath.row - 1].user_image)
            {
                cell.profile_pic.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "ss"))
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
            return UITableView.automaticDimension
    }
    
}

extension DiscussionDetailsVC
{
    func HitServicegetDiscussionDetail(post_id:String)
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        let parameters = ["community_id" : post_id] as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "getDiscussionDetail", parameters:parameters, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!{
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = dictionary?["data"] as? [String:Any]
                        {
                            let obj = ModelCommunityDetail()
                            
                            if let id = data["id"] as? Int
                            {
                                obj.id = String(id)
                            }
                            if let name = data["name"] as? String
                            {
                                obj.name = name
                            }
                            if let post_date = data["post_date"] as? String
                            {
                                obj.post_date = post_date
                            }
                            if let description = data["description"] as? String
                            {
                                obj.description = description
                            }
                            if let title = data["title"] as? String
                            {
                                obj.title = title
                            }
                            if let user_image = data["user_image"] as? String
                            {
                                obj.user_image = user_image
                            }
                            if let community_images = data["community_images"] as? [[String:Any]]
                            {
                                obj.community_images.removeAll()
                                for images in community_images
                                {
                                    let img = community_Images()
                                    if let community_id = images["community_id"] as? Int
                                    {
                                        img.community_id = String(community_id)
                                    }
                                    if let community_image = images["community_image"] as? String
                                    {
                                        img.community_image = community_image
                                    }
                                    obj.community_images.append(img)
                                }
                            }
                            if let post_comments = data["post_comments"] as? [[String:Any]]
                            {
                                obj.post_comments.removeAll()
                                for images in post_comments
                                {
                                    let post = Modelpost_comments()
                                    if let id = images["id"] as? Int
                                    {
                                        post.id = String(id)
                                    }
                                    if let name = images["name"] as? String
                                    {
                                        post.name = name
                                    }
                                    if let discussion = images["discussion"] as? String
                                    {
                                        post.discussion = discussion
                                    }
                                    if let post_date = images["post_date"] as? String
                                    {
                                        post.post_date = post_date
                                    }
                                    if let user_image = images["user_image"] as? String
                                    {
                                        post.user_image = user_image
                                    }
                                    obj.post_comments.append(post)
                                }
                            }
                            self.objModelCommunityDetail = obj
                            self.discussionTable.reloadData()
                        }
                    }
                    else
                    {
                        if let msg = dictionary?["message"] as? String
                        {
                            self.createAlert(message: msg)
                        }
                    }
                }
            }
            
        }
    }
    
    func HitServicePostComment()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        let parameters = [  "community_id":self.post_Id,"user_id":CommonUtilities.shared.getuserdata()?.id ?? "","discussion":self.commentTxt.text! , "post_date":"2020-08-12"
] as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "commentOnDiscussion", parameters:parameters, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        self.HitServicegetDiscussionDetail(post_id: self.post_Id)
                    }
                    else
                    {
                        if let msg = dictionary?["message"] as? String
                        {
                            self.createAlert(message: msg)
                        }
                    }
                }
            }
            
        }
    }
}

class ModelCommunityDetail
{
    var id = ""
    var description = ""
    var name = ""
    var post_date = ""
    var title = ""
    var user_id = ""
    var user_image = ""
    var post_comments = [Modelpost_comments]()
    var community_images = [community_Images]()
    
    
}

class Modelpost_comments
{
    var id = ""
    var name = ""
    var user_image = ""
    var post_date = ""
    var discussion = ""
    var commented_by = ""
}

class community_Images
{
    var community_id = ""
    var community_image = ""
    var user_id = ""
}
