//
//  DressVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 06/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

class DressVC: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var header_lbl: UILabel!
    @IBOutlet weak var dressCollection: UICollectionView!
    
    var vendorID = ""
    var objDressModel = [DressModel]()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.headerView.dropShadow()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.hitservicerforgetProduct(vendorID: self.vendorID)
    }
    
    @IBAction func tapOnBAck(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension DressVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.objDressModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DressCell", for: indexPath) as! DressCell
        
        cell.dress_name.text = self.objDressModel[indexPath.row].dress_name
        
        cell.amount_lbl.text = "$\(self.objDressModel[indexPath.row].price["low_price"] as? String ?? "") - $\(self.objDressModel[indexPath.row].price["high_price"] as? String ?? "")"
       
        if self.objDressModel[indexPath.row].dress_image.count > 0
        {
            if let img_url = URL(string: self.objDressModel[indexPath.row].dress_image[0].dress_image)
            {
                cell.dressImg.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "add_images"))
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 160.0, height: 205.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        var productImage = ""
        if self.objDressModel[indexPath.row].dress_image.count > 0
        {
            productImage = self.objDressModel[indexPath.row].dress_image[0].dress_image
        }
        let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DressDetailVC") as! DressDetailVC
        vc.product_id = self.objDressModel[indexPath.row].id
        vc.product_image.append(productImage)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension DressVC
{
    func hitservicerforgetProduct(vendorID:String)
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        let headers: HTTPHeaders =
            [
                "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
                "Content-Type": "application/json"
        ]
        ServiceManager.instance.request(method: .get, URLString: "getProduct/"+vendorID, parameters: nil, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = dictionary?["data"] as? [String:Any]
                        {
                            if let inventoryList = data["inventoryList"] as? [[String:Any]]
                            {
                                self.objDressModel.removeAll()
                                for inventory in inventoryList
                                {
                                    let obj = DressModel()
                                    
                                    obj.id = inventory.valueForString(key: "id")
                                    obj.dress_name = inventory.valueForString(key: "dress_name")
                                    obj.price_range_id = inventory.valueForString(key: "price_range_id")
                                    
                                    
                                    if let price = inventory["price"] as? [String:Any]
                                    {
                                        obj.price = price
                                    }
                                    if let dress_image = inventory["dress_image"] as? [[String:Any]]
                                    {
                                        obj.dress_image.removeAll()
                                        
                                        for images in dress_image
                                        {
                                            let img = ImageModel()
                                            
                                            img.dress_image = images.valueForString(key: "dress_image")
                                            img.id = images.valueForString(key: "id")
                                            img.inventory_id = images.valueForString(key: "inventory_id")
                                            obj.dress_image.append(img)
                                        }
                                    }
                                    self.objDressModel.append(obj)
                                }
                                self.dressCollection.reloadData()
                            }
                        }
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                        
                    }
                }
            }
        }
    }
}


class DressModel
{
    var id = ""
    var dress_image = [ImageModel]()
    var dress_name = ""
    var price = [String:Any]()
    var price_range_id = ""
}

class ImageModel
{
    var id = ""
    var dress_image = ""
    var inventory_id = ""
}
