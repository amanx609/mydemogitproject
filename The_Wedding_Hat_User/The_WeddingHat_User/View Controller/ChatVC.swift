//
//  ChatVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 12/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class ChatVC: UIViewController {

    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var msgView: UIView!
    @IBOutlet weak var msgTxt: UITextField!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerlbl: UILabel!
    @IBOutlet weak var chatTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerView.dropShadow()
    }
    @IBAction func tapOnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func tapOnAttachment(_ sender: Any) {
    }
    @IBAction func tapOnSend(_ sender: Any) {
    }
    
}
extension ChatVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let revived_cell = tableView.dequeueReusableCell(withIdentifier: "RecivedCell", for: indexPath) as! RecivedCell
            revived_cell.selectionStyle = .none
            return revived_cell
        }
        else if indexPath.row == 1{
            let send_cell = tableView.dequeueReusableCell(withIdentifier: "SendCell", for: indexPath) as! SendCell
            send_cell.selectionStyle = .none
            return send_cell
        }else if indexPath.row == 2{
            let recivedMedia_cell = tableView.dequeueReusableCell(withIdentifier: "RecivedMediaCell", for: indexPath) as! RecivedMediaCell
            recivedMedia_cell.selectionStyle = .none
            return recivedMedia_cell
        }
        else{
            let sendMedia_cell = tableView.dequeueReusableCell(withIdentifier: "SendMediaCell", for: indexPath) as! SendMediaCell
                       sendMedia_cell.selectionStyle = .none
                       return sendMedia_cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}



