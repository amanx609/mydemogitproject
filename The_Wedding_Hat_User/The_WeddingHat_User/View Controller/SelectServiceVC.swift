//
//  SelectServiceVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 11/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD


protocol dataPopulated {
    func datapassing(cat_name:String,cat_id:String)
}
class SelectServiceVC: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var serviceColl: UICollectionView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    
    var vaiDiscussion = false
    var searchArr = [Category]()
    var categoryList = [Category]()
    var delegate: dataPopulated?
    var dropName = [String]()
    var shouldShowSearchResults = false
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.txtSearch.delegate = self
        self.headerView.dropShadow()
        self.searchView.makeRoundCorner(8)
        self.searchView.dropShadow()
        self.innerView.makeRoundCorner(8)
        self.innerView.dropShadow()
        self.hitServiceforGetCategory()
        if vaiDiscussion == true
        {
            backBtn.isHidden = false
        }
        else
        {
            backBtn.isHidden = true
        }
    }
    
    @IBAction func tapOnBack(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    

     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
       {
           if txtSearch.text! != ""
           {
               self.searchArr.removeAll()
               for i in categoryList
               {
                if (i.category_name).contains(txtSearch.text!)
                   {
                       self.searchArr.append(i)
                   }
               }
           }
           else
           {
               self.searchArr = self.categoryList
           }
           self.serviceColl.reloadData()
           return true
       }
       
       func textFieldDidEndEditing(_ textField: UITextField)
       {
           if txtSearch.text! != ""
           {
               self.searchArr.removeAll()
               for i in categoryList
               {
                   if (i.category_name).contains(txtSearch.text!)
                   {
                       self.searchArr.append(i)
                   }
               }
           }
           else
           {
               self.searchArr = self.categoryList
           }
           self.serviceColl.reloadData()
       }
}

extension SelectServiceVC : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
//        return shouldShowSearchResults == true ? searchArr.count : categoryList.count
//        return categoryList.count
        return searchArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelelctServiceColl", for: indexPath) as! SelelctServiceColl
//        if(shouldShowSearchResults == true)
 //       {
            cell.cat_lbl.text = searchArr[indexPath.row].category_name
                   self.dropName.append(categoryList[indexPath.row].category_name)

   //     }
//        else
//        {
//          cell.cat_lbl.text = categoryList[indexPath.row].category_name
//                 self.dropName.append(categoryList[indexPath.row].category_name)
//        }
//        cell.cat_lbl.text = categoryList[indexPath.row].category_name
//        self.dropName.append(categoryList[indexPath.row].category_name)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width = collectionView.frame.width
        if(UIScreen.main.bounds.width > 375)
        {
            return CGSize(width: (width / 2) - 20, height: 145)
        }
        return CGSize(width: (width / 2) - 20, height: 135)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if self.vaiDiscussion == true
        {
            self.delegate?.datapassing(cat_name: categoryList[indexPath.row].category_name, cat_id: categoryList[indexPath.row].category_id)
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "VendorListVC") as! VendorListVC
            vc.category_id = categoryList[indexPath.row].category_id
            vc.category_name = categoryList[indexPath.row].category_name
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}

extension SelectServiceVC
{
    func hitServiceforGetCategory()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let parameters = [
            "search" : ""
        ]
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "getCategory", parameters: parameters, encoding: JSONEncoding.default, headers: headers) { (sucess, dictionary, error) in
            if (sucess)!
            {
                KRProgressHUD.dismiss()
                if let statuscode = dictionary!["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let categorydata = dictionary!["data"]!["category"] as? [Dictionary<String,AnyObject>]
                        {
                            for category_item in categorydata
                            {
                                let cat_detail = Category()
                                if let category_id = category_item["id"] as? Int
                                {
                                    cat_detail.category_id = "\(category_id)"
                                    
                                }
                                if let category_name = category_item["category_name"] as? String
                                {
                                    cat_detail.category_name = category_name
                                    self.dropName.append(category_name)
                                }
                                if let category_subcat = category_item["sub_category"] as? [[String:Any]]
                                {
                                    for category_subcat_item in category_subcat
                                    {
                                        let sub_cat_detail = Subcategory()
                                        if let subcategory_id = category_subcat_item["id"] as? String
                                        {
                                            sub_cat_detail.subcategory_id = subcategory_id
                                        }
                                        if let subcategory_name = category_subcat_item["category_name"] as? String
                                        {
                                            sub_cat_detail.subcategory_name = subcategory_name
                                        }
                                        cat_detail.subcategory_list.append(sub_cat_detail)
                                    }
                                }
                                self.categoryList.append(cat_detail)
                                self.searchArr.append(cat_detail)
                            }
                            self.serviceColl.reloadData()
                        }
                    }
                    else
                    {
                        let message = dictionary!["message"] as? String
                        self.createAlert(message: message!)
                    }
                }
            }
        }
    }
    
}
