//
//  MessagesVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 12/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class MessagesVC: UIViewController {

    @IBOutlet weak var headerlbl: UILabel!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var messagesTable: UITableView!
    @IBOutlet weak var selectedVendorView: UIView!
    @IBOutlet weak var selectedPartyView: UIView!
    @IBOutlet weak var selectedCommunityView: UIView!
    @IBOutlet weak var unselectedVendorView: UIView!
    @IBOutlet weak var unselectedPartyView: UIView!
    @IBOutlet weak var unselectedCommunityView: UIView!
    @IBOutlet weak var vendorScreen: UIView!
    @IBOutlet weak var partyScreen: UIView!
    @IBOutlet weak var newNotificationLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerView.dropShadow()
        self.searchView.makeRoundCorner(8)
        self.searchView.dropShadow()
        self.messagesTable.separatorColor = .clear
        designSetup()
    }
 @IBAction func tapOnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension MessagesVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessagesCell", for: indexPath) as! MessagesCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    
}

extension MessagesVC{
    @IBAction func vendorButtonTap(_ sender: Any)
    {
        if selectedVendorView.isHidden == true {
            selectedVendorView.isHidden = false
            newNotificationLabel.isHidden = true
            selectedCommunityView.isHidden = true
            selectedPartyView.isHidden = true

            vendorScreen.isHidden = false
            unselectedVendorView.isHidden = true
            unselectedPartyView.isHidden = false
            unselectedCommunityView.isHidden = false
        }else{
            selectedVendorView.isHidden = true
            unselectedVendorView.isHidden = false
            vendorScreen.isHidden = true
            newNotificationLabel.isHidden = false
            

            
        }
        print("vendor")

    }
    
    @IBAction func partyButtonTap(_ sender: Any)
    {
        if selectedPartyView.isHidden == true
        {
            selectedVendorView.isHidden = true
            selectedCommunityView.isHidden = true
            selectedPartyView.isHidden = false
            vendorScreen.isHidden = false
            unselectedVendorView.isHidden = false
            unselectedPartyView.isHidden = true
            unselectedCommunityView.isHidden = false
        }
        else
        {
            selectedPartyView.isHidden = true
            unselectedPartyView.isHidden = false
            vendorScreen.isHidden = true
        }
        print("partyButtonTap")
        
    }
    
    @IBAction func communityButtonTap(_ sender: Any) {
        if selectedCommunityView.isHidden == true{
            selectedCommunityView.isHidden = false
             selectedVendorView.isHidden = true
            selectedPartyView.isHidden = true
                       
                       unselectedVendorView.isHidden = false
                       unselectedPartyView.isHidden = false
                       unselectedCommunityView.isHidden = true
                      self.messagesTable.isHidden = false
            self.vendorScreen.isHidden = true
        }else{
            selectedCommunityView.isHidden = true
            unselectedCommunityView.isHidden = false
            self.messagesTable.isHidden = true
        }
        print("communityButtonTap")
        
    }
    
    
    
    
    func designSetup(){

        self.messagesTable.isHidden = true
        self.vendorScreen.isHidden = true
        self.partyScreen.isHidden = true
        self.selectedPartyView.isHidden = true
        self.selectedVendorView.isHidden = true
        self.selectedCommunityView.isHidden = true
        self.unselectedVendorView.makeRoundCorner(6.0)
        self.unselectedVendorView.dropShadow()
        self.unselectedPartyView.makeRoundCorner(5.0)
        self.unselectedPartyView.dropShadow()
        self.unselectedCommunityView.makeRoundCorner(5.0)
        self.unselectedCommunityView.dropShadow()
        self.selectedVendorView.makeRoundCorner(5.0)
        self.selectedVendorView.dropShadow()
        self.selectedCommunityView.makeRoundCorner(5.0)
        self.selectedCommunityView.dropShadow()
        self.selectedPartyView.makeRoundCorner(5.0)
        self.selectedPartyView.dropShadow()
    }
    
}


