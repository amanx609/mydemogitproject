//
//  SplashVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 15/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5, execute: {
            if CommonUtilities.shared.getuserdata()?.id == "" || CommonUtilities.shared.getuserdata()?.id == nil
            {
                CommonUtilities.shared.setupinitialView()
            }
            else
            {
                CommonUtilities.shared.sidemenu()
                CommonUtilities.shared.hitServicegetEventType()
            }
        })
    }
}
