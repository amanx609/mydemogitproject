//
//  BudgeterCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 04/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class BudgeterCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var budgetImg: UIImageView!
    @IBOutlet weak var budgetlbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var pointsLbl: UILabel!
    @IBOutlet weak var totalPoints: UILabel!
    @IBOutlet weak var maindescLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainView.makeRoundCorner(10)
        self.mainView.dropShadow()
        self.budgetImg.makeRounded()
        self.budgetlbl.text = Constant.string.budgeter.localized()
        self.descLbl.text = Constant.string.spent.localized()
        self.maindescLbl.text = Constant.string.manageExpense.localized()
    }
    
}
