//
//  AddDiscussionVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 13/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import KRProgressHUD
import YangMingShan
import OpalImagePicker
import Alamofire
import KRProgressHUD
import AlamofireImage
import DropDown

protocol CommunityFormHitApi{
    func hitAPiCommunityForum()
}


class AddDiscussionVC: UIViewController,dataPopulated, UITextViewDelegate {
    
    @IBOutlet weak var allServicesView: UIView!
    @IBOutlet weak var discussionView: UIView!
    @IBOutlet weak var cancel_btn: UIButton!
    @IBOutlet weak var category_lbl: UILabel!
    @IBOutlet weak var titleTxt: UITextField!
    @IBOutlet weak var descTxt: UITextView!
    @IBOutlet weak var catTxt: UITextField!
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var uploadImageCollection: UICollectionView!
    @IBOutlet weak var uploadImageButton: UIButton!
    @IBOutlet weak var heightOfCollection: NSLayoutConstraint!
    var communityApiDelegate : CommunityFormHitApi?
    var categoryName = [String]()
    var catArray = [[String:Any]]()
    let dropdown = DropDown()
    var dropDownListData = [String]()
    var objDropData = [DropData]()
    var catId = ""
    var documentsPicArray = [UIImage]()
    var selectedServiceId = 0
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.hitServiceforGetCategory()
        self.setupUI()
        self.descTxt.delegate = self
        self.descTxt.text = "Enter the description"
        self.descTxt.textColor = UIColor(hexString: "#00375F")?.withAlphaComponent(1.0)
    }
    
    func setupUI()
    {
        if(self.documentsPicArray.count == 0){
         self.heightOfCollection.constant = 0
        }
        
        self.cancel_btn.addDashBorder(color: UIColor(hexString: "#00375F")!)
        self.titleTxt.colorPlaceHolder(placeholder:"Add title", color: "#00375F")
        self.catTxt.addRightImageAsRightView(strImgName: "down-icon", rightPadding: 10)
        self.headView.dropShadowBlueColor()
        if(documentsPicArray.count == 5){
            uploadImageButton.isHidden = true
        }else{
            uploadImageButton.isHidden = false
        }
        
    }
    
    @IBAction func tapOncancel(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnSend(_ sender: Any)
    {
        if self.catId == ""
        {
            self.createAlert(message: "Please select category")
            return
        }
        if self.titleTxt.text == ""
        {
            self.createAlert(message: "Enter the title")
            return
        }
        self.hitServiceAddDiscussion()
    }
    
    @IBAction func tapOnCategory(_ sender: Any)
    {
//        let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "SelectServiceVC") as! SelectServiceVC
//        vc.delegate = self
//        vc.vaiDiscussion = true
//
//        self.categoryName = vc.dropName
        
        self.setServicesList()
        
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: true, completion: nil)
    }
    
    func datapassing(cat_name:String,cat_id:String)
    {
        self.catId = cat_id
        self.catTxt.text = cat_name
        
        
    }
    
    
    @IBAction func uploadImageButtonTap(_ sender: Any) {
        
    }
    
    @IBAction func backButtonTap(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func openCamera(_ sender : UIButton){
        if(documentsPicArray.count < 6){
         self.chooseImage()
        }else{
        createAlert(message: "Maximum 5 photos can be taken")
        }
    }
    
        func setServicesList()
        {
            dropdown.textColor = UIColor(hexString: "#00375F")!
            dropdown.dataSource = self.dropDownListData
            dropdown.anchorView = allServicesView
            dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.catTxt.text = item
                self.catId = "\(self.objDropData[index].Id)"
                
            }
            dropdown.animationduration = 0.20
            dropdown.show()
        }

    
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
//        if descTxt.text == UIColor(hexString: "#00375F")?.withAlphaComponent(0.5)
//        {
//            descTxt.text = nil
//            descTxt.textColor = UIColor(hexString: "#00375F")
//        }
        if descTxt.text == "Enter the description" {
            descTxt.text = nil
            descTxt.textColor = UIColor(hexString: "#00375F")
        }
    }
    func textViewDidChange(_ textView: UITextView)
    {
        print(descTxt.text!)
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if descTxt.text.isEmpty
        {
            descTxt.text = "Enter the description"
            descTxt.textColor = UIColor(hexString: "#00375F")
//                ?.withAlphaComponent(0.5)
        }
    }
    
}

extension AddDiscussionVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if(documentsPicArray.count != 0)
        {
            return documentsPicArray.count
        }
        else
        {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddDiscussionUploadImageCollCell", for: indexPath) as! AddDiscussionUploadImageCollCell
        if(documentsPicArray.count != 0)
        {
            if(documentsPicArray.count != 5){
                self.uploadImageButton.isHidden = false
            }else{
                self.uploadImageButton.isHidden = true

            }
            cell.imageView.image = documentsPicArray[indexPath.row]
        }
//        if indexPath.row == 0
//        {
//            if(cell.imageView.image != nil)
//            {
//                cell.crossButton.isHidden = false
//            }
//            cell.imageView.image = #imageLiteral(resourceName: "add_images")
//            cell.crossButton.isHidden = true
//        }
//        else
//        {
//            cell.crossButton.isHidden = false
//        }
        
        cell.crossButton.isHidden = false
//        cell.uploadButton.addTarget(self, action: #selector(uploadImage), for: .touchUpInside)
        cell.crossButton.addTarget(self, action: #selector(deleteButton(_:)), for: .touchUpInside)
        cell.crossButton.tag = indexPath.row
        
        return cell 
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cell_width = ((collectionView.frame.width) / 3 - 10)
        if(UIScreen.main.bounds.width > 375){
            if(documentsPicArray.count == 0){
                self.heightOfCollection.constant = 0
                return CGSize.zero
            }else if(documentsPicArray.count <= 3){
                self.heightOfCollection.constant = 90
                return CGSize(width: cell_width, height: 80)
            }else{
                self.heightOfCollection.constant = 180
                return CGSize(width: cell_width, height: 80)
            }
        }else{
            if(documentsPicArray.count == 0){
                self.heightOfCollection.constant = 0
                return CGSize.zero
            }else if(documentsPicArray.count <= 3){
                self.heightOfCollection.constant = 65
                return CGSize(width: cell_width, height: 55)
            }else{
                self.heightOfCollection.constant = 130
                return CGSize(width: cell_width, height: 55)
            }
        }
    }
    
    @objc func uploadImage()
    {
        self.chooseImage()
    }
    
    
    @objc func deleteButton(_ sender : UIButton){
        let deleteimg = sender.convert(CGPoint.zero, to: uploadImageCollection)
        guard let indexpath = uploadImageCollection.indexPathForItem(at: deleteimg)else{
            return
        }
        documentsPicArray.remove(at: indexpath.item)
        if(documentsPicArray.count == 0){
            self.heightOfCollection.constant = 0
        }
        uploadImageCollection.reloadData()
    }
    
    //    @objc func tapOnDeletePic(sender: UIButton)
    //    {
    //        //        let cell = sender.superview?.superview?.superview?.superview
    //        //        let indexPath1 :IndexPath = self.uploadImageCollection.indexPath(for: cell as! UICollectionViewCell)!
    
    //        let indexPath = IndexPath.init(row:indx, section: 0)
    //        let cell = self.uploadImageCollection.cellForItem(at: indexPath) as! AddDiscussionUploadImageCollCell
    //        documentsPicArray.remove(at: indexPath.row)
    //        self.uploadImageCollection.reloadData()
    //    }
}

extension AddDiscussionVC : OpalImagePickerControllerDelegate
{
    func chooseImage()
    {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        let actionSheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                imagePickerController.sourceType = .camera
                self.viewController?.present(imagePickerController, animated: true, completion: nil)
            }
            else
            {
                let alertController = UIAlertController(title: Constant.string.appname.localized(), message:
                    "Camera not avilable" , preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: Constant.string.ok.localized(), style: .default)); self.viewController?.present(alertController, animated: true, completion: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title:"Photo Album", style: .default, handler: {(action:UIAlertAction)in
            let imagePicker = OpalImagePickerController()
            let configuration = OpalImagePickerConfiguration()
            configuration.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select more than 5 images!", comment: "")
            imagePicker.configuration = configuration
            imagePicker.imagePickerDelegate = self
            imagePicker.maximumSelectionsAllowed = 5 - self.documentsPicArray.count
            imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
            imagePicker.modalPresentationStyle = .fullScreen
            self.viewController?.presentOpalImagePickerController(imagePicker, animated: true,
                                                                  select: { (assets) in
                                                                    //Select Assets
                                                                    print("This is assests\(assets)")
                                                                    self.viewController?.dismiss(animated: true, completion: nil)
                                                                    self.documentsPicArray.append(contentsOf: self.getAssetThumbnail(assets: assets))
                                                                    self.uploadImageCollection.reloadData()
            }, cancel: {
            })
        }))
        actionSheet.addAction(UIAlertAction(title: Constant.string.cancel.localized(), style: .cancel, handler: nil))
        self.viewController?.present(actionSheet,animated: true,completion: nil)
    }
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        documentsPicArray.append(image)
        self.uploadImageCollection.reloadData()
        picker.dismiss(animated: true, completion: nil)
        
    }
    override func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    func getAssetThumbnail(assets: [PHAsset]) -> [UIImage] {
        var arrayOfImages = [UIImage]()
        for asset in assets {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.version = .original
            option.isSynchronous = true
            manager.requestImageData(for: asset, options: option) { data, _, _, _ in
                if let data = data {
                    arrayOfImages.append(UIImage(data: data)!)
                }
            }
        }
        return arrayOfImages
    }
}



extension AddDiscussionVC
{
    func hitServiceAddDiscussion()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        let parameters = ["user_id":CommonUtilities.shared.getuserdata()?.id as Any,
                          "title":self.titleTxt.text!,
                          "description":self.descTxt.text!,
                          "cat_id":self.catId,
            ] as [String : Any]
        print(parameters)
        Alamofire.upload(multipartFormData: { multipartFormData in
            for i in 0..<self.documentsPicArray.count
            {
                let imageData1 = self.documentsPicArray[i].jpegData(compressionQuality: 0.5)!
                multipartFormData.append(imageData1, withName: "community_images[]", fileName: "\(i)image.jpg", mimeType: "image/jpeg")
                print("success");
            }
            
            for (key, value) in parameters
            {
                multipartFormData.append(((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!), withName: key)
            }
        }, to: BASEURL + "addDiscussion", headers: headers,
           encodingCompletion: { encodingResult in
            switch encodingResult
            {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    KRProgressHUD.dismiss()
                    let result = response.result.value
                    let responseDic = result as! [String : Any]
                    print(responseDic)
                    let msg = responseDic["message"] as! String
                    if let statuscode = responseDic["status"] as? NSNumber{
                        if statuscode == 200
                        {
                            self.communityApiDelegate?.hitAPiCommunityForum()
                            self.navigationController?.popViewController(animated: true)
                        }
                        else
                        {
                            self.createAlert(message: msg)
                        }
                    }
                    print("success uload");
                }
            case .failure(let error):
                print(error)
                KRProgressHUD.dismiss()
            }
        })
    }
    
    
    func hitServiceforGetCategory()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let parameters = [
            "search" : ""
        ]
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "getCategory", parameters: parameters, encoding: JSONEncoding.default, headers: headers) { (sucess, dictionary, error) in
            if (sucess)!
            {
                KRProgressHUD.dismiss()
                if let statuscode = dictionary!["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let categorydata = dictionary!["data"]!["category"] as? [Dictionary<String,AnyObject>]
                        {
                            for category_item in categorydata
                            {
                                let cat_detail = DropData()
                                if let category_id = category_item["id"] as? Int
                                {
                                    cat_detail.Id = category_id
                                    
                                }
                                if let category_name = category_item["category_name"] as? String
                                {
                                    cat_detail.name = category_name
                                    self.dropDownListData.append(category_name)
                                }
                                if let category_subcat = category_item["sub_category"] as? [[String:Any]]
                                {
                                    for category_subcat_item in category_subcat
                                    {
                                        let sub_cat_detail = Subcategory()
                                        if let subcategory_id = category_subcat_item["id"] as? String
                                        {
                                            sub_cat_detail.subcategory_id = subcategory_id
                                        }
                                        if let subcategory_name = category_subcat_item["category_name"] as? String
                                        {
                                            sub_cat_detail.subcategory_name = subcategory_name
                                        }
                                    }
                                }
                                
                                self.objDropData.append(cat_detail)

                            }
                            self.dropdown.dataSource = self.dropDownListData
                            }
//                            self.serviceColl.reloadData()
                        
                    }
                    else
                    {
                        let message = dictionary!["message"] as? String
                        self.createAlert(message: message!)
                    }
                }
            }
        }
    }
}


class DropData{
    var name = ""
    var Id = 0
}
