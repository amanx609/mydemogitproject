//
//  CategoryVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 21/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD


class CategoryVC: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var categoryHeaderLbl: UILabel!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var innerSearchView: UIView!
    @IBOutlet weak var categoryTable: UITableView!
    
    var categoryList = [Category]()
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.headerView.dropShadow()
        self.searchView.makeRoundCorner(8)
        self.searchView.dropShadow()
        self.innerSearchView.makeRoundCorner(8)
        self.innerSearchView.dropShadow()
        self.categoryTable.separatorColor = .clear
        self.hitServiceforGetCategory()
    }

    @IBAction func tapOnback(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
}

extension CategoryVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return categoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Category_Cell", for: indexPath) as! Category_Cell
        cell.category_lbl.text = categoryList[indexPath.row].category_name
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VendorListVC") as! VendorListVC
        vc.category_id = categoryList[indexPath.row].category_id
        vc.category_name = categoryList[indexPath.row].category_name
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension CategoryVC
{
    func hitServiceforGetCategory()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let parameters = [
            "search" : ""
        ]
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "getCategory", parameters: parameters, encoding: JSONEncoding.default, headers: headers) { (sucess, dictionary, error) in
            if (sucess)!
            {
                KRProgressHUD.dismiss()
                if let statuscode = dictionary!["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let categorydata = dictionary!["data"]!["category"] as? [Dictionary<String,AnyObject>]
                        {
                            for category_item in categorydata
                            {
                                let cat_detail = Category()
                                if let category_id = category_item["id"] as? Int
                                {
                                    cat_detail.category_id = "\(category_id)"
                                }
                                if let category_name = category_item["category_name"] as? String
                                {
                                    cat_detail.category_name = category_name
                                }
                                if let category_subcat = category_item["sub_category"] as? [[String:Any]]
                                {
                                    for category_subcat_item in category_subcat
                                    {
                                        let sub_cat_detail = Subcategory()
                                        if let subcategory_id = category_subcat_item["id"] as? String
                                        {
                                            sub_cat_detail.subcategory_id = subcategory_id
                                        }
                                        if let subcategory_name = category_subcat_item["category_name"] as? String
                                        {
                                            sub_cat_detail.subcategory_name = subcategory_name
                                        }
                                        cat_detail.subcategory_list.append(sub_cat_detail)
                                    }
                                }
                                self.categoryList.append(cat_detail)
                            }
                            self.categoryTable.reloadData()
                        }
                    }
                    else
                    {
                        let message = dictionary!["message"] as? String
                        self.createAlert(message: message!)
                    }
                }
            }
        }
    }
    
}


class Category
{
    var category_id = ""
    var category_name = ""
    var subcategory_list = [Subcategory]()
}

class Subcategory
{
    var subcategory_id = ""
    var subcategory_name = ""

}


