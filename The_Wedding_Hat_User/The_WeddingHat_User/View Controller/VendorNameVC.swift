//
//  VendorNameVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 08/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class VendorNameVC: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var faqButton: UIButton!
    @IBOutlet weak var vendorNameLbl: UILabel!
    @IBOutlet weak var featuredView: UIView!
    @IBOutlet weak var featuredlbl: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var topCollectionView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    var tableData  = [Model]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        tableData.append(Model.init(heading: "About", insideHeading: ["Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."]))
        tableData.append(Model.init(heading: "Social Links", insideHeading: ["facebook", "instagram"]))
        tableData.append(Model.init(heading: "Services Offered", insideHeading: ["There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary"]))
        tableData.append(Model.init(heading: "Amenities & Capacities", insideHeading: ["Amenities"]))
        tableData.append(Model.init(heading: "Pricing", insideHeading: ["20"]))
        tableData.append(Model.init(heading: "Deal & Discount", insideHeading: ["There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary"]))
        tblView.tableHeaderView = headerView
    }
    
    

 

}

extension VendorNameVC : UITableViewDelegate , UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableData.count == 0{
            return 0
        }else{
            return tableData.count
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData[section].insideHeading?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableData.count == 0{
            return UITableViewCell()
        }else{
            if indexPath.section == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AboutCell", for: indexPath) as! AboutCell
                cell.contentLabel.textColor = UIColor(hexString: "#00375F")
                cell.contentLabel.text = tableData[indexPath.section].insideHeading?[indexPath.row]
                return cell
            }else if indexPath.section == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SocialLinkCell", for: indexPath) as! SocialLinkCell
                cell.socialName.textColor = UIColor(hexString: "#00375F")
                cell.socialName.text = tableData[indexPath.section].insideHeading?[indexPath.row]
                cell.linkImages.image = #imageLiteral(resourceName: "noun_Ring_2231407")
                return cell

            }else if indexPath.section == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesOfferedCell", for: indexPath) as! ServicesOfferedCell
                cell.contentLabel.textColor = UIColor(hexString: "#00375F")
                cell.contentLabel.text = tableData[indexPath.section].insideHeading?[indexPath.row]
                return cell

            }else if indexPath.section == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AmenitiesCell", for: indexPath) as! AmenitiesCell
                               cell.label.textColor = UIColor(hexString: "#00375F")
                               cell.label.text = tableData[indexPath.section].insideHeading?[indexPath.row]
                               return cell

                
            }else if indexPath.section == 4{
                let cell = tableView.dequeueReusableCell(withIdentifier: "PricingCell", for: indexPath) as! PricingCell
                               cell.label.textColor = UIColor(hexString: "#00375F")
                               cell.label.text = tableData[indexPath.section].insideHeading?[indexPath.row]
                               return cell

                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DealAndDiscountCell", for: indexPath) as! DealAndDiscountCell
                               cell.label.textColor = UIColor(hexString: "#00375F")
                               cell.label.text = tableData[indexPath.section].insideHeading?[indexPath.row]
                               return cell
            }
        }
    }
    
//    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1{
            return tableData[section].heading
        }else if section == 2{
            return tableData[section].heading
        }else if section == 3{
            return tableData[section].heading
        }else if section == 4{
            return tableData[section].heading
        }else if section == 5{
            return tableData[section].heading
        }else{
            return tableData[section].heading
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension VendorNameVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryNameVendorImageCell", for: indexPath) as! CategoryNameVendorImageCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionwidth = collectionView.bounds.width
        let collectionHeight = collectionView.bounds.height
        return CGSize(width: collectionwidth, height: collectionHeight)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
            let headView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 60))
            let label = UILabel(frame: CGRect(x: 20, y: 10, width: headView.frame.width - 10, height: 20))
            label.textColor = UIColor(hexString: "CC948E")
        label.font = UIFont(name: "Montserrat-SemiBold", size: 14.0)
            label.text = tableData[section].heading
            headView.addSubview(label)
            return headView
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }



}


class Model{
    var heading : String?
    var insideHeading : [String]?
    
    init(heading : String , insideHeading : [String]) {
        self.heading = heading
        self.insideHeading = insideHeading
    }
}
