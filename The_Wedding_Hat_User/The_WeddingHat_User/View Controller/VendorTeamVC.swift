//
//  VendorTeamVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 12/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import SkeletonView

class VendorTeamVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var topHead: UIView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var headerCollectionView: UICollectionView!
    var selectedIndex = Int()
    let collData: [[String: String]] = [["data": "Saved"] , ["data": "Hired"], ["data": "Negotiating"]]
    var collDataModel = [CollDataModel]()
    var selectedCell = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.headerCollectionView.delegate = self
        self.headerCollectionView.dataSource = self
        self.setDesign()
        self.setUpDataModel()
    }
    
    internal func setUpDataModel() {
        collDataModel = collData.map {CollDataModel.init($0)}
        self.collDataModel[0].isSelected = true
        self.headerCollectionView.reloadData()
    }
    
    @IBAction func backButtonTap(_ sender: UIButton) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
}

extension VendorTeamVC{
    func setDesign(){
        self.topHead.dropShadowBlueColor()
    }   
}

extension VendorTeamVC : UITableViewDelegate , UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(selectedCell == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "VendorTeamTableCell", for: indexPath) as! VendorTeamTableCell
            return cell
        }else if(selectedCell == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "VendorTeamWeddingCell", for: indexPath) as! VendorTeamWeddingCell
            cell.negotiationLbl.text = "Confirmed"
            cell.review.text = "Confirmed"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VendorTeamWeddingCell", for: indexPath) as! VendorTeamWeddingCell
            cell.review.isHidden = true
            cell.negotiationLbl.text = "Negotiation"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(selectedCell == 0){
                return 235
        }else{
            return 150
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 100
    }
    
}

extension VendorTeamVC: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collDataModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VendorTeamColCell", for: indexPath) as! VendorTeamColCell
        cell.cellLabel.text = self.collDataModel[indexPath.row].name
        if self.collDataModel[indexPath.row].isSelected {
            cell.cellLabel.textColor = .white
            cell.cellView.backgroundColor = UIColor(hexString: "#00375F")
        } else {
            cell.cellLabel.textColor = UIColor(hexString: "#00375F")
            cell.cellView.backgroundColor  = .white
        }
             return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        return CGSize(width: (width / 2) - 20, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedCell = indexPath.row
        for (index, model) in self.collDataModel.enumerated() {
            if index != indexPath.row {
                model.isSelected = false
            }else{
                model.isSelected = true
            }
            self.tblView.reloadData()
        }
        self.headerCollectionView.reloadData()

    }
}

class CollDataModel {
    
    var name: String!
    var isSelected = false
    
    init(_ dict : [String: String]) {
        self.name = dict["data"]
    }
}
