//
//  NewAddMemberVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 14/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD
import AlamofireImage


protocol HitAddWeddingPartyListApi {
    func NewAddMember()
}

class NewAddMemberVC: UIViewController {
    
    @IBOutlet weak var downView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headView: UIView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var provideAccessView: UIView!
    @IBOutlet weak var selectRole: TextFieldNotCopyPaste!
    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var emailId: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    var delegate : HitAddWeddingPartyListApi?
    @IBOutlet weak var backButton: UIButton!
    
    let picker = UIPickerView()
    var indexPath = 0
    var memberCount = 0
    var selectedIndexPath  = Int()
    //    var delegate : AddMember?
    //    var weddingDelegate : AddMemberWeddingList?
    var  roleId = ""
    var memberAccessID = [String]()
    var memberData = [[String:Any]]()
    var objMemberAccessModel = [MemberAccessModel]()
    var objMemberRoleModel = [MemberRoleModel]()
    var isPay = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.layoutMargins = UIEdgeInsets.zero
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.picker.delegate = self
        self.picker.dataSource = self
        self.fullName.delegate = self
        self.hitServicegetMemberRole()
        self.setupUI()
        if(memberCount > 0)
        {
            self.saveButton.setTitle("Pay", for: .normal)
            self.isPay = true
        }
        
    }
    @IBAction func tapOnSave(_ sender: Any)
    {
         self.validation()
    }
}

extension NewAddMemberVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objMemberAccessModel.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewMemberTableViewCell", for: indexPath) as! AddNewMemberTableViewCell
            cell.layoutMargins = UIEdgeInsets.zero
            cell.memberAccessName.text = objMemberAccessModel[indexPath.row - 1].access
            if self.memberAccessID.contains(objMemberAccessModel[indexPath.row - 1].id)
            {
                cell.buttonSelect.setImage(#imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle"), for: .normal)
            }
            else
            {
                cell.buttonSelect.setImage(#imageLiteral(resourceName: "tick_unselected"), for: .normal)
            }
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        175
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        130
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let index = memberAccessID.index(where: {$0 == objMemberAccessModel[indexPath.row - 1].id})
        {
            self.memberAccessID.remove(at: index)
            print(memberAccessID)
        }
        else
        {
            self.memberAccessID.append(objMemberAccessModel[indexPath.row - 1].id)
            print(memberAccessID)
        }
        self.tableView.reloadData()
    }
}

extension NewAddMemberVC : UIPickerViewDelegate , UIPickerViewDataSource{
    func setupUI(){
        self.headView.dropShadowBlueColor()
        self.provideAccessView.makeRoundCorner(5.0)
        self.provideAccessView.layer.maskedCorners = [.layerMaxXMinYCorner , .layerMinXMinYCorner]
        self.provideAccessView.layer.borderWidth = 1
        self.provideAccessView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.selectRole.inputView = picker
        self.selectRole.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneRoleButton))
       // self.saveButton.addTarget(self, action: #selector(validation), for: .touchUpInside)
        self.selectRole.colorPlaceHolder(placeholder: "Select role", color:"00375F" )
        self.emailId.colorPlaceHolder(placeholder: "Email address", color: "00375F")
        self.fullName.colorPlaceHolder(placeholder: "Full name", color:"00375F" )
        self.backButton.addTarget(self, action: #selector(backButtonTap), for: .touchUpInside)
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return objMemberRoleModel.count
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return objMemberRoleModel[row].role
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.indexPath = row
    }
    
    @objc func doneRoleButton(_ sender : Any)
    {
        if objMemberRoleModel.count == 0{}else
        {
            self.selectRole.text = self.objMemberRoleModel[indexPath].role
            self.roleId = self.objMemberRoleModel[indexPath].id
        }
    }
    
    @objc func validation() -> Bool
    {
        guard let role = selectRole.text, !role.isEmpty  else{
            self.createAlert(message: Constant.string.slecetRole.localized())
            return false
        }
        guard let name = fullName.text, !name.isEmpty  else
        {
            self.createAlert(message: Constant.string.memberName.localized())
            return false
        }
        guard let email = emailId.text, !email.isEmpty  else
        {
            self.createAlert(message: Constant.string.memberEmail.localized())
            return false
        }
        guard CommonUtilities.shared.isValidEmail(testStr: emailId.text!) else
        {
            self.createAlert(message: Constant.string.validEmail.localized())
            return false
        }
        if self.memberAccessID.count == 0
        {
            self.createAlert(message: Constant.string.provideAccess.localized())
            return false
        }
        else
        {
            print(memberData)
            let MemberData: [String: Any] = ["user_member_role_id":self.roleId,
                                             "user_member_access_id": self.memberAccessID,
                                             "member_name":self.fullName.text!,
                                             "email":self.emailId.text!]
            self.memberData.append(MemberData)
            if self.isPay == true
            {
                self.presentAlertViewWithOneButton(alertTitle:  Constant.string.appname.localized(), alertMessage: "EGP 50 will be deduct to add the new member", btnOneTitle: "Pay now", btnOneTapped: { (_) in
                    self.addMember(memberData: self.memberData, flag: "add", payment_type: 1, userType: EventData.shared.user_type)
                })
            }
            else
            {
               self.addMember(memberData: memberData, flag: "add", payment_type: 0, userType: EventData.shared.user_type)
            }
            
            return true
        }
    }
    
    @objc func backButtonTap(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func hitServicegetMemberRole()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        ServiceManager.instance.request(method: .get, URLString: "getMemberRoleAcesss", parameters:nil, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!{
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if let dict = dictionary?["data"] as? [String:Any]
                    {
                        if statuscode == 200
                        {
                            self.objMemberRoleModel.removeAll()
                            if let member_access = dict["member_access"] as? [[String:Any]]{
                                for member in member_access{
                                    var obj = MemberAccessModel()
                                    if let access = member["access"] as? String
                                    {
                                        obj.access = access
                                    }
                                    if let created_at = member["created_at"] as? String
                                    {
                                        obj.created_at = created_at
                                    }
                                    if let id = member["id"] as? Int
                                    {
                                        obj.id = String(id)
                                    }
                                    if let status = member["status"] as? Int
                                    {
                                        obj.status = String(status)
                                    }
                                    self.objMemberAccessModel.append(obj)
                                }
                                print(self.objMemberAccessModel.count)
                            }
                            if let member_role = dict["member_role"] as? [[String:Any]]{
                                for memberRole in member_role{
                                    var objRole = MemberRoleModel()
                                    if let role = memberRole["role"] as? String
                                    {
                                        objRole.role = role
                                    }
                                    if let created_at = memberRole["created_at"] as? String
                                    {
                                        objRole.created_at = created_at
                                    }
                                    if let id = memberRole["id"] as? Int
                                    {
                                        objRole.id = String(id)
                                    }
                                    self.objMemberRoleModel.append(objRole)
                                }
                                self.tableView.reloadData()
                            }
                        }
                        else{
                            //self.createAlert(title: Constant.string.appname, message: msg)
                        }
                    }
                    
                    
                }
            }
            else{
                // print(parameters)
            }
        }
    }
    
    
    func addMember(memberData : [[String:Any]] , flag : String , payment_type: Int , userType : String)
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        var event_id = ""
        if let eventid = UserDefaults.standard.object(forKey: "EventIDFromCreateEVent") as? String{
            event_id = eventid
        }
        let headers: HTTPHeaders = [ "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,"Accept": "application/json"]
        let params : Parameters = ["user_id" : CommonUtilities.shared.getuserdata()?.id ?? "",
                                   "event_id" : event_id,
                                   "payment_status" : payment_type,
                                   "flag": flag,
                                   "members" : memberData,
                                  // "event_user_type" : "Admin",
                                   "event_created_user_id": "1"
            ] as [String:Any]
        print(params)
        ServiceManager.instance.request(method: .post, URLString: "addmember", parameters: params, encoding: JSONEncoding.default, headers: headers) { (sucess, response, error) in
            KRProgressHUD.dismiss()
            if(sucess)!
            {
                if let sucesscode = response?["status"] as? NSNumber
                {
                    if(sucesscode == 200)
                    {
                        self.delegate?.NewAddMember()
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }
}

extension NewAddMemberVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == fullName
        {
            let maxLength = 20
            let currentString: NSString = fullName!.text as! NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return false
    }
}





class MemberAccessModel{
    var access = ""
    var created_at = ""
    var id = ""
    var status = ""
}
class MemberRoleModel{
    var created_at = ""
    var id = ""
    var role = ""
}
