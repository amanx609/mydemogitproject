//
//  SignUpPageVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 28/05/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD
import AlamofireImage

class SignUpPageVC: UIViewController{
    
    @IBOutlet weak var fullnameText: UITextField!
    @IBOutlet weak var emailIdText: UITextField!
    @IBOutlet weak var countryNameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var reEnterPassword: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var alreadyAccount: UILabel!
    @IBOutlet weak var lblPOlicy: UILabel!
    @IBOutlet weak var arrowButton: UIButton!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var checkBox: UIButton!
    @IBOutlet weak var signInView: NSLayoutConstraint!
    var BoxClick = false
    var picker = UIPickerView()
    var toolbar = UIToolbar()
    var selectedCountryRow = 0
    var countryId = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpDesigns()
        self.picker.delegate = self
        self.picker.dataSource = self
        self.tblView.delegate = self
        self.tblView.dataSource = self
        customLbl()
    }
    
  
    func setUpDesigns(){
        
        
        
        // create attributed string
        
      
        switch UIScreen.main.bounds.width {
        case 375:
            self.signInView.constant = 85
        default:
            self.signInView.constant = 65
        }
//        if(UIScreen.main.bounds.width > 375){
//            self.signInView.constant = 85
//
//        }else{
//            self.signInView.constant = 65
//        }
        countryNameText.inputView = picker
        countryNameText.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        signUpButton.makeRoundCorner(14)
        self.signUpButton.setTitle(Constant.string.signUp.localized(), for: .normal)
        self.signInButton.setTitle(Constant.string.signIn.localized(), for: .normal)
        self.alreadyAccount.text = Constant.string.alreadyAccount.localized()
        self.fullnameText.colorPlaceHolder(placeholder:Constant.string.fullname.localized(),color:"#00375F")
        self.emailIdText.colorPlaceHolder(placeholder:Constant.string.email.localized(),color:"#00375F")
        self.countryNameText.colorPlaceHolder(placeholder:Constant.string.country.localized(),color:"#00375F")
        self.passwordText.colorPlaceHolder(placeholder:Constant.string.createPassword.localized(),color:"#00375F")
        self.reEnterPassword.colorPlaceHolder(placeholder:Constant.string.confirmPassword.localized(),color:"#00375F")
        if GlobalData.shared.countryArray.count == 0{
            CommonUtilities.shared.hitServiceCountryList()
        }
        else{
            print("Working")
        }
    }
    
    func customLbl(){
        let myString = NSMutableAttributedString(string: "By creating an account with us you are agreeing to our ")
        myString.addAttributes([NSAttributedString.Key.font : UIFont(name: "Montserrat", size: 14)!], range: NSMakeRange(0, myString.length))
        let selectablePart = NSMutableAttributedString(string: "Terms & Conditions. ")
        selectablePart.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Montserrat", size: 14)!, range: NSMakeRange(0, selectablePart.length))
              // Add an underline to indicate this portion of text is selectable (optional)
              selectablePart.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: NSMakeRange(0,selectablePart.length))
              selectablePart.addAttribute(NSAttributedString.Key.underlineColor, value: UIColor(hexString: "#00375F")!, range: NSMakeRange(0, selectablePart.length))
              // Add an NSLinkAttributeName with a value of an url or anything else
//              selectablePart.addAttribute(NSAttributedString.Key.link, value: "Terms & Conditions.", range: NSMakeRange(0,selectablePart.length))

              // Combine the non-selectable string with the selectable string
              myString.append(selectablePart)
        let myStr = NSMutableAttributedString(string: " Your data will be used in accordance with our ")
        myStr.addAttributes([NSAttributedString.Key.font : UIFont(name: "Montserrat", size: 14)!], range: NSMakeRange(0, myStr.length))
        let selectable = NSMutableAttributedString(string: "Privacy Policy.")
         selectable.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Montserrat", size: 14)!, range: NSMakeRange(0, selectable.length))
        selectable.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: NSMakeRange(0,selectable.length))
        selectable.addAttribute(NSAttributedString.Key.underlineColor, value: UIColor(hexString: "#00375F")!, range: NSMakeRange(0, selectable.length))
        
        myStr.append(selectable)
        
        myString.append(myStr)

        lblPOlicy.attributedText = myString
        
        
    }
    
    func validation() -> Bool
    {
        guard let fullname = fullnameText.text, !fullname.isEmpty  else{
            self.createAlert(message: Constant.string.Empty_name.localized())
            return false
        }
        guard let email = emailIdText.text, !email.isEmpty  else{
            self.createAlert(message: Constant.string.Empty_email.localized())
            return false
        }
        guard CommonUtilities.shared.isValidEmail(testStr: emailIdText.text!) else{
            self.createAlert(message: Constant.string.validEmail.localized())
            return false
        }
        guard let country = countryNameText.text, !country.isEmpty  else{
            self.createAlert(message: Constant.string.empty_country.localized())
            return false
        }
        guard let password = passwordText.text, !password.isEmpty else{
            self.createAlert(message: Constant.string.createPassword.localized())
            return false
        }
        guard  let checkCount = passwordText.text?.count , checkCount >= 8 else {
            self.createAlert(message: Constant.string.passwordLessThanEight.localized())
            return false
        }
        guard let confirmPassword = reEnterPassword.text, !confirmPassword.isEmpty else {
            self.createAlert(message: Constant.string.confirmPasswordSignUp.localized())
            return false
        }
        if (passwordText.text != reEnterPassword.text){
            self.createAlert(message: Constant.string.passwordnotMatchUp.localized())
            return false
        }
        if (BoxClick == false){
            self.createAlert(message: Constant.string.policy)
            return false
        }
      
        self.hitServiceForSignUp(countryID: self.countryId)
        return true
    }
}
extension SignUpPageVC{
    
    @IBAction func signUpButtonTap(_ sender: UIButton) {
            self.validation()
        }
    
    @IBAction func signInButtonTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func countryListButton(_ sender: UIButton) {
        //self.setPickerView()
    }
    @IBAction func checkBoxClick(_ sender: Any) {
        if(self.BoxClick) == false{
            checkBox.setImage(#imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle"), for: .normal)
        }else{
            checkBox.setImage(#imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle-outline"), for: .normal)
        }
        self.BoxClick = !self.BoxClick
      }
    
}

extension SignUpPageVC{
    
    func hitServiceForSignUp(countryID:Int)
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let parameters =
            [
                "name" : self.fullnameText.text!,
                "email": self.emailIdText.text!,
                "login_type":"normal",
                "password":self.passwordText.text!,
                "country_id":countryID,
                "social_id":""
                ]as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString:"signup", parameters: parameters as [String : AnyObject] , encoding: JSONEncoding.default, headers: nil)
        { (success, dictionary, error) in
            print(dictionary ?? "")
            KRProgressHUD.dismiss()
            if let statuscode = dictionary?["status"] as? NSNumber{
                let msg = dictionary?["message"] as! String
                if statuscode == 200{
                    self.presentAlertViewWithOneButton(alertTitle: Constant.string.appname.localized(), alertMessage: Constant.string.verification.localized(), btnOneTitle: Constant.string.ok.localized(), btnOneTapped: { (_) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }
                else{
                    self.createAlert(message: "There is an existing account with the entered Email Id. Please Sign in or Reset your password.")
//                    self.createAlert(message: msg)
                }
            }
        }
    }
}

extension SignUpPageVC : UIPickerViewDelegate,UIPickerViewDataSource{
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if GlobalData.shared.countryArray.count > 0{
            return GlobalData.shared.countryArray.count
        }
        else{
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return GlobalData.shared.countryArray[row].country_name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedCountryRow = row
    }
    @objc func doneButtonClicked(_ sender: Any) {
        if  GlobalData.shared.countryArray.count == 0{}else{
            self.countryNameText.text = GlobalData.shared.countryArray[selectedCountryRow].country_name
            self.countryId = GlobalData.shared.countryArray[selectedCountryRow].id
        }
    }
}
extension SignUpPageVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 650
    }
    
    
}








