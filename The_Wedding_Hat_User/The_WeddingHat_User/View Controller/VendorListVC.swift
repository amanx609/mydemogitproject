//
//  VendorListVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 15/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD
import AlamofireImage

class VendorListVC: UIViewController, vendorlistProtocol
{
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var filterByType_lbl: UILabel!
    @IBOutlet weak var txtFilter: UITextField!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var categoryName_lbl: UILabel!
    @IBOutlet weak var vendor_list: UITableView!
  
    var category_id = ""
    var category_name = ""
    
    var objModelVendorList = [ModelVendorList]()

    override func viewDidLoad()
    {
        self.headerView.dropShadow()
        self.filterView.makeRoundCorner(4)
        self.vendor_list.separatorColor = .clear
        self.categoryName_lbl.text = category_name
        self.hitServicegetVendorList()
        self.txtFilter.addRightImageAsRightView(strImgName: "down-icon", rightPadding: 10)
    }
    
    @IBAction func tapOnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func taponbookmark(index: Int)
    {
        self.hitservicerforbookmark(vendorid: self.objModelVendorList[index].id, issaved: self.objModelVendorList[index].is_saved)
    }
}

extension VendorListVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.objModelVendorList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VendorListCell", for: indexPath) as! VendorListCell
        cell.populatedata(data: self.objModelVendorList[indexPath.row])
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vendor_story_board = UIStoryboard(name: "Vendordetails", bundle: nil)
        let vc = vendor_story_board.instantiateViewController(withIdentifier: "VendorDetailVC") as! VendorDetailVC
        vc.vendor_id = self.objModelVendorList[indexPath.row].id
        vc.vendor_Name = self.objModelVendorList[indexPath.row].name
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
}

extension VendorListVC
{
    func hitServicegetVendorList()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
       // KRProgressHUD.show()
        let headers: HTTPHeaders =
            [
                "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
                "Content-Type": "application/json"
        ]
        let parameters = ["cat_id": category_id] as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .get, URLString: "vendorList?cat_id=\(category_id)", parameters: nil, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = dictionary?["data"] as? [String:Any]
                        {
                            if let vendorList = data["ITEMS"] as? [[String:Any]]
                            {
                                self.objModelVendorList.removeAll()
                                for list in vendorList
                                {
                                    let obj = ModelVendorList()
                                    if let id = list["id"] as? Int
                                    {
                                        obj.id = String(id)
                                    }
                                    if let business_description = list["business_description"] as? String
                                    {
                                        obj.business_description = business_description
                                    }
                                    if let image = list["image"] as? String
                                    {
                                        obj.image = image
                                    }
                                    if let latitude = list["latitude"] as? String
                                    {
                                        obj.latitude = latitude
                                    }
                                    if let longitude = list["longitude"] as? String
                                    {
                                        obj.longitude = longitude
                                    }
                                    if let name = list["name"] as? String
                                    {
                                        obj.name = name
                                    }
                                    if let reviews = list["reviews"] as? Int
                                    {
                                        obj.reviews = String(reviews)
                                    }
                                    if let rating = list["rating"] as? Double
                                    {
                                        obj.rating = rating
                                    }
                                    if let address = list["address"] as? String
                                    {
                                        obj.address = address
                                    }
                                    if let is_saved = list["is_saved"] as? Bool
                                    {
                                        obj.is_saved = is_saved
                                    }
                                    if let deal_discount = list["deal_discount"] as? Bool
                                    {
                                        obj.deal_discount = deal_discount
                                    }
                                    self.objModelVendorList.append(obj)
                                }
                                self.vendor_list.reloadData()
                            }
                        }
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                        
                    }
                }
            }
            else{
                print(parameters)
            }
        }
    }
    
    func hitservicerforbookmark(vendorid : String, issaved : Bool)
    {
           KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
           let headers: HTTPHeaders =
               [
                   "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
                   "Content-Type": "application/json"
           ]
       
        let parameters = ["vendor_id": category_id, "user_id" : CommonUtilities.shared.getuserdata()!.id, "is_saved" : issaved] as [String : Any]
        print(parameters)
        
           ServiceManager.instance.request(method: .post, URLString: "saveVendor", parameters: parameters, encoding: JSONEncoding.default, headers: headers)
           { (success, dictionary, error) in
               KRProgressHUD.dismiss()
               if (success)!
               {
                    print(dictionary)
                   if let statuscode = dictionary?["status"] as? NSNumber
                   {
                       if statuscode == 200
                       {
                           if let data = dictionary?["data"] as? [String:Any]
                           {
                               if let vendorList = data["ITEMS"] as? [[String:Any]]
                               {

                            }
                           }
                       }
                       else
                       {
                           if let message = dictionary?["message"] as? String
                           {
                               self.createAlert(message: message)
                           }
                           
                       }
                   }
               }
               else{
                   print(parameters)
               }
           }
       }
}

class ModelVendorList
{
    var business_description = ""
    var id = ""
    var image = ""
    var latitude = "";
    var longitude = "";
    var name = ""
    var reviews = ""
    var rating = 0.0
    var address = ""
    var is_saved = false
    var is_featured = false
    var deal_discount = false
    var event_date = ""
    var reviewRating = ""
    var event_type_name = ""
    var created_at = ""
    var is_hired = ""
    var is_vendor_dress = Bool()
    var categoryArr = [String]()
}
