//
//  CommunityVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 13/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD
import AlamofireImage

class CommunityVC: UIViewController, communitydelegate, CommunityFormHitApi
{
    
    
     
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var add_btn: UIButton!
    @IBOutlet weak var communityTable: UITableView!
    
    var CommunityPostIndex = Int()
    var objCommunityModel = CommunityModel()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.add_btn.makeRounded()
        self.headerView.dropShadow()
        self.HitServicecommunityFourm()
        self.communityTable.separatorColor = .clear
        
    }
    @IBAction func tapOnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func tapOnAddDiscussion(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddDiscussionVC") as! AddDiscussionVC
//        vc.modalPresentationStyle = .overCurrentContext
//        self.viewController?.present(vc, animated: true, completion: {})
        vc.communityApiDelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func taponcommentbtn(index: IndexPath)
    {
        //jdvsn
    }
    @IBAction func tapOnProfilePic(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CommunityDetail") as! CommunityDetail
        vc.viaUser = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func tapOnName(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CommunityDetail") as! CommunityDetail
        vc.viaUser = true
        vc.name = CommonUtilities.shared.getuserdata()?.name ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func tapOnPic(sender: UIButton)
    {
    let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CommunityDetail") as! CommunityDetail
        vc.viaUser = false
        vc.name = self.objCommunityModel.post[sender.tag].user_data["name"] as? String ?? ""
        vc.user_Id = self.objCommunityModel.post[sender.tag].user_data["id"] as? String ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func hitAPiCommunityForum() {
        self.HitServicecommunityFourm()
    }
}

extension CommunityVC: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        //return self.objCommunityModel.post.count + 1
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (section == 0)
        {
            return 2
        }
        else
        {
            return self.objCommunityModel.post.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (indexPath.section == 0)
        {
            if (indexPath.row == 0)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalDetailCell", for: indexPath) as! PersonalDetailCell
                cell.selectionStyle = .none
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TopContributerCell", for: indexPath) as! TopContributerCell
                cell.selectionStyle = .none
                cell.populatedData(objData:self.objCommunityModel.topContributors)
                return cell
            }
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommunityPostCell", for: indexPath) as! CommunityPostCell
            cell.delegate = self
            cell.selectionStyle = .none
            cell.name_btn.tag = indexPath.row
            cell.profile_btn.tag = indexPath.row
            cell.nameLbl.text = self.objCommunityModel.post[indexPath.row].user_data["name"] as? String ?? ""
            if let img_url = URL(string: self.objCommunityModel.post[indexPath.row].user_data["user_image"] as? String ?? "")
            {
                cell.profilePic.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "ss"))
            }
            cell.question_lbl.text = self.objCommunityModel.post[indexPath.row].title
            if(self.objCommunityModel.post[indexPath.row].created_at == ""){
                cell.date.text = "24 Aug 2020"
            }else{
                cell.date.text = DateFormatter.shared().dateFromWebtoApp(self.objCommunityModel.post[indexPath.row].created_at)
//                self.objCommunityModel.post[indexPath.row].created_at
            }
            cell.desc_lbl.text = self.objCommunityModel.post[indexPath.row].description
            cell.name_btn.addTarget(self, action: #selector(tapOnPic(sender:)), for: .touchUpInside)
            cell.profile_btn.addTarget(self, action: #selector(tapOnPic(sender:)), for: .touchUpInside)
            return cell
         }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DiscussionDetailsVC") as! DiscussionDetailsVC
        vc.post_Id = self.objCommunityModel.post[indexPath.row].id
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (indexPath.section == 0)
        {
            if (indexPath.row == 1)
            {
                return 170
            }
            else
            {
                return UITableView.automaticDimension
            }
        }
        return UITableView.automaticDimension
    }
    
}

extension CommunityVC
{
    func HitServicecommunityFourm()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        let parameters = ["user_id":CommonUtilities.shared.getuserdata()?.id as Any] as [String : Any]
        ServiceManager.instance.request(method: .post, URLString: "communityFourm", parameters:parameters, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!{
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = dictionary?["data"] as? [String:Any]
                        {
                            let communityObj = CommunityModel()
                            if let topContributors = data["topContributors"] as? [[String:Any]]
                            {
                                communityObj.topContributors.removeAll()
                                for top in topContributors
                                {
                                    let obj = TopContributorsModel()
                                    
                                    obj.commented_by = top.valueForString(key: "commented_by")
                                    obj.total_comments = top.valueForString(key: "total_comments")
                                    if let user_data = top["user_data"] as? [String:Any]
                                    {
                                        obj.user_data = user_data
                                    }
                                    communityObj.topContributors.append(obj)
                                }
                            }
                            if let post = data["posts"] as? [[String:Any]]
                            {
                                communityObj.post.removeAll()
                                for post in post
                                {
                                    let objPost = PostModel()
                                    
                                    objPost.count = 1
                                    objPost.isReply = false
                                    objPost.isExpand = false
                                    objPost.id = post.valueForString(key: "id")
                                    objPost.title = post.valueForString(key: "title")
                                    objPost.cat_id = post.valueForString(key: "cat_id")
                                    objPost.updated_at = post.valueForString(key: "updated_at")
                                    objPost.created_at = post.valueForString(key: "created_at")
                                    objPost.description = post.valueForString(key: "description")
                                    
                                    if let user_data = post["user_data"] as? [String:Any]
                                    {
                                        objPost.user_data = user_data
                                        
                                        if let country = post["country"] as?[String:Any]
                                        {
                                            objPost.country_name = country.valueForString(key: "country_name")
                                        }
                                    }
                                    
                                    if let comment = post["forum_discussion"] as? [[String:Any]]
                                    {
                                        objPost.comment.removeAll()
                                        for commentdata in comment
                                        {
                                            let objComment = CommentModel()
                                            
                                            objComment.id = commentdata.valueForString(key: "id")
                                            objComment.post_date = commentdata.valueForString(key: "post_date")
                                            objComment.discussion = commentdata.valueForString(key: "discussion")
                                            objComment.commented_by = commentdata.valueForString(key: "commented_by")
                                            objComment.community_id = commentdata.valueForString(key: "community_id")
                                            
                                            if let user_data = commentdata["user_data"] as? [String:Any]
                                            {
                                                objComment.user_data = user_data
                                            }
                                            
                                            objPost.comment.append(objComment)
                                        }
                                    }
                                    communityObj.post.append(objPost)
                                }
                            }
                            self.objCommunityModel = communityObj
                            self.communityTable.reloadData()
                        }
                    }
                    else{
                        
                    }
                }
            }
            
        }
    }
}


class CommunityModel
{
    var post = [PostModel]()
    var topContributors = [TopContributorsModel]()
}

class TopContributorsModel
{
    var commented_by = ""
    var total_comments = ""
    var user_data = [String:Any]()
    
}
class PostModel
{
    var id = ""
    var title = ""
    var user_data = [String:Any]()
    var updated_at = ""
    var cat_id = ""
    var description = ""
    var created_at = ""
    var country_name = ""
    var count = Int()
    var isReply = Bool()
    var isExpand = Bool()
    var comment = [CommentModel]()
}
class CommentModel
{
    var id = ""
    var post_date = ""
    var discussion = ""
    var commented_by = ""
    var community_id = ""
    var user_data = [String:Any]()
}
