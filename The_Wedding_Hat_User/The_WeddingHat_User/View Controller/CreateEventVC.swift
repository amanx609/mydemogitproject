//
//  CreateEventVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 03/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD
import AlamofireImage

class CreateEventVC: UIViewController, SaveMember {
    
    

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var insideViewTbl: UIView!
    @IBOutlet weak var eventUploadImage: UIImageView!
    @IBOutlet weak var eventTypeTextField: UITextField!
    @IBOutlet weak var eventNameTextField: UITextField!
    @IBOutlet weak var brideNameTextField: UITextField!
    @IBOutlet weak var groomNameTextField: UITextField!
    @IBOutlet weak var selectCountryTextField: UITextField!
    @IBOutlet weak var selectCityTextField: UITextField!
    @IBOutlet weak var eventDateTextField: UITextField!
    @IBOutlet weak var noOfGuestsTextField: UITextField!
    @IBOutlet weak var yourBudgetTextField: UITextField!
    @IBOutlet weak var brideNameView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet var outerView: UIView!
    @IBOutlet weak var groomNameView: UIView!
    @IBOutlet weak var eventNameView: UIView!
    @IBOutlet weak var mainView: UIView!
//    @IBOutlet weak var addMoreMembers: UIButton!
    @IBOutlet weak var imgLbl: UILabel!
    @IBOutlet weak var groomBrideView: UIView!
    @IBOutlet weak var topCountryView: NSLayoutConstraint!
    @IBOutlet weak var heightofGroombrideHeight: NSLayoutConstraint!
    @IBOutlet weak var headerLbl: UILabel!
    
    var tempEditCity = [CountryModel]()
//    var obj = AddMemberVC()
    var indexRow = 0
    var countryId = ""
    var selectedEventRow = 0
    var selectCityRow = 0
    var eventTypeID = ""
    var event_id = ""
    var cityID = ""
    var viaEdit = false
    var objEditDataModel = [EditDataModel]()
    var selectedImage: UIImage?
    var MemberData = [[String:Any]]()
    var MemberCellCount = 0
    var eventTID = ""
    var eventIndex : String?
    var memberEdit = ""
    var imageString = ""
    var partyRow = ""
    var eventName = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        self.setupDesign()
        self.setpicker()
        self.tblView.delegate = self
//        self.tblView.dataSource = self
        self.brideNameTextField.delegate = self
        self.groomNameTextField.delegate = self
        self.eventNameTextField.delegate = self
        self.noOfGuestsTextField.delegate = self
        self.yourBudgetTextField.delegate = self
        //self.eventUploadImage.image = //#imageLiteral(resourceName: "eventPalceHolder")
    }
    
    func saveWeddingPartyMember(wedMemdata: [[String : Any]]) {
//        self.MemberData = wedMemdata
        self.MemberCellCount = self.MemberCellCount + wedMemdata.count
        self.tblView.reloadData()
        self.memberEdit = "add"
        
    }
    
    
    func addNewMember(memData: [[String : Any]]) {
        self.MemberData = memData
        print(self.MemberData)
        self.MemberCellCount = self.MemberData.count
        self.tblView.reloadData()
        self.memberEdit = "add"
    }
    func setpicker(){
        if GlobalData.shared.eventTypeArray.count == 0{
            self.eventTypeTextField.setPickerData(arrPickerData:[], selectedPickerDataHandler: { (text, row, component) in
            }, defaultPlaceholder: nil)
        }
        else{
            
            self.eventTypeTextField.setPickerData(arrPickerData:
                (GlobalData.shared.eventTypeArray.map({$0.event_type as String})), selectedPickerDataHandler: { (text, row, component) in
                    self.eventTypeID = String(GlobalData.shared.eventTypeArray[row].id)
                    self.eventIndex = String(GlobalData.shared.eventTypeArray[row].event_type)
                    self.eventName = String(GlobalData.shared.eventTypeArray[row].event_type)
                    print(self.eventIndex)
                     if self.eventIndex == "Party"{
                        self.groomBrideView.isHidden = true
                        self.topCountryView.constant = 15.0
                        self.heightofGroombrideHeight.constant = 0.0
                        self.tblView.reloadData()
                    }else{
                        self.groomBrideView.isHidden = false
                        self.topCountryView.constant = 111.0
                        self.heightofGroombrideHeight.constant = 81.0
                        self.tblView.reloadData()
                    }
            }, defaultPlaceholder: nil)
        }
        if GlobalData.shared.countryArray.count == 0{
            self.selectCountryTextField.setPickerData(arrPickerData:[], selectedPickerDataHandler: { (text, row, component) in
            }, defaultPlaceholder: nil)
            self.selectCityTextField.setPickerData(arrPickerData: [], selectedPickerDataHandler: { (text, row, component) in
            }, defaultPlaceholder: nil)
        }
        else{
            var upperRow = Int()

            self.selectCountryTextField.setPickerData(arrPickerData:
                (GlobalData.shared.countryArray.map({$0.country_name as String})), selectedPickerDataHandler: { (text, row, component) in
                    self.selectCityTextField.text = ""
                    self.countryId = String(GlobalData.shared.countryArray[row].id)
                    upperRow = row
                    self.selectCityTextField.setPickerData(arrPickerData:
                        (GlobalData.shared.countryArray[upperRow].cities.map({$0.city_name as String})), selectedPickerDataHandler: { (text, row, component) in
                            self.cityID = GlobalData.shared.countryArray[upperRow].cities[row].id
                    }, defaultPlaceholder: nil)
            }, defaultPlaceholder: nil)
        }
     }
}

extension CreateEventVC{
    @IBAction func uploadEventImage(_ sender: UIButton) {
        self.presentImagePickerController(allowEditing: true) { (image, nil) in
            if image != nil {
                self.eventUploadImage.contentMode = .scaleAspectFill
                self.eventUploadImage.image = self.cropImage(image: image!, to: 10)
                self.selectedImage = image
                self.imgLbl.isHidden = true
            }
        }
    }
    @IBAction func selectCityButton(_ sender: UIButton) {
        self.createAlert(message: Constant.string.underdevlopment)
    }
//    @IBAction func addMoreMemberButton(_ sender: UIButton) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMemberVC") as! AddMemberVC
//        vc.delegate = self
//        vc.memberData = self.MemberData
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: true, completion: nil)
//    }
    @IBAction func cancelButtonTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func createButtonTap(_ sender: UIButton) {
        self.validation()
    }
    @objc func connectedDelete(sender: UIButton){
        let cell = sender.superview?.superview
        let indexPath1 :IndexPath = self.tblView.indexPath(for: cell as! EventViewCell)!
        if MemberData.count != 0{
            if self.MemberCellCount == MemberData.count{
                MemberData.remove(at: indexPath1.row)
                print(MemberData)
                self.memberEdit = "edit"
            }
            MemberCellCount = MemberCellCount - 1
            self.memberEdit = "edit"
            tblView.reloadData()
        }
    }
}
extension CreateEventVC: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return MemberCellCount
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "EventViewCell", for: indexPath) as! EventViewCell
//        cell.selectionStyle = .none
//        cell.deleteButton.tag = indexPath.row
//        cell.memberName.text = self.MemberData[indexPath.row]["member_name"] as? String ?? ""
//        cell.deleteButton.addTarget(self, action: #selector(connectedDelete(sender:)), for: .touchUpInside)
//        return cell
//    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 50
//    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return insideViewTbl
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return outerView
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if self.eventIndex == "Party"
        {
            return 517
        }
        else{
            if eventTID == "4"{
                return 517
            }else{
                return 630
            }
        }
    }
}
extension CreateEventVC{
    
    //MARK:- Add Member by Create Event VC
    func hitServiceAddMember(membersData:[[String : Any]],eventID:String,flag:String){
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        let parameters: Parameters = [
            "user_id":CommonUtilities.shared.getuserdata()?.id,
            "event_id":eventID,
            "members" : membersData,
            "flag":flag,
            "payment_status" : 0
        ]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "addmember", parameters: parameters as? [String : Any], encoding: JSONEncoding.default, headers: headers) { (sucess, dictionary, error) in
            if (sucess)!{
                if let statuscode = dictionary!["status"] as? NSNumber{
                    if statuscode == 200{
                        
                    }
                }
            }
        }
    }
    
    
    //MARK:- ADD EVENT
    func hitServiceAddEvent(eventTypeId:String,countryId:String,cityId:String){
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        let parameters: Parameters = [
            "user_id":CommonUtilities.shared.getuserdata()?.id,
            "event_type_id": eventTypeId,
            "event_name": self.eventNameTextField.text!,
            "bride_name":self.brideNameTextField.text!,
            "groom_name":self.groomNameTextField.text!,
            "country_id":countryId,
            "city_id":cityId,
            "event_date": dateChange(self.eventDateTextField.text!),
            "budget":self.yourBudgetTextField.text!,
            "no_of_guest":self.noOfGuestsTextField.text!,
            "event_type_name":self.eventTypeTextField.text!,
            "country_name":self.selectCountryTextField.text!,
            "city_name":self.selectCityTextField.text!
        ]
        print(parameters)
        Alamofire.upload(multipartFormData: { multipartFormData in
            if self.selectedImage != nil {
                if self.selectedImage!.size.width != 0{
                    let imageData1 =
                        self.selectedImage!.jpegData(compressionQuality: 0.5)!
                    multipartFormData.append(imageData1, withName: "event_image", fileName: "\(imageData1)images.jpg", mimeType: "image/jpeg")
                }
            }
            for (key, value) in parameters{
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: BASEURL + "addEvent", headers: headers,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    KRProgressHUD.dismiss()
                    let result = response.result.value
                    print("This is result dude \(result)")
                    var responseDic = result as! [String : Any]
                    print(responseDic)
                    if let statuscode = responseDic["status"] as? NSNumber{
                        if statuscode == 200{
                            if let data = responseDic["data"] as? [String:Any]{
                                if let event_id = data["event_id"] as? String{
                                    if self.MemberData.count != 0{
                                        if self.memberEdit == "add"{
                                            self.hitServiceAddMember(membersData: self.MemberData, eventID: event_id, flag: "add")
                                        }
                                        else{
                                            self.hitServiceAddMember(membersData: self.MemberData, eventID: event_id, flag: "edit")
                                        }
                                    }
                                }
                            }
                            self.dismiss(animated: true, completion: nil)
                        }
                        else
                        {
                            //self.createAlert(message: msg)
                        }
                    }
                    print("success uload");
                }
            case .failure(let error):
                print(error)
                KRProgressHUD.dismiss()
            }
        })
    }
    
    
    //MARK:- Edit Event
    func hitServiceEditEvent(eventTypeId:String,eventId:String,countryId:String,cityId:String)
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        let parameters: Parameters = [
            "user_id": (CommonUtilities.shared.getuserdata()?.id)!,
            "event_id":eventId,
            "event_type_id": self.eventTypeID,
            "event_name": self.eventNameTextField.text!,
            "bride_name":self.brideNameTextField.text!,
            "groom_name":self.groomNameTextField.text!,
            "country_id":countryId,
            "city_id":cityId,
            "event_date":self.dateChange(eventDateTextField.text!),
            "budget":self.yourBudgetTextField.text!,
            "no_of_guest":self.noOfGuestsTextField.text!
        ]
        print(parameters)
        Alamofire.upload(multipartFormData: { multipartFormData in
            if self.selectedImage != nil {
                if self.selectedImage!.size.width != 0{
                    let imageData1 =
                        self.selectedImage!.jpegData(compressionQuality: 0.5)!
                    multipartFormData.append(imageData1, withName: "event_image", fileName: "\(imageData1)images.jpg", mimeType: "image/jpeg")
                }
            }
            for (key, value) in parameters{
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: BASEURL + "updateEvent", headers: headers,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    KRProgressHUD.dismiss()
                    let result = response.result.value
                    print("This is result dude \(result)")
                    let responseDic = result as! [String : Any]
                    print(responseDic)
                    let msg = responseDic["message"] as? String
                    if let statuscode = responseDic["status"] as? NSNumber{
                        if statuscode == 200{
                            if self.MemberData.count == 0{}else{
                                if self.memberEdit == "add"{
                                    self.hitServiceAddMember(membersData: self.MemberData, eventID: self.event_id, flag: "add")
                                }
                                else{
                                    self.hitServiceAddMember(membersData: self.MemberData, eventID: self.event_id, flag: "edit")
                                }
                            }
                            
                            self.dismiss(animated: true, completion: nil)
                        }
                        else if statuscode == 422{
                            self.createAlert(message: msg!)
                        }else{
                            self.presentAlertViewWithOneButton(alertTitle: Constant.string.appname.localized(), alertMessage: msg!, btnOneTitle: Constant.string.ok.localized()) { (UIAlertAction) in
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                    print("success uload");
                }
            case .failure(let error):
                print(error)
                KRProgressHUD.dismiss()
            }
        })
    }
    
    //MARK:- Event Data
    
    func hitServicegetEventData(eeventId:String){
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        ServiceManager.instance.request(method: .get, URLString: "getEditEvent/"+String(eeventId), parameters:nil, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!{
                if let statuscode = dictionary?["status"] as? NSNumber{
                    self.objEditDataModel.removeAll()
                    let Obj = EditDataModel()
                    if let dict = dictionary?["data"] as? [String:Any]{
                        if statuscode == 200{
                            Obj.bride_name = dict.valueForString(key: "bride_name")
                            if let budget = dict.valueForInt(key: "budget"){
                                Obj.budget = String(budget)
                            }
                            if let city_id = dict.valueForInt(key: "city_id"){
                                Obj.city_id = String(city_id)
                            }
                            if let country_id = dict.valueForInt(key: "country_id"){
                                Obj.country_id = String(country_id)
                            }
                            Obj.event_date = dict.valueForString(key: "event_date")
                            if let event_id = dict.valueForInt(key: "event_id"){
                                Obj.event_id = String(event_id)
                            }
                            Obj.event_image = dict.valueForString(key: "event_image")
                            Obj.event_name = dict.valueForString(key: "event_name")
                            Obj.groom_name = dict.valueForString(key: "groom_name")
                            if let no_of_guest = dict.valueForInt(key: "no_of_guest"){
                                Obj.no_of_guest = String(no_of_guest)
                            }
                            if let user_id = dict.valueForInt(key: "user_id"){
                                Obj.user_id = String(user_id)
                            }
                            if let event_type_id = dict.valueForInt(key: "event_type_id"){
                                Obj.event_type_id = String(event_type_id)
                            }
                            if let country = dict["country"] as? [String:Any]{
                                if let country_name = country["country_name"] as? String{
                                    Obj.country_name = country_name
                                    if let id = country["id"] as? Int {
                                        self.countryId = String(id)
                                    }
                                }
                            }
                            if let city = dict["city"] as? [String:Any]{
                                Obj.city_name = city.valueForString(key: "city_name")
                                if let id = city["id"] as? Int {
                                    self.cityID = String(id)
                                }
                            }
                            if let event_type = dict["event_type"] as? [String:Any]{
                                Obj.event_type = event_type.valueForString(key: "event_type_name")
                                self.eventTypeID = event_type.valueForString(key: "id")
                            }
                            if let event_member = dict["event_member"] as? [[String:Any]]{
                                for member in event_member{
                                    let mem = EventMember()
                                   mem.email = member.valueForString(key: "email")
                                    mem.member_name = member.valueForString(key: "member_name")
                                    if let id = member.valueForInt(key: "id"){
                                        mem.id = String(id)
                                    }
                                    if let user_event_id = member.valueForInt(key: "user_event_id"){
                                        mem.user_event_id = String(user_event_id)
                                    }
                                    if let user_member_role_id = member.valueForInt(key: "user_member_role_id"){
                                        mem.user_member_role_id = String(user_member_role_id)
                                    }
                                    if let access = member["access"] as? [[String:Any]]{
                                        for memAccess in access{
                                            let memAcc = MemberAccess()
                                            if let user_member_access_id = memAccess.valueForInt(key: "user_member_access_id"){
                                                memAcc.user_member_access_id = String(user_member_access_id)
                                            }
                                            if let user_member_id = memAccess.valueForInt(key: "user_member_id"){
                                                memAcc.user_member_id = String(user_member_id)
                                            }
                                            mem.access.append(memAcc)
                                        }
                                    }
                                    Obj.event_member.append(mem)
                                }
                            }
                            self.objEditDataModel.append(Obj)
                            self.viaEditEvent()
                            self.tblView.reloadData()
                        }
                        else{
                            //self.createAlert(title: Constant.string.appname, message: msg)
                        }
                    }
                }
            }
            else{
                // print(parameters)
            }
        }
    }  
}

extension CreateEventVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if(textField == yourBudgetTextField || textField == noOfGuestsTextField)
        {
             // Uses the number format corresponding to your Locale
             let formatter = NumberFormatter()
             formatter.numberStyle = .decimal
             formatter.locale = Locale.current
             formatter.maximumFractionDigits = 0


            // Uses the grouping separator corresponding to your Locale
            // e.g. "," in the US, a space in France, and so on
            if let groupingSeparator = formatter.groupingSeparator {

                if string == groupingSeparator {
                    return true
                }
//                if(textField == noOfGuestsTextField)
//                {
//
//                    if let intValue = Int(newText), intValue <= 5000 {
//                    return true
//                }

                if let textWithoutGroupingSeparator = textField.text?.replacingOccurrences(of: groupingSeparator, with: "") {
                    var totalTextWithoutGroupingSeparators = textWithoutGroupingSeparator + string
                    if(textField == noOfGuestsTextField)
                    {
                        if(Int(totalTextWithoutGroupingSeparators) ?? 100000 > 5000 )
                        {
                            self.createAlert(message: "Number of guests cannot exceed 5,000" )
                            return false
                        }
                    }
                    if string.isEmpty { // pressed Backspace key
                        totalTextWithoutGroupingSeparators.removeLast()
                    }
                    
                    if let numberWithoutGroupingSeparator = formatter.number(from: totalTextWithoutGroupingSeparators),
                        let formattedText = formatter.string(from: numberWithoutGroupingSeparator) {

                        textField.text = formattedText
                        return false
                    }
                }
            }
            return true
        }
        
        
//            if textField == yourBudgetTextField {
//
//                let newText = NSString(string: yourBudgetTextField.text!).replacingCharacters(in: range, with: string)
//                if newText.isEmpty {
//                    return true
//                }
//                else if let intValue = Int(newText), intValue <= 1000000{
//                    return true
//                } else {
//                    self.createAlert(message: Constant.string.maximumamount)
//                }
//            } else
                if textField == noOfGuestsTextField
                {
                let newText = NSString(string: noOfGuestsTextField.text!).replacingCharacters(in: range, with: string)
                if newText.isEmpty {
                    return true
                }
//                else if let intValue = Int(newText), intValue <= 5000 {
//                    return true
//                } else {
//                    self.createAlert(message: Constant.string.maximumguest)
//                }
            }else if textField == brideNameTextField{
                let maxLength = 20
                let currentString: NSString = brideNameTextField!.text as! NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
        }else if textField == groomNameTextField{
                let maxLength = 20
                let currentString: NSString = groomNameTextField!.text as! NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
        }else if textField == eventNameTextField{
                let maxLength = 20
                let currentString: NSString = eventNameTextField!.text as! NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
        }
        
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
//        if textField == self.yourBudgetTextField {
//
//            if self.yourBudgetTextField.text == "0" ||  self.yourBudgetTextField.text == ""{
//                self.createAlert(message: "Amount can't be zero")
//            }
//        } else if textField == self.noOfGuestsTextField {
//            if self.noOfGuestsTextField.text == "0" ||  self.noOfGuestsTextField.text == ""{
//                self.createAlert(message: "Guest can't be zero")
//            }
//        }
    }
    
}

extension CreateEventVC{
    func viaEditEvent(){
        if let img_url = URL(string:self.objEditDataModel[0].event_image){
            self.imgLbl.isHidden = true
            self.imageString = self.objEditDataModel[0].event_image
            self.eventUploadImage.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "HeaderRing"))
        }
        self.eventTypeTextField.text = self.objEditDataModel[0].event_type
        self.eventNameTextField.text = self.objEditDataModel[0].event_name
        self.brideNameTextField.text = self.objEditDataModel[0].bride_name
        self.groomNameTextField.text = self.objEditDataModel[0].groom_name
        self.selectCountryTextField.text = self.objEditDataModel[0].country_name
        self.selectCityTextField.text = self.objEditDataModel[0].city_name
        self.eventDateTextField.text = self.objEditDataModel[0].event_date
        self.noOfGuestsTextField.text = self.objEditDataModel[0].no_of_guest
        self.yourBudgetTextField.text = self.objEditDataModel[0].budget
        
        if GlobalData.shared.countryArray.count >= 0{
            self.tempEditCity =  GlobalData.shared.countryArray.filter {
                $0.country_name.contains(self.objEditDataModel[0].country_name) }
            
            self.selectCityTextField.setPickerData(arrPickerData:
                (self.tempEditCity[0].cities.map({$0.city_name as String})), selectedPickerDataHandler: { (text, row, component) in
                    self.cityID = self.tempEditCity[0].cities[row].id
            }, defaultPlaceholder: nil)
        }
        if self.objEditDataModel[0].event_type == "Party"{
                              self.groomBrideView.isHidden = true
                              self.topCountryView.constant = 15.0
                              self.heightofGroombrideHeight.constant = 0.0
                              self.tblView.reloadData()
                          }else{
                              self.groomBrideView.isHidden = false
                              self.topCountryView.constant = 111.0
                              self.heightofGroombrideHeight.constant = 81.0
                              self.tblView.reloadData()
                          }
        
        if self.objEditDataModel[0].event_member.count == 0{}else{
            self.MemberData.removeAll()
            var memberAccessID = [String]()
            for member in self.objEditDataModel[0].event_member{
                for acces in member.access{
                    memberAccessID.append(acces.user_member_access_id)
                }
                var MemberData: [String: Any] = ["user_member_role_id":member.user_member_role_id,
                "user_member_access_id": memberAccessID,
                "member_name":member.member_name,
                "email":member.email]
                self.MemberData.append(MemberData)
            }
            self.MemberCellCount = self.MemberData.count
            self.tblView.reloadData()
        }
        
        
    }
    
    func setupDesign(){
        if eventUploadImage.image == nil{
            self.imgLbl.isHidden = false
        }
        if eventTID == "4"{
            self.groomBrideView.isHidden = true
            self.topCountryView.constant = 15.0
            self.heightofGroombrideHeight.constant = 0.0
            self.tblView.reloadData()
        }else{
            self.groomBrideView.isHidden = false
            self.topCountryView.constant = 111.0
            self.heightofGroombrideHeight.constant = 81.0
            self.tblView.reloadData()
        }
        self.mainView.makeRoundCorner(8)
        self.mainView.dropShadow()
        self.createButton.makeRoundCorner(10.0)
//        self.addMoreMembers.makeRoundCorner(5.0)
//        self.addMoreMembers.isHidden = true

        self.eventUploadImage.makeRoundCorner(10.0)
        self.eventUploadImage.layer.borderWidth = 2.0
        self.outerView.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 10.0)
        self.insideViewTbl.roundCorners(corners: [.topLeft , .topRight], radius: 10.0)
        self.eventUploadImage.layer.borderColor = UIColor(hexString: "#00375F")?.cgColor
        self.eventDateTextField.setDatePickerWithDateFormate(dateFormate: "dd MMM yyyy", defaultDate: nil, isPrefilledDate: false) { (date) in
        }
        self.eventDateTextField.setDatePickerMode(mode: .date)
        self.eventDateTextField.setMinimumDate(minDate:  NSDate() as Date)
    }
    func setData()
    {
        if(viaEdit)
        {
            self.eventTypeTextField.isUserInteractionEnabled = false
            self.eventTypeTextField.text = ""
        }
        self.cancelButton.addDashBorder(color: UIColor(hexString: "#00375F")!)
//        self.addMoreMembers.setTitle(Constant.string.add_more_member.localized(), for: .normal)
        self.eventTypeTextField.colorPlaceHolder(placeholder: Constant.string.event_type.localized(), color: "#00375F")
        self.eventNameTextField.colorPlaceHolder(placeholder: Constant.string.event_name.localized(), color: "#00375F")
        self.brideNameTextField.colorPlaceHolder(placeholder: Constant.string.bride_name.localized(), color: "#00375F")
        self.groomNameTextField.colorPlaceHolder(placeholder: Constant.string.groom_name.localized(), color: "#00375F")
        self.selectCountryTextField.colorPlaceHolder(placeholder: Constant.string.select_country.localized(), color: "#00375F")
        self.selectCityTextField.colorPlaceHolder(placeholder: Constant.string.select_city.localized(), color: "#00375F")
        self.eventNameTextField.colorPlaceHolder(placeholder: Constant.string.event_name.localized(), color: "#00375F")
        self.eventDateTextField.colorPlaceHolder(placeholder: Constant.string.event_date.localized(), color: "#00375F")
        self.yourBudgetTextField.colorPlaceHolder(placeholder: Constant.string.your_budget.localized(), color: "#00375F")
        self.noOfGuestsTextField.colorPlaceHolder(placeholder: Constant.string.no_of_guest.localized(), color: "#00375F")
        if GlobalData.shared.eventTypeArray.count == 0 || GlobalData.shared.countryArray.count == 0{
            CommonUtilities.shared.hitServicegetEventType()
            CommonUtilities.shared.hitServiceCountryList()
        }
        if self.viaEdit == true{
            self.headerLbl.text = Constant.string.editEvent.localized()
            self.createButton.setTitle(Constant.string.updateEvent.localized(), for: .normal)
            self.hitServicegetEventData(eeventId: self.event_id)
        }
        else{
            self.headerLbl.text = Constant.string.create_event.localized()
            self.createButton.setTitle(Constant.string.create_event.localized(), for: .normal)
        }
    }
    func validation() -> Bool{
        guard let eventType = eventTypeTextField.text, !eventType.isEmpty  else{
            self.createAlert(message: Constant.string.eventType.localized())
            return false
        }
        guard let eventName = eventNameTextField.text, !eventName.isEmpty  else{
            self.createAlert(message: Constant.string.eventName.localized())
            return false
        }
        if self.eventTypeTextField.text == "Party"{}else{
            guard let bridename = brideNameTextField.text, !bridename.isEmpty  else{
                self.createAlert(message: Constant.string.brideName.localized())
                return false
            }
            guard let groomname = groomNameTextField.text, !groomname.isEmpty else{
                self.createAlert(message: Constant.string.groomName.localized())
                return false
            }
        }
        guard let country = selectCountryTextField.text, !country.isEmpty else{
            self.createAlert(message: Constant.string.Emptycountry.localized())
            return false
        }
        guard let city = selectCityTextField.text, !city.isEmpty else{
            self.createAlert(message: Constant.string.city.localized())
            return false
        }
        guard let eventDate = eventDateTextField.text, !eventDate.isEmpty else{
            self.createAlert(message: Constant.string.eventDate.localized())
            return false
        }
//        guard let guest = noOfGuestsTextField.text, !guest.isEmpty else{
//            self.createAlert(message: Constant.string.guestNo.localized())
//            return false
//        }
//        guard let checkguest = noOfGuestsTextField.text?.count , checkguest > 0 else {
//            self.createAlert(message: Constant.string.maximumguest.localized())
//            return false
//        }
//        guard let budget = yourBudgetTextField.text, !budget.isEmpty else{
//            self.createAlert(message: Constant.string.budget.localized())
//            return false
//        }
//        guard let checkamount = yourBudgetTextField.text?.count , checkamount > 0 else {
//            self.createAlert(message: Constant.string.maximumamount.localized())
//            return false
//        }
        if self.viaEdit == true{
            if self.objEditDataModel[0].event_image == "" && self.selectedImage == nil{
                self.createAlert(message: Constant.string.emptyImg.localized())
            }
        }else{
//            if self.selectedImage == nil{
//                self.createAlert(message: Constant.string.emptyImg.localized())
//                return false
//            }
            print("no img")
        }
        if viaEdit == true{
            self.hitServiceEditEvent(eventTypeId:eventTID, eventId: self.objEditDataModel[0].event_id, countryId: self.countryId, cityId: self.cityID)
        }
        else{
            self.hitServiceAddEvent(eventTypeId: self.eventTypeID, countryId: self.countryId, cityId: self.cityID)
        }
        return true
    }
}

extension CreateEventVC{
    func cropImage(image: UIImage, to aspectRatio: CGFloat) -> UIImage {

        let imageAspectRatio = image.size.height/image.size.width

        var newSize = image.size

        if imageAspectRatio > aspectRatio {
            newSize.height = image.size.width * aspectRatio
        } else if imageAspectRatio < aspectRatio {
            newSize.width = image.size.height / aspectRatio
        } else {
            return image
        }

        let center = CGPoint(x: image.size.width/2, y: image.size.height/2)
        let origin = CGPoint(x: center.x - newSize.width/2, y: center.y - newSize.height/2)

        let cgCroppedImage = image.cgImage!.cropping(to: CGRect(origin: origin, size: CGSize(width: newSize.width, height: newSize.height)))!
        let croppedImage = UIImage(cgImage: cgCroppedImage, scale: image.scale, orientation: image.imageOrientation)

        return croppedImage
    }
    
    
    
}


//extension CreateEventVC : UITextFieldDelegate
//{
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//         // Uses the number format corresponding to your Locale
//         let formatter = NumberFormatter()
//         formatter.numberStyle = .decimal
//         formatter.locale = Locale.current
//         formatter.maximumFractionDigits = 0
//
//
//        // Uses the grouping separator corresponding to your Locale
//        // e.g. "," in the US, a space in France, and so on
//        if let groupingSeparator = formatter.groupingSeparator {
//
//            if string == groupingSeparator {
//                return true
//            }
//
//
//            if let textWithoutGroupingSeparator = textField.text?.replacingOccurrences(of: groupingSeparator, with: "") {
//                var totalTextWithoutGroupingSeparators = textWithoutGroupingSeparator + string
//                if string.isEmpty { // pressed Backspace key
//                    totalTextWithoutGroupingSeparators.removeLast()
//                }
//                if let numberWithoutGroupingSeparator = formatter.number(from: totalTextWithoutGroupingSeparators),
//                    let formattedText = formatter.string(from: numberWithoutGroupingSeparator) {
//
//                    textField.text = formattedText
//                    return false
//                }
//            }
//        }
//        return true
//    }

//}

class EditDataModel{
    var bride_name = ""
    var budget = ""
    var city_id = ""
    var country_id = ""
    var event_date = ""
    var event_id = ""
    var event_image = ""
    var event_name = ""
    var event_type_id = ""
    var groom_name = ""
    var no_of_guest = ""
    var user_id = ""
    var country_name = ""
    var city_name = ""
    var event_type = ""
    var event_member = [EventMember]()
}

class EventMember{
    var access = [MemberAccess]()
    var email = ""
    var id = ""
    var member_name = ""
    var user_event_id = ""
    var user_member_role_id = ""
}
class MemberAccess{
    var user_member_access_id = ""
    var user_member_id = ""
}

class EventType{
    var event_type_name = ""
    var id = Int()
}
