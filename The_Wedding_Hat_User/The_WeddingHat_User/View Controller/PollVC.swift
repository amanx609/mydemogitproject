//
//  PollVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 26/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

class PollVC: UIViewController,hitservice {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var past_btn: UIButton!
    @IBOutlet weak var pastView: UIView!
    @IBOutlet weak var going_view: UIView!
    @IBOutlet weak var ongoing_btn: UIButton!
    @IBOutlet weak var pollsTable: UITableView!
  
    var flag = "1"
    var questionObj = [QuestionAnswer]()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupDesign()
        self.hitServicePoll(flag: "1")
    }
    
    @IBAction func tapOnGoing(_ sender: Any)
    {
        self.flag = "1"
        self.past_btn.setTitleColor(UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0), for: .normal)
        self.ongoing_btn.setTitleColor(UIColor.white, for: .normal)
        self.pastView.backgroundColor = UIColor.white
        self.going_view.backgroundColor = UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0)
         self.hitServicePoll(flag: "1")
        
    }
   
    @IBAction func tapOnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnPast(_ sender: Any)
    {
        self.flag = "2"
        self.going_view.backgroundColor = UIColor.white
        self.past_btn.setTitleColor(UIColor.white, for: .normal)
        self.ongoing_btn.setTitleColor(UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0), for: .normal)
        self.pastView.backgroundColor = UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0)
         self.hitServicePoll(flag: "2")
    }
    
    func setupDesign()
    {
        self.headerView.dropShadow()
        self.going_view.makeRoundCorner(8)
        self.going_view.dropShadow()
        self.pastView.makeRoundCorner(8)
        self.pastView.dropShadow()
        self.pollsTable.separatorColor = .clear
        self.pastView.backgroundColor = UIColor.white
        self.going_view.backgroundColor = UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0)
        self.past_btn.setTitleColor(UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0), for: .normal)
        self.ongoing_btn.setTitleColor(UIColor.white, for: .normal)
    }
    func reload()
    {
        self.hitServicePoll(flag: "1")
        self.pollsTable.reloadData()
    }
    
}

extension PollVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.questionObj.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Polls_Cell", for: indexPath) as! Polls_Cell
        cell.selectionStyle = .none
        cell.delegate = self
        cell.flag = self.flag
        cell.question_lbl.text = self.questionObj[indexPath.row].question
        cell.populateData(obj:self.questionObj[indexPath.row].answer)
        cell.question_Id = self.questionObj[indexPath.row].que_Id
        cell.is_answer = self.questionObj[indexPath.row].is_answer
        if self.questionObj[indexPath.row].is_answer == true
        {
            cell.heightofsubmit.constant = 0.0
        }
        else
        {
            cell.heightofsubmit.constant = 40.0
        }
        return cell
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
}


extension PollVC
{
    func hitServicePoll(flag:String)
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2837521732, blue: 0.44864434, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [ "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Content-Type": "application/json"]
        let params : Parameters = ["flag":flag]
        //"question?flag="+flag
        ServiceManager.instance.request(method: .get, URLString: "question", parameters: params, encoding: URLEncoding.default, headers: headers) { (sucess, response, error) in
            KRProgressHUD.dismiss()
            if (sucess)!{
                if let statuscode = response?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = response?["data"] as? [String : Any]{
                            if let questionsArry = data["questions"] as? [[String:Any]]
                            {
                                self.questionObj.removeAll()
                                for allque in questionsArry
                                {
                                    let obj = QuestionAnswer()
                                    if let id = allque["id"] as? Int
                                    {
                                        obj.que_Id = String(id)
                                    }
                                    if let questions = allque["question"] as? String
                                    {
                                        obj.question = questions
                                    }
                                    if let is_selected = allque["is_answer"] as? Bool
                                    {
                                        obj.is_answer = is_selected
                                    }
                                    if let optionArr = allque["options"] as? [[String:Any]]
                                    {
                                        for option in optionArr
                                        {
                                            let ans = Answer()
                                            if let options = option["option"] as? String
                                            {
                                                ans.option = options
                                            }
                                            if let option_id = option["id"] as? Int
                                            {
                                                ans.option_id = String(option_id)
                                            }
                                            if let is_selected = option["is_selected"] as? Bool
                                            {
                                                ans.is_selected = is_selected
                                            }
                                            if let percentage = option["percentage"] as? Int
                                            {
                                                ans.percentage = String(percentage)
                                            }
                                            obj.answer.append(ans)
                                        }
                                    }
                                    self.questionObj.append(obj)
                                }
                                self.pollsTable.reloadData()
                            }
                        }
                    }
                    else
                    {
                        if let message = response?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                    }
                }
            }
            else{
                print("error")
            }
        }
    }
}
