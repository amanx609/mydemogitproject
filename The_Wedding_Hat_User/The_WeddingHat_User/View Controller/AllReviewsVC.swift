//
//  AllReviewsVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 21/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class AllReviewsVC: UIViewController {

    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    
    var isExpand = false
    var isExpandArr = [Bool]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headView.dropShadowBlueColor()
        self.tblView.reloadData()
        self.addButton.makeRounded()

    }
    

    

}
extension AllReviewsVC : UITableViewDelegate , UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            if(isExpand == false){
                return 5
            }else{
                return 1
            }
        }else{
            if(isExpand == false){
                return 6
            }
            else{
                return 2
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        
        if(indexPath.section == 0){
            if(indexPath.row == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserRatingCell", for: indexPath) as! UserRatingCell
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserRatingTblViewCell", for: indexPath) as! UserRatingTblViewCell
//                if(indexPath.row == 1){
//                    cell.mainView.layer.maskedCorners = [.layerMinXMinYCorner , .layerMaxXMinYCorner]
//                }
//                if(indexPath.row == 4){
//                    cell.mainView.layer.maskedCorners = [.layerMaxXMaxYCorner , .layerMinXMaxYCorner]
//
//                }
                return cell
            }
        }else{
                if(indexPath.row == 0){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "UserDetailsRatingCell", for: indexPath) as! UserDetailsRatingCell
                              return cell
                }else if(indexPath.row == 5){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DescTblCell", for: indexPath) as! DescTblCell
                    return cell
                }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserRatingTblViewCell", for: indexPath) as! UserRatingTblViewCell
                    cell.mainView.dropShadowBlueColor()
                    return cell
            }
        }
            //else{
//                if(indexPath.row == 0){
//                    let cell = tableView.dequeueReusableCell(withIdentifier: "UserDetailsRatingCell", for: indexPath) as? UserDetailsRatingCell
//                    return cell!
//                }else if(indexPath.row == 5 ){
//                    let cell = tableView.dequeueReusableCell(withIdentifier: "DescTblCell", for: indexPath) as? DescTblCell
//                    return cell!
//                }else{
//                    let cell = tableView.dequeueReusableCell(withIdentifier: "UserRatingTblViewCell", for: indexPath) as? UserRatingTblViewCell
//                    return cell!
//                }
//
//            }
            
        }
        
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.reloadData()
    }
}









