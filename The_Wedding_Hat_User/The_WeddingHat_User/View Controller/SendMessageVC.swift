//
//  SendMessageVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 07/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

class SendMessageVC: UIViewController,UITextViewDelegate {
    
    
    @IBOutlet weak var txtView: UIView!
    @IBOutlet weak var send_btn: UIButton!
    @IBOutlet weak var header_lbl: UILabel!
    @IBOutlet weak var cancel_btn: UIButton!
    @IBOutlet weak var meassageView: UIView!
    @IBOutlet weak var event_dateView: UIView!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var eventDateTxt: UITextField!
    
    var vendor_id = ""
    var inventroy_id = ""
    var event_typeID = ""
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.txtMessage.delegate = self
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100))
        {
            self.meassageView.dropShadow()
            self.txtMessage.text = "Your query here..."
            self.eventDateTxt.colorPlaceHolder(placeholder:"Event Date",color:"#00375F")
            self.cancel_btn.addDashBorder(color: UIColor(hexString: "#00375F")!)
            self.txtMessage.textColor = UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0)
            self.eventDateTxt.setDatePickerMode(mode: .date)
            self.eventDateTxt.setDatePickerWithDateFormate(dateFormate: "dd-MM-YYYY", defaultDate: .none, isPrefilledDate: false)
            { (selecteddate) in
               
            }
            
        }
    }
    @IBAction func tapOnCancel(_ sender: Any)
    {
        self.dismiss(animated: true) { }
    }
    
    @IBAction func tapOnSend(_ sender: Any)
    {
        self.HitserviceForSendQuery()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if txtMessage.text == "Your query here..."
        {
            self.txtMessage.text = nil
            self.txtMessage.textColor = UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0)
        }
    }
    func textViewDidChange(_ textView: UITextView)
    {
        print(self.txtMessage.text!)
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if txtMessage.text.isEmpty
        {
            self.txtMessage.text = "Your query here..."
            self.txtMessage.textColor = UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0)
        }
    }
    
}


extension SendMessageVC
{
    func HitserviceForSendQuery()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        let headers: HTTPHeaders = [ "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Content-Type": "application/json"]
        
        let parameters =
            [
                "event_type_id" : self.event_typeID,
                "vendor_id" : self.vendor_id,
                "inventory_id" :self.inventroy_id,
                "user_id" : CommonUtilities.shared.getuserdata()?.id ?? "",
                "query" : self.txtMessage.text!,
                "event_date" : self.eventDateTxt.text!
                
                ] as [String : Any]
        print(parameters)
        ServiceManager.instance.request(method: .post, URLString: "sendQuery", parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!
            {
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        let alert = UIAlertController(title: Constant.string.appname.localized(), message: "Successfully query is sent.", preferredStyle: .alert)
                        self.present(alert, animated: true, completion: nil)
                        let when = DispatchTime.now() + 2
                        DispatchQueue.main.asyncAfter(deadline: when){
                            alert.dismiss(animated: true, completion: nil)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                    }
                }
            }
            else
            {
                print(parameters)
            }
        }
    }
}
