//
//  VendorCategoryVC.swift
//  The_WeddingHat_User
//
//  Created by apple on 07/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class VendorCategoryVC: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var searchTopView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchTopView.makeRoundCorner(5.0)
        self.searchTopView.dropShadow()
//        self.tblView.delegate = self
//        self.tblView.dataSource = self

    }
    


}

//extension VendorCategoryVC : UITableViewDelegate , UITableViewDataSource{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 10
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "VendorCategoryTableCell", for: indexPath) as! VendorCategoryTableCell
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 60
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CategoryNameVC") as! CategoryNameVC
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc , animated: true , completion: nil)
//    }
//}

extension VendorCategoryVC{
    
    @IBAction func backButtonTap(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
