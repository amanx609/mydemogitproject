//
//  ApplicationMessages.swift
//  CollectMee
//
//  Created by mac-0007 on 12/12/17.
//  Copyright © 2017 mac-0007. All rights reserved.
//

import Foundation

//..... GENERAL .....

var CError  :String { return CLocalize(text: "ERROR!") }
var CMessageRetry  :String { return CLocalize(text: "An error has occured. Please check your network connection or try again.") }

var CMessageNoResultFound1 : String { return CLocalize(text:  "Nothing to see here yet!") }
var CMessageNoResultFound2 : String { return CLocalize(text:  "Sorry we couldn't find any result here!\ntap anywhere to refresh the page.") }
var CMessageNoInternet : String { return CLocalize(text:  "Please check your internet connection or tap anywhere to refresh the page.") }
var CMessageOtherError : String { return CLocalize(text:  "Something wrong here...\ntap anywhere to refresh the page.") }
var CMessageWhatsAppNotSupport : String { return CLocalize(text:  "Your device is not support whatsapp.") }
var CMessageEmailNotSupport : String { return CLocalize(text:  "Your device does not support email feature.") }
var CMessageChangeLanguage : String { return CLocalize(text:  "Are you sure you want to change your language?") }
var CMessageDelete : String { return CLocalize(text:  "Are you sure want to delete?") }
var CMessageLogout : String { return CLocalize(text:  "Are you sure want to logout?") }



//..... LRF .....

var CMessageBlankEmail : String { return CLocalize(text:  "Please enter Email Id.") }
var CMessageInvalidEmail : String { return CLocalize(text:  "Please enter valid Email Id.") }
var CMessageEmailNotRegister : String { return CLocalize(text:  "Email Id is not registered with us.") }
var CMessageBlankPassword : String { return CLocalize(text:  "Please enter Password.") }
var CMessageInvalidPassword : String { return CLocalize(text:  "Password is invalid.") }
var CMessageEmailPasswordNotMatch : String { return CLocalize(text:  "Email and password combination does not match.") }

var CMessageBlankResetCode : String { return CLocalize(text:  "Please enter Reset Code.") }
var CMessageBlankNewPassword : String { return CLocalize(text:  "Please enter New Password.") }
var CMessageBlankConfirmPassword : String { return CLocalize(text:  "Please enter Confirm Password.") }
var CMessagePasswordDoNotMatch : String { return CLocalize(text:  "Password and Confirm Password must be same.") }
var CMessagePasswordsDoNotMatch : String { return CLocalize(text:  "New Password and Confirm Password must be same.") }
var CMessagePasswordsCharacterLimit : String { return CLocalize(text:  "Password should be minimum 6 alphanumeric characters.") }

var CMessageBlankName : String { return CLocalize(text:  "Please enter Name.") }
var CMessageBlankFirstName : String { return CLocalize(text:  "Please enter First Name.") }
var CMessageBlankLastName : String { return CLocalize(text:  "Please enter Last Name.") }
var CMessageBlankContactNumber : String { return CLocalize(text:  "Please enter Contact Number.") }
var CMessageValidContactNumber : String { return CLocalize(text:  "Please enter valid Contact Number.") }
var CMessageBlankCountry : String { return CLocalize(text:  "Please select Country.") }
var CMessageBlankGender : String { return CLocalize(text:  "Please select Gender.") }
var CMessageBlankDateOfBirth : String { return CLocalize(text:  "Please select Date of Birth.") }
var CMessageInvalidReferralCode : String { return CLocalize(text:  "Please enter valid Referral Code.") }
var CMessageBlankInterest : String { return CLocalize(text:  "Please select Interest.") }
var CMessageInvalidMobileNumber : String { return CLocalize(text:  "Please enter valid mobile number.") }

var CMessageBlankOldPassword : String { return CLocalize(text:  "Please enter Old Password.") }
var CMessageBlankPromoCode : String { return CLocalize(text:  "Please enter Promo Code.") }

var CMessageSelectContact : String { return CLocalize(text:  "Please select atleast one contact for invitation.") }
var CMessageBlankInviteMessage : String { return CLocalize(text:  "Message can’t be blank.") }
var CMessageBlankMessage : String { return CLocalize(text:  "Please enter your feedback.") }
var CMessageSentFeedback : String { return CLocalize(text:  "Thank you for writing. We appreciate your feedback.") }

var CMessageSeletGiftCard : String { return CLocalize(text:  "Please select one Gift Card.") }
var CMessageSeletProduct : String { return CLocalize(text:  "Please select product.") }
//var CMessageSeletProduct : String { return CLocalize(text:  "Please select one Product.") }
var CMessageBlankLocation : String { return CLocalize(text:  "Please select your location.") }
var CMessageSelectContactsLimit : String { return CLocalize(text:  "You can't select more than 10 contacts") }

var CMessageNoResultFound : String { return CLocalize(text:  "Sorry, No results found.") }

var CBtnOk      :String { return CLocalize(text: "OK") }
var CBtnCancel  :String { return CLocalize(text: "Cancel") }
var CBtnYes     :String { return CLocalize(text: "Yes") }
var CBtnNo      :String { return CLocalize(text: "No") }
var CBtnRetry   :String { return CLocalize(text: "RETRY") }
var CBtnCANCEL  :String { return CLocalize(text: "CANCEL") }

var CGenderMale      : String { return CLocalize(text:  "Male") }
var CGenderFemale    : String { return CLocalize(text:  "Female") }


var CNoProducts             : String { return CLocalize(text:  "Products are not available.") }
var CProductAddedToCart     : String { return CLocalize(text:  "Product added in your cart.") }


var CProductAddedToWish  : String { return CLocalize(text:  "Product added to wish list. Thank you for showing interest in our product.") }
var CProductRemoveFromWish  : String { return CLocalize(text:  "Product removed from wish list.") }
var CSelectCapacity  : String { return CLocalize(text:  "Please select capacity.") }
var CSelectSize  : String { return CLocalize(text:  "Please select size.") }



var CDeleteMees     : String { return CLocalize(text:  "Are you sure you want to remove mees?") }
var CDeleteItem     : String { return CLocalize(text:  "Are you sure you want to remove this item?") }
var CCancelOrder    : String { return CLocalize(text:  "Are you sure you want to cancel this order?") }
var CCancelReason    : String { return CLocalize(text:  "Reason for cancel") }
var CMeesNotAvailable    : String { return CLocalize(text:  "You can’t enter Mee’s more than available Mee’s.") }



var CBlankPermanentAddress  : String { return CLocalize(text:  "Please add permanent address.") }
var CCountryNotMatched : String { return CLocalize(text:  "Selected country not matched with profile.") }
var CBlankDeliveryAddress   : String { return CLocalize(text:  "Please add delivery address.") }
var CBlankCountry           : String { return CLocalize(text:  "Please select country.") }











