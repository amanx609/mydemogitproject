//
//  ApplicationConstants.swift
//  CollectMee
//
//  Created by mac-0007 on 12/12/17.
//  Copyright © 2017 mac-0007. All rights reserved.
//

import Foundation
import UIKit

//MARK:-
//MARK:- Disable print for production.

func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    #if DEBUG
        Swift.print(items[0], separator:separator, terminator: terminator)
    #endif
}


//MARK:-
//MARK:- Status

let CJsonResponse       = "response"
let CJsonMessage        = "message"
let CJsonStatus         = "status"
let CStatusCode         = "status_code"
let CJsonTitle          = "title"
let CJsonData           = "data"

let CLimit              = 20
let CStatusZero         = 0
let CStatusOne          = 1
let CStatusTwo          = 2
let CStatusThree        = 3
let CStatusFour         = 4
let CStatusFive         = 5
let CStatusEight        = 8
let CStatusNine         = 9
let CStatusTen          = 10
let CStatusEleven       = 11
let CStatusTwelve       = 12

let CDeviceType         = 2

let CStatusTwoHundred   = NSNumber(value: 200 as Int)       //  Success
let CStatusFourHundredAndOne = NSNumber(value: 401 as Int)     //  Unauthorized user access
let CStatusFiveHundredAndFiftyFive = NSNumber(value: 555 as Int)   //  Invalid request
let CStatusFiveHundredAndFiftySix = NSNumber(value: 556 as Int)   //  Invalid request
let CStatusFiveHundredAndFifty = NSNumber(value: 550 as Int)        //  Inactive/Delete user
let CStatusFiveHunred   = NSNumber(value: 500 as Int)



//MARK:-
//MARK:- Fonts
enum CFontAvenirLTSTDType:Int {
    case Black
    case Heavy
    case Light
    case Medium
    case Roman
}

func CFontAvenirLTSTD(size: CGFloat, type: CFontAvenirLTSTDType) -> UIFont {
    switch type {
    case .Black:
        return UIFont.init(name: "AvenirLTStd-Black", size: size)!
    case .Heavy:
        return UIFont.init(name: "AvenirLTStd-Heavy", size: size)!
    case .Light:
        return UIFont.init(name: "AvenirLTStd-Light", size: size)!
    case .Medium:
        return UIFont.init(name: "AvenirLTStd-Medium", size: size)!
    default:
        return UIFont.init(name: "AvenirLTStd-Roman", size: size)!
    }
}


//MARK:-
//MARK:- Notification Constants

let NotificationDidUpdateLocation       = "NotificationDidUpdateLocation"
let NotificationDidUpdateUserDetail     = "NotificationDidUpdateUserDetail"



//MARK:-
//MARK:- UserDefaults Keys

let UserDefaultTutorialScreen       = "UserDefaultTutorialScreen"
let UserDefaultLoginUserToken       = "UserDefaultLoginUserToken"
let UserDefaultLoginUserID          = "UserDefaultLoginUserID"
let UserDefaultLanguageID           = "UserDefaultLanguageID"
let UserDefaultDeviceToken          = "UserDefaultDeviceToken"
let UserDefaultDevicePlayerId       = "UserDefaultDevicePlayerId"
let UserDefaultCurrentLatitude      = "UserDefaultCurrentLatitude"
let UserDefaultCurrentLongitude     = "UserDefaultCurrentLongitude"



//MARK:-
//MARK:- Color
let ColorBlue           = CRGB(r: 54, g: 151, b: 195)
let ColorGray           = CRGB(r: 188, g: 192, b: 193)
let ColorBlack          = CRGB(r: 0, g: 0, b: 0)
let ColorWhite          = CRGB(r: 255, g: 255, b: 255)
let ColorDarkPink       = CRGB(r: 194, g: 14, b: 114)
let ColorLightBlue      = CRGB(r: 125, g: 179, b: 209)
let Color_86ADBA        = CRGB(r: 134, g: 173, b: 186)
let Color_D5ECF2        = CRGB(r: 213, g: 236, b: 242)
let Color_40C4B2        = CRGB(r: 64, g: 196, b: 178)
let Color_Attr_Border   = CRGB(r: 240, g: 240, b: 240).cgColor




//MARK:-
//MARK:- UIStoryboard Constant
let CTutorial_SB    = UIStoryboard(name: "Tutorial", bundle:  nil)
let CLRF_SB         = UIStoryboard(name: "LRF", bundle:  nil)
let CMain_SB        = UIStoryboard(name: "Main", bundle:  nil)
let CSettings_SB    = UIStoryboard(name: "Settings", bundle:  nil)
let CPromotion_SB   = UIStoryboard(name: "Promotion", bundle:  nil)
let CInviteGet_SB   = UIStoryboard(name: "InviteGet", bundle:  nil)
let CCart_SB        = UIStoryboard(name: "Cart", bundle:  nil)


//MARK:-
//MARK:- Application Language
let CLanguageArabic            = "ar"
let CLanguageEnglish           = "en"



//MARK:-
//MARK:- Other
let CComponentJoinedString          = ", "




//MARK:-
//MARK:- Reward and Product Cell Ratio

let CRewardCellRatio            = CGSize(width: ((CScreenWidth - 1) / 2), height: (CScreenWidth * 133) / 375)
let CProductCellRatio           = CGSize(width: IS_iPhone_5 || IS_iPhone_6 ? CProductCellWidthRatio5s : CProductCellWidthRatio, height: CProductCellHeightRatio)
let CProductCellWidthRatio      = ((CScreenWidth * 172) / 375)
let CProductCellWidthRatio5s    = ((CScreenWidth * 172) / 375) - 2
let CProductCellHeightRatio     = (CScreenWidth * 240) / 375

let CProductCellWidth           = (CScreenWidth * (174 / 375))
let CProductCellHeight          = (CProductCellWidth * (240 / 174))
let CProductCellSpacing         = (CScreenWidth - (CProductCellWidth * 2)) / 3
let CRewardCellSectionInset     = UIEdgeInsets(top: 0, left: 0, bottom: 1, right: 0)
let CProductCellSectionInset    = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
let CProductCellLineSpacing     = 10
let CRewardCellLineSpacing      = 1





let CAccountTypeNormal       = 0
let CAccountTypeFacebook     = 1
let CAccountTypeGoogle       = 2
let CAccountTypeInstagram    = 3



let COrderStatusPending         = 1
let COrderStatusPlaced          = 2
let COrderStatusConfirmed       = 3
let COrderStatusDispatched      = 4
let COrderStatusOutForDelivery  = 5
let COrderStatusDelivered       = 6
let COrderStatusCancelled       = 7



func CLocalize(text: String) -> String {
    return Localization.sharedInstance.localizedString(forKey: text , value: text)
}




