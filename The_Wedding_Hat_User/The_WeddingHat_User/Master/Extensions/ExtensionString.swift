//
//  ExtensionString.swift
//  Swifty_Master
//
//  Created by Mind-0002 on 01/09/17.
//  Copyright © 2017 Mind. All rights reserved.
//

import Foundation

// MARK: - Extension of String For Converting it TO Int AND URL.
extension String {
    
    /// A Computed Property (only getter) of Int For getting the Int? value from String.
    /// This Computed Property (only getter) returns Int? , it means this Computed Property (only getter) return nil value also , while using this Computed Property (only getter) please use if let. If you are not using if let and if this Computed Property (only getter) returns nil and when you are trying to unwrapped this value("Int!") then application will crash.
    var toInt:Int? {
        return Int(self)
    }
    
    var toDouble:Double? {
        return Double(self)
    }
    
    var toFloat:Float? {
        return Float(self)
    }
    
    
    /// A Computed Property (only getter) of URL For getting the URL from String.
    /// This Computed Property (only getter) returns URL? , it means this Computed Property (only getter) return nil value also , while using this Computed Property (only getter) please use if let. If you are not using if let and if this Computed Property (only getter) returns nil and when you are trying to unwrapped this value("URL!") then application will crash.
    var toURL:URL? {
        return URL(string: self)
    }
    
}

extension String {
    
    var trim:String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    
    var isBlank:Bool {
        return self.trim.isEmpty
    }
    
    var isAlphanumeric:Bool {
      return !isBlank && rangeOfCharacter(from: .alphanumerics) != nil
    }
    
    var isAlpha:Bool {
        let allowed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        let characterSet = CharacterSet(charactersIn: allowed)
        return !isBlank && rangeOfCharacter(from: characterSet) != nil
    }
    
    var isValidEmail:Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let predicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return predicate.evaluate(with:self)
    }
    
    var isValidPhoneNo:Bool {
        
        let phoneCharacters = CharacterSet(charactersIn: "+0123456789").inverted
        let arrCharacters = self.components(separatedBy: phoneCharacters)
        return self == arrCharacters.joined(separator: "")
    }
    
//    func setStrikeLineInlbl(text: String) -> NSAttributedString {
//        
//        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: text)
//        attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
//        attributeString.addAttribute(NSAttributedStringKey.strokeColor, value: Color_86ADBA, range: NSMakeRange(0, attributeString.length))
//        return attributeString
//    }
    
}


extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: String.Encoding.unicode) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
            
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
 
    static func dateStringFrom(timestamp: String, withFormate:String) -> String {
        let fromDate:Date = Date(timeIntervalSince1970: Double(timestamp)!)
        
        if Localization.sharedInstance.getLanguage() == CLanguageArabic {
            DateFormatter.shared().locale = Locale(identifier: "ar_DZ")
        } else {
            DateFormatter.shared().locale = NSLocale.current
        }
        
        return DateFormatter.shared().string(fromDate: fromDate, dateFormat: withFormate)
    }
    
    func dateFrom(timestamp: String) -> Date? {
        let fromDate:Date = Date(timeIntervalSince1970: Double(timestamp)!)
        let stringDate = DateFormatter.shared().string(fromDate: fromDate, dateFormat: "dd MMM, YYYY")
        return DateFormatter.shared().date(fromString: stringDate, dateFormat: "dd MMM, YYYY")
    }
    
    func durationString(duration: String) -> String
    {
        let calender:Calendar = Calendar.current as Calendar
        let fromDate:Date = Date(timeIntervalSince1970: Double(duration)!)
        let unitFlags = Set<Calendar.Component>([.year, .month, .day, .hour, .minute])
        let dateComponents = calender.dateComponents(unitFlags, from:fromDate , to: Date())
        
        let years:NSInteger = dateComponents.year!
        let months:NSInteger = dateComponents.month!
        let days:NSInteger = dateComponents.day!
        let hours:NSInteger = dateComponents.hour!
        let minutes:NSInteger = dateComponents.minute!
        
//        print(calender.date(from: dateComponents)!)
        print(DateFormatter.shared().string(fromDate: DateFormatter.shared().dateGMT(fromString: DateFormatter.shared().stringGMT(fromDate: calender.date(from: dateComponents)!, dateFormat: "dd MMM, HH:mm"), dateFormat: "dd MMM, HH:mm")!, dateFormat: "dd MMM, HH:mm"))
        
        return DateFormatter.shared().string(fromDate: DateFormatter.shared().date(fromString: DateFormatter.shared().stringGMT(fromDate: calender.date(from: dateComponents)!, dateFormat: "dd MMM, hh:mm a"), dateFormat: "dd MMM, hh:mm a")!, dateFormat: "dd MMM, hh:mm a")
        
//        return DateFormatter.shared().stringGMT(fromDate: calender.date(from: dateComponents)!, dateFormat: "dd MMM, hh:mm a")
        
        var durations:NSString = "Just Now"
        
        if (years > 0) {
            durations = "\(years) years ago" as NSString
        }
        else if (months > 0) {
            durations = "\(months) months ago" as NSString
        }
        else if (days > 0) {
            durations = "\(days) days ago" as NSString
        }
        else if (hours > 0) {
            durations = "\(hours) hours ago" as NSString
        }
        else if (minutes > 0) {
            durations = "\(minutes) mins ago" as NSString
        }
        
        return durations as String;
    }
    
    
    func maskingEmail(email: String) -> String {
        let emailComponents = email.components(separatedBy: "@")
        let emailDomainComponents = emailComponents[1].components(separatedBy: ".")
       
        let maskedEmailName = String(emailComponents[0].prefix(1)) + String("***") + emailComponents[0].suffix(1)
        
//        let maskedEmailName = String(repeating: "*", count: Swift.max(0, emailComponents[0].count-3)) + emailComponents[0].suffix(3)
        let maskedEmailProvider = String(repeating: "*", count: Swift.max(0, emailDomainComponents[0].count-3)) + emailDomainComponents[0].suffix(3)
        let emailDomain = emailDomainComponents[1]
        return "\(maskedEmailName)@\(maskedEmailProvider).\(emailDomain)"
    }
    
}

extension String{

    private static let decimalFormatter:NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.allowsFloats = true
        return formatter
    }()

    private var decimalSeparator:String{
        return String.decimalFormatter.decimalSeparator ?? "."
    }

    func isValidDecimal(maximumFractionDigits:Int)->Bool{

        // Depends on you if you consider empty string as valid number
        guard self.isEmpty == false else {
            return true
        }

        // Check if valid decimal
        if let _ = String.decimalFormatter.number(from: self){

            // Get fraction digits part using separator
            let numberComponents = self.components(separatedBy: decimalSeparator)
            let fractionDigits = numberComponents.count == 2 ? numberComponents.last ?? "" : ""
            return fractionDigits.count <= maximumFractionDigits
        }

        return false
    }

    func currencyInputFormatting() -> String {

            var number: NSNumber!
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.currencySymbol = "$"
    //        formatter.maximumFractionDigits = 0
    //        formatter.minimumFractionDigits = 1

            var amountWithPrefix = self

            let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
            amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")

            let double = (amountWithPrefix as NSString).doubleValue
            number = NSNumber(value: (double / 1))


            guard number != 0 as NSNumber else {
                return ""
            }

            return formatter.string(from: number)!
        }
    
    
}


