//
//  OnBoardingScreen.swift
//  The_WeddingHat_User
//
//  Created by appl on 05/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import KRProgressHUD
import Localize_Swift


class OnBoardingScreen: UIViewController {
    
    @IBOutlet weak var quoteLbl: UILabel!
    @IBOutlet weak var authorlbl: UILabel!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var languageLbl: UILabel!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var selectedLanguagelbl: UILabel!
    let dropDown = DropDown()
    var selectedindex = Int()
    var objModelQuotes = [ModelQuotes]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.quoteLbl.alpha = 0.0
        self.authorlbl.alpha = 0.0
        self.startBtn.makeRoundCorner(10)
        self.languageView.makeRoundCorner(10)
        self.hitServicegetQuotesData()
        
    }
    
    @IBAction func tapOnLanguage(_ sender: Any)
    {
//        self.setlanguage()
//        self.LanguageActionSheet()

    }
    @IBAction func tapOnLetsStart(_ sender: Any)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
  
    func setlanguage()
    {
        dropDown.anchorView = languageView
        dropDown.dataSource = ["English","Arabic"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectedLanguagelbl.text = item
            if index == 0{
                //                UserDefaults.standard.set(1, forKey: "selectedcountryLanguage")
                //                Localize.setCurrentLanguage("en")
            }else if index == 1{
                UserDefaults.standard.set(2, forKey: "selectedcountryLanguage")
                Localize.setCurrentLanguage("ar")
            }else{
                //                UserDefaults.standard.set(1, forKey: "selectedcountryLanguage")
                //                Localize.setCurrentLanguage("en")
            }
        }

        dropDown.animationduration = 0.20
        dropDown.show()
    }
    
//    func LanguageActionSheet(){
//        self.presentActionsheetWithTwoButtons(actionSheetTitle: "", actionSheetMessage: "Select Language", btnOneTitle: "English", btnOneStyle: .default, btnOneTapped: { (UIAlertAction) in
//            self.selectedLanguagelbl.text = "English"
////            UserDefaults.standard.set(1, forKey: "selectedcountryLanguage")
////            Localize.setCurrentLanguage("en")
//        }, btnTwoTitle: "Arabic", btnTwoStyle: .default) { (UIAlertAction) in
//            self.selectedLanguagelbl.text = "Arabic"
////            UserDefaults.standard.set(2, forKey: "selectedcountryLanguage")
////            Localize.setCurrentLanguage("ar")
//        }
//    }
    
    func setData()
    {
        _ =  Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.animation), userInfo: nil, repeats: true)
    }
    
    @objc func animation()
    {
        if self.selectedindex == self.objModelQuotes.count - 1
        {
            self.selectedindex = 0
        }
        else
        {
           self.selectedindex  = self.selectedindex + 1
        }
        UIView.animate(withDuration: 2.0, animations: {
            self.quoteLbl.alpha = 1.0
            self.authorlbl.alpha = 1.0
            self.quoteLbl.text = self.objModelQuotes[self.selectedindex].quotes
            self.authorlbl.text = self.objModelQuotes[self.selectedindex].author_name
        }, completion: nil)
    }
}


extension OnBoardingScreen
{
    func hitServicegetQuotesData()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .get, URLString: "getAllQuotes", parameters:nil, encoding: JSONEncoding.default, headers: nil)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!{
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = dictionary?["data"] as? [String:Any]{
                            if let quotesData = data["quotes"] as? [[String:Any]]
                            {
                                for quotes in quotesData
                                {
                                    let obj = ModelQuotes()
                                    if let id = quotes["id"] as? Int
                                    {
                                        obj.id = String(id)
                                    }
                                    if let status = quotes["status"] as? Int
                                    {
                                        obj.status = String(status)
                                    }
                                    if let quotes = quotes["quotes"] as? String
                                    {
                                        obj.quotes = quotes
                                    }
                                    if let author_name = quotes["author_name"] as? String
                                    {
                                        obj.author_name = author_name
                                    }
                                    if let updated_at = quotes["updated_at"] as? String
                                    {
                                        obj.updated_at = updated_at
                                    }
                                    if let created_at = quotes["created_at"] as? String
                                    {
                                        obj.created_at = created_at
                                    }
                                    self.objModelQuotes.append(obj)
                                }
                                self.setData()
                            }
                        }
                    }
                }
                else
                {
                    if let message = dictionary?["message"] as? String
                    {
                        self.createAlert(message: message)
                    }
                }
            }
        }
    }
}


class ModelQuotes
{
    var id = ""
    var status = ""
    var quotes = ""
    var updated_at = ""
    var author_name = ""
    var created_at = ""
    
}
