//
//  SelelctServiceColl.swift
//  The_WeddingHat_User
//
//  Created by appl on 11/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class SelelctServiceColl: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var cat_lbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib()
    {
        self.imgView.makeRounded()
        self.mainView.makeRoundCorner(8)
        self.mainView.planningDropShadow()
    }
    
}
