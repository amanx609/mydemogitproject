//
//  OptionCollCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 26/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class OptionCollCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var option_lbl: UILabel!
    @IBOutlet weak var img_click: UIImageView!
    @IBOutlet weak var percent_lbl: UILabel!
    
    override func awakeFromNib() {
        self.mainView.makeRoundCorner(8)
        self.mainView.dropShadow()
    }
    
    
    func createBorder()
    {
        self.mainView.layer.borderWidth = 2
        self.mainView.layer.borderColor = UIColor(red: 204/255, green: 148/255, blue: 146/255, alpha: 1).cgColor
    }
    
    func removeBorder()
    {
        self.mainView.layer.borderWidth = 0
        self.mainView.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1).cgColor
    }
}
