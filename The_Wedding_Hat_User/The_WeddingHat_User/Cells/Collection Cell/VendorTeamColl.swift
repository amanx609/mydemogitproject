//
//  VendorTeamColl.swift
//  The_WeddingHat_User
//
//  Created by appl on 01/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class VendorTeamColl: UICollectionViewCell {
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var main_view: UIView!
    @IBOutlet weak var name_lbl: UILabel!
    @IBOutlet weak var loc: UILabel!
    @IBOutlet weak var img_view: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var rating_view: UIView!
    @IBOutlet weak var rating_lbl: UILabel!
    @IBOutlet weak var review: UIView!
    @IBOutlet weak var review_lbl: UILabel!
    @IBOutlet weak var cat_lbl: UILabel!
    
    override func awakeFromNib() {
        self.main_view.makeRoundCorner(8)
        self.main_view.dropShadow()
        self.review.makeRoundCorner(4)
        self.rating_view.makeRoundCorner(3)
    }
    
}
