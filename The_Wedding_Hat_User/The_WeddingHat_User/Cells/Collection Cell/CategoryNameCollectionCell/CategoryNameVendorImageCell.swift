//
//  CategoryNameVendorImageCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 10/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class CategoryNameVendorImageCell: UICollectionViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var bookMarkButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    override func awakeFromNib() {
        self.mainView.dropShadow()
    }
}
