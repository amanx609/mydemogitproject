//
//  SubmitWeddingVendorsCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 13/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class SubmitWeddingVendorsCell: UICollectionViewCell {
    
    @IBOutlet weak var vendorNameLbl: UILabel!
    @IBOutlet weak var tickImage: UIImageView!
    @IBOutlet weak var viewInside: UIView!
    
    override func awakeFromNib() {
        toggleSelected()
    }
    func toggleSelected()
    {
     if (isSelected){
         self.tickImage.image = #imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle")
        }else {
         self.tickImage.image = #imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle-outline")
        }
    }
}
