//
//  UploadImageCollectionCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 13/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class UploadImageCollectionCell: UICollectionViewCell {
   
    @IBOutlet weak var uploadImage: UIImageView!
   
    override func awakeFromNib()
    {
        self.contentView.layer.borderWidth = 2.0
        self.contentView.layer.borderColor = #colorLiteral(red: 0, green: 0.2837521732, blue: 0.44864434, alpha: 1)
        self.contentView.makeRoundCorner(10.0)
        
    }
}
