//
//  AddDiscussionUploadImageCollCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 01/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import YangMingShan
import OpalImagePicker

class AddDiscussionUploadImageCollCell: UICollectionViewCell {
    
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var uploadButton : UIButton!
    @IBOutlet weak var addImageButton: UIButton!
    
    
    var documentsPicArray = [UIImage]()

    override func awakeFromNib() {
        self.imageView.makeRoundCorner(8)
        self.imageView.layer.borderWidth = 1
        self.imageView.layer.borderColor = UIColor(red: 0.0/255.0, green: 55.0/255.0, blue: 95.0/255.0, alpha: 1.0).cgColor
        
    }
}

