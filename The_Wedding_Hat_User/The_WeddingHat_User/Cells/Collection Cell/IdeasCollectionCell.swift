//
//  IdeasCollectionCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 10/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class IdeasCollectionCell: UICollectionViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellLabel: UILabel!
    
    override func awakeFromNib() {
        self.setDesign()
    }
    func setDesign(){
        cellView.makeRoundCorner(10.0)
        cellView.dropShadow(shadowColor: UIColor(named: "shadowColor")!, borderColor: UIColor(hexString: "#054D821A")! , opacity: 0.5, offSet: .zero, radius: 2.0, scale: true)
    }

    
    }
