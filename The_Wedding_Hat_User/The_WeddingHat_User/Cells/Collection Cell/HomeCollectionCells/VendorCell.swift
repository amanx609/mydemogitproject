//
//  VendorCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 04/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class VendorCell: UITableViewCell {

    @IBOutlet weak var mainVieww: UIView!
    @IBOutlet weak var vendorIcon: UIImageView!
    @IBOutlet weak var mainHeading: UILabel!
    @IBOutlet weak var desclbl: UILabel!
    @IBOutlet weak var pointsLbl: UILabel!
    @IBOutlet weak var totalPoints: UILabel!
    @IBOutlet weak var VendorColl: UICollectionView!
    var venuArr = ["Venue","DJ","Venue","DJ"]
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUI()
        self.VendorColl.delegate = self
        self.VendorColl.dataSource = self
    }
    func setUI(){
        self.mainVieww.makeRoundCorner(10)
        self.mainVieww.dropShadow()
        self.vendorIcon.makeRounded()
        self.mainHeading.text = Constant.string.vendorTeam.localized()
        self.desclbl.text = Constant.string.vendorHired.localized()
    }
}

extension VendorCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return venuArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VendorCollCell", for: indexPath) as! VendorCollCell
        cell.nameLbl.text = self.venuArr[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let text = self.venuArr[indexPath.row]//self.ObjcategoryArray[indexPath.row - 1].category_name
            let width = self.estimatedFrame(text: text, font: UIFont(name: "Montserrat-Regular", size: 26.0)!).width
            print(width)
        return CGSize(width: width + 10.0, height: 50.0)
        
    }
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
        let size = CGSize(width: 200, height: 1000) // temporary size
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size,
                                                   options: options,
                                                   attributes: [NSAttributedString.Key.font: font],
                                                   context: nil)
    }
    
}

