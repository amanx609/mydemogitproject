//
//  EventCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 04/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

protocol pushToEventVC {
    func pushtoVC()
}
class EventCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var editLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var eventPic: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var daysLbl: UILabel!
    @IBOutlet weak var emptyVIew: UIView!
    @IBOutlet weak var emptyMsgLbl: UILabel!
    @IBOutlet weak var createEventBtn: UIButton!
    @IBOutlet weak var topOfLbl: NSLayoutConstraint!
    @IBOutlet weak var completeEventLbl: UILabel!
    @IBOutlet weak var brideNameLbl: UILabel!
    @IBOutlet weak var uniqueCode: UILabel!
    @IBOutlet weak var andLbl: UILabel!
    @IBOutlet weak var eventImageConstraints: NSLayoutConstraint!
    static let  identifier = "EventCell"
    var modeleventobj = ModelEventList()
    var delgate : pushToEventVC?
    override func awakeFromNib() {
        self.imgView.makeRoundCorner(10)
        self.editView.makeRoundCorner(6)
        self.mainView.makeRoundCorner(10)
        self.mainView.dropShadow()
        self.imgView.layer.borderWidth = 2
        self.createEventBtn.makeRoundCorner(10)
        self.createEventBtn.layer.borderWidth = 2
        self.createEventBtn.layer.borderColor = UIColor(hexString: "#00375F")?.cgColor
        self.emptyVIew.makeRoundCorner(10.0)
        self.eventPic.image = UIImage(named: "eventPalceHolder")
                self.emptyVIew.dropShadow(shadowColor: UIColor(named: "shadowColor")!, borderColor: .white, opacity: 1, offSet: CGSize(width: 0, height: 0), radius: 6, scale: true)
//        self.emptyVIew.addDashBorder(color: UIColor(hexString: "#00375F")!)

        self.imgView.layer.borderColor = UIColor(hexString: "054D82")?.cgColor
        self.completeEventLbl.isHidden = true
        if(eventPic.image == UIImage(named: "eventPalceHolder")){
            eventPic.contentMode = .scaleAspectFit
        }else{
            eventPic.contentMode = .scaleToFill
        }
        
        
       
        
        
    }
    @IBAction func tapOnCreateEvent(_ sender: Any) {
        self.delgate?.pushtoVC()
    }
    

}

