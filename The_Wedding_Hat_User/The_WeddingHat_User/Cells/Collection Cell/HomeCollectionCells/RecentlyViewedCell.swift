//
//  RecentlyViewedCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 04/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class RecentlyViewedCell: UICollectionViewCell {
    
    @IBOutlet weak var superMainView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var vendorImg: UIImageView!
    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var reviewsLbl: UILabel!
    @IBOutlet weak var starIcon: UIImageView!
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var featuredView: UIView!
    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var reviewView: UIView!
    @IBOutlet weak var ratingView: UIView!
    
    override func awakeFromNib() {
        setupUI()
    }
    
}

extension RecentlyViewedCell{
    func setupUI(){
        self.superMainView.makeRoundCorner(10)
        self.superMainView.dropShadow()
        self.reviewView.makeRoundCorner(5.0)
        self.ratingView.makeRoundCorner(5.0)
//        self.sceneView.makeRoundCorner(5.0)
//        self.sceneView.layer.borderWidth = 1
//        self.sceneView.layer.borderColor = UIColor(hexString: "#00375F")?.cgColor
//        self.amountView.makeRoundCorner(5.0)
//        self.amountView.layer.borderWidth = 1
//        self.amountView.layer.borderColor = UIColor(hexString: "#00375F")?.cgColor
//        self.guestView.makeRoundCorner(5.0)
//        self.guestView.layer.borderWidth = 1
//        self.guestView.layer.borderColor = UIColor(hexString: "#00375F")?.cgColor
        //self.featuredView.makeRoundCorner(2.0)
    }
}
