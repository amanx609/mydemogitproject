//
//  ContributerCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 13/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class ContributerCell: UICollectionViewCell
{
    
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var name_lbl: UILabel!
    
    override func awakeFromNib()
    {
        self.imgVw.makeRounded()
    }
}
