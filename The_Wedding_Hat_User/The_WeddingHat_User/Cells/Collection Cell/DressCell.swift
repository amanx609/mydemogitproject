//
//  DressCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 06/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class DressCell: UICollectionViewCell
{
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var dressImg: UIImageView!
    @IBOutlet weak var dress_name: UILabel!
    @IBOutlet weak var price_lbl: UILabel!
    @IBOutlet weak var amount_lbl: UILabel!
    @IBOutlet weak var like_btn: UIButton!
    
    override func awakeFromNib()
    {
        self.mainView.makeRoundCorner(8)
        self.dressImg.layer.cornerRadius = 8
        self.dressImg.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        self.mainView.layer.masksToBounds = false
        self.mainView.layer.shadowColor = UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0).cgColor
        self.mainView.layer.shadowOpacity = 0.5
        self.mainView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.mainView.layer.shadowRadius = 1
    
    }
    
}
