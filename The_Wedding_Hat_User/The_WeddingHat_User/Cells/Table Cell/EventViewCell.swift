//
//  EventViewCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 04/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class EventViewCell: UITableViewCell {
    
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var memberName: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupDesign()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setupDesign(){
        viewCell.makeRoundCorner(5.0)
    }
    
    func addNewMember(name: String) {
        
    }
}


