//
//  SocialLinkCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 11/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class SocialLinkCell: UITableViewCell {
    
    @IBOutlet weak var linkImages: UIImageView!
      @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var socialName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
  
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
