//
//  DealAndDiscountCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 12/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class DealAndDiscountCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
