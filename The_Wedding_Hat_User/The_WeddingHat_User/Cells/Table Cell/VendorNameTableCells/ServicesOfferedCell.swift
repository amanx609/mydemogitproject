//
//  ServicesOfferedCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 11/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class ServicesOfferedCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var contentLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
