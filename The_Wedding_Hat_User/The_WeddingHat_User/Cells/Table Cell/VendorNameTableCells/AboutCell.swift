//
//  AboutCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 11/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class AboutCell: UITableViewCell {

    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
