//
//  RealWeddingCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 10/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class RealWeddingCell: UITableViewCell {
    
    @IBOutlet weak var mainVIew: UIView!
    @IBOutlet weak var loc_lbl: UILabel!
    @IBOutlet weak var name_lbl: UILabel!
    @IBOutlet weak var wedding_lbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib()
    {
        self.mainVIew.makeRoundCorner(8)
        self.mainVIew.dropShadow()
        self.imgView.layer.cornerRadius = 8
        self.imgView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]
        //        .layerMaxXMaxYCorner – lower right corner
        //        .layerMaxXMinYCorner – top right corner
        //        .layerMinXMaxYCorner – lower left corner
        //        .layerMinXMinYCorner – top left corner
    }

}
