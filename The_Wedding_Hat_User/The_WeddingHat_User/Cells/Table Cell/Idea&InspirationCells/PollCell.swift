//
//  PollCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 10/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class PollCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var optionLabel: UILabel!
    @IBOutlet weak var tickImage: UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.designSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func designSetup()
    {
        mainView.makeRoundCorner(10)
        mainView.dropShadow()
    }
    
    func createBorder()
    {
        self.mainView.layer.borderWidth = 2
        self.mainView.layer.borderColor = UIColor(red: 204/255, green: 148/255, blue: 146/255, alpha: 1).cgColor
    }
    
    func removeBorder()
    {
        self.mainView.layer.borderWidth = 0
        self.mainView.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1).cgColor
    }

}
