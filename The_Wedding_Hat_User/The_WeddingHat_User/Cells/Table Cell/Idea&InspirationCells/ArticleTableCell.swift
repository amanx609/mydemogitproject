//
//  ArticleTableCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 19/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class ArticleTableCell: UITableViewCell {

    
    @IBOutlet weak var articleImage : UIImageView!
    @IBOutlet weak var articleLabel : UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
