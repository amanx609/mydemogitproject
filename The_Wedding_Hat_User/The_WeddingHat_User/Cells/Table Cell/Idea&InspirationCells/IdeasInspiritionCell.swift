//
//  IdeasInspiritionCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 26/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class IdeasInspiritionCell: UITableViewCell {

    @IBOutlet weak var main_view: UIView!
    @IBOutlet weak var name_lbl: UILabel!
    @IBOutlet weak var img_view: UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.main_view.makeRoundCorner(8)
        self.main_view.dropShadow()
    }
}
