//
//  SendCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 13/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class SendCell: UITableViewCell {
    
    @IBOutlet weak var txtView: UIView!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var msgTxt: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
