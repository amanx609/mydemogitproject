//
//  RecivedMediaCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 13/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class RecivedMediaCell: UITableViewCell {

    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var timelbl: UILabel!
    @IBOutlet weak var mediaImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
