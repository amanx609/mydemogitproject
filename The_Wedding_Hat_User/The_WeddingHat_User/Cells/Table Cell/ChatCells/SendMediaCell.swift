//
//  SendMediaCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 17/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class SendMediaCell: UITableViewCell {

    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var sendMediaImage: UIImageView!
    @IBOutlet weak var sendMediaTimeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
