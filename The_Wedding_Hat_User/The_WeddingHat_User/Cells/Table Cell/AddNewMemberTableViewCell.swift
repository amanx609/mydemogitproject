//
//  AddNewMemberTableViewCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 29/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class AddNewMemberTableViewCell: UITableViewCell {

    @IBOutlet weak var cellMainView: UIView!
    @IBOutlet weak var memberAccessName: UILabel!
    @IBOutlet weak var buttonSelect: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.layoutMargins = UIEdgeInsets.zero
        buttonSelect.isUserInteractionEnabled = false
//        self.cellMainView.dropShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
