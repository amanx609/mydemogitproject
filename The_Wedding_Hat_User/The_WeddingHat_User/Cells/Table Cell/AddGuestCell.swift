//
//  AddGuestCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 17/09/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class AddGuestCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var age_view: UIView!
    @IBOutlet weak var cross_btn: UIButton!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var adultBtn: UIButton!
    @IBOutlet weak var childBtn: UIButton!
    @IBOutlet weak var ConfirmDeclineView : UIView!
    @IBOutlet weak var ConfrimDeclineViewHeightConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    @IBOutlet weak var updateButtonView : UIView!
    @IBOutlet weak var updateConfirmDeclineButton : UIButton!
    
    var cell_age_group = ""
    var Selected = false
    var updateStatus = Bool()
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.updateButtonView.isHidden = false
        self.mainView.makeRoundCorner(8)
        self.mainView.dropShadow()
        self.cross_btn.makeRounded()
        self.age_view.makeRoundCorner(8)
        self.age_view.layer.borderColor = #colorLiteral(red: 0.5923900604, green: 0.5958476067, blue: 0.5888268948, alpha: 1)
        self.age_view.layer.borderWidth = 1
        self.confrimBtn()
        self.declineBtn()
        self.updateButtonView.isHidden = true
//        self.pending()
//        self.Responded()
        
        
    }
    
    
    @IBAction func AdultBtnTap(_ sender: UIButton)
    {
        self.cell_age_group = "Adult"
        self.selctedAdult()
        
    }
    
    @IBAction func ChildBtnTap(_ sender: UIButton)
    {
        self.cell_age_group = "Child"
        self.selctedChild()
    }
    
    @IBAction func declineBtnTap(_ sender: Any) {
        
    }
    
    @IBAction func confirmBtnTap(_ sender: Any) {
    }
    
    
    
    
    
    
    func selctedChild()
    {
        self.Selected = true
        self.childBtn.makeRoundCorner(5)
        self.adultBtn.setTitle("Adult", for: .normal)
        self.childBtn.setTitle("Child", for: .normal)
        self.childBtn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        self.childBtn.backgroundColor = #colorLiteral(red: 0, green: 0.2837521732, blue: 0.44864434, alpha: 1)
        self.adultBtn.setTitleColor(#colorLiteral(red: 0, green: 0.2837521732, blue: 0.44864434, alpha: 1), for: .normal)
        self.adultBtn.backgroundColor = .white
    }
  
    func selctedAdult()
    {
        self.Selected = true
        self.adultBtn.makeRoundCorner(5)
        self.adultBtn.setTitle("Adult", for: .normal)
        self.childBtn.setTitle("Child", for: .normal)
        self.adultBtn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        self.adultBtn.backgroundColor = #colorLiteral(red: 0, green: 0.2837521732, blue: 0.44864434, alpha: 1)
        self.childBtn.setTitleColor(#colorLiteral(red: 0, green: 0.2837521732, blue: 0.44864434, alpha: 1), for: .normal)
        self.childBtn.backgroundColor = .white
    }
    
    func confrimBtn()
    {
        self.confirmButton.layer.cornerRadius = 6
        self.confirmButton.layer.borderWidth = 2
        self.confirmButton.layer.borderColor = UIColor(hexString: "#6DD214")?.cgColor
    }
    
    func declineBtn()
    {
        self.declineButton.layer.cornerRadius = 6
        self.declineButton.layer.borderWidth = 2
        self.declineButton.layer.borderColor = UIColor(hexString: "#F64F3C")?.cgColor
    }
    
    
}

extension AddGuestCell{
    func validation() -> Bool{
        guard let firstName = firstName.text , !firstName.isEmpty else{
            print("Enter first name")
            return false
        }
        guard let lastName = lastName.text , !lastName.isEmpty else{
            print("Enter last name")
            return false
        }
        return true
        if(Selected == false){
            print("select age group")
            return false
        }
    }
    
    func pending()
    {
        self.firstName.isUserInteractionEnabled = false
        self.lastName.isUserInteractionEnabled = false
        self.adultBtn.isUserInteractionEnabled = false
        self.childBtn.isUserInteractionEnabled = false
        self.cross_btn.isUserInteractionEnabled = false
        self.cross_btn.isHidden = true
    }
    
    func Responded()
    {
        self.firstName.isUserInteractionEnabled = false
        self.lastName.isUserInteractionEnabled = false
        self.adultBtn.isUserInteractionEnabled = false
        self.childBtn.isUserInteractionEnabled = false
        self.cross_btn.isUserInteractionEnabled = false
        self.cross_btn.isHidden = true
    }
    
    func UpdateAsConfrim()
    {
        self.updateStatus = true
        self.updateButtonView.isHidden = false
//        self.ConfirmDeclineView.isHidden = true
        self.updateConfirmDeclineButton.setTitle("Mark as Decline", for: .normal)
        self.updateConfirmDeclineButton.setTitleColor(#colorLiteral(red: 0.9812672734, green: 0.4039103091, blue: 0.2982072234, alpha: 1), for: .normal)
        self.updateConfirmDeclineButton.layer.borderWidth = 2
        self.updateConfirmDeclineButton.layer.borderColor = #colorLiteral(red: 0.9812672734, green: 0.4039103091, blue: 0.2982072234, alpha: 1)
        self.updateConfirmDeclineButton.layer.cornerRadius = 6
        
    }
    
    func UpdateAsDecline()
    {
        self.updateStatus = false
        self.updateButtonView.isHidden = false
//        self.ConfirmDeclineView.isHidden = true
        self.updateConfirmDeclineButton.setTitle("Mark as Confirm", for: .normal)
        self.updateConfirmDeclineButton.setTitleColor(#colorLiteral(red: 0.4274509804, green: 0.8235294118, blue: 0.07843137255, alpha: 1), for: .normal)
        self.updateConfirmDeclineButton.layer.borderWidth = 2
        self.updateConfirmDeclineButton.layer.borderColor = #colorLiteral(red: 0.4882465601, green: 0.8395010233, blue: 0.09232813865, alpha: 1)
        self.updateConfirmDeclineButton.layer.cornerRadius = 6
    }
    
    func CompaninonStatusConfirm()
    {
        
    }
    
    
}
