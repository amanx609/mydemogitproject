//
//  AllReviewCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 28/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import Foundation
import UIKit



class UserRatingCell : UITableViewCell{
    @IBOutlet weak var mainView : UIView!
    @IBOutlet weak var userReviewNumber : UILabel!
    @IBOutlet weak var downImg : UIImage!
    override func awakeFromNib() {
        
    }
}


class UserRatingTblViewCell : UITableViewCell{
    @IBOutlet weak var mainView : UIView!
    @IBOutlet weak var labeView : UIView!
    @IBOutlet weak var lblNumber: UILabel!
    override func awakeFromNib() {
        self.mainView.dropShadowBlueColor()
    }
}

class DescTblCell : UITableViewCell{
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var descTxtLbl: UILabel!
    override func awakeFromNib() {
        self.contentView.dropShadowBlueColor()
        self.mainView.layer.cornerRadius = 10
    }
}

class UserDetailsRatingCell : UITableViewCell{
    @IBOutlet weak var mainView : UIView!
    @IBOutlet weak var nameLbl : UILabel!
    @IBOutlet weak var starImg : UIImage!
    @IBOutlet weak var ratingView : UIView!
    @IBOutlet weak var ratingLbl : UILabel!
    @IBOutlet weak var downIcon : UIImage!
    @IBOutlet weak var mainImg : UIImage!
    @IBOutlet weak var heldOnDate : UILabel!
    @IBOutlet weak var reviewOnDate : UILabel!
    @IBOutlet weak var vendorTypeLbl : UILabel!
    override func awakeFromNib() {
        self.mainView.layer.cornerRadius = 10
        self.mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.mainView.clipsToBounds = true
        self.contentView.dropShadowBlueColor()
    }
}
