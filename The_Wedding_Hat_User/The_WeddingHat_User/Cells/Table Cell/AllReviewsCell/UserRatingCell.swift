//
//  UserRatingCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 25/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class UserRatingCell: UITableViewCell {

    @IBOutlet weak var userRatingLbl: UILabel!
    @IBOutlet weak var downImg: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



class UserRatingTblViewCell : UITableViewCell{
    
    @IBOutlet weak var reviewNumber: UILabel!
    @IBOutlet weak var LblView: UIView!
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var serviceLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainView.dropShadowBlueColor()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
}

class DescTblCell : UITableViewCell{
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var descTxtLbl: UILabel!
    
    override  func awakeFromNib(){
        self.contentView.dropShadowBlueColor()
        self.mainView.layer.cornerRadius = 10
        self.mainView.layer.maskedCorners = [.layerMinXMaxYCorner , .layerMaxXMaxYCorner]

    }
}
