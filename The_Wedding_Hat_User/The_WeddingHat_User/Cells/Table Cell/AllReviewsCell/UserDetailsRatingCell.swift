//
//  UserDetailsRatingCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 25/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class UserDetailsRatingCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var starImg: UIImageView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var downimg: UIImageView!
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var vendorTypelbl: UILabel!
    @IBOutlet weak var heldOnDateLbl: UILabel!
    @IBOutlet weak var reviewOnDateLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        self.mainView.makeRoundCorner(10)
        self.mainView.layer.maskedCorners = [.layerMinXMinYCorner , .layerMaxXMinYCorner]
        self.mainView.dropShadowBlueColor()




        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}









