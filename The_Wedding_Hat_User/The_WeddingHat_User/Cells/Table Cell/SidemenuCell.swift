//
//  SidemenuCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 09/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class SidemenuCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var menulbl: UILabel!
    @IBOutlet weak var bottomlbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var centerOfImgView: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
