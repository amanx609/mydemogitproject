//
//  VendorListCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 15/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

protocol vendorlistProtocol
{
    func taponbookmark(index:Int)
}

class VendorListCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var vendorName_lbl: UILabel!
    @IBOutlet weak var Featured_Lbl: UILabel!
    @IBOutlet weak var Location_Lbl: UILabel!
    @IBOutlet weak var bookmark_btn: UIButton!
    @IBOutlet weak var rating_lbl: UILabel!
    @IBOutlet weak var review_lbl: UILabel!
    @IBOutlet weak var img_view: UIView!
    @IBOutlet weak var vendor_icon: UIImageView!
    @IBOutlet weak var desc_txt: UILabel!
    @IBOutlet weak var degree_view: UIView!
    @IBOutlet weak var budget_view: UIView!
    @IBOutlet weak var guest_view: UIView!
    @IBOutlet weak var guest_lbl: UILabel!
    @IBOutlet weak var budget_lbl: UILabel!
    @IBOutlet weak var discount_view: UIView!
    @IBOutlet weak var discount_lbl: UILabel!
    @IBOutlet weak var degree_lbl: UILabel!
    var delegate : vendorlistProtocol?
    
    override func awakeFromNib()
    {
        self.mainView.makeRoundCorner(8)
        self.mainView.dropShadow()
        self.Featured_Lbl.makeRoundCorner(6)
        self.review_lbl.makeRoundCorner(4)
        self.rating_lbl.makeRoundCorner(4)
        self.degree_view.makeRoundCorner(4)
        self.budget_view.makeRoundCorner(4)
        self.guest_view.makeRoundCorner(4)
        self.img_view.makeRoundCorner(8)
        self.degree_view.layer.borderWidth = 1
        self.budget_view.layer.borderWidth = 1
        self.guest_view.layer.borderWidth = 1
        self.img_view.layer.borderWidth = 0.1
        self.discount_view.layer.cornerRadius = 8
        self.discount_view.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMinYCorner]
        self.img_view.layer.borderColor = UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0).cgColor
        self.guest_view.layer.borderColor = UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0).cgColor
        self.budget_view.layer.borderColor = UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0).cgColor
        self.degree_view.layer.borderColor = UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0).cgColor
    }
    
    func populatedata(data : ModelVendorList)
    {
        self.vendorName_lbl.text = data.name
        self.desc_txt.text = data.business_description
        if let img_url = URL(string: data.image)
        {
            self.vendor_icon.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "muzica_icon"))
        }
        self.bookmark_btn.isSelected = data.is_saved
        self.Featured_Lbl.isHidden = data.is_featured
        self.rating_lbl.text = " \(data.rating) "
        self.review_lbl.text = " \(data.reviews) Reviews  "
        self.Location_Lbl.text = "Noida, India"//data.address
        self.discount_view.isHidden = data.deal_discount
    }
    
    @IBAction func boolmarkVendor(sender : UIButton)
    {
        if let cellindex = ((self.superview) as? UITableView)?.indexPath(for: self)
        {
            delegate?.taponbookmark(index: cellindex.row)
        }
    }
    
   
}


