//
//  CommunityDetailCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 30/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class CommunityDetailCell: UITableViewCell {
    
    @IBOutlet weak var main_view: UIView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var badge_lbl: UILabel!
    @IBOutlet weak var date_lbl: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var posted_lbl: UILabel!
    @IBOutlet weak var posted_heading: UILabel!
    @IBOutlet weak var question_lbl: UILabel!
    @IBOutlet weak var desc_lbl: UILabel!
    @IBOutlet weak var reply_btn: UIButton!
    @IBOutlet weak var comments_lbl: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.profilePic.makeRounded()
        self.main_view.makeRoundCorner(8)
        self.main_view.dropShadow()
        self.reply_btn.makeRoundCorner(6)
        self.badge_lbl.makeRoundCorner(8)
        self.badge_lbl.layer.borderWidth = 1
        self.badge_lbl.layer.borderColor = UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0).cgColor

    }
}
