//
//  PersonalDetailCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 13/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class PersonalDetailCell: UITableViewCell {

    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var desc_lbl: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.imgView.makeRounded()
        self.desc_lbl.makeRoundCorner(8)
        self.desc_lbl.layer.borderWidth = 1
        self.nameLbl.text = CommonUtilities.shared.getuserdata()?.name
        self.desc_lbl.layer.borderColor = UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0).cgColor
    }

    @IBAction func tapOnForumTopic(_ sender: Any)
    {
        self.viewController?.createAlert(message: Constant.string.underdevlopment)
    }
}
