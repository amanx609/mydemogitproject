//
//  TopContributerCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 13/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class TopContributerCell: UITableViewCell {

    
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var contributerColl: UICollectionView!
    
    var data = [TopContributorsModel]()
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        self.contributerColl.delegate = self
        self.contributerColl.dataSource = self
    }
    
    func populatedData(objData:[TopContributorsModel])
    {
        self.data = objData
        self.contributerColl.reloadData()
    }

}


extension TopContributerCell : UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContributerCell", for: indexPath) as! ContributerCell
        if let img_url = URL(string:self.data[indexPath.row].user_data["user_image"] as? String ?? "")
        {
            cell.imgVw.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "user_profile_icon"))
        }
        cell.name_lbl.text = self.data[indexPath.row].user_data["name"] as? String ?? ""
        
        return cell
    }
}
