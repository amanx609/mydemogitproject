//
//  CommunityPostCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 13/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

protocol communitydelegate
{
    func taponcommentbtn(index : IndexPath)
}

class CommunityPostCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var badge_lbl: UILabel!
    @IBOutlet weak var date_lbl: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var posted_lbl: UILabel!
    @IBOutlet weak var posted_heading: UILabel!
    @IBOutlet weak var question_lbl: UILabel!
    @IBOutlet weak var desc_lbl: UILabel!
    @IBOutlet weak var reply_btn: UIButton!
    @IBOutlet weak var profile_btn: UIButton!
    @IBOutlet weak var name_btn: UIButton!
    @IBOutlet weak var comments_lbl: UILabel!
    

    var delegate : communitydelegate?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.profilePic.makeRounded()
        self.mainView.makeRoundCorner(8)
        self.mainView.dropShadow()
        self.reply_btn.makeRoundCorner(6)
        self.badge_lbl.makeRoundCorner(8)
        self.badge_lbl.layer.borderWidth = 1
        self.badge_lbl.layer.borderColor = UIColor(hexString: "#00375F")?.cgColor
    }
    
    
}
