//
//  CommentCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 31/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var name_lbl: UILabel!
    @IBOutlet weak var time_lbl: UILabel!
    @IBOutlet weak var edit_btn: UIButton!
    @IBOutlet weak var post_lbl: UILabel!
    @IBOutlet weak var profile_pic: UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
    }
    
    @IBAction func tapOnEdit(_ sender: Any)
    {
        
    }
}
