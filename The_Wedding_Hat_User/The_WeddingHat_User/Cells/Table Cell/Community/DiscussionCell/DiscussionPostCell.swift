//
//  DiscussionPostCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 31/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class DiscussionPostCell: UITableViewCell {

    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var name_lbl: UILabel!
    @IBOutlet weak var time_lbl: UILabel!
    @IBOutlet weak var post_lbl: UILabel!
    @IBOutlet weak var badge_lbl: UILabel!
    @IBOutlet weak var edit_btn: UIButton!
    @IBOutlet weak var posted_lbl: UILabel!
    @IBOutlet weak var post_img: UIImageView!
    @IBOutlet weak var question_lbl: UILabel!
    @IBOutlet weak var badge_img: UIImageView!
    @IBOutlet weak var postedHeading: UILabel!
    @IBOutlet weak var profile_pic: UIImageView!
    @IBOutlet weak var allCommentsNumber: UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.mainView.makeRoundCorner(8)
        self.mainView.dropShadow()
        self.post_img.makeRoundCorner(8)
        
    }
    
    @IBAction func tapOnEdit(_ sender: Any)
    {
        
    }
}
