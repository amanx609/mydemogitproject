//
//  AddMoreTableCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 08/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class AddMoreTableCell: UITableViewCell {

    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var cellLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
