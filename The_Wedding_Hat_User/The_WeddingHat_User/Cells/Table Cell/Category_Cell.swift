//
//  Category_Cell.swift
//  The_WeddingHat_User
//
//  Created by appl on 21/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class Category_Cell: UITableViewCell {
    
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var category_lbl: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.mainView.makeRoundCorner(8)
        self.mainView.dropShadow()
    }
    
}
