//
//  VendorTeamTableCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 12/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class VendorTeamTableCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var subMainView: UIView!
    @IBOutlet weak var dealAndDiscountView: UIView!
    @IBOutlet weak var degreeView: UIView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var guestView: UIView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var featuredView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupDesign()
        self.dealAndDiscountView.roundCorners(corners: [.topLeft , .bottomRight], radius: 5.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupDesign(){
        self.subMainView.makeRoundCorner(10.0)
        self.subMainView.dropShadow(shadowColor: UIColor(named: "shadowColor")!, borderColor: .white, opacity: 1.0, offSet: .zero, radius: 5.0, scale: true)
        self.priceView.makeRoundCorner(5)
        self.degreeView.makeRoundCorner(5)
        self.guestView.makeRoundCorner(5)
        self.ratingView.makeRoundCorner(5)
        self.featuredView.makeRoundCorner(3)
        self.priceView.layer.borderWidth = 1
        self.priceView.layer.borderColor = #colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)
        self.degreeView.layer.borderWidth = 1
        self.degreeView.layer.borderColor = #colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)
        self.guestView.layer.borderWidth = 1
        self.guestView.layer.borderColor = #colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)
    }
}
