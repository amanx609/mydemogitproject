//
//  VendorCategoryTableCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 07/07/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class VendorCategoryTableCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        cellView.makeRoundCorner(8.0)
        cellView.dropShadow()
    }
    @IBOutlet weak var cellLbl: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
