//
//  Article_Cell.swift
//  The_WeddingHat_User
//
//  Created by appl on 19/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class Article_Cell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var desc_lbl: UILabel!
    @IBOutlet weak var posted_lbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var category_lbl: UILabel!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.mainView.makeRoundCorner(8)
        self.mainView.dropShadow()
        self.imgView.makeRoundCorner(8)
        self.category_lbl.makeRoundCorner(4)
        self.imgView.layer.cornerRadius = 8
        self.imgView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]
        //        .layerMaxXMaxYCorner – lower right corner
        //        .layerMaxXMinYCorner – top right corner
        //        .layerMinXMaxYCorner – lower left corner
        //        .layerMinXMinYCorner – top left corner
    }
}
