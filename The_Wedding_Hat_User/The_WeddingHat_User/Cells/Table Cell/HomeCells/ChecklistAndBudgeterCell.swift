//
//  ChecklistAndBudgeterCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 04/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class ChecklistAndBudgeterCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var registerImg: UIImageView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var getPointLbl: UILabel!
    @IBOutlet weak var totalPointLbl: UILabel!
    @IBOutlet weak var maindescLbl: UILabel!
    @IBOutlet weak var invitesLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainView.makeRoundCorner(10)
        self.mainView.dropShadow()
        self.registerImg.makeRounded()
        self.headerLbl.text = Constant.string.checklist.localized()
        self.descLbl.text = Constant.string.taskCompelete.localized()
        self.invitesLbl.text = Constant.string.sendInvites.localized()
        self.maindescLbl.text = Constant.string.upcomingTask.localized()
    }

}
