//
//  tblcellwithsinglelbl.swift
//  The_WeddingHat_User
//
//  Created by appl on 31/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class tblcellwithsinglelbl: UITableViewCell {

    @IBOutlet weak var socialImages_Coll: UICollectionView!
    var linkArr = [SocialLinks]()
   
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.socialImages_Coll.delegate = self
        self.socialImages_Coll.dataSource = self
    }
    
    func populateData(data:[SocialLinks])
    {
        
        self.linkArr = data
        self.socialImages_Coll.reloadData()
    }

}

extension tblcellwithsinglelbl: UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.linkArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SocialImagesCell", for: indexPath) as! SocialImagesCell
       
        if self.linkArr[indexPath.row].type == "instagram"
        {
            cell.img_view.image = #imageLiteral(resourceName: "instagram")
        }
        else if self.linkArr[indexPath.row].type == "twitter"
        {
            cell.img_view.image = #imageLiteral(resourceName: "twitter")
        }
        else if self.linkArr[indexPath.row].type == "linked"
        {
            cell.img_view.image = #imageLiteral(resourceName: "linkedin")
        }
        else if self.linkArr[indexPath.row].type == "facebook"
        {
            cell.img_view.image = #imageLiteral(resourceName: "facebook")
        }
        else
        {
            cell.img_view.image = #imageLiteral(resourceName: "web")
        }

        return cell
    }
    
    
}

class SocialImagesCell : UICollectionViewCell
{
    @IBOutlet weak var img_view: UIImageView!
    
}
