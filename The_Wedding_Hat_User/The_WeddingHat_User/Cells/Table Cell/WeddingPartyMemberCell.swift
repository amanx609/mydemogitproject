//
//  WeddingPartyMemberCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 20/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class WeddingPartyMemberCell: UITableViewCell {

    

    @IBOutlet weak var memberImage: UIImageView!
    @IBOutlet weak var labelView: UIView!
    @IBOutlet weak var mainImageView: UIView!
    @IBOutlet weak var freePaidView: UIView!
    @IBOutlet weak var freePaidLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.uiDesign()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

extension WeddingPartyMemberCell{
    func uiDesign(){
        self.labelView.makeRoundCorner(10)
        self.labelView.dropShadowBlueColor()
        self.mainImageView.makeRoundCorner(10.0)
        self.mainImageView.dropShadowBlueColor()
        self.memberImage.makeRoundCorner(5.0)
        self.freePaidView.makeRoundCorner(9)
       

    }
}
