//
//  MessagesCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 12/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class MessagesCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgVIew: UIView!
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var badgeLbl: UILabel!
    @IBOutlet weak var profilePic: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        imgVIew.makeRounded()
        badgeLbl.makeRounded()
        profilePic.makeRounded()

    }
}
