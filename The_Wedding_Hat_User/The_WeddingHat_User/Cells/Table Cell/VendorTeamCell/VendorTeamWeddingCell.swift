//
//  VendorTeamWeddingCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 22/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class VendorTeamWeddingCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var subMainView: UIView!
    @IBOutlet weak var review: UILabel!
    @IBOutlet weak var vendorTypeImage: UIImageView!
    @IBOutlet weak var negotiationLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var vendorTypeLbl: UILabel!
    
    @IBOutlet weak var middleArrow: UIImageView!
    @IBOutlet weak var upperArrow: UIImageView!
    
 
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupDesign()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    func setupDesign(){
           self.mainView.makeRoundCorner(10.0)
           self.mainView.dropShadow(shadowColor: UIColor(named: "shadowColor")!, borderColor: .white, opacity: 1.0, offSet: .zero, radius: 5.0, scale: true)
        self.vendorTypeImage.layer.borderWidth = 1
        self.vendorTypeImage.layer.borderColor = #colorLiteral(red: 0.9333333333, green: 0.9725490196, blue: 0.9960784314, alpha: 1)
        
       }

}
