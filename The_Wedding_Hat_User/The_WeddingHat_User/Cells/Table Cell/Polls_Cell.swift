//
//  Polls_Cell.swift
//  The_WeddingHat_User
//
//  Created by appl on 26/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import KRProgressHUD

protocol hitservice {
    func reload()
}

class Polls_Cell: UITableViewCell {
    
    @IBOutlet weak var main_view: UIView!
    @IBOutlet weak var submit_btn: UIButton!
    @IBOutlet weak var question_lbl: UILabel!
    @IBOutlet weak var question_view: UIView!
    @IBOutlet weak var heightofsubmit: NSLayoutConstraint!
   // @IBOutlet weak var option_coll: UICollectionView!
    @IBOutlet weak var heightOfColl: NSLayoutConstraint!
    
        @IBOutlet weak var option_coll: UICollectionView! {
               didSet {
                   collectionViewLayout?.estimatedItemSize = CGSize(width: 1, height: 1)
                   selectionStyle = .none
               }
           }

           var collectionViewLayout: UICollectionViewFlowLayout? {
               return option_coll.collectionViewLayout as? UICollectionViewFlowLayout
           }



    
    
    
    
    
    var flag = ""
    var option = [Answer]()
    var option_id = ""
    var question_Id = ""
    var selectedIndex = -1
    var delegate:hitservice?
    var is_answer = Bool()
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.option_coll.delegate = self
        self.option_coll.dataSource = self
        self.main_view.makeRoundCorner(8)
        self.main_view.dropShadow()
        self.submit_btn.makeRoundCorner(8)
        self.main_view.layer.borderWidth = 1
        self.question_view.layer.cornerRadius = 8
        self.question_view.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]
        //self.main_view.layer.borderColor = (UIColor.blue.withAlphaComponent(0.5).cgColor)
        
        
        //UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0).cgColor
        //        .layerMaxXMaxYCorner – lower right corner
        //        .layerMaxXMinYCorner – top right corner
        //        .layerMinXMaxYCorner – lower left corner
        //        .layerMinXMinYCorner – top left corner
    }
    func populateData(obj:[Answer])
    {
        self.option = obj
        self.option_coll.reloadData()
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize
    {
        option_coll.layoutIfNeeded()
        let topConstraintConstant = contentView.constraint(byIdentifier: "topAnchor")?.constant ?? 0
        let bottomConstraintConstant = contentView.constraint(byIdentifier: "bottomAnchor")?.constant ?? 0
        let trailingConstraintConstant = contentView.constraint(byIdentifier: "trailingAnchor")?.constant ?? 0
        let leadingConstraintConstant = contentView.constraint(byIdentifier: "leadingAnchor")?.constant ?? 0
        option_coll.frame = CGRect(x: 0, y: 0, width: targetSize.width - trailingConstraintConstant - leadingConstraintConstant, height: 1)
        let size = option_coll.collectionViewLayout.collectionViewContentSize
        let newSize = CGSize(width: size.width, height: size.height + 145 + self.heightofsubmit.constant + question_view.frame.height + topConstraintConstant + bottomConstraintConstant)
        print(size.height)
        return newSize
    }
    
    @IBAction func tapOnSubmit(_ sender: Any)
    {
        if self.option_id != ""
        {
            self.hitPollAnswerService(question_Id: self.question_Id, optn_Id: self.option_id)
        }
    }
}

extension Polls_Cell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.option.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OptionCollCell", for: indexPath) as! OptionCollCell
        cell.option_lbl.text = self.option[indexPath.row].option
        
        if is_answer == true
        {
            if flag == "1"
            {
                cell.percent_lbl.isHidden = true
                cell.img_click.isHidden = false
                if self.option[indexPath.row].is_selected == true
                {
                    cell.img_click.image = #imageLiteral(resourceName: "checkMarkSelect")
                    cell.createBorder()
                    cell.option_lbl.textColor = UIColor(red: 204/255, green: 148/255, blue: 146/255, alpha: 1)
                    
                }
                else
                {
                    cell.img_click.image = #imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle-outline")
                    cell.removeBorder()
                    cell.option_lbl.textColor = UIColor(red: 0/255, green: 55/255, blue: 95/255, alpha: 1)
                }
            }
            else
            {
                
                cell.percent_lbl.isHidden = false
                cell.percent_lbl.text = self.option[indexPath.row].percentage+"%"
                if self.option[indexPath.row].is_selected == true
                {
                    cell.img_click.isHidden = true
                    
                    cell.createBorder()
                    cell.option_lbl.textColor = UIColor(red: 204/255, green: 148/255, blue: 146/255, alpha: 1)
                    
                }
                else
                {
                    cell.img_click.isHidden = true
                    cell.removeBorder()
                    cell.option_lbl.textColor = UIColor(red: 0/255, green: 55/255, blue: 95/255, alpha: 1)
                }
                
            }
            
            
        }
        else
        {
            if selectedIndex == indexPath.row
            {
                cell.img_click.image = #imageLiteral(resourceName: "checkMarkSelect")
            }
            else
            {
                cell.img_click.image = #imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle-outline")
            }
        }
       
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if self.is_answer == false
        {
            self.selectedIndex = indexPath.row
            self.option_id = self.option[indexPath.row].option_id
            self.option_coll.reloadData()
        }
        
    }
    
}

extension Polls_Cell
{
    func hitPollAnswerService(question_Id : String , optn_Id : String)
    {
        let headers: HTTPHeaders = [ "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Content-Type": "application/json"]
        let param =
            [
                "user_id": CommonUtilities.shared.getuserdata()?.id ,
                "question_id" : question_Id ,
                "option_id" : optn_Id
        ]
        ServiceManager.instance.request(method: .post, URLString: "pollAnswer", parameters: param as [String : Any], encoding: JSONEncoding.default, headers: headers) { (sucess, response, error) in
            if (sucess)!
            {
                if let statuscode = response?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        self.delegate?.reload()
                        self.heightofsubmit.constant = 0.0
                    }
                }
            }
            else
            {
                print(param)
            }
        }
    }
}
