//
//  VendorOptionCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 15/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class VendorOptionCell: UITableViewCell {

    @IBOutlet weak var tickImage: UIImageView!
    @IBOutlet weak var vendorNameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.toggleSelected()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    func toggleSelected()
    {
     if (isSelected){
         tickImage.image = #imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle")
        }else {
         tickImage.image = #imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle-outline")
        }
    }

}
