//
//  UploadYourPhotosCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 16/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import YangMingShan
import OpalImagePicker
protocol imagesArr {
    func imagesTransfer(imagesArr:[UIImage])
}

class UploadYourPhotosCell: UITableViewCell {

    
    @IBOutlet weak var reviewTextView: UITextView!
    var documentsPicArray = [UIImage]()
    
    var delegate: imagesArr?
    override func awakeFromNib()
    {
        super.awakeFromNib()
        reviewTextView.delegate = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

extension UploadYourPhotosCell: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.textColor = UIColor.black
        }
    }
}
    

extension UploadYourPhotosCell : OpalImagePickerControllerDelegate,UIImagePickerControllerDelegate , UINavigationControllerDelegate
{
    func chooseImage()
    {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        let actionSheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                imagePickerController.sourceType = .camera
                self.viewController?.present(imagePickerController, animated: true, completion: nil)
            }
            else
            {
                let alertController = UIAlertController(title: Constant.string.appname.localized(), message:
                    "Camera not avilable" , preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: Constant.string.ok.localized(), style: .default));  self.viewController?.present(alertController, animated: true, completion: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title:"Photo Album", style: .default, handler: {(action:UIAlertAction)in
            let imagePicker = OpalImagePickerController()
            let configuration = OpalImagePickerConfiguration()
            configuration.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select more than 5 images!", comment: "")
            imagePicker.configuration = configuration
            imagePicker.imagePickerDelegate = self
            imagePicker.maximumSelectionsAllowed = 5 - self.documentsPicArray.count
            imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
            imagePicker.modalPresentationStyle = .fullScreen
            self.viewController?.presentOpalImagePickerController(imagePicker, animated: true,
                                                  select: { (assets) in
                                                    //Select Assets
                                                    print("This is assests\(assets)")
                                                    self.viewController?.dismiss(animated: true, completion: nil)
                                                    self.documentsPicArray.append(contentsOf: self.getAssetThumbnail(assets: assets))
                                                    //self.imageCollectionView.reloadData()
            }, cancel: {
            })
        }))
        actionSheet.addAction(UIAlertAction(title: Constant.string.cancel.localized(), style: .cancel, handler: nil))
        self.viewController?.present(actionSheet,animated: true,completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        documentsPicArray.append(image)
        delegate?.imagesTransfer(imagesArr: documentsPicArray)
      // self.imageCollectionView.reloadData()
        picker.dismiss(animated: true, completion: nil)

    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    func getAssetThumbnail(assets: [PHAsset]) -> [UIImage] {
        var arrayOfImages = [UIImage]()
        for asset in assets {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.version = .original
            option.isSynchronous = true
            manager.requestImageData(for: asset, options: option) { data, _, _, _ in
                if let data = data {
                    arrayOfImages.append(UIImage(data: data)!)
                }
            }
        }
        return arrayOfImages
    }
}
