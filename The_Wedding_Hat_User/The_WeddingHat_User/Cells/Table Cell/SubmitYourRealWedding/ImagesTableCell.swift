//
//  ImagesTableCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 16/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import YangMingShan
import OpalImagePicker

class ImagesTableCell: UITableViewCell
{

    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    var documentsPicArray = [UIImage]()
    var collectionViewLayout: UICollectionViewFlowLayout?
    {
        return imageCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
    }

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageCollectionView.delegate = self
        self.imageCollectionView.dataSource = self
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    // MARK: -
    // MARK: UIView functions
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize
    {
        contentView.layoutIfNeeded()
        let topConstraintConstant = contentView.constraint(byIdentifier: "topAnchor")?.constant ?? 0
        let bottomConstraintConstant = contentView.constraint(byIdentifier: "bottomAnchor")?.constant ?? 0
        let trailingConstraintConstant = contentView.constraint(byIdentifier: "trailingAnchor")?.constant ?? 0
        let leadingConstraintConstant = contentView.constraint(byIdentifier: "leadingAnchor")?.constant ?? 0
        contentView.frame = CGRect(x: 0, y: 0, width: targetSize.width + trailingConstraintConstant - leadingConstraintConstant, height: 1)
        let size = imageCollectionView.collectionViewLayout.collectionViewContentSize
        let newSize = CGSize(width: size.width, height: size.height + topConstraintConstant + bottomConstraintConstant)
        return newSize
    }
    
    @objc func upload(sender: UIButton)
    {
       chooseImage()
    }
    

}

//MARK:- Collection View
extension ImagesTableCell: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return documentsPicArray.count 
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadImageCellColl", for: indexPath) as! UploadImageCellColl
        if documentsPicArray.count != 0
        {
            cell.addImages.image = documentsPicArray[indexPath.row]
        }
        
        cell.uploadBtn.tag = indexPath.row
        cell.uploadBtn.addTarget(self, action: #selector(upload(sender:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        let cell_width = (self.frame.width - 60) / 3
        return CGSize(width: cell_width, height: 80)
    }
}

//MARK:- Upload Collection View Cell
class UploadImageCellColl : UICollectionViewCell
{
    @IBOutlet weak var addImages: UIImageView!
    @IBOutlet weak var uploadBtn: UIButton!
    
    
    override func awakeFromNib()
    {
        self.addImages.makeRoundCorner(8)
        self.addImages.layer.borderWidth = 1
        self.addImages.layer.borderColor = UIColor(red: 20.0/255.0, green: 54.0/255.0, blue: 92.0/255.0, alpha: 1.0).cgColor
    }
    @IBAction func tapOnUpload(_ sender: Any)
    {
        //chooseImage()
    }
}

//MARK:- Upload Image 

extension ImagesTableCell : OpalImagePickerControllerDelegate,UIImagePickerControllerDelegate , UINavigationControllerDelegate
{
    func chooseImage()
    {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        let actionSheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                imagePickerController.sourceType = .camera
                self.viewController?.present(imagePickerController, animated: true, completion: nil)
            }
            else
            {
                let alertController = UIAlertController(title: Constant.string.appname.localized(), message:
                    "Camera not avilable" , preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: Constant.string.ok.localized(), style: .default));  self.viewController?.present(alertController, animated: true, completion: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title:"Photo Album", style: .default, handler: {(action:UIAlertAction)in
            let imagePicker = OpalImagePickerController()
            let configuration = OpalImagePickerConfiguration()
            configuration.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select more than 5 images!", comment: "")
            imagePicker.configuration = configuration
            imagePicker.imagePickerDelegate = self
            imagePicker.maximumSelectionsAllowed = 5 - self.documentsPicArray.count
            imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
            imagePicker.modalPresentationStyle = .fullScreen
            self.viewController?.presentOpalImagePickerController(imagePicker, animated: true,
                                                  select: { (assets) in
                                                    //Select Assets
                                                    print("This is assests\(assets)")
                                                    self.viewController?.dismiss(animated: true, completion: nil)
                                                    self.documentsPicArray.append(contentsOf: self.getAssetThumbnail(assets: assets))
                                                    self.imageCollectionView.reloadData()
            }, cancel: {
            })
        }))
        actionSheet.addAction(UIAlertAction(title: Constant.string.cancel.localized(), style: .cancel, handler: nil))
        self.viewController?.present(actionSheet,animated: true,completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        documentsPicArray.append(image)
       self.imageCollectionView.reloadData()
        picker.dismiss(animated: true, completion: nil)

    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    func getAssetThumbnail(assets: [PHAsset]) -> [UIImage] {
        var arrayOfImages = [UIImage]()
        for asset in assets {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.version = .original
            option.isSynchronous = true
            manager.requestImageData(for: asset, options: option) { data, _, _, _ in
                if let data = data {
                    arrayOfImages.append(UIImage(data: data)!)
                }
            }
        }
        return arrayOfImages
    }
}
