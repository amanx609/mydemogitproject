//
//  SubmitYourWeddingCell.swift
//  The_WeddingHat_User
//
//  Created by apple on 15/08/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

protocol AllVendorYes
{
    func isAnswer(isanswerKey:Bool)
}

class SubmitYourWeddingCell: UITableViewCell
{
    @IBOutlet weak var yesView: UIView!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var yesBtnLbl: UILabel!
    @IBOutlet weak var noView: UIView!
    @IBOutlet weak var noBtn: UIButton!
    @IBOutlet weak var noBtnLbl: UILabel!
    @IBOutlet weak var heightOfQuestion: NSLayoutConstraint!
    @IBOutlet weak var pleaseSelectVendorLbl: UILabel!
    
    var delegate:AllVendorYes?
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func yesBtnClick(_ sender: Any)
    {
        noBtn.setImage(#imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle-outline"), for: .normal)
        yesBtn.setImage(#imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle"), for: .normal)
        heightOfQuestion.constant = 0.0
        self.delegate?.isAnswer(isanswerKey: true)
        
    }
    
    @IBAction func noBtnClick(_ sender: Any)
    {
        noBtn.setImage(#imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle"), for: .normal)
        yesBtn.setImage(#imageLiteral(resourceName: "Icon ionic-ios-checkmark-circle-outline"), for: .normal)
        heightOfQuestion.constant = 37.0
        self.delegate?.isAnswer(isanswerKey: false)
    }
}
