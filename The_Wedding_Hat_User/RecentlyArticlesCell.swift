//
//  RecentlyArticlesCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 04/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class RecentlyArticlesCell: UICollectionViewCell {
    
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var articleImg: UIImageView!
    @IBOutlet weak var articleNameLbl: UILabel!
    let gradientLayer = CAGradientLayer()
    
    override func awakeFromNib()
    {
        self.mainView.makeRoundCorner(10)
        self.mainView.dropShadow()
        self.articleImg.makeRoundCorner(10)
        mainView.dropShadow()
        gradientLayer.opacity = 0.6
        gradientLayer.colors = [UIColor.black.cgColor, UIColor.black.cgColor]
        articleImg.layer.addSublayer(gradientLayer)
    }
    override func layoutSubviews()
    {
        super.layoutSubviews()
        gradientLayer.frame = self.bounds
    }
}
