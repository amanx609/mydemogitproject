//
//  HomeVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 02/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit
import Alamofire
import SkeletonView
import KRProgressHUD
import AlamofireImage
import SDWebImage


class HomeVC: UIViewController,pushToEventVC {
    
    @IBOutlet var newHeaderView: UIView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var homeTable: UITableView!
    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var proPic: UIImageView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet var topHeader: UIView!
    @IBOutlet weak var yourEventLbl: UILabel!
    @IBOutlet weak var addEventLbl: UILabel!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var planningLbl: UILabel!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var plusImg: UIImageView!
    @IBOutlet weak var addEventBtn: UIButton!
    @IBOutlet weak var featuredlbl: UILabel!
    @IBOutlet weak var recentlyViewedLbl: UILabel!
    @IBOutlet weak var recentlyArticlesLbl: UILabel!
    @IBOutlet weak var FeaturedViewAllBtn: UIButton!
    @IBOutlet weak var recentlyArticlesViewAllBtn: UIButton!
    @IBOutlet weak var featuredPageController: UIPageControl!
    @IBOutlet weak var recentlyViewedPageController: UIPageControl!
    @IBOutlet weak var recentlyArticlesPageController: UIPageControl!
    @IBOutlet weak var eventCollection: UICollectionView!
    @IBOutlet weak var featuredCollection: UICollectionView!
    @IBOutlet weak var recentlyViewedCollection: UICollectionView!
    @IBOutlet weak var recentlyArticlesCollection: UICollectionView!
    @IBOutlet weak var topOfEventCollection: NSLayoutConstraint!
    @IBOutlet weak var topToaddEvent: NSLayoutConstraint!
    @IBOutlet weak var addEventConstraint: NSLayoutConstraint!
    let gradientLayer = CAGradientLayer()

    
    var objModelEvent = [ModelEventList]()
    var objArticelModel = [ArticelModel]()
    var objEventData = [EventData]()
    var scrollCollection = ""
    var dotsCount = 10
    var currentPage : Int?
    var picindexPath = 0
    var eventImage = ""
    //MARK:- New Header
    @IBOutlet weak var weddingList: UIView!
    @IBOutlet weak var weddingName: UILabel!
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var weddingListTextField: UITextField!
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var daysView : UIView!
    @IBOutlet weak var daysLabel : UILabel!
    @IBOutlet weak var createNewEvent : UIButton!
    
    let picker = UIPickerView()
    
    @IBOutlet weak var andLbl : UILabel!
    @IBOutlet weak var eventTypeName: UIView!
    @IBOutlet weak var eventTypeNameLbl: UILabel!
    @IBOutlet weak var eventTypeNameImg: UIImageView!
    
    @IBOutlet weak var brideName : UILabel!
    @IBOutlet weak var groomName : UILabel!
    @IBOutlet weak var admin : UILabel!
    @IBOutlet weak var eventCode : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    @IBOutlet weak var editButtonTap: UIButton!
    @IBOutlet weak var EmptyView:UIView!
    @IBOutlet weak var MainEventView: UIView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setNewHeaderDesign()
        self.HitServiceget_Articles()
        self.hitServicegetEventData()
        CommonUtilities.shared.hitServicegetEventType()
        self.view.showGradientSkeleton()
        self.picker.delegate = self
        self.picker.dataSource = self
        self.setUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.hitServicegetEventData()
        self.HitServiceget_Articles()
    }
    func setNewHeaderDesign(){
        self.addView.layer.borderWidth = 1.5
        self.addView.layer.borderColor = #colorLiteral(red: 0, green: 0.2837521732, blue: 0.44864434, alpha: 1)
        self.addView.makeRoundCorner(10.0)
        self.eventTypeName.makeRoundCorner(8.0)
        self.MainEventView.clipsToBounds = true
        self.mainImage.clipsToBounds = true
        
        gradientLayer.opacity = 0.6
               gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor]
               mainImage.layer.addSublayer(gradientLayer)
        gradientLayer.frame = self.mainImage.frame
        
        self.weddingList.makeRoundCorner(10.0)
        self.weddingList.dropShadow()
    }
    
    func setUI()
    {
        self.buttonView.makeRounded()
        self.headerView.dropShadow()
        self.homeTable.separatorColor = .clear
        self.featuredlbl.text = Constant.string.featuredVendor.localized()
        self.recentlyViewedLbl.text = Constant.string.recentlyViwed.localized()
        self.recentlyArticlesLbl.text = Constant.string.recentlyAritcles.localized()
        self.FeaturedViewAllBtn.setTitle(Constant.string.viewAll.localized(), for: .normal)
        self.recentlyArticlesViewAllBtn.setTitle(Constant.string.viewAll.localized(), for: .normal)
        self.pageController.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        self.featuredPageController.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        self.recentlyViewedPageController.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        self.recentlyArticlesPageController.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        //NEW Header
        
//        self.dropDownButton.isUserInteractionEnabled = false
        
       
        self.weddingListTextField.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        self.weddingListTextField.textColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
        self.weddingListTextField.inputView = picker
        self.weddingListTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneRoleButton(_:)))
    }
    
    
    
    
    
    func setDesign()
    {
        if self.objModelEvent.count == 0{
            self.yourEventLbl.isHidden = true
            self.plusImg.isHidden = true
            self.addEventLbl.isHidden = true
            self.addEventBtn.isHidden = true
            self.planningLbl.isHidden = true
            self.pageController.isHidden = true
            self.topToaddEvent.constant = 0.0
            
            self.topOfEventCollection.constant = 8.0
        }
        else{
            
            self.yourEventLbl.isHidden = false
            self.plusImg.isHidden = false
            self.addEventLbl.isHidden = false
            self.addEventBtn.isHidden = false
            self.planningLbl.isHidden = false
            self.pageController.isHidden = false
            self.topToaddEvent.constant = 0.0
            self.topOfEventCollection.constant = 43.0
            
        }
    }
    func pushtoVC()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventVC") as! CreateEventVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func tapOnNotification(_ sender: Any)
    {
        self.createAlert(message: Constant.string.underdevlopment)
    }
    
    @IBAction func tapOnChat(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "IdeasAndInspire", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SendInvitationVC") as! SendInvitationVC
        self.navigationController?.pushViewController(vc, animated: true)
        
        //self.createAlert(message: Constant.string.underdevlopment)
    }
    @IBAction func tapOnAddEvent(_ sender: Any)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventVC") as! CreateEventVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func newRingButtonTap(_ sender: UIButton)
    {
        self.slideMenuController()?.toggleLeft()
    }
    
    @IBAction func tapOnVendersViewAll(_ sender: Any)
    {
        self.createAlert(message: Constant.string.underdevlopment)
    }
    
    @IBAction func tapOnRecentlyArtilcesViewAll(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "IdeasAndInspire", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ArticleVC") as! ArticleVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @IBAction func addEventBtnTap(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventVC") as! CreateEventVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func editEventBtnTap(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventVC") as! CreateEventVC
        vc.eventTID = self.objModelEvent[picindexPath].event_type_id
        vc.viaEdit = true
        vc.event_id = self.objModelEvent[picindexPath].id
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    
    //    func setNewPicker(){
    //        if objModelEvent.count == 0{
    //            self.weddingListTextField.setPickerData(arrPickerData:[], selectedPickerDataHandler: { (text, row, component) in
    //            }, defaultPlaceholder: nil)
    //        }else{
    //            self.weddingListTextField.setPickerData(arrPickerData: objModelEvent.map({$0.event_name}), selectedPickerDataHandler: { (text, row, component) in
    //            }, defaultPlaceholder: nil)
    //        }
    //
    //    }
    
    
    
    @objc func EditEvent(sender: UIButton){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventVC") as! CreateEventVC
        vc.eventTID = self.objModelEvent[sender.tag].event_type_id
        vc.viaEdit = true
        vc.event_id = self.objModelEvent[sender.tag].id
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
//MARK:- Table View
extension HomeVC: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.objModelEvent.count == 0
        {
            return 0
        }
        else{
            return 4
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return newHeaderView
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.objModelEvent.count == 0
        {
            return UITableViewCell.init()
        }
        else
        {
            if indexPath.row == 0
            {
                let checklistCell = tableView.dequeueReusableCell(withIdentifier: "ChecklistAndBudgeterCell", for: indexPath) as! ChecklistAndBudgeterCell
                checklistCell.selectionStyle = .none
                return checklistCell
            }
            else if indexPath.row == 1
            {
                let budgetCell = tableView.dequeueReusableCell(withIdentifier: "BudgeterCell", for: indexPath) as! BudgeterCell
                budgetCell.selectionStyle = .none
                return budgetCell
            }
            else if indexPath.row == 2
            {
                let guestCell = tableView.dequeueReusableCell(withIdentifier: "GuestCell", for: indexPath) as! GuestCell
                guestCell.selectionStyle = .none
                return guestCell
            }
            else{
                let vendorCell = tableView.dequeueReusableCell(withIdentifier: "VendorCell", for: indexPath) as! VendorCell
                vendorCell.selectionStyle = .none
                return vendorCell
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.objModelEvent.count == 0
        {
            return 300
        }
        else
        {
            if(UIScreen.main.bounds.width == 375){
                return 320
            }else{
                return 350
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 900
    }
}
//MARK:- Collection Views
extension HomeVC : UICollectionViewDelegate
    ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == eventCollection
        {
            if self.objModelEvent.count == 0
            {
                return 1
            }
            else
            {
                if self.objModelEvent.count < dotsCount
                {
                    self.pageController.numberOfPages = self.objModelEvent.count
                }
                else
                {
                    self.pageController.numberOfPages = dotsCount
                }
                //                self.pageController.numberOfPages = self.objModelEvent.count
                return self.objModelEvent.count
            }
            
        }
        else if collectionView == featuredCollection
        {
            self.featuredPageController.numberOfPages = 5
            return 5
        }
        else if collectionView == recentlyViewedCollection
        {
            self.recentlyViewedPageController.numberOfPages = 5
            return 5
        }
        else
        {
            if self.objArticelModel.count < dotsCount
            {
                self.recentlyArticlesPageController.numberOfPages = self.objModelEvent.count
            }
            else
            {
                self.recentlyArticlesPageController.numberOfPages = dotsCount
            }
            
            return self.objArticelModel.count
            
            //self.recentlyArticlesPageController.numberOfPages = self.objArticelModel.count
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == eventCollection
        {
            if objModelEvent.count == 0{
                let eventCell = collectionView.dequeueReusableCell(withReuseIdentifier: EventCell.identifier, for: indexPath) as! EventCell
                eventCell.delgate = self
                eventCell.mainView.isHidden = true
                eventCell.emptyVIew.isHidden = false
                return eventCell
            }
            else{
                let eventCell = collectionView.dequeueReusableCell(withReuseIdentifier: EventCell.identifier, for: indexPath) as! EventCell
                eventCell.mainView.isHidden = false
                eventCell.emptyVIew.isHidden = true
                eventCell.editBtn.tag = indexPath.row
                EventData.shared.eventID = self.objModelEvent[0].id
                eventCell.eventNameLbl.text = self.objModelEvent[indexPath.row].event_name
                eventCell.nameLbl.text = self.objModelEvent[indexPath.row].bride_name
                eventCell.uniqueCode.text = self.objModelEvent[indexPath.row].UEI_Code
                eventCell.brideNameLbl.text = self.objModelEvent[indexPath.row].groom_name
                eventCell.andLbl.text = "&"
                let dateString = self.objModelEvent[indexPath.row].event_date
                
                //MARK:- converted into MMM,dd,yyyyqw
                eventCell.dateLbl.text = self.convertDateFormater(dateString)
                
                //MARK:- Days Left
                let dayLeft = self.completedDate(givenDate: dateString)//self.getDate(givenDate: dateString)
                //    eventCell.daysLbl.text = dayLeft
                let remaningDate = completedDate(givenDate: dateString)
                
                if self.objModelEvent[indexPath.row].event_type_id == "4"
                {
                    if remaningDate == "Completed"
                    {
                        eventCell.completeEventLbl.isHidden = false
                        eventCell.daysLbl.isHidden = true
                        eventCell.completeEventLbl.text = "Enjoy your party!"
                    }
                    else if remaningDate == "We hope you had a blast!"
                    {
                        eventCell.completeEventLbl.isHidden = false
                        eventCell.daysLbl.isHidden = true
                        eventCell.completeEventLbl.text = "Enjoy your party!"
                    }
                    else{
                        eventCell.completeEventLbl.isHidden = true
                        eventCell.daysLbl.isHidden = false
                        eventCell.daysLbl.text = dayLeft
                    }
                }
                else if self.objModelEvent[indexPath.row].event_type_id == "1"
                {
                    if remaningDate == "Completed"
                    {
                        eventCell.completeEventLbl.isHidden = false
                        eventCell.daysLbl.isHidden = true
                        eventCell.completeEventLbl.text = "Congratulations!"
                    }
                    else if remaningDate == "dayLeft"
                    {
                        eventCell.completeEventLbl.isHidden = false
                        eventCell.daysLbl.isHidden = true
                        eventCell.completeEventLbl.text = "It's your wedding day!"
                    }
                    else
                    {
                        eventCell.completeEventLbl.isHidden = true
                        eventCell.daysLbl.isHidden = false
                        eventCell.daysLbl.text = dayLeft
                    }
                }
                else
                {
                    if remaningDate == "Completed"
                    {
                        eventCell.completeEventLbl.isHidden = false
                        eventCell.daysLbl.isHidden = true
                        eventCell.completeEventLbl.text = "Congratulations!"
                    }
                    else if remaningDate == "We hope you had a blast!"
                    {
                        eventCell.completeEventLbl.isHidden = false
                        eventCell.daysLbl.isHidden = true
                        eventCell.completeEventLbl.text = "It's your engagement day!"
                    }
                    else
                    {
                        eventCell.completeEventLbl.isHidden = true
                        eventCell.daysLbl.isHidden = false
                        eventCell.daysLbl.text = dayLeft
                    }
                }
                
                if let img_url = URL(string:self.objModelEvent[indexPath.row].event_image)
                {
                    eventCell.eventPic.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "HeaderRing"))
                }
                if self.objModelEvent[indexPath.row].event_type_id == "4"
                {
                    eventCell.nameLbl.isHidden = true
                    eventCell.andLbl.isHidden = true
                    eventCell.brideNameLbl.isHidden = true
                    eventCell.topOfLbl.constant = 30
                    
                }
                else
                {
                    eventCell.nameLbl.isHidden = false
                    eventCell.andLbl.isHidden = false
                    eventCell.brideNameLbl.isHidden = false
                    eventCell.topOfLbl.constant = 43.5
                }
                eventCell.editBtn.addTarget(self, action: #selector(EditEvent(sender:)), for: .touchUpInside)
                
                return eventCell
            }
        }
        else if collectionView == featuredCollection
        {
            let featuredCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeaturedCell", for: indexPath) as! FeaturedCell
            return featuredCell
        }
        else if collectionView == recentlyViewedCollection
        {
            let recentlyViewedCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentlyViewedCell", for: indexPath) as! RecentlyViewedCell
            return recentlyViewedCell
        }
        else
        {
            let recentArticles = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentlyArticlesCell", for: indexPath) as! RecentlyArticlesCell
            
            if let img_url = URL(string: self.objArticelModel[indexPath.row].image)
            {
                recentArticles.articleImg.af_setImage(withURL: img_url, placeholderImage: #imageLiteral(resourceName: "EmptyEvent"))
            }
            recentArticles.articleNameLbl.text = self.objArticelModel[indexPath.row].title
            
            return recentArticles
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == recentlyArticlesCollection
        {
            let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PDFViewVC") as! PDFViewVC
            vc.pdf = self.objArticelModel[indexPath.row].postLink
            self.viewController?.present(vc, animated: true, completion: nil)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        if collectionView == eventCollection
        {
            if objModelEvent.count == 0
            {
                let collectionwidth = collectionView.bounds.width
                return CGSize(width: collectionwidth  , height: 210)
            }
            else
            {
                if(objModelEvent[indexPath.row].event_type_id == "4")
                {
                    let collectionwidth = collectionView.bounds.width
                    return CGSize(width: collectionwidth  , height:230)
                }
                else
                {
                    let collectionwidth = collectionView.bounds.width
                    let collectionHeight = collectionView.bounds.height
                    return CGSize(width: collectionwidth  , height:245)
                }
            }
        }
        else
        {
            let collectionwidth = collectionView.bounds.width
            let collectionHeight = collectionView.bounds.height
            return CGSize(width: collectionwidth  , height:collectionHeight)
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        if collectionView == eventCollection{
            self.scrollCollection = "Event"
        }
        else if collectionView == featuredCollection{
            self.scrollCollection = "Featured"
        }
        else if collectionView == recentlyViewedCollection{
            self.scrollCollection = "Recent Viewed"
        }
        else{
            self.scrollCollection = "Recent Article"
        }
    }
    //MARK:- Scrolling Functions
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let pageWidth = scrollView.frame.width
        self.currentPage = Int(scrollView.contentOffset.x + pageWidth / 2) / Int(scrollView.frame.width)
        self.pageController.currentPage = self.currentPage! % dotsCount
        
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.scrollCollection == "Event"
        {
            self.pageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
            
            
            if self.objModelEvent.count != 0
            {
                for cell in eventCollection.visibleCells
                {
                    let indexPath = eventCollection.indexPath(for: cell)
                    EventData.shared.eventID = self.objModelEvent[indexPath!.row].id
                    print(EventData.shared.eventID)
                }
            }
            
        }
        else if self.scrollCollection == "Featured"
        {
            self.featuredPageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
        else if self.scrollCollection == "Recent Viewed"
        {
            self.recentlyViewedPageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
        else
        {
            self.recentlyArticlesPageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
        if self.scrollCollection == "Event"
        {
            self.pageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
        else if self.scrollCollection == "Featured"
        {
            self.featuredPageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
        else if self.scrollCollection == "Recent Viewed"
        {
            self.recentlyViewedPageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
        else
        {
            self.recentlyArticlesPageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
    }
    
    
}


//MARK:- Picker

extension HomeVC : UIPickerViewDelegate , UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return  objModelEvent.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return objModelEvent[row].event_name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.picindexPath = row
    }
    @objc func doneRoleButton(_ sender : Any){
        if objModelEvent.count == 0{
            self.EmptyView.isHidden = false
        }else{
            setPicker()
    }
    }
    func setPicker()
    {
        self.EmptyView.isHidden = true
        self.eventCode.text = self.objModelEvent[picindexPath].UEI_Code
        self.dateLbl.text = self.convertDateFormater(objModelEvent[picindexPath].event_date)
        self.eventTypeNameLbl.text = self.objModelEvent[picindexPath].eventDetails[0].event_type_name
        self.daysLabel.text = self.completedDate(givenDate: objModelEvent[picindexPath].event_date)
        self.admin.text = self.objModelEvent[picindexPath].user_type
        UserDefaults.standard.set(objModelEvent[picindexPath].id, forKey: "EventIDFromCreateEVent")
        if(objModelEvent[picindexPath].event_type_id == "4"){
            self.weddingListTextField.text = self.objModelEvent[picindexPath].event_name
            self.brideName.text = "Party"
            self.groomName.text = ""
            self.andLbl.isHidden = true
            self.mainImage.sd_setImage(with: URL(string: objModelEvent[picindexPath].event_image),placeholderImage: #imageLiteral(resourceName: "weddingImg"))
         }else{
            self.mainImage.sd_setImage(with: URL(string: objModelEvent[picindexPath].event_image),placeholderImage: #imageLiteral(resourceName: "weddingImg"))
            self.weddingListTextField.text = "\(self.objModelEvent[picindexPath].groom_name) " + "\(self.objModelEvent[picindexPath].bride_name) " + "\(self.objModelEvent[picindexPath].event_name)"
            self.brideName.text = self.objModelEvent[picindexPath].bride_name
            self.groomName.text = self.objModelEvent[picindexPath].groom_name
            self.brideName.isHidden = false
            self.groomName.isHidden = false
            self.andLbl.isHidden = false
        }
        
        
        
    }
}
//MARK:- Event Data
extension HomeVC
{
    func hitServicegetEventData()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token,
            "Accept": "application/json"
        ]
        ServiceManager.instance.request(method: .get, URLString: "getEvents/"+CommonUtilities.shared.getuserdata()!.id, parameters:nil, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!{
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = dictionary?["data"] as? [String:Any]{
                            if let eventlist = data["eventList"] as? [[String:Any]]{
                                self.objModelEvent.removeAll()
                                for list in eventlist
                                {
                                    var obj = ModelEventList()
                                    if let id = list["id"] as? Int{
                                        obj.id = String(id)
                                    }
                                    if let bride_name = list["bride_name"] as? String{
                                        obj.bride_name = bride_name
                                    }
                                    if let event_date = list["event_date"] as? String{
                                        obj.event_date = event_date
                                    }
                                    if let event_image = list["event_image"] as? String{
                                        self.eventImage = event_image
                                        obj.event_image = event_image
                                        
                                    }
                                    if let event_name = list["event_name"] as? String{
                                        obj.event_name = event_name
                                    }
                                    if let groom_name = list["groom_name"] as? String{
                                        obj.groom_name = groom_name
                                    }
                                    if let event_type_id = list["event_type_id"] as? Int{
                                        obj.event_type_id = String(event_type_id)
                                    }
                                    if let UEI_Code = list["UEI_Code"] as? String
                                    {
                                        obj.UEI_Code = UEI_Code
                                    }
                                    if let event_user_created_id = list["event_created_user_id"] as? Int{
                                        obj.event_created_user_id = event_user_created_id
                                    }
                                    if let user_type = list["user_type"] as? String{
                                        obj.user_type = user_type
                                        EventData.shared.user_type = user_type
                                        print(EventData.shared.user_type)
                                        
                                    }
                                    if let event_type = list["event_type"] as? [String:Any]{
                                        let details_obj = EventData()
                                        if let event_type_name = event_type["event_type_name"] as? String{
                                            details_obj.event_type_name = event_type_name
                                        }
                                        obj.eventDetails.append(details_obj)
                                    }
                                    self.objModelEvent.append(obj)
                                }
//                                self.weddingListTextField.text = self.objModelEvent[0].event_name
                                self.setPicker()
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                    self.eventCollection.reloadData()
                                }
                                //                                self.eventCollection.reloadData()
                                self.homeTable.reloadData()
                            }
                        }
                        if self.objModelEvent.count == 0{
                            self.setDesign()
                        }
                    }
                    
                }
            }
        }
    }
    func HitServiceget_Articles()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0.2156862745, blue: 0.3725490196, alpha: 1)])
        KRProgressHUD.show()
        
        let headers: HTTPHeaders = [ "Authorization": "Bearer "+CommonUtilities.shared.getuserdata()!.access_token, "Content-Type": "application/json"]
        ServiceManager.instance.request(method: .get, URLString: "getArticles", parameters: nil, encoding: JSONEncoding.default, headers: headers)
        { (success, dictionary, error) in
            KRProgressHUD.dismiss()
            if (success)!{
                if let statuscode = dictionary?["status"] as? NSNumber
                {
                    if statuscode == 200
                    {
                        if let data = dictionary?["data"] as? [String:Any]
                        {
                            if let article = data["article"] as? [[String:Any]]
                            {
                                for articleList in article
                                {
                                    let obj = ArticelModel()
                                    
                                    if let author_name = articleList["author_name"] as? String
                                    {
                                        obj.author_name = author_name
                                    }
                                    if let postLink = articleList["postLink"] as? String
                                    {
                                        obj.postLink = postLink
                                    }
                                    if let post_date = articleList["post_date"] as? String
                                    {
                                        obj.post_date = post_date
                                    }
                                    if let image = articleList["image"] as? String
                                    {
                                        obj.image = image
                                    }
                                    if let title = articleList["title"] as? [String:Any]
                                    {
                                        if let rendered = title["rendered"] as? String
                                        {
                                            obj.title = rendered
                                        }
                                    }
                                    self.objArticelModel.append(obj)
                                }
                                self.recentlyArticlesCollection.reloadData()
                            }
                            
                        }
                        
                    }
                    else
                    {
                        if let message = dictionary?["message"] as? String
                        {
                            self.createAlert(message: message)
                        }
                    }
                }
            }
        }
    }
}


class ModelEventList
{
    var id = ""
    var bride_name = ""
    var event_date = ""
    var event_image = ""
    var event_name = ""
    var groom_name = ""
    var event_type_id = ""
    var event_created_user_id = Int()
    var UEI_Code = ""
    var user_type = ""
    var eventDetails = [EventData]()
}

class EventData
{
    static var shared = EventData()
    var eventID = ""
    var event_type_name = ""
    var user_type = ""
    
}
