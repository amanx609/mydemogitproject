//
//  VendorCollCell.swift
//  The_WeddingHat_User
//
//  Created by appl on 04/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//

import UIKit

class VendorCollCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    
    override func awakeFromNib() {
        self.mainView.makeRoundCorner(6)
    }
}
