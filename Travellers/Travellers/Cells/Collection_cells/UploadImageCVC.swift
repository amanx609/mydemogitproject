//
//  UploadImageCVC.swift
//  Travellers
//
//  Created by apple on 27/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class UploadImageCVC: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var crossBtn: UIButton!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
