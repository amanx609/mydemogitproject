//
//  OnBoardingCollCell.swift
//  Travellers
//
//  Created by apple on 14/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class OnBoardingCollCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var main_image: UIImageView!
    @IBOutlet weak var welcom_lbl: UILabel!
    @IBOutlet weak var quotes_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.main_image.image = #imageLiteral(resourceName: "Mask Group 31")
    }
}
