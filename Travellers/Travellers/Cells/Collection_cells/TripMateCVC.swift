//
//  TripMateCVC.swift
//  Travellers
//
//  Created by apple on 28/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class TripMateCVC: UICollectionViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var friendsImage: UIImageView!
    @IBOutlet weak var crossImage: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        crossImage.layer.cornerRadius = crossImage.bounds.height / 2
        crossImage.layer.borderWidth = 2
        crossImage.layer.borderColor = UIColor(hexString: "#0E1533")?.cgColor
    }
    
}



class AddMoreMate : UICollectionViewCell
{
    
    @IBOutlet weak var mainView: UIView!
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
