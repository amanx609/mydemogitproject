//
//  City_Collection_Cell.swift
//  Travellers
//
//  Created by apple on 18/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class City_Collection_Cell: UICollectionViewCell {
 
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var cityName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainView.layer.cornerRadius = 18
        self.mainView.layer.masksToBounds = true
    }
}
