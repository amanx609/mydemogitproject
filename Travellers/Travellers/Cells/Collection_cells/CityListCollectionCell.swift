//
//  CityListCollectionCell.swift
//  Travellers
//
//  Created by apple on 19/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class CityListCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var Trip_image: UIImageView!
    @IBOutlet weak var type_name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainView.layer.cornerRadius = 20
    }
}
