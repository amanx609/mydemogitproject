//
//  AboutUsCVCell.swift
//  Travellers
//
//  Created by apple on 03/11/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class AboutUsCVCell: UICollectionViewCell {
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var activityImage: UIImageView!
    @IBOutlet weak var activityNameLbl: UILabel!
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
