//
//  Trip_List_Cell.swift
//  Travellers
//
//  Created by apple on 19/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class Trip_List_Cell: UICollectionViewCell {
    
    @IBOutlet weak var main_View: UIView!
    @IBOutlet weak var type_name: UILabel!
    @IBOutlet weak var trip_type_image: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        main_View.layer.cornerRadius = 20
        
    }
}
