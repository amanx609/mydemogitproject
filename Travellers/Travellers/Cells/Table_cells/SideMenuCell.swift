//
//  SideMenuCell.swift
//  Travellers
//
//  Created by apple on 21/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var sideImg: UIImageView!
    @IBOutlet weak var sideLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
