//
//  ProfileTableCell.swift
//  Travellers
//
//  Created by apple on 02/11/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class ProfileTableCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var dotView: UIView!
    @IBOutlet weak var countryVisited: UILabel!
    @IBOutlet weak var circularDot: UIView!
    @IBOutlet weak var downArrowBtn: UIButton!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var numberOfLikes: UILabel!
    @IBOutlet weak var numberOfComment: UILabel!
    @IBOutlet weak var bookmarkBtn: UIButton!
    @IBOutlet weak var noOfCitiesVisited: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUi()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setUi()
    {
        self.dotView.layer.cornerRadius = self.dotView.bounds.height / 2
        self.circularDot.layer.cornerRadius = self.circularDot.bounds.height / 2
        self.profileImage.layer.cornerRadius = self.profileImage.bounds.height / 2
        self.mainImage.layer.cornerRadius = 15
        self.mainImage.clipsToBounds = true
    }

}
