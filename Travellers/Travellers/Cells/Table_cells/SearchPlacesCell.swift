//
//  SearchPlacesCell.swift
//  Travellers
//
//  Created by apple on 03/11/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class SearchPlacesCell: UITableViewCell {

    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var tourType: UILabel!
    @IBOutlet weak var locatiionIcon: UIImageView!
    @IBOutlet weak var cityBoldLabel: UILabel!
    @IBOutlet weak var noOfBlogs: UILabel!
    @IBOutlet weak var listOfImageView: UIView!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var img5: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUI()
    {
        mainImage.layer.cornerRadius = 15
        mainImage.clipsToBounds = true
        img1.layer.borderColor = UIColor.white.cgColor
        img1.layer.borderWidth = 1
        img2.layer.borderColor = UIColor.white.cgColor
        img2.layer.borderWidth = 1
        img3.layer.borderColor = UIColor.white.cgColor
        img3.layer.borderWidth = 1
        img4.layer.borderColor = UIColor.white.cgColor
        img4.layer.borderWidth = 1
        img5.layer.borderColor = UIColor.white.cgColor
        img5.layer.borderWidth = 1
    }

}
