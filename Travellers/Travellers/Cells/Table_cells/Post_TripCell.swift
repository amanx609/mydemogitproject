//
//  Post_TripCell.swift
//  Travellers
//
//  Created by apple on 22/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class Post_TripCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var country_img: UIImageView!
    @IBOutlet weak var country_nameLbl: UILabel!
    @IBOutlet weak var button_edit: UIButton!
    @IBOutlet weak var button_delete: UIButton!
    
    @IBOutlet weak var city_collectionView: UICollectionView!
    
     var cityArr = ["Mumbai","Delhi","Kolkata","bihar","Noida","Gangtok","Bhubneswar","Ooty","Goa","Banglore","Kanpur","Lucknow","Varanasi","Jaunpur","Kochin","kerela","Combitore","Mysuru","Podicherry","Daman","Gujrat","Tirupati","Jammu","Haryana"]
      
    override func awakeFromNib() {
        self.city_collectionView.delegate = self
        self.city_collectionView.dataSource = self
        self.mainView.layer.borderColor = UIColor.white.cgColor
        self.mainView.layer.borderWidth = 1
        self.mainView.layer.cornerRadius = 16
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

extension Post_TripCell: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cityArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Post_TripCity_CollCell", for: indexPath) as! Post_TripCity_CollCell
        cell.mainView.layer.cornerRadius = 5
        cell.city_name.text = cityArr[indexPath.row]
        cell.roundView.layer.cornerRadius = cell.roundView.frame.height / 2
        return cell
    }
    
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let text = self.cityArr[indexPath.row]
       let width = self.estimatedFrame(text: text, font: UIFont(name: "SFProDisplay-Regular", size: 26.0)!).width
            print(width)
        return CGSize(width: width + 5.0, height: 50.0)
        
    }
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
        let size = CGSize(width: 150, height: 1000) // temporary size
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size,
                                                   options: options,
                                                   attributes: [NSAttributedString.Key.font: font],
                                                   context: nil)
    }
    
}
