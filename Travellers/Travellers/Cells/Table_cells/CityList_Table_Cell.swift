//
//  CityList_Table_Cell.swift
//  Travellers
//
//  Created by apple on 18/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class CityList_Table_Cell: UITableViewCell , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
  
    @IBOutlet weak var city_List_coll: UICollectionView!
    var iSelected : [String] = []
    var cityArr = ["Mumbai","Delhi","Kolkata","bihar","Noida","Gangtok","Bhubneswar","Ooty","Goa","Banglore","Kanpur","Lucknow","Varanasi","Jaunpur","Kochin","kerela","Combitore","Mysuru","Podicherry","Daman","Gujrat","Tirupati","Jammu","Haryana"]
    
    var seletedArray = [Int]()
    override func awakeFromNib() {
        self.city_List_coll.delegate = self
        self.city_List_coll.dataSource = self
        super.awakeFromNib()
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cityArr.count
      }
      
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "City_Collection_Cell", for: indexPath) as! City_Collection_Cell
        cell.cityName.text = cityArr[indexPath.row]
        if(seletedArray.contains(indexPath.row)){
            cell.mainView.backgroundColor = UIColor(named: "sign_in_color")
            cell.cityName.textColor = .white
        }else{
            cell.mainView.backgroundColor = .white
            cell.cityName.textColor = UIColor(named: "app_color")
        }
        return cell
      }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let text = self.cityArr[indexPath.row]
       let width = self.estimatedFrame(text: text, font: UIFont(name: "SFProDisplay-Regular", size: 26.0)!).width
            print(width)
        return CGSize(width: width + 5.0, height: 50.0)
        
    }
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
        let size = CGSize(width: 150, height: 1000) // temporary size
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size,
                                                   options: options,
                                                   attributes: [NSAttributedString.Key.font: font],
                                                   context: nil)
    }
    
//    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
//           city_List_coll.layoutIfNeeded()
//
////        let topConstraintConstant = contentView.constraint(forAttribute: "topAnchor")?.constant ?? 0
////        let bottomConstraintConstant = contentView.constraint(forAttribute: "bottomAnchor")?.constant ?? 0
////        let trailingConstraintConstant = contentView.constraint(forAttribute: "trailingAnchor")?.constant ?? 0
////        let leadingConstraintConstant = contentView.constraint(forAttribute: "leadingAnchor")?.constant ?? 0
//
//           city_List_coll.frame = CGRect(x: 0, y: 0, width: targetSize.width , height: 1)
//
//           let size = city_List_coll.collectionViewLayout.collectionViewContentSize
//           let newSize = CGSize(width: size.width, height: size.height)
//           return newSize
//       }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let index = seletedArray.index(of: indexPath.row)
        {
            self.seletedArray.remove(at: index)
            print(index)
        }
        else
        {
            self.seletedArray.append(indexPath.row)
        }
        collectionView.reloadData()
    }
}

