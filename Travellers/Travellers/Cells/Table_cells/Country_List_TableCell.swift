//
//  Country_List_TableCell.swift
//  Travellers
//
//  Created by apple on 16/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class Country_List_TableCell: UITableViewCell {

    @IBOutlet weak var country_FlagImg: UIImageView!
    @IBOutlet weak var country_name: UILabel!
    @IBOutlet weak var tick_img: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
