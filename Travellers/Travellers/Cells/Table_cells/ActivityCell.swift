//
//  ActivityCell.swift
//  Travellers
//
//  Created by apple on 27/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class ActivityCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = UIColor.white.cgColor
        mainView.layer.cornerRadius = 16

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
