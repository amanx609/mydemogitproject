//
//  HeaderViewCell.swift
//  Travellers
//
//  Created by apple on 27/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class HeaderViewCell: UITableViewCell {

    @IBOutlet weak var Headerlabel: UILabel!
    @IBOutlet weak var headerButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
