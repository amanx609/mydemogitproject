//
//  Country.swift
//  Travellers
//
//  Created by apple on 18/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation

struct Country {
    var countryName = ""
    var countryFlag = ""
}
