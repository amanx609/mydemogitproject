//
//  SidemenuVC.swift
//  The_WeddingHat_User
//
//  Created by appl on 06/06/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//
/*
import UIKit

class SidemenuVC: UIViewController {
    
    
    let defaults = UserDefaults.standard
    
    var menuArray = [["image":"noun_Community_1044649",
                      "text":Constant.string.Community.localized()],
                     ["image":"noun_event_2080617",
                      "text":Constant.string.Events.localized()],
                     ["image":"Group 1539",
                      "text":Constant.string.Dresses.localized()],
                     ["image":"new_ring",
                      "text":Constant.string.Jewelry.localized()],
                     ["image":"Icon ionic-md-help-buoy",
                      "text":Constant.string.Help.localized()],
                     ["image":"Icon awesome-star",
                      "text":Constant.string.rates.localized()],
                     ["image":"Icon awesome-share-alt",
                      "text":Constant.string.Share.localized()],
                     ["image":"Icon awesome-info-circle",
                      "text":Constant.string.about.localized()],
                     ["image":"noun_logout_1350704",
                      "text":Constant.string.logout.localized()]]
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var sidemenuTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.makeRoundCorner(10)
        self.view.dropShadow()
        self.sidemenuTable.separatorColor = .clear
    }
}


extension SidemenuVC: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SidemenuCell", for: indexPath) as! SidemenuCell
        cell.selectionStyle = .none
        cell.menulbl.text = menuArray[indexPath.row]["text"]
        cell.imgView.image =  UIImage(named: menuArray[indexPath.row]["image"]!)
        if indexPath.row == 3{
            cell.bottomlbl.isHidden = false
            cell.centerOfImgView.constant = -10.0
        }
        else
        {
            cell.bottomlbl.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3{
            return 80
        }
        else{
            return 60
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            self.slideMenuController()?.closeLeft()
            let storyboard = UIStoryboard(name: "Vendordetails", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CommunityVC") as! CommunityVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 8{
            self.slideMenuController()?.closeLeft()
            if SocialLogin.shared.loginvia == true {
                self.presentAlertViewWithTwoButtons(alertTitle: Constant.string.appname.localized(), alertMessage: Constant.string.logoutMsg.localized(), btnOneTitle: Constant.string.Yes.localized(), btnOneTapped: { (_) in
                    let manager = LoginManager()
                    manager.logOut()
                    UserDefaults.standard.removeObject(forKey: "User_info")
                    CommonUtilities.shared.setLoginScreen()
                }, btnTwoTitle:  Constant.string.No.localized(), btnTwoTapped: { (_) in
                    print(Constant.string.No.localized())
                })
            }else{
                self.presentAlertViewWithTwoButtons(alertTitle: Constant.string.appname.localized(), alertMessage: Constant.string.logoutMsg.localized(), btnOneTitle: Constant.string.Yes.localized(), btnOneTapped: { (_) in
                    UserDefaults.standard.removeObject(forKey: "User_info")
                    self.defaults.set(true, forKey: "Logout")
                    CommonUtilities.shared.isLogout = true
                    CommonUtilities.shared.setLoginScreen()
                }, btnTwoTitle: Constant.string.No.localized(), btnTwoTapped: { (_) in
                    print(Constant.string.No.localized())
                })
            }
        }
        else{
            self.createAlert(message: Constant.string.underdevlopment)
        }
    }
}
*/
