//
//  File.swift
//  Wadooni
//
//  Created by appl on 25/11/19.
//  Copyright © 2019 com.ripenapps.wadooni. All rights reserved.
//

import Foundation

struct Constant
{
    static var string = Constant()
    let appname = "Traveller"
    let ok = "Ok"
    let No = "No"
    let Yes = "Yes"
    let underdevlopment = "Under Development"
    
    //====================  Log In VC =====================
    
    let forgotPassword = "Forgot password?"
    let email = "Email Id"
    let password = "Password"
    let signIn = "Sign in"
    let signInWith = "Sign in with Facebook"
    let noAccount = "Don't have an account?"
    let signUp = "Sign up"
    let orWith = "Or"
    
    //////====================  Pllaceholder Msgs(Log In VC) =====================
    
    let Empty_email = "Enter your Email Id"
    let validEmail = "Please enter a valid email Id"
    let enterPassword = "You need to enter the password to login"
    let passwordLessThanEight = "Your password must be at least 8 characters"
    let wrongPassword = "Wrong password"
    let verification = "We have sent a verification email to your registered address. Once you verify your email address, you will have access to your profile and all the planning tools and vendor listings on The Wedding Hat! The verification email will expire in 24 hours."
    let validPhoneNumber = "Please enter a valid mphone no"
    let validPassword = "Please enter a valid password"
    //================== SignUp VC =======================
    
    let fullname = "Full name"
    let createPassword = "Create password"
    let country = "Country of residence"
    let confirmPassword = "Confirm password"
    let sign = "Sign in"
    let done = "Done"
    let alreadyAccount = "Already have an account?"
    
    //////====================  Pllaceholder Msgs(SignUP VC) =====================
    
    let Empty_name = "Enter your name"
    let empty_country = "Please select the country of residence"
    let passwordnotMatchUp = "Your password and confirm password do not match"
    let confirmPasswordSignUp = "Please confirm your password"
    let policy = "Please agree to the Terms and Conditions and Privacy Policy"
    let user_name = "Enter your user name"
    let phone_number = "Enter your mobile number"
    
    
     //=================== Create Event VC =====================
    
    
    let event_type = "Event type"
    let event_name = "Event name"
    let bride_name = "Bride name"
    let groom_name = "Groom name"
    let select_country = "Select country"
    let select_city = "Select city"
    let event_date = "Event date (dd/mm/yy)"
    let no_of_guest = "No. of guests"
    let your_budget = "Your budget"
    let add_member_to_event = "Add members to this event"
    let member_name = "Member name"
    let add_more_member = "Add a member"
    let cancel = "cancel"
    let create_event = "Create event"
    let editEvent = "Edit event"
    let updateEvent = "Update event"
    
    
    //////====================  Pllaceholder Msgs(Create Event VC) =====================
    
    let eventType = "Select the event type"
    let eventName = "Enter the event name"
    let brideName = "Enter the bride name"
    let groomName = "Enter the groom name"
    let Emptycountry = "Please select a country"
    let city = "Please select a city"
    let eventDate = "Enter the event date"
    let guestNo = "Enter the number of guest"
    let budget = "Enter your budget"
    let emptyImg = "Upload an event image"
    
    
    
    //=================== Home VC =====================
    
    let yourEvent = "Your events"
    let addEvent = "Add event"
    let edit = "Edit"
    let planning = "Planning"
    let checklist = "CheckList"
    let taskCompelete = "Tasks compeleted"
    let upcomingTask = "Upcoming task:"
    let sendInvites = "Send invites"
    let budgeter = "Budgeter"
    let spent = "Total booked"
    let manageExpense = "Manage and keep track of your expenses"
    let guest = "Guest list"
    let confirmedGuest = "Guests confirmed"
    let pending = "Pending"
    let declined = "Declined"
    let vendorTeam = "Vendor team"
    let vendorHired = "Vendors hired:"
    let featuredVendor = "Featured vendors"
    let viewAll = "View all"
    let review = "Reviews"
    let recentlyViwed = "Recently viewed"
    let recentlyAritcles = "Recent articles"
    let selectRole = "Select role"
    
    
    //=================== Planning VC =====================
    
    let plan = "Planning"
    let journey = "Journey"
    let dream = "Dream board"

    //===================== SideMenu VC =================
    
    let Create_a_poll = "Create a poll"
    let Create_an_itinerary = "Create an itinerary"
    let Settings = "Settings"
    let logout = "Log Out"
    let about = "About"
    
    
    //////====================  Pllaceholder Msgs(SideMenu VC) =====================
    
    let logoutMsg = "Are you sure you want to logout?"
    
    //===================== RecoverLinkVC VC =================
    let  resetMsg = "A reset link has been sent to your email"
    let kindlyreset = "Please reset your password and sign in."
    
   // ================ Add more member ===============
        let memberName = "Enter the member name"
        let memberEmail = "Enter the member's email id"
        let slecetRole = "Select the role of the member"
        let provideAccess = "Provide atlest one access to the member"
        let maximumamount = "Budget cannot be exceed to 1 million"
        let maximumguest = "No. of guests cannot exceed 5,000"
        let recoverPassword = "******"
}
