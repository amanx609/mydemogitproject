//
//  ServiceManager.swift
//  Wadooni
//
//  Created by appl on 23/11/19.
//  Copyright © 2019 com.ripenapps.wadooni. All rights reserved.
//

import Foundation
import Alamofire

class ServiceManager: NSObject
{
    var reachability : NetworkReachabilityManager?
    static let instance = ServiceManager()
    var alertShowing = false
    
    
    enum Method: String
    {
        case GET_REQUEST = "GET"
        case POST_REQUEST = "POST"
        case PUT_REQUEST = "PUT"
        case DELETE_REQUEST = "DELETE"
    }
    
    //Alamofire.request(kApiURL, method: method, parameters: parameters, encoding: encoding, headers: headers)
    func request(method: HTTPMethod, URLString: String, parameters: [String : Any]?, encoding: ParameterEncoding, headers:  [String: String]? ,isshowing : Bool = true, completionHandler: @escaping (_ success:Bool?,[String : AnyObject]?, NSError?) -> ())
    {
        if ReachabilityNetwork.isConnectedToNetwork() == true
        {
            let kApiURL = BASEURL + URLString;
            Alamofire.request(kApiURL, method: method, parameters: parameters, encoding: encoding, headers: headers).response(completionHandler: { (response) in
                do
                {
                    if (response.error == nil)
                    {
                        let jsonResult = try JSONSerialization.jsonObject(with: response.data!, options: []) as! [String : AnyObject]
                        print(jsonResult)
                       completionHandler(true, jsonResult, nil)
                    }
                    else
                    {
                        print(response.error!)
                        completionHandler(false, nil, response.error! as NSError)
                    }
                }
                catch let error as NSError
                {
                    print(error)
                    if let param = parameters
                    {
                        self.hitGetCrash(getParams: param)
                    }
                    completionHandler(false, nil, nil)
                }
            })
        }
        else
        {
            if(isshowing)
            {
                if(!self.alertShowing)
                {
                    self.alertShowing = true
                    let alert = UIAlertController(title: "Network Problem", message: kAlertNoNetworkMessage, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: { (act) in
                        self.alertShowing = false
                    }))
                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
                }
                completionHandler(false ,nil, NSError(domain:"somedomain", code:9002))
                
            }}
    }
    
    func hitGetCrash(getParams: [String : Any])
    {
//        let dic = getParams
//        
//        let cookieHeader = (dic.flatMap({ (key, value) -> String in
//            return "\(key)=\(value)"
//        }) as Array).joined(separator: ";")
//        
//        print(cookieHeader) // key2=value2;key1=value1
//        print("This is our crash params \(cookieHeader)")
//        let params : Parameters = ["method":"apiParameter","parameter": cookieHeader, "device_type": "ios", "app_key": "123456"]
//        
//        Alamofire.request(BASEURL, method: .post,parameters: params,encoding: JSONEncoding.default, headers: [:] ).responseJSON {
//            response in
//            switch response.result {
//                
//            case .success:
//                print(params)
//                print(response)
//                
//                break
//            case .failure(let error):
//                print(params)
//                print(error)
//                
//            }
//        }
        
    }
    
}

extension UIApplication
{
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController?
    {
        if let nav = base as? UINavigationController
        {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController
        {
            if let selected = tab.selectedViewController
            {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController
        {
            return topViewController(base: presented)
        }
        return base
    }
}

