//
//  CommanUtilities.swift
//  The_WeddingHat_User
//
//  Created by apple on 20/05/20.
//  Copyright © 2020 com.ripenapps.local. All rights reserved.
//


import UIKit
import Alamofire
import Foundation
import MBProgressHUD
import AlamofireImage
import SlideMenuControllerSwift


class CommonUtilities{
    
    var window : UIWindow?
    let preferences = UserDefaults.standard
    var isLogout = Bool()
    static let shared = CommonUtilities()
    
    func getuserdata() -> Userinfo?
    {
        let objectData = UserDefaults.standard.object(forKey: "User_info") as? Data?
        if let storedObject = objectData as? NSData
        {
            let fetchedObject: Userinfo = Userinfo(data: storedObject)!
            return fetchedObject
        }
        else
        {
            return nil
        }
    }
    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func validPassword(testPassword:String) -> Bool
    {
        let passRegEx =   "^(?=.*?[A-Z]).{8,}$"
        let passTest = NSPredicate(format: "SELF MATCHES %@",passRegEx)
        return passTest.evaluate(with: testPassword)
    }

    func sidemenu()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navcontroller = storyboard.instantiateViewController(withIdentifier: "TabBar") as! TabBar
        let sidemenuvc = storyboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        let sideMenuViewController: SlideMenuController = SlideMenuController(mainViewController: navcontroller, leftMenuViewController: sidemenuvc)
        SlideMenuOptions.contentViewScale = 1.0
        let topnavigation = UINavigationController(rootViewController: sideMenuViewController)
        topnavigation.isNavigationBarHidden = true
        UIApplication.shared.windows.first?.rootViewController = topnavigation
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        SlideMenuOptions.leftViewWidth = 320
    }
    
    func setLoginScreen()
    {
        let storyboard = UIStoryboard(name: "Registration", bundle: nil)
        guard let rootVC = storyboard.instantiateViewController(withIdentifier: "Login_VC") as? Login_VC else {
            print("ViewController not found")
            return
        }
        let rootNC = UINavigationController(rootViewController: rootVC)
        UIApplication.shared.windows.first?.rootViewController = rootNC
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        rootNC.navigationBar.isHidden = true
        
    }
    
    func getselectedLanguage() -> Int
    {
        let objectData = UserDefaults.standard.object(forKey: "selectedcountryLanguage") as? Int?
        if let storedObject = objectData as? Int
        {
            return storedObject
        }
        return 0
    }
    
    
    func isLogin() -> Bool
    {
        
        if CommonUtilities.shared.getuserdata()?.id == "" || CommonUtilities.shared.getuserdata()?.id == nil
        {
            let logout = CommonUtilities.shared.preferences.object(forKey: "Logout")
            if((logout) != nil)
            {
                CommonUtilities.shared.setLoginScreen()
            }
            else
            {
//                                    CommonUtilities.shared.setLoginScreen()

                let storyboard = UIStoryboard.init(name: "Registration", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "OnBoardingScreenVC") as! OnBoardingScreenVC
                let navObj = UINavigationController(rootViewController: vc)
                navObj.isNavigationBarHidden = true
                UIApplication.shared.windows.first?.rootViewController = navObj
            }
            
            
            //                                    if self.isAppAlreadyLaunchedOnce() == false
            //                                    {
            //                                        let checkTime = self.preference.object(forKey: "Demo") as! String
            //                                        if checkTime == "First"
            //                                        {
            //                                            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            //                                            let vc = storyboard.instantiateViewController(withIdentifier: "OnBoardingScreen") as! OnBoardingScreen
            //                                            let navObj = UINavigationController(rootViewController: vc)
            //                                            navObj.isNavigationBarHidden = true
            //                                            UIApplication.shared.windows.first?.rootViewController = navObj
            //                                        }
            //
            //                                    }
                                            
        }
        else
        {
            CommonUtilities.shared.sidemenu()
        }
        return true
    }
}










struct Userinfo
{
    var fullName = ""
    var userName = ""
    var dailCode = ""
    var mobileNumber = ""
    var email = ""
    var dob = ""
    var profileImage = ""
    var gender = ""
    var accountStatus = ""
    var status = ""
    var deviceToken = ""
    var _id = ""
    var createdAt = ""
    var updatedAt = ""
    var _v = ""
    var avatar = ""
    var id = ""
    var token = ""
   
}

extension Userinfo
{
    init?(data: NSData)
    {
        if let coding = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? Encoding_user
        {
            fullName        = coding.fullName
            userName        = coding.userName
            dailCode        = coding.dailCode
            mobileNumber    = coding.mobileNumber
            email           = coding.email
            dob             = coding.dob
            profileImage    = coding.profileImage
            gender          = coding.gender
            accountStatus   = coding.accountStatus
            status          = coding.status
            deviceToken     = coding.deviceToken
            _id             = coding._id
            createdAt       = coding.createdAt
            updatedAt       = coding.updatedAt
            _v              = coding._v
            avatar          = coding.avatar
            id              = coding.id
            token           = coding.token
            
        }
        else
        {
            return nil
        }
    }
    
    func encode() -> NSData
    {
        return NSKeyedArchiver.archivedData(withRootObject: Encoding_user(self)) as NSData
    }
    
    @objc(_TtCV3XOC8UserinfoP33_32514F4DE9FDE7B9B50DBF8C5E6774B513Encoding_user) private class Encoding_user: NSObject, NSCoding
    {
        var fullName = ""
        var userName = ""
        var dailCode = ""
        var mobileNumber = ""
        var email = ""
        var dob = ""
        var profileImage = ""
        var gender = ""
        var accountStatus = ""
        var status = ""
        var deviceToken = ""
        var _id = ""
        var createdAt = ""
        var updatedAt = ""
        var _v = ""
        var avatar = ""
        var id = ""
        var token = ""
        
        init(_ myObject: Userinfo)
        {
            fullName = myObject.fullName
            userName = myObject.userName
            dailCode = myObject.dailCode
            mobileNumber = myObject.mobileNumber
            email = myObject.email
            dob = myObject.dob
            profileImage = myObject.profileImage
            gender = myObject.gender
            accountStatus = myObject.accountStatus
            status = myObject.status
            deviceToken = myObject.deviceToken
            _id = myObject._id
            createdAt = myObject.createdAt
            updatedAt = myObject.updatedAt
            _v = myObject._v
            avatar = myObject.avatar
            id = myObject.id
            token = myObject.token
            
        }
        
        @objc required init?(coder aDecoder: NSCoder)
        {
            email = (aDecoder.decodeObject(forKey: "email") as? String)!
            id = (aDecoder.decodeObject(forKey: "id") as? String)!
            fullName = (aDecoder.decodeObject(forKey: "fullName") as? String)!
            userName = (aDecoder.decodeObject(forKey: "userName") as? String)!
            dailCode = (aDecoder.decodeObject(forKey: "dailCode") as? String)!
            mobileNumber = (aDecoder.decodeObject(forKey: "mobileNumber") as? String)!
            dailCode = (aDecoder.decodeObject(forKey: "dailCode") as? String)!
            avatar = (aDecoder.decodeObject(forKey: "avatar") as? String)!
            token = (aDecoder.decodeObject(forKey: "token") as? String)!
            _v = (aDecoder.decodeObject(forKey: "_v") as? String)!
            _id = (aDecoder.decodeObject(forKey: "_id") as? String)!
            updatedAt = (aDecoder.decodeObject(forKey: "updatedAt") as? String)!
            createdAt = (aDecoder.decodeObject(forKey: "createdAt") as? String)!
            deviceToken = (aDecoder.decodeObject(forKey: "deviceToken") as? String)!
            status = (aDecoder.decodeObject(forKey: "status") as? String)!
            accountStatus = (aDecoder.decodeObject(forKey: "accountStatus") as? String)!
            gender = (aDecoder.decodeObject(forKey: "gender") as? String)!
            dob = (aDecoder.decodeObject(forKey: "dob") as? String)!

        }
        @objc func encode(with aCoder: NSCoder)
        {
            aCoder.encode(fullName, forKey: "fullName")
            aCoder.encode(mobileNumber , forKey: "mobileNumber")
            aCoder.encode(userName, forKey: "userName")
            aCoder.encode(dailCode , forKey: "dailCode")
            aCoder.encode(email , forKey: "email")
            aCoder.encode(id , forKey: "id")
            aCoder.encode(dob , forKey: "dob")
            aCoder.encode(profileImage , forKey: "profileImage")
            aCoder.encode(_v , forKey: "_v")
            aCoder.encode(token , forKey: "token")
            aCoder.encode(createdAt , forKey: "createdAt")
            aCoder.encode(updatedAt , forKey: "updatedAt")
            aCoder.encode(avatar , forKey: "avatar")
            aCoder.encode(gender , forKey: "gender")
            aCoder.encode(accountStatus , forKey: "accountStatus")
            aCoder.encode(deviceToken , forKey: "deviceToken")
            aCoder.encode(status , forKey: "status")
            aCoder.encode(_id , forKey: "_id")
            
        
        }
    }
}



