//
//  ExtensionUIColor.swift
//  Swifty_Master
//
//  Created by mind-0002 on 10/11/17.
//  Copyright © 2017 Mind. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience init?(hexString: String) {
        var chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
        let red, green, blue, alpha: CGFloat
        switch chars.count {
        case 3:
            chars = chars.flatMap { [$0, $0] }
            fallthrough
        case 6:
            chars = ["F","F"] + chars
            fallthrough
        case 8:
            alpha = CGFloat(strtoul(String(chars[0...1]), nil, 16)) / 255
            red   = CGFloat(strtoul(String(chars[2...3]), nil, 16)) / 255
            green = CGFloat(strtoul(String(chars[4...5]), nil, 16)) / 255
            blue  = CGFloat(strtoul(String(chars[6...7]), nil, 16)) / 255
        default:
            return nil
        }
        self.init(red: red, green: green, blue:  blue, alpha: alpha)
    }
    
    func getRGB() -> (r:CGFloat , g:CGFloat , b:CGFloat , a:CGFloat)? {
        
        var red:CGFloat = 0.0
        var green:CGFloat = 0.0
        var blue:CGFloat = 0.0
        var alpha:CGFloat = 0.0
        
        guard self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) else { return nil }
        
        return (red , green , blue , alpha)
    }
    
//    func lightColor(byPercentage:CGFloat) -> UIColor? {
//        return adjustColor(byPercentage: abs(byPercentage))
//    }
//    
//    func darkColor(byPercentage:CGFloat) -> UIColor? {
//        return adjustColor(byPercentage: (-1 * abs(byPercentage)))
//    }
//
//    private func adjustColor(byPercentage:CGFloat) -> UIColor? {
//
//        guard let RGB = self.getRGB() else { return nil }
//
//        return UIColor(red: min(RGB.r + byPercentage/100.0, 1.0), green: min(RGB.g + byPercentage/100.0, 1.0), blue: min(RGB.b + byPercentage/100.0, 1.0), alpha: RGB.a)
//    }
}

//extension UIColor {
//    public convenience init?(hexString: String) {
//        let r, g, b, a: CGFloat
//
//        if hexString.hasPrefix("#") {
//            let start = hexString.index(hexString.startIndex, offsetBy: 1)
//            let hexColor = String(hexString[start...])
//
//            if hexColor.count == 8 {
//                let scanner = Scanner(string: hexColor)
//                var hexNumber: UInt64 = 0
//
//                if scanner.scanHexInt64(&hexNumber) {
//                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
//                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
//                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
//                    a = CGFloat(hexNumber & 0x000000ff) / 255
//
//                    self.init(red: r, green: g, blue: b, alpha: a)
//                    return
//                }
//            } else if hexColor.count == 6 {
//                let scanner = Scanner(string: hexColor)
//                var hexNumber: UInt64 = 0
//
//                if scanner.scanHexInt64(&hexNumber) {
//                    r = CGFloat((hexNumber & 0xff0000) >> 16) / 255
//                    g = CGFloat((hexNumber & 0x00ff00) >> 8) / 255
//                    b = CGFloat((hexNumber & 0x0000ff) >> 0) / 255
//
//                    self.init(red: r, green: g, blue: b, alpha: 1)
//                    return
//                }
//            }
//        }
//
//        return nil
//    }
//}
