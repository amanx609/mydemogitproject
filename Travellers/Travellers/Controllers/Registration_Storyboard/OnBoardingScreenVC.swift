//
//  ViewController.swift
//  Traveller
//
//  Created by apple on 12/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class OnBoardingScreenVC: UIViewController {

    @IBOutlet weak var quotes_lbl: UILabel!
    @IBOutlet weak var logo_name_lbl: UILabel!
    @IBOutlet weak var logo_image: UIImageView!
    @IBOutlet weak var page_control: UIPageControl!
    @IBOutlet weak var create_account_btn: UIButton!
    @IBOutlet weak var sign_in: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    var currentPage : Int?
    var dotsCount = 5
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
    }


}

extension OnBoardingScreenVC
{
    func setUpUI()
    {
        create_account_btn.layer.borderColor = UIColor(named: "border_color")?.cgColor
        sign_in.layer.borderWidth = 1
        create_account_btn.layer.borderWidth = 1
        self.page_control.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        self.page_control.numberOfPages = dotsCount
    }
    
    
}

extension OnBoardingScreenVC:  UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
   
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 397
    }
    
}

extension OnBoardingScreenVC
{
    @IBAction func sign_In_button(_ sender: UIButton)
    {
        let storybrd = UIStoryboard(name: "Registration", bundle: nil)
        let vc = storybrd.instantiateViewController(withIdentifier: "Login_VC") as! Login_VC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func create_account(_ sender : UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUp_VC") as! SignUp_VC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}





















extension OnBoardingScreenVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout, UIScrollViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let coll_cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnBoardingCollCell", for: indexPath) as! OnBoardingCollCell
        return coll_cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.size.width, height: self.collectionView.frame.size.height)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
         let pageWidth = scrollView.frame.width
               self.currentPage = Int(scrollView.contentOffset.x + pageWidth / 2) / Int(scrollView.frame.width)
               self.page_control.currentPage = self.currentPage! % dotsCount
    }
    
    
}

