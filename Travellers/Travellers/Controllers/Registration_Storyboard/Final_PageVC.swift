//
//  Final_PageVC.swift
//  Travellers
//
//  Created by apple on 20/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class Final_PageVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup   after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            CommonUtilities.shared.sidemenu()
        }
    }
    
  func pushToHomeVC()
  {
    
      let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
      vc.isFromCountry = true
      self.navigationController?.pushViewController(vc, animated: true)
  }

}
