//
//  Type_TripratVC.swift
//  Travellers
//
//  Created by apple on 19/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class Type_TripratVC: UIViewController {

    
   
    @IBOutlet weak var cntView: UIView!
    @IBOutlet weak var cntinueButton: UIButton!
    
    @IBOutlet weak var tripTypeColl: UICollectionView!
    
    var iSelected : [Int] = []
    var venuArr = ["Adventure trips", "Solo trips" , "Guided trips" , "Backpacking trips" , "Self drive trips" , "Comfortable & Relax trips" , "Photography trips" , "Natural landscapes trips" , "Art trips hiking" , "Wilderness trips","Adventure sports trips","Shopping trips"]
    var whiteIcon = ["Adventure tripsw", "Solo tripsw" , "Guided tripsw" , "Backpacking tripsw" , "Self drive tripsw" , "Comfortable & Relax tripsw" , "Photography tripsw" , "Natural landscapes tripsw" , "Art trips hikingw" , "Wilderness tripsw","Adventure sports tripsw","Shopping tripsw"]
    override func awakeFromNib() {
        super.awakeFromNib()
    }
       
    
    @IBAction func continue_btnClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Country_Screen") as! Country_Screen
        vc.countrySelect = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func back_BtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
}

extension Type_TripratVC: UICollectionViewDelegate , UICollectionViewDelegateFlowLayout , UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return venuArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Trip_List_Cell", for: indexPath) as! Trip_List_Cell
        cell.trip_type_image.image = UIImage(named: venuArr[indexPath.row])
        cell.type_name.text = venuArr[indexPath.row]
        if(iSelected.contains(indexPath.row))
        {
            cell.trip_type_image.image = UIImage(named: whiteIcon[indexPath.row])
            cell.type_name.textColor = .white
            cell.main_View.backgroundColor = UIColor(named: "sign_in_color")
        }
        else
        {
            cell.trip_type_image.image = UIImage(named: venuArr[indexPath.row])
            cell.type_name.textColor = UIColor(named: "app_color")
            cell.main_View.backgroundColor = .white
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let text = self.venuArr[indexPath.row]
       let width = self.estimatedFrame(text: text, font: UIFont(name: "SFProDisplay-Regular", size: 26.0)!).width
            print(width)
        return CGSize(width: width + 5.0, height: 50.0)
        
    }
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
        let size = CGSize(width: 150, height: 1000) // temporary size
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size,
                                                   options: options,
                                                   attributes: [NSAttributedString.Key.font: font],
                                                   context: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let index = iSelected.index(of: indexPath.row)
            {
                self.iSelected.remove(at: index)
                print(iSelected)
            }
            else
            {
                if(iSelected.count > 6)
                {
                    self.createAlert(message: "Can't select more than 7 trips")
                    print(iSelected)
                }else
                {
                    self.iSelected.append(indexPath.row)
                    print(iSelected)
                }
              
            }
        self.tripTypeColl.reloadData()
        
    }
}
