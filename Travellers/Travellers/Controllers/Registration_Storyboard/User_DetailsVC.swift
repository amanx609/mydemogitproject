//
//  User_DetailsVC.swift
//  Travellers
//
//  Created by apple on 16/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import DropDown
import ACFloatingTextfield_Objc
import IQKeyboardManagerSwift
import KRProgressHUD
import Alamofire

class User_DetailsVC: UIViewController {

    @IBOutlet weak var user_image_view: UIView!
    @IBOutlet weak var user_image: UIImageView!
    @IBOutlet weak var camera_view: UIView!
    @IBOutlet weak var camera_open_btn: UIButton!
    @IBOutlet weak var dob_text: ACFloatingTextField!
    @IBOutlet weak var continue_btn: UIButton!
    @IBOutlet weak var city_select_view: UIView!
    @IBOutlet weak var country_select_view: UIView!
    @IBOutlet weak var country_flagimg: UIImageView!
    @IBOutlet weak var country_name: UILabel!
    @IBOutlet weak var city_name: UILabel!
    let imagePicker = UIImagePickerController()
    var imgProfile : UIImage?
    var index = Int()
    
    var obj = [Country]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker.delegate = self
        self.user_image.layer.cornerRadius = user_image.frame.height / 2
        self.user_image.clipsToBounds = true
        self.camera_view.layer.cornerRadius = camera_view.frame.height / 2
        self.camera_view.layer.borderColor = UIColor.black.cgColor
        self.camera_view.layer.borderWidth = 1
        self.setDOB()
    }
    

    
    @IBAction func openCamera(_ sender: Any) {
        self.openCamera()
    }
    @IBAction func continue_ButtonClick(_ sender: Any)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Type_TripratVC") as! Type_TripratVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func select_CountryTap(_ sender: Any)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Country_Screen") as! Country_Screen
        vc.countrySelected = {
            str in
            self.country_name.text = str
        }
        self.present(vc , animated: true , completion: nil)
    }
    
    @IBAction func select_CityTap(_ sender: Any)
    {
        self.createAlert(message: Constant.string.underdevlopment)
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CityList") as! CityList
//               self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension User_DetailsVC
{
    func setDOB()
    {
        let maxDate = Calendar.current.date(byAdding: .year, value: -10, to: Date()) ?? Date()
        self.dob_text.setDatePickerWithDateFormate(dateFormate: "MMM dd , yyyy", defaultDate: nil, isPrefilledDate: false) { (date) in
        }
        self.dob_text.setDatePickerMode(mode: .date)
        self.dob_text.setMaximumDate(maxDate: maxDate)
    }
    
    
    func setCountry()
    {
       
    }
    
    func openCamera()
    {
        
                let actionSheetController = UIAlertController(title: "Please take picture", message: nil, preferredStyle: .actionSheet)
                let saveimgActionButton = UIAlertAction(title: "Camera", style: .default) { action -> Void in
                    if UIImagePickerController.isSourceTypeAvailable(.camera)
                    {
                        self.imagePicker.sourceType = .camera
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                }
                actionSheetController.addAction(saveimgActionButton)
                
                let galaryActionButton = UIAlertAction(title: "Photo Library", style: .default) { action -> Void in
                    self.imagePicker.sourceType = .photoLibrary
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
                actionSheetController.addAction(galaryActionButton)
                let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel){ action -> Void in
                    print("Cancel")
                }
                actionSheetController.addAction(cancelActionButton)
                if let popoverController = actionSheetController.popoverPresentationController {
                  popoverController.sourceView = self.view
                  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                }
                self.present(actionSheetController, animated: true, completion: nil)
            }
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
            {
                let imagess = info[.originalImage] as! UIImage
                user_image.image = imagess
                imgProfile = imagess
                picker.dismiss(animated: true, completion: nil)
            }
    override func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
            {
                picker.dismiss(animated: true, completion: nil)
            }
    
}
    

