//
//  CityList.swift
//  Travellers
//
//  Created by apple on 19/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class CityList: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout{
   

    @IBOutlet weak var cityCollectionList: UICollectionView!
    var venuArr = ["Adventure trips", "Solo trips" , "Guided trips" , "Backpacking trips" , "Self drive trips" , "Comfortable & Relax trips" , "Photography trips" , "Natural landscapes trips" , "Art trips hiking" , "Wilderness trips","Adventure sports trips","Shopping trips"]
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
  
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return venuArr.count
     }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CityListCollectionCell", for: indexPath) as! CityListCollectionCell
         cell.type_name.text = self.venuArr[indexPath.row]
        cell.Trip_image.image = UIImage(named: venuArr[indexPath.row])
         return cell
     }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
             let text = self.venuArr[indexPath.row]//self.ObjcategoryArray[indexPath.row - 1].category_name
        let width = self.estimatedFrame(text: text, font: UIFont(name: "SFProDisplay-Regular", size: 26.0)!).width
             print(width)
         return CGSize(width: width + 10.0, height: 50.0)
         
     }
     func estimatedFrame(text: String, font: UIFont) -> CGRect {
         let size = CGSize(width: 150, height: 1000) // temporary size
         let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
         return NSString(string: text).boundingRect(with: size,
                                                    options: options,
                                                    attributes: [NSAttributedString.Key.font: font],
                                                    context: nil)
     }
  
       
}
