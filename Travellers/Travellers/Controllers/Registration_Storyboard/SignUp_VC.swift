//
//  SignUp_VC.swift
//  Travellers
//
//  Created by apple on 15/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Objc
import IQKeyboardManagerSwift
import KRProgressHUD
import Alamofire


class SignUp_VC: UIViewController , UITextFieldDelegate {

    @IBOutlet weak var nameTxtField: ACFloatingTextField!
    @IBOutlet weak var userNameText: ACFloatingTextField!
    @IBOutlet weak var emailAddress: ACFloatingTextField!
    @IBOutlet weak var mobileNumber: ACFloatingTextField!
    @IBOutlet weak var password: ACFloatingTextField!
    @IBOutlet weak var confirmPassword: ACFloatingTextField!
    @IBOutlet weak var passwordEyeBtn: UIButton!
    @IBOutlet weak var confirmPasswordEyeBtn: UIButton!
    @IBOutlet weak var upperCaseImg: UIImageView!
    @IBOutlet weak var charaterImg: UIImageView!
    @IBOutlet weak var crossBtn: UIButton!
    @IBOutlet weak var user_already_usedLbl: UILabel!
    
    @IBOutlet weak var tbl_view: UITableView!
    @IBOutlet var header_view: UIView!
    @IBOutlet var footer_View: UIView!
    var iconClick = true
    var confrmIconClick = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userNameText.delegate = self
        self.tbl_view.reloadData()
        self.password.delegate = self
        self.crossBtn.isHidden = true
        self.user_already_usedLbl.isHidden = true
    }
    

    @IBAction func back_btn_tap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signIn_btnClick(_ sender: Any)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Login_VC") as! Login_VC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func create_Account(_ sender: Any)
    {
//        self.pushToVC()
//TODO:- Uncomment validation before sending
        self.Validate()
    }
    
    
    
    @IBAction func eyeBtnTap(_ sender: Any)
    {
        self.eyeBtnTap()
    }
    
    @IBAction func confirmPasswordEye(_ sender: Any)
    {
        if(self.confrmIconClick == true)
        {
            self.confrmIconClick == true
            self.confirmPassword.isSecureTextEntry = false
            self.confirmPasswordEyeBtn.setImage(#imageLiteral(resourceName: "hide"), for: .normal)
        }
        else
        {
            self.confirmPassword.isSecureTextEntry = true
            self.confirmPasswordEyeBtn.setImage(#imageLiteral(resourceName: "565654"), for: .normal)
        }
        self.confrmIconClick = !self.confrmIconClick
    }
    func eyeBtnTap()
    {
        if(self.iconClick == true) {
            self.password.isSecureTextEntry  = false
            self.passwordEyeBtn.setImage(#imageLiteral(resourceName: "hide"), for: .normal)
        } else {
            self.password.isSecureTextEntry = true
            self.passwordEyeBtn.setImage(#imageLiteral(resourceName: "565654"), for: .normal)
        }
        self.iconClick = !self.iconClick
    }
    
    
    func Validate() -> (Bool , String)
    {
        if let text = nameTxtField.text , text.isEmpty
        {
            self.createAlert(message: Constant.string.Empty_name)
            return(false , Constant.string.Empty_name)
        }
        
        if let userName = userNameText.text , userName.isEmpty
        {
            self.createAlert(message: Constant.string.user_name)
//            self.userNameText.addTarget(self, action: #selector(textFieldDidEndEditing), for: .editingDidEnd)
            return(false , Constant.string.fullname)
        }
        
        if let email = emailAddress.text , email.isEmpty
        {
            self.createAlert(message: Constant.string.Empty_email)
            return(false , "")
        }
        
        guard CommonUtilities.shared.isValidEmail(testStr: emailAddress.text!) else
        {
            self.createAlert(message: Constant.string.validEmail)
            
            return (false , Constant.string.validEmail)
        }
        
        if let mobile = mobileNumber.text , mobile.isEmpty
        {
            self.createAlert(message: Constant.string.phone_number)
            return(false , "")
        }
        
        
        if let password = password.text , password.isEmpty
        {
            self.createAlert(message: Constant.string.password)
            return(false , "")
        }        
        guard  let checkCount = password.text?.count , checkCount >= 8 else
        {
            self.createAlert(message: Constant.string.passwordLessThanEight)
            return (false, Constant.string.passwordLessThanEight)
        }
        
        guard CommonUtilities.shared.validPassword(testPassword: password.text!) else
        {
            self.createAlert(message: Constant.string.validPassword)
            return (false , Constant.string.validPassword)
        }
        guard let confirmPassword = confirmPassword.text, !confirmPassword.isEmpty else
        {
            createAlert(message: Constant.string.confirmPassword)
            return (false , "")
        }
        if(password.text != self.confirmPassword.text)
        {
            createAlert(message: Constant.string.passwordnotMatchUp)
          return (false , "")
        }
        self.signUPServices()
        return (true , "Sucess")
    }

    func textFieldDidChangeSelection(_ textField: UITextField) {
          if(textField  == userNameText)
              {
                if(userNameText.text == "")
                {
                    self.crossBtn.isHidden = true
                    self.user_already_usedLbl.isHidden = true
                }
                else
                {
                    self.crossBtn.isHidden = false
                    self.user_already_usedLbl.isHidden = false
                }
              }
              else if(textField == password)
              {
                  if(CommonUtilities.shared.validPassword(testPassword: password.text!)) == true
                  {
                      self.charaterImg.image = #imageLiteral(resourceName: "green_circle")
                      self.upperCaseImg.image = #imageLiteral(resourceName: "green_circle")
                  }
                  else
                  {
                      self.charaterImg.image = UIImage(named: "white_circle")
                      self.upperCaseImg.image = UIImage(named: "white_circle")
                  }
              }
              else
              {
                  print("")
              }
    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if(textField  == userNameText)
//        {
//            userNameText.errorText = "enter user name"
//            userNameText.errorTextColor = UIColor(hexString: "#F01616")
//            userNameText.errorLineColor = UIColor(hexString: "#F01616")
//        }
//        else if(textField == password)
//        {
//            if(CommonUtilities.shared.isValidEmail(testStr: password.text!)) == true
//            {
//                self.charaterImg.image = #imageLiteral(resourceName: "green_circle")
//                self.upperCaseImg.image = #imageLiteral(resourceName: "green_circle")
//            }
//            else
//            {
//                self.charaterImg.image = UIImage(named: "white_circle")
//                self.upperCaseImg.image = UIImage(named: "white_circle")
//            }
//        }
//        else
//        {
//            print("")
//        }
//        return true
//    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
}

extension SignUp_VC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footer_View
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return header_view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 255
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 630
    }
}

extension SignUp_VC
{
    func signUPServices()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)])
        KRProgressHUD.show()
        let params : Parameters = [
            "fullName": self.nameTxtField.text!,
            "userName": self.userNameText.text!,
            "email": emailAddress.text!,
            "dialcode": "+91",
            "mobile": mobileNumber.text!,
            "password":password.text!,
            "country":"India",
            "city":"Meerut"
            
        ] as [String:Any]
            
        print(params)
        ServiceManager.instance.request(method: .post, URLString: "register", parameters: params, encoding: JSONEncoding.default, headers: nil) { (sucess, response, error) in
            print(response ?? "")
            KRProgressHUD.dismiss()
            if let statuscode = response?["statusCode"] as? NSNumber{
                let msg = response?["message"] as! String
                if let dict = response?["data"] as? [String:Any]
                {
                    if statuscode == 200
                    {
                        var userInfoObj = Userinfo()

                        userInfoObj.token = dict["token"] as? String ?? ""
                        if let user = dict["user"] as? [String:Any]
                        {
                            userInfoObj.fullName = user["fullName"] as? String ?? ""
                            userInfoObj.userName = user["userName"] as? String ?? ""
                            userInfoObj.email = user["email"] as? String ?? ""
                            userInfoObj.dob = user["dob"] as? String ?? ""
                            userInfoObj.accountStatus = user["accountStatus"] as? String ?? ""
                            userInfoObj.avatar = user["avatar"] as? String ?? ""
                            userInfoObj.mobileNumber = user["mobileNumber"] as? String ?? ""
                            userInfoObj.deviceToken = user["deviceToken"] as? String ?? ""
                            userInfoObj._id = user["_id"] as? String ?? ""
                            userInfoObj._v = user["_v"] as? String ?? ""
                            userInfoObj.profileImage = user["profileImage"] as? String ?? ""
                            userInfoObj.createdAt = user["createdAt"] as? String ?? ""
                            userInfoObj.updatedAt = user["updatedAt"] as? String ?? ""
                        }
                        let userDefaults = UserDefaults.standard
                        userDefaults.set(userInfoObj.encode(), forKey: "User_info")
                        print(CommonUtilities.shared.getuserdata()?.token)
                        self.pushToVC()
                    }
                    else
                    {
                        self.createAlert(message: msg)
                    }
                }
                
            }
        }
    }
    
    
    func pushToVC()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "User_DetailsVC") as! User_DetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
