//
//  OTP_ScreenVC.swift
//  Travellers
//
//  Created by apple on 16/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class OTP_ScreenVC: UIViewController , UITextFieldDelegate {
    @IBOutlet weak var confirm_Btn: UIButton!
    @IBOutlet weak var otp_View_one: UIView!
    @IBOutlet weak var otp_View_two : UIView!
    @IBOutlet weak var otp_View_three : UIView!
    @IBOutlet weak var otp_View_four : UIView!
    @IBOutlet weak var otp_text_one : UITextField!
    @IBOutlet weak var otp_text_two : UITextField!
    @IBOutlet weak var otp_text_three : UITextField!
    @IBOutlet weak var otp_text_four : UITextField!
    
    var otpnum = ""
      var otp1 = ""
      var otp2 = ""
      var otp3 = ""
      var otp4 = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        otp_text_one.delegate = self
        otp_text_two.delegate = self
        otp_text_three.delegate = self
        otp_text_four.delegate = self
        textFeildSet()

    }
    

}

extension OTP_ScreenVC
{
    @IBAction func back_btnClick(_ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirm_BtnClick(_ sender : UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "User_DetailsVC") as! User_DetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func send_AgainBtn(_ sender : UIButton)
    {
        otp_text_one.text?.removeAll()
        otp_text_two.text?.removeAll()
        otp_text_three.text?.removeAll()
        otp_text_four.text?.removeAll()
    }
    
    func textFeildSet()
    {
        otp_text_one.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otp_text_two.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otp_text_three.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otp_text_four.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otp_text_one.becomeFirstResponder()
    }
    
    @objc func textFieldDidChange(textField: UITextField)
    {
        let text = textField.text
        if text?.utf8.count == 1
        {
            switch textField
            {
            case otp_text_one:
                otp_text_two.becomeFirstResponder()
            case otp_text_two:
                otp_text_three.becomeFirstResponder()
            case otp_text_three:
                otp_text_four.becomeFirstResponder()
            case otp_text_four:
                otp_text_four.resignFirstResponder()
            default:
                break
            }
        }
        
        if text?.utf8.count == 0
        {
            switch textField{
            case otp_text_one:
                otp_text_one.becomeFirstResponder()
            case otp_text_two:
                otp_text_one.becomeFirstResponder()
            case otp_text_three:
                otp_text_two.becomeFirstResponder()
            case otp_text_four:
                otp_text_three.becomeFirstResponder()
            default:
                break
            }
        }
        else
        {}
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
      {
      let maxLength = 1
      let currentString: NSString = textField.text! as NSString
      let newString: NSString =
      currentString.replacingCharacters(in: range, with: string) as NSString
      otpnum = otp1 + otp2 + otp3 + otp4
      return newString.length <= maxLength
}
    
  @IBAction func tapSubmit(_ sender: UIButton) {

    if ( otp_text_one.text == "" )||(otp_text_two.text == "" )||(otp_text_three.text == "")||(otp_text_four.text == "" ) {
        createAlert(message: "Please enter OTP")
    }
    else
    {
    otp1 = otp_text_one.text!
    otp2 = otp_text_two.text!
    otp3 = otp_text_three.text!
    otp4 = otp_text_four.text!
    otpnum = otp1 + otp2 + otp3 + otp4
    }
    }
    
}
