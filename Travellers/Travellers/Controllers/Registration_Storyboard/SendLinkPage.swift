//
//  SendLinkPage.swift
//  Travellers
//
//  Created by apple on 20/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class SendLinkPage: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTP_ScreenVC") as! OTP_ScreenVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
