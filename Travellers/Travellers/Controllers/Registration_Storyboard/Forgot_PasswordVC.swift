//
//  Forgot_PasswordVC.swift
//  Travellers
//
//  Created by apple on 15/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IQKeyboardManagerSwift
import KRProgressHUD
import Alamofire

class Forgot_PasswordVC: UIViewController {

    @IBOutlet weak var send_linkBtn: UIButton!
    @IBOutlet weak var email_mobile_txtfield: SkyFloatingLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUi()
        

    }
    
    @IBAction func back_btn_tap(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func sendLink(_ sender: Any) {
        self.validate()
    }
    
}

extension Forgot_PasswordVC
{
    func setUi()
    {
        self.send_linkBtn.layer.cornerRadius = 20
    }
    
    func validate() -> Bool
    {
        if let email = email_mobile_txtfield.text , email.isEmpty{
            return false
        }
        guard CommonUtilities.shared.isValidEmail(testStr: email_mobile_txtfield.text!) else
        {
            self.createAlert(message: Constant.string.validEmail)
         return false
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SendLinkPage") as! SendLinkPage
        self.navigationController?.pushViewController(vc, animated: true)
        return true
    }
    
    func resetPassword()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)])
        let params : Parameters = ["username": email_mobile_txtfield.text!]
        ServiceManager.instance.request(method: .post, URLString: "forgot/password", parameters: params, encoding: JSONEncoding.default, headers: nil, isshowing: true) { (sucess, response, error) in
            KRProgressHUD.dismiss()
            if(sucess)!
            {
                let msg = response?["message"] as? String
                if let sucessCode = response?["sucessCode"] as? NSNumber
                {
                    if(sucessCode == 200)
                    {
                        if let dict = response?["data"] as? [String:Any]
                        {
                            print(dict)
                        }
                        else
                        {
                            self.createAlert(message: msg!)
                        }
                    }
                }
                
            }
        }
    }
    
}
