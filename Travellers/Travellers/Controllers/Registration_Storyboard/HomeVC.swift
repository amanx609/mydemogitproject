//
//  HomeVC.swift
//  Travellers
//
//  Created by apple on 19/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var logout: UIButton!
    var isFromCountry = false
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    

    @IBAction func logoutClick(_ sender: Any) {
        if(isFromCountry == true)
        {
            self.navigationController?.popToRootViewController(animated: true)
        }
        else
        {
        self.presentAlertViewWithOneButton(alertTitle: "Are you sure you want to Logout", alertMessage: "" , btnOneTitle: "OK") { (action) in
            self.navigationController?.popViewController(animated: true)
            
            }
        }
    }
    
}
