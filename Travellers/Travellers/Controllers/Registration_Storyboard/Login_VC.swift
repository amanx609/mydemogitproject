//
//  Login_VC.swift
//  Travellers
//
//  Created by apple on 14/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IQKeyboardManagerSwift
import Alamofire
import KRProgressHUD



class Login_VC: UIViewController {

    @IBOutlet weak var email_text_field: SkyFloatingLabelTextField!
    @IBOutlet weak var password_text_field: SkyFloatingLabelTextField!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var eye_btn: UIButton!
    @IBOutlet weak var footer_View: UIView!
    @IBOutlet weak var eyeBtn: UIButton!
    
    var eyeTap = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    @IBAction func create_Account_click(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUp_VC") as! SignUp_VC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Back_btn_tap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func Forgot_Password_BtnClick(_ sender : UIButton)
    {
        let vc =  UIStoryboard(name: "Registration", bundle: nil).instantiateViewController(withIdentifier: "Forgot_PasswordVC") as! Forgot_PasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func loginBtnTap(_ sender: Any) {
        self.validate()
    }
    
    @IBAction func eyeBtnTap(_ sender: Any) {
    if(eyeTap == true)
    {
        password_text_field.isSecureTextEntry = false
        eyeBtn.setImage(#imageLiteral(resourceName: "hide"), for: .normal)
    }
    else
    {
        eyeBtn.setImage(#imageLiteral(resourceName: "565654"), for: .normal)
        password_text_field.isSecureTextEntry = true
    }
        self.eyeTap = !self.eyeTap
}
    
    
    func validate() -> Bool
    {
        if let email = email_text_field.text , email.isEmpty {
            self.createAlert(message: Constant.string.Empty_email)
            return false
        }
        guard CommonUtilities.shared.isValidEmail(testStr: email_text_field.text!)else {
            self.createAlert(message: Constant.string.validEmail)
            return false
        }
        if let password = password_text_field.text , password.isEmpty{
           self.createAlert(message: Constant.string.enterPassword)
            return false
        }
        self.LoginServiceHit()
      return true
    }
}



extension Login_VC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 120
    }
}
extension Login_VC
{
    func LoginServiceHit()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)])
        KRProgressHUD.show()
        let params : Parameters = [ "username" : email_text_field.text , "password" : password_text_field.text ] as! [String:Any]
        ServiceManager.instance.request(method: .post, URLString: "login", parameters: params, encoding: JSONEncoding.default, headers: nil, isshowing: true) { (sucess, response, error) in
            print(response ?? "")
            KRProgressHUD.dismiss()
            if(sucess)!
            {
                if let statusCode = response?["statusCode"] as? NSNumber
                {
                     let msg = response?["message"] as? String
                    if let dict = response?["data"] as? [String:Any]
                    {
                        if(statusCode == 200)
                        {
                            var userInfoObj = Userinfo()
                            userInfoObj.token = dict["token"] as? String ?? ""
                            if let user = dict["user"] as? [String:Any]
                            {
                                userInfoObj.fullName = user["fullName"] as? String ?? ""
                                userInfoObj.userName = user["userName"] as? String ?? ""
                                userInfoObj.email = user["email"] as? String ?? ""
                                userInfoObj.dob = user["dob"] as? String ?? ""
                                userInfoObj.accountStatus = user["accountStatus"] as? String ?? ""
                                userInfoObj.avatar = user["avatar"] as? String ?? ""
                                userInfoObj.mobileNumber = user["mobileNumber"] as? String ?? ""
                                userInfoObj.deviceToken = user["deviceToken"] as? String ?? ""
                                userInfoObj._id = user["_id"] as? String ?? ""
                                userInfoObj._v = user["_v"] as? String ?? ""
                                userInfoObj.profileImage = user["profileImage"] as? String ?? ""
                                userInfoObj.createdAt = user["createdAt"] as? String ?? ""
                                userInfoObj.updatedAt = user["updatedAt"] as? String ?? ""
                            }
                            let userDefaults = UserDefaults.standard
                            userDefaults.set(userInfoObj.encode(), forKey: "User_info")
                            print(CommonUtilities.shared.getuserdata()?.token)
                            CommonUtilities.shared.sidemenu()
                        }
                        else
                        {
                            self.createAlert(message: msg!)
                        }
                    }
                }
            }
            else
            {
                
            }
            
        }
    }
    
    
}
