//
//  Country_Screen.swift
//  Travellers
//
//  Created by apple on 16/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import Alamofire
import IQKeyboardManagerSwift
import ADCountryPicker
import KRProgressHUD


class Country_Screen: UIViewController {

    @IBOutlet weak var serach_Country_txtfield: UITextField!
    @IBOutlet weak var country_tableView: UITableView!
    @IBOutlet weak var continue_btn: UIButton!
    var selectedCountry : Int?
    var countyArr = ["Australia","Austria","Argentina","Aruba","Amenia"]
    var countryList = [Country]()
    var countrySelect = false
    var selectedCounty = [Int]()
    var countrySelected : ((_ str: String) -> ())?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        self.setCountryApi()
    }
    
}



extension Country_Screen{
    
    func setUI()
    {
        self.continue_btn.layer.cornerRadius = 20
    }

    @IBAction func back_BtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func skip_BtnTap(_ sender: Any) {
       self.pushVC()
    }
    @IBAction func continue_BtnClick(_ sender: Any) {
        self.pushVC()
    }
    
    func pushVC()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Final_PageVC") as! Final_PageVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}

extension Country_Screen : UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Country_List_TableCell", for: indexPath) as! Country_List_TableCell
        
        cell.country_name.text = countryList[indexPath.row].countryName
        cell.country_FlagImg.image = UIImage(named: countryList[indexPath.row].countryFlag)
        print("\u{1F1E9}\u{1F1FF}")
                    
        
        
//        let flagImage = ADCountryPicker()
//        let ifd = flagImage.getFlag(countryCode: "IN")
//        cell.country_FlagImg.image  = ifd
        
        if(countrySelect == true){
            if(selectedCounty.contains(indexPath.row)){
                cell.tick_img.image = #imageLiteral(resourceName: "Path 1161")
            }else{
                cell.tick_img.image = nil
            }
            return cell
        }
        else
        {
            if(selectedCountry == indexPath.row)
            {
                cell.tick_img.image = #imageLiteral(resourceName: "Path 1161")
                return cell
            }
            else
            {
                cell.tick_img.image = nil
                return cell
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
//        return 165
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(countrySelect == true)
        {
           if let index = selectedCounty.index(of: indexPath.row)
                 {
                     self.selectedCounty.remove(at: index)
                     print(index)
                 }
                 else
                 {
                     self.selectedCounty.append(indexPath.row)
                 }
                 country_tableView.reloadData()
        }
        else
        {
            self.selectedCountry = indexPath.row
            self.countrySelected!(countryList[indexPath.row].countryName)
            tableView.reloadData()
            
            dismiss(animated: true, completion: nil)
        }
       
    }
}

extension Country_Screen
{
    func setCountryApi()
    {
        KRProgressHUD.set(activityIndicatorViewColors: [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)])
        KRProgressHUD.show()
        
        ServiceManager.instance.request(method: .get, URLString: "country", parameters: nil, encoding: JSONEncoding.default,  headers: nil, isshowing: true) { (sucess, response, error) in
            KRProgressHUD.dismiss()
            if(sucess)!
            {
                if let statuscode = response?["statusCode"] as? NSNumber
                {
                    if let dict = response?["data"] as? [String:Any]
                    {
                        if statuscode == 200
                        {
                            if let countryList = dict["countryCities"] as? [[String:Any]]
                            {
                                for country in countryList
                                {
                                    var countryListObj = Country()
                                    countryListObj.countryFlag = country["flag"] as? String ?? ""
                                    countryListObj.countryName = country["country"] as? String ?? ""
                                    self.countryList.append(countryListObj)
                                }
                                self.country_tableView.reloadData()
                            }
                        }
                    }
                }
            }
            
        }
        
        
    }
    
    
    func decode(_ s: String) -> String?
        {
            let data = s.data(using: .utf8)!
            return String(data: data, encoding: .nonLossyASCII)
        }
}
