//
//  Edit_ProfileVC.swift
//  Travellers
//
//  Created by apple on 03/11/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class Edit_ProfileVC: UIViewController {
   
    
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var profile_imgView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var continueButton: UIButton!
    
    var cellArr = ["Edit Private Details","Change Triprate Type","Change Country","Change Password"]
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()

    }
    
    func setUI()
    {
        self.continueButton.layer.cornerRadius = 20
        self.profileImage.layer.cornerRadius = self.profileImage.bounds.height / 2
        self.profileImage.clipsToBounds = true
        self.cameraView.layer.cornerRadius =    self.cameraView.bounds.height / 2
        self.cameraView.layer.borderWidth = 1
        self.cameraView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

        
    }

}

extension Edit_ProfileVC :  UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "Edit_ProfileTVCell") as? Edit_ProfileTVCell
        cell?.lbl.text = cellArr[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        450
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let vc = self.storyboard?.instantiateViewController(identifier: "Edit_ProfileDetailVC") as! Edit_ProfileDetailVC
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            createAlert(message: Constant.string.underdevlopment)
        }
    }
    
}

extension Edit_ProfileVC
{
    @IBAction func openCamera(_ sender : UIButton)
    {
        
    }
    
    @IBAction func backButtonTap(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
}
