//
//  Edit_ProfileDetailVC.swift
//  Travellers
//
//  Created by apple on 03/11/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import TweeTextField

class Edit_ProfileDetailVC: UIViewController {

    @IBOutlet weak var emailAddressTextField: TweeActiveTextField!
    @IBOutlet weak var mobileNumberTextField: TweeActiveTextField!
    @IBOutlet weak var saveChangesBtn: UIButton!
    @IBOutlet weak var emailAddressBtn: UIButton!
    @IBOutlet weak var passwordBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.saveChangesBtn.layer.cornerRadius = 20
    }
    

    @IBAction func backBtnTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
