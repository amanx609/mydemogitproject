//
//  ViewController.swift
//  Travellers
//
//  Created by apple on 14/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import TTGTagCollectionView

class ViewController: UIViewController, TTGTextTagCollectionViewDelegate {

    
    
    @IBOutlet weak var mainView: UIView!
    let collectionView = TTGTextTagCollectionView()
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.alignment = .fillByExpandingWidth
        collectionView.delegate = self
        collectionView.scrollDirection = .horizontal
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.numberOfLines = 5
        mainView.addSubview(collectionView)
        
        let config = TTGTextTagConfig()
        config.backgroundColor = .white
        config.textColor = UIColor(named: "app_color")
        config.borderColor = .black
        config.borderWidth = 1
        collectionView.addTags(["AMMAMAMAMA" , "aaaaa", "sgdghfkhbsjhzsdjasdjb","AMMAMAMAMA" , "aaaaa", "sgdghfkhbsjhzsdjasdjb","AMMAMAMAMA" , "aaaaa", "sgdghfkhbsjhzsdjasdjb","AMMAMAMAMA" , "aaaaa", "sgdghfkhbsjhzsdjasdjb","AMMAMAMAMA" , "aaaaa", "sgdghfkhbsjhzsdjasdjb","AMMAMAMAMA" , "aaaaa", "sgdghfkhbsjhzsdjasdjb","AMMAMAMAMA" , "aaaaa", "sgdghfkhbsjhzsdjasdjb","AMMAMAMAMA" , "aaaaa", "sgdghfkhbsjhzsdjasdjb"], with: config)
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = CGRect(x: 0, y: 70, width: mainView.frame.width, height: 200)
    }


}

