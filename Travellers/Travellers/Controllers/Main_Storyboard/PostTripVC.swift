//
//  PostTripVC.swift
//  Travellers
//
//  Created by apple on 22/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import YangMingShan
import OpalImagePicker
import ACFloatingTextfield_Objc



class PostTripVC: UIViewController , UITextViewDelegate  {
  

    @IBOutlet var headerView: UIView!
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var add_Trip_pic: UIButton!
    @IBOutlet weak var upload_Photo_collectionView: UICollectionView!
    @IBOutlet weak var addTripBtn: UIButton!
    @IBOutlet weak var tripTypeButtonView: UIView!
    @IBOutlet weak var postTrip: UIButton!
    @IBOutlet weak var planTrip: UIButton!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var addYourTripLbl: UILabel!
    @IBOutlet weak var tripFriendsCollectionView: UICollectionView!
    
    @IBOutlet weak var followerButton: UIButton!
    @IBOutlet weak var publicButton: UIButton!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var totalCount: UILabel!
    @IBOutlet weak var yearOfTravelTextField: ACFloatingTextField!
    @IBOutlet weak var monthOfTravel: ACFloatingTextField!
    @IBOutlet weak var profile_image: UIImageView!
    @IBOutlet weak var daysOfTravel: ACFloatingTextField!
    
    
    
    var documentsPicArray = [UIImage]()
    var tripMate = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
         setUI()
    }
    
    
    func setUI()
    { tblView.delegate = self
        tblView.dataSource = self
        addTripBtn.isHidden = true
        tripTypeButtonView.layer.cornerRadius = 16
        tripTypeButtonView.layer.borderWidth = 1
        tripTypeButtonView.layer.borderColor = UIColor.white.cgColor
        postTrip.layer.cornerRadius = 10
        planTrip.layer.cornerRadius = 10
        publicButton.layer.cornerRadius = 16
        followerButton.layer.cornerRadius = 16
        postButton.layer.cornerRadius = 10
        descriptionTextView.delegate = self
        countLabel.text = "100"
        profile_image.layer.cornerRadius = profile_image.bounds.height / 2
        monthOfTravel.setDatePickerWithDateFormate(dateFormate: "MMMM", defaultDate: nil
        , isPrefilledDate: false) { (date) in
            
        }
        
//        yearOfTravelTextField.setDatePickerWithDateFormate(dateFormate: "yyyy", defaultDate: nil, isPrefilledDate: true) { (date) in
//        }
        
        
}
    
    
    func textViewDidChange(_ textView: UITextView) {
         print(descriptionTextView.text!)
        countLabel.text = "\(100 - textView.text.count)"
    }

         func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    return textView.text.count + (text.count - range.length) <= 100
    }
    
}


//MARK:- TableView
extension PostTripVC: UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return 3
        case 2:
             return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Post_TripCell", for: indexPath) as? Post_TripCell
            return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityCell", for: indexPath) as? ActivityCell
            return cell!
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 900
        default:
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderViewCell") as? HeaderViewCell
        switch section {
        case 0:
            return headerView
        case 1:
            headerCell?.Headerlabel.text = "Select countries and cities visited"
            return headerCell
        default:
             headerCell?.Headerlabel.text = "Your Activities"
            return headerCell
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch section {
        case 0,1:
            return nil
        default:
            return footerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 0 , 1:
            return 0
        default:
            return 340

        }
    }
    
}


//MARK:- CollectionView
extension PostTripVC : UICollectionViewDataSource , UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == upload_Photo_collectionView)
        {
            switch documentsPicArray.count {
            case 0:
                return  1
            default:
                return documentsPicArray.count
            }
        }
        else
        {
            switch tripMate.count {
            case 0:
                return  1
            default:
                return tripMate.count + 1
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == upload_Photo_collectionView)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadImageCVC", for: indexPath) as! UploadImageCVC
            switch documentsPicArray.count {
            case 0:
                cell.mainView.isHidden = false
                cell.mainView.layer.cornerRadius = 10
                cell.mainView.layer.borderWidth = 1
            cell.mainView.layer.borderColor = UIColor.white.cgColor
                return cell
            default:
                cell.mainView.isHidden = true
                cell.imageView.isHidden = false
                cell.imageView.layer.cornerRadius = 10
                cell.imageView.clipsToBounds = true
                cell.imageView.layer.borderColor = UIColor.white.cgColor
                cell.mainImage.image = documentsPicArray[indexPath.row]
//                cell.crossBtn.addTarget(self, action: #selector(deleteImage(_:_:)), for: .touchUpInside)
                cell.crossBtn.addTarget(self, action: #selector(deleteImageTop(_:_:)), for: .touchUpInside)
                self.addTripBtn.isHidden = false
                return cell
            }
        }
        else
        {
            switch tripMate.count {
            case 0:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddMoreMate", for: indexPath) as! AddMoreMate
                cell.mainView.addDashBorder(color: #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1))
                cell.mainView.makeRounded()
                return cell
            default:
                if indexPath.row == tripMate.count
                {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddMoreMate", for: indexPath) as! AddMoreMate
                    cell.mainView.addDashBorder(color: #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1))
                    cell.mainView.makeRounded()
                    return cell
                }
                else
                {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TripMateCVC", for: indexPath) as! TripMateCVC
                    cell.friendsImage.layer.cornerRadius = cell.friendsImage.bounds.height / 2
                    cell.friendsImage.image = tripMate[indexPath.row]
                    cell.crossImage.addTarget(self, action: #selector(deleteImageBottom(_:_:)), for: .touchUpInside)
//                     cell.crossImage.addTarget(self, action: #selector(deleteImage(_:_:)), for: .touchUpInside)
                    return cell
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == upload_Photo_collectionView)
        {
        return CGSize(width: collectionView.frame.width - 20, height: collectionView.frame.height - 10)
        }
        else
        {
            return CGSize(width: 80, height: 80)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == upload_Photo_collectionView)
        {
            chooseImage(collectionview: upload_Photo_collectionView)
        }
        else
        {
            chooseImage(collectionview: tripFriendsCollectionView)
        }
    }
    
    @objc func deleteImageTop(_ sender: UIButton , _ collectionView : UICollectionView)
    {
        if  let cell = sender.superview?.superview?.superview?.superview as? UploadImageCVC
            {
                if let indexpath = upload_Photo_collectionView.indexPath(for: cell )
                {
                    documentsPicArray.remove(at: indexpath.row)
                    upload_Photo_collectionView.reloadData()
                  
                }
            }
    }
    @objc func deleteImageBottom(_ sender: UIButton , _ collectionView : UICollectionView)
    {
        if  let cell = sender.superview?.superview?.superview?.superview as? TripMateCVC
            {
                if let indexpath = tripFriendsCollectionView.indexPath(for: cell )
                {
                    tripMate.remove(at: indexpath.row)
                    tripFriendsCollectionView.reloadData()
                  
                }
            }
    }
//    @objc func deleteImage(_ sender: UIButton , _ collectionView : UICollectionView)
//    {
//        if collectionView.tag == 2
//        {
//            let deletingIndx = sender.convert(CGPoint.zero, to: tripFriendsCollectionView)
//            guard let indexPath = tripFriendsCollectionView.indexPathForItem(at: deletingIndx) else
//            { return }
//            tripMate.remove(at: indexPath.item)
//            tripFriendsCollectionView.reloadData()
//        }else
//        {
//           let deletingIndx = sender.convert(CGPoint.zero, to: upload_Photo_collectionView)
//            guard let indexPath = upload_Photo_collectionView.indexPathForItem(at: deletingIndx) else
//            { return }
//            documentsPicArray.remove(at: indexPath.item)
//            upload_Photo_collectionView.reloadData()
//        }
//
//
//    }
}

//MARK:- Image Picker
extension PostTripVC : OpalImagePickerControllerDelegate
{
    func chooseImage(collectionview:UICollectionView)
    {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        let actionSheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                imagePickerController.sourceType = .camera
                self.viewController?.present(imagePickerController, animated: true, completion: nil)
            }
            else
            {
                let alertController = UIAlertController(title: Constant.string.appname, message:
                    "Camera not avilable" , preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: Constant.string.ok, style: .default)); self.viewController?.present(alertController, animated: true, completion: nil)
            }
        }))
        if(collectionview == upload_Photo_collectionView)
        {
            actionSheet.addAction(UIAlertAction(title:"Photo Album", style: .default, handler: {(action:UIAlertAction)in
                let imagePicker = OpalImagePickerController()
                let configuration = OpalImagePickerConfiguration()
                configuration.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select more than 5 images!", comment: "")
                imagePicker.configuration = configuration
                imagePicker.imagePickerDelegate = self
                imagePicker.maximumSelectionsAllowed = 5 - self.documentsPicArray.count
                imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
                imagePicker.modalPresentationStyle = .fullScreen
                self.viewController?.presentOpalImagePickerController(imagePicker, animated: true,
                                                                      select: { (assets) in
                                                                        //Select Assets
                                                                        print("This is assests\(assets)")
                                                                        self.viewController?.dismiss(animated: true, completion: nil)
                                                                        self.documentsPicArray.append(contentsOf: self.getAssetThumbnail(assets: assets))
                                                                        self.upload_Photo_collectionView.reloadData()
                }, cancel: {
                })
            }))
        }
        else
        {
            actionSheet.addAction(UIAlertAction(title:"Photo Album", style: .default, handler: {(action:UIAlertAction)in
                let imagePicker = OpalImagePickerController()
                let configuration = OpalImagePickerConfiguration()
                configuration.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select more than 7 images!", comment: "")
                imagePicker.configuration = configuration
                imagePicker.imagePickerDelegate = self
                imagePicker.maximumSelectionsAllowed = 7 - self.tripMate.count
                imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
                imagePicker.modalPresentationStyle = .fullScreen
                self.viewController?.presentOpalImagePickerController(imagePicker, animated: true,
                                                                      select: { (assets) in
                                                                        //Select Assets
                                                                        print("This is assests\(assets)")
                                                                        self.viewController?.dismiss(animated: true, completion: nil)
                                                                        self.tripMate.append(contentsOf: self.getAssetThumbnail(assets: assets))
                                                                        self.tripFriendsCollectionView.reloadData()
                }, cancel: {
                })
            }))
        }
        actionSheet.addAction(UIAlertAction(title: Constant.string.cancel, style: .cancel, handler: nil))
        self.viewController?.present(actionSheet,animated: true,completion: nil)
    }
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        if(upload_Photo_collectionView != nil)
        {
            documentsPicArray.append(image)
            self.upload_Photo_collectionView.reloadData()
            picker.dismiss(animated: true, completion: nil)
        }else
        {
            tripMate.append(image)
            self.tripFriendsCollectionView.reloadData()
            picker.dismiss(animated: true, completion: nil)
        }
       
        
    }
    override func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    func getAssetThumbnail(assets: [PHAsset]) -> [UIImage] {
        var arrayOfImages = [UIImage]()
        for asset in assets {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.version = .original
            option.isSynchronous = true
            manager.requestImageData(for: asset, options: option) { data, _, _, _ in
                if let data = data {
                    arrayOfImages.append(UIImage(data: data)!)
                }
            }
        }
        return arrayOfImages
    }
}


