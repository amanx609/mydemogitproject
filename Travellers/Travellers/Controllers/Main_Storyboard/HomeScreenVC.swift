//
//  HomeScreenVC.swift
//  Travellers
//
//  Created by apple on 21/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class HomeScreenVC: UIViewController , UITextFieldDelegate {

    @IBOutlet weak var profile_img: UIImageView!
    @IBOutlet weak var address_lbl: UILabel!
    @IBOutlet weak var name_lbl: UILabel!
    @IBOutlet weak var chat_btn: UIButton!
    @IBOutlet weak var notification_btn: UIButton!
    @IBOutlet weak var serach_textField: UITextField!
    @IBOutlet weak var search_View: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serach_textField.delegate = self
        serach_textField.addTarget(self, action: #selector(myTargetFunction), for: .touchDown)
        self.SetUi()

    }
      
    @IBAction func sideMenuBtnTap(_ sender: Any) {
        self.slideMenuController()?.toggleLeft()
    }
    
    @IBAction func notification_btnClick(_ sender: Any) {
        self.createAlert(message: Constant.string.underdevlopment)
    }
    
    @IBAction func chat_btnClick(_ sender: Any) {
        self.createAlert(message: Constant.string.underdevlopment)
    }

    func SetUi()
    {
        self.profile_img.layer.cornerRadius = profile_img.bounds.height / 2
        self.search_View.layer.cornerRadius = 11
        
    }

    @objc func myTargetFunction(textField: UITextField) {
        let vc = self.storyboard?.instantiateViewController(identifier: "SearchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
