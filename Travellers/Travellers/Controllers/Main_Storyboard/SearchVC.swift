//
//  SearchVC.swift
//  Travellers
//
//  Created by apple on 03/11/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {

    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var crossBtn: UIButton!
    @IBOutlet weak var peopleBtn: UIButton!
    @IBOutlet weak var peoplebtnView: UIView!
    @IBOutlet weak var placesBtn: UIButton!
    @IBOutlet weak var placesBtnView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var crossBtnImg: UIImageView!
    
    var placeArr = ["San Francisco" ,"United States","San Francisco" ,"United States","San Francisco" ,"United States"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.estimatedRowHeight = 100

    }
    



}

extension SearchVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchPlacesCell", for: indexPath) as! SearchPlacesCell
        cell.mainImage.image = #imageLiteral(resourceName: "Image 12")
        cell.cityBoldLabel.text = placeArr[indexPath.row]
        cell.countryLbl.text = placeArr[indexPath.row]
        cell.locatiionIcon.image = UIImage(named: "location_icon")
        cell.img1.image = #imageLiteral(resourceName: "Image 12")
        cell.img2.image = #imageLiteral(resourceName: "Image 12")
        cell.img3.image = #imageLiteral(resourceName: "Image 12")
        cell.img4.image = #imageLiteral(resourceName: "Image 12")
        cell.img5.image = #imageLiteral(resourceName: "Image 12")



        return cell
        
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchPeopleCell", for: indexPath) as! SearchPeopleCell
//        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
   
}
extension SearchVC
{
    @IBAction func backBtnTap(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if(searchTextField.text == "")
        {
            self.crossBtnImg.isHidden = true
            self.crossBtn.isEnabled = false
        }
        else
        {
            self.crossBtnImg.isHidden = false
            self.crossBtn.isEnabled = true
        }
    }
    
    
}
