//
//  SideMenuVC.swift
//  Travellers
//
//  Created by apple on 21/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class SideMenuVC: UIViewController {

    @IBOutlet weak var topMainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var side_profile_img: UIImageView!
    @IBOutlet weak var sideProfileBtn: UIButton!
    
    var sideMenuArray = [["image":"Create_a_poll",
    "text":Constant.string.Create_a_poll],
   ["image":"Create_an_itinerary",
    "text":Constant.string.Create_an_itinerary],
   ["image":"Settings",
   "text":Constant.string.Settings],
    ["image":"about",
    "text":Constant.string.about],
    ["image":"logout",
    "text":Constant.string.logout]]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUi()

    }
    
    
    
    @IBAction func sideProfileBtn(_ sender: Any) {
        let vc : ProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func logout(){
        let storyboard = UIStoryboard(name: "Registration", bundle: nil)
        guard let rootVC = storyboard.instantiateViewController(withIdentifier: "Login_VC") as? Login_VC else {
            print("ViewController not found")
            return
        }
        UserDefaults.standard.removeObject(forKey: "User_info")
        let rootNC = UINavigationController(rootViewController: rootVC)
        UIApplication.shared.windows.first?.rootViewController = rootNC
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        rootNC.navigationBar.isHidden = true
    }

}
extension SideMenuVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sideMenuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
        cell.sideImg.image = UIImage(named: sideMenuArray[indexPath.row]["image"]!)
        cell.sideLbl.text = sideMenuArray[indexPath.row]["text"]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 4)
        {
            slideMenuController()?.closeLeft()
            self.presentAlertViewWithTwoButtons(alertTitle: "Are you sure you want to logout", alertMessage: "", btnOneTitle: "Yes", btnOneTapped: { (action) in
                self.logout()
            }, btnTwoTitle: "No") { (UIAlertAction) in
                self.slideMenuController()?.openLeft()
            }
            //CommonUtilities.shared.setLoginScreen()
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                self.logout()
//            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
}
extension SideMenuVC{
    func setUpUi()
    {
        self.side_profile_img.layer.cornerRadius = side_profile_img.bounds.height / 2
    }
}
