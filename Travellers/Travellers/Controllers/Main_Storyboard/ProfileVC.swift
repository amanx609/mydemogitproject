//
//  ProfileVC.swift
//  Travellers
//
//  Created by apple on 02/11/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileMainView: UIView!
    @IBOutlet weak var myJourneyBtn: UIButton!
    @IBOutlet weak var editProfileBtn: UIButton!
    @IBOutlet weak var dropDownBtn: UIButton!
    @IBOutlet weak var profileNameLbl: UILabel!
    @IBOutlet weak var profileMainImg: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var countryFlagImg: UIImageView!
    @IBOutlet weak var countryNameLbl: UILabel!
    @IBOutlet weak var profileDetailsView: UIView!
    @IBOutlet weak var numberOfFollowers: UILabel!
    @IBOutlet weak var citiesVisited: UILabel!
    @IBOutlet weak var countryVisited: UILabel!
    @IBOutlet weak var noOfFollowing: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var aboutUsView: UIView!
    @IBOutlet weak var aboutUsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var aboutMeTextLbl: UILabel!
    @IBOutlet weak var activityCollectionView: UICollectionView!
    var dropDownClick = false
    var CollArr = ["Trips","Pictures","Blogs","Planned Trip","Itinerary"]
    var venuArr = ["Adventure trips", "Solo trips" , "Guided trips" , "Backpacking trips" , "Self drive trips" , "Comfortable & Relax trips" , "Photography trips" , "Natural landscapes trips" , "Art trips hiking" , "Wilderness trips","Adventure sports trips","Shopping trips"]
    var whiteIcon = ["Adventure tripsw", "Solo tripsw" , "Guided tripsw" , "Backpacking tripsw" , "Self drive tripsw" , "Comfortable & Relax tripsw" , "Photography tripsw" , "Natural landscapes tripsw" , "Art trips hikingw" , "Wilderness tripsw","Adventure sports tripsw","Shopping tripsw"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUi()
    }
    
    
    @IBAction func backBtnTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setUi()
    {
        self.profileMainImg.layer.cornerRadius = self.profileMainImg.bounds.height / 2
        self.myJourneyBtn.layer.cornerRadius = 10
        self.editProfileBtn.layer.cornerRadius = 10
        self.dropDownBtn.layer.cornerRadius = 5
        self.aboutUsView.isHidden = true
        self.aboutUsHeightConstraint.constant = 0
    }
    
}

extension ProfileVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CollArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProfileTableCell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableCell", for: indexPath) as! ProfileTableCell
        switch indexPath.row {
        case 0 , 3 :
            cell.mainImage.image = #imageLiteral(resourceName: "profile_big")
        case 1 , 2 , 4:
            cell.mainImage.image = #imageLiteral(resourceName: "profile_small")
        default:
            cell.mainImage.image = nil
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(dropDownClick == true)
        {
            return 650

        }
        else
        {
            return 450

        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 410
    }
}

extension ProfileVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case collectionView:
            return CollArr.count
        default:
            return venuArr.count
        }
//        return CollArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == self.collectionView)
        {
            let cell : headerCVCell = collectionView.dequeueReusableCell(withReuseIdentifier: "headerCVCell", for: indexPath) as! headerCVCell
            cell.typeOfActivityLbl.text = CollArr[indexPath.row]
            return cell
        }
        else
        {
            let cell : AboutUsCVCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutUsCVCell", for: indexPath) as! AboutUsCVCell
            cell.view.layer.cornerRadius = 19
            cell.activityNameLbl.text = venuArr[indexPath.row]
            cell.activityImage.image = UIImage(named: whiteIcon[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == self.collectionView)
        {
            let text = self.CollArr[indexPath.row]
       let width = self.estimatedFrame(text: text, font: UIFont(name: "SFProDisplay-Regular", size: 26.0)!).width
            print(width)
        return CGSize(width: width + 25.0, height: 50.0)
        }else
        {
            let text = self.venuArr[indexPath.row]
       let width = self.estimatedFrame(text: text, font: UIFont(name: "SFProDisplay-Regular", size: 14.0)!).width
            print(width)
        return CGSize(width: width + 5.0, height: 45.0)
        }
          
    }
    
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
        let size = CGSize(width: 150, height: 1000) // temporary size
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size,
                                                   options: options,
                                                   attributes: [NSAttributedString.Key.font: font],
                                                   context: nil)
    }
    
   
}

extension ProfileVC
{
    
    @IBAction func DropDownBtn(_ sender: UIButton)
    {
        dropDownClick = !dropDownClick
        if(dropDownClick == false)
        {
            aboutUsView.isHidden = true
            aboutUsHeightConstraint.constant = 0
        }
        else
        {
            aboutUsView.isHidden = false
            aboutUsHeightConstraint.constant = 185
        }
        tableView.reloadData()
    }
    
    @IBAction func editProfileBtnTap(_ sender: UIButton)
    {
        let vc : Edit_ProfileVC = self.storyboard?.instantiateViewController(identifier: "Edit_ProfileVC") as! Edit_ProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
