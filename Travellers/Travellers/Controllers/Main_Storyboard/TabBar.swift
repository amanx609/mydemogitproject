//
//  TabBar.swift
//  Travellers
//
//  Created by apple on 20/10/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class TabBar: UITabBarController , UITabBarControllerDelegate {

    var Homebtn : UITabBarItem!
    var CreateTrip : UITabBarItem!
    var CreateBlog : UITabBarItem!
    var CheckIn : UITabBarItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        Homebtn = UITabBarItem()
        CreateTrip = UITabBarItem()
        CreateBlog = UITabBarItem()
        CheckIn = UITabBarItem()
        setUpTabBarElements()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC1 = storyboard.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
        let VC2 = storyboard.instantiateViewController(withIdentifier: "UnderDevelopmentVC") as! UnderDevelopmentVC
        let VC3 = storyboard.instantiateViewController(withIdentifier: "UnderDevelopmentVC") as! UnderDevelopmentVC
        let VC4 = storyboard.instantiateViewController(withIdentifier: "PostTripVC") as! PostTripVC

        VC1.tabBarItem = Homebtn
        VC2.tabBarItem = CreateBlog
        VC3.tabBarItem = CreateTrip
        VC4.tabBarItem = CheckIn
        
        self.viewControllers = [VC1 , VC2 , VC3 , VC4]
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 1)], for: .selected)
        self.Homebtn.title = "Home"
        self.CreateTrip.title = "Create Trip"
        self.CreateBlog.title = "Create Blog"
        self.CheckIn.title = "Check in"
    }
    
    
    
    
    override func viewWillLayoutSubviews()
    {
        var newTabBarFrame = tabBar.frame
        let newTabBarHeight: CGFloat = 75
        newTabBarFrame.size.height = newTabBarHeight
        newTabBarFrame.origin.y = self.view.frame.size.height - newTabBarHeight
        tabBar.bottomAnchor.constraint(equalToSystemSpacingBelow: view.bottomAnchor, multiplier:0)
        
        tabBar.barTintColor =  UIColor(red: 14/255, green: 19/255, blue: 43/255, alpha: 1)
        tabBar.layer.backgroundColor = UIColor(red: 14/255, green: 19/255, blue: 43/255, alpha: 1).cgColor
//        tabBar.layer.shadowOffset = CGSize(width: 0, height: 1)
//        tabBar.layer.shadowOpacity = 1.0
//        tabBar.layer.shadowRadius = 4.0
        tabBar.layer.cornerRadius = 10
        tabBar.layer.maskedCorners = [.layerMaxXMinYCorner , .layerMinXMinYCorner]
        tabBar.frame = newTabBarFrame
        
    }
    
    
    
    func setUpTabBarElements()
     {
         self.Homebtn.image = #imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal)
         self.CreateTrip.image = #imageLiteral(resourceName: "create_trip").withRenderingMode(.alwaysOriginal)
         self.CreateBlog.image = #imageLiteral(resourceName: "create_blog").withRenderingMode(.alwaysOriginal)
         self.CheckIn.image = #imageLiteral(resourceName: "check_in").withRenderingMode(.alwaysOriginal)
         self.Homebtn.selectedImage = #imageLiteral(resourceName: "home_selected").withRenderingMode(.alwaysOriginal)
         self.CreateTrip.selectedImage = #imageLiteral(resourceName: "create_trip_selected").withRenderingMode(.alwaysOriginal)
         self.CreateBlog.selectedImage = #imageLiteral(resourceName: "create_blog_selected").withRenderingMode(.alwaysOriginal)
         self.CheckIn.selectedImage = #imageLiteral(resourceName: "check_in_selected").withRenderingMode(.alwaysOriginal)
    }


}
