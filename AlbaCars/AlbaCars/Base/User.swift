//
//  User.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation


class User: BaseCodable {
  
  //Variables
  var email: String?
  var name: String?
  var countryCode: String?
  var country: String?
  var city: String?
  var isServiceSelected: Bool?
  var mobile: String?
  var dob: String?
  var id: Int?
  var isMobileVerified: Bool?
  var userType: Int?
  var emailVerify: Bool?
  var googleId: String?
  var facebookId: String?
  var image: String?
  var selectedServiceList: [Int]?
  var location: String?
  var mobileWithCode: String?
  var rating: Double?
  var noOfCount: Int?

    private enum CodingKeys: String, CodingKey {
    case email, name, countryCode, country, city, isServiceSelected, mobile, dob, id, isMobileVerified, userType, emailVerify, googleId, facebookId, image, selectedServiceList, location, mobileWithCode,rating,noOfCount
  }

  required init(from decoder: Decoder) throws {
    let values = try! decoder.container(keyedBy: CodingKeys.self)
    self.email = try values.decodeIfPresent(String.self, forKey: .email)
    self.name = try values.decodeIfPresent(String.self, forKey: .name)
    self.countryCode = try values.decodeIfPresent(String.self, forKey: .countryCode)
    self.country = try values.decodeIfPresent(String.self, forKey: .country)
    self.city = try values.decodeIfPresent(String.self, forKey: .city)
    self.isServiceSelected = try values.decodeIfPresent(Bool.self, forKey: .isServiceSelected)
    self.mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
    self.dob = try values.decodeIfPresent(String.self, forKey: .dob)
    self.id = try values.decodeIfPresent(Int.self, forKey: .id)
    self.isMobileVerified = try values.decodeIfPresent(Bool.self, forKey: .isMobileVerified)
    self.userType = try values.decodeIfPresent(Int.self, forKey: .userType)
    self.emailVerify = try values.decodeIfPresent(Bool.self, forKey: .emailVerify)
    self.googleId = try values.decodeIfPresent(String.self, forKey: .googleId)
    self.facebookId = try values.decodeIfPresent(String.self, forKey: .facebookId)
    self.image = try values.decodeIfPresent(String.self, forKey: .image)
    self.selectedServiceList = try values.decodeIfPresent([Int].self, forKey: .selectedServiceList)
    self.rating = try values.decodeIfPresent(Double.self, forKey: .rating)
    self.noOfCount = try values.decodeIfPresent(Int.self, forKey: .noOfCount)

    if let userCity = self.city {
      self.location = userCity
    }
    
    if let userCountry = self.country {
      if self.location == nil {
        self.location = userCountry
      } else {
        self.location = self.location! + ", \(userCountry)"
      }
    }
    
    if let countryCode = self.countryCode, let mobile = self.mobile {
      self.mobileWithCode = countryCode + "| " + mobile
    }
  }
    
    func userRating()-> Double {
        return Helper.toDouble(self.rating)
    }
    
    func numberOfRated()-> String {
        
        if let count = self.noOfCount, count > 0  {
            return "(\(Helper.toString(object: count)))"
        }
        return ""
    }
}
