//
//  DIConfigurator.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

enum StoryboardType: String {
    
    case LaunchScreen
    case LoginSignup
    case CustomerMain
    case CustomerHome
    case DealerMain
    case Main
    case SupplierMain
    case Account
    case CustomerCar
    case DealerAuction
    case DealerFleetBiding
    case CustomerRecovery
    case SupplierRecovery
    case SupplierTenders
    case SmartTenders
    
    var storyboardName: String {
        return rawValue
    }
}


class DIConfigurator: NSObject {
    
    // MARK: - Singleton Instantiation
    private static let _sharedInstance: DIConfigurator = DIConfigurator()
    static var sharedInstance: DIConfigurator {
        return _sharedInstance
    }
    
    //MARK: - Initialization Method
    private override init() {
        
    }
    
    func getViewControler(storyBoard: StoryboardType, indentifier: String) -> UIViewController {
        let storyB = UIStoryboard(name: storyBoard.rawValue, bundle: nil)
        return storyB.instantiateViewController(withIdentifier: indentifier)
    }
    
    
    func getLoginTypeVC() -> LoginTypeViewC {
        if let loginTypeVC = self.getViewControler(storyBoard: .LoginSignup, indentifier: LoginTypeViewC.className) as? LoginTypeViewC {
            return loginTypeVC
        } else {
            fatalError("Not able to initialize LoginTypeViewC")
        }
    }
    
    func getSignupVC() -> SignupViewC {
        if let signupVC = self.getViewControler(storyBoard: .LoginSignup, indentifier: SignupViewC.className) as? SignupViewC {
            signupVC.viewModel = SignupViewM()
            return signupVC
        } else {
            fatalError("Not able to initialize SignupViewC")
        }
    }
    
    func getLoginVC() -> LoginViewC {
        if let loginVC = self.getViewControler(storyBoard: .LoginSignup, indentifier: LoginViewC.className) as? LoginViewC {
            loginVC.viewModel = LoginViewM()
            return loginVC
        } else {
            fatalError("Not able to initialize LoginViewC")
        }
    }
    
    func getCreateAccountVC() -> CreateAccountViewC {
        if let createAccountVC = self.getViewControler(storyBoard: .LoginSignup, indentifier: CreateAccountViewC.className) as? CreateAccountViewC {
            createAccountVC.viewModel = CreateAccountViewM()
            return createAccountVC
        } else {
            fatalError("Not able to initialize CreateAccountViewC")
        }
    }
    
    func getForgotPasswordVC() -> ForgotPasswordViewC {
        if let forgotPasswordVC = self.getViewControler(storyBoard: .LoginSignup, indentifier: ForgotPasswordViewC.className) as? ForgotPasswordViewC {
            forgotPasswordVC.viewModel = ForgotPasswordViewM()
            return forgotPasswordVC
        } else {
            fatalError("Not able to initialize ForgotPasswordViewC")
        }
    }
    
    func getChangePasswordVC() -> ChangePasswordViewC {
        if let changePasswordVC = self.getViewControler(storyBoard: .LoginSignup, indentifier: ChangePasswordViewC.className) as? ChangePasswordViewC {
            //forgotPasswordVC.viewModel = ChangePasswordViewC()
            return changePasswordVC
        } else {
            fatalError("Not able to initialize ChangePasswordViewC")
        }
    }
    
    func getOTPVerificationVC() -> OTPVerificationViewC {
        if let otpVerifyVC = self.getViewControler(storyBoard: .LoginSignup, indentifier: OTPVerificationViewC.className) as? OTPVerificationViewC {
            otpVerifyVC.viewModel = OTPVerificationViewM()
            return otpVerifyVC
        } else {
            fatalError("Not able to initialize SignupViewC")
        }
    }
    
    
    func getBuyCarsVC() -> BuyCarsViewC {
        if let buyCarsVC = self.getViewControler(storyBoard: .CustomerHome, indentifier: BuyCarsViewC.className) as? BuyCarsViewC {
            return buyCarsVC
        } else {
            fatalError("Not able to initialize BuyCarsViewC")
        }
    }
    
    func getFilterCarsVC() -> FilterCarsViewC {
        if let filterCarsVC = self.getViewControler(storyBoard: .CustomerHome, indentifier: FilterCarsViewC.className) as? FilterCarsViewC {
            filterCarsVC.viewModel = FilterCarsViewM()
            return filterCarsVC
        } else {
            fatalError("Not able to initialize FilterCarsViewC")
        }
    }
    
    func getAddNewCarViewC() -> CAddNewCarViewC {
        if let addCarViewC = self.getViewControler(storyBoard: .DealerMain, indentifier: CAddNewCarViewC.className) as? CAddNewCarViewC {
            
            return addCarViewC
        } else {
            fatalError("Not able to initialize CAddNewCarViewC")
        }
    }
    
    func getMyCarDetailsViewC() -> MyCarDetailsViewC {
        
        if let carDetailsViewC = self.getViewControler(storyBoard: .DealerMain, indentifier: MyCarDetailsViewC.className) as? MyCarDetailsViewC {
            return carDetailsViewC
        } else {
            fatalError("Not able to initialize CAddNewCarViewC")
        }
    }
    
    func getSideMenuVC() -> SideMenuViewC {
        if let sideMenuVC = self.getViewControler(storyBoard: .Main, indentifier: SideMenuViewC.className) as? SideMenuViewC {
            return sideMenuVC
        } else {
            fatalError("Not able to initialize SideMenuViewC")
        }
    }
    
    func getMainViewController() -> MainViewController {
        if let mainVC = self.getViewControler(storyBoard: .Main, indentifier: MainViewController.className) as? MainViewController {
            return mainVC
        } else {
            fatalError("Not able to initialize MainViewController")
        }
    }
    
    func getHomeVC() -> HomeViewC {
        if let homeVC = self.getViewControler(storyBoard: .Main, indentifier: HomeViewC.className) as? HomeViewC {
            return homeVC
        } else {
            fatalError("Not able to initialize HomeViewC")
        }
    }
    
    // MARK: - Settings View Controller
    func getSettingsVC() -> SettingsViewC {
        if let settingsVC = self.getViewControler(storyBoard: .DealerMain, indentifier: SettingsViewC.className) as? SettingsViewC {
            return settingsVC
        } else {
            fatalError("Not able to initialize SettingsViewC")
        }
    }
    
    // MARK: - Notification Settings View Controller
    func getNotificationSettingsVC() -> NotificationSettingsViewC {
        if let notificationSettingsVC = self.getViewControler(storyBoard: .DealerMain, indentifier: NotificationSettingsViewC.className) as? NotificationSettingsViewC {
            return notificationSettingsVC
        } else {
            fatalError("Not able to initialize NotificationSettingsViewC")
        }
    }
    
    func getMyCarsVC() -> CMyCarsViewC {
        if let myCarsVC = self.getViewControler(storyBoard: .Main, indentifier: CMyCarsViewC.className) as? CMyCarsViewC {
            return myCarsVC
        } else {
            fatalError("Not able to initialize CMyCarsViewC")
        }
    }
    
    func getHelpCenterVC() -> HelpCenterViewC {
        if let helpCenterVC = self.getViewControler(storyBoard: .Main, indentifier: HelpCenterViewC.className) as? HelpCenterViewC {
            return helpCenterVC
        } else {
            fatalError("Not able to initialize HelpCenterViewC")
        }
    }
    
    func getFeedbackVC() -> FeedbackViewC {
        if let feedbackVC = self.getViewControler(storyBoard: .Main, indentifier: FeedbackViewC.className) as? FeedbackViewC {
            return feedbackVC
        } else {
            fatalError("Not able to initialize FeedbackViewC")
        }
    }
    
    func getPaymentVC() -> PaymentViewC {
        if let paymentVC = self.getViewControler(storyBoard: .Main, indentifier: PaymentViewC.className) as? PaymentViewC {
            return paymentVC
        } else {
            fatalError("Not able to initialize PaymentViewC")
        }
    }
    
    func getAddNewCardVC() -> AddNewCardViewC {
        if let addNewCardVC = self.getViewControler(storyBoard: .Main, indentifier: AddNewCardViewC.className) as? AddNewCardViewC {
            return addNewCardVC
        } else {
            fatalError("Not able to initialize AddNewCardViewC")
        }
    }
    
    func getMyTransactionsVC() -> SMyTransactionsViewC {
        if let myTransactionsVC = self.getViewControler(storyBoard: .Main, indentifier: SMyTransactionsViewC.className) as? SMyTransactionsViewC {
            return myTransactionsVC
        } else {
            fatalError("Not able to initialize SMyTransactionsViewC")
        }
    }
    
    func getEditProfileVC() -> EditProfileViewC {
        if let editProfileVC = self.getViewControler(storyBoard: .Main, indentifier: EditProfileViewC.className) as? EditProfileViewC {
            return editProfileVC
        } else {
            fatalError("Not able to initialize EditProfileViewC")
        }
    }
    
    func getCDocumentDetailsVC() -> CDocumentDetailsViewC {
        if let documentDetailsVC = self.getViewControler(storyBoard: .CustomerHome, indentifier: CDocumentDetailsViewC.className) as? CDocumentDetailsViewC {
            documentDetailsVC.viewModel = DocumentDetailsViewM()
            return documentDetailsVC
        } else {
            fatalError("Not able to initialize DocumentDetailsViewC")
        }
    }
    
    // MARK: - WebView View Controller
    func getwebViewVC() -> WebViewVController {
        if let webViewVC = self.getViewControler(storyBoard: .DealerMain, indentifier: WebViewVController.className) as? WebViewVController {
            return webViewVC
        } else {
            fatalError("Not able to initialize WebViewVController")
        }
    }
    
    // MARK: - ImagePicker Alert View Controller
    func getImagePickerAlertVC() -> ImagePickerAlertViewC {
        if let imagePickerAlertVC = self.getViewControler(storyBoard: .DealerMain, indentifier: ImagePickerAlertViewC.className) as? ImagePickerAlertViewC {
            return imagePickerAlertVC
        } else {
            fatalError("Not able to initialize ImagePickerAlertViewC")
        }
    }
    
    //MARK: - DocumentList ViewController
    func getDocumentListViewC() -> DocumentListViewC {
        if let documentListViewC = self.getViewControler(storyBoard: .DealerMain, indentifier: DocumentListViewC.className) as? DocumentListViewC {
            return documentListViewC
        } else {
            fatalError("Not able to initialize DocumentListViewC")
        }
    }
    
    // MARK: - Register Payment View Controller
    func getRegisterPaymentViewC() -> RegistrationPaymentViewC {
        if let registerPaymentViewC = self.getViewControler(storyBoard: .DealerMain, indentifier: RegistrationPaymentViewC.className) as? RegistrationPaymentViewC {
            return registerPaymentViewC
        } else {
            fatalError("Not able to initialize RegistrationPaymentViewC")
        }
    }
    
    //MARK: - Car Details View Controller
    func getCCarDetailsVC() -> CCarDetailsViewC {
        if let carDetailsVC = self.getViewControler(storyBoard: .CustomerHome, indentifier: CCarDetailsViewC.className) as? CCarDetailsViewC {
            carDetailsVC.viewModel = CarDetailsViewM()
            return carDetailsVC
        } else {
            fatalError("Not able to initialize CarDetailsViewC")
        }
    }
    
    //MARK: - Book Now View Controller
    func getCBookNowVC() -> CBookNowViewC {
        if let bookNowVC = self.getViewControler(storyBoard: .CustomerHome, indentifier: CBookNowViewC.className) as? CBookNowViewC {
            return bookNowVC
        } else {
            fatalError("Not able to initialize BookNowViewC")
        }
    }
    
    func getSChooseServiceVC() -> SChooseServiceViewC {
        if let chooseServiceVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SChooseServiceViewC.className) as? SChooseServiceViewC {
            return chooseServiceVC
        } else {
            fatalError("Not able to initialize SChooseServiceViewC")
        }
    }
    
    // MARK: - UploadDocument View Controller
    func getUploadDocumentViewC() -> UploadDocumentViewC {
        if let uploadDocumentViewC = self.getViewControler(storyBoard: .LoginSignup, indentifier: UploadDocumentViewC.className) as? UploadDocumentViewC {
            return uploadDocumentViewC
        } else {
            fatalError("Not able to initialize UploadDocumentViewC")
        }
    }
    
    //MARK:- BookCarFinance View Controller
    func getCBookCarFinanceViewC() -> CBookCarFinanceViewC {
        if let bookCarFinanceViewC = self.getViewControler(storyBoard: .CustomerHome, indentifier: CBookCarFinanceViewC.className) as? CBookCarFinanceViewC {
            return bookCarFinanceViewC
        } else {
            fatalError("Not able to initialize CBookCarFinanceViewC")
        }
    }
    
    //MARK:- AutoFinance View Controller
    func getCAutoFinanceViewC() -> CAutoFinanceViewC {
        if let autoFinanceViewC = self.getViewControler(storyBoard: .CustomerHome, indentifier: CAutoFinanceViewC.className) as? CAutoFinanceViewC {
            return autoFinanceViewC
        } else {
            fatalError("Not able to initialize CAutoFinanceViewC")
        }
    }
    
    //MARK:- BuyCarNow View Controller
    func getCBuyCarNowViewC() -> CBuyNowCarViewC {
        if let buyCarNowViewC = self.getViewControler(storyBoard: .CustomerHome, indentifier: CBuyNowCarViewC.className) as? CBuyNowCarViewC {
            buyCarNowViewC.viewModel = BuyNowCarViewM()
            return buyCarNowViewC
        } else {
            fatalError("Not able to initialize CBuyNowCarViewC")
        }
    }
    
    //MARK:- FinanceForm View Controller
    func getCFinanceFormViewC() -> CFinanceFormViewC {
        if let financeFormVC = self.getViewControler(storyBoard: .CustomerHome, indentifier: CFinanceFormViewC.className) as? CFinanceFormViewC {
            financeFormVC.viewModel = CFinanceFormViewM()
            return financeFormVC
        } else {
            fatalError("Not able to initialize CFinanceFormViewC")
        }
    }
    
    func getCApplicationsViewC() -> CApplicationsViewC {
        if let cApplicationsViewC = self.getViewControler(storyBoard: .Account, indentifier: CApplicationsViewC.className) as? CApplicationsViewC {
            return cApplicationsViewC
        } else {
            fatalError("Not able to initialize CApplicationsViewC")
        }
    }
    
    func getPaymentHistoryViewC() -> PaymentHistoryViewC {
        if let paymentHistoryViewC = self.getViewControler(storyBoard: .Account, indentifier: PaymentHistoryViewC.className) as? PaymentHistoryViewC {
            return paymentHistoryViewC
        } else {
            fatalError("Not able to initialize PaymentHistoryViewC")
        }
    }
    
    func getMyAccountVC() -> MyAccountViewC {
        if let myAccountVC = self.getViewControler(storyBoard: .Account, indentifier: MyAccountViewC.className) as? MyAccountViewC {
            return myAccountVC
        } else {
            fatalError("Not able to initialize MyAccountViewC")
        }
    }
    
    func getNotificationVC() -> SNotificationViewC {
        if let notificationVC = self.getViewControler(storyBoard: .Account, indentifier: SNotificationViewC.className) as? SNotificationViewC {
            return notificationVC
        } else {
            fatalError("Not able to initialize SNotificationViewC")
        }
    }
    
    func getWatchListVC() -> WatchListViewC {
        if let notificationVC = self.getViewControler(storyBoard: .Account, indentifier: WatchListViewC.className) as? WatchListViewC {
            return notificationVC
        } else {
            fatalError("Not able to initialize WatchListViewC")
        }
    }
    
    func getUploadCarImagesVC() -> CUploadCarImagesViewC {
        if let uploadCarImagesVC = self.getViewControler(storyBoard: .CustomerCar, indentifier: CUploadCarImagesViewC.className) as? CUploadCarImagesViewC {
            return uploadCarImagesVC
        } else {
            fatalError("Not able to initialize CUploadCarImagesViewC")
        }
    }
    
    func getSellCarListVC() -> CSellCarListViewC {
        if let sellCarListVC = self.getViewControler(storyBoard: .CustomerCar, indentifier: CSellCarListViewC.className) as? CSellCarListViewC {
            return sellCarListVC
        } else {
            fatalError("Not able to initialize CSellCarListViewC")
        }
    }
    
    func getSellCarFormVC() -> CSellCarFormViewC {
        if let sellCarListVC = self.getViewControler(storyBoard: .CustomerCar, indentifier: CSellCarFormViewC.className) as? CSellCarFormViewC {
            return sellCarListVC
        } else {
            fatalError("Not able to initialize CSellCarFormViewC")
        }
    }
    
    func getCSellCarIntroVC() -> CSellCarIntroViewC {
        if let sellCarIntroVC = self.getViewControler(storyBoard: .CustomerCar, indentifier: CSellCarIntroViewC.className) as? CSellCarIntroViewC {
            return sellCarIntroVC
        } else {
            fatalError("Not able to initialize CSellCarIntroViewC")
        }
    }
    
    func getDFleetBidingVC() -> DFleetBidingViewC {
        if let dFleetBidingVC = self.getViewControler(storyBoard: .DealerFleetBiding, indentifier: DFleetBidingViewC.className) as? DFleetBidingViewC {
            dFleetBidingVC.viewModel = DFleetBidingViewM()
            return dFleetBidingVC
        } else {
            fatalError("Not able to initialize DFleetBidingViewC")
        }
    }
    
    func getCRequestCarVC() -> CRequestCarViewC {
        if let requestCarVC = self.getViewControler(storyBoard: .CustomerCar, indentifier: CRequestCarViewC.className) as? CRequestCarViewC {
            return requestCarVC
        } else {
            fatalError("Not able to initialize CRequestCarViewC")
        }
    }
    
    func getCRequestedCarListVC() -> CRequestedCarListViewC {
        if let requestedCarVC = self.getViewControler(storyBoard: .CustomerCar, indentifier: CRequestedCarListViewC.className) as? CRequestedCarListViewC {
            return requestedCarVC
        } else {
            fatalError("Not able to initialize CRequestedCarListViewC")
        }
    }
    
    func getDAuctionVC() -> DAuctionViewC {
        if let auctionVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DAuctionViewC.className) as? DAuctionViewC {
            return auctionVC
        } else {
            fatalError("Not able to initialize DAuctionViewC")
        }
    }
    
    func getDAuctionAddCarFirstVC() -> DAuctionAddCarFirstViewC {
        if let auctionVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DAuctionAddCarFirstViewC.className) as? DAuctionAddCarFirstViewC {
            return auctionVC
        } else {
            fatalError("Not able to initialize DAuctionAddCarFirstViewC")
        }
    }
    
    func getDAuctionAddCarSecondVC() -> DAuctionAddCarSecondViewC {
        if let auctionVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DAuctionAddCarSecondViewC.className) as? DAuctionAddCarSecondViewC {
            return auctionVC
        } else {
            fatalError("Not able to initialize DAuctionAddCarSecondViewC")
        }
    }
    
    func getDAuctionAddCarThirdVC() -> DAuctionAddCarThirdViewC {
        if let auctionVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DAuctionAddCarThirdViewC.className) as? DAuctionAddCarThirdViewC {
            return auctionVC
        } else {
            fatalError("Not able to initialize DAuctionAddCarThirdViewC")
        }
    }
    
    func getDChooseAuctionTypeVC() -> DChooseAuctionTypeViewC {
        if let auctionVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DChooseAuctionTypeViewC.className) as? DChooseAuctionTypeViewC {
            return auctionVC
        } else {
            fatalError("Not able to initialize DChooseAuctionTypeViewC")
        }
    }
        
    func getDBrandFleetListVC() -> DBrandFleetListViewC {
        if let brandFleetListVC = self.getViewControler(storyBoard: .DealerFleetBiding, indentifier: DBrandFleetListViewC.className) as? DBrandFleetListViewC {
            brandFleetListVC.viewModel = DBrandFleetListViewM()
            return brandFleetListVC
        } else {
            fatalError("Not able to initialize DBrandFleetListViewC")
        }
    }
    
    func getDFleetBidingListVC() -> DFleetBidingListViewC {
        if let fleetBidingListVC = self.getViewControler(storyBoard: .DealerFleetBiding, indentifier: DFleetBidingListViewC.className) as? DFleetBidingListViewC {
            fleetBidingListVC.viewModel = DFleetBidingListViewM()
            return fleetBidingListVC
        } else {
            fatalError("Not able to initialize DFleetBidingListViewC")
        }
    }
    
    func getDMyBidsVC() -> DMyBidsViewC {
        if let myBidsVC = self.getViewControler(storyBoard: .DealerFleetBiding, indentifier: DMyBidsViewC.className) as? DMyBidsViewC {
            myBidsVC.viewModel = DMyBidsViewM()
            return myBidsVC
        } else {
            fatalError("Not able to initialize DMyBidsViewC")
        }
    }
    
    func getDAuctionAddCarFourthVC() -> DAuctionAddCarFourthViewC {
        if let auctionVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DAuctionAddCarFourthViewC.className) as? DAuctionAddCarFourthViewC {
            return auctionVC
        } else {
            fatalError("Not able to initialize DAuctionAddCarFourthViewC")
        }
    }
    
    func getDAuctionAddCarFifthVC() -> DAuctionAddCarFifthViewC {
        if let auctionVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DAuctionAddCarFifthViewC.className) as? DAuctionAddCarFifthViewC {
            return auctionVC
        } else {
            fatalError("Not able to initialize DAuctionAddCarFifthViewC")
        }
    }
    
    func getDAuctionUploadCarImageVC() -> DAuctionUploadCarImageViewC {
        if let auctionVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DAuctionUploadCarImageViewC.className) as? DAuctionUploadCarImageViewC {
            return auctionVC
        } else {
            fatalError("Not able to initialize DAuctionUploadCarImageViewC")
        }
    }
    
    func getDSavedAuctionVC() -> DSavedAuctionViewC {
        if let auctionVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DSavedAuctionViewC.className) as? DSavedAuctionViewC {
            return auctionVC
        } else {
            fatalError("Not able to initialize DSavedAuctionViewC")
        }
    }
    
    func getDAuctionConfirmationVC() -> DAuctionConfirmationViewC {
        if let auctionVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DAuctionConfirmationViewC.className) as? DAuctionConfirmationViewC {
            return auctionVC
        } else {
            fatalError("Not able to initialize DAuctionConfirmationViewC")
        }
    }
    
    func getDAuctionAddCarSixthVC() -> DAuctionAddCarSixthViewC {
        if let auctionVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DAuctionAddCarSixthViewC.className) as? DAuctionAddCarSixthViewC {
            return auctionVC
        } else {
            fatalError("Not able to initialize DAuctionAddCarSixthViewC")
        }
    }
    
    func getCarFinanceStatusVC() -> CarFinanceStatusViewC {
        if let carFinanceStatusVC = self.getViewControler(storyBoard: .CustomerHome, indentifier: CarFinanceStatusViewC.className) as? CarFinanceStatusViewC {
            return carFinanceStatusVC
        } else {
            fatalError("Not able to initialize CarFinanceStatusViewC")
        }
    }
    
    func getDAuctionCongratulationsVC() -> DAuctionCongratulationsViewC {
        if let auctionCongratulationsVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DAuctionCongratulationsViewC.className) as? DAuctionCongratulationsViewC {
            return auctionCongratulationsVC
        } else {
            fatalError("Not able to initialize DAuctionCongratulationsViewC")
        }
    }
    
    func getDAllAuctionRoomVC() -> DAllAuctionRoomViewC {
        if let allAuctionRoomVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DAllAuctionRoomViewC.className) as? DAllAuctionRoomViewC {
            allAuctionRoomVC.viewModel = DAllAuctionRoomViewM()
            return allAuctionRoomVC
        } else {
            fatalError("Not able to initialize DAllAuctionRoomViewC")
        }
    }
    
    // MARK: - Auction List
    func getDAuctionListVC() -> DAuctionListViewC {
        if let auctionListVC = self.getViewControler(storyBoard: .DealerFleetBiding, indentifier: DAuctionListViewC.className) as? DAuctionListViewC {
            return auctionListVC
        } else {
            fatalError("Not able to initialize DAuctionListViewC")
        }
    }
    
    // MARK: - Featured Auctions
    func getDFeaturedAuctionsVC() -> DFeaturedAuctionsViewC {
        if let featuredAuctionsVC = self.getViewControler(storyBoard: .DealerFleetBiding, indentifier: DFeaturedAuctionsViewC.className) as? DFeaturedAuctionsViewC {
            return featuredAuctionsVC
        } else {
            fatalError("Not able to initialize DFeaturedAuctionsViewC")
        }
    }
    
    // MARK: - Live Auction List
    func getDLiveAuctionListVC() -> DLiveAuctionListViewC {
        if let liveAuctionListVC = self.getViewControler(storyBoard: .DealerFleetBiding, indentifier: DLiveAuctionListViewC.className) as? DLiveAuctionListViewC {
            return liveAuctionListVC
        } else {
            fatalError("Not able to initialize DLiveAuctionListViewC")
        }
    }
    
    // MARK: - Upcoming Auction List
    func getDUpcomingAuctionListVC() -> DUpcomingAuctionListViewC {
        if let upcomingAuctionListVC = self.getViewControler(storyBoard: .DealerFleetBiding, indentifier: DUpcomingAuctionListViewC.className) as? DUpcomingAuctionListViewC {
            return upcomingAuctionListVC
        } else {
            fatalError("Not able to initialize DUpcomingAuctionListViewC")
        }
    }
    
    // MARK: - Auction Room
    func getDAuctionRoomVC() -> DAuctionRoomViewC {
        if let auctionRoomVC = self.getViewControler(storyBoard: .DealerFleetBiding, indentifier: DAuctionRoomViewC.className) as? DAuctionRoomViewC {
            auctionRoomVC.viewModel = DAuctionRoomViewM()
            return auctionRoomVC
        } else {
            fatalError("Not able to initialize DAuctionRoomViewC")
        }
    }
    
    // MARK: - COffersAndDealsViewC
    func getCOffersAndDealsVC() -> COffersAndDealsViewC {
        if let upcomingAuctionListVC = self.getViewControler(storyBoard: .CustomerMain, indentifier: COffersAndDealsViewC.className) as? COffersAndDealsViewC {
            return upcomingAuctionListVC
        } else {
            fatalError("Not able to initialize COffersAndDealsViewC")
        }
    }
    
    // MARK: - DAuctionFilterVC
    func getDAuctionFilterVC() -> DAuctionFilterViewC {
        if let auctionFilterVC = self.getViewControler(storyBoard: .DealerMain, indentifier: DAuctionFilterViewC.className) as? DAuctionFilterViewC {
            return auctionFilterVC
        } else {
            fatalError("Not able to initialize DAuctionFilterViewC")
        }
    }
    // MARK: - CServiceContractDetailsViewC
    func getCServiceContractDetailsVC() -> CServiceContractDetailsViewC {
        if let contractDetailsVC = self.getViewControler(storyBoard: .CustomerMain, indentifier: CServiceContractDetailsViewC.className) as? CServiceContractDetailsViewC {
            return contractDetailsVC
        } else {
            fatalError("Not able to initialize CServiceContractDetailsViewC")
        }
    }
    
    // MARK: - CThirdPartyWarrantyDetailsViewC
    func getCThirdPartyWarrantyDetailVC() -> CThirdPartyWarrantyDetailsViewC {
        if let warrantyDetailVC = self.getViewControler(storyBoard: .CustomerMain, indentifier: CThirdPartyWarrantyDetailsViewC.className) as? CThirdPartyWarrantyDetailsViewC {
            return warrantyDetailVC
        } else {
            fatalError("Not able to initialize CThirdPartyWarrantyDetailsViewC")
        }
    }
    
    
    // MARK: - CThirdPartyWarrantyVC
    func getCThirdPartyWarrantyVC() -> CThirdPartyWarrantyViewC {
        if let cThirdPartyWarrantyVC = self.getViewControler(storyBoard: .CustomerCar, indentifier: CThirdPartyWarrantyViewC.className) as? CThirdPartyWarrantyViewC {
            return cThirdPartyWarrantyVC
        } else {
            fatalError("Not able to initialize CThirdPartyWarrantyViewC")
        }
    }
    
    
    // MARK: - ChooseCarForServiceViewC
    func getChooseCarForServiceVC() -> ChooseCarForServiceViewC {
        if let chooseCarForServiceVC = self.getViewControler(storyBoard: .CustomerMain, indentifier: ChooseCarForServiceViewC.className) as? ChooseCarForServiceViewC {
            return chooseCarForServiceVC
        } else {
            fatalError("Not able to initialize ChooseCarForServiceViewC")
        }
    }
    
    // MARK: - CServiceHistoryViewC
    func getCServiceHistoryVC() -> CServiceHistoryViewC {
        if let serviceHistoryVC = self.getViewControler(storyBoard: .CustomerMain, indentifier: CServiceHistoryViewC.className) as? CServiceHistoryViewC {
            return serviceHistoryVC
        } else {
            fatalError("Not able to initialize CServiceHistoryViewC")
        }
    }
    
    // MARK: - CServiceContractWarrantyVC
    func getCServiceContractVC() -> CServiceContractViewC {
        if let cServiceContractVC = self.getViewControler(storyBoard: .CustomerCar, indentifier: CServiceContractViewC.className) as? CServiceContractViewC {
            return cServiceContractVC
        } else {
            fatalError("Not able to initialize CServiceContractViewC")
        }
    }
    
    // MARK: - SmartTendersViewC
    func getSmartTendersVC() -> SmartTendersViewC {
        if let smartTendersVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: SmartTendersViewC.className) as? SmartTendersViewC {
            return smartTendersVC
        } else {
            fatalError("Not able to initialize SmartTendersViewC")
        }
    }
    
    // MARK: - RecoveryHomeViewC
    func getRecoveryHomeVC() -> RecoveryHomeViewC {
        if let smartTendersVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: RecoveryHomeViewC.className) as? RecoveryHomeViewC {
            return smartTendersVC
        } else {
            fatalError("Not able to initialize RecoveryHomeViewC")
        }
    }
    
    // MARK: - AllTendersViewC
    func getAllTendersVC() -> AllTendersViewC {
        if let allTendersVC = self.getViewControler(storyBoard: .CustomerCar, indentifier: AllTendersViewC.className) as? AllTendersViewC {
            return allTendersVC
        } else {
            fatalError("Not able to initialize AllTendersViewC")
        }
    }
    
    // MARK: - RDBidListViewC
    func getRDBidListVC() -> RDBidListViewC {
        if let rdBidListVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: RDBidListViewC.className) as? RDBidListViewC {
            rdBidListVC.viewModel = RDBidListViewM()
            return rdBidListVC
        } else {
            fatalError("Not able to initialize RDBidListViewC")
        }
    }
    
    // MARK: - FirstRecoveryReqViewC
    func getFirstRecoveryReqVC() -> FirstRecoveryReqVC {
        if let firstRecoveryReqVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: FirstRecoveryReqVC.className) as? FirstRecoveryReqVC {
            return firstRecoveryReqVC
        } else {
            fatalError("Not able to initialize FirstRecoveryReqVC")
        }
    }
    
    // MARK: - SecondRecoveryReqViewC
    func getSecondRecoveryReqVC() -> SecondRecoveryReqVC {
        if let secondRecoveryReqVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: SecondRecoveryReqVC.className) as? SecondRecoveryReqVC {
            return secondRecoveryReqVC
        } else {
            fatalError("Not able to initialize SecondRecoveryReqVC")
        }
    }
    
    // MARK: - JobDoneViewC
    func getJobDoneViewC() -> JobDoneViewC {
        if let jobDoneViewC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: JobDoneViewC.className) as? JobDoneViewC {
            return jobDoneViewC
        } else {
            fatalError("Not able to initialize JobDoneViewC")
        }
    }
    
    // MARK: - DSoldUnsoldCarViewC
    func getDSoldUnsoldCarViewC() -> DSoldUnsoldCarViewC {
        if let dSoldUnsoldCarVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DSoldUnsoldCarViewC.className) as? DSoldUnsoldCarViewC {
            dSoldUnsoldCarVC.viewModel = DSoldUnsoldCarViewM()
            return dSoldUnsoldCarVC
        } else {
            fatalError("Not able to initialize DSoldUnsoldCarViewC")
        }
    }
    
    // MARK: - VITenderListViewC
    func getVITenderListVC() -> VITenderListViewC {
        if let viTenderListVC = self.getViewControler(storyBoard: .CustomerCar, indentifier: VITenderListViewC.className) as? VITenderListViewC {
            return viTenderListVC
        } else {
            fatalError("Not able to initialize VITenderListViewC")
        }
    }
    
    // MARK: - VITenderListViewC
    func getVITenderReqVC() -> VITenderRequestViewC {
        if let viTenderRequestVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: VITenderRequestViewC.className) as? VITenderRequestViewC {
            return viTenderRequestVC
        } else {
            fatalError("Not able to initialize VITenderRequestViewC")
        }
    }
    
    // MARK: - Leads
    func getDLeadsVC() -> DLeadsViewC {
        if let leadsVC = self.getViewControler(storyBoard: .DealerFleetBiding, indentifier: DLeadsViewC.className) as? DLeadsViewC {
            // auctionRoomVC.viewModel = DAuctionRoomViewM()
            return leadsVC
        } else {
            fatalError("Not able to initialize DLeadsViewC")
        }
    }
    
    // MARK: - Leads
    func getDMyLeadsVC() -> DMyLeadsViewC {
        if let leadsVC = self.getViewControler(storyBoard: .DealerFleetBiding, indentifier: DMyLeadsViewC.className) as? DMyLeadsViewC {
            // auctionRoomVC.viewModel = DAuctionRoomViewM()
            return leadsVC
        } else {
            fatalError("Not able to initialize DMyLeadsViewC")
        }
    }
    
    // MARK: - Leads
    func getDLeadsFilterVC() -> DLeadsFilterViewC {
        if let leadsVC = self.getViewControler(storyBoard: .DealerFleetBiding, indentifier: DLeadsFilterViewC.className) as? DLeadsFilterViewC {
            // auctionRoomVC.viewModel = DAuctionRoomViewM()
            return leadsVC
        } else {
            fatalError("Not able to initialize DLeadsFilterViewC")
        }
    }
    
    // MARK: - VIFirstViewC
    func getVIFirstDetailVC() -> VIFirstDetailViewC {
        if let viFirstDetailVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: VIFirstDetailViewC.className) as? VIFirstDetailViewC {
            return viFirstDetailVC
        } else {
            fatalError("Not able to initialize VIFirstDetailViewC")
        }
    }
    
    // MARK: - DLeaderboardViewC
    func getDLeaderboardVC() -> DLeaderboardViewC {
        if let dLeaderboardVC = self.getViewControler(storyBoard: .DealerMain, indentifier: DLeaderboardViewC.className) as? DLeaderboardViewC {
            dLeaderboardVC.viewModel = DLeaderboardViewM()
            return dLeaderboardVC
        } else {
            fatalError("Not able to initialize DLeaderboardViewC")
        }
    }
    
    // MARK: - VISecondViewC
    func getVISecondDetailVC() -> VISecondDetailViewC {
        if let viSecondDetailVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: VISecondDetailViewC.className) as? VISecondDetailViewC {
            return viSecondDetailVC
        } else {
            fatalError("Not able to initialize VISecondDetailViewC")
        }
    }
    
    // MARK: - USTenderListViewC
    func getUSTenderListVC() -> USTenderListViewC {
        if let usTenderListVC = self.getViewControler(storyBoard: .CustomerCar, indentifier: USTenderListViewC.className) as? USTenderListViewC {
            return usTenderListVC
        } else {
            fatalError("Not able to initialize USTenderListViewC")
        }
    }
    
    // MARK: - USFirstDetailViewC
    func getUSFirstDetailVC() -> USFirstDetailViewC {
        if let usFirstDetailVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: USFirstDetailViewC.className) as? USFirstDetailViewC {
            return usFirstDetailVC
        } else {
            fatalError("Not able to initialize USFirstDetailViewC")
        }
    }
    
    // MARK: - USSecondDetailViewC
    func getUSSecondDetailVC() -> USSecondDetailViewC {
        if let viSecondDetailVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: USSecondDetailViewC.className) as? USSecondDetailViewC {
            return viSecondDetailVC
        } else {
            fatalError("Not able to initialize USSecondDetailViewC")
        }
    }
    
    // MARK: - SRecoveryTenderViewC
    func getSRecoveryTenderVC() -> SRecoveryTenderViewC {
        if let recoveryTenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SRecoveryTenderViewC.className) as? SRecoveryTenderViewC {
            return recoveryTenderVC
        } else {
            fatalError("Not able to initialize SRecoveryTenderViewC")
        }
    }
    
    func getSRecoveryTenderDetailsVC() -> SRecoveryTenderDetailsViewC {
        if let recoveryTenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SRecoveryTenderDetailsViewC.className) as? SRecoveryTenderDetailsViewC {
            return recoveryTenderVC
        } else {
            fatalError("Not able to initialize SRecoveryTenderDetailsViewC")
        }
    }
    
    // MARK: - USTenderListViewC
    func getUSTenderReqVC() -> USTenderReqViewC {
        if let usTenderRequestVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: USTenderReqViewC.className) as? USTenderReqViewC {
            return usTenderRequestVC
        } else {
            fatalError("Not able to initialize USTenderReqViewC")
        }
    }
    
    
    // MARK: - Recovery second detail
    func getSecondRecoveryDetailVC() -> SecondRecoveryDetailVC {
        if let recoverySecondDetailVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: SecondRecoveryDetailVC.className) as? SecondRecoveryDetailVC {
            return recoverySecondDetailVC
        } else {
            fatalError("Not able to initialize SecondRecoveryDetailVC")
        }
    }
    func getSBidSubmittedVC() -> SBidSubmittedViewC {
        if let recoveryTenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SBidSubmittedViewC.className) as? SBidSubmittedViewC {
            return recoveryTenderVC
        } else {
            fatalError("Not able to initialize SBidSubmittedViewC")
        }
    }
    
    func getSBidsRejectedVC() -> SBidsRejectedViewC {
        if let recoveryTenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SBidsRejectedViewC.className) as? SBidsRejectedViewC {
            return recoveryTenderVC
        } else {
            fatalError("Not able to initialize SBidsRejectedViewC")
        }
    }
    
    func getSJobCompletedVC() -> SJobCompletedViewC {
        if let recoveryTenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SJobCompletedViewC.className) as? SJobCompletedViewC {
            return recoveryTenderVC
        } else {
            fatalError("Not able to initialize SJobCompletedViewC")
        }
    }
    
    func getSTenderWonVC() -> STenderWonViewC {
        if let recoveryTenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: STenderWonViewC.className) as? STenderWonViewC {
            return recoveryTenderVC
        } else {
            fatalError("Not able to initialize STenderWonViewC")
        }
    }
    
    func getSVehicleInspectionTendersVC() -> SVehicleInspectionTendersViewC {
        if let recoveryTenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SVehicleInspectionTendersViewC.className) as? SVehicleInspectionTendersViewC {
            return recoveryTenderVC
        } else {
            fatalError("Not able to initialize SVehicleInspectionTendersViewC")
        }
    }
    
    func getVITenderDetailsVC() -> VITenderDetailsViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: VITenderDetailsViewC.className) as? VITenderDetailsViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize VITenderDetailsViewC")
        }
    }
    
    func getSVIBidsRejectedVC() -> SVIBidsRejectedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SVIBidsRejectedViewC.className) as? SVIBidsRejectedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SVIBidsRejectedViewC")
        }
    }
    
    func getSVIBidSubmittedVC() -> SVIBidSubmittedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SVIBidSubmittedViewC.className) as? SVIBidSubmittedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SVIBidSubmittedViewC")
        }
    }
    
    func getSVIJobCompletedVC() -> SVIJobCompletedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SVIJobCompletedViewC.className) as? SVIJobCompletedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SVIJobCompletedViewC")
        }
    }
    
    func getSVITenderWonVC() -> SVITenderWonViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SVITenderWonViewC.className) as? SVITenderWonViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SVITenderWonViewC")
        }
    }
    
    func getSUpholsteryTendersVC() -> SUpholsteryTendersViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SUpholsteryTendersViewC.className) as? SUpholsteryTendersViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SUpholsteryTendersViewC")
        }
    }
    
    func getSUpholsteryTenderDetailsVC() -> SUpholsteryTenderDetailsViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SUpholsteryTenderDetailsViewC.className) as? SUpholsteryTenderDetailsViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SUpholsteryTenderDetailsViewC")
        }
    }
    
    func getSUpholsteryBidsRejectedVC() -> SUpholsteryBidsRejectedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SUpholsteryBidsRejectedViewC.className) as? SUpholsteryBidsRejectedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SUpholsteryBidsRejectedViewC")
        }
    }
    
    func getSUpholsteryBidSubmittedVC() -> SUpholsteryBidSubmittedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SUpholsteryBidSubmittedViewC.className) as? SUpholsteryBidSubmittedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SUpholsteryBidSubmittedViewC")
        }
    }
    
    func getSUpholsteryJobCompletedVC() -> SUpholsteryJobCompletedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SUpholsteryJobCompletedViewC.className) as? SUpholsteryJobCompletedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SUpholsteryJobCompletedViewC")
        }
    }
    
    func getSUpholsteryTenderWonVC() -> SUpholsteryTenderWonViewC {
           if let tenderVC = self.getViewControler(storyBoard: .SupplierRecovery, indentifier: SUpholsteryTenderWonViewC.className) as? SUpholsteryTenderWonViewC {
               return tenderVC
           } else {
               fatalError("Not able to initialize SUpholsteryTenderWonViewC")
           }
       }
    
    func getSCarDetailingTenderVC() -> SCarDetailingTenderViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SCarDetailingTenderViewC.className) as? SCarDetailingTenderViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SCarDetailingTenderViewC")
        }
    }
    
    func getSCarDetailingTenderDetailsVC() -> SCarDetailingTenderDetailsViewC {
           if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SCarDetailingTenderDetailsViewC.className) as? SCarDetailingTenderDetailsViewC {
               return tenderVC
           } else {
               fatalError("Not able to initialize SCarDetailingTenderDetailsViewC")
           }
    }
    
    func getSInsuranceTenderVC() -> SInsuranceTenderViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SInsuranceTenderViewC.className) as? SInsuranceTenderViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SInsuranceTenderViewC")
         }
     }
    
    func getSInsuranceTenderDetailsVC() -> SInsuranceTenderDetailsViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SInsuranceTenderDetailsViewC.className) as? SInsuranceTenderDetailsViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SInsuranceTenderDetailsViewC")
        }
    }
     
    func getSBankValuationTenderVC() -> SBankValuationTenderViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SBankValuationTenderViewC.className) as? SBankValuationTenderViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SBankValuationTenderViewC")
        }
    }
    
    func getSBankValuationTenderDetailsVC() -> SBankValuationTenderDetailsViewC {
           if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SBankValuationTenderDetailsViewC.className) as? SBankValuationTenderDetailsViewC {
               return tenderVC
           } else {
               fatalError("Not able to initialize SBankValuationTenderDetailsViewC")
           }
       }
    
    func getSCarDetailingTenderWonVC() -> SCarDetailingTenderWonViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SCarDetailingTenderWonViewC.className) as? SCarDetailingTenderWonViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SCarDetailingTenderWonViewC")
        }
    }
    
    func getSCarDetailingBidSubmittedVC() -> SCarDetailingBidSubmittedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SCarDetailingBidSubmittedViewC.className) as? SCarDetailingBidSubmittedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SCarDetailingBidSubmittedViewC")
        }
    }
    
    func getSCarDetailingBidsRejectedVC() -> SCarDetailingBidsRejectedViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SCarDetailingBidsRejectedViewC.className) as? SCarDetailingBidsRejectedViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SCarDetailingBidsRejectedViewC")
         }
     }
    
    func getSCarDetailingJobCompletedVC() -> SCarDetailingJobCompletedViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SCarDetailingJobCompletedViewC.className) as? SCarDetailingJobCompletedViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SCarDetailingJobCompletedViewC")
         }
     }
    
    func getSBankValuationTenderWonVC() -> SBankValuationTenderWonViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SBankValuationTenderWonViewC.className) as? SBankValuationTenderWonViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SBankValuationTenderWonViewC")
         }
     }
    
    func getSBankValuationBidsRejectedVC() -> SBankValuationBidsRejectedViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SBankValuationBidsRejectedViewC.className) as? SBankValuationBidsRejectedViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SBankValuationBidsRejectedViewC")
         }
     }
    
    func getSBankValuationBidSubmittedVC() -> SBankValuationBidSubmittedViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SBankValuationBidSubmittedViewC.className) as? SBankValuationBidSubmittedViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SBankValuationBidSubmittedViewC")
         }
     }
    
    func getSBankValuationJobCompletedVC() -> SBankValuationJobCompletedViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SBankValuationJobCompletedViewC.className) as? SBankValuationJobCompletedViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SBankValuationJobCompletedViewC")
         }
     }
    
    func getSInsuranceTenderWonVC() -> SInsuranceTenderWonViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SInsuranceTenderWonViewC.className) as? SInsuranceTenderWonViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SInsuranceTenderWonViewC")
         }
     }
    
    func getSInsuranceJobCompletedVC() -> SInsuranceJobCompletedViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SInsuranceJobCompletedViewC.className) as? SInsuranceJobCompletedViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SInsuranceJobCompletedViewC")
         }
     }
    
    func getSInsuranceBidSubmittedVC() -> SInsuranceBidSubmittedViewC {
           if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SInsuranceBidSubmittedViewC.className) as? SInsuranceBidSubmittedViewC {
               return tenderVC
           } else {
               fatalError("Not able to initialize SInsuranceBidSubmittedViewC")
           }
       }
    
    func getSInsuranceBidsRejectedVC() -> SInsuranceBidsRejectedViewC {
           if let tenderVC = self.getViewControler(storyBoard: .SupplierMain, indentifier: SInsuranceBidsRejectedViewC.className) as? SInsuranceBidsRejectedViewC {
               return tenderVC
           } else {
               fatalError("Not able to initialize SInsuranceBidsRejectedViewC")
           }
       }
    
    func getSCarServicingTenderVC() -> SCarServicingTenderViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SCarServicingTenderViewC.className) as? SCarServicingTenderViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize getSCarServicingTenderVC")
        }
    }
    
    func getSCarServicingTenderDetailsVC() -> SCarServicingTenderDetailsViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SCarServicingTenderDetailsViewC.className) as? SCarServicingTenderDetailsViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SCarServicingTenderDetailsViewC")
        }
    }
    
    func getSCarServicingBidSubmittedVC() -> SCarServicingBidSubmittedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SCarServicingBidSubmittedViewC.className) as? SCarServicingBidSubmittedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SCarServicingBidSubmittedViewC")
        }
    }
    
    func getSCarServicingBidsRejectedVC() -> SCarServicingBidsRejectedViewC {
           if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SCarServicingBidsRejectedViewC.className) as? SCarServicingBidsRejectedViewC {
               return tenderVC
           } else {
               fatalError("Not able to initialize SCarServicingBidsRejectedViewC")
           }
       }
    
    func getSCarServicingTenderWonVC() -> SCarServicingTenderWonViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SCarServicingTenderWonViewC.className) as? SCarServicingTenderWonViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SCarServicingTenderWonViewC")
        }
    }
    
    func getSCarServicingJobCompletedVC() -> SCarServicingJobCompletedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SCarServicingJobCompletedViewC.className) as? SCarServicingJobCompletedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SCarServicingJobCompletedViewC")
        }
    }
    
    func getSSparePartsTenderVC() -> SSparePartsTenderViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SSparePartsTenderViewC.className) as? SSparePartsTenderViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SSparePartsTenderViewC")
        }
    }
    
    func getSSparePartsTenderDetailsVC() -> SSparePartsTenderDetailsViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SSparePartsTenderDetailsViewC.className) as? SSparePartsTenderDetailsViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SSparePartsTenderDetailsViewC")
        }
    }
    
    func getSSparePartsTenderWonVC() -> SSparePartsTenderWonViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SSparePartsTenderWonViewC.className) as? SSparePartsTenderWonViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SSparePartsTenderWonViewC")
        }
    }
    
    func getSSparePartsJobCompletetVC() -> SSparePartsJobCompletetViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SSparePartsJobCompletetViewC.className) as? SSparePartsJobCompletetViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SSparePartsJobCompletetViewC")
        }
    }
    
    func getSSparePartsBidSubmittedVC() -> SSparePartsBidSubmittedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SSparePartsBidSubmittedViewC.className) as? SSparePartsBidSubmittedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SSparePartsBidSubmittedViewC")
        }
    }
    
    func getSSparePartsBidsRejectedVC() -> SSparePartsBidsRejectedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SSparePartsBidsRejectedViewC.className) as? SSparePartsBidsRejectedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SSparePartsBidsRejectedViewC")
        }
    }
    
    func getSWheelsTenderVC() -> SWheelsTenderViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SWheelsTenderViewC.className) as? SWheelsTenderViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SWheelsTenderViewC")
        }
    }
    
    func getSWheelsTenderDetailsVC() -> SWheelsTenderDetailsViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SWheelsTenderDetailsViewC.className) as? SWheelsTenderDetailsViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SWheelsTenderDetailsViewC")
        }
    }
    
    func getSWheelsTenderWonVC() -> SWheelsTenderWonViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SWheelsTenderWonViewC.className) as? SWheelsTenderWonViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SWheelsTenderWonViewC")
        }
    }
    
    func getSWheelsJobCompletedVC() -> SWheelsJobCompletedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SWheelsJobCompletedViewC.className) as? SWheelsJobCompletedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SWheelsJobCompletedViewC")
        }
    }
    
    func getSWheelsBidSubmittedVC() -> SWheelsBidSubmittedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SWheelsBidSubmittedViewC.className) as? SWheelsBidSubmittedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SWheelsBidSubmittedViewC")
        }
    }
    
    func getSWheelsBidsRejectedVC() -> SWheelsBidsRejectedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SWheelsBidsRejectedViewC.className) as? SWheelsBidsRejectedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SWheelsBidsRejectedViewC")
        }
    }
    
    func getSWindowTintingTenderVC() -> SWindowTintingTenderViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SWindowTintingTenderViewC.className) as? SWindowTintingTenderViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SWindowTintingTenderViewC")
         }
     }
    
    func getSWindowTintingTenderDetailsVC() -> SWindowTintingTenderDetailsViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SWindowTintingTenderDetailsViewC.className) as? SWindowTintingTenderDetailsViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SWindowTintingTenderDetailsViewC")
         }
     }
    
    func getSWindowTintingTenderWonVC() -> SWindowTintingTenderWonViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SWindowTintingTenderWonViewC.className) as? SWindowTintingTenderWonViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SWindowTintingTenderWonViewC")
         }
     }
   
    func getSWindowTintingJobCompletedVC() -> SWindowTintingJobCompletedViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SWindowTintingJobCompletedViewC.className) as? SWindowTintingJobCompletedViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SWindowTintingJobCompletedViewC")
         }
     }
    
    func getSWindowTintingBidSubmittedVC() -> SWindowTintingBidSubmittedViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SWindowTintingBidSubmittedViewC.className) as? SWindowTintingBidSubmittedViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SWindowTintingBidSubmittedViewC")
         }
     }
    
    func getSWindowTintingBidsRejectedVC() -> SWindowTintingBidsRejectedViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SWindowTintingBidsRejectedViewC.className) as? SWindowTintingBidsRejectedViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SWindowTintingBidsRejectedViewC")
         }
     }
    
    func getSBodyWorkTenderVC() -> SBodyWorkTenderViewC {
         if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SBodyWorkTenderViewC.className) as? SBodyWorkTenderViewC {
             return tenderVC
         } else {
             fatalError("Not able to initialize SBodyWorkTenderViewC")
         }
     }
    
    func getSBodyWorkTenderDetailsVC() -> SBodyWorkTenderDetailsViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SBodyWorkTenderDetailsViewC.className) as? SBodyWorkTenderDetailsViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SBodyWorkTenderDetailsViewC")
        }
    }
    
    func getSBodyWorkBidSubmittedVC() -> SBodyWorkBidSubmittedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SBodyWorkBidSubmittedViewC.className) as? SBodyWorkBidSubmittedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SBodyWorkBidSubmittedViewC")
        }
    }
    
    func getSBodyWorkBidsRejectedVC() -> SBodyWorkBidsRejectedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SBodyWorkBidsRejectedViewC.className) as? SBodyWorkBidsRejectedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SBodyWorkBidsRejectedViewC")
        }
    }
    
    func getSBodyWorkTenderWonVC() -> SBodyWorkTenderWonViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SBodyWorkTenderWonViewC.className) as? SBodyWorkTenderWonViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SBodyWorkTenderWonViewC")
        }
    }
    
    func getSBodyWorkJobCompletedVC() -> SBodyWorkJobCompletedViewC {
        if let tenderVC = self.getViewControler(storyBoard: .SupplierTenders, indentifier: SBodyWorkJobCompletedViewC.className) as? SBodyWorkJobCompletedViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize SBodyWorkJobCompletedViewC")
        }
    }
        
    func getImageViewerVC() -> ImageViewerViewC {
        if let imageViewerVC = self.getViewControler(storyBoard: .Main, indentifier: ImageViewerViewC.className) as? ImageViewerViewC {
            return imageViewerVC
        } else {
            fatalError("Not able to initialize ImageViewerViewC")
        }
    }
    
    func getServicingRequestViewC() -> ServicingRequestViewC {
        if let tenderVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: ServicingRequestViewC.className) as? ServicingRequestViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize ServicingRequestViewC")
        }
    }
        
    func getDLiveAuctionRoomVC() -> DLiveAuctionRoomViewC {
        if let dLiveAuctionRoomVC = self.getViewControler(storyBoard: .DealerAuction, indentifier: DLiveAuctionRoomViewC.className) as? DLiveAuctionRoomViewC {
            dLiveAuctionRoomVC.viewModel = DLiveAuctionRoomViewM()
            return dLiveAuctionRoomVC
        } else {
            fatalError("Not able to initialize DLiveAuctionRoomViewC")
        }
    }
    
    func getServicingTenderListViewC() -> ServicingTenderListViewC {
        if let tenderVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: ServicingTenderListViewC.className) as? ServicingTenderListViewC {
            return tenderVC
        } else {
            fatalError("Not able to initialize ServicingTenderListViewC")
        }
    }
    
    func getServicingBidListVC() -> ServicingBidListVC {
        if let tenderVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: ServicingBidListVC.className) as? ServicingBidListVC {
            return tenderVC
        } else {
            fatalError("Not able to initialize ServicingBidListVC")
        }
    }
    
    func getServicingBidAcceptedVC() -> ServicingBidAcceptedVC {
        if let tenderVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: ServicingBidAcceptedVC.className) as? ServicingBidAcceptedVC {
            return tenderVC
        } else {
            fatalError("Not able to initialize ServicingBidAcceptedVC")
        }
    }
    
    func getPTenderListVC() -> PTenderListViewC {
        if let pTenderListVC = self.getViewControler(storyBoard: .SmartTenders, indentifier: PTenderListViewC.className) as? PTenderListViewC {
            pTenderListVC.viewModel = AllTendersViewM()
            return pTenderListVC
        } else {
            fatalError("Not able to initialize PTenderListViewC")
        }
    }
    
    func getPTenderRequestVC() -> PTenderRequestViewC {
        if let pTenderRequestVC = self.getViewControler(storyBoard: .SmartTenders, indentifier: PTenderRequestViewC.className) as? PTenderRequestViewC {
            pTenderRequestVC.viewModel = PTenderRequestViewM()
            return pTenderRequestVC
        } else {
            fatalError("Not able to initialize PTenderRequestViewC")
        }
    }
    
    func getRimsRequestVC() -> RimsRequestViewC {
        if let rimsTenderReqVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: RimsRequestViewC.className) as? RimsRequestViewC {
            rimsTenderReqVC.viewModel = RimsRequestViewM()
            return rimsTenderReqVC
        } else {
            fatalError("Not able to initialize RimsRequestViewC")
        }
    }
    
    func getTyresRequestVC() -> TyresRequestViewC {
        if let tyresTenderReqVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: TyresRequestViewC.className) as? TyresRequestViewC {
            tyresTenderReqVC.viewModel = TyresRequestViewM()
            return tyresTenderReqVC
        } else {
            fatalError("Not able to initialize TyresRequestViewC")
        }
    }
    
    func getWTenderListVC() -> WTenderListViewC {
        if let wTenderListVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: WTenderListViewC.className) as? WTenderListViewC {
            return wTenderListVC
        } else {
            fatalError("Not able to initialize WTenderListViewC")
        }
    }
    
    func getWBidListVC() -> WBidListViewC {
        if let wBidListVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: WBidListViewC.className) as? WBidListViewC {
            return wBidListVC
        } else {
            fatalError("Not able to initialize WBidListViewC")
        }
    }

    func getWBidAcceptedVC() -> WBidAcceptedViewC {
        if let wBidAcceptedVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: WBidAcceptedViewC.className) as? WBidAcceptedViewC {
            return wBidAcceptedVC
        } else {
            fatalError("Not able to initialize WBidAcceptedViewC")
        }
    }
    
    func getWTTenderReqVC() -> WTTenderReqViewC {
           if let tyresTenderReqVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: WTTenderReqViewC.className) as? WTTenderReqViewC {
               
               return tyresTenderReqVC
           } else {
               fatalError("Not able to initialize WTTenderReqViewC")
           }
       }
       
       func getWTTenderListVC() -> WTTenderListViewC {
           if let wTTenderListVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: WTTenderListViewC.className) as? WTTenderListViewC {
               return wTTenderListVC
           } else {
               fatalError("Not able to initialize WTTenderListViewC")
           }
       }
       
       func getWTBidListVC() -> WTBidListViewC {
           if let wTBidListVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: WTBidListViewC.className) as? WTBidListViewC {
               return wTBidListVC
           } else {
               fatalError("Not able to initialize WTBidListViewC")
           }
       }

       func getWTBidAcceptedVC() -> WTBidAcceptedViewC {
           if let wTBidAcceptedVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: WTBidAcceptedViewC.className) as? WTBidAcceptedViewC {
               return wTBidAcceptedVC
           } else {
               fatalError("Not able to initialize WTBidAcceptedViewC")
           }
       }
 
    
    func getPTenderBidListVC() -> PTenderBidListViewC {
        if let pTenderBidListVC = self.getViewControler(storyBoard: .SmartTenders, indentifier: PTenderBidListViewC.className) as? PTenderBidListViewC {
            pTenderBidListVC.viewModel = PTenderBidListViewM()
            return pTenderBidListVC
        } else {
            fatalError("Not able to initialize PTenderBidListViewC")
        }
    }
    
    func getPTenderBidAcceptedVC() -> PTenderBidAcceptedViewC {
        if let pTenderBidAcceptedVC = self.getViewControler(storyBoard: .SmartTenders, indentifier: PTenderBidAcceptedViewC.className) as? PTenderBidAcceptedViewC {
            pTenderBidAcceptedVC.viewModel = PTenderBidAcceptedViewM()
            return pTenderBidAcceptedVC
        } else {
            fatalError("Not able to initialize PTenderBidAcceptedViewC")
        }
    }
    
    func getBVTenderListVC() -> BVTenderListViewC {
        if let bvTenderListVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: BVTenderListViewC.className) as? BVTenderListViewC {
            return bvTenderListVC
        } else {
            fatalError("Not able to initialize BVTenderListViewC")
        }
    }
    
    func getBVBidListVC() -> BVBidListViewC {
        if let bvBidListVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: BVBidListViewC.className) as? BVBidListViewC {
            return bvBidListVC
        } else {
            fatalError("Not able to initialize BVBidListViewC")
        }
    }
    
    func getBVBidAcceptedVC() -> BVBidAcceptedViewC {
        if let bvBidAcceptedVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: BVBidAcceptedViewC.className) as? BVBidAcceptedViewC {
            return bvBidAcceptedVC
        } else {
            fatalError("Not able to initialize BVBidAcceptedViewC")
        }
    }
    
    func getBVUploadCarImagesVC() -> BVUploadImgReqViewC {
        if let bvUploadCarImagesVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: BVUploadImgReqViewC.className) as? BVUploadImgReqViewC {
            return bvUploadCarImagesVC
        } else {
            fatalError("Not able to initialize BVUploadImgReqViewC")
        }
    }
    
      func getBVTenderRequestVC() -> BVTenderRequestViewC {
          if let bvTenderRequestVC = self.getViewControler(storyBoard: .CustomerRecovery, indentifier: BVTenderRequestViewC.className) as? BVTenderRequestViewC {
              return bvTenderRequestVC
          } else {
              fatalError("Not able to initialize BVTenderRequestViewC")
          }
      }
    
    func getQWRequestViewC() -> QWTenderRequestViewC {
        if let qWRequestViewC = self.getViewControler(storyBoard: .SmartTenders, indentifier: QWTenderRequestViewC.className) as? QWTenderRequestViewC {
            return qWRequestViewC
        } else {
            fatalError("Not able to initialize QWTenderRequestViewC")
        }
    }
    
    func getCDDRequestViewC() -> CDDTenderRequestViewC {
        if let cddRequestViewC = self.getViewControler(storyBoard: .SmartTenders, indentifier: CDDTenderRequestViewC.className) as? CDDTenderRequestViewC {
            return cddRequestViewC
        } else {
            fatalError("Not able to initialize CDDTenderRequestViewC")
        }
    }
    
    func getCDTenderListViewC() -> CDTenderListViewC {
          if let cdTenderListViewC = self.getViewControler(storyBoard: .SmartTenders, indentifier: CDTenderListViewC.className) as? CDTenderListViewC {
              return cdTenderListViewC
          } else {
              fatalError("Not able to initialize CDTenderListViewC")
          }
      }
    
    func getCDBidListViewC() -> CDBidListViewC {
        if let cdBidListViewC = self.getViewControler(storyBoard: .SmartTenders, indentifier: CDBidListViewC.className) as? CDBidListViewC {
            return cdBidListViewC
        } else {
            fatalError("Not able to initialize CDBidListViewC")
        }
    }
    
    func getCDBidAcceptedViewC() -> CDBidAcceptedViewC {
        if let cdBidAcceptedViewC = self.getViewControler(storyBoard: .SmartTenders, indentifier: CDBidAcceptedViewC.className) as? CDBidAcceptedViewC {
            return cdBidAcceptedViewC
        } else {
            fatalError("Not able to initialize CDBidAcceptedViewC")
        }
    }
    
    func getCDOptionsViewC() -> CDOptionsViewC {
        if let cdOptionsViewC = self.getViewControler(storyBoard: .SmartTenders, indentifier: CDOptionsViewC.className) as? CDOptionsViewC {
            return cdOptionsViewC
        } else {
            fatalError("Not able to initialize CDOptionsViewC")
        }
    }

    func getBWTenderListVC() -> BWTenderListViewC {
        if let bwTenderListVC = self.getViewControler(storyBoard: .CustomerMain, indentifier: BWTenderListViewC.className) as? BWTenderListViewC {
            return bwTenderListVC
        } else {
            fatalError("Not able to initialize BWTenderListViewC")
        }
    }
    
    func getPPTenderRequestVC() -> PPTenderRequestViewC {
        if let tenderRequestVC = self.getViewControler(storyBoard: .CustomerMain, indentifier: PPTenderRequestViewC.className) as? PPTenderRequestViewC {
            return tenderRequestVC
        } else {
            fatalError("Not able to initialize PPTenderRequestViewC")
        }
    }
    
    func getPPTenderDentRequestVC() -> PPTenderDentRequestViewC {
        if let tenderRequestVC = self.getViewControler(storyBoard: .CustomerMain, indentifier: PPTenderDentRequestViewC.className) as? PPTenderDentRequestViewC {
            return tenderRequestVC
        } else {
            fatalError("Not able to initialize PPTenderDentRequestViewC")
        }
    }
    
    func getPPTenderRequestUploadImageVC() -> PPTenderRequestUploadImageViewC {
        if let tenderRequestVC = self.getViewControler(storyBoard: .CustomerMain, indentifier: PPTenderRequestUploadImageViewC.className) as? PPTenderRequestUploadImageViewC {
            return tenderRequestVC
        } else {
            fatalError("Not able to initialize PPTenderRequestUploadImageViewC")
        }
    }
    
    func getITenderRequestVC() -> ITenderRequestViewC {
         if let tenderRequestVC = self.getViewControler(storyBoard: .SmartTenders, indentifier: ITenderRequestViewC.className) as? ITenderRequestViewC {
             return tenderRequestVC
         } else {
             fatalError("Not able to initialize ITenderRequestViewC")
         }
     }
    
    func getITenderListVC() -> ITenderListViewC {
        if let tenderListVC = self.getViewControler(storyBoard: .SmartTenders, indentifier: ITenderListViewC.className) as? ITenderListViewC {
            return tenderListVC
        } else {
            fatalError("Not able to initialize ITenderListViewC")
        }
    }
    
    func getIBidListVC() -> IBidListViewC {
        if let bidListVC = self.getViewControler(storyBoard: .SmartTenders, indentifier: IBidListViewC.className) as? IBidListViewC {
            return bidListVC
        } else {
            fatalError("Not able to initialize IBidListViewC")
        }
    }
    
    func getIBidAcceptedVC() -> IBidAcceptedViewC {
        if let bidAcceptedVC = self.getViewControler(storyBoard: .SmartTenders, indentifier: IBidAcceptedViewC.className) as? IBidAcceptedViewC {
            return bidAcceptedVC
        } else {
            fatalError("Not able to initialize IBidAcceptedViewC")
        }
    }

    
    
    func getCPPTenderRequestVC() -> CPPTenderRequestViewC {
        if let tenderRequestVC = self.getViewControler(storyBoard: .CustomerMain, indentifier: CPPTenderRequestViewC.className) as? CPPTenderRequestViewC {
            return tenderRequestVC
        } else {
            fatalError("Not able to initialize CPPTenderRequestViewC")
        }
    }
    
    func getBWBidListVC() -> BWBidListViewC {
        if let bidListVC = self.getViewControler(storyBoard: .SmartTenders, indentifier: BWBidListViewC.className) as? BWBidListViewC {
            return bidListVC
        } else {
            fatalError("Not able to initialize BWBidListViewC")
        }
    }
    
    func getBWBidAcceptedVC() -> BWBidAcceptedViewC {
        if let bidAcceptedVC = self.getViewControler(storyBoard: .SmartTenders, indentifier: BWBidAcceptedViewC.className) as? BWBidAcceptedViewC {
            return bidAcceptedVC
        } else {
            fatalError("Not able to initialize BWBidAcceptedViewC")
        }
    }
    
    func getIUploadCarImagesVC() -> IUploadImagesViewC {
        if let iUploadCarImagesVC = self.getViewControler(storyBoard: .SmartTenders, indentifier: IUploadImagesViewC.className) as? IUploadImagesViewC {
            return iUploadCarImagesVC
        } else {
            fatalError("Not able to initialize IUploadImagesViewC")
        }
    }
    
    // Mark:- PDFReaderVC
       func getPDFReaderVC() -> PDFReaderViewController {
           if let pdfReaderVC = self.getViewControler(storyBoard: .CustomerCar, indentifier: PDFReaderViewController.className) as? PDFReaderViewController {
               return pdfReaderVC
           } else {
               fatalError("Not able to initialize PDFReaderViewController")
           }
       }
    
}
