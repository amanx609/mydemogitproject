//
//  BaseTableViewCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}


struct CellInfo {
  
  var cellType: CellType!
  var placeHolder: String!
  var value: String!
  var height: CGFloat!
  var info: [String: AnyObject]?
  var isSecureText: Bool!
  var returnKeyType: UIReturnKeyType!
  var keyboardType: UIKeyboardType!
  var capitalizationType: UITextAutocapitalizationType!
  var isUserInteractionEnabled: Bool!
    
    
  init(cellType: CellType, placeHolder: String = "", value: String = "", info:[String: AnyObject]? = nil, isSecureText: Bool = false, height: CGFloat = UITableView.automaticDimension, returnKeyType: UIReturnKeyType = .done, keyboardType: UIKeyboardType = .default, capitalizationType: UITextAutocapitalizationType = .none, placeHolderColor: UIColor = UIColor.black, isUserInteractionEnabled: Bool = true) {
    self.cellType = cellType
    self.placeHolder = placeHolder
    self.value = value
    self.placeHolder = placeHolder
    self.height = height
    self.info = info
    self.isSecureText = isSecureText
    self.returnKeyType = returnKeyType
    self.keyboardType = keyboardType
    self.capitalizationType = capitalizationType
    self.isUserInteractionEnabled = isUserInteractionEnabled
  }
}

enum CellType {
  case SignupButtonCell
  case TextInputCell
  case BottomLabelCell
  case PhoneCell
  case DropDownCell
  case BottomButtonCell
  case TermsConditionsCell
  case ForgotPasswordButtonCell
  case BottomLabelRegularTextCell
  case MyCarsCell
  case ChooseBrandsTableCell
  case SelectYearTableCell
  case SelectMileageTableCell
  case SelectPriceTableCell
  case LabelSwitchCell
  case LabelDropDownCell
  case MileageTextFieldCell
  case SideMenuCell
  case ContactusCell
  case VideoCell
  case DocumentDetailsTableCell
  case DocumentDetailsHeaderCell
  case FinanceSectionCell
  case TextViewCell
  case HomeHeaderCollectionCell
  case HomeCollectionCell
  case CarInfoTableCell
  case CarFinanceTableCell
  case CarDescriptionTableCell
  case DocumentsRequiredTableCell
  case CarContactUsTableCell
  case DocumentListCell
  case SpecificationOptionsTableCell
  case ExtraServicesCollectionCell
  case SChooseServiceCollectionCell
  case SChooseServiceHeaderCollectionCell
  case CarAttributesTableCell
  case ExtrasTableCell
  case ApplicationsTableViewCell
  case PaymentCell
  case CardInfoCell
  case DateRangeCell
  case MyTransactionDetailCell
  case TwoRadioButtonCell
  case DropDownQuestionCell
  case ThreeRadioButtonCell
  case AnswerCell
  case PaymentFilterCollectionCell
  case PaymentCollectionTableViewCell
  case BuyCarsTableCell
  case PriceRangeTextFieldCell
  case MileageRangeCell
  case DAuctionAddCarInputCell
  case DAuctionAddCarDropDownCell
  case DAuctionUploadCarImageCollectionCell
  case TyresConditionCell
  case DAuctionUploadHeaderCollectionCell
  case DAuctionUploadFooterCollectionCell
  case DSavedAuctionCell
  case NoReserveCell
  case DAddAuctionImageCell
  case UploadCarImages
  case AuctionListTableCell
  case AuctionRoomCollectionCell
  case SPaymentHistoryCell
  case PaymentHistoryTableCell
  case OffersAndDealsCell
  case LeftRightLabelCell
  case ItemsCoveredCell
  case ItemsCoveredCollectionCell
  case PackageWarrantyTableCell
  case headerCollectionCell
  case ServiceHistoryUploadDocumentCell
  case TenderStatusDetailCell
  case STCarDetailCell
  case STBidsCountCell
  case RDMapCell
  case RDContactInfoCell
  case RDBidListCell
  case LocationTextFieldsCell
  case ImageDropDownCell
  case VITenderListCell
  case BuyLeadCell
  case LeadsBuyCell
  case LeadsSellCell
  case MyLeadsSellCell
  case MyLeadsBuyCell
  case LeadAddedCell
  case MaxBudgetCell
  case BidDetailCell
  case VIContactInfoCell
  case SPDetailCell
  case USTenderListCell
  case CarImagesTableCell
  case USDescriptonCell
  case ActiveTenderCell
  case MyTenderCell
  case TenderDetailsTitleCell
  case TenderCarDetailsCell
  case TenderDetailsAttributedCell
  case LocationCell
  case UploadVehicleImagesCell
  case DashedTextFieldCell
  case AdditionalInfoCell
  case DaysRequiredCell
  case JobCell
  case RecoveryTypeCell
  case BidAmountCell
  case EnterAdditionalInfoCell
  case EnterBidDetailsCell
  case RDDriverInfoCell
  case TenderWonInfoCell
  case TenderCompletedCell
  case VITenderLocationCell
  case ThreeImageCell
  case LeaderboardFirstRankCell
  case LeaderboardListCell
  case SServiceDetailsCell
  case DLAMultipleCarImagesCell
  case DLACarInfoCell
  case DLABidInfoCell
  case DLASectionHeaderCell
  case ChooseServiceOptionCell
  case DLACarDetailCell
  case ServicingTenderListCell
  case DLAOtherInfoCell
  case ServiceDetailCell
  case FourRadioButtonCell
  case WheelSizeInfoCell
  case WServiceDetailsCell
  case SparePartInfoCell
  case WTBidListCell
  case WTServiceInfoCell
  case BVDocumentImagesCell
  case CarDetailServiceTypeCell
  case CDOptionsCell
  case BVCarImagesCell
  case DentDescriptionCell
  case InsuranceDetailsCell
  case InsuranceDateCell
  case InsuranceTypeCell
  case DeliveryLocationCell
  case DChooseAuctionTypeCell
  case MyCarDetailsCell
    
}
