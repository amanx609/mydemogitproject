//
//  BaseCodable.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 30/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

protocol BaseCodable: Codable {
  init?(jsonData: Data)
}

extension BaseCodable {
  init?(jsonData: Data) {
    let jsonDecoder = JSONDecoder()
    do {
      self = try jsonDecoder.decode(Self.self, from: jsonData)
    } catch let error {
        print(error)
      return nil
    }
  }
}
