//
//  UserSession.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

class UserSession: NSObject {
  
  private static let _sharedInstance: UserSession = UserSession()
  static var shared: UserSession {
    return _sharedInstance
  }
  
  var userId: Int?
  var userType: UserType?
  var name: String?
  var email: String?
  var image: String?
  var mobile: String?
  var dob: String?
  var mobileWithCode: String?
  var countryCode: String?
  var socialAccount = false
  
  //MARK: - Initialization Method
  private override init() {
    
  }
  
  func initializeValuesForUser() {
    //User
    if let user = UserDefaultsManager.sharedInstance.getUserValueFor(key: .user) {
      //UserId
      if let userID = user.id {
        self.userId = userID
      }
      //UserType
      if let userTypeValue = user.userType, let userType = UserType(rawValue: userTypeValue) {
        self.userType = userType
      }
      //Name
      if let userName = user.name {
        self.name = userName
      }
      //Email
      if let userEmail = user.email {
        self.email = userEmail
      }
      //Image
      if let userImage = user.image {
        self.image = userImage
      }
      //phone
      if let mobile = user.mobile {
        self.mobile = mobile
      }
      //dob
      if let dob = user.dob {
        self.dob = dob
      }
      //MobileWithCountryCode
      if let mobileWC = user.mobileWithCode {
        self.mobileWithCode = mobileWC
      }
      //CountryCode
      if let countryC = user.countryCode {
        self.countryCode = countryC
      }
      
    }
  }
  
  func isLoggedIn() -> Bool {
    if let _ = UserDefaultsManager.sharedInstance.getIntValueFor(key: .userId) {
      return true
    } else {
      return false
    }
  }
  
  func getAccessToken() -> String {
    if let token = UserDefaultsManager.sharedInstance.getStringValueFor(key: .token) {
      return token
    } else {
      return ""
    }
  }
  
  func getDeviceToken() -> String {
    if let deviceToken = UserDefaultsManager.sharedInstance.getStringValueFor(key: .deviceToken) {
      return deviceToken
    } else {
      return "12345"
    }
  }
  
  func getUsertype() -> Int {
    if let userType = self.userType?.rawValue {
      return userType
    } else if let savedUserType = UserDefaultsManager.sharedInstance.getIntValueFor(key: .userType) {
      return savedUserType
    } else {
      return 0
    }
  }
  
  func removeData() {
    self.userId = nil
    self.userType = nil
    self.name = nil
    self.email = nil
    self.image = nil
    UserDefaultsManager.sharedInstance.removeAllValues()
  }
  
  func getUserId() -> Int {
    if let user = UserDefaultsManager.sharedInstance.getUserValueFor(key: .user), let id = user.id {
      return id
    }
    return 0
  }
  
  func getSelectedServiceList() -> [Int]? {
    if let selectedServiceList = UserDefaultsManager.sharedInstance.getIntArrayValueFor(key: .selectedServiceList) {
      return selectedServiceList
    }
    return nil
  }
  
}


