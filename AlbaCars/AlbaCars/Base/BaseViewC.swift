//
//  BaseViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import UIKit

enum UINavigationBarButtonType: Int {
    case back
    case save
    case cross
    case hamburgerWhite
    case hamburgerBlack
    case filter
    case backWhite
    case filterWhite
    case notification
    case reset
    case apply
    
    var iconImage: UIImage? {
        switch self {
        case .back: return #imageLiteral(resourceName: "back")
        case .cross: return #imageLiteral(resourceName: "cross")
        case .hamburgerWhite: return #imageLiteral(resourceName: "menu")
        case .hamburgerBlack: return #imageLiteral(resourceName: "hamburger_black")
        case .backWhite: return #imageLiteral(resourceName: "backWhite")
        case .notification: return #imageLiteral(resourceName: "notification")
        default: return nil
        }
    }
    
    var title: String? {
        switch self {
        case .filter, .filterWhite: return "Filter".localizedString()
        case .reset: return "Reset".localizedString()
        case .apply: return "Apply".localizedString()
        default: return nil
        }
    }
}

protocol BaseViewCDelegate {
    func navigationBarButtonDidTap(_ buttonType: UINavigationBarButtonType)
}


class BaseViewC: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var navigationTitleLabel: UILabel!
    
    //MARK: - Variables
    var baseDelegate: BaseViewCDelegate?
    private enum NavBarConstants {
        static let navButtonWidth: CGFloat = 35.0
        static let edgeInset: CGFloat = 10.0
        static let titleButtonWidth: CGFloat = 55.0
        static let titleButtonHeight: CGFloat = 25.0
    }
    
    // MARK: - LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    deinit {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.baseDelegate = nil
    }
    // MARK:- Private methods
    
    private func setup() {
        if navigationTitleLabel != nil {
            navigationTitleLabel.font = UIFont.font(name: .AirbnbCerealApp, weight: .Bold, size: .size_18)
        }
        
    }
    
    //MARK: - Public Methods
    func setupNavigationBarTitle(title: String, barColor: UIColor = .white, titleColor: UIColor = .black, leftBarButtonsType: [UINavigationBarButtonType], rightBarButtonsType: [UINavigationBarButtonType]) {
        
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = barColor
        self.navigationController?.navigationBar.backgroundColor = barColor
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: titleColor]
        
        if !title.isEmpty {
            self.navigationItem.title = title
        }
        var rightBarButtonItems = [UIBarButtonItem]()
        for rightButtonType in rightBarButtonsType {
            let rightButtonItem = getBarButtonItem(for: rightButtonType, isLeftBarButtonItem: false)
            rightBarButtonItems.append(rightButtonItem)
        }
        if rightBarButtonItems.count > 0 {
            self.navigationItem.rightBarButtonItems = rightBarButtonItems
        }
        var leftBarButtonItems = [UIBarButtonItem]()
        for leftButtonType in leftBarButtonsType {
            let leftButtonItem = getBarButtonItem(for: leftButtonType, isLeftBarButtonItem: true)
            leftBarButtonItems.append(leftButtonItem)
        }
        if leftBarButtonItems.count > 0 {
            self.navigationItem.leftBarButtonItems = leftBarButtonItems
        }
    }
    
    func getBarButtonItem(for type: UINavigationBarButtonType, isLeftBarButtonItem: Bool) -> UIBarButtonItem {
        var barButtonHeight = 40
        if let height = self.navigationController?.navigationBar.frame.size.height {
            barButtonHeight = Int(height)
        }
        let button = UIButton(frame: CGRect(x: 30, y: 0, width: Int(NavBarConstants.navButtonWidth), height: barButtonHeight))
        //SetAttributes
        button.titleLabel?.font = UIFont.font(name: .AirbnbCerealApp, weight: .Bold, size: .size_10)
        button.titleLabel?.textAlignment = .left
        button.tag = type.rawValue
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: isLeftBarButtonItem ? -NavBarConstants.edgeInset : NavBarConstants.edgeInset, bottom: 0, right: isLeftBarButtonItem ? NavBarConstants.edgeInset : -NavBarConstants.edgeInset)
        button.frame.size.width = Constants.Devices.ScreenHeight == 568.0 ? 40.0 : 60.0
        button.addTarget(self, action: #selector(BaseViewC.navigationButtonTapped(_:)), for: .touchUpInside)
        //SetImage
        if let iconImage = type.iconImage {
            button.imageView?.contentMode = .scaleAspectFit
            button.setImage(iconImage, for: UIControl.State())
            return UIBarButtonItem(customView: button)
        }
        //SetTitle
        if let title = type.title {
            button.setTitle(title, for: UIControl.State())
            if type == .filter {
                button.setTitleColor(UIColor.white, for: .normal)
                button.backgroundColor = UIColor.redButtonColor
                button.roundCorners(Constants.UIConstants.sizeRadius_3half)
                let barButton = UIBarButtonItem(customView: button)
                let currHeight = barButton.customView?.heightAnchor.constraint(equalToConstant: NavBarConstants.titleButtonHeight)
                currHeight?.isActive = true
                let currWidth = barButton.customView?.widthAnchor.constraint(equalToConstant: NavBarConstants.titleButtonWidth)
                currWidth?.isActive = true
                return barButton
            } else if type == .filterWhite {
                button.setTitleColor(UIColor.redButtonColor, for: .normal)
                button.backgroundColor = UIColor.white
                button.roundCorners(Constants.UIConstants.sizeRadius_3half)
                button.titleLabel?.font = UIFont.font(name: .AirbnbCerealApp, weight: .Medium, size: .size_10)
                let barButton = UIBarButtonItem(customView: button)
                let currHeight = barButton.customView?.heightAnchor.constraint(equalToConstant: NavBarConstants.titleButtonHeight)
                currHeight?.isActive = true
                let currWidth = barButton.customView?.widthAnchor.constraint(equalToConstant: NavBarConstants.titleButtonWidth)
                currWidth?.isActive = true
                return barButton
            } else if type == .reset {
                button.setTitleColor(UIColor.white, for: .normal)
                button.backgroundColor = UIColor.redButtonColor
                button.roundCorners(Constants.UIConstants.sizeRadius_3half)
                let barButton = UIBarButtonItem(customView: button)
                let currHeight = barButton.customView?.heightAnchor.constraint(equalToConstant: NavBarConstants.titleButtonHeight)
                currHeight?.isActive = true
                let currWidth = barButton.customView?.widthAnchor.constraint(equalToConstant: NavBarConstants.titleButtonWidth)
                currWidth?.isActive = true
                return barButton
            } else if type == .apply {
                button.setTitleColor(UIColor.white, for: .normal)
                button.backgroundColor = UIColor.redButtonColor
                button.roundCorners(Constants.UIConstants.sizeRadius_3half)
                let barButton = UIBarButtonItem(customView: button)
                let currHeight = barButton.customView?.heightAnchor.constraint(equalToConstant: NavBarConstants.titleButtonHeight)
                currHeight?.isActive = true
                let currWidth = barButton.customView?.widthAnchor.constraint(equalToConstant: NavBarConstants.titleButtonWidth)
                currWidth?.isActive = true
                return barButton
            }
        }
        return UIBarButtonItem()
    }
    
    @objc func navigationButtonTapped(_ sender: AnyObject) {
        guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
        switch buttonType {
        case .back, .backWhite: backButtonTapped()
        case .hamburgerWhite: hamburgerButtonTapped(sender)
        case .hamburgerBlack: hamburgerButtonTapped(sender)
        case .cross: crossButtonTapped()
        case .filterWhite: filterButtonTapped()
           
        default: break
        }
        self.baseDelegate?.navigationBarButtonDidTap(buttonType)
    }
    
    func filterButtonTapped() {
        
    }
    
    func crossButtonTapped() {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func hamburgerButtonTapped(_ sender: AnyObject) {
        self.showLeftViewAnimated(sender)
    }
    
    func backButtonTapped() {
        if self.navigationController!.viewControllers.count > 1 {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    // MARK: - IBActions
    
    @IBAction func backButtonTapped(_ sender: Any) {
        if (self.navigationController?.viewControllers.count ?? 0)>1 {
            self.navigationController?.popViewController(animated: true)
        } else {
            dismissKeyboard()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
extension CAShapeLayer {
    func drawCircleAtLocation(location: CGPoint, withRadius radius: CGFloat, andColor color: UIColor, filled: Bool) {
        fillColor = filled ? color.cgColor : UIColor.white.cgColor
        strokeColor = color.cgColor
        let origin = CGPoint(x: location.x - radius, y: location.y - radius)
        path = UIBezierPath(ovalIn: CGRect(origin: origin, size: CGSize(width: radius * 2, height: radius * 2))).cgPath
    }
}

private var handle: UInt8 = 0

extension UIBarButtonItem {
    private var badgeLayer: CAShapeLayer? {
        if let b: AnyObject = objc_getAssociatedObject(self, &handle) as AnyObject? {
            return b as? CAShapeLayer
        } else {
            return nil
        }
    }

    func addBadge(number: Int, withOffset offset: CGPoint = CGPoint.zero, andColor color: UIColor = UIColor.red, andFilled filled: Bool = true) {
        guard let view = self.value(forKey: "view") as? UIView else { return }

        badgeLayer?.removeFromSuperlayer()

        // Initialize Badge
        let badge = CAShapeLayer()
        let radius = CGFloat(7)
        let location = CGPoint(x: view.frame.width - (radius + offset.x), y: (radius + offset.y))
        badge.drawCircleAtLocation(location: location, withRadius: radius, andColor: color, filled: filled)
        view.layer.addSublayer(badge)

        // Initialiaze Badge's label
        let label = CATextLayer()
        label.string = "\(number)"
        label.alignmentMode = CATextLayerAlignmentMode.center
        label.fontSize = 11
        label.frame = CGRect(origin: CGPoint(x: location.x - 4, y: offset.y), size: CGSize(width: 8, height: 16))
        label.foregroundColor = filled ? UIColor.white.cgColor : color.cgColor
        label.backgroundColor = UIColor.clear.cgColor
        label.contentsScale = UIScreen.main.scale
        badge.addSublayer(label)

        // Save Badge as UIBarButtonItem property
        objc_setAssociatedObject(self, &handle, badge, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }

    func updateBadge(number: Int) {
        if let text = badgeLayer?.sublayers?.filter({ $0 is CATextLayer }).first as? CATextLayer {
            text.string = "\(number)"
        }
    }

    func removeBadge() {
        badgeLayer?.removeFromSuperlayer()
    }
}

