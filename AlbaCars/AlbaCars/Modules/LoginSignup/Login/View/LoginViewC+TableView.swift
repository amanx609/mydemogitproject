//
//  LoginViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension LoginViewC: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.loginDataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellInfo = self.loginDataSource[indexPath.row]
    return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return self.loginDataSource[indexPath.row].height
  }
}

extension LoginViewC {
  func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
    guard let cellType = cellInfo.cellType else { return UITableViewCell() }
    switch cellType {
    case .TextInputCell:
      let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate = self
      cell.configureView(cellInfo: cellInfo)
      return cell
    case .BottomLabelCell:
      let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate = self
      cell.configureView(cellInfo: cellInfo)
      return cell
    case .BottomLabelRegularTextCell:
      let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.configureViewWithLightText(cellInfo: cellInfo)
      return cell
    case .BottomButtonCell:
      let cell: BottomButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate = self
      cell.configureView(cellInfo: cellInfo)
      return cell
    case .SignupButtonCell:
      let cell: SignupButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate = self
      cell.configureView(cellInfo: cellInfo)
      return cell
    case .ForgotPasswordButtonCell:
      let cell: ForgotPasswordButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate = self
      cell.configureView(cellInfo: cellInfo)
      return cell
    default:
      return UITableViewCell()
    }
  }
}

//MARK: - TextInputCellDelegate
extension LoginViewC: TextInputCellDelegate {
  func tapNextKeyboard(cell: TextInputCell) {
    self.view.endEditing(true)
  }
  
  func didChangeText(cell: TextInputCell, text: String) {
    guard let indexPath = self.loginTableView.indexPath(for: cell) else { return }
    self.loginDataSource[indexPath.row].value = text
  }
}

//MARK: - ForgotPasswordCellDelegate
extension LoginViewC: ForgotPasswordCellDelegate {
  func didTapForgotPasswordButton(cell: ForgotPasswordButtonCell) {
    self.view.endEditing(true)
    let forgotPasswordVC = DIConfigurator.sharedInstance.getForgotPasswordVC()
    self.navigationController?.pushViewController(forgotPasswordVC, animated: true)
  }
}

//MARK: - BottomButtonCellDelegate
extension LoginViewC: BottomButtonCellDelegate {
  func didTapBottomButton(cell: BottomButtonCell) {
    self.view.endEditing(true)
    self.requestSignInAPI()
  }
}

//MARK: - SignupButtonCellDelegate
extension LoginViewC: SignupButtonCellDelegate {
  func didTapSignupButton(cell: SignupButtonCell) {
    guard let indexPath = self.loginTableView.indexPath(for: cell) else { return }
    let cellInfo = self.loginDataSource[indexPath.row]
    if let info = cellInfo.info,
      let buttonTypeValue = info[Constants.UIKeys.buttonType] as? String,
      let socialType = SocialType(rawValue: buttonTypeValue) {
      switch socialType {
      case .google:
        GoogleManager.sharedInstance.loginWithGoogle { (socialUser) in
          guard let user = socialUser else { return }
          self.requestSocialSignInAPI(socialUser: user)
        }
      case .facebook:
        FacebookManager.sharedInstance.loginWithFacebook(readPermissions: [
        Permission.publicProfile.stringValue,
        Permission.email.stringValue,
        Permission.birthday.stringValue]) { (socialUser) in
          guard let user = socialUser else { return }
          self.requestSocialSignInAPI(socialUser: user)
        }
      default:
        return
      }
    }
  }
}

//MARK: - BottomLabelCellDelegate
extension LoginViewC: BottomLabelCellDelegate {
  func didTapLabel(cell: BottomLabelCell) {
    if let viewControllers = self.navigationController?.viewControllers {
      //CreateAccountViewCIsPresentInNavigationStack
      for viewController in viewControllers {
        if viewController is CreateAccountViewC {
          self.navigationController?.popToViewController(viewController, animated: true)
          return
        }
      }
      //CreateAccountViewCNotPresentInNavigationStack
      let createAccountVC = DIConfigurator.sharedInstance.getCreateAccountVC()
      self.navigationController?.pushViewController(createAccountVC, animated: true)
    }
  }
}


