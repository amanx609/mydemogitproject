//
//  LoginViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

class LoginViewC: BaseViewC {
  
  //MARK: - IBOutlets
  @IBOutlet weak var loginTableView: TPKeyboardAvoidingTableView!
  @IBOutlet var headerView: UIView!
  @IBOutlet weak var titleLabel: UILabel!
  
  //MARK: - Variables
  var loginDataSource: [CellInfo] = []
  var viewModel: LoginVModeling?
  
  //MARK: - LifeCycle Methods
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setup()
  }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
  
  deinit {
    print("deinit LoginViewC")
  }
  
  //MARK: - Private Methods
  private func setup() {
    self.recheckVM()
    self.setupTableView()
    if let dataSource = self.viewModel?.getLoginDataSource() {
      self.loginDataSource = dataSource
    }
  }
  
  private func recheckVM() {
    if self.viewModel == nil {
      self.viewModel = LoginViewM()
    }
  }
  
  private func setupTableView() {
    self.registerNibs()
    self.loginTableView.delegate = self
    self.loginTableView.dataSource = self
    self.loginTableView.separatorStyle = .none
    self.loginTableView.allowsSelection = false
    self.loginTableView.tableHeaderView = self.headerView
    self.loginTableView.bounces = false
  }
  
  private func registerNibs() {
    self.loginTableView.register(TextInputCell.self)
    self.loginTableView.register(BottomLabelCell.self)
    self.loginTableView.register(BottomButtonCell.self)
    self.loginTableView.register(SignupButtonCell.self)
    self.loginTableView.register(ForgotPasswordButtonCell.self)
  }
  
  private func goToNextPage(signupPageNum: Int) {
    let screenType = ScreenNameAfterLogin(rawValue: signupPageNum)
    switch screenType {
    case .otp:
      let otpViewC = DIConfigurator.sharedInstance.getOTPVerificationVC()
      self.navigationController?.pushViewController(otpViewC, animated: true)
    case .serviceType:
      let chooseServiceViewC = DIConfigurator.sharedInstance.getSChooseServiceVC()
      self.navigationController?.pushViewController(chooseServiceViewC, animated: true)
    case .documents:
      let documentViewC = DIConfigurator.sharedInstance.getUploadDocumentViewC()
      self.navigationController?.pushViewController(documentViewC, animated: true)
    case .payment:
      let registerPaymentViewC = DIConfigurator.sharedInstance.getRegisterPaymentViewC()
      self.navigationController?.pushViewController(registerPaymentViewC, animated: true)
    case .home:
      if let user = UserDefaultsManager.sharedInstance.getUserValueFor(key: .user),
        let userId = user.id {
        UserDefaultsManager.sharedInstance.saveIntValueFor(key: .userId, value: userId)
      }
      AppDelegate.delegate.showHome()
    default:
      break
    }
  }
  
  
  //MARK: - API Methods
  func requestSignInAPI() {
    self.viewModel?.requestSignInAPI(arrData: self.loginDataSource) { (success, signupPageNumber) in
      if success {
        //GoToNextPageAccordingToSignupPageNumber
        Threads.performTaskInMainQueue {
          self.goToNextPage(signupPageNum: signupPageNumber)
        }
      }
    }
  }
  
  func requestSocialSignInAPI(socialUser: SocialUser) {
    self.viewModel?.requestSocialSignInAPI(socialUser: socialUser, completion: { (success, signupPageNumber) in
      if success {
        //GoToNextPageAccordingToSignupPageNumber
        Threads.performTaskInMainQueue {
          self.goToNextPage(signupPageNum: signupPageNumber)
        }
      } else {
        Threads.performTaskInMainQueue {
          let createAccountVC = DIConfigurator.sharedInstance.getCreateAccountVC()
          createAccountVC.socialUser = socialUser
          self.navigationController?.pushViewController(createAccountVC, animated: true)
        }
      }
    })
  }
}
