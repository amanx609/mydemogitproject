//
//  LoginViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol LoginVModeling: BaseVModeling {
    func getLoginDataSource() -> [CellInfo]
    func requestSignInAPI(arrData: [CellInfo], completion: @escaping (Bool, Int)-> Void)
    func requestSocialSignInAPI(socialUser: SocialUser, completion: @escaping (Bool, Int)-> Void)
    func requestProfileAPI(completion: @escaping (Bool)-> Void)
    func getVatSettings(completion: @escaping (Bool)-> Void) 
}

class LoginViewM: LoginVModeling {
    
    func getLoginDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Email Cell
        var emailInfo = [String: AnyObject]()
        emailInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "email")
        emailInfo[Constants.UIKeys.inputType] = TextInputType.email as AnyObject
        let emailCell = CellInfo(cellType: .TextInputCell, placeHolder: "Email".localizedString(), value: "", info: emailInfo, height: Constants.CellHeightConstants.height_80)
        array.append(emailCell)
        
        //Password Cell
        var passwordInfo = [String: AnyObject]()
        passwordInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "password")
        passwordInfo[Constants.UIKeys.inputType] = TextInputType.password as AnyObject
        let passwordCell = CellInfo(cellType: .TextInputCell, placeHolder: "Password".localizedString(), value: "", info: passwordInfo, isSecureText: true, height: Constants.CellHeightConstants.height_80)
        array.append(passwordCell)
        
        //Forgot Password
        let forgotPasswordButtonCell = CellInfo(cellType: .ForgotPasswordButtonCell, placeHolder: "Forgot Password?".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_30)
        array.append(forgotPasswordButtonCell)
        
        //Login Cell
        let loginCell = CellInfo(cellType: .BottomButtonCell, placeHolder: "Login".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_80)
        array.append(loginCell)
        
        //OR Cell
        let orCell = CellInfo(cellType: .BottomLabelRegularTextCell, placeHolder: "Or", value: "", info: nil, height: Constants.CellHeightConstants.height_40)
        array.append(orCell)
        
        //Google Cell
        var googleInfo = [String: AnyObject]()
        googleInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "google")
        googleInfo[Constants.UIKeys.backgroundColor] = UIColor.white
        googleInfo[Constants.UIKeys.textColor] = UIColor.black
        googleInfo[Constants.UIKeys.buttonType] = SocialType.google.rawValue as AnyObject
        let googleCell = CellInfo(cellType: .SignupButtonCell, placeHolder: "Login Using Google".localizedString(), value: "", info: googleInfo, height: Constants.CellHeightConstants.height_80)
        array.append(googleCell)
        
        //Facebook Cell
        var facebookInfo = [String: AnyObject]()
        facebookInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "facebook")
        facebookInfo[Constants.UIKeys.backgroundColor] = UIColor.facebookButtonColor
        facebookInfo[Constants.UIKeys.textColor] = UIColor.white
        facebookInfo[Constants.UIKeys.buttonType] = SocialType.facebook.rawValue as AnyObject
        let facebookCell = CellInfo(cellType: .SignupButtonCell, placeHolder: "Login Using Facebook".localizedString(), value: "", info: facebookInfo, height: Constants.CellHeightConstants.height_80)
        array.append(facebookCell)
        
        //AlreadyHaveAccountCell
        var alreadyAccountInfo = [String: AnyObject]()
        alreadyAccountInfo[Constants.UIKeys.placeholderText] = "Don’t have an account Yet? Register Now".localizedString() as AnyObject
        alreadyAccountInfo[Constants.UIKeys.tappableText] = "Register Now".localizedString() as AnyObject
        alreadyAccountInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.center as AnyObject
        let alreadyAccountCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "", value: "", info: alreadyAccountInfo, height: Constants.CellHeightConstants.height_50)
        array.append(alreadyAccountCell)
        return array
    }
    
    //MARK: - API Methods
    func requestSignInAPI(arrData: [CellInfo], completion: @escaping (Bool, Int)-> Void) {
        if self.validateSignInData(arrData: arrData) {
            let signInParams = self.getSignInParams(arrData: arrData)
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .signin(param: signInParams))) { (response, success) in
                if success {
                    if let safeResponse =  response as? [String: AnyObject],
                        let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                        let token = result[ConstantAPIKeys.token] as? String,
                        let profile = result[ConstantAPIKeys.profile] as? [String: AnyObject],
                        let signupPageNumber = result[ConstantAPIKeys.signupPageNumber] as? Int,
                        let registrationAmount = result[ConstantAPIKeys.registrationAmount] as? Int,
                        let serviceList = result[ConstantAPIKeys.selectedServiceList] as? [Int],
                        let profileData = profile.toJSONData(),
                        let user = User(jsonData: profileData) {
                        UserDefaultsManager.sharedInstance.saveUserValueFor(key:.user, value: user)
                        UserDefaultsManager.sharedInstance.saveStringValueFor(key: .token, value: token)
                        UserDefaultsManager.sharedInstance.saveIntValueFor(key: .registrationAmount, value: registrationAmount)
                        UserDefaultsManager.sharedInstance.saveIntArrayValueFor(key: .selectedServiceList, value: serviceList)
                        completion(success, signupPageNumber)
                    }
                }
            }
        }
    }
    
    //MARK: - API Methods
    func requestProfileAPI(completion: @escaping (Bool)-> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .profile)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let profile = result[ConstantAPIKeys.profile] as? [String: AnyObject],
                    let profileData = profile.toJSONData(),
                    let user = User(jsonData: profileData) {
                    UserDefaultsManager.sharedInstance.saveUserValueFor(key:.user, value: user)
                    completion(success)
                }
            }
        }
    }
    
    func requestSocialSignInAPI(socialUser: SocialUser, completion: @escaping (Bool, Int)-> Void) {
        let socialSignInParams = self.getSocialSignInParams(socialUser: socialUser)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .socialSignin(param: socialSignInParams))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let token = result[ConstantAPIKeys.token] as? String,
                    let profile = result[ConstantAPIKeys.profile] as? [String: AnyObject],
                    let signupPageNumber = result[ConstantAPIKeys.signupPageNumber] as? Int,
                    let registrationAmount = result[ConstantAPIKeys.registrationAmount] as? Int,
                    let serviceList = result[ConstantAPIKeys.selectedServiceList] as? [Int],
                    let profileData = profile.toJSONData(),
                    let user = User(jsonData: profileData) {
                    UserDefaultsManager.sharedInstance.saveUserValueFor(key:.user, value: user)
                    UserDefaultsManager.sharedInstance.saveStringValueFor(key: .token, value: token)
                    UserDefaultsManager.sharedInstance.saveIntValueFor(key: .registrationAmount, value: registrationAmount)
                    UserDefaultsManager.sharedInstance.saveIntArrayValueFor(key: .selectedServiceList, value: serviceList)
                    completion(success, signupPageNumber)
                }
            } else {
                if let safeResponse = response as? [String: AnyObject],
                    let responseCode = safeResponse[kResponseCode] as? Int,
                    responseCode == ResponseCodes.noSocialAccountResponseCode {
                    completion(false, 0)
                }
            }
        }
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
    private func validateSignInData(arrData: [CellInfo]) -> Bool {
        var isValid = true
        if let email = arrData[0].value, email.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter email.".localizedString())
            isValid = false
        } else if let email = arrData[0].value, !email.isValidEmail() {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter a valid email.".localizedString())
            isValid = false
        } else if let password = arrData[1].value, password.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter password.".localizedString())
            isValid = false
        }
        return isValid
    }
    
    
    private func getSignInParams(arrData: [CellInfo])-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.mobileOrEmail] = arrData[0].value as AnyObject
        params[ConstantAPIKeys.password] = arrData[1].value as AnyObject
        params[ConstantAPIKeys.deviceType] = Constants.Devices.deviceType as AnyObject
        params[ConstantAPIKeys.deviceToken] = UserSession.shared.getDeviceToken() as AnyObject
        return params
    }
    
    private func getSocialSignInParams(socialUser: SocialUser)-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.socialId] = socialUser.socialId as AnyObject
        params[ConstantAPIKeys.socialType] = socialUser.socialType as AnyObject
        params[ConstantAPIKeys.deviceType] = Constants.Devices.deviceType as AnyObject
        params[ConstantAPIKeys.deviceToken] = UserSession.shared.getDeviceToken() as AnyObject
        return params
    }
    
    func getVatSettings(completion: @escaping (Bool)-> Void) {
      APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getSettings)) { (response, success) in
        if success {
          if let safeResponse =  response as? [String: AnyObject],
            let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
             {
            UserDefaultsManager.sharedInstance.saveDoubleValueFor(key: .vatPercentage, value: Helper.toDouble(result[ConstantAPIKeys.vatPercentage]))
            completion(true)
          }
        }
      }
    }
    func getNotificationCount(completion: @escaping (Bool)-> Void){
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .notificationCount)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject] {
                    UserDefaultsManager.sharedInstance.saveIntValueFor(key: .notificationCount, value: result[ConstantAPIKeys.count] as! Int)
                    completion(success)
                }
            }
        }
    }
}
