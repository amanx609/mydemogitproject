//
//  ForgotPasswordButtonCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol ForgotPasswordCellDelegate: class {
  func didTapForgotPasswordButton(cell: ForgotPasswordButtonCell)
}

class ForgotPasswordButtonCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
     @IBOutlet weak var forgotPasswordButton: UIButton!
    //MARK: - Variables
     weak var delegate: ForgotPasswordCellDelegate?
     
     //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.forgotPasswordButton.setTitle(cellInfo.placeHolder, for: .normal)
    }
    
    //MARK: - IBActions
    @IBAction func tapForgotPassword(_ sender: Any) {
      if let delegate = self.delegate {
        delegate.didTapForgotPasswordButton(cell: self)
      }
    }
}
