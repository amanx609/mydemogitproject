//
//  ProfileImageTableViewCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/16/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class ProfileImageTableViewCell: BaseTableViewCell,NibLoadableView,ReusableView {
    
    //MARK: - IBOutlets

    //MARK: - LifeCycleMethods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
