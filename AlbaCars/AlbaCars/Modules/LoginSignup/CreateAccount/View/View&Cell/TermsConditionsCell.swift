//
//  TermsConditionsCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
protocol TermsConditionsCellDelegate: class {
  func didTapCheckBox(cell: TermsConditionsCell, isChecked: Bool)
  func didTapTermsConditions(cell: TermsConditionsCell)
}

class TermsConditionsCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
    @IBOutlet weak var checkBoxButton: UIButton!
    
    //MARK: - Variables
     weak var delegate: TermsConditionsCellDelegate?
    
     //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        
    }
    
    //MARK: - Public Methods
    func configureViewAuctionCell(cellInfo: CellInfo) {
        checkBoxButton.isUserInteractionEnabled = false
        checkBoxButton.setImage(#imageLiteral(resourceName: "red_tick"), for: .normal)
    }
    
    //MARK: - IBActions
    
    @IBAction func didTapCheckBoxButton(_ sender: Any) {
        var isChecked = false
        if checkBoxButton.imageView?.image == UIImage(named: "checked") {
            checkBoxButton.setImage(UIImage(named: "unchecked"), for: .normal)
            isChecked = false
        } else {
            checkBoxButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            isChecked = true
        }
        if let delegate = self.delegate {
            delegate.didTapCheckBox(cell: self, isChecked: isChecked)
        }
    }
    
    @IBAction func didTapTermsConditionsButton(_ sender: Any) {
        
        if let delegate = self.delegate {
            delegate.didTapTermsConditions(cell: self)
        }
    }

}
