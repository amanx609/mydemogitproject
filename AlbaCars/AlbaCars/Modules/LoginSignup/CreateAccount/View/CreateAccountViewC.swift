//
//  CreateAccountViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

class CreateAccountViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var createAccountTableView: TPKeyboardAvoidingTableView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet weak var uploadImageButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileImageBgView: UIImageView!
    
    //MARK: - Variables
    
    var createAccountDataSource: [CellInfo] = []
    var viewModel: CreateAccountVModeling?
    var isTermsConditionAccepted = false
    var profileImageUrl = ""
    var socialUser: SocialUser?
    
    //MARK: - LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    deinit {
        print("deinit CreateAccountViewC")
    }
    
    override func navigationButtonTapped(_ sender: AnyObject) {
        guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
        switch buttonType {
        case .back:
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.navigationController?.popViewController(animated: true)
        default: break
        }
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Create an Account".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.recheckVM()
        self.setupTableView()
        self.setupImageView()
        if let dataSource = self.viewModel?.getCreateAccountDataSource(socialUser: self.socialUser) {
            self.createAccountDataSource = dataSource
        }
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = CreateAccountViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.createAccountTableView.delegate = self
        self.createAccountTableView.dataSource = self
        self.createAccountTableView.separatorStyle = .none
        self.createAccountTableView.allowsSelection = false
        self.createAccountTableView.tableHeaderView = self.headerView
    }
    
    private func registerNibs() {
        self.createAccountTableView.register(TextInputCell.self)
        self.createAccountTableView.register(DropDownCell.self)
        self.createAccountTableView.register(PhoneCell.self)
        self.createAccountTableView.register(BottomLabelCell.self)
        self.createAccountTableView.register(BottomButtonCell.self)
        self.createAccountTableView.register(TermsConditionsCell.self)
    }
    
    private func setupImageView() {
        self.profileImageView.layer.cornerRadius = profileImageView.frame.width / 2
        self.profileImageBgView.layer.cornerRadius = profileImageBgView.frame.width/2
        self.profileImageView.layer.masksToBounds = true
        //SocialSignup - SetImageIfPresent
        if let userImageUrl = self.socialUser?.image {
            self.profileImageUrl = userImageUrl
            self.profileImageView.setImage(urlStr: userImageUrl, placeHolderImage: nil)
            self.uploadImageButton.setTitle("", for: .normal)
            self.uploadImageButton.backgroundColor = UIColor.clear
        }
        viewWillLayoutSubviews()
    }
    
    private func updateDateOfBirth(_ dob: String) {
        self.createAccountDataSource[5].value = dob
        self.createAccountTableView.reloadData()
    }
    
    private func uploadImageToS3(image: UIImage) {
        Loader.showLoader()
        self.profileImageView.image = image
        self.uploadImageButton.setTitle("", for: .normal)
        self.uploadImageButton.backgroundColor = UIColor.clear
        S3Manager.sharedInstance.uploadImage(image: image, imageQuality: .medium, progress: nil) { (response, imageName, error) in
            guard let imageName = imageName as? String else { return }
            self.profileImageUrl = imageName
            Threads.performTaskInMainQueue {
                // self.profileImageView.setImage(urlStr: response, placeHolderImage: nil)
                Loader.hideLoader()
            }
        }
    }
    //MARK: - Public Methods
    func setupDatePicker(_ textField: UITextField) {
        textField.inputView = self.datePicker
        textField.inputAccessoryView = Helper.getDoubleButtonToolBar(leftButtonTitle: StringConstants.Text.Cancel.localizedString(), rightButtonTitle: StringConstants.Text.done.localizedString(), leftButtonselector: #selector(self.cancelButtonTapped), rightButtonSelector: #selector(self.doneButtonTapped), target: self)
        let date = Calendar.current.date(byAdding: .year, value: -13, to: Date())
        self.datePicker.maximumDate = date
    }
    
    func showCityListPopup(indexPath: IndexPath) {
        self.view.endEditing(true)
        if let cityListPopup = SingleSelectListPopup.inistancefromNib() {
            cityListPopup.initializeViewWith(title: "Choose City".localizedString(), arrayList: Constants.uaeCityList, key: Constants.UIKeys.cityName) { [weak self] (response) in
                guard let sSelf = self else { return }
                if let cityName = response[Constants.UIKeys.cityName] as? String {
                    sSelf.createAccountDataSource[indexPath.row].value = cityName
                    sSelf.createAccountTableView.reloadData()
                }
            }
            cityListPopup.showWithAnimated(animated: true)
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func didTapUploadImage(_ sender: Any) {
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.uploadImageToS3(image: image)
                }
            }
        }
    }
    
    //MARK: - API Methods
    func requestCreateAccountAPI() {
        self.viewModel?.requestCreateAccountAPI(arrData: self.createAccountDataSource, image: self.profileImageUrl, isTermsConditionAccepted: self.isTermsConditionAccepted, socialUser: self.socialUser, completion: { (success, user) in
            let otpViewController = DIConfigurator.sharedInstance.getOTPVerificationVC()
            otpViewController.user = user
            self.navigationController?.pushViewController(otpViewController, animated: true)
        })
    }
    
    //MARK: - Selectors
    @objc func doneButtonTapped() {
        self.view.endEditing(true)
        let selectedDate = self.datePicker.date
        let convertedDate = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatWithoutSpace)
        self.updateDateOfBirth(convertedDate)
    }
    
    @objc func cancelButtonTapped() {
        self.view.endEditing(true)
    }
    
    @objc func showPassword(sender: UIButton) {
        
        let indexPa = IndexPath(row: 6, section: 0)
        if let cell = self.createAccountTableView.cellForRow(at: indexPa) as? TextInputCell {
            cell.inputTextField.isSecureTextEntry = !sender.isSelected
            cell.showPasswordButton.isSelected = !sender.isSelected
        }
    }
}
