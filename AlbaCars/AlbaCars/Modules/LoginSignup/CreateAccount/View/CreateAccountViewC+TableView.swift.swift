//
//  CreateAccountViewC+TableView.swift.swift
//  AlbaCars
//
//  Created by Narendra on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension CreateAccountViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.createAccountDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.createAccountDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.createAccountDataSource[indexPath.row].height
    }
}

extension CreateAccountViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .TextInputCell:
            let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            if indexPath.row == 5 {
                self.setupDatePicker(cell.inputTextField)
            } else {
                cell.inputTextField.inputView = nil
                cell.inputTextField.inputAccessoryView = nil
            }
            cell.showPasswordButton.addTarget(self, action: #selector(showPassword(sender:)), for: .touchUpInside)
            cell.showPasswordButton.isHidden = indexPath.row == 6 ? false : true
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .PhoneCell:
            let cell: PhoneCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.placeholderImageView.isHidden = true
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .DropDownCell:
            let cell: DropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .BottomButtonCell:
            let cell: BottomButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .TermsConditionsCell:
            let cell: TermsConditionsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - BottomButtonCellDelegate
extension CreateAccountViewC: BottomButtonCellDelegate {
    func didTapBottomButton(cell: BottomButtonCell) {
        self.view.endEditing(true)
        self.requestCreateAccountAPI()
    }
}

//MARK: - TermsConditions Cell
extension CreateAccountViewC: TermsConditionsCellDelegate {
    
    func didTapCheckBox(cell: TermsConditionsCell, isChecked: Bool) {
        isTermsConditionAccepted = isChecked
    }
    
    func didTapTermsConditions(cell: TermsConditionsCell) {
        let webViewVC = DIConfigurator.sharedInstance.getwebViewVC()
        webViewVC.webContentType = .termsConditions
        self.navigationController?.pushViewController(webViewVC, animated: true)
    }
}


//MARK: - BottomLabelCellDelegate
extension CreateAccountViewC: BottomLabelCellDelegate {
    func didTapLabel(cell: BottomLabelCell) {
        if let viewControllers = self.navigationController?.viewControllers {
            //LoginIsPresentInNavigationStack
            for viewController in viewControllers {
                if viewController is LoginViewC {
                    self.navigationController?.popToViewController(viewController, animated: true)
                    return
                }
            }
            //LoginNotPresentInNavigationStack
            let loginVC = DIConfigurator.sharedInstance.getLoginVC()
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
}

//MARK: - DropDownCellDelegate
extension CreateAccountViewC: DropDownCellDelegate {
    func didTapDropDownCell(cell: DropDownCell) {
        guard let indexPath = self.createAccountTableView.indexPath(for: cell) else { return }
        self.showCityListPopup(indexPath: indexPath)
    }
}

//MARK: - TextInputCellDelegate
extension CreateAccountViewC: TextInputCellDelegate {
    
    func tapNextKeyboard(cell: TextInputCell) {
        self.view.endEditing(true)
    }
    
    func didChangeText(cell: TextInputCell, text: String) {
        guard let indexPath = self.createAccountTableView.indexPath(for: cell) else { return }
        self.createAccountDataSource[indexPath.row].value = text
    }
}

//MARK: - PhoneCell Delegate
extension CreateAccountViewC: PhoneCellDelegate {
    func didTapCountryCode(cell: PhoneCell) {
        
    }
    
    func didChangeText(cell: PhoneCell, text: String) {
        guard let indexPath = self.createAccountTableView.indexPath(for: cell) else { return }
        self.createAccountDataSource[indexPath.row].value = text
    }
}
