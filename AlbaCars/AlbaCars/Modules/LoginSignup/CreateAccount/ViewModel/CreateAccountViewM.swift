//
//  CreateAccountViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol CreateAccountVModeling: BaseVModeling {
  func getCreateAccountDataSource(socialUser: SocialUser?) -> [CellInfo]
  func requestCreateAccountAPI(arrData: [CellInfo], image: String, isTermsConditionAccepted: Bool, socialUser: SocialUser?, completion: @escaping (Bool, User)-> Void)
}

class CreateAccountViewM: CreateAccountVModeling {
  
  func getCreateAccountDataSource(socialUser: SocialUser?) -> [CellInfo] {
    
    var array = [CellInfo]()
    
    //Full Name Cell
    var userName = ""
    if let name = socialUser?.name {
      userName = name
    }
    var fullNameInfo = [String: AnyObject]()
    fullNameInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "userName")
    fullNameInfo[Constants.UIKeys.inputType] = TextInputType.name as AnyObject
    let fullNameCell = CellInfo(cellType: .TextInputCell, placeHolder: "Full Name".localizedString(), value: userName, info: fullNameInfo, height: Constants.CellHeightConstants.height_80)
    array.append(fullNameCell)
    
    //Email Cell
    var emailInfo = [String: AnyObject]()
    emailInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "email")
    emailInfo[Constants.UIKeys.inputType] = TextInputType.email as AnyObject
    if let sUser = socialUser {
      let email = sUser.email
      let emailCell = CellInfo(cellType: .TextInputCell, placeHolder: "Email".localizedString(), value: email, info: emailInfo, height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.emailAddress, isUserInteractionEnabled: false)
      array.append(emailCell)
    } else {
        let emailCell = CellInfo(cellType: .TextInputCell, placeHolder: "Email".localizedString(), value: "", info: emailInfo, height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.emailAddress)
        array.append(emailCell)
    }
        
    //Phone Cell
    var phoneInfo = [String: AnyObject]()
    phoneInfo[Constants.UIKeys.countryCodePlaceholder] = "+971" as AnyObject
    let phoneCell = CellInfo(cellType: .PhoneCell, placeHolder: "Phone Number".localizedString(), value: "", info: phoneInfo,height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.numberPad)
    array.append(phoneCell)
    
    //Country Cell
    let countryCell = CellInfo(cellType: .TextInputCell, placeHolder: "", value: "UAE".localizedString(), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
    array.append(countryCell)
    
    //City Cell
    let cityCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "Select Your City".localizedString(), info: nil, height: Constants.CellHeightConstants.height_80)
    array.append(cityCell)
    
    //Date of  Birth
    var userDob = ""
    if let dob = socialUser?.birthday {
      userDob = dob
    }
    var dobInfo = [String: AnyObject]()
    dobInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "date")
    dobInfo[Constants.UIKeys.inputType] = TextInputType.dob as AnyObject
    let dobCell = CellInfo(cellType: .TextInputCell, placeHolder: "Date of Birth".localizedString(), value: userDob, info: dobInfo, height: Constants.CellHeightConstants.height_80)
    array.append(dobCell)
    
    //SocialSignupWon'tHavePasswordFields
    if socialUser == nil {
      //Password Cell
      var passwordInfo = [String: AnyObject]()
      passwordInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "password")
      passwordInfo[Constants.UIKeys.inputType] = TextInputType.password as AnyObject
      passwordInfo[Constants.UIKeys.isSecuredText] = true as AnyObject
      let passwordCell = CellInfo(cellType: .TextInputCell, placeHolder: "Password".localizedString(), value: "", info: passwordInfo, isSecureText: true, height: Constants.CellHeightConstants.height_80)
      array.append(passwordCell)
      
      //Confirm Password Cell
      var confirmPasswordInfo = [String: AnyObject]()
      confirmPasswordInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "password")
      confirmPasswordInfo[Constants.UIKeys.inputType] = TextInputType.password as AnyObject
      passwordInfo[Constants.UIKeys.isSecuredText] = true as AnyObject
      let confirmPasswordCell = CellInfo(cellType: .TextInputCell, placeHolder: "Confirm Password".localizedString(), value: "", info: confirmPasswordInfo,isSecureText: true, height: Constants.CellHeightConstants.height_80 )
      array.append(confirmPasswordCell)
    }
    
    //Terms Conditions Cell
    let termsConditionsCell = CellInfo(cellType: .TermsConditionsCell, placeHolder: "Register".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_50)
    array.append(termsConditionsCell)
    
    //Register Cell
    let registerCell = CellInfo(cellType: .BottomButtonCell, placeHolder: "Register".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_80)
    array.append(registerCell)
    
    //AlreadyHaveAccountCell
    var alreadyAccountInfo = [String: AnyObject]()
    alreadyAccountInfo[Constants.UIKeys.placeholderText] = "Have an account? Login Now".localizedString() as AnyObject
    alreadyAccountInfo[Constants.UIKeys.tappableText] = "Login Now".localizedString() as AnyObject
    let alreadyAccountCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "", value: "", info: alreadyAccountInfo, height: Constants.CellHeightConstants.height_50)
    array.append(alreadyAccountCell)
    return array
  }
  
  //MARK: - API Methods
  func requestCreateAccountAPI(arrData: [CellInfo], image: String, isTermsConditionAccepted: Bool, socialUser: SocialUser?, completion: @escaping (Bool, User)-> Void) {
    //SocialSignup
    if let sUser = socialUser {
      if self.validateCreateSocialAccountData(arrData: arrData, isTermsConditionAccepted: isTermsConditionAccepted) {
        let createAccountParams = self.getCreateSocialAccountParams(arrData: arrData, imageUrl: image, socialUser: sUser)
        self.requestAPI(params: createAccountParams) { (success, user) in
          completion(success, user)
        }
      }
    } else {
      //NormalSignup
      if self.validateCreateAccountData(arrData: arrData, isTermsConditionAccepted: isTermsConditionAccepted) {
        let createAccountParams = self.getCreateAccountParams(arrData: arrData, imageUrl: image)
        self.requestAPI(params: createAccountParams) { (success, user) in
          completion(success, user)
        }
      }
    }
  }
  
  private func requestAPI(params: APIParams, completion: @escaping (Bool, User)-> Void) {
    APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .signup(param: params))) { (response, success) in
      if success {
        if let safeResponse = response as? [String: AnyObject],
          let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
          let token = result[ConstantAPIKeys.token] as? String,
          let profile = result[ConstantAPIKeys.profile] as? [String: AnyObject],
          let registrationAmount = result[ConstantAPIKeys.registrationAmount] as? Int,
          let profileDict = profile.toJSONData() {
          do {
            let decoder = JSONDecoder()
            let user = try decoder.decode(User.self, from: profileDict)
            UserDefaultsManager.sharedInstance.saveUserValueFor(key:.user, value: user)
            UserDefaultsManager.sharedInstance.saveStringValueFor(key: .token, value: token)
            UserDefaultsManager.sharedInstance.saveIntValueFor(key: .registrationAmount, value: registrationAmount)
            completion(success,user)
          } catch let error {
            print(error)
          }
        }
      }
    }
  }
  
  //MARK: - Private Methods
  //DataValidationsBeforeAPICall
  private func validateCreateAccountData(arrData: [CellInfo], isTermsConditionAccepted: Bool) -> Bool {
    var isValid = true
    if let name = arrData[0].value, name.isEmpty {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter name.".localizedString())
      isValid = false
    } else if let email = arrData[1].value, email.isEmpty {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter email.".localizedString())
      isValid = false
    } else if let email = arrData[1].value, !email.isValidEmail() {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter a valid email.".localizedString())
      isValid = false
    } else if let phone = arrData[2].value, phone.isEmpty {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter your phone number".localizedString())
      isValid = false
    } else if let phone = arrData[2].value, phone.count < Constants.Validations.mobileNumberMinLength {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter a valid phone number.".localizedString())
      isValid = false
    } else if let city = arrData[4].value, city.isEmpty {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select city.".localizedString())
      isValid = false
    } else if let dob = arrData[5].value, dob.isEmpty {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter date of birth.".localizedString())
      isValid = false
    } else if let password = arrData[6].value, password.isEmpty {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter password.".localizedString())
      isValid = false
    } else if let password = arrData[6].value, password.count < Constants.Validations.passwordMinLength {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Password must be 6 to 18 characters.".localizedString())
      isValid = false
    } else if let confirmPassword = arrData[7].value, confirmPassword.isEmpty {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter confirm password.".localizedString())
      isValid = false
    } else if let confirmPassword = arrData[7].value, let password = arrData[6].value, confirmPassword != password {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Password and confirm password doesn't match.".localizedString())
      isValid = false
    } else if !isTermsConditionAccepted {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please accept terms and conditions.".localizedString())
      isValid = false
    }
    return isValid
  }
  
  private func validateCreateSocialAccountData(arrData: [CellInfo], isTermsConditionAccepted: Bool) -> Bool {
    var isValid = true
    if let name = arrData[0].value, name.isEmpty {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter name.".localizedString())
      isValid = false
    } else if let email = arrData[1].value, email.isEmpty {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter email.".localizedString())
      isValid = false
    } else if let phone = arrData[2].value, phone.isEmpty {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter your phone number".localizedString())
      isValid = false
    } else if let phone = arrData[2].value, phone.count < Constants.Validations.mobileNumberMinLength {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter a valid phone number.".localizedString())
      isValid = false
    } else if !isTermsConditionAccepted {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please accept terms and conditions.".localizedString())
      isValid = false
    }
    return isValid
  }
  
  
  private func getCreateAccountParams(arrData: [CellInfo], imageUrl: String)-> APIParams {
    var params: APIParams = APIParams()
    params[ConstantAPIKeys.deviceType] = Constants.Devices.deviceType as AnyObject
    params[ConstantAPIKeys.deviceToken] = UserSession.shared.getDeviceToken() as AnyObject
    params[ConstantAPIKeys.name] = arrData[0].value as AnyObject
    params[ConstantAPIKeys.email] = arrData[1].value as AnyObject
    params[ConstantAPIKeys.mobile] = arrData[2].value as AnyObject
    if let info = arrData[2].info {
      params[ConstantAPIKeys.countryCode] = info[Constants.UIKeys.countryCodePlaceholder] as AnyObject
    }
    params[ConstantAPIKeys.country] = arrData[3].value as AnyObject
    params[ConstantAPIKeys.city] = arrData[4].value as AnyObject
    let dateStr = arrData[5].value.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
    params[ConstantAPIKeys.dob] = dateStr as AnyObject
    params[ConstantAPIKeys.password] = arrData[6].value as AnyObject
    params[ConstantAPIKeys.image] = imageUrl as AnyObject
    params[ConstantAPIKeys.socialId] = "0" as AnyObject
    params[ConstantAPIKeys.socialType] = "0" as AnyObject
    params[ConstantAPIKeys.userType] = UserDefaultsManager.sharedInstance.getIntValueFor(key: .userType) as AnyObject?
    return params
  }
  
  private func getCreateSocialAccountParams(arrData: [CellInfo], imageUrl: String, socialUser: SocialUser)-> APIParams {
    var params: APIParams = APIParams()
    params[ConstantAPIKeys.deviceType] = Constants.Devices.deviceType as AnyObject
    params[ConstantAPIKeys.deviceToken] = UserSession.shared.getDeviceToken() as AnyObject
    params[ConstantAPIKeys.name] = arrData[0].value as AnyObject
    params[ConstantAPIKeys.email] = arrData[1].value as AnyObject
    params[ConstantAPIKeys.mobile] = arrData[2].value as AnyObject
    if let info = arrData[2].info {
      params[ConstantAPIKeys.countryCode] = info[Constants.UIKeys.countryCodePlaceholder] as AnyObject
    }
    params[ConstantAPIKeys.country] = arrData[3].value as AnyObject
    params[ConstantAPIKeys.city] = arrData[4].value as AnyObject
    let dateStr = arrData[5].value.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
    params[ConstantAPIKeys.dob] = dateStr as AnyObject
    params[ConstantAPIKeys.image] = imageUrl as AnyObject
    params[ConstantAPIKeys.socialId] = socialUser.socialId as AnyObject
    params[ConstantAPIKeys.socialType] = socialUser.socialType as AnyObject
    params[ConstantAPIKeys.password] = "" as AnyObject
    params[ConstantAPIKeys.userType] = UserDefaultsManager.sharedInstance.getIntValueFor(key: .userType) as AnyObject?
    
    return params
  }
  
  
}
