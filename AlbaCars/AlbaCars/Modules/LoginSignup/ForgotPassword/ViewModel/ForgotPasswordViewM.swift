//
//  ForgotPasswordViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import Foundation
import UIKit

protocol ForgotPasswordVModeling: BaseVModeling {
  func getForgotPasswordDataSource() -> [CellInfo]
  func requestForgotPasswordAPI(arrData: [CellInfo], completion: @escaping (Bool)-> Void)
}

class ForgotPasswordViewM: ForgotPasswordVModeling {
  
  func getForgotPasswordDataSource() -> [CellInfo] {
    
    var array = [CellInfo]()
    
    //Title Cell
    let titleCell = CellInfo(cellType: .BottomLabelRegularTextCell, placeHolder: "Enter Your Registered Email Address\nor Mobile Number", value: "", info: nil, height: Constants.CellHeightConstants.height_80)
    array.append(titleCell)
    
    //Email Address Cell
    var emailInfo = [String: AnyObject]()
    emailInfo[Constants.UIKeys.inputType] = TextInputType.email as AnyObject
    let emailCell = CellInfo(cellType: .TextInputCell, placeHolder: "Email Address".localizedString(), value: "", info: emailInfo, height: Constants.CellHeightConstants.height_80)
    array.append(emailCell)
    
    //OR Cell
    let orCell = CellInfo(cellType: .BottomLabelRegularTextCell, placeHolder: "Or", value: "", info: nil, height: Constants.CellHeightConstants.height_30)
    array.append(orCell)
    
    //Mobile Number Cell
    var mobileNumberInfo = [String: AnyObject]()
    mobileNumberInfo[Constants.UIKeys.countryCodePlaceholder] = "+971" as AnyObject
    let mobileNoCell = CellInfo(cellType: .PhoneCell, placeHolder: "Mobile Number".localizedString(), value: "", info: mobileNumberInfo, height: Constants.CellHeightConstants.height_80)
    array.append(mobileNoCell)
    
    //Submit Cell
    let submitCell = CellInfo(cellType: .BottomButtonCell, placeHolder: "Submit".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_130)
    array.append(submitCell)
    
    return array
  }
  
  //MARK: - API Methods
  func requestForgotPasswordAPI(arrData: [CellInfo], completion: @escaping (Bool)-> Void) {
    if self.validateForgotPasswordData(arrData: arrData) {
      let forgotPasswordParams = self.getForgotPasswordParams(arrData: arrData)
      APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .forgotPassword(param: forgotPasswordParams))) { (response, success) in
        if success {
          if let safeResponse =  response as? [String: AnyObject],
            let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject] {
              completion(success)
          }
        }
      }
    }
  }
  
  //MARK: - Private Methods
  //DataValidationsBeforeAPICall
  private func validateForgotPasswordData(arrData: [CellInfo]) -> Bool {
    var isValid = true
    //EmailIsEmpty
    if let email = arrData[1].value, email.isEmpty {
      //PhoneIsAlsoEmpty
      if let phoneNumber = arrData[3].value, phoneNumber.isEmpty {
        Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter email.".localizedString())
        isValid = false
      } else if let phoneNumber = arrData[3].value, (phoneNumber.count < Constants.Validations.mobileNumberMinLength || phoneNumber.count > Constants.Validations.mobileNumberMaxLength) {
        Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter a valid phone number.".localizedString())
        isValid = false
      }
    } else if let email = arrData[1].value, !email.isValidEmail() {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter a valid email.".localizedString())
      isValid = false
    }
    return isValid
  }
  
  private func getForgotPasswordParams(arrData: [CellInfo])-> APIParams {
    var params: APIParams = APIParams()
    params[ConstantAPIKeys.email] = "" as AnyObject
    params[ConstantAPIKeys.mobile] = "" as AnyObject
    params[ConstantAPIKeys.countryCode] = "" as AnyObject
    //EmailEntered
    if let email = arrData[1].value, !email.isEmpty {
      params[ConstantAPIKeys.email] = email as AnyObject
    } else {
      //PhoneEntered
      params[ConstantAPIKeys.mobile] = arrData[3].value as AnyObject
      if let info = arrData[3].info,
          let countryCode = info[Constants.UIKeys.countryCodePlaceholder] as? String {
        params[ConstantAPIKeys.countryCode] = countryCode as AnyObject
      }
    }
    return params
  }
}
