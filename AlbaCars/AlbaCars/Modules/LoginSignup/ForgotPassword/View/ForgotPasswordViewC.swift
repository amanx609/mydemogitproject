//
//  ForgotPasswordViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

class ForgotPasswordViewC: BaseViewC {
  
  //MARK: - IBOutlets
  @IBOutlet weak var forgotTableView: TPKeyboardAvoidingTableView!
  @IBOutlet var headerView: UIView!
  
  //MARK: - Variables
  
  var forgotDataSource: [CellInfo] = []
  var viewModel: ForgotPasswordVModeling?
  
  //MARK: - LifeCycle Methods
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setup()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(false, animated: false)
  }
  
  deinit {
    print("deinit ForgotPasswordViewC")
  }
  
  //MARK: - Private Methods
  private func setup() {
    self.setupNavigationBarTitle(title: "Forgot Password".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
    self.recheckVM()
    self.setupTableView()
    if let dataSource = self.viewModel?.getForgotPasswordDataSource() {
      self.forgotDataSource = dataSource
    }
  }
  
  private func recheckVM() {
    if self.viewModel == nil {
      self.viewModel = ForgotPasswordViewM()
    }
  }
  
  private func setupTableView() {
    self.registerNibs()
    self.forgotTableView.delegate = self
    self.forgotTableView.dataSource = self
    self.forgotTableView.separatorStyle = .none
    self.forgotTableView.allowsSelection = false
    self.forgotTableView.tableHeaderView = self.headerView
    self.forgotTableView.bounces = false
  }
  
  private func registerNibs() {
    self.forgotTableView.register(TextInputCell.self)
    self.forgotTableView.register(PhoneCell.self)
    self.forgotTableView.register(BottomLabelCell.self)
    self.forgotTableView.register(BottomButtonCell.self)
    self.forgotTableView.register(SignupButtonCell.self)
  }
  
  //MARK: - API Methods
  func requestForgotPasswordAPI() {
    self.viewModel?.requestForgotPasswordAPI(arrData: self.forgotDataSource) { (success) in
      if success {
        Threads.performTaskInMainQueue {
            let changePasswordViewC = DIConfigurator.sharedInstance.getChangePasswordVC()
            changePasswordViewC.paramInfo[ConstantAPIKeys.senderType] = ChangePwdSenderType(rawValue: 0) as AnyObject
            changePasswordViewC.paramInfo[ConstantAPIKeys.email] = self.forgotDataSource[0].value as AnyObject
            changePasswordViewC.paramInfo[ConstantAPIKeys.email] = self.forgotDataSource[1].placeHolder as AnyObject
            changePasswordViewC.paramInfo[ConstantAPIKeys.email] = self.forgotDataSource[1].value as AnyObject
            changePasswordViewC.changePasswordType = .forgotPassword
            self.navigationController?.pushViewController(changePasswordViewC, animated: true)
        }        
      }
    }
  }
}
