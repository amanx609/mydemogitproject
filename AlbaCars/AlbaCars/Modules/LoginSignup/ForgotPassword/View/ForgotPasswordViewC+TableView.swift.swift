  //
  //  ForgotPasswordViewC+TableView.swift.swift
  //  AlbaCars
  //
  //  Created by Narendra on 12/10/19.
  //  Copyright © 2019 Appventurez. All rights reserved.
  //
  
  
  import Foundation
  import UIKit
  
  extension ForgotPasswordViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return self.forgotDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cellInfo = self.forgotDataSource[indexPath.row]
      return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return self.forgotDataSource[indexPath.row].height
    }
  }
  
  extension ForgotPasswordViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
      guard let cellType = cellInfo.cellType else { return UITableViewCell() }
      switch cellType {
      case .TextInputCell:
        let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.configureView(cellInfo: cellInfo)
        cell.placeholderView.isHidden = true
        return cell
      case.PhoneCell:
        let cell: PhoneCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.configureView(cellInfo: cellInfo)
        return cell
      case .BottomLabelRegularTextCell:
        let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureViewWithLightText(cellInfo: cellInfo)
        return cell
      case .BottomButtonCell:
        let cell: BottomButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.configureView(cellInfo: cellInfo)
        return cell
      default:
        return UITableViewCell()
      }
    }
  }
  
  //MARK: - TextInputCellDelegate
  extension ForgotPasswordViewC: TextInputCellDelegate {
    func tapNextKeyboard(cell: TextInputCell) {
      self.view.endEditing(true)
    }
    
    func didChangeText(cell: TextInputCell, text: String) {
      guard let indexPath = self.forgotTableView.indexPath(for: cell) else { return }
      self.forgotDataSource[indexPath.row].value = text.trim()
        
        let indexPa = IndexPath(row: 3, section: 0)
        
        if let cell = self.forgotTableView.cellForRow(at: indexPa) as? PhoneCell {
            
            if text.trim().isEmpty {
                cell.isUserInteractionEnabled = true
                cell.alpha = 1
            } else {
                cell.isUserInteractionEnabled = false
                cell.alpha = 0.50
            }
        }
        
    }
  }
  
  //MARK: - PhoneCellDelegate
  extension ForgotPasswordViewC: PhoneCellDelegate {
  
    
    func didTapCountryCode(cell: PhoneCell) {
//      guard let indexPath = self.forgotTableView.indexPath(for: cell) else { return }
//      let cellInfo = self.forgotDataSource[indexPath.row]
//      CountryCodeManager.shared.getCountryCode { [weak self] (countryCode) in
//        guard let sSelf = self else { return }
//        if let countryCodeValue = countryCode,
//          var info = cellInfo.info {
//          info[Constants.UIKeys.countryCodePlaceholder] = countryCodeValue as AnyObject
//          sSelf.forgotDataSource[indexPath.row].info = info
//          sSelf.forgotTableView.reloadData()
//        }
//      }
    }
    
    func didChangeText(cell: PhoneCell, text: String) {
        guard let indexPath = self.forgotTableView.indexPath(for: cell) else { return }
        self.forgotDataSource[indexPath.row].value = text.trim()
        
        let indexPa = IndexPath(row: 1, section: 0)
        
        if let cell = self.forgotTableView.cellForRow(at: indexPa) as? TextInputCell {
            
            if text.trim().isEmpty {
                cell.isUserInteractionEnabled = true
                cell.alpha = 1
            } else {
                cell.isUserInteractionEnabled = false
                cell.alpha = 0.50
            }
        }
        
    }
  }
  
  //MARK: - BottomButtonCellDelegate
  extension ForgotPasswordViewC: BottomButtonCellDelegate {
    func didTapBottomButton(cell: BottomButtonCell) {
      self.view.endEditing(true)
      self.requestForgotPasswordAPI()
    }
  }
