//
//  LoginTypeViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/9/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class LoginTypeViewC: BaseViewC {
  
  //MARK: - IBOutlet
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var customerButton: UIButton!
  @IBOutlet weak var supplierButton: UIButton!
  @IBOutlet weak var dealerButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    
  //MARK: - LifeCycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setup()
  }
  
  deinit {
    print("deinit LoginTypeViewC")
  }
  
  //MARK: - Private Method
  private func setup() {
    self.setupViews()
    self.supplierButton.addShadow(ofColor:  UIColor.lightGray, radius: Constants.UIConstants.sizeRadius_7)
    self.dealerButton.addShadow(ofColor:  UIColor.lightGray, radius: Constants.UIConstants.sizeRadius_7)
  }
    
    private func setupViews() {
        self.customerButton.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.gradientView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.supplierButton.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.dealerButton.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.customerButton.setTitle(StringConstants.Text.customer, for: .normal)
        self.dealerButton.setTitle(StringConstants.Text.dealer, for: .normal)
        self.supplierButton.setTitle(StringConstants.Text.supplier, for: .normal)
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
    }
  
  //MARK: - IBActions
  @IBAction func tapCustomer(_ sender: Any) {
    UserDefaultsManager.sharedInstance.saveIntValueFor(key: .userType, value: UserType.customer.rawValue)
    let signupVC = DIConfigurator.sharedInstance.getSignupVC()
    self.navigationController?.pushViewController(signupVC, animated: true)
  }
  
  @IBAction func tapDealer(_ sender: Any) {
    UserDefaultsManager.sharedInstance.saveIntValueFor(key: .userType, value: UserType.dealer.rawValue)
    let signupVC = DIConfigurator.sharedInstance.getSignupVC()
    self.navigationController?.pushViewController(signupVC, animated: true)
  }
  
  @IBAction func tapSupplier(_ sender: Any) {
    UserDefaultsManager.sharedInstance.saveIntValueFor(key: .userType, value: UserType.supplier.rawValue)
    let signupVC = DIConfigurator.sharedInstance.getSignupVC()
    self.navigationController?.pushViewController(signupVC, animated: true)
  }
}
