//
//  SignupViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SignupVModeling: BaseVModeling {
  func getSignupDataSource() -> [CellInfo]
  func requestSocialSignInAPI(socialUser: SocialUser, completion: @escaping (Bool, Int)-> Void)
}

class SignupViewM: BaseViewM, SignupVModeling {
  
  func getSignupDataSource() -> [CellInfo] {
    
    var array = [CellInfo]()
    
    //Email Cell
    var emailInfo = [String: AnyObject]()
    emailInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "emailWhite")
    emailInfo[Constants.UIKeys.backgroundColor] = UIColor.redButtonColor
    emailInfo[Constants.UIKeys.textColor] = UIColor.white
    emailInfo[Constants.UIKeys.buttonType] = SocialType.email.rawValue as AnyObject
    let emailCell = CellInfo(cellType: .SignupButtonCell, placeHolder: "Register using Email".localizedString(), value: "", info: emailInfo, height: Constants.CellHeightConstants.height_80)
    array.append(emailCell)
    
    //Google Cell
    var googleInfo = [String: AnyObject]()
    googleInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "google")
    googleInfo[Constants.UIKeys.backgroundColor] = UIColor.white
    googleInfo[Constants.UIKeys.textColor] = UIColor.black
    googleInfo[Constants.UIKeys.buttonType] = SocialType.google.rawValue as AnyObject
    let googleCell = CellInfo(cellType: .SignupButtonCell, placeHolder: "Register using Google".localizedString(), value: "", info: googleInfo, height: Constants.CellHeightConstants.height_80)
    array.append(googleCell)
    
    //Facebook Cell
    var facebookInfo = [String: AnyObject]()
    facebookInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "facebook")
    facebookInfo[Constants.UIKeys.backgroundColor] = UIColor.facebookButtonColor
    facebookInfo[Constants.UIKeys.textColor] = UIColor.white
    facebookInfo[Constants.UIKeys.buttonType] = SocialType.facebook.rawValue as AnyObject
    let facebookCell = CellInfo(cellType: .SignupButtonCell, placeHolder: "Register using Facebook".localizedString(), value: "", info: facebookInfo, height: Constants.CellHeightConstants.height_80)
    array.append(facebookCell)
    
    //AlreadyHaveAccountCell
    var alreadyAccountInfo = [String: AnyObject]()
    alreadyAccountInfo[Constants.UIKeys.placeholderText] = "Already have an account? Login Now".localizedString() as AnyObject
    alreadyAccountInfo[Constants.UIKeys.tappableText] = "Login Now".localizedString() as AnyObject
    let alreadyAccountCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "", value: "", info: alreadyAccountInfo, height: Constants.CellHeightConstants.height_100)
    array.append(alreadyAccountCell)
    return array
  }
  
  //MARK: - API Methods
  func requestSocialSignInAPI(socialUser: SocialUser, completion: @escaping (Bool, Int)-> Void) {
    let socialSignInParams = self.getSocialSignInParams(socialUser: socialUser)
    APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .socialSignin(param: socialSignInParams))) { (response, success) in
      if success {
        if let safeResponse =  response as? [String: AnyObject],
          let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
          let token = result[ConstantAPIKeys.token] as? String,
          let profile = result[ConstantAPIKeys.profile] as? [String: AnyObject],
          let signupPageNumber = result[ConstantAPIKeys.signupPageNumber] as? Int,
          
          let profileDict = profile.toJSONData() {
          do {
            let decoder = JSONDecoder()
            let user = try decoder.decode(User.self, from: profileDict)
            UserDefaultsManager.sharedInstance.saveUserValueFor(key:.user, value: user)
            UserDefaultsManager.sharedInstance.saveStringValueFor(key: .token, value: token)
            completion(success, signupPageNumber)
          } catch let error {
            print(error)
          }
        }
      } else {
        if let safeResponse = response as? [String: AnyObject],
          let responseCode = safeResponse[kResponseCode] as? Int,
          responseCode == ResponseCodes.noSocialAccountResponseCode {
            completion(false, 0)
          }
        }
      
        
      print(response)
      /*if success {
        if let safeResponse =  response as? [String: AnyObject],
          let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
          let token = result[ConstantAPIKeys.token] as? String,
          let profile = result[ConstantAPIKeys.profile] as? [String: AnyObject],
          //let signupPageNumber = result[ConstantAPIKeys.token] as? Int,
          let profileDict = profile.toJSONData() {
          do {
            let decoder = JSONDecoder()
            let user = try decoder.decode(User.self, from: profileDict)
            
            UserDefaultsManager.sharedInstance.saveUserValueFor(key:.user, value: user)
            UserDefaultsManager.sharedInstance.saveStringValueFor(key: .token, value: token)
              completion(success)
          } catch let error {
            print(error)
          }
        }
      }*/
    }
  }
  
  //MARK: - Private Methods
  private func getSocialSignInParams(socialUser: SocialUser)-> APIParams {
    var params: APIParams = APIParams()
    params[ConstantAPIKeys.socialId] = socialUser.socialId as AnyObject
    params[ConstantAPIKeys.socialType] = socialUser.socialType as AnyObject
    params[ConstantAPIKeys.deviceType] = Constants.Devices.deviceType as AnyObject
    params[ConstantAPIKeys.deviceToken] = UserSession.shared.getDeviceToken() as AnyObject
    return params
  }
  
}
