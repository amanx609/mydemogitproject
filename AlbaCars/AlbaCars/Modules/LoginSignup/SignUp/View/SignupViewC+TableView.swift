//
//  SignupViewC+TableView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension SignupViewC: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.signUpDataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellInfo = self.signUpDataSource[indexPath.row]
    return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return self.signUpDataSource[indexPath.row].height
  }
}

extension SignupViewC {
  func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
    guard let cellType = cellInfo.cellType else { return UITableViewCell() }
    switch cellType {
    case .SignupButtonCell:
      let cell: SignupButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate = self
      cell.configureView(cellInfo: cellInfo)
      return cell
      
    case .BottomLabelCell:
      let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate = self
      cell.configureView(cellInfo: cellInfo)
      return cell
      
    default:
      return UITableViewCell()
    }
  }
}

//MARK: - SignupButtonCellDelegate
extension SignupViewC: SignupButtonCellDelegate {
  func didTapSignupButton(cell: SignupButtonCell) {
    guard let indexPath = self.signupTableView.indexPath(for: cell) else { return }
    let cellInfo = self.signUpDataSource[indexPath.row]
    if let info = cellInfo.info,
      let buttonTypeValue = info[Constants.UIKeys.buttonType] as? String,
      let socialType = SocialType(rawValue: buttonTypeValue) {
      switch socialType {
      case .email:
        let createAccountVC = DIConfigurator.sharedInstance.getCreateAccountVC()
        self.navigationController?.pushViewController(createAccountVC, animated: true)
      case .google:
        GoogleManager.sharedInstance.loginWithGoogle { (socialUser) in
          guard let user = socialUser else { return }
          self.requestSocialSignInAPI(socialUser: user)
        }
      case .facebook:
        FacebookManager.sharedInstance.loginWithFacebook(readPermissions: [
        Permission.publicProfile.stringValue,
        Permission.email.stringValue,
        Permission.birthday.stringValue]) { (socialUser) in
          guard let user = socialUser else { return }
          self.requestSocialSignInAPI(socialUser: user)
        }
      }
    }
  }
}

//MARK: - BottomLabelCellDelegate
extension SignupViewC: BottomLabelCellDelegate {
  func didTapLabel(cell: BottomLabelCell) {
    let loginVC = DIConfigurator.sharedInstance.getLoginVC()
    self.navigationController?.pushViewController(loginVC, animated: true)
  }
}
