//
//  SignupButtonCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol SignupButtonCellDelegate: class {
  func didTapSignupButton(cell: SignupButtonCell)
}

class SignupButtonCell: BaseTableViewCell, ReusableView, NibLoadableView {
  
  //MARK: - IBOutlets
  @IBOutlet weak var shadowView: UIView!
  @IBOutlet weak var bgView: UIView!
  @IBOutlet weak var placeholderLabel: UILabel!
  @IBOutlet weak var placeholderImageView: UIImageView!
  
  //MARK: - Variables
  weak var delegate: SignupButtonCellDelegate?
  
  //MARK: - LifeCycle Methods
  override func awakeFromNib() {
    super.awakeFromNib()
    self.setupView()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  //MARK: - Private Methods
  private func setupView() {
    self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_7)
    self.shadowView.addShadow(Constants.UIConstants.sizeRadius_5, shadowOpacity: Constants.UIConstants.shadowOpacity)
    self.bgView.roundCorners(Constants.UIConstants.sizeRadius_7)
  }
  
  //MARK: - Public Methods
  func configureView(cellInfo: CellInfo) {
    if let info = cellInfo.info {
      //Placeholder
      self.placeholderLabel.text = cellInfo.placeHolder
      //Image
      if let placeholderImage = info[Constants.UIKeys.placeholderImage] as? UIImage {
        self.placeholderImageView.image = placeholderImage
      }
      //BgColor
      if let bgColor = info[Constants.UIKeys.backgroundColor] as? UIColor {
        self.bgView.backgroundColor = bgColor
      }
      //TextColor
      if let textColor = info[Constants.UIKeys.textColor] as? UIColor {
        self.placeholderLabel.textColor = textColor
      }
    }
  }
  
  //MARK: - IBActions
  @IBAction func tapSignup(_ sender: UIButton) {
    if let delegate = self.delegate {
      delegate.didTapSignupButton(cell: self)
    }
  }
}
