//
//  SignupViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class SignupViewC: BaseViewC {
  
  //MARK: - IBOutlets
  @IBOutlet weak var signupTableView: UITableView!
  @IBOutlet var headerView: UIView!
  
  //MARK: - Variables
  var viewModel: SignupVModeling?
  var signUpDataSource: [CellInfo] = []
  
  //MARK: - LifeCycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setup()
  }
  
  //MARK: - Private Methods
  private func setup() {
    self.recheckVM()
    self.setupTableView()
    if let dataSource = self.viewModel?.getSignupDataSource() {
      self.signUpDataSource = dataSource
    }
  }
  
  private func recheckVM() {
    if self.viewModel == nil {
      self.viewModel = SignupViewM()
    }
  }
  
  private func setupTableView() {
    self.registerNibs()
    self.signupTableView.delegate = self
    self.signupTableView.dataSource = self
    self.signupTableView.separatorStyle = .none
    self.signupTableView.allowsSelection = false
    self.signupTableView.tableHeaderView = self.headerView
  }
  
  private func registerNibs() {
    self.signupTableView.register(SignupButtonCell.self)
    self.signupTableView.register(BottomLabelCell.self)
  }
  
  private func goToNextPage(signupPageNum: Int) {
    let screenType = ScreenNameAfterLogin(rawValue: signupPageNum)
    switch screenType {
    case .otp:
      let otpViewC = DIConfigurator.sharedInstance.getOTPVerificationVC()
      self.navigationController?.pushViewController(otpViewC, animated: true)
    case .serviceType:
      let chooseServiceViewC = DIConfigurator.sharedInstance.getSChooseServiceVC()
      self.navigationController?.pushViewController(chooseServiceViewC, animated: true)
    case .documents:
      let documentViewC = DIConfigurator.sharedInstance.getUploadDocumentViewC()
      self.navigationController?.pushViewController(documentViewC, animated: true)
    case .payment:
      let registerPaymentViewC = DIConfigurator.sharedInstance.getRegisterPaymentViewC()
      self.navigationController?.pushViewController(registerPaymentViewC, animated: true)
    case .home:
      AppDelegate.delegate.showHome()
    default:
      break
    }
  }
  
  //MARK: - API Methods
  func requestSocialSignInAPI(socialUser: SocialUser) {
    self.viewModel?.requestSocialSignInAPI(socialUser: socialUser, completion: { (success, signupPageNumber) in
      if success {
        //GoToNextPageAccordingToSignupPageNumber
        Threads.performTaskInMainQueue {
          self.goToNextPage(signupPageNum: signupPageNumber)
        }
      } else {
        Threads.performTaskInMainQueue {
          let createAccountVC = DIConfigurator.sharedInstance.getCreateAccountVC()
          createAccountVC.socialUser = socialUser
          self.navigationController?.pushViewController(createAccountVC, animated: true)
        }
      }
    })
  }
}
