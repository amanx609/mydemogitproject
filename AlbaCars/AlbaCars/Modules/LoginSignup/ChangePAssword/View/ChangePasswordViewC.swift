//
//  ChangePasswordViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

enum ChangePasswordType {
    
    case changePassword
    case forgotPassword
    
    var title: String? {
        switch self {
        case .changePassword: return "Change Password".localizedString()
        case .forgotPassword: return "Reset Password".localizedString()
        }
    }
}

class ChangePasswordViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var changePasswordTableView: TPKeyboardAvoidingTableView!
    @IBOutlet var headerView: UIView!
    
    //MARK: - Variables
    var chnagePasswordDataSource: [CellInfo] = []
    var viewModel: ChangePasswordVModeling?
    var paramInfo = [String: AnyObject]()
    var changePasswordType: ChangePasswordType = .changePassword
    
    //MARK: - LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    deinit {
        print("deinit ChangePasswordViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: Helper.toString(object: changePasswordType.title), leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.recheckVM()
        self.setupTableView()
        if let dataSource = self.viewModel?.getChangePasswordDataSource(changePasswordType: changePasswordType) {
            self.chnagePasswordDataSource = dataSource
        }
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = ChangePasswordViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.changePasswordTableView.delegate = self
        self.changePasswordTableView.dataSource = self
        self.changePasswordTableView.separatorStyle = .none
        self.changePasswordTableView.allowsSelection = false
        self.changePasswordTableView.tableHeaderView = self.headerView
    }
    
    private func registerNibs() {
        self.changePasswordTableView.register(TextInputCell.self)
        self.changePasswordTableView.register(BottomLabelCell.self)
        self.changePasswordTableView.register(BottomButtonCell.self)
        self.changePasswordTableView.register(SignupButtonCell.self)
    }
    
    //MARK: - API Methods
    func requestChangePasswordAPI() {
        
        self.viewModel?.requestChangePasswordAPI(arrData: self.chnagePasswordDataSource, paramInfo: self.paramInfo, changePasswordType: changePasswordType) { (success) in
            if success {
                //ShowHome
                Threads.performTaskInMainQueue {
                    if let senderType = self.paramInfo[ConstantAPIKeys.senderType] as? ChangePwdSenderType {
                        switch senderType {
                        case .forgotPassword:
                            AppDelegate.delegate.showLogin()
                        case .profile:
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        }
    }
    
    
}
