//
//  ChangePasswordViewC+TableView.swift.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import Foundation
import UIKit

extension ChangePasswordViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chnagePasswordDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.chnagePasswordDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.chnagePasswordDataSource[indexPath.row].height
    }
}

extension ChangePasswordViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .TextInputCell:
            let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .BottomLabelRegularTextCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureViewWithLightText(cellInfo: cellInfo)
            return cell
        case .BottomButtonCell:
            let cell: BottomButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            cell.delegate = self
            return cell
        case .SignupButtonCell:
            let cell: SignupButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - SignupButtonCellDelegate
extension ChangePasswordViewC: SignupButtonCellDelegate {
    func didTapSignupButton(cell: SignupButtonCell) {
        
    }
}
//MARK: - TextInputCellDelegate
extension ChangePasswordViewC: TextInputCellDelegate {
  func tapNextKeyboard(cell: TextInputCell) {
    self.view.endEditing(true)
  }
  
  func didChangeText(cell: TextInputCell, text: String) {
    guard let indexPath = self.changePasswordTableView.indexPath(for: cell) else { return }
    self.chnagePasswordDataSource[indexPath.row].value = text
  }
}

//MARK: - BottomButtonCellDelegate
extension ChangePasswordViewC: BottomButtonCellDelegate {
  func didTapBottomButton(cell: BottomButtonCell) {
    self.view.endEditing(true)
    self.requestChangePasswordAPI()
  }
}
