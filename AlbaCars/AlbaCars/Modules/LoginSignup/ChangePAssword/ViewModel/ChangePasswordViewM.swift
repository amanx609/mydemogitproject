//
//  ChangePasswordViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol ChangePasswordVModeling: BaseVModeling {
    func getChangePasswordDataSource(changePasswordType: ChangePasswordType) -> [CellInfo]
    func requestChangePasswordAPI(arrData: [CellInfo],paramInfo: [String: AnyObject],changePasswordType: ChangePasswordType, completion: @escaping (Bool)-> Void)
}

class ChangePasswordViewM: ChangePasswordVModeling {
    
    func getChangePasswordDataSource(changePasswordType: ChangePasswordType) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Old Password Cell
        var passwordInfo = [String: AnyObject]()
        passwordInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "password")
        passwordInfo[Constants.UIKeys.inputType] = TextInputType.password as AnyObject
        let passwordCell = CellInfo(cellType: .TextInputCell, placeHolder: changePasswordType == .changePassword ? "Old Password".localizedString(): "Temporary password".localizedString(), value: "", info: passwordInfo, isSecureText: true, height: Constants.CellHeightConstants.height_80)
        array.append(passwordCell)
        
        //New Password Cell
        var newPasswordInfo = [String: AnyObject]()
        newPasswordInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "password")
        newPasswordInfo[Constants.UIKeys.inputType] = TextInputType.password as AnyObject
        let newPasswordCell = CellInfo(cellType: .TextInputCell, placeHolder: "New Password".localizedString(), value: "", info: newPasswordInfo, isSecureText: true, height: Constants.CellHeightConstants.height_80)
        array.append(newPasswordCell)
        
        //Confirm New Password Cell
        var confirmNewPasswordInfo = [String: AnyObject]()
        confirmNewPasswordInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "password")
        confirmNewPasswordInfo[Constants.UIKeys.inputType] = TextInputType.password as AnyObject
        let confirmNewPasswordCell = CellInfo(cellType: .TextInputCell, placeHolder: "Confirm New Password".localizedString(), value: "", info: confirmNewPasswordInfo, isSecureText: true, height: Constants.CellHeightConstants.height_80)
        array.append(confirmNewPasswordCell)
        
        //Submit Cell
        let submitCell = CellInfo(cellType: .BottomButtonCell, placeHolder: "Submit".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_130)
        array.append(submitCell)
        
        return array
    }
    
    //MARK: - API Methods
    func requestChangePasswordAPI(arrData: [CellInfo], paramInfo: [String: AnyObject],changePasswordType: ChangePasswordType, completion: @escaping (Bool)-> Void) {
       if self.validateChangePasswordData(arrData: arrData,changePasswordType: changePasswordType) {
        let params = self.getChangePasswordParams(arrData: arrData, paramInfo: paramInfo)
        guard let senderType = paramInfo[ConstantAPIKeys.senderType] as? ChangePwdSenderType else { return }
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .changepassword(param: params, senderType: senderType))) { (response, success) in
           if success {
             if let safeResponse =  response as? [String: AnyObject],
               let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
               {
                completion(success)
             }
           }
         }
       }
     }
    
    //MARK: - Private Methods
     //DataValidationsBeforeAPICall
     private func validateChangePasswordData(arrData: [CellInfo],changePasswordType: ChangePasswordType) -> Bool {
       var isValid = true
       if let email = arrData[0].value, email.isEmpty {
        Alert.showOkAlert(title: StringConstants.Text.AppName, message: changePasswordType == .changePassword ? "Please enter old password.".localizedString() : "Please enter temporary password.".localizedString())
         isValid = false
       } else if let newPassword = arrData[1].value, newPassword.isEmpty {
         Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter new password.".localizedString())
         isValid = false
       } else if let confirmPassword = arrData[2].value, confirmPassword.isEmpty {
         Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter confirm new password.".localizedString())
        
         isValid = false
       } else if let confirmPassword = arrData[2].value, let newPassword = arrData[1].value, confirmPassword != newPassword{
          Alert.showOkAlert(title: StringConstants.Text.AppName, message: "New password and confirm new password doesn't match.".localizedString())
         
          isValid = false
        }
       return isValid
     }
    
    private func getChangePasswordParams(arrData: [CellInfo], paramInfo: [String: AnyObject])-> APIParams {
      var params: APIParams = APIParams()
        if let senderType = paramInfo[ConstantAPIKeys.senderType] as? ChangePwdSenderType {
            switch senderType {
                  case .forgotPassword:
                    let mobile = paramInfo[ConstantAPIKeys.mobile] as? String ?? ""
                    let countryCode = paramInfo[ConstantAPIKeys.countryCode] as? String ?? ""
                    let email = paramInfo[ConstantAPIKeys.email] as? String ?? ""

                    params[ConstantAPIKeys.mobile] = mobile as AnyObject
                    params[ConstantAPIKeys.countryCode] = countryCode as AnyObject
                    params[ConstantAPIKeys.email] = email as AnyObject
                    params[ConstantAPIKeys.currentPassword] = arrData[0].value as AnyObject
                    params[ConstantAPIKeys.newPassword] = arrData[1].value as AnyObject
                    
                      
                  case .profile:
                        let email = UserSession.shared.email
                        params[ConstantAPIKeys.email] = email as AnyObject
                        params[ConstantAPIKeys.currentPassword] = arrData[0].value as AnyObject
                        params[ConstantAPIKeys.newPassword] = arrData[1].value as AnyObject
                  }
        }
      return params
    }
}
