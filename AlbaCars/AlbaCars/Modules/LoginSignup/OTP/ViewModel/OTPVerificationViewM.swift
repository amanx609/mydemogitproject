//
//  OTPVerificationViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 11/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

protocol OTPVerificationVModeling {
    func requestVerifyOTPAPI(param: APIParams, completion: @escaping (Bool)-> Void)
    func requestResendOTPAPI(param: APIParams, completion: @escaping (Bool)-> Void)
    
}

class OTPVerificationViewM: OTPVerificationVModeling {
    
    //MARK: - API Methods
    func requestVerifyOTPAPI(param: APIParams, completion: @escaping (Bool)-> Void) {
        if self.validateVerifyOTPData(param: param) {
            //let verifyOTPParams = self.getVerifyOTPParams(param: param)
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .verifyOTP(param: param))) { (response, success) in
                if success {
                    if let safeResponse =  response as? [String: AnyObject],
                        let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject] {
                        completion(success)
                    }
                } else {
                    completion(success)
                }
            }
        }
    }
    
    func requestResendOTPAPI(param: APIParams, completion: @escaping (Bool)-> Void) {
        if self.validateVerifyOTPData(param: param) {
            //let verifyOTPParams = self.getVerifyOTPParams(param: param)
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .sendOTP(param: param))) { (response, success) in
                if success {
                    if let safeResponse =  response as? [String: AnyObject],
                        let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject] {
                        completion(success)
                    }
                } else {
                    completion(success)
                }
            }
        }
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
    private func validateVerifyOTPData(param: [String: AnyObject]) -> Bool {
        var isValid = true
        if let mobile = param[ConstantAPIKeys.mobile] as? String, mobile.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Mobile number not found.".localizedString())
            isValid = false
        } else if let countryCode = param[ConstantAPIKeys.countryCode] as? String, countryCode.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Country code not found.".localizedString())
            isValid = false
        } else if let otp = param[ConstantAPIKeys.otp] as? String {
            if otp.isEmpty || otp.count < 4 {
                Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter valid OTP.".localizedString())
                isValid = false
            }
            
        }
        return isValid
    }
    
    private func getVerifyOTPParams(param: [String: AnyObject])-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.countryCode] = param[ConstantAPIKeys.countryCode] as AnyObject
        params[ConstantAPIKeys.mobile] = param[ConstantAPIKeys.mobile] as AnyObject
        params[ConstantAPIKeys.otp] = param[ConstantAPIKeys.otp] as AnyObject
        return params
    }

}
