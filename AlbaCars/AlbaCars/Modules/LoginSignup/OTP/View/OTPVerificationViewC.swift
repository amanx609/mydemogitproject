//
//  OTPVerificationViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

class OTPVerificationViewC: BaseViewC {
  
  //MARK: - IBOutlets
  
  @IBOutlet weak var codeFirstTextField: OTPTextField!
  @IBOutlet weak var codeSecondTextField: OTPTextField!
  @IBOutlet weak var codeThirdTextField: OTPTextField!
  @IBOutlet weak var codeFourthTextField: OTPTextField!
  @IBOutlet weak var resendButton: UIButton!
  @IBOutlet weak var verifyButton: UIButton!
  @IBOutlet weak var resendTimerLabel: UILabel!
  @IBOutlet weak var wehavesentCodeLabel: UILabel!
  @IBOutlet weak var didntReceiveLabel: UILabel!
  @IBOutlet var tableFooterView: UIView!
  @IBOutlet var tableHeaderView: UIView!
  @IBOutlet weak var otpVerificationTableView: TPKeyboardAvoidingTableView!
  
  //MARK: - Variables
  var viewModel: OTPVerificationVModeling?
  var otp = ""
  var user: User?
  var editedPhoneNumber: String = ""
  
  //MARK: - Lifecycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }
  
  deinit {
    self.removeNotifications()
    print("deinit OTPVerificationViewC")
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(false, animated: false)
  }
  
  override func navigationButtonTapped(_ sender: AnyObject) {
    guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
    switch buttonType {
    case .back: //self.goToPreviousScreen()
         self.navigationController?.popToRootViewController(animated: true)
    default: break
    }
  }
  
  //MARK: - Private Methods
  
  private func setup() {
    self.setupNavigationBarTitle(title: "OTP Verification".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
    if self.user == nil {
        self.user = UserDefaultsManager.sharedInstance.getUserValueFor(key: .user)
    }
    self.registerNotifications()
    self.setupResendButton()
    self.setupTable()
    self.setupRoundCorners()
    self.setupTexts()
    self.setupTextFields()
    self.codeFirstTextField.becomeFirstResponder()
    
  }
  
  private func registerNotifications() {
    NotificationCenter.default.addObserver(self, selector: #selector(timeUpdated(_:)), name: NSNotification.Name(rawValue: AppNotifications.timeUpdated.rawValue), object: nil)
  }
  
  private func removeNotifications() {
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: AppNotifications.timeUpdated.rawValue), object: nil)
  }
  
  private func setupTexts() {
    self.didntReceiveLabel.text = StringConstants.Text.didntReceive.localizedString()
    if !self.editedPhoneNumber.isEmpty {
      if let countryCode = self.user?.countryCode {
        let phone = countryCode + self.editedPhoneNumber
        self.wehavesentCodeLabel.text = StringConstants.Text.otpSent.localizedString() + " \(phone.phoneFormat())"
      }
    } else {
      if let mobile = self.user?.mobile, let countryCode = self.user?.countryCode {
        let phone = countryCode + mobile
        self.wehavesentCodeLabel.text = StringConstants.Text.otpSent.localizedString() + " \(phone.phoneFormat())"
      }
    }
    
    
    verifyButton.setTitle(StringConstants.Text.verifyOTP.localizedString(), for: .normal)
  }
  
  private func setupResendButton() {
    self.resendButton.setTitleColor(UIColor.gray, for: .normal)
    self.resendButton.isEnabled = false
    self.resendTimerLabel.text = StringConstants.TimeTexts.time_min_3
    self.resendTimerLabel.isHidden = false
    TimerManager.sharedInstance.stopTimer()
    TimerManager.sharedInstance.initializeTimer(counter: Constants.Validations.resendOTPStartTime)
  }
  
  private func setupRoundCorners() {
    verifyButton.roundCorners(Constants.UIConstants.sizeRadius_7)
  }
  
  private func setupTextFields() {
    
    codeFirstTextField.delegate = self
    codeSecondTextField.delegate = self
    codeThirdTextField.delegate = self
    codeFourthTextField.delegate = self
    codeFirstTextField.makeLayer(color: UIColor.black, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    codeSecondTextField.makeLayer(color: UIColor.black, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    codeThirdTextField.makeLayer(color: UIColor.black, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    codeFourthTextField.makeLayer(color: UIColor.black, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
  }
  
  private func setupTable() {
    self.otpVerificationTableView.tableHeaderView = tableHeaderView
    self.otpVerificationTableView.sectionHeaderHeight = 200
    self.otpVerificationTableView.tableFooterView = tableFooterView
    self.otpVerificationTableView.sectionFooterHeight = 100
    self.otpVerificationTableView.backgroundColor = UIColor.white
    
  }
  
  private func goToPreviousScreen() {
    if let vcCount = self.navigationController?.viewControllers.count,
      let _ = self.navigationController?.viewControllers[vcCount - 2] as? LoginViewC {
      self.navigationController?.setNavigationBarHidden(true, animated: false)
      self.navigationController?.popViewController(animated: true)
    } else {
      self.navigationController?.popViewController(animated: true)
    }
  }
  
  private func clearTextFields() {
    self.codeFirstTextField.text = ""
    self.codeSecondTextField.text = ""
    self.codeThirdTextField.text = ""
    self.codeFourthTextField.text = ""
    self.codeFirstTextField.becomeFirstResponder()
  }
  
  private func moveForwardDirection(textField: UITextField, isForward: Bool) {
    switch textField {
    case self.codeFirstTextField:
      if isForward {
        self.codeSecondTextField.becomeFirstResponder()
      }
    case self.codeSecondTextField:
      if isForward {
        self.codeThirdTextField.becomeFirstResponder()
      } else {
        self.codeSecondTextField.text = ""
        self.codeFirstTextField.becomeFirstResponder()
      }
    case self.codeThirdTextField:
      if isForward {
        self.codeFourthTextField.becomeFirstResponder()
      } else {
        self.codeThirdTextField.text = ""
        self.codeSecondTextField.becomeFirstResponder()
      }
    case self.codeFourthTextField:
      if isForward {
        //self.callVerifyOTPAPI()
        self.codeFourthTextField.resignFirstResponder()
      } else {
        self.codeFourthTextField.text = ""
        self.codeThirdTextField.becomeFirstResponder()
      }
    default: break
    }
  }
  
  private func getOTP() {
    if let codeFirst = self.codeFirstTextField.text, let codeSecond = self.codeSecondTextField.text, let codeThird = self.codeThirdTextField.text, let codeFourth = self.codeFourthTextField.text {
      self.otp = codeFirst + codeSecond + codeThird + codeFourth
    }
  }
  
  private func saveUserId() {
    if let userTypeVal = UserDefaultsManager.sharedInstance.getIntValueFor(key: .userType), let userType = UserType(rawValue: userTypeVal) {
      if userType == .customer {
        let userId = UserSession.shared.getUserId()
        UserDefaultsManager.sharedInstance.saveIntValueFor(key: .userId, value: userId)
      }
    }
  }
  
  private func goToNextPage() {
    if let userTypeValue = UserDefaultsManager.sharedInstance.getIntValueFor(key: .userType) {
      let userType = UserType(rawValue: userTypeValue)
      switch userType {
      case .customer: AppDelegate.delegate.showHome()
      case .dealer:
        let documentViewC = DIConfigurator.sharedInstance.getUploadDocumentViewC()
        self.navigationController?.pushViewController(documentViewC, animated: true)
      case .supplier:  let chooseServiceViewC = DIConfigurator.sharedInstance.getSChooseServiceVC()
      self.navigationController?.pushViewController(chooseServiceViewC, animated: true)
      case .none:
        break
      }
    }
  }
  
  //MARK: - Selectors
  @objc func timeUpdated(_ notificaion: NSNotification) {
    if TimerManager.sharedInstance.timeLabelText == StringConstants.TimeTexts.time_0 {
      self.resendTimerLabel.isHidden = true
      TimerManager.sharedInstance.stopTimer()
      self.resendTimerLabel.text = StringConstants.TimeTexts.time_0
      self.resendButton.setTitleColor(UIColor.redButtonColor, for: .normal)
      self.resendButton.isEnabled = true
    } else {
      self.resendTimerLabel.text = TimerManager.sharedInstance.timeLabelText
    }
  }
  
  //MARK: - API Methods
  private func callVerifyOTPAPI() {
    let param = getAPIParameters(isResend: false)
    self.viewModel?.requestVerifyOTPAPI(param: param, completion: { [weak self] (success) in
      guard let sSelf = self else { return }
      if success {
        //UserIsChangingPhoneNumberFromEditProfile
        if !sSelf.editedPhoneNumber.isEmpty {
          Threads.performTaskInMainQueue {
            if let user = UserDefaultsManager.sharedInstance.getUserValueFor(key: .user) {
              user.mobile = sSelf.editedPhoneNumber
              UserDefaultsManager.sharedInstance.saveUserValueFor(key: .user, value: user)
            }
            sSelf.navigationController?.popToRootViewController(animated: true)
          }
        } else {
          //UserIsRegisteringNumberFromSignup
          Threads.performTaskInMainQueue {
            sSelf.saveUserId()
            sSelf.goToNextPage()
          }
        }
      } else {
        sSelf.clearTextFields()
      }
    })
  }
  
  private func requestResendOTPAPI() {
    let param = getAPIParameters(isResend: true)
    self.viewModel?.requestResendOTPAPI(param: param, completion: { (success) in
      Threads.performTaskInMainQueue {
        self.setupResendButton()
      }
    })
  }
    
  private func getAPIParameters(isResend: Bool) -> APIParams {
    self.getOTP()
    var param: APIParams = APIParams()
    if !isResend {
      param[ConstantAPIKeys.otp] = self.otp as AnyObject
    }
    if !self.editedPhoneNumber.isEmpty {
      if let countryCode = self.user?.countryCode {
        param[ConstantAPIKeys.countryCode] = countryCode as AnyObject
        param[ConstantAPIKeys.mobile] = self.editedPhoneNumber as AnyObject
      }
    } else {
      if let countryCode = self.user?.countryCode, let mobile = self.user?.mobile {
        param[ConstantAPIKeys.countryCode] = countryCode as AnyObject
        param[ConstantAPIKeys.mobile] = mobile  as AnyObject
      }
    }
    return param
  }
  
  //MARK: - IBActions
  
  @IBAction func verifyButtonTapped(_ sender: Any) {
    self.callVerifyOTPAPI()
    
  }
  
  @IBAction func resendButtonTapped(_ sender: Any) {
    self.requestResendOTPAPI()
  }
}

extension OTPVerificationViewC: OTPTextFieldDelegate {
  
  func backspacePressed(textField: UITextField) {
    self.moveForwardDirection(textField: textField, isForward: false)
  }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if string.isEmpty {
      textField.text = ""
      self.moveForwardDirection(textField: textField, isForward: false)
    } else {
      textField.text = string
      self.moveForwardDirection(textField: textField, isForward: true)
    }
    return false
    
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == self.codeFirstTextField {
      self.codeSecondTextField.becomeFirstResponder()
      return false
    } else if textField == self.codeSecondTextField {
      self.codeThirdTextField.becomeFirstResponder()
      return false
    } else if textField == self.codeThirdTextField {
      self.codeFourthTextField.becomeFirstResponder()
      return false
    } else if textField == self.codeFourthTextField {
      textField.resignFirstResponder()
      return true
    }
    return true
  }
}

