//
//  MyCarDetailsViewM.swift
//  AlbaCars
//
//  Created by Narendra on 6/19/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol MyCarDetailsViewModeling {

    func getDataSource(myCarsModel: MyCarsModel?) -> [CellInfo]
    func requestDeleteCarAPI(carId:Int, completion: @escaping (Bool)-> Void)
    func requestMyCarDetailsAPI(carId: Int, completion: @escaping (Any,Bool)-> Void)
    func requestUploadHistoryCarAPI(vehicleId:Int,documentUrl:String, completion: @escaping (Bool)-> Void)
}

class MyCarDetailsViewM: MyCarDetailsViewModeling {
    
    func getDataSource(myCarsModel: MyCarsModel?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        // Car Brand Cell
        let brandCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Car Brand :".localizedString(), value: Helper.toString(object: myCarsModel?.brandName), height: Constants.CellHeightConstants.height_40)
        array.append(brandCell)
        
        // Car model Cell
        let modelCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Car Model :".localizedString(), value: Helper.toString(object: myCarsModel?.modelName), height: Constants.CellHeightConstants.height_40)
        array.append(modelCell)
        
        // Car type
        let carTypeCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Car Type :".localizedString(), value: Helper.toString(object: myCarsModel?.typeName), height: Constants.CellHeightConstants.height_40)
        array.append(carTypeCell)
        
        // select car year
        let carYearCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Car year :".localizedString(), value: Helper.toString(object: myCarsModel?.year), info: nil, height: Constants.CellHeightConstants.height_40)
        array.append(carYearCell)
        
        // select carPlate
        let carPlateCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Car Plate/ VIN Number :".localizedString(), value: Helper.toString(object: myCarsModel?.plateNumber), info: nil,  height: Constants.CellHeightConstants.height_40)
        array.append(carPlateCell)
        
        // select carMileage
        let carMileageCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Car Mileage :".localizedString(), value: Helper.toString(object: myCarsModel?.mileage), info: nil, height: Constants.CellHeightConstants.height_40)
        array.append(carMileageCell)
        
        //No. of cylinders
        let cylinderNum = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Cylinders :", value: Helper.toString(object: myCarsModel?.cylinders), height: Constants.CellHeightConstants.height_40)
        array.append(cylinderNum)
        
        // engine Size
        let engineSizeCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Engine Size :".localizedString(), value: Helper.toString(object: myCarsModel?.engine), info: nil, height: Constants.CellHeightConstants.height_40, keyboardType: UIKeyboardType.decimalPad)
        array.append(engineSizeCell)
        
        // Service History Title Cell
        let chooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Service History".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(chooseLabelCell)
        
        // Service History Cell
        
        if let serviceHistoryDocument = myCarsModel?.serviceHistoryDocuments, serviceHistoryDocument.count > 0 {
            
            var count = 0
            if serviceHistoryDocument.count.isMultiple(of: 2){
                count = serviceHistoryDocument.count/2
            } else {
                count = Int(serviceHistoryDocument.count/2) + 1
            }
            
            let serviceHistoryCell = CellInfo(cellType: .MyCarDetailsCell, placeHolder: "", value: "", info: nil, height: CGFloat(count) * Constants.CellHeightConstants.height_140)
            array.append(serviceHistoryCell)
        }
                
        return array
        
    }
    
    func requestDeleteCarAPI(carId:Int, completion: @escaping (Bool)-> Void) {
    
        let deleteCarParams = self.getDeleteCarParams(carId: carId)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .deleteCar(param: deleteCarParams))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject] {
                    completion(success)
                }
            }
        }
        
    }
    
    func requestUploadHistoryCarAPI(vehicleId:Int,documentUrl:String, completion: @escaping (Bool)-> Void) {
    
        let params = self.getUploadHistoryCarParams(vehicleId: vehicleId, documentUrl: documentUrl)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .uploadServiceDocument(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject] {
                    completion(success)
                }
            }
        }
        
    }
    
    private func getUploadHistoryCarParams(vehicleId:Int,documentUrl:String)-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        params[ConstantAPIKeys.document] = documentUrl as AnyObject
        return params
    }
    
    private func getDeleteCarParams(carId:Int)-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.id] = carId as AnyObject
        return params
    }
    
    func requestMyCarDetailsAPI(carId: Int, completion: @escaping (Any,Bool) -> Void) {
        let param = getAPIParams(carId: carId)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getCarDetails(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let carModel = try decoder.decode(MyCarsModel.self, from: resultData)
                        completion(carModel, success)
                    } catch let error {
                        print(error)
                    }
                }
            }
            
        }
    }
    
    private func getAPIParams(carId: Int) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.id] = carId as AnyObject
        return param
    }

}
