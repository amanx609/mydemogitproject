//
//  MyCarDetailsViewC.swift
//  AlbaCars
//
//  Created by Narendra on 6/19/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
import PDFReader

class MyCarDetailsViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var carTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    
    //MARK: - Properties
    var viewModel: MyCarDetailsViewModeling?
    var carDataSource: [CellInfo] = []
    var myCarsModel: MyCarsModel?
    var vehicleId = 0
    var carDetail:Bool = false
    var documentUrl:String = ""
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Loader.hideLoader()
    }
    
    //MARK: - Private Methods
    private func setup() {
        setupViewModel()
        registerNibs()
        setupTableView()
        self.setupNavigationBarTitle(title: "My Car Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        
        if self.carDetail {
            self.requestMyCarDetailsAPI()
        } else {
            self.setupDataSource()
            carTableView.tableHeaderView = self.headerView
        }
        setupView()
        roundCorners()
    }
    
    private func setupTableView() {
        self.carTableView.separatorStyle = .none
        carTableView.backgroundColor = UIColor.white
        carTableView.delegate = self
        carTableView.dataSource = self
        //carTableView.tableHeaderView = self.headerView
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = MyCarDetailsViewM()
        }
    }
    
    private func roundCorners() {
        Threads.performTaskInMainQueue  {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
        }
    }
    
    private func setupView() {
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        self.uploadButton.setTitle(StringConstants.Text.uploadhistory.localizedString(), for: .normal)
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
    }
    
    private func setupDataSource() {
        if let dataSource = self.viewModel?.getDataSource(myCarsModel: myCarsModel) {
            self.carDataSource = dataSource
        }
        
        Threads.performTaskInMainQueue {
            self.carTableView.reloadData()
        }
        
    }
    
    private func registerNibs() {
        self.carTableView.register(DLACarDetailCell.self)
        self.carTableView.register(BottomLabelCell.self)
        self.carTableView.register(MyCarDetailsCell.self)
    }
    
    func requestDeleteCarAPI() {
        self.viewModel?.requestDeleteCarAPI(carId: Helper.toInt(myCarsModel?.id), completion: { (success) in
            
            if success {
                
                Threads.performTaskInMainQueue {
                    
                    Alert.showOkAlertWithCallBack(title: StringConstants.Text.AppName.localizedString(), message: "Deleted successfully".localizedString()) { (_) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    // self.navigationController?.popViewController(animated: true)
                }
                
            }
            
        })
    }
    
    func requestUploadHistoryCarAPI() {
        
        Threads.performTaskAfterDealy(0.2) {
            
            self.viewModel?.requestUploadHistoryCarAPI(vehicleId: Helper.toInt(self.myCarsModel?.id), documentUrl: self.documentUrl, completion: { (success) in
                
                if success {
                    Alert.showOkAlertWithCallBack(title: StringConstants.Text.AppName.localizedString(), message: "Uploaded successfully".localizedString()) { (_) in
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            })
        }
        
    }
    
    func requestMyCarDetailsAPI() {
        
        self.viewModel?.requestMyCarDetailsAPI(carId: Helper.toInt(vehicleId), completion: { (response,success) in
            
            if success {
                
                if let data = response as? MyCarsModel {
                    self.myCarsModel = data
                    Threads.performTaskInMainQueue {
                        self.setupDataSource()
                    }
                }
                                
            }
            
        })
    }
 
    func openPdfFile(documentFileURL: URL) {
        Threads.performTaskInMainQueue {
                    let pdfReaderVC = DIConfigurator.sharedInstance.getPDFReaderVC()
                    pdfReaderVC.pdfFileUrl = documentFileURL
                    self.navigationController?.pushViewController(pdfReaderVC, animated: true)
        //            if let document = PDFDocument(url: documentFileURL) {
        //                // Add PDFView to view controller.
        //                let pdfView = PDFView(frame: self.view.bounds)
        //                pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //                self.view.addSubview(pdfView)
        //
        //                // Fit content in PDFView.
        //                pdfView.autoScales = true
        //                pdfView.document = document
        //                let readerController = PDFViewController.createNew(with: document)
        //                readerController.scrollDirection = .vertical
        //                self.navigationController?.pushViewController(readerController, animated: true)
                   // }
                }
    }
    
    private func selectDocument() {
        
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage(isDocument: true) { (image, url) in
                if let image = image {
                    
                    let imageSize = image.sizeOfImage()
                    if imageSize <= 25 {
                        self.uploadImageToS3(image: image)
                        
                    } else {
                        Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Document size cannot be greater than 25 MB.".localizedString())
                    }
                } else if let url = url {
                    do {
                        if  url.pathExtension.uppercased() == "PDF" {
                            if Helper.checkFileSize(path: url.path, maxSize: 25.0)  {
                                self.uploadDoc(url)
                            } else {
                                Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Document size cannot be greater than 25 MB.".localizedString())
                            }
                        } else {
                            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please upload PDF file.".localizedString())
                        }
                    }
                }
            }
        }
    }
    
      private func uploadImageToS3(image: UIImage) {
          

          Loader.showLoader()
          S3Manager.sharedInstance.uploadImage(image: image, imageQuality: .low, progress: nil) { (response, imageName, error) in
              
              Threads.performTaskInMainQueue {
                  Loader.hideLoader()
              }
              
              if error == nil {
                  
                 guard let imageName = imageName as? String else { return }
                 self.documentUrl = imageName
                 self.requestUploadHistoryCarAPI()
                  
              } else {
                  Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please upload it again.".localizedString())
              }
    
          }

      }
      
      private func uploadDoc(_ fileURL: URL) {
         
          Loader.showLoader()

          S3Manager.sharedInstance.uploadOtherFile(fileUrl: fileURL, conentType: "File", progress: nil) { (response, imageName, error) in
              
              Threads.performTaskInMainQueue {
                  Loader.hideLoader()
              }
              
              if error == nil {
                  
                  guard let imageName = imageName as? String else { return }
                  self.documentUrl = Helper.toString(object: imageName)
                   self.requestUploadHistoryCarAPI()
                  
              } else {
                  Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please upload it again.".localizedString())
              }
              
          }
              
      }
    
    //MARK: - IBActions
    @IBAction func editButtonTapped(_ sender: Any) {
        let addNewCarViewC = DIConfigurator.sharedInstance.getAddNewCarViewC()
        addNewCarViewC.cAddNewCarType = .edit
        addNewCarViewC.myCarsModel = self.myCarsModel
        self.navigationController?.pushViewController(addNewCarViewC, animated: true)
    }
    
    @IBAction func deleteButtonTapped(_ sender: Any) {
    
        Alert.showAlertWithActionWithCancel(title: StringConstants.Text.AppName.localizedString(), message: StringConstants.Message.areYouSureYouWantToDelete.localizedString(), actionTitle: StringConstants.Text.Yes.localizedString(), cancelTitle: StringConstants.Text.No.localizedString(), action: { (_) in
            self.requestDeleteCarAPI()
        }, cancelAction: { (_) in

        })
        
    }
    
    @IBAction func uploadButtonTapped(_ sender: Any) {
        self.selectDocument()
    }
    
}
