//
//  MyCarDetailsCollectionCell.swift
//  AlbaCars
//
//  Created by Narendra on 6/22/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class MyCarDetailsCollectionCell: BaseCollectionViewCell, NibLoadableView, ReusableView {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var placeHolderImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureView(serviceHistory: ServiceHistoryDocuments) {
        
        self.dateLabel.text = Helper.toString(object: serviceHistory.createdAt).getDateStringFrom(inputFormat: Constants.Format.apiDateTimeFormat, outputFormat: Constants.Format.dateFormatMMMdyyyy)
        
        if let imageName = serviceHistory.document {
            
            if imageName.contains(".pdf") {
                self.placeHolderImageView.setImage(urlStr:imageName, placeHolderImage: UIImage(named: "pdficon"))
            } else {
                self.placeHolderImageView.setImage(urlStr:imageName, placeHolderImage: nil)
            }
        }
    }

}
