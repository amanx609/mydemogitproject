//
//  MyCarDetailsCell.swift
//  AlbaCars
//
//  Created by Narendra on 6/22/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol MyCarDetailsCellDelegate: class {
    func didTapMyCarDetailsCell(fileName:String)
}

class MyCarDetailsCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!

    weak var delegate: MyCarDetailsCellDelegate?
    var dataSource: [ServiceHistoryDocuments] = []

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setupCollectionView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupCollectionView() {
        self.registerNibs()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //depending upon direction of collection view
        self.collectionView?.setCollectionViewLayout(layout, animated: true)
    }
    
    private func registerNibs() {
        self.collectionView.register(MyCarDetailsCollectionCell.self)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
       // dataSource = ["1584685181apache-jmeter.pdf"]
        Threads.performTaskInMainQueue {
            self.collectionView.reloadData()
        }
        
    }
}


extension MyCarDetailsCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getCell(collectionView: collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/2 - 5
        return CGSize(width: width, height: Constants.CellHeightConstants.height_100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        if let delegate = self.delegate {
            
            let serviceHistory = self.dataSource[indexPath.row]

            if let imageName = serviceHistory.document {
                
                if imageName.contains(".pdf") {
                    delegate.didTapMyCarDetailsCell(fileName: self.dataSource[indexPath.row].document ?? "")
                } else {

                }
            }
                        
        }
    }
    
}

extension MyCarDetailsCell {
    
    func getCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MyCarDetailsCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        let serviceHistory = self.dataSource[indexPath.row]
        cell.configureView(serviceHistory: serviceHistory)
        return cell
    }
}
