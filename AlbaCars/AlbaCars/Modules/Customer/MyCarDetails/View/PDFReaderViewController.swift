//
//  PDFReaderViewController.swift
//  AlbaCars
//
//  Created by Apple on 12/11/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
import PDFKit

class PDFReaderViewController: BaseViewC {
    
    var pdfFileUrl:URL?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Service History Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.openPDF(documentFileURL: pdfFileUrl!)
    }
    
    func openPDF(documentFileURL: URL){
        if let document = PDFDocument(url: documentFileURL) {
        // Add PDFView to view controller.
            let pdfView = PDFView(frame: self.view.bounds)
            pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.view.addSubview(pdfView)
            
            // Fit content in PDFView.
            pdfView.autoScales = true
            pdfView.document = document
        }
    }


}
