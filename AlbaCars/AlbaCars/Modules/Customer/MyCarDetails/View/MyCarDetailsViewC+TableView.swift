//
//  MyCarDetailsViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 6/19/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit
import PDFReader

extension MyCarDetailsViewC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.carDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.carDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.carDataSource[indexPath.row].height
    }
}

extension MyCarDetailsViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .DLACarDetailCell:
            let cell: DLACarDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureMyCarDetailsView(cellInfo: cellInfo)
            return cell
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureViewWithLightText(cellInfo: cellInfo)
            return cell
        case .MyCarDetailsCell:
            let cell: MyCarDetailsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            
            if let serviceHistoryDocument = myCarsModel?.serviceHistoryDocuments {
                cell.dataSource = serviceHistoryDocument
//                Threads.performTaskInMainQueue {
//                    cell.collectionView.reloadData()
//                }
            }
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

extension MyCarDetailsViewC: MyCarDetailsCellDelegate {
    
    func didTapMyCarDetailsCell(fileName: String) {
        
        Threads.performTaskAfterDealy(0.2) {
            
            if fileName.contains("http"), let url = URL(string: fileName) {
                self.openPdfFile(documentFileURL: url)
            } else {
                
                if let prefixS3Url = UserDefaultsManager.sharedInstance.getStringValueFor(key: .prefixS3Url) {
                    if let url = URL(string:  prefixS3Url + fileName){
                        self.openPdfFile(documentFileURL: url)
                    }
                }
            }
            
//            if let prefixS3Url = UserDefaultsManager.sharedInstance.getStringValueFor(key: .prefixS3Url) {
//
//                if Helper.isFileExistsInDirectory(fileName: fileName) {
//
//                    if let localUrl = Helper.urlInDocumentsDirectory(fileName: fileName) {
//                        self.openPdfFile(documentFileURL: localUrl)
//                    }
//
//                } else {
//
//                    if let url = URL(string:  prefixS3Url + fileName), let localUrl = Helper.urlInDocumentsDirectory(fileName: fileName) {
//                        Downloader.load(url: url, to: localUrl) {
//                            Loader.hideLoader()
//                           if let localUrl = Helper.urlInDocumentsDirectory(fileName: fileName) {
//                                self.openPdfFile(documentFileURL: localUrl)
//                            }
//                        }
//                    }
//                }
//
//            }
        }
        
    }

}
