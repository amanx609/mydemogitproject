//
//  CarDetailModel.swift
//  AlbaCars
//
//  Created by Narendra on 1/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

class CarDetailModel: BaseCodable {
    
    //Variables
    var id: Int?
    var carTitle: String?
    var brand: Int?
    var model: Int?
    var type: Int?
    var price: Int?
    var year: Int?
    var mileage: String?
    var financingYear: Int?
    var isCarBooked: Bool?
    var monthlyInstallment: Double?
    var status: String?
    var holdDateTime: String?
    var color: String?
    var vehicleOption: String?
    var vehicleDescription: String?
    var brandName: String?
    var modelName: String?
    var typeName: String?
    var vehicleImages: [String]?
    var offerImage: String?
    var extras: [Extras]?
    var options: [String]?
    var wheelSize: String?
    var serviceHistory: String?
    var warranty: String?
    var downpayment: Int?
    var cylinder: String?
    var contactUs: [ContactUs]?
    var bookingPrice: Int?
    var vat: Int?
    
}

class Extras: BaseCodable {
    
//    //Variables
//    //var free: Bool?
//    var price: String?
//   // var freeValue: String?
//    var title: String?
//
//    private enum CodingKeys:String, CodingKey {
//      case price,title //Server keys are same as we define here
//    }
//
//    required init(from decoder: Decoder) throws {
//      let values = try! decoder.container(keyedBy: CodingKeys.self)
//    //  self.free = try values.decodeIfPresent(Bool.self, forKey: .free)
//      self.title = try values.decodeIfPresent(String.self, forKey: .title)
//      //self.freeValue = try values.decodeIfPresent(String.self, forKey: .freeValue)
//      self.price = try values.decode(String.self, forKey: .price)
//    }
//
    var price: Double?
    var title: String?

    private enum CodingKeys: String, CodingKey {
        case price = "price", title = "title"
    }
    
    init(price: Double? = nil, title: String? = nil) {
        self.price = price
        self.title = title
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.title = try container.decodeIfPresent(String.self, forKey: .title)
        do {
            price = try Double(container.decode(String.self, forKey: .price))
        } catch DecodingError.typeMismatch {
            price = try container.decode(Double.self, forKey: .price)
        }
    }
    
}

//Images
class VehicleImages: BaseCodable {
    //Variables
   var image: String?
}

class ContactUs: BaseCodable {
    
    //Variables
    var image: String?
    var name: String?
    var countryCode: String?
    var mobile: String?
    
    func contactNumber()-> String {
        return Helper.toString(object: countryCode) + Helper.toString(object:mobile)
    }
}
