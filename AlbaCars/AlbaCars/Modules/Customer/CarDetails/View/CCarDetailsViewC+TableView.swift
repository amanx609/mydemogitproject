//
//  CCarDetailsViewC+TableView.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 13/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension CCarDetailsViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carDetailsDatasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.carDetailsDatasource[indexPath.row]
        return self.getCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellInfo = self.carDetailsDatasource[indexPath.row]
        guard let cellType = cellInfo.cellType else { return 0.0 }
        if cellType == .CarDescriptionTableCell {
            return UITableView.automaticDimension
        }
        return self.carDetailsDatasource[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.carDetailsDatasource[indexPath.row].height
    }
}

extension CCarDetailsViewC {
    func getCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
      guard let cellType = cellInfo.cellType else { return UITableViewCell() }
      switch cellType {
      case .CarInfoTableCell:
        return getCarInfoCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
      case .CarFinanceTableCell:
        return getCarFinanceCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
      case .SpecificationOptionsTableCell:
        return getCarSpecificationOptionCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
      case .CarDescriptionTableCell:
        return getCarDescriptionCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
      case .DocumentsRequiredTableCell:
        return getDocumentsRequiredCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
      case .CarContactUsTableCell:
        return getCarContactUsCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
      default:
        return UITableViewCell()
      }
    }
    
    func getCarInfoCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: CarInfoTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(cellInfo: cellInfo)
        return cell
    }
    
    func getCarFinanceCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: CarFinanceTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(cellInfo: cellInfo)
        return cell
    }
    
    func getCarSpecificationOptionCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: SpecificationOptionsTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
       // cell.data = self.optionsDatasource
        cell.configureView(cellInfo: cellInfo)
        return cell
    }
    
    func getCarDescriptionCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: CarDescriptionTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(cellInfo: cellInfo)
        return cell
    }
    
    func getDocumentsRequiredCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: DocumentsRequiredTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.documentRequiredDidTapped = { [weak self] () in
            guard let strongSelf = self else {
                return
            }
            strongSelf.moveToDocumentDetailsVC()
        }
        return cell
    }
    
    func getCarContactUsCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: CarContactUsTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(cellInfo: cellInfo)
        return cell
    }
}
