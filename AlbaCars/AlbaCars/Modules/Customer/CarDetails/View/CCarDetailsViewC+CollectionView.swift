//
//  CCarDetailsViewC+CollectionView.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 13/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//MARK:- UICollectionView Delegates & Datasource Methods
extension CCarDetailsViewC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.vehicleImagessCollectionView {
            return self.vehicleImagesDatasource.count
        }
        return self.extraServicesDatasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return getCell(collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.vehicleImagessCollectionView {
            return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
        }
        
        return CGSize(width: (65.0 * Constants.Devices.ScreenWidth)/375.0, height: Constants.CellHeightConstants.height_106)
        //return CGSize(width: 65.0 , height: Constants.CellHeightConstants.height_106)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //selectBrandOptionDidTapped?(indexPath)
        if collectionView != self.vehicleImagessCollectionView {
            // Add Extras
            if let extras = self.extraServicesDatasource[indexPath.item].info?[Constants.UIKeys.extras] as? Extras {
                
                if let isExtrasAdded = self.extraServicesDatasource[indexPath.item].info?[Constants.UIKeys.isExtrasAdded] as? Bool, isExtrasAdded == false {
                    self.showDropDownAt(indexPath: indexPath)
                } else {
                    self.extraServicesDatasource[indexPath.item].info?[Constants.UIKeys.isExtrasAdded] = false as AnyObject
                    self.priceCalculate()
                    Threads.performTaskInMainQueue {
                        self.extraServicesCollectionView.reloadData()
                    }
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in vehicleImagessCollectionView.visibleCells {
            let indexPath = vehicleImagessCollectionView.indexPath(for: cell)
            self.pageControl.currentPage = indexPath?.row ?? 0
        }
    }
}

extension CCarDetailsViewC {
    func getCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.vehicleImagessCollectionView {
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CarImageCollectionCell.className, for: indexPath) as? CarImageCollectionCell else {
                return UICollectionViewCell()
            }
            cell.configureView(vehicleImages: self.vehicleImagesDatasource[indexPath.row])
            return cell
        }
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExtraServicesCollectionCell.className, for: indexPath) as? ExtraServicesCollectionCell else {
            return UICollectionViewCell()
        }
        cell.data = self.extraServicesDatasource[indexPath.item]
        return cell
    }
}
