//
//  CCarDetailsViewC.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 12/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CCarDetailsViewC: BaseViewC {
    
    //MARK:- IBOutlets
    
    @IBOutlet weak var extrasCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var carDetailsTableView: UITableView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var separatorLabel: UILabel!
    @IBOutlet weak var bookNowView: UIView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var sideLabel: UILabel!
    @IBOutlet weak var bookNowButton: UIButton!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var extrasLabel: UILabel!
    @IBOutlet weak var tapHereLabel: UILabel!
    @IBOutlet weak var extraServicesCollectionView: UICollectionView!
    @IBOutlet weak var carPriceLabel: UILabel!
    @IBOutlet weak var bookNowLabel: UILabel!
    @IBOutlet weak var friendsThinkingLabel: UILabel!
    @IBOutlet weak var shareWithFriendButton: UIButton!
    @IBOutlet weak var vehicleImagessCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    //MARK:- Variables
    var viewModel: CarDetailsVModeling?
    var carDetailsDatasource = [CellInfo]()
    var optionsDatasource = [String]()
    var extraServicesDatasource = [CellInfo]()
    var carDetailModel = CarDetailModel()
    var vehicleImagesDatasource = [String]()
    var vehicleId = 0
    
    //MARK:- LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Car Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
    }
    
    //MARK:- Private Methods
    private func setUp() {
        self.recheckVM()
        self.localizeTexts()
        self.getDatasourceForCarDetails()
        self.separatorLabel.roundCorners(Constants.UIConstants.sizeRadius_4)
        self.bookNowButton.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.gradientView.roundCorners(Constants.UIConstants.sizeRadius_7)
        Threads.performTaskInMainQueue {
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.setUpTableView()
        self.setUpCollectionView()
        self.priceCalculate()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = CarDetailsViewM()
        }
    }
    
    private func getDatasourceForCarDetails() {
        
        if let dataSource = self.viewModel?.getCarDetailsDatasource(carDetails: self.carDetailModel) {
            self.carDetailsDatasource = dataSource
            
        }
        if let dataSource = self.viewModel?.getCarOptionsDatasource() {
            self.optionsDatasource = dataSource
        }
        if let dataSource = self.viewModel?.getExtraServicesDatasource(extras: self.carDetailModel.extras) {
            self.extraServicesDatasource = dataSource
            if self.extraServicesDatasource.count == 0{
                self.extraServicesCollectionView.translatesAutoresizingMaskIntoConstraints = true
                self.headerView.frame = CGRect(x: self.headerView.frame.origin.x, y: self.headerView.frame.origin.y, width: self.headerView.frame.size.width, height: 310)
                self.extraServicesCollectionView.isHidden = true
                self.extrasLabel.isHidden = true
                self.tapHereLabel.isHidden = true
                self.sideLabel.isHidden = true
                
            }else{
                self.headerView.frame = CGRect(x: self.headerView.frame.origin.x, y: self.headerView.frame.origin.y, width: self.headerView.frame.size.width, height: 442)
                self.extraServicesCollectionView.isHidden = false
                self.extrasLabel.isHidden = false
                self.tapHereLabel.isHidden = false
                self.sideLabel.isHidden = false
            }
        }
    }
    
    private func localizeTexts() {
        self.extrasLabel.text = "Extras".localizedString()
        self.tapHereLabel.text = "Tap here to add extras services".localizedString()
        self.bookNowLabel.text = "Book Now for Only ".localizedString() + "AED".localizedString() + " " + Helper.toString(object: carDetailModel.bookingPrice)
        self.bookNowButton.setTitle("Book Now".localizedString(), for: .normal)
        self.friendsThinkingLabel.text = "See What your Friends think about this car".localizedString()
        self.shareWithFriendButton.setTitle("SHARE WITH FRIEND".localizedString(), for: .normal)
        self.carImageView.setImage(urlStr:Helper.toString(object: carDetailModel.offerImage), placeHolderImage: nil)
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.carDetailsTableView.delegate = self
        self.carDetailsTableView.dataSource = self
        self.carDetailsTableView.separatorStyle = .none
        self.carDetailsTableView.tableHeaderView = self.headerView
    }
    
    private func setUpCollectionView() {
        self.extraServicesCollectionView.registerNib(nib: ExtraServicesCollectionCell.className)
        self.extraServicesCollectionView.delegate = self
        self.extraServicesCollectionView.dataSource = self
        
        self.vehicleImagessCollectionView.delegate = self
        self.vehicleImagessCollectionView.dataSource = self
        self.vehicleImagessCollectionView.register(CarImageCollectionCell.self)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal //depending upon direction of collection view
        self.vehicleImagessCollectionView?.setCollectionViewLayout(layout, animated: true)
        
        if let vehicleImagess = self.carDetailModel.vehicleImages {
            self.vehicleImagesDatasource = vehicleImagess
            self.pageControl.numberOfPages = self.vehicleImagesDatasource.count
            self.pageControl.currentPage = 0
            Threads.performTaskInMainQueue {
                self.vehicleImagessCollectionView.reloadData()
            }
        }
    }
    
    private func registerNibs() {
        self.carDetailsTableView.registerMultiple(nibs: [CarInfoTableCell.className, CarFinanceTableCell.className, CarDescriptionTableCell.className, DocumentsRequiredTableCell.className, CarContactUsTableCell.className, SpecificationOptionsTableCell.className])
    }
    
    func priceCalculate() {
        let totalPrice = Helper.getExtraPrice(extraServices: self.extraServicesDatasource) + Helper.toInt(carDetailModel.price)
        let price = "Price".localizedString() + " : " + "AED".localizedString() + " " + Helper.toString(object: totalPrice)
        let attributedString = NSMutableAttributedString(string: price, attributes: [
            .font: UIFont.font(name: .AirbnbCerealApp, weight: .Medium, size: .size_18),
            .foregroundColor: UIColor.redButtonColor])
        attributedString.apply(color: UIColor.black, subString: "Price :")
        self.carPriceLabel.attributedText = attributedString
    }
    
    func moveToDocumentDetailsVC() {
        let documentDetailsVC = DIConfigurator.sharedInstance.getCDocumentDetailsVC()
        self.navigationController?.pushViewController(documentDetailsVC, animated: true)
    }
    
    func moveToBookNowVC() {
        let bookNowVC = DIConfigurator.sharedInstance.getCBookNowVC()
        if let extraServices = self.viewModel?.getAddedExtrasData(extraService: self.extraServicesDatasource) {
            bookNowVC.extraServicesDatasource = extraServices
        }
        bookNowVC.vehicleId = self.vehicleId
        bookNowVC.carDetailModel = self.carDetailModel
        self.navigationController?.pushViewController(bookNowVC, animated: true)
    }
    
    func showDropDownAt(indexPath: IndexPath) {
        let data = self.extraServicesDatasource[indexPath.item]
        let title = Helper.toString(object: data.info?[Constants.UIKeys.placeholderText])
        let price = Helper.toString(object: data.info?[Constants.UIKeys.servicePrice])
        
        guard let addExtraPopup = t.inistancefromNib() else { return }
        addExtraPopup.initializeViewWith(title:title, price: price) { [weak self] (response) in
            
            guard let sSelf = self else { return }
            // sSelf.addCarDataSource[indexPath.row].value = conditionStatus
            sSelf.extraServicesDatasource[indexPath.item].info?[Constants.UIKeys.isExtrasAdded] = true as AnyObject
            sSelf.extraServicesCollectionView.reloadData()
            sSelf.priceCalculate()

        }
        addExtraPopup.showWithAnimated(animated: true)
        
    }
    
    //MARK: - APIMethods
    func requestVehicleDetailsAPI(vehicleId: Int,notificationType: [Int]) {
        self.viewModel?.requestNotifyAPI(vehicleId: vehicleId, notificationType: notificationType, completion: { [weak self] (status) in
            guard let sSelf = self else { return }
            sSelf.requestAddWatchListAPI(vehicleId: vehicleId, notificationType: notificationType)
        })
    }
    
    func requestAddWatchListAPI(vehicleId: Int,notificationType: [Int]) {
        self.viewModel?.requestaddWatchListAPI(vehicleId: vehicleId, notificationType: notificationType, completion: { [weak self] (status) in
            guard self != nil else { return }
            
        })
    }
    
    //MARK:- IBAction Methods
    
    @IBAction func bookNowButtonTapped(_ sender: UIButton) {
        if Helper.toBool(self.carDetailModel.isCarBooked)  {
            Alert.showOkAlert(title: StringConstants.Text.AppName.localizedString(), message: "This car is already booked.".localizedString())
            
        } else {
            self.moveToBookNowVC()
        }
        
    }
    
    @IBAction func notifyButtonTapped(_ sender: UIButton) {
        guard let notificationPopup = NotificationPopup.inistancefromNib() else { return }
        let notificationPopupType = NotificationPopupType.buyCarNotify
        notificationPopup.initializeViewWith(title: notificationPopupType.headerTitle, arrayList: notificationPopupType.dataList) { [weak self] (notificationType) in
            guard let sSelf = self else { return }
            //api for notify me
            sSelf.requestVehicleDetailsAPI(vehicleId: Helper.toInt(sSelf.carDetailModel.id), notificationType: notificationType)
        }
        notificationPopup.showWithAnimated(animated: true)
    }
    
    @IBAction func shareWithFriendButtonTapped(_ sender: UIButton) {
    
        let text = "https://www.albacars.ae/"
        let textShare = [text]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
}
