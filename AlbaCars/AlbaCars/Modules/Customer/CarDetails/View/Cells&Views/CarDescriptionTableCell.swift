//
//  CarDescriptionTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 13/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CarDescriptionTableCell: BaseTableViewCell, NibLoadableView, ReusableView {

    //MARK:- IBOutlets
    @IBOutlet weak var descriptionTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.descriptionTitleLabel.text = "Description".localizedString()
       // self.descriptionLabel.text = "Accountability: Misuse of the `website or Application will lead to termination of the account and legal action will be applicable.Any dispute or claim arising out of or in connection with this website shall be governed and construed in accordance with the laws of UAE.United Arab of Emirates is our country of domicile.Minors under the age of 18 shall are prohibited to register as a User of this website and are not allowed to transact or use the website."
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.descriptionLabel.text = cellInfo.value.html2String
    }
}
