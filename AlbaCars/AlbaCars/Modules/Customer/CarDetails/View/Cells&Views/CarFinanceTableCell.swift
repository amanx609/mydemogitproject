//
//  CarFinanceTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 13/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CarFinanceTableCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var yearDurationLabel: UILabel!
    @IBOutlet weak var downPaymentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.titleLabel.text = "Finance".localizedString()
        self.priceLabel.text = "AED :".localizedString() + " 1,939/ " + "Per Month".localizedString()
        self.yearDurationLabel.text = "5 " + "Years of bank finance".localizedString()
        self.downPaymentLabel.text = "0 %" + " Downpayment".localizedString()
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        //CarDetailModel
        
        if let info = cellInfo.info,let carDetail = info[Constants.UIKeys.carDetail] as? CarDetailModel {
            self.priceLabel.text = "AED :".localizedString() + " \(Helper.toString(object: carDetail.monthlyInstallment))/ " + "Per Month".localizedString()
            self.yearDurationLabel.text = "\(Helper.toString(object: carDetail.financingYear)) " + "Years of bank finance".localizedString()
            self.downPaymentLabel.text = "\(Helper.toString(object: carDetail.downpayment)) %" + " Downpayment".localizedString()
        }
    }
    
}
