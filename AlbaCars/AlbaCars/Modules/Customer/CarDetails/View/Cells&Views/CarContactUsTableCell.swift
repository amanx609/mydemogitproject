//
//  CarContactUsTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 13/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CarContactUsTableCell: BaseTableViewCell, NibLoadableView, ReusableView {

    //MARK:- IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contactUsTableView: UITableView!
    
    //MARK:- Variables
    var contactUsDatasource = [ContactUs]()

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.titleLabel.text = "Contact Us".localizedString()
        self.registerNibs()
        self.contactUsTableView.delegate = self
        self.contactUsTableView.dataSource = self
        self.contactUsTableView.separatorStyle = .none
    }
    
    private func registerNibs() {
        self.contactUsTableView.register(nib: ContactUsTableCell.className)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info,let contactUs = info[Constants.UIKeys.contactUs] as? [ContactUs] {
            self.contactUsDatasource = contactUs
            Threads.performTaskInMainQueue {
                self.contactUsTableView.reloadData()
            }
        }

    }
}

//MARK:- UITableView Delegates & Datasource Methods
extension CarContactUsTableCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contactUsDatasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let contactUs = self.contactUsDatasource[indexPath.row]
        return getCell(tableView: tableView, indexPath: indexPath,contactUs:contactUs)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.CellHeightConstants.height_53
    }
}

extension CarContactUsTableCell {
    func getCell(tableView: UITableView, indexPath: IndexPath,contactUs:ContactUs) -> UITableViewCell {
         let cell: ContactUsTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
         cell.delegate = self
         cell.configureView(contactUs: contactUs)
        return cell
    }
}
// ContactUsTableCellDelegate
extension CarContactUsTableCell:ContactUsTableCellDelegate {
    func didTapCall(cell: ContactUsTableCell) {
        guard let indexPath = self.contactUsTableView.indexPath(for: cell) else { return }
        let contactUs = self.contactUsDatasource[indexPath.row]
        if let url = URL(string: "tel://\(Helper.toString(object: contactUs.contactNumber()))"),
        UIApplication.shared.canOpenURL(url) {
           if #available(iOS 10, *) {
             UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
