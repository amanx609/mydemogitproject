//
//  CarImageCollectionCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class CarImageCollectionCell: BaseCollectionViewCell,ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var placeholderImageView: UIImageView!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: - Public Methods
    func configureView(vehicleImages: String) {
        //Image
        //if let profileImage = vehicleImages.image {
            self.placeholderImageView.setImage(urlStr: vehicleImages, placeHolderImage: nil)
       // }
    }
    
    func configureForUSDetailCell(cellData: [String: AnyObject]) {
        if let image = cellData[Constants.UIKeys.image] as? String {
            self.placeholderImageView.setImage(urlStr: image, placeHolderImage: nil)
        }
    }
}
