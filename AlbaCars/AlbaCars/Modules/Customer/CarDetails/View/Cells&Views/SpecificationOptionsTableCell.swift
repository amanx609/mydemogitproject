//
//  SpecificationOptionsTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 16/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class SpecificationOptionsTableCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var specificationsButton: UIButton!
    @IBOutlet weak var optionsButton: UIButton!
    @IBOutlet weak var separatorLabel: UILabel!
    @IBOutlet weak var odometerLabel: UILabel!
    @IBOutlet weak var cylinderLabel: UILabel!
    @IBOutlet weak var wheelSizeLabel: UILabel!
    @IBOutlet weak var optionLabel: UILabel!
    @IBOutlet weak var serviceHistoryLabel: UILabel!
    @IBOutlet weak var warrantyLabel: UILabel!
    @IBOutlet weak var specificationsView: UIView!
    @IBOutlet weak var optionsCollectionView: UICollectionView!
    @IBOutlet weak var leadingSeparatorLabelConstant: NSLayoutConstraint!
    
    //MARK:- Variables
    var data: [String]? {
        didSet {
            if let _ = self.data {
                configureCell()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.configureSpecificationsView()
    }
    
    private func configureCell() {
        self.registerNibs()
        self.optionsCollectionView.delegate = self
        self.optionsCollectionView.dataSource = self
    }
    
    private func registerNibs() {
        self.optionsCollectionView.registerNib(nib: LeftValueOptionsCell.className)
        self.optionsCollectionView.registerNib(nib: RightValueOptionsCell.className)
    }
    
    private func configureSpecificationsView() {
        self.specificationsButton.isSelected = true
        self.optionsButton.isSelected = false
        self.leadingSeparatorLabelConstant.constant = 0
        self.specificationsView.isHidden = false
        self.optionsCollectionView.isHidden = true
        self.specificationsButton.titleLabel?.font = UIFont.font(name: .AirbnbCerealApp, weight: .Medium, size: .size_18)
        self.optionsButton.titleLabel?.font = UIFont.font(name: .AirbnbCerealApp, weight: .Book, size: .size_18)
    }
    
    private func configureOptionsView() {
        self.specificationsButton.isSelected = false
        self.optionsButton.isSelected = true
        self.leadingSeparatorLabelConstant.constant = self.frame.size.width/2
        self.specificationsView.isHidden = true
        self.optionsCollectionView.isHidden = false
        self.specificationsButton.titleLabel?.font = UIFont.font(name: .AirbnbCerealApp, weight: .Book, size: .size_18)
        self.optionsButton.titleLabel?.font = UIFont.font(name: .AirbnbCerealApp, weight: .Medium, size: .size_18)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        //CarDetailModel
        
        if let info = cellInfo.info,let carDetail = info[Constants.UIKeys.carDetail] as? CarDetailModel {
            self.odometerLabel.text = "Odometer : ".localizedString() + Helper.toString(object: carDetail.mileage)
            self.wheelSizeLabel.text = "Wheel Size : ".localizedString() + Helper.toString(object: carDetail.wheelSize)
            self.serviceHistoryLabel.text = "Service History : ".localizedString() + Helper.toString(object: carDetail.serviceHistory)
            self.cylinderLabel.text = "Cyllinder : ".localizedString() + Helper.toString(object: carDetail.cylinder)
            self.optionLabel.text = "Option : ".localizedString() + Helper.toString(object: carDetail.vehicleOption)
            self.warrantyLabel.text = "Warranty : ".localizedString() + Helper.toString(object: carDetail.warranty)
            self.data = carDetail.options
            
            //            Threads.performTaskInMainQueue {
            //                self.contactUsTableView.reloadData()
            //            }
        }
    }
    
    //MARK:- IBAction Methods
    
    @IBAction func specificationsButtonTapped(_ sender: UIButton) {
        self.configureSpecificationsView()
    }
    
    @IBAction func optionsButtonTapped(_ sender: UIButton) {
        self.configureOptionsView()
    }
}

//MARK:- UICollectionView Delegates & Datasource Methods
extension SpecificationOptionsTableCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let arrOptions = data {
            return arrOptions.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item % 2 == 0 {
            return getLeftValueCell(collectionView, indexPath: indexPath)
        }
        return getRightValueCell(collectionView, indexPath: indexPath)
    }
}

extension SpecificationOptionsTableCell {
    func getLeftValueCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LeftValueOptionsCell.className, for: indexPath) as? LeftValueOptionsCell else {
            return UICollectionViewCell()
        }
        cell.optionsLabel.text = self.data?[indexPath.item]
        return cell
    }
    
    func getRightValueCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RightValueOptionsCell.className, for: indexPath) as? RightValueOptionsCell else {
            return UICollectionViewCell()
        }
        cell.optionsLabel.text = self.data?[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2, height: Constants.CellHeightConstants.height_40)
    }
}


