//
//  ExtraServicesCollectionCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 13/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class ExtraServicesCollectionCell: UICollectionViewCell {
    
    //MARK:- IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var extraServiceLabel: UILabel!
    @IBOutlet weak var freeImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var extraServiceImageView: UIImageView!
    
    //MARK:- Variables
    var data: CellInfo? {
        didSet {
            if let data = self.data {
                configureCell(data)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        
    }
    
    private func configureCell(_ data: CellInfo) {
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_16)
        self.bgView.clipsToBounds = true
        if let info = data.info {
            //Placeholder
            if let placeHolderText = info[Constants.UIKeys.placeholderText] as? String {
                self.extraServiceLabel.text = placeHolderText
            }
            
//            if let extras = info[Constants.UIKeys.extras] as? Extras {
//                self.freeImageView.isHidden = false
//                self.priceLabel.isHidden = true
//
//                if let selectedImage = info[Constants.UIKeys.selectedImage] as? UIImage {
//                    self.extraServiceImageView.image = selectedImage
//                }
//            } else {
//                self.freeImageView.isHidden = true
//                self.priceLabel.isHidden = false
//                if let unselectedImage = info[Constants.UIKeys.unselectedImage] as? UIImage {
//                    self.extraServiceImageView.image = unselectedImage
//                }
//                if let servicePrice = info[Constants.UIKeys.servicePrice] as? String {
//                    self.priceLabel.text = "AED".localizedString() + " " + servicePrice
//                }
//            }
            
            self.freeImageView.isHidden = true
            self.priceLabel.isHidden = false
            if let unselectedImage = info[Constants.UIKeys.unselectedImage] as? UIImage {
                self.extraServiceImageView.image = unselectedImage
            }
            if let servicePrice = info[Constants.UIKeys.servicePrice] as? String {
                self.priceLabel.text = "AED".localizedString() + " " + servicePrice
            }
            
            if let isExtrasAdded = info[Constants.UIKeys.isExtrasAdded] as? Bool, isExtrasAdded == true {
              
                if let selectedImage = info[Constants.UIKeys.selectedImage] as? UIImage {
                    self.extraServiceImageView.image = selectedImage
                }
            }
            
        }
    }
}
