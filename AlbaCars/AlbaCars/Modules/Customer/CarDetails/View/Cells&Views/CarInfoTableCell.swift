//
//  CarInfoTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 13/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CarInfoTableCell: BaseTableViewCell, NibLoadableView, ReusableView {

    //MARK:- IBOutlets
    
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var carDescriptionLabel: UILabel!
    @IBOutlet weak var brandTitleLabel: UILabel!
    @IBOutlet weak var brandValueLabel: UILabel!
    @IBOutlet weak var carTypeTitleLabel: UILabel!
    @IBOutlet weak var carTypeValueLabel: UILabel!
    @IBOutlet weak var modelTitleLabel: UILabel!
    @IBOutlet weak var modelValueLabel: UILabel!
    @IBOutlet weak var optionsTitleLabel: UILabel!
    @IBOutlet weak var optionsValueLabel: UILabel!
    @IBOutlet weak var yearTitleLabel: UILabel!
    @IBOutlet weak var yearValueLabel: UILabel!
    @IBOutlet weak var colorTitleLabel: UILabel!
    @IBOutlet weak var colorValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.brandTitleLabel.text = "Brand :".localizedString()
        self.carTypeTitleLabel.text = "Car Type :".localizedString()
        self.modelTitleLabel.text = "Model :".localizedString()
        self.optionsTitleLabel.text = "Options :".localizedString()
        self.yearTitleLabel.text = "Year :".localizedString()
        self.colorTitleLabel.text = "Color :".localizedString()
        self.colorValueLabel.roundCorners(self.colorValueLabel.frame.size.width/2)
        self.colorValueLabel.makeLayer(color: .black, boarderWidth: Constants.UIConstants.borderWidth_1, round: self.colorValueLabel.frame.size.width/2)
    }
    
    //MARK: - Public Methods
       func configureView(cellInfo: CellInfo) {
           //CarDetailModel
           if let info = cellInfo.info,let carDetail = info[Constants.UIKeys.carDetail] as? CarDetailModel {
            self.carNameLabel.text = Helper.toString(object: carDetail.brandName)
            self.carDescriptionLabel.text = Helper.toString(object: carDetail.carTitle)
            self.brandValueLabel.text = Helper.toString(object: carDetail.brandName)
            self.modelValueLabel.text = Helper.toString(object: carDetail.modelName)
            self.optionsValueLabel.text =  Helper.toString(object: carDetail.vehicleOption)
            self.yearValueLabel.text =  Helper.toString(object: carDetail.year)
            self.carTypeValueLabel.text =  Helper.toString(object: carDetail.typeName)
            self.colorValueLabel.backgroundColor = UIColor.colorWith(hexString: Helper.toString(object: carDetail.color))
           }
       }
}
