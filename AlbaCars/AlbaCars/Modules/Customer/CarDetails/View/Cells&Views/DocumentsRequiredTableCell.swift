//
//  DocumentsRequiredTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 13/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DocumentsRequiredTableCell: BaseTableViewCell, NibLoadableView, ReusableView {

    //MARK:- IBOutlets
    @IBOutlet weak var documentsRequiredButton: UIButton!
    @IBOutlet weak var shadowView: UIView!
    
    //MARK:- Variables
    var documentRequiredDidTapped:(()-> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.shadowView.addShadow(Constants.UIConstants.sizeRadius_7, shadowOpacity: Constants.UIConstants.shadowOpacity)
        self.documentsRequiredButton.roundCorners(Constants.UIConstants.sizeRadius_3half)
        if Constants.Devices.IsiPhone6 {
            self.documentsRequiredButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 301, bottom: 0, right: 0)
        } else if Constants.Devices.IsiPhone6P || Constants.Devices.IsiPhone11 {
            self.documentsRequiredButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 340, bottom: 0, right: 0)
        } else if Constants.Devices.IsiPhone11 {
            self.documentsRequiredButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 340, bottom: 0, right: 0)
        }
        
        self.layoutIfNeeded()
    }
    
    //MARK:- IBAction Methods
    
    @IBAction func documentRequiredButtonTapped(_ sender: UIButton) {
        documentRequiredDidTapped?()
    }
}
