//
//  ContactUsTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 13/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol ContactUsTableCellDelegate: class, BaseProtocol {
    func didTapCall(cell: ContactUsTableCell)
}

class ContactUsTableCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK:- IBOutlets
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var callNowLabel: UILabel!
    @IBOutlet weak var callButton: UIButton!
    
    //MARK: - Variables
    weak var delegate: ContactUsTableCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.profileImageView.roundCorners(self.profileImageView.layer.frame.width/2)
    }
    
    //MARK: - Public Methods
    func configureView(contactUs: ContactUs) {
        self.nameLabel.text = Helper.toString(object: contactUs.name)
        self.contactNumberLabel.text = contactUs.contactNumber()
        //Image
        if let profileImage = contactUs.image {
            self.profileImageView.setImage(urlStr: profileImage, placeHolderImage: nil)
        }
    }
    
    @IBAction func tapCallButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapCall(cell: self)
        }
    }
    
}
