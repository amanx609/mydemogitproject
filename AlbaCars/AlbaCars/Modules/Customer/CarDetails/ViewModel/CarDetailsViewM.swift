//
//  CarDetailsViewM.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 13/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol CarDetailsVModeling: BaseVModeling {
    func getCarDetailsDatasource(carDetails:CarDetailModel) -> [CellInfo]
    func getCarOptionsDatasource() -> [String]
    func getExtraServicesDatasource(extras: [Extras]?) -> [CellInfo]
    func requestNotifyAPI(vehicleId: Int, notificationType: [Int], completion: @escaping (Bool) -> Void)
    func getAddedExtrasData(extraService:[CellInfo]) -> [CellInfo]
    func requestaddWatchListAPI(vehicleId: Int, notificationType: [Int], completion: @escaping (Bool) -> Void)
}

class CarDetailsViewM: BaseViewM, CarDetailsVModeling {
    func getExtraServicesDatasource(extras: [Extras]?) -> [CellInfo] {
        var dataSource: [CellInfo] = []
        
        if let extraItem = extras {
            
            for extra in extraItem {
                
                switch extra.title?.lowercased() {
                case Extra.warranty.title.lowercased():
                    //Warranty Cell
                    var warrantyInfo = [String: AnyObject]()
                    warrantyInfo[Constants.UIKeys.selectedImage] = #imageLiteral(resourceName: "ex_selected_warranty")
                    warrantyInfo[Constants.UIKeys.unselectedImage] = #imageLiteral(resourceName: "ex_warranty.png")
                    warrantyInfo[Constants.UIKeys.isExtrasAdded] = false as AnyObject
                   // warrantyInfo[Constants.UIKeys.placeholderText] = extra.freeValue as AnyObject
                    warrantyInfo[Constants.UIKeys.servicePrice] = Helper.toString(object: extra.price) as AnyObject
                    warrantyInfo[Constants.UIKeys.extras] = extra as AnyObject
                    let warrantyCell = CellInfo(cellType: .ExtraServicesCollectionCell, placeHolder: "", value: "", info: warrantyInfo, height: Constants.CellHeightConstants.height_106)
                    dataSource.append(warrantyCell)
                    
                case Extra.registration.title.lowercased():
                    
                    //Registration Cell
                    var registrationInfo = [String: AnyObject]()
                    registrationInfo[Constants.UIKeys.selectedImage] = #imageLiteral(resourceName: "ex_selected_registration.png")
                    registrationInfo[Constants.UIKeys.unselectedImage] = #imageLiteral(resourceName: "ex_registration.png")
                    registrationInfo[Constants.UIKeys.isExtrasAdded] = false as AnyObject
                    registrationInfo[Constants.UIKeys.placeholderText] = "Registration".localizedString() as AnyObject
                    registrationInfo[Constants.UIKeys.servicePrice] = Helper.toString(object: extra.price) as AnyObject
                    registrationInfo[Constants.UIKeys.extras] = extra as AnyObject
                    let registrationCell = CellInfo(cellType: .ExtraServicesCollectionCell, placeHolder: "", value: "", info: registrationInfo, height: Constants.CellHeightConstants.height_106)
                    dataSource.append(registrationCell)
                    
                case Extra.salik.title.lowercased():
                    
                    //Salik Cell
                    var salikInfo = [String: AnyObject]()
                    salikInfo[Constants.UIKeys.unselectedImage] = #imageLiteral(resourceName: "ex_salik.png")
                    salikInfo[Constants.UIKeys.selectedImage] = #imageLiteral(resourceName: "ex_salik_selected.png")
                    salikInfo[Constants.UIKeys.isExtrasAdded] = false as AnyObject
                    salikInfo[Constants.UIKeys.placeholderText] = "Salik".localizedString() as AnyObject
                    salikInfo[Constants.UIKeys.servicePrice] = Helper.toString(object: extra.price) as AnyObject
                    salikInfo[Constants.UIKeys.extras] = extra as AnyObject
                    let salikCell = CellInfo(cellType: .ExtraServicesCollectionCell, placeHolder: "", value: "", info: salikInfo, height: Constants.CellHeightConstants.height_106)
                    dataSource.append(salikCell)
                    
                case Extra.windowTinting.title.lowercased():
                    
                    //Window Tinting Cell
                    var windowTintingInfo = [String: AnyObject]()
                    windowTintingInfo[Constants.UIKeys.unselectedImage] = #imageLiteral(resourceName: "ex_window_tinting.png")
                    windowTintingInfo[Constants.UIKeys.selectedImage] = #imageLiteral(resourceName: "ex_selected_window_tinting.png")
                    windowTintingInfo[Constants.UIKeys.isExtrasAdded] = false as AnyObject
                    windowTintingInfo[Constants.UIKeys.placeholderText] = "Window Tinting".localizedString() as AnyObject
                    windowTintingInfo[Constants.UIKeys.servicePrice] = Helper.toString(object: extra.price) as AnyObject
                    windowTintingInfo[Constants.UIKeys.extras] = extra as AnyObject
                    let windowTintingCell = CellInfo(cellType: .ExtraServicesCollectionCell, placeHolder: "", value: "", info: windowTintingInfo, height: Constants.CellHeightConstants.height_106)
                    dataSource.append(windowTintingCell)
                    
                case Extra.service.title.lowercased():
                    
                    //Service Cell
                    var serviceInfo = [String: AnyObject]()
                    serviceInfo[Constants.UIKeys.unselectedImage] = #imageLiteral(resourceName: "ex_service.png")
                    serviceInfo[Constants.UIKeys.selectedImage] = #imageLiteral(resourceName: "ex_service_selected.png")
                    serviceInfo[Constants.UIKeys.isExtrasAdded] = false as AnyObject
         //           serviceInfo[Constants.UIKeys.placeholderText] = Helper.toString(object: extra.freeValue) as AnyObject
                    serviceInfo[Constants.UIKeys.servicePrice] = Helper.toString(object: extra.price) as AnyObject
                    serviceInfo[Constants.UIKeys.extras] = extra as AnyObject
                    let serviceInfoCell = CellInfo(cellType: .ExtraServicesCollectionCell, placeHolder: "", value: "", info: serviceInfo, height: Constants.CellHeightConstants.height_106)
                    dataSource.append(serviceInfoCell)
                    
                default:
                    break
                }
            }
        }
        
        return dataSource
    }
    
    func getCarOptionsDatasource() -> [String] {
        var dataSource: [String] = []
        dataSource.append(contentsOf: ["Premium Sound System", "Alloy Wheel", "Cruise Control", "Bluetooth", "USB", "AUX", "Multifunctional Steering Wheel"])
        return dataSource
    }
    
    func getCarDetailsDatasource(carDetails:CarDetailModel) -> [CellInfo] {
        
        var dataSource: [CellInfo] = []
        var carInfo = [String: AnyObject]()
        carInfo[Constants.UIKeys.carDetail] = carDetails as AnyObject
        let carInfoCell = CellInfo(cellType: .CarInfoTableCell, placeHolder: "", value: "", info: carInfo, height: Constants.CellHeightConstants.height_168)
        dataSource.append(carInfoCell)
        
        var carFinanceInfo = [String: AnyObject]()
        carFinanceInfo[Constants.UIKeys.carDetail] = carDetails as AnyObject
        let carFinanceCell = CellInfo(cellType: .CarFinanceTableCell, placeHolder: "", value: "", info: carFinanceInfo, height: Constants.CellHeightConstants.height_97)
        dataSource.append(carFinanceCell)
        
        var carSpecificationInfo = [String: AnyObject]()
        carSpecificationInfo[Constants.UIKeys.carDetail] = carDetails as AnyObject
        let carSpecificationOptionCell = CellInfo(cellType: .SpecificationOptionsTableCell, placeHolder: "", value: "", info: carSpecificationInfo, height: Constants.CellHeightConstants.height_220)
        dataSource.append(carSpecificationOptionCell)
        
        let carDescriptionCell = CellInfo(cellType: .CarDescriptionTableCell, placeHolder: "", value: Helper.toString(object: carDetails.vehicleDescription), info: nil, height: Constants.CellHeightConstants.height_100)
        dataSource.append(carDescriptionCell)
        
        let documentsRequiredCell = CellInfo(cellType: .DocumentsRequiredTableCell, placeHolder: "", value: "", info: nil, height: Constants.CellHeightConstants.height_65)
        dataSource.append(documentsRequiredCell)
        var carContactUsInfo = [String: AnyObject]()
        carContactUsInfo[Constants.UIKeys.contactUs] = carDetails.contactUs as AnyObject
        let carContactUsTableCell = CellInfo(cellType: .CarContactUsTableCell, placeHolder: "", value: "", info: carContactUsInfo, height: Constants.CellHeightConstants.height_65 + Constants.CellHeightConstants.height_53 * CGFloat(Helper.toInt(carDetails.contactUs?.count)))
        dataSource.append(carContactUsTableCell)
        
        return dataSource
    }
    
    func getAddedExtrasData(extraService:[CellInfo]) -> [CellInfo] {
        var dataSource = [CellInfo]()
        for extras in extraService {
            if let info = extras.info, let isExtrasAdded = info[Constants.UIKeys.isExtrasAdded] as? Bool, isExtrasAdded == true {
                dataSource.append(extras)
            }
        }
        return dataSource
    }
    
    func requestNotifyAPI(vehicleId: Int, notificationType: [Int], completion: @escaping (Bool) -> Void) {
        let param = getNotifyAPIParams(vehicleId: vehicleId, notificationType: notificationType)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .notifyMe(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject]
                   // let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                {
                    Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Notification added Successfully".localizedString())
                    completion(success)
                }
            }
        }
    }
    
    func requestaddWatchListAPI(vehicleId: Int, notificationType: [Int], completion: @escaping (Bool) -> Void) {
         let param = getWatchListAPIParams(vehicleId: vehicleId, notificationType: notificationType)
         APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .addWatchList(param: param))) { (response, success) in
             if success {
                 if let safeResponse =  response as? [String: AnyObject]
                    // let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                 {
                   
                 }
             }
         }
     }
    
    private func getNotifyAPIParams(vehicleId: Int, notificationType: [Int]) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        var notificationTypeStr = ""
        for type in notificationType {
            if notificationTypeStr == "" {
                notificationTypeStr.append("\(type)")
            } else {
                notificationTypeStr.append(",\(type)")
            }
        }
        param[ConstantAPIKeys.notificationType] = notificationTypeStr as AnyObject
        return param
    }
    
    private func getWatchListAPIParams(vehicleId: Int, notificationType: [Int]) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        var notificationTypeStr = ""
        for type in notificationType {
            if notificationTypeStr == "" {
                notificationTypeStr.append("\(type)")
            } else {
                notificationTypeStr.append(",\(type)")
            }
        }
        param[ConstantAPIKeys.alert] = notificationTypeStr as AnyObject
        return param
    }
}
