//
//  RecoveryDetailContactInfoCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 13/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class RDContactInfoCell: BaseTableViewCell, NibLoadableView, ReusableView {

    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bgViewLeading: NSLayoutConstraint!
    @IBOutlet weak var bgViewTralling: NSLayoutConstraint!
    @IBOutlet weak var contactNameLabel: UILabel!
    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
           profileImageView.layer.borderColor = UIColor.black.cgColor
           profileImageView.layer.cornerRadius = profileImageView.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.contactNameLabel.text = cellInfo.placeHolder
        self.contactNumberLabel.text = "+971" + cellInfo.value
        
        if let info = cellInfo.info, let isHide = info[Constants.UIKeys.isHide] as? Bool {
            self.sepratorView.isHidden = isHide
        } else {
            self.sepratorView.isHidden = true
        }
    }
    
    func configureTenderDetailsView(cellInfo: CellInfo) {
        self.bgViewLeading.constant = 20
        self.bgViewTralling.constant = 20
        self.titleLabel.isHidden = true
        self.contactNameLabel.text = cellInfo.placeHolder
        self.contactNumberLabel.text = cellInfo.value
        
        if let info = cellInfo.info {
           //Image
            if let image = info[Constants.UIKeys.image] as? String, !image.isEmpty {
                   self.profileImageView.setImage(urlStr: Helper.toString(object: image), placeHolderImage: #imageLiteral(resourceName: "user_placeholder"))
               }
           }
        }
    
}
