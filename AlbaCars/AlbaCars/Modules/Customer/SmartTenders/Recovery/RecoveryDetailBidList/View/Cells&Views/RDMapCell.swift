//
//  RecoveryDetailMapCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 13/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

class RDMapCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBActions
    // @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var gMapView: GMSMapView!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.mapView.delegate = self
        self.gMapView.delegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        
        if let tenderDetails = cellInfo.info?[Constants.UIKeys.cellInfo] as? RDBidListModel {
            
            self.gMapView.clear()
            
            
            //            let allAnnotations = self.mapView.annotations
            //            self.mapView.removeAnnotations(allAnnotations)
            var bounds = GMSCoordinateBounds()
            
            if let pickupLocationLatitude = tenderDetails.pickupLocationLatitude, let pickupLocationLongitude = tenderDetails.pickupLocationLongitude {
                
                let locationCoordinate = CLLocationCoordinate2DMake(pickupLocationLatitude, pickupLocationLongitude)
                
                let markerView = UIImageView(image: UIImage(named: "greenMarker"))
                let marker = GMSMarker()
                marker.position = locationCoordinate
                marker.iconView = markerView
                marker.map = self.gMapView
                bounds = bounds.includingCoordinate(marker.position)
                
                //                let annotation = Annotation()
                //                annotation.identifier = 0
                //                annotation.coordinate = locationCoordinate
                //                self.mapView.addAnnotation(annotation)
                //
                //                let region = MKCoordinateRegion(center: locationCoordinate, span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
                //                self.mapView.setRegion(region, animated: true)
                
            }
            
            if let dropoffLocationLatitude = tenderDetails.dropoffLocationLatitude, let dropLocationLongitude = tenderDetails.dropoffLocationLongitude {
                
                let locationCoordinate = CLLocationCoordinate2DMake(dropoffLocationLatitude, dropLocationLongitude)
                
                //creating a marker view
                let markerView = UIImageView(image: UIImage(named: "redMarker"))
                let marker = GMSMarker()
                marker.position = locationCoordinate
                marker.iconView = markerView
                marker.map = self.gMapView
                bounds = bounds.includingCoordinate(marker.position)
                
                //                let locationCoordinate = CLLocationCoordinate2DMake(dropoffLocationLatitude, dropLocationLongitude)
                //                let annotation = Annotation()
                //                annotation.identifier = 1
                //                annotation.coordinate = locationCoordinate
                //                self.mapView.addAnnotation(annotation)
                //
                //                let region = MKCoordinateRegion(center: locationCoordinate, span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
                //                self.mapView.setRegion(region, animated: true)
                
            }
            
            if let pickupLocationLatitude = tenderDetails.pickupLocationLatitude, let pickupLocationLongitude = tenderDetails.pickupLocationLongitude,let dropoffLocationLatitude = tenderDetails.dropoffLocationLatitude, let dropLocationLongitude = tenderDetails.dropoffLocationLongitude  {
                let sourceLocation = CLLocationCoordinate2DMake(pickupLocationLatitude, pickupLocationLongitude)
                let destinationLocation = CLLocationCoordinate2DMake(dropoffLocationLatitude, dropLocationLongitude)
                
                
                let path = GMSMutablePath()
                path.add(sourceLocation)
                path.add(destinationLocation)
                let polyline = GMSPolyline(path: path)
                //            polyline.strokeColor = .black
                //            polyline.strokeWidth = 3.0
                //            polyline.map = self.gMapView
                
                let strokeStyles = [GMSStrokeStyle.solidColor(.black), GMSStrokeStyle.solidColor(.clear)]
                let strokeLengths = [NSNumber(value: 10), NSNumber(value: 10)]
                if let path = polyline.path {
                    polyline.spans = GMSStyleSpans(path, strokeStyles, strokeLengths, .rhumb)
                }
                polyline.map = self.gMapView
                
                //                for poll in self.mapView.overlays {
                //                    self.mapView.removeOverlay(poll)
                //                }
                //
                //                let sourceLocation = CLLocationCoordinate2DMake(pickupLocationLatitude, pickupLocationLongitude)
                //                let destinationLocation = CLLocationCoordinate2DMake(dropoffLocationLatitude, dropLocationLongitude)
                //
                //                // Store the coordinates in an array.
                //                var coordinates_1 = [sourceLocation,destinationLocation]
                //
                //                // Create polyline.
                //                let myPolyLine_1: MKPolyline = MKPolyline(coordinates: &coordinates_1, count: coordinates_1.count)
                //
                //                // Add circle to mapView.
                //                self.mapView.addOverlay(myPolyLine_1)
            }
            
            // self.mapView.showAnnotations(self.mapView.annotations, animated: true)
            
            self.gMapView.setMinZoom(1, maxZoom: 10)
            let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
            self.gMapView.animate(with: update)
            
        }
        
    }
    
}
//
//extension RDMapCell: MKMapViewDelegate {
//    
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        
//        if annotation is MKUserLocation {
//            return nil
//        }
//        
//        var imageName =  ""
//        
//        if annotation is Annotation {
//            let annot = annotation as! Annotation
//            
//            imageName = annot.identifier == 0 ? "greenMarker" : "redMarker"
//        }
//        
//        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "Customannotation")
//        annotationView.image = UIImage(named: imageName)
//        
//        annotationView.canShowCallout = false
//        return annotationView
//    }
//    
//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//        guard let polyline = overlay as? MKPolyline else {
//            fatalError("Not a MKPolyline")
//        }
//        
//        let renderer = MKPolylineRenderer(polyline: polyline)
//        renderer.strokeColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
//        renderer.lineWidth = 2
//        renderer.lineDashPattern = [4, 4]
//        return renderer
//    }
//}

extension RDMapCell: GMSMapViewDelegate {
    
    
}
