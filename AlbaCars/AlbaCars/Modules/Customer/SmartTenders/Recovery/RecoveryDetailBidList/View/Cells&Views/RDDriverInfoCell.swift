//
//  RDDriverInfoCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 14/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class RDDriverInfoCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var driverContactLabel: UILabel!
    @IBOutlet weak var driverImageView: UIImageView!
    @IBOutlet weak var truckNumberLabel: UILabel!

    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setup() {
        Threads.performTaskInMainQueue {
            self.driverImageView.roundCorners(Constants.UIConstants.sizeRadius_7)
            self.driverImageView.makeLayer(color: .white, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        }
    }
    

    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            self.driverNameLabel.text = Helper.toString(object: info[Constants.UIKeys.driverName])
            self.driverContactLabel.text = "+971" + Helper.toString(object: info[Constants.UIKeys.driverContact])
            self.driverImageView.setImage(urlStr: Helper.toString(object: info[Constants.UIKeys.driverImage]), placeHolderImage: UIImage(named: "profileImgPlaceholder"))
            
            if let truckNumber = info[Constants.UIKeys.truckNumber] as? String, !truckNumber.isEmpty {
                self.truckNumberLabel.isHidden = false
                self.truckNumberLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.truckNumber, secondText: Helper.toString(object:truckNumber), firstTextColor: .white, secondTextColor: .white, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
            } else {
                self.truckNumberLabel.isHidden = true
            }
            
        }
        
    }
    
}
