//
//  RecoveryDetailBidListViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 13/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class RDBidListViewC: BaseViewC {

    //MARK: - IBOutlets
    @IBOutlet weak var recoveryDetailTableView: UITableView!
    @IBOutlet weak var dashedLineFirstImageView: UIImageView!
    @IBOutlet weak var dashedLineSecondImageView: UIImageView!
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var thirdButton: UIButton!
    @IBOutlet weak var timeFirstLabel: UILabel!
    @IBOutlet weak var timeSecondLabel: UILabel!
    @IBOutlet weak var timeThirdLabel: UILabel!
    @IBOutlet var footerView: UIView!

    //MARK: - Variables
    var viewModel: RDBidListVModeling?
    var recoveryDetailDataSource: [CellInfo] = []
    var tenderId: Int?
    var nextPageNumber = Int(1)
    var tenderDetails: RDBidListModel?
    var tenderStatus: TenderStatus = .all
    var acceptBtnCounter = 0
    var durationTimer : Timer?
    var allBidList: [BidListModel] = []
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    deinit {
        print("deinit RDBidListViewC")
        self.invalidateTimer()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Recovery Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.recheckVM()
        self.setupTableView()
        self.requestTenderDetailsAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = RDBidListViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.recoveryDetailTableView.delegate = self
        self.recoveryDetailTableView.dataSource = self
        self.recoveryDetailTableView.separatorStyle = .none
    }
    
    private func registerNibs() {
        self.recoveryDetailTableView.register(STCarDetailCell.self)
        self.recoveryDetailTableView.register(RDMapCell.self)
        self.recoveryDetailTableView.register(RDContactInfoCell.self)
        self.recoveryDetailTableView.register(STBidsCountCell.self)
        self.recoveryDetailTableView.register(RDBidListCell.self)
        self.recoveryDetailTableView.register(BottomLabelCell.self)
        self.recoveryDetailTableView.register(DashedTextFieldCell.self)
    }
    
    private func loadDataSource(tenderDetails: RDBidListModel, bidList: [BidListModel]) {
        if let dataSource = self.viewModel?.getRecoveryDetailDataSource(tenderDetails: tenderDetails, bidList: bidList, tenderStatus: self.tenderStatus) {
            self.recoveryDetailDataSource = dataSource
            self.recoveryDetailTableView.reloadData()
            
            if self.tenderStatus == .completedTenders {
                self.recoveryDetailTableView.tableFooterView = self.footerView
                self.setRecoveryStatus()
            }
        }
    }
    
    private func setupTimer() {
        self.invalidateTimer()
        self.durationTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimeValueInDataSource), userInfo: nil, repeats: true)
    }
    
    private func invalidateTimer() {
        self.durationTimer?.invalidate()
        self.durationTimer = nil
    }
    
     private func setRecoveryStatus() {
    
         if let tenderRecoveryStatuses = self.tenderDetails?.tenderRecoveryStatuses, tenderRecoveryStatuses.count > 0 {
             
             for i in 0..<tenderRecoveryStatuses.count {
                 switch i {
                 case 0:
                     self.timeFirstLabel.text = tenderRecoveryStatuses[i].startDate?.getDateStringFrom(inputFormat: Constants.Format.apiSpaceDateTimeFormat, outputFormat: Constants.Format.displayDateTimeFormat)
                     self.firstButton.isSelected = true
                 case 1:
                     self.timeSecondLabel.text = tenderRecoveryStatuses[i].startDate?.getDateStringFrom(inputFormat: Constants.Format.apiSpaceDateTimeFormat, outputFormat: Constants.Format.displayDateTimeFormat)
                     self.secondButton.isSelected = true
                     self.thirdButton.isSelected = true
                     self.dashedLineFirstImageView.image = #imageLiteral(resourceName: "dashedGreen")
                 case 2:
                     self.timeThirdLabel.text = tenderRecoveryStatuses[i].startDate?.getDateStringFrom(inputFormat: Constants.Format.apiSpaceDateTimeFormat, outputFormat: Constants.Format.displayDateTimeFormat)
                     self.thirdButton.isSelected = true
                     self.dashedLineSecondImageView.image = #imageLiteral(resourceName: "dashedGreen")
                 default:
                     break
                 }
             }
         }
         
     }
    
    //MARK: - Selector Methods
    @objc func updateTimeValueInDataSource() {
        if var stCarDetailCellInfo = self.recoveryDetailDataSource.first?.info,
            let tenderDetail = stCarDetailCellInfo[Constants.UIKeys.cellInfo] as? RDBidListModel {
            switch self.tenderStatus {
            case .upcomingTenders:
                if let timerDuration = tenderDetail.startTimerDuration {
                    tenderDetail.startTimerDuration = timerDuration - 1
                    
                    //RemoveDataIfDurationIsFinished
                    if timerDuration == 0 {
                        //ActionWhenTimerIsZero
                    }
                }
            case .inProgress, .noBidAccepted:
                if let timerDuration = tenderDetail.endTimerDuration {
                    tenderDetail.endTimerDuration = timerDuration - 1
                    
                    //RemoveDataIfDurationIsFinished
                    if timerDuration == 0 {
                        //ActionWhenTimerIsZero
                    }
                }
            default:
                break
            }
            stCarDetailCellInfo[Constants.UIKeys.cellInfo] = tenderDetail
            self.recoveryDetailDataSource[0].info = stCarDetailCellInfo
            let stCarDetailCellIndexPath = IndexPath(row: 0, section: 0)
            self.recoveryDetailTableView.reloadRows(at: [stCarDetailCellIndexPath], with: .none)
        }
    }
    
    //MARK: - APIMethods
    private func requestTenderDetailsAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
        //params[ConstantAPIKeys.type] = 1 as AnyObject
        self.viewModel?.requestTenderDetailsAPI(params: params, completion: {[weak self] (tenderDetails) in
            guard let sSelf = self else { return }
            sSelf.tenderDetails = tenderDetails
            sSelf.loadDataSource(tenderDetails: tenderDetails, bidList: [])
            sSelf.setupTimer()
            sSelf.requestBidListAPI(tenderDetails: tenderDetails)
        })
    }
    
     func requestBidListAPI(tenderDetails: RDBidListModel) {
        var params = APIParams()
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
        params[ConstantAPIKeys.orderBy] = 1 as AnyObject
        self.viewModel?.requestBidListAPI(params: params, completion: { [weak self] (nextPageNumber, perPage, bidList) in
            guard let sSelf = self else { return }
             sSelf.nextPageNumber = nextPageNumber
            sSelf.allBidList = sSelf.allBidList + bidList
            sSelf.loadDataSource(tenderDetails: tenderDetails, bidList: sSelf.allBidList)
        })
    }
        
    //MARK: - Public Methods
    func requestAcceptBid(bidId: Int, winAmount: Double) {
        
//        let vatAmmount = Helper.calculateVat(vatPercentage: self.tenderDetails?.vatPercentage, bidAmount: winAmount)
//        let totalAmmount = winAmount + vatAmmount

        let paymentDetails = PaymentDetailsData(viewController: self, totalAmmount: winAmount, referenceId: Helper.toInt(self.tenderId), paymentServiceCategoryId: PaymentServiceCategoryId.smartTenders,vatAmmount: 0)
        
        PaymentManager.sharedInstance.initiatePayment(paymentDetailsData: paymentDetails) { (success, paymentID) in
            
            if success {
                
                var params = APIParams()
                params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
                params[ConstantAPIKeys.bidId] = bidId as AnyObject
                params[ConstantAPIKeys.paymentId] = paymentID as AnyObject
                params[ConstantAPIKeys.winAmount] = winAmount as AnyObject

                self.viewModel?.requestAcceptBidAPI(params: params, completion: { [weak self] (success)  in
                    guard let sSelf = self else { return }
                    
                    let secondDetailVC = DIConfigurator.sharedInstance.getSecondRecoveryDetailVC()
                    secondDetailVC.tenderId = sSelf.tenderId
                    secondDetailVC.acceptedBidId = bidId
                    secondDetailVC.tenderStatus = sSelf.tenderStatus
                    sSelf.navigationController?.pushViewController(secondDetailVC, animated: true)
                })
                
            }
        }
    }
    
    func requestRejectBid(bidId: Int) {
        var params = APIParams()
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        params[ConstantAPIKeys.bidId] = bidId as AnyObject
        
        self.viewModel?.requestRejectBidAPI(params: params, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            sSelf.requestTenderDetailsAPI()
            
        })
    }

    func requestCancelTender() {
        var params = APIParams()
        params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
        self.viewModel?.requestCancelTenderAPI(params: params, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            if let viewControllers = sSelf.navigationController?.viewControllers {
                //CreateAccountViewCIsPresentInNavigationStack
                for viewController in viewControllers {
                    if viewController is RecoveryHomeViewC {
                        sSelf.navigationController?.popToViewController(viewController, animated: true)
                        return
                    }
                }
            }
            
        })
    }
    
    func requestBidTenderDetails(bidId: Int) {
        
        let recoveryTenderViewModel:SRecoveryTenderViewM = SRecoveryTenderViewM()
        var params = APIParams()
        params[ConstantAPIKeys.tenderId] = bidId as AnyObject
        recoveryTenderViewModel.requestMyTenderDetailsAPI(params: params, completion: { [weak self] (tender) in
            guard let sSelf = self else { return }
            Threads.performTaskInMainQueue {
                let submittedVC = DIConfigurator.sharedInstance.getSBidSubmittedVC()
                submittedVC.tenderDetails = tender
                sSelf.navigationController?.pushViewController(submittedVC, animated: true)
            }
        })
    }
}
