//
//  RDBidListCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 14/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
protocol RDBidListCellDelegate: class {
    func didTapCheck(cell: RDBidListCell)
    func didTapCross(cell: RDBidListCell)
}

class RDBidListCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var agencyImageView: UIImageView!
    @IBOutlet weak var agencyNameLabel: UILabel!
    @IBOutlet weak var agencyRatingView: ShowRating!
    @IBOutlet weak var recoveryTypeLabel: UILabel!
    @IBOutlet weak var deliveryTimeLabel: UILabel!
    @IBOutlet weak var pickupServiceLabel: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var vatLabel: UILabel!

    //MARK: - Variables
    weak var delegate: RDBidListCellDelegate?
    var bidId: Int?
    var bidAmount: Double?
    
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.shadowView.addShadow(ofColor:  UIColor.shadowColor, radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        self.containerView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.priceView.roundCorners([.bottomLeft, .bottomRight], radius: Constants.UIConstants.sizeRadius_16)
        agencyImageView.layer.cornerRadius = agencyImageView.frame.height/2

    }
    
    private func setupRecoveryData(info: [String: AnyObject]) {
        self.pickupServiceLabel.isHidden = true
        self.recoveryTypeLabel.attributedText = String.getAttributedText(firstText: "Types of Recovery : ".localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.recoveryType]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        let days = Helper.toInt(info[Constants.UIKeys.time])
        var daysStr = ""
        if days == 0 || days == 1 {
            daysStr = "\(days) day"
            
        } else {
            daysStr = "\(days) days"
        }
        self.deliveryTimeLabel.attributedText = String.getAttributedText(firstText: "Time of Delivery : ".localizedString(), secondText: daysStr, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.priceLabel.text = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.bidAmount]), text: StringConstants.Text.Currency.localizedString())
    }
    
    private func setupCarDetailingData(info: [String: AnyObject]) {
        self.pickupServiceLabel.isHidden = true
        let days = Helper.toInt(info[Constants.UIKeys.time])
        var daysStr = ""
        if days == 0 || days == 1 {
            daysStr = "\(days) day"
            
        } else {
            daysStr = "\(days) days"
        }
        self.recoveryTypeLabel.attributedText = String.getAttributedText(firstText: "Time Needed For Job: ".localizedString(), secondText: daysStr, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.priceLabel.text = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.bidAmount]), text: StringConstants.Text.Currency.localizedString())
        
        if let type = info[Constants.UIKeys.recoveryType] as? Int, type == 2, let serviceType = info[Constants.UIKeys.serviceType] as? Int{
            
            self.deliveryTimeLabel.attributedText = String.getAttributedText(firstText: "Service Type : ".localizedString(), secondText: Helper.toString(object: Helper.getDetailingService(statusCode: serviceType)), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
        } else {
            self.deliveryTimeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.pickUpService.localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.status]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        }

    }
    
    private func setupServicingData(info: [String: AnyObject]) {
        self.pickupServiceLabel.isHidden = true
        self.recoveryTypeLabel.attributedText = String.getAttributedText(firstText:"Types of Service : ".localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.serviceType]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        let days = Helper.toInt(info[Constants.UIKeys.time])
        var daysStr = ""
        if days == 0 || days == 1 {
            daysStr = "\(days) day"
            
        } else {
            daysStr = "\(days) days"
        }
        self.deliveryTimeLabel.attributedText = String.getAttributedText(firstText: "Time of Delivery : ".localizedString(), secondText: daysStr, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.priceLabel.text = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.bidAmount]), text: StringConstants.Text.Currency.localizedString())
    }
    
    private func setupInspectionData(info: [String: AnyObject]) {
        self.pickupServiceLabel.isHidden = false
        self.recoveryTypeLabel.attributedText = String.getAttributedText(firstText: "Inspection Date  : ".localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.date]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.deliveryTimeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.bidInspectionTime.localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.time]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.pickupServiceLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.pickUpService.localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.status]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.priceLabel.text = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.bidAmount]), text: StringConstants.Text.Currency.localizedString())
    }
    
    private func setupForUpholstery(info: [String: AnyObject]) {
        
        let days = Helper.toInt(info[Constants.UIKeys.time])
        var daysStr = ""
        if days == 0 || days == 1 {
            daysStr = "\(days) day"
            
        } else {
            daysStr = "\(days) days"
        }
        
        self.recoveryTypeLabel.attributedText = String.getAttributedText(firstText: "Time Needed For Job : ".localizedString(), secondText: daysStr, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.deliveryTimeLabel.attributedText = String.getAttributedText(firstText: "Providing Pickup & Drop Off : ".localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.status]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.pickupServiceLabel.text = "Bidder Comments: \(Helper.toString(object: info[Constants.UIKeys.bidComments]))"
        self.priceLabel.text = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.bidAmount]), text: StringConstants.Text.Currency.localizedString())
        
    }
    
    private func setupSparePartsData(info: [String: AnyObject]) {
        self.recoveryTypeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.timeNeededText.localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.daysForService]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.deliveryTimeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.partTypeText.localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.partType]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.pickupServiceLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.partNameText.localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.partName]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.priceLabel.text = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.bidAmount]), text: StringConstants.Text.Currency.localizedString())
    }
    
    private func setupWheelsData(info: [String: AnyObject]) {
        
        self.recoveryTypeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.timeNeededText.localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.time]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.deliveryTimeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.serviceType.localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.serviceType]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        
        let partName = Helper.toString(object: info[Constants.UIKeys.partType])
        
        if !partName.isEmpty {
        
            self.pickupServiceLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.partTypeText.localizedString(), secondText:partName, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        } else {
            self.pickupServiceLabel.attributedText = String.getAttributedText(firstText: "Wheel Size : ".localizedString(), secondText:Helper.toString(object: info[Constants.UIKeys.wheelSize]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        }
        
        self.priceLabel.text = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.bidAmount]), text: StringConstants.Text.Currency.localizedString())
    }
    
    private func setupBWData(info: [String: AnyObject]) {
        self.pickupServiceLabel.isHidden = true
        let days = Helper.toInt(info[Constants.UIKeys.time])
        var daysStr = ""
        if days == 0 || days == 1 {
            daysStr = "\(days) day"
            
        } else {
            daysStr = "\(days) days"
        }
        self.recoveryTypeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.timeNeededText.localizedString(), secondText: daysStr, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.deliveryTimeLabel.attributedText = String.getAttributedText(firstText: Helper.toString(object: info[Constants.UIKeys.serviceType]), secondText: Helper.toString(object: info[Constants.UIKeys.serviceTypeValue]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.pickupServiceLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.partTypeText.localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.partType]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.priceLabel.text = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.bidAmount]), text: StringConstants.Text.Currency.localizedString())
    }
    
    private func setupQuickWashData(info: [String: AnyObject]) {
        self.pickupServiceLabel.isHidden = true
        let days = Helper.toInt(info[Constants.UIKeys.time])
        
        var daysStr = ""
        if days == 0 || days == 1 {
            daysStr = "\(days) day"
            
        } else {
            daysStr = "\(days) days"
        }
        
        self.deliveryTimeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.bidInspectionTime.localizedString(), secondText: daysStr, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)

        
        self.recoveryTypeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.recoveryType.localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.serviceType]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)

        self.priceLabel.text = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.bidAmount]), text: StringConstants.Text.Currency.localizedString())
    }
    
    private func setupBVData(info: [String: AnyObject]) {
        self.pickupServiceLabel.isHidden = true
        self.recoveryTypeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.bankName.localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.bankName]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        let vehicleValue = Helper.toInt(info[Constants.UIKeys.carPrice])
        self.deliveryTimeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.requestedCar.localizedString(), secondText: "AED \(vehicleValue)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.priceLabel.text = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.bidAmount]), text: StringConstants.Text.Currency.localizedString())
    }
    
    private func setupInsuranceData(info: [String: AnyObject]) {
        self.recoveryTypeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.insuranceName.localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.time]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.deliveryTimeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.insuranceTypeText.localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.serviceType]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.pickupServiceLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.vehicleValueText.localizedString(), secondText: "AED \(Helper.toInt(info[Constants.UIKeys.vehicleValue]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        self.priceLabel.text = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.bidAmount]), text: StringConstants.Text.Currency.localizedString())
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo, type: ChooseServiceType) {
        if let info = cellInfo.info {
            self.agencyNameLabel.text = Helper.toString(object: info[ConstantAPIKeys.name])
            self.bidId = Helper.toInt(info[ConstantAPIKeys.bidId])
            self.vatLabel.text = Helper.toString(object: info[ConstantAPIKeys.vatAmmount])
            if let image = info[Constants.UIKeys.image] as? String, !image.isEmpty {
                self.agencyImageView.setImage(urlStr: Helper.toString(object: image), placeHolderImage: #imageLiteral(resourceName: "user_placeholder"))
            }
            //vatAmmount
            // self.agencyRatingView.rating = Helper.toInt(info[ConstantAPIKeys.rating])
            self.agencyRatingView.configureView(rating:Helper.toDouble(info[ConstantAPIKeys.rating]))
            switch type {
            case .recovery:
                self.setupRecoveryData(info: info)
            case .vehicleInspection:
                self.setupInspectionData(info: info)
            case .upholstery:
                self.setupForUpholstery(info: info)
            case .carServicing:
                self.setupServicingData(info: info)
            case .spareParts:
                self.setupSparePartsData(info: info)
            case .wheels:
                self.setupWheelsData(info: info)
            case .bankValuation:
                self.setupBVData(info: info)
            case .insurance:
                self.setupInsuranceData(info: info)
            case .bodyWork:
                self.setupBWData(info: info)
            case .carDetailing:
                self.setupCarDetailingData(info: info)
            default:
                break
            }
        }
        
    }
    
    //MARK: - IBActions
    @IBAction func tapCross(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapCross(cell: self)
        }
        
    }
    
    @IBAction func tapCheck(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapCheck(cell: self)
        }
    }
}
