//
//  RecoveryDetailBidListViewC+TableView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 13/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit
import MapKit

extension RDBidListViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recoveryDetailDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.recoveryDetailDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellInfo = self.recoveryDetailDataSource[indexPath.row]
        guard let cellType = cellInfo.cellType else { return self.recoveryDetailDataSource[indexPath.row].height }
        if self.tenderStatus == .completedTenders || (self.tenderStatus == .allBidsRejected) {
            
            switch cellType {
            case .STBidsCountCell:
                return Constants.CellHeightConstants.height_0
            case .RDBidListCell:
                return Constants.CellHeightConstants.height_0
            default:
                break
            }
        }
        return self.recoveryDetailDataSource[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cellInfo = self.recoveryDetailDataSource[indexPath.row]
        guard let cellType = cellInfo.cellType else { return }
        switch cellType {
        case .RDBidListCell:
            if let info = cellInfo.info {
                self.requestBidTenderDetails(bidId: Helper.toInt(info[Constants.UIKeys.id]))
            }

           break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let lastVisibleRow = self.recoveryDetailTableView.indexPathsForVisibleRows?.last?.row else { return }
        if (self.recoveryDetailDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
            if let tenderDetail = self.tenderDetails {
                self.requestBidListAPI(tenderDetails: tenderDetail)
            }
        }
    }
    
}

extension RDBidListViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .STCarDetailCell:
            let cell: STCarDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo, type: .recoveryRequest, tenderStatus: self.tenderStatus)
            return cell
            
        case .RDMapCell:
            let cell: RDMapCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .RDContactInfoCell:
            let cell: RDContactInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .STBidsCountCell:
            let cell: STBidsCountCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .recoveryRequest)
            return cell
            
        case .RDBidListCell:
            let cell: RDBidListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo, type: .recovery)
            return cell
            
        case .DashedTextFieldCell:
            let cell: DashedTextFieldCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            cell.isUserInteractionEnabled = false
            return cell
            
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

extension RDBidListViewC: RDBidListCellDelegate {

    func didTapCheck(cell: RDBidListCell) {
        guard let indexPath = self.recoveryDetailTableView.indexPath(for: cell) else { return }
        let bidId = Helper.toInt(self.recoveryDetailDataSource[indexPath.row].info?[Constants.UIKeys.id])
        
        Alert.showAlertWithActionWithCancel(title: StringConstants.Text.AppName.localizedString(), message: StringConstants.Message.albaDisclaimer.localizedString(), actionTitle: StringConstants.Text.ok.localizedString(), cancelTitle: StringConstants.Text.cancel.localizedString(), action: { (_) in
            let bidAmount = Helper.toDouble(self.recoveryDetailDataSource[indexPath.row].info?[Constants.UIKeys.bidAmount])
            self.requestAcceptBid(bidId: bidId, winAmount: bidAmount)
            
        }, cancelAction: { (_) in

        })
        
    }
    
    func didTapCross(cell: RDBidListCell) {
        guard let indexPath = self.recoveryDetailTableView.indexPath(for: cell) else { return }
        let bidId = Helper.toInt(self.recoveryDetailDataSource[indexPath.row].info?[Constants.UIKeys.id])
        self.requestRejectBid(bidId: bidId)
    }
}

extension RDBidListViewC: STCarDetailCellDelegate {
    func didTapCancelButton(cell: STCarDetailCell) {
        self.requestCancelTender()
    }
    
}
