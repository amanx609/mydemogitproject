//
//  RecoveryDetailBidListViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 13/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol RDBidListVModeling: BaseVModeling {
    func getRecoveryDetailDataSource(tenderDetails: RDBidListModel, bidList: [BidListModel],tenderStatus: TenderStatus) -> [CellInfo]
    func requestTenderDetailsAPI(params: APIParams, completion: @escaping (RDBidListModel) -> Void)
    func requestBidListAPI(params: APIParams, completion: @escaping (Int, Int, [BidListModel]) -> Void)
    func requestAcceptBidAPI(params: APIParams, completion: @escaping (Bool) -> Void)
    func requestRejectBidAPI(params: APIParams, completion: @escaping (Bool) -> Void)
    func requestCancelTenderAPI(params: APIParams, completion: @escaping (Bool) -> Void)
}

class RDBidListViewM: BaseViewM, RDBidListVModeling {
    
    
    func getRecoveryDetailDataSource(tenderDetails: RDBidListModel, bidList: [BidListModel],tenderStatus: TenderStatus) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //CarDetailCell
        var carDetailInfo = [String: AnyObject]()
        carDetailInfo[Constants.UIKeys.cellInfo] = tenderDetails
        let carDetailCell = CellInfo(cellType: .STCarDetailCell, placeHolder: "", value: "", info: carDetailInfo, height: Constants.CellHeightConstants.height_240)
        array.append(carDetailCell)
        
        
        if tenderStatus == .completedTenders {
            
            //ContactInfoCell
            let contactInfoCell = CellInfo(cellType: .RDContactInfoCell, placeHolder: Helper.toString(object: tenderDetails.name), value: Helper.toString(object: tenderDetails.contact), info: nil, height: Constants.CellHeightConstants.height_100)
            array.append(contactInfoCell)
            
            //MapCell
            var mapInfo = [String: AnyObject]()
            mapInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
            let mapCell = CellInfo(cellType: .RDMapCell, placeHolder: "", value: "", info: mapInfo, height: Constants.CellHeightConstants.height_165)
            array.append(mapCell)
            
            //Pickup Code Cell
            var codeTitleInfo = [String: AnyObject]()
            codeTitleInfo[Constants.UIKeys.placeholderText] = "Pickup Code".localizedString() as AnyObject
            codeTitleInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.center as AnyObject
            let codeTitleCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "".localizedString(), value: "", info: codeTitleInfo, height: Constants.CellHeightConstants.height_40)
            array.append(codeTitleCell)

            
            //Coupon code cell
            var codeCellInfo = [String: AnyObject]()
            codeCellInfo[Constants.UIKeys.backgroundColor] = UIColor.pinkButtonColor as AnyObject
            codeCellInfo[Constants.UIKeys.boundaryColor] = UIColor.redButtonColor as AnyObject
            codeCellInfo[Constants.UIKeys.inputType] = TextInputType.pickupCode as AnyObject
            let codeCell = CellInfo(cellType: .DashedTextFieldCell, placeHolder: "", value: Helper.toString(object: tenderDetails.pickupCode), info: codeCellInfo, height: Constants.CellHeightConstants.height_60)
            array.append(codeCell)
            
            //Drop Off Cell
            var dropTitleInfo = [String: AnyObject]()
            dropTitleInfo[Constants.UIKeys.placeholderText] = "Drop off Code".localizedString() as AnyObject
            dropTitleInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.center as AnyObject
            let dropTitleCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "".localizedString(), value: "", info: dropTitleInfo, height: Constants.CellHeightConstants.height_40)
            array.append(dropTitleCell)
            
            var dropOffCode = Helper.toString(object: tenderDetails.dropOffCode)
            if dropOffCode == "N/A" || dropOffCode == "n/a" {
                dropOffCode = ""
            }
            var codeCell2Info = [String: AnyObject]()
            codeCell2Info[Constants.UIKeys.backgroundColor] = UIColor.lightGreenColor as AnyObject
            codeCell2Info[Constants.UIKeys.boundaryColor] = UIColor.greenColor as AnyObject
            codeCell2Info[Constants.UIKeys.inputType] = TextInputType.dropOffCode as AnyObject
            let codeCell2 = CellInfo(cellType: .DashedTextFieldCell, placeHolder: "To be provided  by driver after end of recovery".localizedString(), value: dropOffCode, info: codeCell2Info, height: Constants.CellHeightConstants.height_60)
            array.append(codeCell2)
            
        } else {
            
            //MapCell
            var mapInfo = [String: AnyObject]()
            mapInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
            let mapCell = CellInfo(cellType: .RDMapCell, placeHolder: "", value: "", info: mapInfo, height: Constants.CellHeightConstants.height_165)
            array.append(mapCell)
            
            //ContactInfoCell
            let contactInfoCell = CellInfo(cellType: .RDContactInfoCell, placeHolder: Helper.toString(object: tenderDetails.name), value: Helper.toString(object: tenderDetails.contact), info: nil, height: Constants.CellHeightConstants.height_100)
            array.append(contactInfoCell)
            
            //BidsCountCell
            var bidsCountCellInfo = [String: AnyObject]()
            bidsCountCellInfo[Constants.UIKeys.isSecondDetailPage] = false as AnyObject
            let bidsCount = Helper.toInt(tenderDetails.bidsCount)
            let bidsCountCell = CellInfo(cellType: .STBidsCountCell, placeHolder: "", value: Helper.toString(object: bidsCount), info: bidsCountCellInfo, height: Constants.CellHeightConstants.height_55)
            array.append(bidsCountCell)
            
            //BidCell
            for i in 0..<bidList.count {
                var bidCellInfo = [String: AnyObject]()
                bidCellInfo[Constants.UIKeys.recoveryType] = bidList[i].recoveryType as AnyObject
                bidCellInfo[Constants.UIKeys.bidAmount] = bidList[i].amount as AnyObject
                bidCellInfo[Constants.UIKeys.time] = bidList[i].daysForService as AnyObject
                bidCellInfo[Constants.UIKeys.id] = bidList[i].bidId as AnyObject
                bidCellInfo[ConstantAPIKeys.name] = bidList[i].name as AnyObject
                bidCellInfo[ConstantAPIKeys.rating] = bidList[i].rating as AnyObject
                bidCellInfo[ConstantAPIKeys.vatAmmount] = tenderDetails.getVatPercentage() as AnyObject
                bidCellInfo[ConstantAPIKeys.image] = bidList[i].image as AnyObject

                let firstBidCell = CellInfo(cellType: .RDBidListCell, placeHolder: "", value: "", info: bidCellInfo, height: UITableView.automaticDimension)
                array.append(firstBidCell)
            }
        }
        return array
    }
    
    func requestTenderDetailsAPI(params: APIParams, completion: @escaping (RDBidListModel) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderDetails(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData(),
                    let tenderDetails = RDBidListModel(jsonData: resultData) {
                    completion(tenderDetails)
                }
            }
        }
    }
    
    func requestBidListAPI(params: APIParams, completion: @escaping (Int, Int, [BidListModel]) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .bidList(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let resultData = result.toJSONData(),
                    let bidList = Bids(jsonData: resultData) {
                    completion(nextPageNumber, perPage,bidList.bids ?? [])
                }
            }
        }
    }
    
    func requestAcceptBidAPI(params: APIParams, completion: @escaping (Bool) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .acceptBid(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let data = result[ConstantAPIKeys.data] as? [String: AnyObject]
//                    let jsonData = data.toJSONData(),
//                    let secondDetailData = AcceptBidModel(jsonData: jsonData)
                {
                    completion(success)
                }
            }
        }
    }
    
    func requestRejectBidAPI(params: APIParams, completion: @escaping (Bool) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .rejectBid(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                {
                    print(result)
                    completion(success)
                }
            }
        }
        
        
    }
    
    func requestCancelTenderAPI(params: APIParams, completion: @escaping (Bool) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .cancelTender(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject]
                {
                    completion(success)
                }
            }
        }
        
        
    }
}
