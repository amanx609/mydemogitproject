//
//  AddNewCar+TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension CAddNewCarViewC: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.addCarDataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellInfo = self.addCarDataSource[indexPath.row]
    return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return self.addCarDataSource[indexPath.row].height
  }
}

extension CAddNewCarViewC {
  
  func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
    guard let cellType = cellInfo.cellType else { return UITableViewCell() }
    switch cellType {
    case .DropDownCell:
      let cell: DropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate = self
      cell.configureView(cellInfo: cellInfo)
      return cell
      
    case .TextInputCell:
      let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate = self
      cell.placeholderView.isHidden = true
      cell.configureView(cellInfo: cellInfo)
      
      if indexPath.row == 3 || indexPath.row == 7 {
        cell.inputTextField.inputAccessoryView = Helper.getSingleButtonToolBar(buttonTitle: StringConstants.Text.done.localizedString(), selector: #selector(self.doneButtonTapped), target: self)
      } else {
        cell.inputTextField.inputAccessoryView = nil
      }
      
      return cell
      
    default:
      return UITableViewCell()
    }
  }
}

//MARK: - DropDownCellDelegate
extension CAddNewCarViewC: DropDownCellDelegate {
  func didTapDropDownCell(cell: DropDownCell) {
    guard let indexPath = self.addNewCarTableView.indexPath(for: cell) else { return }
    self.showDropDownAt(indexPath: indexPath)
  }
}

//MARK: - TextInputCellDelegate
extension CAddNewCarViewC: TextInputCellDelegate {
  func tapNextKeyboard(cell: TextInputCell) {
    guard var indexPath = self.addNewCarTableView.indexPath(for: cell) else { return }
    indexPath.row += 1
    if let cell = self.addNewCarTableView.cellForRow(at: indexPath) as? TextInputCell {
      cell.inputTextField.becomeFirstResponder()
    } else {
      self.view.endEditing(true)
    }
  }
  
  func didChangeText(cell: TextInputCell, text: String) {
    guard let indexPath = self.addNewCarTableView.indexPath(for: cell) else { return }
    self.addCarDataSource[indexPath.row].value = text
  }
}
