//
//  AddNewCarViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

class CAddNewCarViewC: BaseViewC {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var addNewCarTableView: TPKeyboardAvoidingTableView!
    @IBOutlet weak var saveCarButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    
    //MARK: - Properties
    var viewModel: CAddNewCarViewModeling?
    var addCarDataSource: [CellInfo] = []
    var vehicleBrandDataSource: [[String: AnyObject]] = []
    var vehicleTypeDataSource: [[String: AnyObject]] = []
    var vehicleModelDataSource: [[String: AnyObject]] = []
    var cAddNewCarType: CAddNewCarType = .add
    var myCarsModel: MyCarsModel?

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        setupView()
        roundCorners()
        setupViewModel()
        registerNibs()
        setupTableView()
        self.setupNavigationBarTitle(title: cAddNewCarType  == .add ? StringConstants.Text.addNewCar.localizedString() : "Update Car".localizedString() , leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.requestVehicleBrandAPI()
        self.requestVehicleTypeAPI()
    }
    
    private func setupTableView() {
        self.addNewCarTableView.separatorStyle = .none
        addNewCarTableView.backgroundColor = UIColor.white
        addNewCarTableView.delegate = self
        addNewCarTableView.dataSource = self
        if let dataSource = self.viewModel?.getAddCarDataSource(myCarsModel: myCarsModel) {
            self.addCarDataSource = dataSource
        }
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = CAddNewCarVM()
        }
    }
    
    private func roundCorners() {
        Threads.performTaskInMainQueue  {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.saveCarButton.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
        }
    }
    
    private func setupView() {
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        self.saveCarButton.setTitle(StringConstants.Text.saveCar.localizedString(), for: .normal)
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
    }
    
    private func registerNibs() {
        self.addNewCarTableView.register(DropDownCell.self)
        self.addNewCarTableView.register(TextInputCell.self)
    }
    
    func showDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        switch indexPath.row {
        case 0:
            if self.vehicleBrandDataSource.count == 0 {
                self.requestVehicleBrandAPI()
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Brand".localizedString(), arrayList: self.vehicleBrandDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let brandId = response[ConstantAPIKeys.id] as? Int,
                    let brandName = response[ConstantAPIKeys.title] as? String else { return }
                sSelf.requestVehicleModelAPI(brandId: brandId)
                sSelf.addCarDataSource[indexPath.row].value = brandName
                sSelf.addCarDataSource[indexPath.row].placeHolder = "\(brandId)"
                sSelf.addNewCarTableView.reloadData()
            }
        case 1:
            if self.vehicleModelDataSource.count == 0 {
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Model".localizedString(), arrayList: self.vehicleModelDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let modelName = response[ConstantAPIKeys.title] as? String,
                    let modelId = response[ConstantAPIKeys.id] as? Int else { return }
                
                sSelf.addCarDataSource[indexPath.row].value = modelName
                sSelf.addCarDataSource[indexPath.row].placeHolder = "\(modelId)"
                sSelf.addNewCarTableView.reloadData()
            }
        case 2:
            if self.vehicleTypeDataSource.count == 0 {
                self.requestVehicleTypeAPI()
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Type".localizedString(), arrayList: self.vehicleTypeDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let carType = response[ConstantAPIKeys.title] as? String,
                    let carId = response[ConstantAPIKeys.id] as? Int else { return }
                sSelf.addCarDataSource[indexPath.row].value = carType
                sSelf.addCarDataSource[indexPath.row].placeHolder = "\(carId)"
                sSelf.addNewCarTableView.reloadData()
            }
        case 3:
            let yearsList = Helper.getListOfYearsFrom(2005)
            listPopupView.initializeViewWith(title: "Car Year".localizedString(), arrayList: yearsList, key: Constants.UIKeys.year) { [weak self] (response) in
                guard let sSelf = self,
                    let carType = response[Constants.UIKeys.year] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = carType
                sSelf.addNewCarTableView.reloadData()
            }
        case 6:
            listPopupView.initializeViewWith(title: "No. Of Cylinders".localizedString(), arrayList: Constants.carCylindersList, key: Constants.UIKeys.noOfCylinders) { [weak self] (response) in
                guard let sSelf = self,
                    let noOfCylinders = response[Constants.UIKeys.noOfCylinders] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = noOfCylinders
                sSelf.addNewCarTableView.reloadData()
            }
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    //MARK: - API Methods
    func requestVehicleBrandAPI() {
        self.viewModel?.requestVehicleBrandAPI(completion: { (success, vehicleBrands) in
            if success {
                self.vehicleBrandDataSource = vehicleBrands
                
                if let brandId = self.myCarsModel?.make {
                    self.requestVehicleModelAPI(brandId: brandId)
                }
            }
        })
    }
    
    func requestVehicleTypeAPI() {
        self.viewModel?.requestVehicleTypeAPI(completion: { (success, vehicleTypes) in
            if success {
                self.vehicleTypeDataSource = vehicleTypes
            }
        })
    }
    
    func requestVehicleModelAPI(brandId: Int) {
        self.viewModel?.requestVehicleModelAPI(brandId: brandId, completion: { (success, vehicleModels) in
            if success {
                self.vehicleModelDataSource = vehicleModels
            }
        })
    }
    
    func callAddCarAPI() {
        self.viewModel?.requestPostCarAPI(arrData: self.addCarDataSource, completion: { (success) in
            Threads.performTaskInMainQueue {
                self.navigationController?.popViewController(animated: true)
            }
        })
    }
    
    func callUpdateAddCarAPI() {
        self.viewModel?.requestUpdateCarAPI(carId:Helper.toInt(myCarsModel?.id),arrData: self.addCarDataSource, completion: { (success) in
            Threads.performTaskInMainQueue {
                self.navigationController?.popToRootViewController(animated: true)
            }
        })
    }
    
    //MARK: - IBActions
    @IBAction func saveCarButtonTapped(_ sender: Any) {
        
        if cAddNewCarType == .add {
            self.callAddCarAPI()
        } else {
            self.callUpdateAddCarAPI()
        }
    }
    
    //MARK: - Selectors
    @objc func doneButtonTapped() {
      self.view.endEditing(true)
    }
}
