//
//  AddNewCarVM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol CAddNewCarViewModeling {
    
    func getAddCarDataSource(myCarsModel: MyCarsModel?) -> [CellInfo]
    func requestVehicleBrandAPI(completion: @escaping (Bool, [[String: AnyObject]])-> Void)
    func requestVehicleTypeAPI(completion: @escaping (Bool, [[String: AnyObject]])-> Void)
    func requestVehicleModelAPI(brandId: Int, completion: @escaping (Bool, [[String: AnyObject]])-> Void)
    func requestPostCarAPI(arrData: [CellInfo], completion: @escaping (Bool)-> Void)
    func requestUpdateCarAPI(carId:Int, arrData: [CellInfo], completion: @escaping (Bool)-> Void)
}

class CAddNewCarVM: CAddNewCarViewModeling {
    
    func getAddCarDataSource(myCarsModel: MyCarsModel?) -> [CellInfo] {
        var array = [CellInfo]()
        
        //Select Car Brand Cell
        let brandId = myCarsModel == nil ? "" : Helper.toString(object: myCarsModel?.make)
        let brandName = myCarsModel == nil ? StringConstants.Text.selectCarBrand.localizedString() : Helper.toString(object: myCarsModel?.brandName)
        let brandCell = CellInfo(cellType: .DropDownCell, placeHolder: brandId, value: brandName, height: Constants.CellHeightConstants.height_80)
        array.append(brandCell)
        
        // Car model Cell
        let modelId = myCarsModel == nil ? "" : Helper.toString(object: myCarsModel?.model)
        let modelName = myCarsModel == nil ? StringConstants.Text.selectCarModel.localizedString() : Helper.toString(object: myCarsModel?.modelName)
        let modelCell = CellInfo(cellType: .DropDownCell, placeHolder: modelId, value: modelName, height: Constants.CellHeightConstants.height_80)
        array.append(modelCell)
        
        //Select car type
        let typeId = myCarsModel == nil ? "" : Helper.toString(object: myCarsModel?.type)
        let typeName = myCarsModel == nil ? StringConstants.Text.selectCarType.localizedString() : Helper.toString(object: myCarsModel?.typeName)
        let carTypeCell = CellInfo(cellType: .DropDownCell, placeHolder: typeId, value: typeName, height: Constants.CellHeightConstants.height_80)
        array.append(carTypeCell)
        
        // select car year
        var yearInfo = [String: AnyObject]()
        yearInfo[Constants.UIKeys.inputType] = TextInputType.modelYear as AnyObject
        let carYearCell = CellInfo(cellType: .TextInputCell, placeHolder: "Car year".localizedString(), value: Helper.toString(object: myCarsModel?.year), info: yearInfo, height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.numberPad)
        array.append(carYearCell)
        
        // select carPlate
        var carPlateInfo = [String: AnyObject]()
        carPlateInfo[Constants.UIKeys.inputType] = TextInputType.plateNumber as AnyObject
        let carPlateCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.carPlate.localizedString(), value: Helper.toString(object: myCarsModel?.plateNumber), info: carPlateInfo,  height: Constants.CellHeightConstants.height_80)
        array.append(carPlateCell)
        
        // select carMileage
        var carMileageInfo = [String: AnyObject]()
        carMileageInfo[Constants.UIKeys.inputType] = TextInputType.mileage as AnyObject
        let carMileageCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.carMileage.localizedString(), value: Helper.toString(object: myCarsModel?.mileage), info: carMileageInfo, height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.numberPad)
        array.append(carMileageCell)
        
        //No. of cylinders
        let numOfCylinders = myCarsModel == nil ? StringConstants.Text.numOfCylinders.localizedString() : Helper.toString(object: myCarsModel?.cylinders)
        let cylinderNum = CellInfo(cellType: .DropDownCell, placeHolder: "", value: numOfCylinders, height: Constants.CellHeightConstants.height_80)
        array.append(cylinderNum)
        
        // engine Size
        var engineSizeInfo = [String: AnyObject]()
        engineSizeInfo[Constants.UIKeys.inputType] = TextInputType.engineSize as AnyObject
        let engineSizeCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.engineSize.localizedString(), value: Helper.toString(object: myCarsModel?.engine), info: engineSizeInfo, height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.decimalPad)
        array.append(engineSizeCell)
        
        return array
    }
    
    //MARK: - API Methods
    func requestVehicleBrandAPI(completion: @escaping (Bool, [[String: AnyObject]])-> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .vehicleBrand)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let vehicleBrands = safeResponse[ConstantAPIKeys.result] as? [[String: AnyObject]] {
                    completion(success, vehicleBrands)
                }
            }
        }
    }
    
    func requestVehicleTypeAPI(completion: @escaping (Bool, [[String: AnyObject]])-> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .vehicleType)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let vehicleBrands = safeResponse[ConstantAPIKeys.result] as? [[String: AnyObject]] {
                    completion(success, vehicleBrands)
                }
            }
        }
    }
    
    func requestVehicleModelAPI(brandId: Int, completion: @escaping (Bool, [[String: AnyObject]])-> Void) {
        
        var vehicleModelParams = APIParams()
        vehicleModelParams[ConstantAPIKeys.brandId] = brandId as AnyObject
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .vehicleModel(param: vehicleModelParams))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let vehicleBrands = safeResponse[ConstantAPIKeys.result] as? [[String: AnyObject]] {
                    completion(success, vehicleBrands)
                }
            }
        }
    }
    
    func requestPostCarAPI(arrData: [CellInfo], completion: @escaping (Bool)-> Void) {
        if self.validatePostCarData(arrData: arrData) {
            let postCarParams = self.getPostCarParams(carId: nil, arrData: arrData)
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .postCar(param: postCarParams))) { (response, success) in
                if success {
                    if let safeResponse =  response as? [String: AnyObject] {
                        completion(success)
                    }
                }
            }
        }
    }
    
    func requestUpdateCarAPI(carId:Int, arrData: [CellInfo], completion: @escaping (Bool)-> Void) {
        if self.validatePostCarData(arrData: arrData) {
            var postCarParams = self.getPostCarParams(carId: carId, arrData: arrData)
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .updateCar(param: postCarParams))) { (response, success) in
                if success {
                    if let safeResponse =  response as? [String: AnyObject] {
                        completion(success)
                    }
                }
            }
        }
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
    private func validatePostCarData(arrData: [CellInfo]) -> Bool {
        var isValid = true
        if let carBrand = arrData[0].placeHolder, carBrand.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select car brand.".localizedString())
            isValid = false
        } else if let carModel = arrData[1].placeHolder, carModel.isEmpty  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select car model.".localizedString())
            isValid = false
        } else if let carType = arrData[2].placeHolder, carType.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select car type.".localizedString())
            isValid = false
        } else if let carYear = arrData[3].value, carYear.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter car year.".localizedString())
            isValid = false
        } else if let carPlateNumber = arrData[4].value, carPlateNumber.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter car plate / VIN number.".localizedString())
            isValid = false
        } else if let carMileage = arrData[5].value, carMileage.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter car mileage.".localizedString())
            isValid = false
        } else if let noOfCylinders = arrData[6].value, noOfCylinders == StringConstants.Text.numOfCylinders {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select no. of cylinders.".localizedString())
            isValid = false
        } else if let engineSize = arrData[7].value, engineSize.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter engine size.".localizedString())
            isValid = false
        } else if let carYear = arrData[3].value {
            
            if Helper.toInt(carYear) < Constants.Validations.minYear || Helper.toInt(carYear) > Helper.getCurrentYear() {
                Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter year between 1990 to your current year".localizedString())
                isValid = false
            }
            
        }
        
        return isValid
    }
    
    
    private func getPostCarParams(carId:Int?,arrData: [CellInfo])-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.brand] = arrData[0].placeHolder as AnyObject
        params[ConstantAPIKeys.carModel] = arrData[1].placeHolder as AnyObject
        params[ConstantAPIKeys.carType] = arrData[2].placeHolder as AnyObject
        params[ConstantAPIKeys.carYear] = arrData[3].value as AnyObject
        params[ConstantAPIKeys.plateNumber] = arrData[4].value as AnyObject
        params[ConstantAPIKeys.mileage] = arrData[5].value as AnyObject
        params[ConstantAPIKeys.cylinders] = arrData[6].value as AnyObject
        params[ConstantAPIKeys.engine] = arrData[7].value as AnyObject
        
        if let carId = carId {
            params[ConstantAPIKeys.id] = carId as AnyObject
        }
        return params
    }
    
}
