//
//  FilterCarsViewC.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 10/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class FilterCarsViewC: BaseViewC {
    
    //MARK:- IBOutlet
    @IBOutlet weak var collectionViewFilters: UICollectionView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var applyFiltersButton: UIButton!
    @IBOutlet weak var filterOptionsTableView: UITableView!
    @IBOutlet weak var numberOfCarLabel: UILabel!
    
    //MARK:- Variables
    var viewModel: FilterCarsVModeling?
    var filterTypeDataSource: [FilterType] = []
    var brandTypeDataSource: [VehicleBrand] = []
    var filterOptionsDataSource: [CellInfo] = []
    var mileageTypeDataSource: [MileageType] = []
    var selectedYear = ""
    var selectedMileage = ""
    var maximumPrice = ""
    var monthlyInstallment = ""
    var selectParams:((APIParams?)-> Void)?
    var selectedParams: APIParams = APIParams()
    var filterType = 0
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()
    }
    
    override func navigationButtonTapped(_ sender: AnyObject) {
        guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
        switch buttonType {
        case .cross:
            self.dismiss(animated: true, completion: nil)
        case .reset:
            self.resetFilter()
        default: break
        }
    }
    
    //MARK:- Private Methods
    private func setUp() {
        
        //
        self.selectedYear = Helper.toString(object: self.selectedParams[ConstantAPIKeys.year])
        self.selectedMileage = Helper.toString(object: self.selectedParams[ConstantAPIKeys.mileage])
        self.monthlyInstallment = Helper.toString(object: self.selectedParams[ConstantAPIKeys.maxMonthlyInstallment])
        self.maximumPrice = Helper.toString(object: self.selectedParams[ConstantAPIKeys.maximumPrice])
        
        //
        self.recheckVM()
        self.setupNavigationBarTitle(title: "Filter".localizedString(), leftBarButtonsType: [.cross], rightBarButtonsType: [.reset])
        self.registerNibs()
        self.setUpTableView()
        self.setUpCollectionView()
        self.localizeTexts()
        self.setGradientOnApplyButton()
        self.numberOfCarLabel.text = ""

        if let dataSource = self.viewModel?.getFilterTypeOptionsDatasource() {
            self.filterTypeDataSource = dataSource
        }
        
        if let dataSourceOptions = self.viewModel?.getFilterOptionsDatasource(FilterOptions.brand.rawValue, arrBrandType: self.brandTypeDataSource, arrMileageType: self.mileageTypeDataSource, year: self.selectedYear) {
            self.filterOptionsDataSource = dataSourceOptions
        }
        
        self.loadMileageDatasource()
        self.requestVehicleBrandAPI()
        self.requestVehicleCountAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = FilterCarsViewM()
        }
    }
    
    private func setGradientOnApplyButton() {
        self.applyFiltersButton.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.gradientView.roundCorners(Constants.UIConstants.sizeRadius_7)
        Threads.performTaskInMainQueue {
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
    }
    
    private func localizeTexts() {
        self.applyFiltersButton.setTitle("Apply".localizedString(), for: .normal)
    }
    
    private func setUpCollectionView() {
        self.collectionViewFilters.delegate = self
        self.collectionViewFilters.dataSource = self
    }
    
    private func setUpTableView() {
        self.filterOptionsTableView.delegate = self
        self.filterOptionsTableView.dataSource = self
        self.filterOptionsTableView.separatorStyle = .none
        self.filterOptionsTableView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.collectionViewFilters.registerNib(nib: FilterTypeCollectionCell.className)
        self.filterOptionsTableView.registerMultiple(nibs: [ChooseBrandsTableCell.className, SelectYearTableCell.className, SelectPriceTableCell.className, SelectMileageTableCell.className])
    }
    
    func getDatasourceForFilterOptions(_ filterType: Int) {
        self.filterType = filterType
        if let dataSourceOptions = self.viewModel?.getFilterOptionsDatasource(filterType, arrBrandType: self.brandTypeDataSource, arrMileageType: self.mileageTypeDataSource, year: self.selectedYear) {
            self.filterOptionsDataSource = dataSourceOptions
        }
        Threads.performTaskInMainQueue {
            self.filterOptionsTableView.reloadData()
        }
    }
    
    func loadMileageDatasource() {
        
        if let dataSource = self.viewModel?.getMileageOptionsDatasource(seletedMileage: self.selectedMileage) {
            self.mileageTypeDataSource = dataSource
        }
    }
    
    //MARK: - API Methods
    func requestVehicleBrandAPI() {
        self.viewModel?.requestVehicleBrandAPI(completion: { (success, vehicleBrands) in
            if success {
                
                let selectedBrand = Helper.toString(object: self.selectedParams[ConstantAPIKeys.brand])
                
                let selectedBrandArr = selectedBrand.components(separatedBy: ",")

                if !selectedBrand.isEmpty {
                    
                    for vehicleBrand in vehicleBrands {
                        
                        let brandID = Helper.toString(object: vehicleBrand.id)
                        
                        var isSelected = false
                        
                        for id in selectedBrandArr {
                           
                            if id == brandID {
                                isSelected = true
                            }
                        }
                        
                        if isSelected {
                            vehicleBrand.isSelected = true
                            self.brandTypeDataSource.append(vehicleBrand)
                        }
                        else {
                            self.brandTypeDataSource.append(vehicleBrand)
                        }
                    }
                } else {
                    self.brandTypeDataSource = vehicleBrands
                }
                self.getDatasourceForFilterOptions(self.filterType)
            }
            self.requestVehicleCountAPI()
            
        })
    }
    
    func resetFilter() {
        self.selectedYear = ""
        self.selectedMileage = ""
        self.maximumPrice = ""
        self.monthlyInstallment = ""
        self.selectedParams = APIParams()
        for i in 0..<brandTypeDataSource.count {
            self.brandTypeDataSource[i].isSelected = false
        }
        self.loadMileageDatasource()
        self.getDatasourceForFilterOptions(self.filterType)
        Threads.performTaskInMainQueue {
            self.filterOptionsTableView.reloadData()
        }
        self.requestVehicleCountAPI()
    }
    
    //MARK: - API Methods
    func requestVehicleCountAPI() {
        self.viewModel?.requestVehicleCountAPI(brand: self.brandTypeDataSource, year: self.selectedYear, mileage: self.selectedMileage, maxMonthlyInstallment: self.monthlyInstallment, maximumPrice: self.maximumPrice,completion: { (success, count) in
            if success {
                self.numberOfCarLabel.text = count == "0" ? "No Car Found".localizedString() : count + " Cars Found".localizedString()
            }
        })
    }
    
    //MARK:- IBAction Methods
    @IBAction func tapApplyFilterBtn(_ sender: UIButton) {
        let param = self.viewModel?.getFilterParams(brand: self.brandTypeDataSource, year: self.selectedYear, mileage: self.selectedMileage, maxMonthlyInstallment: self.monthlyInstallment, maximumPrice: self.maximumPrice)
        self.selectParams?(param)
        self.dismiss(animated: true)
    }
         
}
