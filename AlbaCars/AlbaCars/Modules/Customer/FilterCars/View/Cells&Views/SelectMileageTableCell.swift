//
//  SelectMileageTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 11/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class SelectMileageTableCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var mileageCollectionView: UICollectionView!
    
    //MARK:- Variables
    var selectMileageOptionDidTapped:((Int)-> Void)?
    var data: [MileageType]? {
        didSet {
            if let data = self.data {
                configureCell(data)
            }
        }
    }
        
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.headerTitleLabel.text = "Maximum Mileage (KM)".localizedString()
        self.registerNibs()
    }
    
    private func registerNibs() {
        self.mileageCollectionView.registerNib(nib: MileageTypeCollectionCell.className)
    }
    
    private func configureCell(_ data: [MileageType]) {
        self.mileageCollectionView.delegate = self
        self.mileageCollectionView.dataSource = self
        self.mileageCollectionView.reloadData()
    }
    
    func getCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MileageTypeCollectionCell.className, for: indexPath) as? MileageTypeCollectionCell else {
            return UICollectionViewCell()
        }
        
        cell.configureCell(self.data![indexPath.row])
        
//        if let arrMileageRange = self.data {
//
//            cell.configureCell(self.data)
//
//            if self.data?[indexPath.row].isSelected == true {
//                cell.mileageRangeLabel.backgroundColor =  UIColor.redButtonColor
//                cell.mileageRangeLabel.textColor =  UIColor.white
//            } else {
//                cell.mileageRangeLabel.backgroundColor =  UIColor.clear
//                cell.mileageRangeLabel.textColor =  UIColor.blackColor
//            }
//        }
        
//
        
        
        return cell
    }
    
}

//MARK:- UICollectionView Delegates & Datasource Methods
extension SelectMileageTableCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let arrMileageRange = data {
            return arrMileageRange.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return getCell(collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150.0, height: 73.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        for i in 0..<self.data!.count {
            self.self.data![i].isSelected = false
        }        
        var selectedRow = -1
        if self.data![indexPath.row].isSelected == true {
            self.data![indexPath.row].isSelected = false
        } else {
            self.data![indexPath.row].isSelected = true
            selectedRow = indexPath.row
        }

        selectMileageOptionDidTapped?(selectedRow)

        Threads.performTaskInMainQueue {
            self.mileageCollectionView.reloadData()
        }
    }
}
