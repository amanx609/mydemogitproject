//
//  ChooseBrandsTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 11/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class ChooseBrandsTableCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var brandsCollectionView: UICollectionView!
    
    //MARK: - Variables
    var selectBrandOptionDidTapped:((IndexPath)-> Void)?
    var data: [VehicleBrand]? {
        didSet {
            if let data = self.data {
                configureCell(data)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUp()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUp() {
        self.headerLabel.text = "Choose Brands".localizedString()
        self.registerNibs()
    }
    
    private func registerNibs() {
        self.brandsCollectionView.registerNib(nib: BrandTypeCollectionCell.className)
    }
    
    private func configureCell(_ data: [VehicleBrand]) {
        self.brandsCollectionView.delegate = self
        self.brandsCollectionView.dataSource = self
        self.brandsCollectionView.reloadData()
    }
    
    func getCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BrandTypeCollectionCell.className, for: indexPath) as? BrandTypeCollectionCell else {
            return UICollectionViewCell()
        }
        if let arrBrands = self.data {
            cell.data = arrBrands[indexPath.item]
        }
        return cell
    }
}

//MARK:- UICollectionView Delegates & Datasource Methods
extension ChooseBrandsTableCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let arrBrands = data {
            return arrBrands.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return getCell(collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/3, height: 140.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectBrandOptionDidTapped?(indexPath)
    }
}
