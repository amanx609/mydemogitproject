//
//  MileageTypeCollectionCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 11/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class MileageTypeCollectionCell: UICollectionViewCell {
    
    //MARK:- IBOutlets
    @IBOutlet weak var mileageRangeLabel: UILabel!
    
    //MARK:- Variables
    //    var data: MileageType? {
    //        didSet {
    //            if let data = self.data {
    //                configureCell(data)
    //            }
    //        }
    //    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK:- Private Methods
    func configureCell(_ data: MileageType) {
        self.mileageRangeLabel.text = data.placeHolderRange
        self.mileageRangeLabel.makeLayer(color: UIColor.borderColor, boarderWidth: Constants.UIConstants.borderWidth_07, round: self.mileageRangeLabel.layer.frame.height/2)
        if data.isSelected == true {
            self.mileageRangeLabel.backgroundColor =  UIColor.redButtonColor
            self.mileageRangeLabel.textColor =  UIColor.white
        } else {
            self.mileageRangeLabel.backgroundColor =  UIColor.clear
            self.mileageRangeLabel.textColor =  UIColor.blackColor
        }
    }
    
}
