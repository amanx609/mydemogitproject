//
//  SelectPriceTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 11/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol SelectPriceTableCellDelegate: class, BaseProtocol {
    func didTapMonthlyInstallment(cell: SelectPriceTableCell)
    func didTapMaximumPrice(cell: SelectPriceTableCell)
}

class SelectPriceTableCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var monthlyInstallmentLabel: UILabel!
    @IBOutlet weak var monthlyInstallmentView: UIView!
    @IBOutlet weak var monthlyInstallmentTextField: UITextField!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var maximumPriceLabel: UILabel!
    @IBOutlet weak var maximumPriceView: UIView!
    @IBOutlet weak var maximumPriceTextField: UITextField!
    
    //MARK: - Variables
    weak var delegate: SelectPriceTableCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.monthlyInstallmentLabel.text = "Maximum Monthly Installment".localizedString()
        self.maximumPriceLabel.text = "Maximum Price".localizedString()
        self.orLabel.roundCorners(Constants.UIConstants.sizeRadius_10)
        self.monthlyInstallmentView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.monthlyInstallmentView.makeLayer(color: UIColor.borderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.maximumPriceView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.maximumPriceView.makeLayer(color: UIColor.borderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
    //MARK: - IBACtions
    @IBAction func tapMonthlyInstallment(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapMonthlyInstallment(cell: self)
        }
    }
    
    @IBAction func tapMaximumPrice(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapMaximumPrice(cell: self)
        }
    }
}

//MARK:- UITextField Delegates
extension SelectPriceTableCell: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
}
