//
//  FilterTypeCollectionCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 10/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class FilterTypeCollectionCell: UICollectionViewCell {
    
    //MARK:- IBOutlet
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var filterTypeLabel: UILabel!
    
    //MARK: - Variables
    var data: FilterType? {
        didSet {
            if let data = self.data {
                configureCell(data)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.configureView()
    }
    
    //MARK:- Private Methods
    private func configureView() {
        self.viewBg.roundCorners(self.viewBg.layer.frame.size.height/2)
    }
    
    private func configureCell(_ data: FilterType) {
        self.filterTypeLabel.text = data.type ?? ""
        self.viewBg.backgroundColor = data.isSelected == true ? UIColor.redButtonColor : UIColor.unselectedButtonColor
        self.filterTypeLabel.textColor = data.isSelected == true ? UIColor.white : UIColor.blackTextColor
    }
}
