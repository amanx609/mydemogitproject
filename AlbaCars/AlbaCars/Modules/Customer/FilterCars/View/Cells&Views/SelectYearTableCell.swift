//
//  SelectYearTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 11/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol SelectYearTableCellDelegate: class, BaseProtocol {
    func didTapOnLabel(cell: SelectYearTableCell)
}

class SelectYearTableCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var placeholderLabel: UILabel!
    
    //MARK: - Variables
    weak var delegate: SelectYearTableCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.containerView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.containerView.makeLayer(color: UIColor.borderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.placeholderLabel.text = cellInfo.value == "" ? "None".localizedString(): cellInfo.value
        
        if let info = cellInfo.info {
            if let headerTitle = info[Constants.UIKeys.headerTitle] as? String {
                self.headerTitleLabel.text = headerTitle
            }
            if let placeholder = info[Constants.UIKeys.placeholderText] as? String {
                self.placeholderLabel.text = placeholder
                self.placeholderLabel.textColor = .lightGray
            }
            if let value = info[Constants.UIKeys.valueText] as? String,
                let textColor = info[Constants.UIKeys.textColor] as? UIColor {
                self.placeholderLabel.text = value
                self.placeholderLabel.textColor = textColor
            }
        }
    }
    
    //MARK: - Public Methods
    func configureAuctionFilterView(cellInfo: CellInfo) {
      //  self.placeholderLabel.text = cellInfo.value
        
        if let info = cellInfo.info {
            if let headerTitle = info[Constants.UIKeys.headerTitle] as? String {
                self.headerTitleLabel.text = headerTitle
            }
            if let placeholder = info[Constants.UIKeys.placeholderText] as? String {
                self.placeholderLabel.text = placeholder
                self.placeholderLabel.textColor = .lightGray
            }
            if let value = cellInfo.value,!value.isEmpty,
                let textColor = info[Constants.UIKeys.textColor] as? UIColor {
                self.placeholderLabel.text = value
                self.placeholderLabel.textColor = textColor
            }
        }
    }
    
    
    //MARK: - IBACtions
    @IBAction func tapLabel(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapOnLabel(cell: self)
        }
    }
}
