//
//  BrandTypeCollectionCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 11/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class BrandTypeCollectionCell: UICollectionViewCell {
    
    //MARK:- IBOutlets
    @IBOutlet weak var brandImageView: UIImageView!
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var brandNameLabel: UILabel!
    
    //MARK:- Variables
    var data: VehicleBrand? {
        didSet {
            if let data = self.data {
                configureCell(data)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:- Private Methods
    private func configureCell(_ data: VehicleBrand) {
        //Image
        if let brandImage = data.logo {
            self.brandImageView.setImage(urlStr: brandImage, placeHolderImage: #imageLiteral(resourceName: "placeholder"))
        }
        
        self.brandImageView.clipsToBounds = true
        self.brandNameLabel.text = data.title
        self.selectedImageView.isHidden = !(data.isSelected ?? false)
    }

}
