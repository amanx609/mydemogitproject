//
//  FilterCarsViewC+TableView.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 10/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//MARK:- UICollectionView Delegates & Datasource Methods
extension FilterCarsViewC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterTypeDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return getCell(collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width-12)/4
        return CGSize(width: width, height: Constants.CollectionCellFrameConstants.height_45)
        //return CGSize(width: Constants.CollectionCellFrameConstants.width_76, height: Constants.CollectionCellFrameConstants.height_45)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        //let cellWidth: CGFloat = Constants.CollectionCellFrameConstants.width_76 // Your cell width
        
        let cellWidth: CGFloat =  (collectionView.frame.size.width-12)/4
        
        //     let numberOfCells = floor(view.frame.size.width / cellWidth)
        //   let edgeInsets = (view.frame.size.width - (4 * cellWidth)) / (4 + 1)
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for i in 0..<filterTypeDataSource.count {
            self.filterTypeDataSource[i].isSelected = false
        }
        self.filterTypeDataSource[indexPath.item].isSelected = true
        
        Threads.performTaskInMainQueue {
            self.collectionViewFilters.reloadData()
        }
        self.getDatasourceForFilterOptions(indexPath.row)
    }
}

//MARK:- UITableView Delegates & Datasource Methods
extension FilterCarsViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterOptionsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.filterOptionsDataSource[indexPath.row]
        return self.getCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.filterOptionsDataSource[indexPath.row].height
    }
}

extension FilterCarsViewC {
    func getCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilterTypeCollectionCell.className, for: indexPath) as? FilterTypeCollectionCell else {
            return UICollectionViewCell()
        }
        cell.data = self.filterTypeDataSource[indexPath.item]
        return cell
    }
    
    func getCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .ChooseBrandsTableCell:
            return getChooseBrandCell(tableView, indexPath: indexPath)
        case .SelectYearTableCell:
            return getSelectYearCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
        case .SelectMileageTableCell:
            return getSelectMileageCell(tableView, indexPath: indexPath)
        case .SelectPriceTableCell:
            return getSelectPriceCell(tableView, indexPath: indexPath)
        default:
            return UITableViewCell()
        }
    }
    
    func getChooseBrandCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: ChooseBrandsTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.data = self.brandTypeDataSource
        cell.selectBrandOptionDidTapped = { [weak self] (indexPath) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.brandTypeDataSource[indexPath.item].isSelected = !(strongSelf.brandTypeDataSource[indexPath.item].isSelected ?? false)
            Threads.performTaskInMainQueue {
                strongSelf.filterOptionsTableView.reloadData()
            }
            strongSelf.requestVehicleCountAPI()
        }
        return cell
    }
    
    func getSelectYearCell(_ tableView: UITableView, indexPath: IndexPath,cellInfo: CellInfo) -> UITableViewCell {
        let cell: SelectYearTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.configureView(cellInfo: cellInfo)
        return cell
    }
    
    func getSelectMileageCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: SelectMileageTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.data = self.mileageTypeDataSource
        cell.selectMileageOptionDidTapped = { [weak self] (indexPath) in
            guard let sSelf = self else {
                return
            }
            sSelf.selectedMileage = ""
            if indexPath >= 0  {
                sSelf.selectedMileage = Helper.toString(object: sSelf.mileageTypeDataSource[0].mileageRange)
            }

            sSelf.requestVehicleCountAPI()
        }
        return cell
    }
    
    func getSelectPriceCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: SelectPriceTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.maximumPriceTextField.text = self.maximumPrice == "" ? "All".localizedString() : "AED ".localizedString() + self.maximumPrice
        cell.monthlyInstallmentTextField.text = self.monthlyInstallment == "" ? "All".localizedString() : "AED ".localizedString() + self.monthlyInstallment
        return cell
    }
}
// SelectYearTableCellDelegate
extension FilterCarsViewC:SelectYearTableCellDelegate {
    func didTapOnLabel(cell: SelectYearTableCell) {
        guard let indexPath = self.filterOptionsTableView.indexPath(for: cell) else { return }
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        var yearsList = [[String: AnyObject]]()
        yearsList.append(contentsOf: [[Constants.UIKeys.year:"None".localizedString() as AnyObject]])
        yearsList.append(contentsOf: Helper.getListOfYearsFrom(2010))
        // var yearsList = Helper.getListOfYearsFrom(2010)
        listPopupView.initializeViewWith(title: "Year".localizedString(), arrayList: yearsList, key: Constants.UIKeys.year) { [weak self] (response) in
            guard let sSelf = self,
                let year = response[Constants.UIKeys.year] as? String else { return }
            sSelf.filterOptionsDataSource[indexPath.row].value = year
            sSelf.filterOptionsTableView.reloadData()
            sSelf.selectedYear = year == "None".localizedString() ? "" : year
            sSelf.requestVehicleCountAPI()
        }
        listPopupView.showWithAnimated(animated: true)
    }
}

// SelectPriceTableCellDelegate
extension FilterCarsViewC:SelectPriceTableCellDelegate {
    func didTapMonthlyInstallment(cell: SelectPriceTableCell) {
        guard let indexPath = self.filterOptionsTableView.indexPath(for: cell) else { return }
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: DropDownType.monthlyInstallment.title, arrayList: DropDownType.monthlyInstallment.dataList, key: DropDownType.monthlyInstallment.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let monthlyInstallment = response[DropDownType.monthlyInstallment.rawValue] as? String else { return }
            sSelf.monthlyInstallment = monthlyInstallment == "All".localizedString() ? "" : monthlyInstallment
            sSelf.monthlyInstallment = sSelf.monthlyInstallment.replacingOccurrences(of: ",", with: "")
            sSelf.monthlyInstallment = sSelf.monthlyInstallment.replacingOccurrences(of: " ", with: "")
            sSelf.monthlyInstallment = sSelf.monthlyInstallment.replacingOccurrences(of: "AED".localizedString(), with: "")
            // sSelf.filterOptionsDataSource[indexPath.row].value = monthlyInstallment
            sSelf.filterOptionsTableView.reloadData()
            sSelf.requestVehicleCountAPI()

        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    func didTapMaximumPrice(cell: SelectPriceTableCell) {
        guard let indexPath = self.filterOptionsTableView.indexPath(for: cell) else { return }
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: DropDownType.maximumPrice.title, arrayList: DropDownType.maximumPrice.dataList, key: DropDownType.maximumPrice.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let maximumPrice = response[DropDownType.maximumPrice.rawValue] as? String else { return }
            //sSelf.filterOptionsDataSource[indexPath.row].value = maximumPrice
            sSelf.maximumPrice = maximumPrice == "All".localizedString() ? "" : maximumPrice
            sSelf.maximumPrice = sSelf.maximumPrice.replacingOccurrences(of: ",", with: "")
            sSelf.maximumPrice = sSelf.maximumPrice.replacingOccurrences(of: " ", with: "")
            sSelf.maximumPrice = sSelf.maximumPrice.replacingOccurrences(of: "AED".localizedString(), with: "")
            sSelf.filterOptionsTableView.reloadData()
            sSelf.requestVehicleCountAPI()

        }
        listPopupView.showWithAnimated(animated: true)
    }
    
}
