//
//  FilterCarsViewM.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 10/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol FilterCarsVModeling: BaseVModeling {
    func getFilterTypeOptionsDatasource() -> [FilterType]
    func getFilterOptionsDatasource(_ id: Int, arrBrandType: [VehicleBrand], arrMileageType: [MileageType],year:String) -> [CellInfo]
    func getMileageOptionsDatasource(seletedMileage:String) -> [MileageType]
    func requestVehicleBrandAPI(completion: @escaping (Bool, [VehicleBrand])-> Void)
    func requestVehicleCountAPI(brand: [VehicleBrand], year: String, mileage: String, maxMonthlyInstallment: String, maximumPrice: String, completion: @escaping (Bool, String)-> Void)
    func getFilterParams(brand: [VehicleBrand], year: String, mileage: String, maxMonthlyInstallment: String, maximumPrice: String) -> APIParams
}

class FilterCarsViewM: BaseViewM, FilterCarsVModeling {
    func getMileageOptionsDatasource(seletedMileage:String) -> [MileageType] {
        
        var dataSource: [MileageType] = []
        var mileageID = 0
        for mileageRangeOptions in MileageRangeOptions.allValues{
            let range1 = MileageType(id: mileageID, isSelected: mileageRangeOptions.value == seletedMileage ? true: false, mileageRange: mileageRangeOptions.value,placeHolderRange:mileageRangeOptions.palceHolder)
            dataSource.append(range1)
            mileageID = mileageID + 1
        }
//
//        let range1 = MileageType(id: 1, isSelected: MileageRangeOptions.range0to10k.value == seletedMileage ? true: false, mileageRange: MileageRangeOptions.range0to10k.value,placeHolderRange:MileageRangeOptions.range0to10k.palceHolder)
//        let range2 = MileageType(id: 2, isSelected: MileageRangeOptions.range10to20k.value == seletedMileage ? true: false, mileageRange: MileageRangeOptions.range10to20k.value,placeHolderRange:MileageRangeOptions.range10to20k.palceHolder )
//        let range3 = MileageType(id: 3, isSelected: MileageRangeOptions.range20to30k.value == seletedMileage ? true: false, mileageRange: MileageRangeOptions.range20to30k.value,placeHolderRange:MileageRangeOptions.range20to30k.palceHolder )
//        let range4 = MileageType(id: 4, isSelected: MileageRangeOptions.range30to40k.value == seletedMileage ? true: false, mileageRange: MileageRangeOptions.range30to40k.value,placeHolderRange:MileageRangeOptions.range30to40k.palceHolder )
//        let range5 = MileageType(id: 5, isSelected: MileageRangeOptions.range40to50k.value == seletedMileage ? true: false, mileageRange: MileageRangeOptions.range40to50k.value,placeHolderRange:MileageRangeOptions.range40to50k.palceHolder )
//        let range6 = MileageType(id: 6, isSelected: MileageRangeOptions.range50to100k.value == seletedMileage ? true: false, mileageRange: MileageRangeOptions.range50to100k.value,placeHolderRange:MileageRangeOptions.range50to100k.palceHolder )
//        dataSource.append(contentsOf: [range1, range2, range3, range4, range5, range6])
        return dataSource
    }
    
    func getFilterOptionsDatasource(_ id: Int, arrBrandType: [VehicleBrand], arrMileageType: [MileageType],year:String) -> [CellInfo] {
        var array = [CellInfo]()
        
        //Choose Brands Cell
        var noOfRows: Int = 0
        let extraRow = arrBrandType.count % 3
        if extraRow == 0 {
            noOfRows = Int(arrBrandType.count/3)
        } else {
            noOfRows = Int(arrBrandType.count/3) + 1
        }
        let chooseBrandCell = CellInfo(cellType: .ChooseBrandsTableCell, placeHolder: "", value: "", info: nil, height: Constants.CellHeightConstants.height_170 * CGFloat(noOfRows))
        
        let selectYearCell = CellInfo(cellType: .SelectYearTableCell, placeHolder: "", value: year, info: nil, height: Constants.CellHeightConstants.height_110)
        
        var noOfRowsMileage: Int = 0
        let extraRowMileage = arrMileageType.count % 2
        if extraRowMileage == 0 {
            noOfRowsMileage = Int(arrMileageType.count/2)
        } else {
            noOfRowsMileage = Int(arrMileageType.count/2) + 1
        }
        let selectMileageCell = CellInfo(cellType: .SelectMileageTableCell, placeHolder: "", value: "", info: nil, height: Constants.CellHeightConstants.height_116  * CGFloat(noOfRowsMileage))
        
        let selectPriceCell = CellInfo(cellType: .SelectPriceTableCell, placeHolder: "", value: "", info: nil, height: Constants.CellHeightConstants.height_310)
        
        switch id {
        case FilterOptions.brand.rawValue:
            array.append(chooseBrandCell)
        case FilterOptions.year.rawValue:
            array.append(selectYearCell)
        case FilterOptions.mileage.rawValue:
            array.append(selectMileageCell)
        case FilterOptions.price.rawValue:
            array.append(selectPriceCell)
            
        default:
            break
        }
        return array
    }
    
    func getFilterTypeOptionsDatasource() -> [FilterType] {
        var datasource: [FilterType] = []
        let brandType = FilterType(id: FilterOptions.brand.rawValue, type: "Brand".localizedString(), isSelected: true)
        let yearType = FilterType(id: FilterOptions.year.rawValue, type: "Year".localizedString(), isSelected: false)
        let mileageType = FilterType(id: FilterOptions.mileage.rawValue, type: "Mileage".localizedString(), isSelected: false)
        let priceType = FilterType(id: FilterOptions.price.rawValue, type: "Price".localizedString(), isSelected: false)
        datasource.append(contentsOf: [brandType, yearType, mileageType, priceType])
        return datasource
    }
    
    //MARK: - API Methods
    func requestVehicleBrandAPI(completion: @escaping (Bool, [VehicleBrand])-> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .vehicleBrand)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [[String: AnyObject]],
                    let brandDataArray = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let brandData = try decoder.decode([VehicleBrand].self, from: brandDataArray)
                        completion(success, brandData)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
    func requestVehicleCountAPI(brand: [VehicleBrand], year: String, mileage: String, maxMonthlyInstallment: String, maximumPrice: String, completion: @escaping (Bool, String)-> Void) {
        let requestParams = self.getFilterParams(brand: brand, year: year, mileage: mileage, maxMonthlyInstallment: maxMonthlyInstallment, maximumPrice: maximumPrice)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .buyVehicleFilterCount(param: requestParams))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                     {
                    completion(success, Helper.toString(object: Helper.toString(object: result[ConstantAPIKeys.count])))
                }
            }
        }
    }
    
    func getFilterParams(brand: [VehicleBrand], year: String, mileage: String, maxMonthlyInstallment: String, maximumPrice: String) -> APIParams {
        var params: APIParams = APIParams()
        
        if brand.count > 0 {
            var brandIDs = ""
            for vehicleBrand in brand {

                if let brandIsSelected = vehicleBrand.isSelected, brandIsSelected == true {
                    if brandIDs.isEmpty {
                        brandIDs = Helper.toString(object: vehicleBrand.id)
                    } else {
                        brandIDs = brandIDs + "," + Helper.toString(object: vehicleBrand.id)
                    }
                }
            }
            
            if !brandIDs.isEmpty {
                params[ConstantAPIKeys.brand] = brandIDs as AnyObject
            }
        }
        
        if !year.isEmpty {
            params[ConstantAPIKeys.year] = year as AnyObject
        }
        
        if !mileage.isEmpty {
            params[ConstantAPIKeys.mileage] = mileage as AnyObject
        }
        
        if !maxMonthlyInstallment.isEmpty {
            params[ConstantAPIKeys.maxMonthlyInstallment] = maxMonthlyInstallment as AnyObject
        }
        
        if !maximumPrice.isEmpty {
            params[ConstantAPIKeys.maximumPrice] = maximumPrice as AnyObject
        }

        return params
    }
  
}

