//
//  FilterType.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 10/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

struct FilterType {
    var id: Int?
    var type: String?
    var isSelected: Bool?
}
