//
//  VehicleBrand.swift
//  AlbaCars
//
//  Created by Narendra on 1/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class VehicleBrand: Codable {
 
    //Variables
    var id: Int?
    var logo: String?
    var title: String?
    var isSelected: Bool?
}

class VehicleBrandList: BaseCodable {
    var vehicleBrands: [VehicleBrand]?
    
    private enum CodingKeys: String, CodingKey {
      case vehicleBrands = "result"
    }
}
