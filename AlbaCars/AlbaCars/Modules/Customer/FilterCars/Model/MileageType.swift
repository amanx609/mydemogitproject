//
//  MileageType.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 11/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

struct MileageType {
    var id: Int?
    var isSelected: Bool?
    var mileageRange: String?
    var placeHolderRange: String?
}
