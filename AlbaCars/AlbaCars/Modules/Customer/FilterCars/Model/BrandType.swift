//
//  BrandType.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 11/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

struct BrandType {
    var id: Int?
    var isSelected: Bool?
    var brandName: String?
    var brandImage: UIImage?
}
