//
//  CSellCarIntroViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CSellCarIntroViewC: BaseViewC {

    //MARK: - IBOutlets
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
        
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        setupView()
        roundCorners()
        self.setupNavigationBarTitle(title: StringConstants.Text.sellYourCar.localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
    }
    
    private func roundCorners() {
        Threads.performTaskInMainQueue  {
            self.gradientView.roundCorners([.allCorners], radius: Constants.UIConstants.sizeRadius_7)
        }
    }
    
    private func setupView() {
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        self.continueButton.setTitle(StringConstants.Text.continueButton.localizedString(), for: .normal)
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
    }
    
    //MARK: - IBActions
    @IBAction func continueButtonTapped(_ sender: Any) {
        let sellCarVC = DIConfigurator.sharedInstance.getSellCarFormVC()
        self.navigationController?.pushViewController(sellCarVC, animated: true)
    }
    
}
