//
//  BuyCarsViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 10/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

protocol BuyCarsVModeling: BaseVModeling {
    func requestBuyVehicleAPI(param: APIParams, completion: @escaping (Int, Int, VehicleList) -> Void)
    func requestVehicleDetailsAPI(vehicleId: Int, completion: @escaping (CarDetailModel) -> Void)
}

class BuyCarsViewM: BaseViewM, BuyCarsVModeling {
    func requestBuyVehicleAPI(param: APIParams, completion: @escaping (Int, Int, VehicleList) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .buyVehicle(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let resultData = result.toJSONData(),
                    let vehicleList = VehicleList(jsonData: resultData) {
                    completion(nextPageNumber, perPage, vehicleList)
                }
            }
        }
    }
    
    func requestVehicleDetailsAPI(vehicleId: Int, completion: @escaping (CarDetailModel) -> Void) {
        let buyVehicleAPIParam = getVehicleDetailsAPIParams(vehicleId: vehicleId)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .buyVehicleDetails(param: buyVehicleAPIParam))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData(){
                    
                    if let carDetailModel = CarDetailModel.init(jsonData: resultData) {
                        completion(carDetailModel)
                    }
                }
            }
        }
    }
    
    private func getVehicleDetailsAPIParams(vehicleId: Int) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        return param
    }
}
