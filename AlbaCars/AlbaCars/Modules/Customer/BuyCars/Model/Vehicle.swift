//
//  Car.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 10/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class Vehicle: BaseCodable {
    
    var id: Int?
    var carTitle: String?
    var brand: Int?
    var model: Int?
    var type: Int?
    var price: Double?
    var year: Int?
    var mileage: Double?
    var financingYear: Int?
    var monthlyInstallment: Double?
    var status: String?
    var holdDateTime: String?
    var vehicleImages: [String]?
    var brandName: String?
    var modelName: String?
    var offerImage: String?
    var vehicleOption: String?
    
    //KeysForAuctionCar
    var date: String?
    var startTime: String?
    var endTime: String?
    var specs: String?
    var name: String?
    var image: String?
    var typeName: String?
    var currentBid: Double?
    var currentDateTime: String?
    
    //Timer
    var timerDuration: Double?
    var totalTime: Double?
    var userId: Int?
    var rating: Double?

    
    required init(from decoder: Decoder) throws {
        let values = try! decoder.container(keyedBy: CodingKeys.self)
        self.id = try values.decodeIfPresent(Int.self, forKey: .id)
        self.carTitle = try values.decodeIfPresent(String.self, forKey: .carTitle)
        self.brand = try values.decodeIfPresent(Int.self, forKey: .brand)
        self.model = try values.decodeIfPresent(Int.self, forKey: .model)
        self.type = try values.decodeIfPresent(Int.self, forKey: .type)
        self.price = try values.decodeIfPresent(Double.self, forKey: .price)
        self.year = try values.decodeIfPresent(Int.self, forKey: .year)
        self.mileage = try values.decodeIfPresent(Double.self, forKey: .mileage)
        self.financingYear = try values.decodeIfPresent(Int.self, forKey: .financingYear)
        self.monthlyInstallment = try values.decodeIfPresent(Double.self, forKey: .monthlyInstallment)
        self.status = try values.decodeIfPresent(String.self, forKey: .status)
        self.holdDateTime = try values.decodeIfPresent(String.self, forKey: .holdDateTime)
        self.vehicleImages = try values.decodeIfPresent([String].self, forKey: .vehicleImages)
        self.brandName = try values.decodeIfPresent(String.self, forKey: .brandName)
        self.modelName = try values.decodeIfPresent(String.self, forKey: .modelName)
        self.offerImage = try values.decodeIfPresent(String.self, forKey: .offerImage)
        self.vehicleOption = try values.decodeIfPresent(String.self, forKey: .vehicleOption)
        self.date = try values.decodeIfPresent(String.self, forKey: .date)
        self.startTime = try values.decodeIfPresent(String.self, forKey: .startTime)
        self.endTime = try values.decodeIfPresent(String.self, forKey: .endTime)
        self.specs = try values.decodeIfPresent(String.self, forKey: .specs)
        self.name = try values.decodeIfPresent(String.self, forKey: .name)
        self.image = try values.decodeIfPresent(String.self, forKey: .image)
        self.typeName = try values.decodeIfPresent(String.self, forKey: .typeName)
        self.currentBid = try values.decodeIfPresent(Double.self, forKey: .currentBid)
        self.currentDateTime = try values.decodeIfPresent(String.self, forKey: .currentDateTime)
        self.userId = try values.decodeIfPresent(Int.self, forKey: .userId)
        self.rating = try values.decodeIfPresent(Double.self, forKey: .rating)
        
        if let strStartTimeDate = self.startTime,
            let strEndDate = self.endTime,
            let currentDate = strStartTimeDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss"),
            let endDate = strEndDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss") {
            self.totalTime = endDate.timeIntervalSince(currentDate)
        }
        
        if let strCurrentDate = self.currentDateTime,
            let strEndDate = self.endTime,
            let currentDate = strCurrentDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss"),
            let endDate = strEndDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss") {
            self.timerDuration = endDate.timeIntervalSince(currentDate)
        }
    }
    
    func getProgress()->Float {
        
        var toltalTimeMin = Helper.toInt(totalTime)
        toltalTimeMin = toltalTimeMin/60
        var timerDurationMin = Helper.toInt(timerDuration)
        timerDurationMin = timerDurationMin / 60
        let val = toltalTimeMin * timerDurationMin
        let remainingTime =  val / 100
        return Float(toltalTimeMin - remainingTime)/100
    }
    
}

class VehicleList: BaseCodable {
    var vehicles: [Vehicle]?
    
    private enum CodingKeys: String, CodingKey {
        case vehicles = "data"
    }
}
