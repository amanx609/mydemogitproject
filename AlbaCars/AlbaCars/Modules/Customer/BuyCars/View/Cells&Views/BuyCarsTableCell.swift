//
//  BuyCarsTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 10/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class BuyCarsTableCell: BaseTableViewCell, ReusableView, NibLoadableView  {

    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var carInfoView: CarInfoView!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        self.carInfoView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
    }
    
    //MARK: - Public Methods
    func configureView(vehicle: Vehicle, setupType: BuyCarCellType) {
       self.carInfoView.setupViewForBuyCars(vehicle: vehicle)
//        switch setupType {
//        case .buyCars:
//            self.carInfoView.setupViewForBuyCars(vehicle: vehicle)
//        case .bankFinance:
//            self.carInfoView.setupViewForBankFinance(vehicle: vehicle)
//        }
        
    }
    
    func configureViewForBankFinance(cellData: CarFinanceStatusModel) {
        self.carInfoView.setupViewForBankFinance(cellData: cellData)
    }
    
    func configureViewForWatchList(cellData: MyWatchListModel) {
        self.carInfoView.setupViewForMyWatchList(watchListData: cellData)
    }
}
