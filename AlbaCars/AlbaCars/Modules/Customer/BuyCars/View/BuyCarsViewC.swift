//
//  BuyCarsViewC.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 10/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class BuyCarsViewC: BaseViewC {
    
    //MARK:- IBOutlet
    @IBOutlet weak var carListTableView: UITableView!
    
    //MARK: - Variables
    var viewModel: BuyCarsVModeling?
    var vehicles: [Vehicle] = []
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    var params: APIParams = APIParams()
    
    //MARK:- LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Buy Cars".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [.filter])
    }
    
    override func navigationButtonTapped(_ sender: AnyObject) {
        guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
        switch buttonType {
        case .back: self.navigationController?.popViewController(animated: true)
        case .filter: self.presentFilterViewC()
        default: break
        }
    }
    
    //MARK:- Private Methods
    private func setUp() {
        self.recheckVM()
        self.setUpTableView()
        self.requestBuyVehicleAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = BuyCarsViewM()
        }
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.carListTableView.delegate = self
        self.carListTableView.dataSource = self
        self.carListTableView.separatorStyle = .none
    }
    
    private func registerNibs() {
        self.carListTableView.register(BuyCarsTableCell.self)
    }
    
    private func presentFilterViewC() {
        let filterVC = DIConfigurator.sharedInstance.getFilterCarsVC()
        filterVC.selectedParams = self.params
        filterVC.selectParams = { [weak self] (params) in
        guard let sSelf = self else {
            return
         }
            sSelf.nextPageNumber = 1
            if let filterParam = params {
                sSelf.params = filterParam
            }
            sSelf.vehicles.removeAll()
            sSelf.requestBuyVehicleAPI()
        }
        let navC = UINavigationController(rootViewController: filterVC)
        self.present(navC, animated: true, completion: nil)
    }
    
    //MARK: - APIMethods
     func requestBuyVehicleAPI() {
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        
        self.viewModel?.requestBuyVehicleAPI(param: params, completion: { [weak self] (nextPage, perPage, vehicleList) in
            guard let sSelf = self,
                  let vehicles = vehicleList.vehicles else { return }
            sSelf.nextPageNumber = nextPage
            sSelf.perPage = perPage
            sSelf.vehicles = sSelf.vehicles + vehicles
            Threads.performTaskInMainQueue {
                sSelf.carListTableView.reloadData()
            }
        })
    }
    
    //MARK: - APIMethods
     func requestVehicleDetailsAPI(vehicleId: Int) {
        
        self.viewModel?.requestVehicleDetailsAPI(vehicleId: vehicleId, completion: { [weak self] (vehicleDetails) in
            guard let sSelf = self else { return }
            Threads.performTaskInMainQueue {
                let carDetailsVC = DIConfigurator.sharedInstance.getCCarDetailsVC()
                carDetailsVC.carDetailModel = vehicleDetails
                carDetailsVC.vehicleId = vehicleId
                sSelf.navigationController?.pushViewController(carDetailsVC, animated: true)
            }
        })
    }

}

