//
//  BuyCarsViewC+TableView.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 10/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension BuyCarsViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.vehicles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getCell(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.CellHeightConstants.height_175
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      guard let lastVisibleRow = self.carListTableView.indexPathsForVisibleRows?.last?.row else { return }
        if (self.vehicles.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
           self.requestBuyVehicleAPI()
      }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let carDetailsVC = DIConfigurator.sharedInstance.getCCarDetailsVC()
        //self.navigationController?.pushViewController(carDetailsVC, animated: true)
        self.requestVehicleDetailsAPI(vehicleId: Helper.toInt(self.vehicles[indexPath.row].id))
    }
}
extension BuyCarsViewC {
    func getCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: BuyCarsTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let vehicle = self.vehicles[indexPath.row]
        cell.configureView(vehicle: vehicle, setupType: .buyCars)
        return cell
    }
}
