//
//  CSellCarListViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol CSellCarListViewModeling {
    func requestGetSellCarAPI(page: Int, completion: @escaping ([CSellCarListModel], Int, Int, Bool)-> Void)
    func requestGetSellCarDetailsAPI(param: APIParams, completion: @escaping (CSellCarListModel,Bool)-> Void)
}

class CSellCarListViewM: CSellCarListViewModeling {
   
    func requestGetSellCarAPI(page: Int, completion: @escaping ([CSellCarListModel], Int, Int, Bool)-> Void) {
        
        let sellCarListParam = getAPIParams(page: page)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getSellVehicle(param: sellCarListParam))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject], let result = safeResponse[kResult] as? [String: AnyObject], let data = result[kData] as? [[String: AnyObject]], let nextPageNumber = result[kNextPageNum] as? Int, let perPage = result[kPerPage] as? Int, let listArray = data.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let carList = try decoder.decode([CSellCarListModel].self, from: listArray)
                        completion(carList, nextPageNumber, perPage, success)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
        
    }
    
    func requestGetSellCarDetailsAPI(param: APIParams, completion: @escaping (CSellCarListModel,Bool)-> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getSellVehicleDetails(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let carList = try decoder.decode(CSellCarListModel.self, from: resultData)
                        completion(carList, success)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
        
    }
    
    private func getAPIParams(page: Int) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.page] = page as AnyObject
        return param
    }
    
    
}



