//
//  CSellCarListViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CSellCarListViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var carListTableView: UITableView!
    @IBOutlet weak var sellCarLabel: UILabel!
    
    //MARK: - Variables
    var carListDataSource = [CSellCarListModel]()
    var viewModel: CSellCarListViewModeling?
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    var sellCarDetails:Bool = false
    var vehicleId = 0
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    deinit {
        print("deinit CSellCarListViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupTableView()
        self.setupNavigationBarTitle(title: StringConstants.Text.sellaCar.localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.sellCarLabel.isHidden = true
        
        if self.sellCarDetails {
            self.requestGetSellCarDetailsAPI()
        } else {
            self.callgetSellCarAPI()
        }
        
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = CSellCarListViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.carListTableView.backgroundColor = UIColor.white
        self.carListTableView.delegate = self
        self.carListTableView.dataSource = self
        self.carListTableView.separatorStyle = .none
        //self.carListTableView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.carListTableView.register(SellCarListCell.self)
    }
    
    //MARK: - Public Methods
    func callgetSellCarAPI() {
        self.viewModel?.requestGetSellCarAPI(page: self.nextPageNumber, completion: { (response, nextPageNum, perPage, success)  in
            if success {
                Threads.performTaskInMainQueue {
                    self.nextPageNumber = nextPageNum
                    self.perPage = perPage
                    self.carListDataSource = self.carListDataSource + response
                    
                    if self.carListDataSource.count != 0 {
                        self.sellCarLabel.isHidden = false
                    }
                    self.carListTableView.reloadData()
                }
            }
        })
    }
    
    func requestGetSellCarDetailsAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.id] = Helper.toString(object: vehicleId) as AnyObject
        
        self.viewModel?.requestGetSellCarDetailsAPI(param: params, completion: { [weak self] (response,success) in
            
            guard let sSelf = self else { return }
            
            if success {
                Threads.performTaskInMainQueue {
                    sSelf.carListDataSource.append(response)
                    sSelf.sellCarLabel.isHidden = false
                    sSelf.carListTableView.reloadData()
                }
            }
        })
    }
    
    
}
