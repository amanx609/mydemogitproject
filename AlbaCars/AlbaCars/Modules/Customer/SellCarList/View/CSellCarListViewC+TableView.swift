//
//  CSellCarListViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension CSellCarListViewC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.carListDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // let document = self.documentListDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let lastVisibleRow = self.carListTableView.indexPathsForVisibleRows?.last?.row else { return }
        if (self.carListDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
            self.callgetSellCarAPI()
        }
    }
    
}

extension CSellCarListViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: SellCarListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(cellData: self.carListDataSource[indexPath.row])
        return cell
    }
}
