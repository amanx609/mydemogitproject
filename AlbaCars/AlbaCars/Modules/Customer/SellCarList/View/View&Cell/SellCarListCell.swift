//
//  SellCarListCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class SellCarListCell: BaseTableViewCell, ReusableView, NibLoadableView  {
    
    //MARK: - IBOutlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var dashedLineFirstView: UIView!
    @IBOutlet weak var dashedLineSecondView: UIView!
    @IBOutlet weak var dashedLineThirdView: UIView!
    
    @IBOutlet weak var status2Button: UIButton!
    @IBOutlet weak var status3Button: UIButton!
    @IBOutlet weak var status1Button: UIButton!
    @IBOutlet weak var status0Button: UIButton!
    @IBOutlet weak var expectedPriceLabel: UILabel!
    @IBOutlet weak var engineSizeLabel: UILabel!
    @IBOutlet weak var odometerLabel: UILabel!
    @IBOutlet weak var carYearLabel: UILabel!
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var combinedCarNameLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.shadowView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        
        Threads.performTaskInMainQueue {
            self.dashedLineFirstView.addDashedLine(color: .greenColor)
            self.dashedLineSecondView.addDashedLine()
            self.dashedLineThirdView.addDashedLine()
        }
    }
    
    //MARK: - Public Methods
    func configureView(cellData: CSellCarListModel) {
        if let images = cellData.images {
            self.carImageView.setImage(urlStr: images[0] , placeHolderImage: #imageLiteral(resourceName: "placeholder"))
            
        }
        switch Helper.toInt(cellData.status) {
        case 0:
            status0Button.isSelected = true
            status1Button.isSelected = false
            status2Button.isSelected = false
            status3Button.isSelected = false
            Threads.performTaskInMainQueue {
                self.dashedLineFirstView.addDashedLine()
                self.dashedLineSecondView.addDashedLine()
                self.dashedLineThirdView.addDashedLine()
            }
        case 1:
            status0Button.isSelected = true
            status1Button.isSelected = true
            status2Button.isSelected = false
            status3Button.isSelected = false
            Threads.performTaskInMainQueue {
                self.dashedLineFirstView.addDashedLine(color: .greenColor)
                self.dashedLineSecondView.addDashedLine()
                self.dashedLineThirdView.addDashedLine()
            }
        case 2:
            status0Button.isSelected = true
            status1Button.isSelected = true
            status2Button.isSelected = true
            status3Button.isSelected = false
            Threads.performTaskInMainQueue {
                self.dashedLineFirstView.addDashedLine(color: .greenColor)
                self.dashedLineSecondView.addDashedLine(color: .greenColor)
                self.dashedLineThirdView.addDashedLine()
            }
        case 3:
            status0Button.isSelected = true
            status1Button.isSelected = true
            status2Button.isSelected = true
            status3Button.isSelected = true
            Threads.performTaskInMainQueue {
                self.dashedLineFirstView.addDashedLine(color: .greenColor)
                self.dashedLineSecondView.addDashedLine(color: .greenColor)
                self.dashedLineThirdView.addDashedLine(color: .greenColor)
            }
        default:
            break
        }
        self.brandNameLabel.text = Helper.toString(object: cellData.brandName)
        self.modelLabel.text = Helper.toString(object: cellData.modelName)
        self.combinedCarNameLabel.text = cellData.getCombinedCarName()
        self.carYearLabel.text = "\(Helper.toString(object: cellData.year))"
        self.odometerLabel.text = "\(Helper.toString(object: cellData.mileage))"
        self.engineSizeLabel.text = "\(Helper.toString(object: cellData.engine))"
        self.expectedPriceLabel.text = cellData.expectedPrice()
    }
}
