//
//  SellCarListModel.swift
//  AlbaCars
//
//  Created by Narendra on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

class CSellCarListModel: Codable {
    
    var engine: Double?
    var id: Int?
    var makeId: Int?
    var mileage: Int?
    var model: Int?
    var price: Int?
    var status: Int?
    var brandName: String?
    var modelName: String?
    var year: Int?
    var images: [String]?
    
    func getCombinedCarName() -> String {
        if let brandName = brandName, let modelName = modelName {
            return brandName + " " + modelName
        }
        return ""
    }
    
    func expectedPrice() -> String {
        return  "AED" + " " + Helper.toString(object: price)
    }
    
}

