//
//  DocumentDetailsViewM.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 12/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DocumentDetailsVModeling: BaseVModeling {
    func getSectionDatasource() -> [String]
    func getCashBuyersOptionsDatasource() -> [CellInfo]
    func getFinanceBuyersOptionsDatasource(_ arrEmployed: [String], arrSelfEmployed: [String], arrCompanies: [String]) -> [CellInfo]
    func getEmployedOptionsDatasource() -> [String]
    func getSelfEmployedOptionsDatasource() -> [String]
    func getCompaniesOptionsDatasource() -> [String]
}

class DocumentDetailsViewM: BaseViewM,DocumentDetailsVModeling {
    func getEmployedOptionsDatasource() -> [String] {
        var datasource : [String] = []
        datasource.append(contentsOf: ["1- Salary Certificate".localizedString(), "2- 3 month bank statement (stamped)".localizedString(), "3- Passport & Visa copies".localizedString(), "4- Emirates ID copy".localizedString(), "( Note: Please contact us if you have received only one or no salaries and work for a listed company)".localizedString()])
        return datasource
    }
    
    func getSelfEmployedOptionsDatasource() -> [String] {
        var datasource : [String] = []
        datasource.append(contentsOf: ["1- Trade License".localizedString(), "2- MOA.".localizedString(), "3- Passport copies of all partners".localizedString(), "4- Passport and visa copies of applicant".localizedString(), "5- Emirates ID".localizedString(), "6- 3 month personal bank statement".localizedString(), "7- 3 month company bank statement".localizedString()])
        return datasource
    }
    
    func getCompaniesOptionsDatasource() -> [String] {
        var datasource : [String] = []
        datasource.append(contentsOf: ["1- Trade License".localizedString(), "2- MOA.".localizedString(), "3- Passport copies of all partners".localizedString(), "4- 3 month company statement".localizedString()])
        return datasource
    }
    
    func getSectionDatasource() -> [String] {
        var datasource = [String]()
        datasource.append(contentsOf: ["Cash Buyers".localizedString(), "Finance Buyers".localizedString()])
        return datasource
    }
    
    func getCashBuyersOptionsDatasource() -> [CellInfo] {
        var datasource: [CellInfo] = []
        let cashBuyersCell = CellInfo(cellType: .DocumentDetailsHeaderCell, placeHolder: "", value: "Cash Buyers".localizedString(), info: nil, height: Constants.CellHeightConstants.height_45)
        let provideCell = CellInfo(cellType: .DocumentDetailsTableCell, placeHolder: "", value: "Please provide:".localizedString(), info: nil, height: Constants.CellHeightConstants.height_30)
        let emiratesCell = CellInfo(cellType: .DocumentDetailsTableCell, placeHolder: "", value: "1- Emirates ID".localizedString(), info: nil, height: Constants.CellHeightConstants.height_30)
        let drivingLicenseCell = CellInfo(cellType: .DocumentDetailsTableCell, placeHolder: "", value: "2- Driving License".localizedString(), info: nil, height: Constants.CellHeightConstants.height_30)
        datasource.append(contentsOf: [cashBuyersCell, provideCell, emiratesCell, drivingLicenseCell])
        return datasource
    }
    
    func getFinanceBuyersOptionsDatasource(_ arrEmployed: [String], arrSelfEmployed: [String], arrCompanies: [String]) -> [CellInfo] {
        var datasource: [CellInfo] = []
        let financeBuyersCell = CellInfo(cellType: .DocumentDetailsHeaderCell, placeHolder: "", value: "Finance Buyers".localizedString(), info: nil, height: Constants.CellHeightConstants.height_45)
        let requiredCell = CellInfo(cellType: .DocumentDetailsTableCell, placeHolder: "", value: "Required Bank finance documents are as follows:".localizedString(), info: nil, height: Constants.CellHeightConstants.height_30)
        let employedCell = CellInfo(cellType: .FinanceSectionCell, placeHolder: "", value: "EMPLOYED:".localizedString(), info: nil, height: Constants.CellHeightConstants.height_35 + Constants.CellHeightConstants.height_30 * CGFloat(arrEmployed.count))
        let selfEmployedCell = CellInfo(cellType: .FinanceSectionCell, placeHolder: "", value: "SELF EMPLOYED:".localizedString(), info: nil, height: Constants.CellHeightConstants.height_35 + Constants.CellHeightConstants.height_30 * CGFloat(arrSelfEmployed.count))
        let companiesCell = CellInfo(cellType: .FinanceSectionCell, placeHolder: "", value: "COMPANIES:".localizedString(), info: nil, height: Constants.CellHeightConstants.height_35 + Constants.CellHeightConstants.height_30 * CGFloat(arrCompanies.count))
        datasource.append(contentsOf: [financeBuyersCell, requiredCell, employedCell, selfEmployedCell, companiesCell])
        return datasource
    }
}
