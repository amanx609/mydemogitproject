//
//  CDocumentDetailsViewC.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 12/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CDocumentDetailsViewC: BaseViewC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var documentDetailsTableView: UITableView!
    
    //MARK:- Variables
    var viewModel: DocumentDetailsVModeling?
    var sectionDataSource = [String]()
    var cashBuyerOptionsDataSource = [CellInfo]()
    var financeBuyerOptionsDataSource = [CellInfo]()
    var employedOptionsDataSource = [String]()
    var selfEmployedOptionsDataSource = [String]()
    var companiesOptionsDataSource = [String]()

    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()
    }
    
    //MARK:- Private Methods
    private func setUp() {
        self.recheckVM()
        self.getDatasourceForDocumentDetails()
        self.setupNavigationBarTitle(title: "Documents Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.setUpTableView()
    }
    
    private func recheckVM() {
      if self.viewModel == nil {
        self.viewModel = DocumentDetailsViewM()
      }
    }
    
    private func getDatasourceForDocumentDetails() {
        if let dataSource = self.viewModel?.getSectionDatasource() {
            self.sectionDataSource = dataSource
        }
        if let dataSource = self.viewModel?.getCashBuyersOptionsDatasource() {
            self.cashBuyerOptionsDataSource = dataSource
        }
        if let dataSource = self.viewModel?.getEmployedOptionsDatasource() {
            self.employedOptionsDataSource = dataSource
        }
        if let dataSource = self.viewModel?.getSelfEmployedOptionsDatasource() {
            self.selfEmployedOptionsDataSource = dataSource
        }
        if let dataSource = self.viewModel?.getCompaniesOptionsDatasource() {
            self.companiesOptionsDataSource = dataSource
        }
        if let dataSource = self.viewModel?.getFinanceBuyersOptionsDatasource(self.employedOptionsDataSource, arrSelfEmployed: self.selfEmployedOptionsDataSource, arrCompanies: self.companiesOptionsDataSource) {
            self.financeBuyerOptionsDataSource = dataSource
        }
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.documentDetailsTableView.delegate = self
        self.documentDetailsTableView.dataSource = self
        self.documentDetailsTableView.separatorStyle = .none
        self.documentDetailsTableView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.documentDetailsTableView.registerMultiple(nibs: [DocumentDetailsTableCell.className, DocumentDetailsHeaderCell.className, FinanceSectionCell.className])
    }

}
