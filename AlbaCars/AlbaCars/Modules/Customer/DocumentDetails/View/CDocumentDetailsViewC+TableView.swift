//
//  CDocumentDetailsViewC+TableView.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 12/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension CDocumentDetailsViewC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionDataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? cashBuyerOptionsDataSource.count : financeBuyerOptionsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cellInfo = self.cashBuyerOptionsDataSource[indexPath.row]
            return self.getCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
        }
        let cellInfo = self.financeBuyerOptionsDataSource[indexPath.row]
        return self.getCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? self.cashBuyerOptionsDataSource[indexPath.row].height : self.financeBuyerOptionsDataSource[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
}

extension CDocumentDetailsViewC {
    func getCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
      guard let cellType = cellInfo.cellType else { return UITableViewCell() }
      switch cellType {
      case .DocumentDetailsHeaderCell:
        return getDocumentDetailsHeaderCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
      case .DocumentDetailsTableCell:
        return getDocumentDetailsCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
      case .FinanceSectionCell:
        return getFinanceSectionCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
      default:
        return UITableViewCell()
      }
    }
    
    func getDocumentDetailsHeaderCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: DocumentDetailsHeaderCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.data = cellInfo
        return cell
    }
    
    func getDocumentDetailsCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: DocumentDetailsTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.data = cellInfo
        return cell
    }
    
    func getFinanceSectionCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: FinanceSectionCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.data = cellInfo
        switch cellInfo.value {
        case FinanceOptions.employed.description :
            cell.arrayData = self.employedOptionsDataSource
        case FinanceOptions.selfEmployed.description :
            cell.arrayData = self.selfEmployedOptionsDataSource
        case FinanceOptions.companies.description :
            cell.arrayData = self.companiesOptionsDataSource
        default: break
        }
        return cell
    }
}
