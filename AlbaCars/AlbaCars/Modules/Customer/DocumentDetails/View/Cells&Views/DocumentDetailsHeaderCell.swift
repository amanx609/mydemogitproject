//
//  DocumentDetailsHeaderCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 12/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DocumentDetailsHeaderCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK:- Variables
    var data: CellInfo? {
        didSet {
            if let data = self.data {
                configureCell(data)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK:- Private Methods
    private func configureCell(_ data: CellInfo) {
        self.titleLabel.text = data.value
    }
    
}
