//
//  DocumentDetailsTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 12/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DocumentDetailsTableCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK:- IBOutlets
    @IBOutlet weak var detailLabel: UILabel!
    
    //MARK:- Variables
    var data: CellInfo? {
        didSet {
            if let data = self.data {
                configureCell(data)
            }
        }
    }
    
    var strData: String? {
        didSet {
            if let data = self.strData {
                configureCell(data)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func configureCell(_ data: CellInfo) {
        self.detailLabel.text = data.value
    }
    
    private func configureCell(_ data: String) {
        self.detailLabel.text = data
    }
}
