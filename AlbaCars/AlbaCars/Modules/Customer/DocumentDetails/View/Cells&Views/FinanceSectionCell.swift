//
//  FinanceSectionCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 12/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class FinanceSectionCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var financeOptionsTableView: UITableView!
    
    //MARK:- Variables
    var data: CellInfo? {
        didSet {
            if let data = self.data {
                configureCell(data)
            }
        }
    }
    var arrayData: [String]? {
        didSet {
            if let data = self.arrayData {
                configureTableView(data)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func configureCell(_ data: CellInfo) {
        self.titleLabel.text = data.value
    }
    
    private func configureTableView(_ data: [String]) {
        self.registerNibs()
        self.financeOptionsTableView.delegate = self
        self.financeOptionsTableView.dataSource = self
        self.financeOptionsTableView.allowsSelection = false
        self.financeOptionsTableView.separatorStyle = .none
        self.financeOptionsTableView.reloadData()
    }
    
    private func registerNibs() {
        self.financeOptionsTableView.registerMultiple(nibs: [DocumentDetailsTableCell.className])
    }
    
    func getDocumentDetailsCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: DocumentDetailsTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        if let arrData = arrayData {
            cell.strData = arrData[indexPath.row]
        }
        return cell
    }
}

//MARK:- UITableView Delegates & Datasource Methods
extension FinanceSectionCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let arrData = arrayData {
            return arrData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getDocumentDetailsCell(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if data?.value == FinanceOptions.employed.description {
            return UITableView.automaticDimension
        }
        return Constants.CellHeightConstants.height_30
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.CellHeightConstants.height_30
    }
}
