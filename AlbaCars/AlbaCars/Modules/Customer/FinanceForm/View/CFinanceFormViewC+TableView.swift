//
//  CBankApplicationFormViewC+TableView.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 23/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension CFinanceFormViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.financeFormDatasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.financeFormDatasource[indexPath.row]
        return getCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.financeFormDatasource[indexPath.row].height
    }
}

extension CFinanceFormViewC {
    func getCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
      guard let cellType = cellInfo.cellType else { return UITableViewCell() }
      switch cellType {
      case .TwoRadioButtonCell:
        return getTwoRadioButtonCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
      case .DropDownQuestionCell:
        return getDropDownQuestionCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
      case .ThreeRadioButtonCell:
        return getThreeRadioButtonCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
      default:
        return UITableViewCell()
      }
    }
    
    func getTwoRadioButtonCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: TwoRadioButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.data = cellInfo
        cell.delegate = self
        cell.optionSelectionDidTapped = {[weak self] (type) in
            guard let strongSelf = self else {
                return
            }
            if type == TwoOptionsSelection.yes {
                strongSelf.financeFormDatasource[indexPath.row].info?[Constants.UIKeys.optionOneSelected] = true as AnyObject
                strongSelf.financeFormDatasource[indexPath.row].info?[Constants.UIKeys.optionTwoSelected] = false as AnyObject
                strongSelf.financeFormDatasource[indexPath.row].value = "1"
                if indexPath.row == 2 || indexPath.row == 4 {
                    strongSelf.financeFormDatasource[indexPath.row].info?[Constants.UIKeys.shouldShowAnswer] = false as AnyObject
                }
            } else {
               strongSelf.financeFormDatasource[indexPath.row].info?[Constants.UIKeys.optionOneSelected] = false as AnyObject
               strongSelf.financeFormDatasource[indexPath.row].info?[Constants.UIKeys.optionTwoSelected] = true as AnyObject
                strongSelf.financeFormDatasource[indexPath.row].value = "0"
            }
            strongSelf.applicationFormTableView.reloadRows(at: [indexPath], with: .none)

//            if let dataSource = strongSelf.viewModel?.getBankApplicationFormDatasource(strongSelf.financeFormDatasource) {
//                strongSelf.financeFormDatasource = dataSource
//            }
        }
        return cell
    }
    
    func getDropDownQuestionCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: DropDownQuestionCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.data = cellInfo
        return cell
    }
    
    func getThreeRadioButtonCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: ThreeRadioButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(cellInfo: cellInfo)
        cell.delegate = self
        return cell
    }
}

extension CFinanceFormViewC: DropDownQuestionCellDelegate {
    func didTapTextField(cell: DropDownQuestionCell) {
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        guard let indexPath = self.applicationFormTableView.indexPath(for: cell) else { return }
        listPopupView.initializeViewWith(title: DropDownType.numberOfEmployees.title, arrayList: DropDownType.numberOfEmployees.dataList, key: DropDownType.numberOfEmployees.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let employees = response[DropDownType.numberOfEmployees.rawValue] as? String else { return }
            
            sSelf.financeFormDatasource[indexPath.row].value = employees
            sSelf.applicationFormTableView.reloadData()
        }
        listPopupView.showWithAnimated(animated: true)
    }
}

extension CFinanceFormViewC: ThreeRadioButtonCellDelegate {
    func didSelectAnOption(cell: ThreeRadioButtonCell, value: String) {
        guard let indexPath = self.applicationFormTableView.indexPath(for: cell) else { return }
        self.financeFormDatasource[indexPath.row].value = value
    }
}

extension CFinanceFormViewC: TwoRadioButtonCellDelegate {
    func tapNextKeyboard(cell: TwoRadioButtonCell) {
        
    }
    
    func didChangeText(cell: TwoRadioButtonCell, text: String) {
        guard let indexPath = self.applicationFormTableView.indexPath(for: cell) else { return }
        self.financeFormDatasource[indexPath.row].info?[Constants.UIKeys.answerValue] = text as AnyObject
    }
    
}
