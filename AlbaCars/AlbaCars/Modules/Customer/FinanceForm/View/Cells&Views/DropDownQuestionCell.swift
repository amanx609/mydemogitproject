//
//  DropDownQuestionCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 23/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol DropDownQuestionCellDelegate: class {
    func didTapTextField(cell: DropDownQuestionCell)
    
}

class DropDownQuestionCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var answerTextField: UITextField!
    
    //MARK:- Variables
    weak var delegate: DropDownQuestionCellDelegate?
    
    var data: CellInfo? {
        didSet {
            if let data = self.data {
                configureCell(data)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.textFieldView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
    private func configureCell(_ data: CellInfo) {
        self.questionLabel.text = data.placeHolder
        self.answerTextField.text = data.value
    }
    
    //MARK: - IBAction
    @IBAction func tapTextFieldViewButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapTextField(cell: self)
        }
    }
}
