//
//  ThreeRadioButtonCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 23/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol ThreeRadioButtonCellDelegate: class {
    func didSelectAnOption(cell: ThreeRadioButtonCell, value: String)
}

class ThreeRadioButtonCell: BaseTableViewCell, NibLoadableView, ReusableView {

    //MARK:- IBOutlets
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var firstOptionButton: UIButton!
    @IBOutlet weak var secondOptionButton: UIButton!
    @IBOutlet weak var thirdOptionButton: UIButton!
    
    //MARK: - Variables
    weak var delegate: ThreeRadioButtonCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.firstOptionButton.setTitle("WPS".localizedString(), for: .normal)
        self.secondOptionButton.setTitle("Cheque".localizedString(), for: .normal)
        self.thirdOptionButton.setTitle("Cash".localizedString(), for: .normal)
    }
    
    private func wpsSelected() {
        firstOptionButton.isSelected = true
        secondOptionButton.isSelected = false
        thirdOptionButton.isSelected = false
        if let delegate = self.delegate {
            delegate.didSelectAnOption(cell: self, value: "WPS".localizedString())
        }
    }
    
    private func cashSelected() {
        firstOptionButton.isSelected = false
                    secondOptionButton.isSelected = false
                    thirdOptionButton.isSelected = true
                    if let delegate = self.delegate {
                       delegate.didSelectAnOption(cell: self, value: "Cash".localizedString())
                   }
    }
    
    private func chequeSelected() {
        firstOptionButton.isSelected = false
                          secondOptionButton.isSelected = true
                          thirdOptionButton.isSelected = false
                          if let delegate = self.delegate {
                              delegate.didSelectAnOption(cell: self, value: "Cheque".localizedString())
                          }
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        
        guard let paymentMenthod = cellInfo.value else { return }
        switch paymentMenthod {
        case "WPS":
            self.wpsSelected()
        case "Cheque":
            self.chequeSelected()
        case "Cash":
            self.cashSelected()
            
        default:
            break
        }
        
    }
    
    //MARK:- IBAction Methods
    
    @IBAction func firstOptionButtonTapped(_ sender: UIButton) {
        self.wpsSelected()
    }
    
    @IBAction func secondOptionButtonTapped(_ sender: UIButton) {
        self.chequeSelected()
    }
    
    @IBAction func thirdOptionButtonTapped(_ sender: UIButton) {
        self.cashSelected()
    }    
}
