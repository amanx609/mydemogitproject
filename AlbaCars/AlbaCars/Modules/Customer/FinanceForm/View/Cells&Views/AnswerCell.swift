//
//  AnswerCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 24/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class AnswerCell: BaseTableViewCell, NibLoadableView, ReusableView {

    //MARK:- IBOutlets
    @IBOutlet weak var answerView: UIView!
    @IBOutlet weak var answerTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.answerView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
}
