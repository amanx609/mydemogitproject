//
//  TwoRadioButtonCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 23/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol TwoRadioButtonCellDelegate: class {
  func tapNextKeyboard(cell: TwoRadioButtonCell)
  func didChangeText(cell: TwoRadioButtonCell, text: String)
}

class TwoRadioButtonCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var buttonYes: UIButton!
    @IBOutlet weak var buttonNo: UIButton!
    @IBOutlet weak var answerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var answerView: UIView!
    @IBOutlet weak var bottomYesButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var answerTextField: UITextField!
    
    //MARK:- Variables
    weak var delegate: TwoRadioButtonCellDelegate?
    var optionSelectionDidTapped:((_ type: TwoOptionsSelection)-> Void)?
    var inputType: TextInputType?
    var data: CellInfo? {
        didSet {
            if let data = self.data {
                configureCell(data)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.answerTextField.delegate = self
        self.buttonYes.setTitle("Yes".localizedString(), for: .normal)
        self.buttonNo.setTitle("No".localizedString(), for: .normal)
        self.answerTextField.keyboardType = .numberPad
        self.answerView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
    private func configureCell(_ data: CellInfo) {
        self.questionLabel.text = data.placeHolder
        
        self.answerTextField.attributedPlaceholder = NSAttributedString(string: data.info?[Constants.UIKeys.answerPlaceholder] as? String ?? "",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.blackColor])
        
        if let info = data.info {
            if info[Constants.UIKeys.optionOneSelected] as? Bool == true {
                if info[Constants.UIKeys.shouldShowAnswer] as? Bool == true {
                    self.answerViewHeightConstraint.constant = 60
                    self.bottomYesButtonConstraint.constant = 98
                } else {
                    self.answerViewHeightConstraint.constant = 0
                    self.bottomYesButtonConstraint.constant = 20
                }
                self.buttonYes.isSelected = true
                self.buttonNo.isSelected = false
            } else if info[Constants.UIKeys.optionTwoSelected] as? Bool == true {
                self.answerViewHeightConstraint.constant = 0
                self.bottomYesButtonConstraint.constant = 20
                self.buttonYes.isSelected = false
                self.buttonNo.isSelected = true
            } else {
                self.answerViewHeightConstraint.constant = 0
                self.bottomYesButtonConstraint.constant = 20
                self.buttonYes.isSelected = false
                self.buttonNo.isSelected = false
            }
        }
        self.answerTextField.text = Helper.toString(object: data.info?[Constants.UIKeys.answerValue])
    }
    
    //MARK:- IBAction Methods
    
    @IBAction func yesButtonTapped(_ sender: UIButton) {
        optionSelectionDidTapped?(TwoOptionsSelection.yes)
    }
    
    @IBAction func noButtonTapped(_ sender: UIButton) {
        optionSelectionDidTapped?(TwoOptionsSelection.no)
    }
}

//MARK:- UITextField Delegates
extension TwoRadioButtonCell: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if let delegate = self.delegate {
      delegate.tapNextKeyboard(cell: self)
    }
    return true
  }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let delegate = delegate, let text = textField.text {
            delegate.didChangeText(cell: self, text: text)
        }
        
    }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if let text = textField.text, let textRange = Range(range, in: text), let delegate = self.delegate {
      let finalText = text.replacingCharacters(in: textRange, with: string)
        if finalText.count > Constants.Validations.lenth6 {
          return false
        }
//      switch inputType {
//      case .name:
//
//      default: break
//      }
      delegate.didChangeText(cell: self, text: finalText)
    }
    return true
  }
}
