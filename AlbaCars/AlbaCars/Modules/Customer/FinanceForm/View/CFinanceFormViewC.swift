//
//  BankApplicationFormViewC.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 23/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CFinanceFormViewC: BaseViewC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var applicationFormTableView: UITableView!
    
    //MARK:- Variables
    var viewModel: CFinanceFormVModeling?
    var financeFormDatasource = [CellInfo]()
    var vehicleId: Int?
    var type: BuyNowOptions?
    var shouldCallPostAPI = false
    var isSentFromHome = false
    var carDetailModel = CarDetailModel()
    var extrasInfoDataSource = [CellInfo]()
    
    //MARK:- LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.setupNavigationBarTitle(title: "Finance".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.setUpTableView()
        self.recheckVM()
        self.gradientView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.submitButton.roundCorners(Constants.UIConstants.sizeRadius_7)
        Threads.performTaskInMainQueue {
          self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.requestGetcarFinanceForm()
//        if let dataSource = self.viewModel?.getBankApplicationFormDatasource(self.financeFormDatasource,[:]) {
//            self.financeFormDatasource = dataSource
//        }
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = CFinanceFormViewM()
        }
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.applicationFormTableView.delegate = self
        self.applicationFormTableView.dataSource = self
        self.applicationFormTableView.separatorStyle = .none
        self.applicationFormTableView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.applicationFormTableView.registerMultiple(nibs: [TwoRadioButtonCell.className, DropDownQuestionCell.className, ThreeRadioButtonCell.className, AnswerCell.className])
    }
    
    private func requestGetcarFinanceForm() {
        self.viewModel?.requestgetCarFinanceAPI(vehicleId: self.vehicleId ?? 0, completion: { [weak self] (success, formDetails, isResultEmpty) in
            print(formDetails)
            guard let sSelf = self else { return }
            Threads.performTaskInMainQueue {
                if isResultEmpty {
                    sSelf.shouldCallPostAPI = true
                }
                sSelf.financeFormDatasource = sSelf.viewModel?.getBankApplicationFormDatasource(sSelf.financeFormDatasource, formDetails) ?? []
                sSelf.applicationFormTableView.reloadData()
            }
        })
    }
    
    private func requestUpdateFinanceAPI() {
        self.viewModel?.requestUpdateCarFinanceAPI(params: self.financeFormDatasource, vehicleId: self.vehicleId ?? 0, completion: {[weak self] (success) in
            guard let sSelf = self else { return }
            if sSelf.isSentFromHome {
                sSelf.navigationController?.popToRootViewController(animated: true)
            } else {
                sSelf.moveToBuyCarNowVC()
            }
        })
    }
    
    private func requestPostFinanceAPI() {
        self.viewModel?.requestcarFinanceAPI(params: self.financeFormDatasource, vehicleId: self.vehicleId ?? 0, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            if sSelf.isSentFromHome {
                sSelf.navigationController?.popToRootViewController(animated: true)
            } else {
                sSelf.moveToBuyCarNowVC()
            }
            
        })

    }
    
    private func moveToBuyCarNowVC() {
        let buyCarNowVC = DIConfigurator.sharedInstance.getCBuyCarNowViewC()
        buyCarNowVC.type = self.type
        buyCarNowVC.carDetailModel = self.carDetailModel
        buyCarNowVC.extrasInfoDataSource = self.extrasInfoDataSource
        self.navigationController?.pushViewController(buyCarNowVC, animated: true)
    }
    
    //MARK:- IBAction Methods
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        if self.shouldCallPostAPI {
            self.requestPostFinanceAPI()
        } else {
            self.requestUpdateFinanceAPI()
        }
    }
}
