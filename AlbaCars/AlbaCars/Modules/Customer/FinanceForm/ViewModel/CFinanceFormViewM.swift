//
//  CBankApplicationFormViewM.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 23/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol CFinanceFormVModeling: BaseVModeling {
    func getBankApplicationFormDatasource(_ cellInfo: [CellInfo], _ financeFormDetails: CCarFinanceModel) -> [CellInfo]
    func requestcarFinanceAPI(params:[CellInfo],vehicleId: Int, completion: @escaping (Bool)-> Void)
    func requestgetCarFinanceAPI(vehicleId: Int, completion: @escaping (Bool,CCarFinanceModel, Bool)-> Void)
    func requestUpdateCarFinanceAPI(params:[CellInfo], vehicleId: Int, completion: @escaping (Bool)-> Void)
    
}

class CFinanceFormViewM: BaseViewM, CFinanceFormVModeling {
    
    func getBankApplicationFormDatasource(_ cellInfo: [CellInfo], _ financeFormDetails: CCarFinanceModel) -> [CellInfo] {
        var dataSource = [CellInfo]()
        
        //Existing Loan Cell
        var existingLoanInfo = [String: AnyObject]()
        if let existingLoan = financeFormDetails.existingLoan {
            existingLoanInfo[Constants.UIKeys.optionOneSelected] = Helper.toInt(existingLoan) == 0 ? false as AnyObject : true as AnyObject
            existingLoanInfo[Constants.UIKeys.optionTwoSelected] = Helper.toInt(existingLoan) == 0 ? true as AnyObject : false as AnyObject
            existingLoanInfo[Constants.UIKeys.answerValue] = Helper.toInt(financeFormDetails.totalInstallments) as AnyObject
        } else {
            existingLoanInfo[Constants.UIKeys.optionOneSelected] = false as AnyObject
            existingLoanInfo[Constants.UIKeys.optionTwoSelected] = false as AnyObject
            existingLoanInfo[Constants.UIKeys.answerValue] = "" as AnyObject
        }
        existingLoanInfo[Constants.UIKeys.shouldShowAnswer] = true as AnyObject
        existingLoanInfo[Constants.UIKeys.answerPlaceholder] = "Total Installments".localizedString() as AnyObject
        
        let optionSelected1 = (existingLoanInfo[Constants.UIKeys.optionOneSelected] as? Bool ?? false) ? "1" : "0"
        var existingLoanCell = CellInfo(cellType: .TwoRadioButtonCell, placeHolder: "Q1. Do you have any Existing Loans including cash advance from employer?".localizedString(), value: optionSelected1, info: existingLoanInfo, height: Constants.CellHeightConstants.height_110)
        if existingLoanInfo[Constants.UIKeys.optionOneSelected] as? Bool == true {
            existingLoanCell.height = Constants.CellHeightConstants.height_185
        }
        dataSource.append(existingLoanCell)
        
        
        
        //Credit Card Cell
        var creditCardInfo = [String: AnyObject]()
        //cellInfo[1].info?[Constants.UIKeys.optionTwoSelected]
        if let card = financeFormDetails.card {
            creditCardInfo[Constants.UIKeys.optionOneSelected] = Helper.toInt(card) == 0 ? false as AnyObject : true as AnyObject //cellInfo[1].info?[Constants.UIKeys.optionOneSelected]
            creditCardInfo[Constants.UIKeys.optionTwoSelected] = Helper.toInt(card) == 0 ? true as AnyObject : false as AnyObject
            creditCardInfo[Constants.UIKeys.answerValue] = Helper.toInt(financeFormDetails.cardLimit) as AnyObject
        } else {
            creditCardInfo[Constants.UIKeys.optionOneSelected] = false as AnyObject
            creditCardInfo[Constants.UIKeys.optionTwoSelected] = false as AnyObject
            creditCardInfo[Constants.UIKeys.answerValue] = "" as AnyObject
        }
        creditCardInfo[Constants.UIKeys.answerPlaceholder] = "Total Card Limits".localizedString() as AnyObject
        let optionSelected2 = (creditCardInfo[Constants.UIKeys.optionOneSelected] as? Bool ?? false) ? "1" : "0"
        creditCardInfo[Constants.UIKeys.shouldShowAnswer] = true as AnyObject
        var creditCardCell = CellInfo(cellType: .TwoRadioButtonCell, placeHolder: "Q2. Do You Have Credit Cards?".localizedString(), value: optionSelected2, info: creditCardInfo, height: Constants.CellHeightConstants.height_110)
        if creditCardInfo[Constants.UIKeys.optionOneSelected] as? Bool == true {
            creditCardCell.height = Constants.CellHeightConstants.height_185
        }
        dataSource.append(creditCardCell)
        
        
        
        //Salary Cell
        let salaryCell = CellInfo(cellType: .ThreeRadioButtonCell, placeHolder: "Q3. How Do You Receive Your Salary?".localizedString(), value: Helper.toString(object: financeFormDetails.salaryPaymentMethod), info: nil, height: Constants.CellHeightConstants.height_95)
        dataSource.append(salaryCell)
        
        
        //Bounced Cheque Cell
        var bouncedChequeInfo = [String: AnyObject]()
        bouncedChequeInfo[Constants.UIKeys.optionOneSelected] = Helper.toInt(financeFormDetails.pastLegalCases) == 0 ? false as AnyObject : true as AnyObject //cellInfo[3].info?[Constants.UIKeys.optionOneSelected]
        bouncedChequeInfo[Constants.UIKeys.optionTwoSelected] = Helper.toInt(financeFormDetails.pastLegalCases) == 0 ? true as AnyObject : false as AnyObject
        
        if let pastLegalCases = financeFormDetails.pastLegalCases {
            bouncedChequeInfo[Constants.UIKeys.optionOneSelected] = Helper.toInt(pastLegalCases) == 0 ? false as AnyObject : true as AnyObject
            bouncedChequeInfo[Constants.UIKeys.optionTwoSelected] = Helper.toInt(pastLegalCases) == 0 ? true as AnyObject : false as AnyObject
        } else {
            bouncedChequeInfo[Constants.UIKeys.optionOneSelected] = false as AnyObject
            bouncedChequeInfo[Constants.UIKeys.optionTwoSelected] = false as AnyObject
        }
        let optionSelected3 = (creditCardInfo[Constants.UIKeys.optionOneSelected] as? Bool ?? false) ? "1" : "0"
        bouncedChequeInfo[Constants.UIKeys.shouldShowAnswer] = false as AnyObject
        let bouncedChequeCell = CellInfo(cellType: .TwoRadioButtonCell, placeHolder: "Q4. Any previous record of bounced cheques or bounced loan installments, defaulting credit cards or criminal record in UAE?".localizedString(), value: optionSelected3, info: bouncedChequeInfo, height: Constants.CellHeightConstants.height_110)
        
        dataSource.append(bouncedChequeCell)
        
        
        
        //Employees Cell
        let employeesCell = CellInfo(cellType: .DropDownQuestionCell, placeHolder: "Q5. How many employees in your company?".localizedString(), value: Helper.toString(object: financeFormDetails.noEmployeesInCompany), info: nil, height: Constants.CellHeightConstants.height_130)
        dataSource.append(employeesCell)
        
        //Signboard Cell
        var signBoardInfo = [String: AnyObject]()
        
        if let companyLandline = financeFormDetails.companyLandline {
            signBoardInfo[Constants.UIKeys.optionOneSelected] = Helper.toInt(companyLandline) == 0 ? false as AnyObject : true as AnyObject
            signBoardInfo[Constants.UIKeys.optionTwoSelected] = Helper.toInt(companyLandline) == 0 ? true as AnyObject : false as AnyObject
        } else {
            signBoardInfo[Constants.UIKeys.optionOneSelected] = false as AnyObject
            signBoardInfo[Constants.UIKeys.optionTwoSelected] = false as AnyObject
        }
        creditCardInfo[Constants.UIKeys.shouldShowAnswer] = false as AnyObject
        let optionSelected4 = (creditCardInfo[Constants.UIKeys.optionOneSelected] as? Bool ?? false) ? "1" : "0"
        let signboardCell = CellInfo(cellType: .TwoRadioButtonCell, placeHolder: "Q6. Does your company have a landline number & a signboard ?".localizedString(), value: optionSelected4, info: signBoardInfo, height: Constants.CellHeightConstants.height_110)
        
        dataSource.append(signboardCell)
        
        return dataSource
    }
    
    // api with post method
    func requestcarFinanceAPI(params: [CellInfo], vehicleId: Int, completion: @escaping (Bool) -> Void) {
        if self.validateBankApplicationFomrmData(arrData: params, vehicleId: vehicleId) {
            let formParams = self.getBankApplicationFormParams(arrData: params,vehicleId: vehicleId)
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .carFinance(param: formParams))) { (response, success) in
                if success {
                    if let safeResponse =  response as? [String: AnyObject] {
                        completion(success)
                    }
                }
            }
        }
        
    }
    
    //api with get method
    func requestgetCarFinanceAPI(vehicleId: Int, completion: @escaping (Bool, CCarFinanceModel, Bool) -> Void) {
        var formParams = APIParams()
        formParams[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getCarFinance(param: formParams))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[kResult] as? [String: AnyObject],
                    let resultData = result.toJSONData()
                {
                    if let formDetails = CCarFinanceModel(jsonData: resultData) {
                        if result.count == 0 {
                            completion(success, formDetails, true)
                        } else {
                            completion(success, formDetails, false)
                        }
                        
                    }
                    
                }
            }
        }
    }
    
    // api with put method
    func requestUpdateCarFinanceAPI(params: [CellInfo], vehicleId: Int, completion: @escaping (Bool) -> Void) {
        if self.validateBankApplicationFomrmData(arrData: params, vehicleId: vehicleId) {
            let formParams = self.getBankApplicationFormParams(arrData: params, vehicleId: vehicleId)
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .updateCarFinance(param: formParams))) { (response, success) in
                if success {
                    if let safeResponse =  response as? [String: AnyObject] {
                        completion(success)
                    }
                }
            }
        }
        
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
    private func validateBankApplicationFomrmData(arrData: [CellInfo], vehicleId: Int) -> Bool {
        var isValid = true
        if let firstOpn = arrData[0].info?[Constants.UIKeys.optionOneSelected] as? Bool, let secondOpn = arrData[0].info?[Constants.UIKeys.optionTwoSelected] as? Bool, (!firstOpn && !secondOpn) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please answer question 1.".localizedString())
            isValid = false
        } else if let existingLoans = arrData[0].info?[Constants.UIKeys.answerValue] as? String, let firstOpn = arrData[0].info?[Constants.UIKeys.optionOneSelected] as? Bool, ( firstOpn && existingLoans.isEmpty)   {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide existing loans detail.".localizedString())
            isValid = false
        } else if let firstOpn = arrData[1].info?[Constants.UIKeys.optionOneSelected] as? Bool, let secondOpn = arrData[1].info?[Constants.UIKeys.optionTwoSelected] as? Bool, (!firstOpn && !secondOpn)  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please answer question 2.".localizedString())
            isValid = false
        } else if let existingLoans = arrData[1].info?[Constants.UIKeys.answerValue] as? String, let firstOpn = arrData[1].info?[Constants.UIKeys.optionOneSelected] as? Bool, ( firstOpn && existingLoans.isEmpty) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide card Limit.".localizedString())
            isValid = false
        } else if let salaryType = arrData[2].placeHolder, salaryType.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please answer question 3.".localizedString())
            isValid = false
        } else if let firstOpn = arrData[3].info?[Constants.UIKeys.optionOneSelected] as? Bool, let secondOpn = arrData[3].info?[Constants.UIKeys.optionTwoSelected] as? Bool, (!firstOpn && !secondOpn) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please answer question 4.".localizedString())
            isValid = false
        } else if let numOfEmployees = arrData[4].value, numOfEmployees.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please answer question 5.".localizedString())
            isValid = false
        } else if let firstOpn = arrData[5].info?[Constants.UIKeys.optionOneSelected] as? Bool, let secondOpn = arrData[5].info?[Constants.UIKeys.optionTwoSelected] as? Bool, (!firstOpn && !secondOpn) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please answer question 6.".localizedString())
            isValid = false
        }
        return isValid
    }
    
    func getBankApplicationFormParams(arrData: [CellInfo], vehicleId: Int) -> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.existingLoan] = arrData[0].value as AnyObject
        if let loan = arrData[0].info?[Constants.UIKeys.answerValue] as? String, loan == "" {
            params[ConstantAPIKeys.totalInstallments] = "0"  as AnyObject
        } else {
            params[ConstantAPIKeys.totalInstallments] = arrData[0].info?[Constants.UIKeys.answerValue]  as AnyObject
        }
        
        params[ConstantAPIKeys.card] = arrData[1].value as AnyObject
        if let card = arrData[1].info?[Constants.UIKeys.answerValue] as? String, card == "" {
            params[ConstantAPIKeys.cardLimit] = "0" as AnyObject
        } else {
            params[ConstantAPIKeys.cardLimit] = arrData[1].info?[Constants.UIKeys.answerValue] as AnyObject
        }
        
        params[ConstantAPIKeys.salaryPaymentMethod] = arrData[2].value as AnyObject
        params[ConstantAPIKeys.pastLegalCases] = arrData[3].value as AnyObject
        params[ConstantAPIKeys.noEmployeesInCompany] = arrData[4].value as AnyObject
        params[ConstantAPIKeys.companyLandline] = arrData[5].value as AnyObject
        params[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        
        return params
    }
}
