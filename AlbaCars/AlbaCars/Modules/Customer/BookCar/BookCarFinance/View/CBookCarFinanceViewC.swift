//
//  CBookCarFinanceViewC.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 18/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CBookCarFinanceViewC: BaseViewC {
    
    //MARK:- IBOutlets
    
    @IBOutlet weak var buyingCarLabel: UILabel!
    @IBOutlet weak var reserveContainerView: UIView!
    @IBOutlet weak var bankApplicationView: UIView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var reserveLabel: UILabel!
    @IBOutlet weak var crossReverseImageView: UIImageView!
    @IBOutlet weak var refundableDepositReverseLabel: UILabel!
    @IBOutlet weak var carReservedLabel: UILabel!
    @IBOutlet weak var tickReverseImageView: UIImageView!
    @IBOutlet weak var bankApplicationLabel: UILabel!
    @IBOutlet weak var tickBankImageView: UIImageView!
    @IBOutlet weak var refundableDepositBankLabel: UILabel!
    @IBOutlet weak var crossBankImageView: UIImageView!
    @IBOutlet weak var carBankApplicationLabel: UILabel!
    @IBOutlet weak var reserveViewTopConstraint: NSLayoutConstraint!
    
    //MARK:- Variables
    var type: BuyNowOptions?
    var extrasInfoDataSource = [CellInfo]()
    var carDetailModel = CarDetailModel()
    var vehicleId = 0
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Book Car".localizedString(), barColor: UIColor.redButtonColor, titleColor: UIColor.white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.localizeTexts()
        self.reserveContainerView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_17, round: Constants.UIConstants.sizeRadius_7)
        self.bankApplicationView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_17, round: Constants.UIConstants.sizeRadius_7)
        self.gradientView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.continueButton.roundCorners(Constants.UIConstants.sizeRadius_7)
        Threads.performTaskInMainQueue {
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        if Constants.Devices.IsiPhone6 {
            self.reserveViewTopConstraint.constant = 20
        }
    }
    
    private func localizeTexts() {
        self.buyingCarLabel.text = "Buying a Car through Bank Finance".localizedString()
        self.reserveLabel.text = "Reserve".localizedString()
        self.refundableDepositReverseLabel.text = "Refundable deposit in case of finance rejection".localizedString()
        self.carReservedLabel.text = "Car is reserved for me".localizedString()
        self.bankApplicationLabel.text = "Bank Application".localizedString()
        self.refundableDepositBankLabel.text = "Refundable deposit in case of finance rejection".localizedString()
        self.carBankApplicationLabel.text = "Car is reserved for me".localizedString()
        self.continueButton.setTitle("Continue".localizedString(), for: .normal)
    }
    
    private func configureReservedSelectedView() {
        self.reserveContainerView.backgroundColor = UIColor.redButtonColor
        self.reserveLabel.textColor = UIColor.white
        self.refundableDepositReverseLabel.textColor = UIColor.white
        self.carReservedLabel.textColor = UIColor.white
        self.crossReverseImageView.image = #imageLiteral(resourceName: "cross_white")
        self.tickReverseImageView.image = #imageLiteral(resourceName: "tick_white")
    }
    
    private func configureReservedUnSelectedView() {
        self.reserveContainerView.backgroundColor = UIColor.white
        self.reserveLabel.textColor = UIColor.redButtonColor
        self.refundableDepositReverseLabel.textColor = UIColor.blackTextColor
        self.carReservedLabel.textColor = UIColor.blackTextColor
        self.crossReverseImageView.image = #imageLiteral(resourceName: "cross_red")
        self.tickReverseImageView.image = #imageLiteral(resourceName: "tick_green")
    }
    
    private func configureBankApplicationUnSelectedView() {
        self.bankApplicationView.backgroundColor = UIColor.white
        self.bankApplicationLabel.textColor = UIColor.redButtonColor
        self.refundableDepositBankLabel.textColor = UIColor.blackTextColor
        self.carBankApplicationLabel.textColor = UIColor.blackTextColor
        self.crossBankImageView.image = #imageLiteral(resourceName: "cross_red")
        self.tickBankImageView.image = #imageLiteral(resourceName: "tick_green")
    }
    
    private func configureBankApplicationSelectedView() {
        self.bankApplicationView.backgroundColor = UIColor.redButtonColor
        self.bankApplicationLabel.textColor = UIColor.white
        self.refundableDepositBankLabel.textColor = UIColor.white
        self.carBankApplicationLabel.textColor = UIColor.white
        self.crossBankImageView.image = #imageLiteral(resourceName: "cross_white")
        self.tickBankImageView.image = #imageLiteral(resourceName: "tick_white")
    }
    
    func moveToAutoFinanceVC() {
        let autoFinanceVC = DIConfigurator.sharedInstance.getCAutoFinanceViewC()
        self.navigationController?.pushViewController(autoFinanceVC, animated: true)
    }
    
    func moveToBuyCarNowVC(_ type: BuyNowOptions) {
        let buyCarNowVC = DIConfigurator.sharedInstance.getCBuyCarNowViewC()
        buyCarNowVC.type = type
        self.navigationController?.pushViewController(buyCarNowVC, animated: true)
    }
    
    func moveToFinanceForm(_ type: BuyNowOptions) {
        let financeFormVC = DIConfigurator.sharedInstance.getCFinanceFormViewC()
        financeFormVC.type = type
        financeFormVC.vehicleId = self.vehicleId
        financeFormVC.carDetailModel = self.carDetailModel
        financeFormVC.extrasInfoDataSource = self.extrasInfoDataSource
        self.navigationController?.pushViewController(financeFormVC, animated: true)
    }
    
    //MARK:- IBAction Methods
    
    @IBAction func reverseViewButtonTapped(_ sender: UIButton) {
        self.type = BuyNowOptions.reserve
        self.configureReservedSelectedView()
        self.configureBankApplicationUnSelectedView()
    }
    
    @IBAction func bankApplicationViewButtonTapped(_ sender: UIButton) {
        self.type = BuyNowOptions.bankApplication
        self.configureBankApplicationSelectedView()
        self.configureReservedUnSelectedView()
    }
    
    @IBAction func continueButtonTapped(_ sender: UIButton) {
        if let type = self.type {
            self.moveToFinanceForm(type)
        } else {
            Alert.showOkAlert(title: StringConstants.Text.AppName.localizedString(), message: StringConstants.Validations.selectMethodForBuying.localizedString())
        }
    }
    
}
