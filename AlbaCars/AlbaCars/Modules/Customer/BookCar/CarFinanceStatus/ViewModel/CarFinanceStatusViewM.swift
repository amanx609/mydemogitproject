//
//  CarFinanceStatusViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/30/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

protocol CarFinanceStatusVModeling: BaseVModeling {
    func requestCarFinanceAPI(param: APIParams, completion: @escaping (Int, Int, CarFinanceStatusList) -> Void)
   // func requestCarFinanceDetailsAPI(vehicleId: Int, completion: @escaping (CarDetailModel) -> Void)
    func requestCarFinanceDetailsAPI(param: APIParams, completion: @escaping (CarFinanceStatusModel?) -> Void)
}

class CarFinanceStatusViewM: BaseViewM, CarFinanceStatusVModeling {
    
    func requestCarFinanceAPI(param: APIParams, completion: @escaping (Int, Int, CarFinanceStatusList) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .bankFinance(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let resultData = result.toJSONData(),
                    let carList = CarFinanceStatusList(jsonData: resultData) {
                    completion(nextPageNumber, perPage, carList)
                }
            }
        }
    }
    
    func requestCarFinanceDetailsAPI(param: APIParams, completion: @escaping (CarFinanceStatusModel?) -> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getFinanceCarDetails(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData(),
                    let carList = CarFinanceStatusModel(jsonData: resultData) {
                    completion(carList)
                }
            }
        }
    }

    
//    func requestCarFinanceDetailsAPI(vehicleId: Int, completion: @escaping (CarDetailModel) -> Void) {
//        let buyVehicleAPIParam = getVehicleDetailsAPIParams(vehicleId: vehicleId)
//        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .buyVehicleDetails(param: buyVehicleAPIParam))) { (response, success) in
//            if success {
//                if let safeResponse =  response as? [String: AnyObject],
//                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
//                    let resultData = result.toJSONData(){
//
//                    if let carDetailModel = CarDetailModel.init(jsonData: resultData) {
//                        completion(carDetailModel)
//                    }
//                }
//            }
//        }
//    }
    
//    private func getVehicleDetailsAPIParams(vehicleId: Int) -> APIParams {
//        var param: APIParams = APIParams()
//        param[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
//        return param
//    }
}
