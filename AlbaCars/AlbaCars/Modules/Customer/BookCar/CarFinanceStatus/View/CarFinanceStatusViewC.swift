
//
//  CarFinanceStatusViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/30/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CarFinanceStatusViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var carFinanceStatusTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var financeStatusView: FinanceView!
    
    //MARK: - Variables
    var vehicles: [CarFinanceStatusModel] = []
    var viewModel: CarFinanceStatusViewM?
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    var financeDetails:Bool = false
    var financeId = 0

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    deinit {
        print("deinit CarFinanceStatusViewC")
    }
    
    //MARK: - Private Methods
    
    private func recheckVM() {
           if self.viewModel == nil {
               self.viewModel = CarFinanceStatusViewM()
           }
    }
    
    private func setup() {
        self.setupNavigationBarTitle(title: "Bank Finance".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.recheckVM()
        self.setupView()
        self.setupTableView()
        
        if financeDetails == true {
            self.requestCarFinanceDetailsAPI()
        } else {
            self.requestCarFinanceAPI()
        }
    }
    
    private func setupView() {
       
        
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.carFinanceStatusTableView.delegate = self
        self.carFinanceStatusTableView.dataSource = self
        self.carFinanceStatusTableView.separatorStyle = .none
        //self.carFinanceStatusTableView.allowsSelection = false
        self.carFinanceStatusTableView.tableHeaderView = self.headerView
    }
    
    private func registerNibs() {
        self.carFinanceStatusTableView.register(BuyCarsTableCell.self)
    }
    
    //MARK: - APIMethods
    func requestCarFinanceAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        
        self.viewModel?.requestCarFinanceAPI(param: params, completion: { [weak self] (nextPage, perPage, vehicleList) in
            guard let sSelf = self,
                let vehicles = vehicleList.carFinanceStatusList else { return }
            sSelf.nextPageNumber = nextPage
            sSelf.perPage = perPage
            sSelf.vehicles = vehicles
            Threads.performTaskInMainQueue {
                sSelf.carFinanceStatusTableView.reloadData()
            }
        })
    }
    
    func requestCarFinanceDetailsAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.id] = Helper.toString(object: financeId) as AnyObject
        
        self.viewModel?.requestCarFinanceDetailsAPI(param: params, completion: { [weak self] financeStatus in
            
            guard let sSelf = self else { return }
            
            if let financeStatus = financeStatus {
                
                sSelf.vehicles.append(financeStatus)
                Threads.performTaskInMainQueue {
                    sSelf.carFinanceStatusTableView.tableFooterView = sSelf.footerView
                    sSelf.carFinanceStatusTableView.reloadData()
                }
                
                sSelf.financeStatusView.setupView(financeStatusData: financeStatus.carFinanceStatus)
                
            }
            
        })
    }
}
