//
//  FinanceView.swift
//  AlbaCars
//
//  Created by Narendra on 7/8/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class FinanceView: UIView {

    //MARK: - IBOutlets
    @IBOutlet weak var dashedLineFirstImageView: UIImageView!
    @IBOutlet weak var dashedLineSecondImageView: UIImageView!
    @IBOutlet weak var applicationSubmittedButton: UIButton!
    @IBOutlet weak var verificationButton: UIButton!
    @IBOutlet weak var approvedButton: UIButton!
    @IBOutlet weak var applicationSubmittedLabel: UILabel!
    @IBOutlet weak var verificationCompletedLabel: UILabel!
    @IBOutlet weak var financeApprovedLabel: UILabel!
    @IBOutlet weak var applicationSubmittedTimeLabel: UILabel!
    @IBOutlet weak var verificationCompletedTimeLabel: UILabel!
    @IBOutlet weak var financeApprovedTimeLabel: UILabel!
    
    var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadNib()
    }
    
    private func loadNib() {
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of:self))
        let nib = UINib(nibName: "FinanceView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func setupView(financeStatusData: [FinanceStatus]?) {
      
        
        if let financeStatusData = financeStatusData {
            
              Threads.performTaskInMainQueue {

              self.applicationSubmittedTimeLabel.text = ""
                self.applicationSubmittedTimeLabel.text = ""
                self.applicationSubmittedButton.isSelected = false
                self.approvedButton.isSelected = false
                self.approvedButton.isSelected = false
                for i in 0..<financeStatusData.count {
                    switch financeStatusData[i].status {
                   
                    case 1:
                        self.applicationSubmittedTimeLabel.text = financeStatusData[i].createdAt?.getDateStringFrom(inputFormat: Constants.Format.apiDateTimeFormat, outputFormat: Constants.Format.displayDateTimeFormat)
                        self.applicationSubmittedButton.isSelected = true
                        self.approvedButton.isSelected = false
                        self.approvedButton.isSelected = false
                    case 2:
                        self.verificationCompletedTimeLabel.text = financeStatusData[i].createdAt?.getDateStringFrom(inputFormat: Constants.Format.apiDateTimeFormat, outputFormat: Constants.Format.displayDateTimeFormat)
                        self.applicationSubmittedButton.isSelected = true
                        self.verificationButton.isSelected = true
                        self.approvedButton.isSelected = false
                        self.dashedLineFirstImageView.image = #imageLiteral(resourceName: "dashedGreen")
                    case 3:
                        self.financeApprovedTimeLabel.text = financeStatusData[i].createdAt?.getDateStringFrom(inputFormat: Constants.Format.apiDateTimeFormat, outputFormat: Constants.Format.displayDateTimeFormat)
                        self.applicationSubmittedButton.isSelected = true
                        self.verificationButton.isSelected = true
                        self.approvedButton.isSelected = true
                        self.dashedLineSecondImageView.image = #imageLiteral(resourceName: "dashedGreen")
                    default:
                        break
                    }
                }
            }
            
        }
            
    }
        
}
