//
//  CarFinanceStatusViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/21/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

//MARK:- UITableView Delegates & Datasource Methods

extension CarFinanceStatusViewC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.vehicles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getCell(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.CellHeightConstants.height_160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !financeDetails {
            guard let financeStatusView = FinanceStatusView.inistancefromNib() else { return }
            financeStatusView.initializeViewWith(financeStatusData: self.vehicles[indexPath.row].carFinanceStatus ?? []) { [weak self] (response) in
            }
            financeStatusView.showWithAnimated(animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
         guard let lastVisibleRow = self.carFinanceStatusTableView.indexPathsForVisibleRows?.last?.row else { return }
         if (self.vehicles.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
           self.requestCarFinanceAPI()
         }
    }
    

}

extension CarFinanceStatusViewC {
    
    func getCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: BuyCarsTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let carData = self.vehicles[indexPath.row]
        cell.configureViewForBankFinance(cellData: carData)
        return cell
    }
}
