//
//  FinanceStatusView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/21/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class FinanceStatusView: UIView {

    //MARK: - IBOutlets
    @IBOutlet weak var dashedLineFirstImageView: UIImageView!
    @IBOutlet weak var dashedLineSecondImageView: UIImageView!
    @IBOutlet weak var applicationSubmittedButton: UIButton!
    @IBOutlet weak var verificationButton: UIButton!
    @IBOutlet weak var approvedButton: UIButton!
    @IBOutlet weak var applicationSubmittedLabel: UILabel!
    @IBOutlet weak var verificationCompletedLabel: UILabel!
    @IBOutlet weak var financeApprovedLabel: UILabel!
    @IBOutlet weak var applicationSubmittedTimeLabel: UILabel!
    @IBOutlet weak var verificationCompletedTimeLabel: UILabel!
    @IBOutlet weak var financeApprovedTimeLabel: UILabel!
    @IBOutlet weak var bgViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Private Methods
    private let maxViewHeight: CGFloat = 289.0
    private var selectionCompletion: (([String : AnyObject]) -> Void)?
    fileprivate var listDataSource = [[String :AnyObject]]()
    fileprivate var key = ""
    
    
      //MARK: - ActionMethods
      override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.fadeOutView()
      }
      
      //MARK: - Public Methods
      class func inistancefromNib() -> FinanceStatusView? {
          if let view = UINib(nibName:FinanceStatusView.className, bundle:nil).instantiate(withOwner:nil, options:nil)[0] as? FinanceStatusView {
              return view
          }
          return nil
      }
      
      func initializeViewWith(financeStatusData: [FinanceStatus], handler: (([String : AnyObject]) -> Void)?)
      {
        Threads.performTaskInMainQueue {
            self.bgViewHeightConstraint.constant = self.maxViewHeight
            self.applicationSubmittedTimeLabel.text = ""
            self.applicationSubmittedTimeLabel.text = ""
            self.applicationSubmittedButton.isSelected = false
            self.approvedButton.isSelected = false
            self.approvedButton.isSelected = false
            for i in 0..<financeStatusData.count {
                switch financeStatusData[i].status {
               
                case 1:
                    self.applicationSubmittedTimeLabel.text = financeStatusData[i].createdAt?.getDateStringFrom(inputFormat: Constants.Format.apiDateTimeFormat, outputFormat: Constants.Format.displayDateTimeFormat)
                    self.applicationSubmittedButton.isSelected = true
                    self.approvedButton.isSelected = false
                    self.approvedButton.isSelected = false
                case 2:
                    self.verificationCompletedTimeLabel.text = financeStatusData[i].createdAt?.getDateStringFrom(inputFormat: Constants.Format.apiDateTimeFormat, outputFormat: Constants.Format.displayDateTimeFormat)
                    self.applicationSubmittedButton.isSelected = true
                    self.verificationButton.isSelected = true
                    self.approvedButton.isSelected = false
                    self.dashedLineFirstImageView.image = #imageLiteral(resourceName: "dashedGreen")
                case 3:
                    self.financeApprovedTimeLabel.text = financeStatusData[i].createdAt?.getDateStringFrom(inputFormat: Constants.Format.apiDateTimeFormat, outputFormat: Constants.Format.displayDateTimeFormat)
                    self.applicationSubmittedButton.isSelected = true
                    self.verificationButton.isSelected = true
                    self.approvedButton.isSelected = true
                    self.dashedLineSecondImageView.image = #imageLiteral(resourceName: "dashedGreen")
                default:
                    break
                }
            }
        }
        

        self.isUserInteractionEnabled = true
        
      }
      
      func showWithAnimated(animated : Bool) {
        if #available(iOS 13.0, *) {
          self.showWithView(view: Constants.Devices.keyWindow!, animated: animated)
        } else {
          self.showWithView(view: UIApplication.shared.keyWindow!, animated: animated)
        }
      }
      
      //MARK: - Private method
    private func initWithTitle(financeStatus: FinanceStatusVal) -> AnyObject {
        self.isUserInteractionEnabled = true
        
        return self
      }
      
     
      private func showWithView(view: UIView, animated : Bool) {
        let rect = view.frame
        self.frame = CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height)
        view.addSubview(self)
        if animated {
          self.fadeIn()
        }
      }
      
      private func fadeIn() {
        self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.alpha = 0
        UIView.animate(withDuration: 0.35) {
          self.alpha = 1
          self.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
      }
      
      private func fadeOutView() {
        UIView.animate(withDuration: 0.2, animations: {
        }) { (finished : Bool) in
          if finished {
            self.removeFromSuperview()
          }
        }
      }
    


}
