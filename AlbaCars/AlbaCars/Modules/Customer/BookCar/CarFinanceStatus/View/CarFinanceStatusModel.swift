//
//  CarFinanceStatusModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/21/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class CarFinanceStatusModel: BaseCodable {
    
    var brand: String?
    var carFinanceStatus: [FinanceStatus]?
    var price: Double?
    var status: String?
    var title: String?
    var vehicleImage: [String]?
    var vehicle_id: Int?
    var year: Int?
    var refId: String?
    var id: Int?

    func getReferenceNumber() -> String? {
        return " " + Helper.toString(object: self.refId)
    }
}

class CarFinanceStatusList: BaseCodable {
    var carFinanceStatusList: [CarFinanceStatusModel]?
    
    private enum CodingKeys: String, CodingKey {
      case carFinanceStatusList = "data"
    }
}


class FinanceStatus: BaseCodable {
    var createdAt: String?
    var status: Int?
}


