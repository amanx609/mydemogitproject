//
//  CAutoFinanceViewC.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 19/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CAutoFinanceViewC: BaseViewC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var autoFinanceTitleLabel: UILabel!
    @IBOutlet weak var autoFinanceDescriptionLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.setupNavigationBarTitle(title: "Finance".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.gradientView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.continueButton.roundCorners(Constants.UIConstants.sizeRadius_7)
        Threads.performTaskInMainQueue {
          self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
    }
    
    private func localizeTexts() {
        self.autoFinanceTitleLabel.text = "Find out if you are eligible for auto finance".localizedString()
        self.autoFinanceDescriptionLabel.text = "Please note that this eligiblity report is based on previous cases and is in no way a guarantee of approval".localizedString()
        self.continueButton.setTitle("Continue".localizedString(), for: .normal)
    }
    
    //MARK:- IBAction Methods
    @IBAction func continueButtonTapped(_ sender: UIButton) {
        let financeFormVC = DIConfigurator.sharedInstance.getCFinanceFormViewC()
        financeFormVC.isSentFromHome = true
        self.navigationController?.pushViewController(financeFormVC, animated: true)
    }
    
}
