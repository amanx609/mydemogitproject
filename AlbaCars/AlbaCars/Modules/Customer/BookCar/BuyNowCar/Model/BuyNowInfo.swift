//
//  BuyNowInfo.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 19/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

struct BuyNowInfo {
    var title: String?
    var infoArray: [CellInfo]?
}
