//
//  PaymentInfoTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 19/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class PaymentInfoTableCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var infoHeadingLabel: UILabel!
    @IBOutlet weak var carAttributesTableView: UITableView!
    @IBOutlet weak var topHeadingLabelConstant: NSLayoutConstraint!
    @IBOutlet weak var heightHeadingLabelConstant: NSLayoutConstraint!
    
    //MARK:- Variables
    var data: BuyNowInfo? {
        didSet {
            if let data = self.data {
                configureCell(data)
            }
        }
    }
    var extrasData = [CellInfo]()
    var cellInfo = [CellInfo]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func configureCell(_ data: BuyNowInfo) {
        self.infoHeadingLabel.text = data.title
        if data.title == "" {
            self.topHeadingLabelConstant.constant = 0
            self.heightHeadingLabelConstant.constant = 0
        } 
        self.cellInfo = data.infoArray ?? []
        self.setUpTableView()
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.carAttributesTableView.delegate = self
        self.carAttributesTableView.dataSource = self
        self.carAttributesTableView.separatorStyle = .none
        self.carAttributesTableView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.carAttributesTableView.registerMultiple(nibs: [CarAttributesTableCell.className, ExtrasTableCell.className])
    }    
}

//MARK:- UITableView Delegates & Datasource Methods
extension PaymentInfoTableCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.cellInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellType = self.cellInfo[indexPath.row].cellType else { return UITableViewCell() }
        switch cellType {
        case .CarAttributesTableCell:
            return getCarAttributesCell(tableView, indexPath: indexPath, cellInfo: self.cellInfo[indexPath.row])
        case .ExtrasTableCell:
            return getExtrasTableCell(tableView, indexPath: indexPath, cellInfo: self.cellInfo[indexPath.row])
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.cellInfo[indexPath.row].height
    }
}

extension PaymentInfoTableCell {
    func getCarAttributesCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: CarAttributesTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.data = cellInfo
        return cell
    }
    
    func getExtrasTableCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: ExtrasTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.data = self.extrasData
        return cell
    }
}
