//
//  ExtrasTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 23/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class ExtrasTableCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var extrasHeadingLabel: UILabel!
    @IBOutlet weak var extraAttributesTableView: UITableView!
    
    //MARK:- Variables
    var data: [CellInfo]? {
        didSet {
            if let data = self.data {
                configureCell(data)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func configureCell(_ data: [CellInfo]) {
        self.registerNibs()
        self.setUpTableView()
    }
    
    private func registerNibs() {
        self.extraAttributesTableView.register(nib: ExtraAttributesTableCell.className)
    }
    
    private func setUpTableView() {
        self.extraAttributesTableView.delegate = self
        self.extraAttributesTableView.dataSource = self
        self.extraAttributesTableView.separatorStyle = .none
        self.extraAttributesTableView.allowsSelection = false
    }
}

//MARK:- UITableView Delegates & Datasource Methods
extension ExtrasTableCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let arrData = data {
            return arrData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getExtraAttributesCell(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.data?[indexPath.row].height ?? 0
    }
}

extension ExtrasTableCell {
    func getExtraAttributesCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: ExtraAttributesTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        if let arrData = data {
            cell.data = arrData[indexPath.row]
        }
        return cell
    }
}
