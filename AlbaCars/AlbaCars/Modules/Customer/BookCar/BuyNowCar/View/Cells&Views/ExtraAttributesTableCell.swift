//
//  ExtraAttributesTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 23/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class ExtraAttributesTableCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    //MARK:- Variables
    var data: CellInfo? {
        didSet {
            if let data = self.data {
                configureCell(data)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func configureCell(_ data: CellInfo) {
        self.titleLabel.text = data.placeHolder
       // self.valueLabel.text = data.value
        
        if let isAddAED = data.info?[Constants.UIKeys.isAddAED] as? Bool, isAddAED == true {
            self.valueLabel.text = "AED".localizedString() + " " + data.value
        } else{
            self.valueLabel.text = data.value
        }
    }
}
