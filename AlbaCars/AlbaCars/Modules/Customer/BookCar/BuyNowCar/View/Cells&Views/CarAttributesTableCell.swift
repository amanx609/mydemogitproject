//
//  CarAttributesTableCell.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 19/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CarAttributesTableCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var attributesTitleLabel: UILabel!
    @IBOutlet weak var attributesValueLabel: UILabel!
    
    //MARK:- Variables
    var data: CellInfo? {
        didSet {
            if let data = self.data {
                configureCell(data)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Private Methods
    private func configureCell(_ data: CellInfo) {
        self.attributesTitleLabel.text = data.placeHolder
        
        if let isAddAED = data.info?[Constants.UIKeys.isAddAED] as? Bool, isAddAED == true {
            self.attributesValueLabel.text = "AED".localizedString() + " " + data.value
        } else{
            self.attributesValueLabel.text = data.value
        }
    }
}
