//
//  CBuyNowCarViewC+TableView.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 19/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension CBuyNowCarViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return buyNowInfoDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let buyNowInfo = self.buyNowInfoDataSource[indexPath.row]
        return getPaymentInfoCell(tableView, indexPath: indexPath, data: buyNowInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellInfoArray = self.buyNowInfoDataSource[indexPath.row].infoArray ?? []
        var height: CGFloat = 0.0
        for i in 0..<cellInfoArray.count {
            height = height + cellInfoArray[i].height
        }
        if let title = self.buyNowInfoDataSource[indexPath.row].title, title == "" {
            return height + Constants.CellHeightConstants.height_15
        }
        return Constants.CellHeightConstants.height_50 + height
    }
}

extension CBuyNowCarViewC {
    func getPaymentInfoCell(_ tableView: UITableView, indexPath: IndexPath, data: BuyNowInfo) -> UITableViewCell {
        let cell: PaymentInfoTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.data = data
        cell.extrasData = self.extrasInfoDataSource
        return cell
    }
}
