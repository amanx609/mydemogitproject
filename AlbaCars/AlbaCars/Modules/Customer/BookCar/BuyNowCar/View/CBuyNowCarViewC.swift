//
//  CBuyNowCarViewC.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 19/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CBuyNowCarViewC: BaseViewC {
    
    //MARK:- IBOutlets
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var bookNowHeadingLabel: UILabel!
    @IBOutlet weak var amountPayableLabel: UILabel!
    @IBOutlet weak var termsAndConditionsLabel: UILabel!
    @IBOutlet weak var termsAndConditionsButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var payNowButton: UIButton!
    @IBOutlet weak var buyCarTableView: UITableView!
    @IBOutlet weak var depositAmountLabel: UILabel!
    @IBOutlet weak var heightAmountViewConstant: NSLayoutConstraint!
    
    //MARK:- Variables
    var buyNowInfoDataSource = [BuyNowInfo]()
    var extrasInfoDataSource = [CellInfo]()
    var viewModel: BuyNowCarVModeling?
    var type: BuyNowOptions?
    var carDetailModel = CarDetailModel()
    var bookingType: Int?
    
    
    //MARK:- LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        switch type {
        case .cash:
            self.setupNavigationBarTitle(title: "Book Car".localizedString(), barColor: UIColor.redButtonColor, titleColor: UIColor.white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
            
        case .reserve:
            self.setupNavigationBarTitle(title: "Reserve Car".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
            self.bookingType = 0
        default:
            self.setupNavigationBarTitle(title: "Bank Application".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
            self.bookingType = 1
        }
    }
    
    deinit {
        print("deinit CBuyNowCarViewC")
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.recheckVM()
        switch type {
        case .cash:
            self.setBuyThroughCashView()
        case .reserve:
            self.setBuyThroughReserveView()
        default:
            self.setBuyThroughBankApplicationView()
        }
        self.localizeTexts()
        self.setUpTableView()
        self.gradientView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.payNowButton.roundCorners(Constants.UIConstants.sizeRadius_7)
        Threads.performTaskInMainQueue {
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
    }
    
    private func setBuyThroughCashView() {
        self.bookNowHeadingLabel.text = "Book this Car now for only".localizedString() + " \("AED".localizedString()) " + "\(Helper.toString(object: self.carDetailModel.bookingPrice)) " + "& Pay the Remaining Balance in Cash/Cheque or Bank Transfer".localizedString()
        self.depositAmountLabel.text = ""
        self.heightAmountViewConstant.constant = 230
        self.getDatasourceForBuyCarThroughCash()
    }
    
    private func setBuyThroughReserveView() {
        self.bookNowHeadingLabel.text = "Reserve the Car for".localizedString() + " \("AED".localizedString()) " + "\(Helper.toString(object: self.carDetailModel.bookingPrice)) " + "& Fill-up Bank Finance Application".localizedString()
        self.getDatasourceForBuyCarThroughCash()
    }
    
    private func setBuyThroughBankApplicationView() {
        self.bookNowHeadingLabel.text = "Book the Car for".localizedString() + " \("AED".localizedString()) " + "\(Helper.toString(object: self.carDetailModel.bookingPrice)) "
        self.getDatasourceForBuyCarThroughCash()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = BuyNowCarViewM()
        }
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.buyCarTableView.delegate = self
        self.buyCarTableView.dataSource = self
        self.buyCarTableView.separatorStyle = .none
        self.buyCarTableView.tableHeaderView = self.headerView
    }
    
    private func registerNibs() {
        self.buyCarTableView.registerMultiple(nibs: [CarAttributesTableCell.className, PaymentInfoTableCell.className, ExtrasTableCell.className])
    }
    
    private func getDatasourceForBuyCarThroughCash() {
        if let dataSource = self.viewModel?.getBuyNowInfoDatasource(carDetailModel: self.carDetailModel, extraService: self.extrasInfoDataSource) {
            self.buyNowInfoDataSource = dataSource
        }
        if let dataSource = self.viewModel?.getExtrasDataSource(extraService: self.extrasInfoDataSource) {
            self.extrasInfoDataSource = dataSource
        }
    }
        
    private func localizeTexts() {
        self.termsAndConditionsLabel.text = "I Read and Accept Terms & Conditions".localizedString()
        self.payNowButton.setTitle("Pay Now".localizedString(), for: .normal)
        let bookingPrice = "Amount Payable Now AED".localizedString() + " " + Helper.toString(object: self.carDetailModel.bookingPrice)
        let attributedString = NSMutableAttributedString(string: bookingPrice, attributes: [
            .font: UIFont.font(name: .AirbnbCerealApp, weight: .Bold, size: .size_18),
            .foregroundColor: UIColor.redButtonColor])
        attributedString.apply(color: UIColor.blackTextColor, subString: "Amount Payable Now".localizedString())
        self.amountPayableLabel.attributedText = attributedString
    }
    
    func moveToFinanceFormVC() {
        let financeFormVC = DIConfigurator.sharedInstance.getCFinanceFormViewC()
        self.navigationController?.pushViewController(financeFormVC, animated: true)
    }
    
    func requestBookCarAPI(paymentID: String) {
        
        guard let type = self.type else { return }
        
        self.viewModel?.requestBookCarAPI(carDetailModel: self.carDetailModel, paymentID: paymentID, extraService: self.extrasInfoDataSource, type: type, completion: { (success) in
            Threads.performTaskInMainQueue {
                self.navigationController?.popToRootViewController(animated: true)
                Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Purchased successfully".localizedString())
            }
        })
    }
    
    func payment(paymentServiceCategoryId: PaymentServiceCategoryId,price:Double?) {
        
        let paymentDetails = PaymentDetailsData(viewController: self, totalAmmount: Helper.toDouble(price), referenceId: Helper.toInt(self.carDetailModel.id), paymentServiceCategoryId: paymentServiceCategoryId,vatAmmount: 0)
        
        PaymentManager.sharedInstance.initiatePayment(paymentDetailsData: paymentDetails) { (success, paymentID) in
            
            if success {
                self.requestBookCarAPI(paymentID: paymentID)
            }
        }
        
    }
    
    //MARK:- IBAction Methods
    @IBAction func payNowButtonTapped(_ sender: UIButton) {
        
        if self.termsAndConditionsButton.isSelected {
            
            guard let type = self.type else { return }
            
            switch type {
            case .cash:
                //self.requestBookCarAPI(paymentID:"0")
                self.payment(paymentServiceCategoryId: PaymentServiceCategoryId.carFinance, price: Helper.toDouble(self.carDetailModel.bookingPrice))
            case .reserve:
                self.payment(paymentServiceCategoryId: PaymentServiceCategoryId.buyVehicle, price: Helper.toDouble(self.carDetailModel.price))
            default:
                self.payment(paymentServiceCategoryId: PaymentServiceCategoryId.carFinance, price: Helper.toDouble(self.carDetailModel.price))
            }
            
//            self.viewModel?.requestBookCarAPI(carDetailModel: self.carDetailModel, extraService: self.extrasInfoDataSource, type: type, completion: { (success) in
//                    Threads.performTaskInMainQueue {
//                        self.navigationController?.popToRootViewController(animated: true)
//                    }
//                })

//            PaymentManager.sharedInstance.initiatePayment(viewController: self, totalAmmount: Helper.toInt(self.carDetailModel.price), referenceId: Helper.toInt(self.carDetailModel.id), paymentServiceCategoryId: PaymentServiceCategoryId.leads) { (success) in
//
//            if success {
//                self.viewModel?.requestBookCarAPI(carDetailModel: self.carDetailModel, extraService: self.extrasInfoDataSource, type: type, completion: { (success) in
//                        Threads.performTaskInMainQueue {
//                            self.navigationController?.popToRootViewController(animated: true)
//                        }
//                    })
//
//                }
//            }

        } else {
            Alert.showOkAlert(title: StringConstants.Text.AppName.localizedString(), message: "Please accept terms and conditions".localizedString())
        }
        
    }
    
    @IBAction func termsAndConditionsButtonTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
}
