//
//  BuyNowCarViewM.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 19/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol BuyNowCarVModeling: BaseVModeling {
    func getBuyNowInfoDatasource(carDetailModel: CarDetailModel,extraService:[CellInfo]) -> [BuyNowInfo]
    func getExtrasDataSource(extraService:[CellInfo]) -> [CellInfo]
    func requestBookCarAPI(carDetailModel: CarDetailModel,paymentID: String,extraService:[CellInfo], type: BuyNowOptions, completion: @escaping (Bool) -> Void)
}

class BuyNowCarViewM: BaseViewM, BuyNowCarVModeling {
    
    func getExtrasDataSource(extraService:[CellInfo]) -> [CellInfo] {
        var dataSource = [CellInfo]()
        
        for extras in extraService {
            
            if let info = extras.info {
               // extarPrice = extarPrice + Helper.toInt(info[Constants.UIKeys.servicePrice])
                let warrantyCell = CellInfo(cellType: .CarAttributesTableCell, placeHolder:Helper.toString(object: info[Constants.UIKeys.placeholderText]) , value: Helper.toString(object: info[Constants.UIKeys.servicePrice]), info: [Constants.UIKeys.isAddAED : true as AnyObject], height: Constants.CellHeightConstants.height_20)
                dataSource.append(warrantyCell)
            }
        }
        
        return dataSource
    }
    
    func getBuyNowInfoDatasource(carDetailModel: CarDetailModel,extraService:[CellInfo]) -> [BuyNowInfo] {
        var dataSource = [BuyNowInfo]()
        var carInfoDatasource = [CellInfo]()
        var priceInfoDatasource = [CellInfo]()
        var amountInfoDatasource = [CellInfo]()
        
       let makeCell = CellInfo(cellType: .CarAttributesTableCell, placeHolder: "Make :".localizedString(), value: Helper.toString(object: carDetailModel.brandName), info: nil, height: Constants.CellHeightConstants.height_30)
        
        let modelCell = CellInfo(cellType: .CarAttributesTableCell, placeHolder: "Model :".localizedString(), value: Helper.toString(object: carDetailModel.modelName), info: nil, height: Constants.CellHeightConstants.height_30)
        
        let yearCell = CellInfo(cellType: .CarAttributesTableCell, placeHolder: "Year :".localizedString(), value: Helper.toString(object: carDetailModel.year), info: nil, height: Constants.CellHeightConstants.height_30)
        carInfoDatasource.append(contentsOf: [makeCell, modelCell, yearCell])
        
        //Car Information
        let carInformationType = BuyNowInfo(title: "Car Information".localizedString(), infoArray: carInfoDatasource)
        
        let vehiclePrice = Helper.toInt(carDetailModel.price)
        let vehiclePriceCell = CellInfo(cellType: .CarAttributesTableCell, placeHolder: "Vehicle Price :".localizedString(), value: Helper.toString(object: vehiclePrice), info: [Constants.UIKeys.isAddAED : true as AnyObject], height: Constants.CellHeightConstants.height_30)

        let extrasCell = CellInfo(cellType: .ExtrasTableCell, placeHolder: "Extras :".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_30 + Constants.CellHeightConstants.height_20 * CGFloat(extraService.count))

        let vatPercentage = Helper.toDouble(UserDefaultsManager.sharedInstance.getDoubleValueFor(key:.vatPercentage))

        
        let (vat, totalAmount, depositAmount,remainingBalance) = getCalculateAmount(carDetailModel: carDetailModel, extraService: extraService)
        
       // let amount = Helper.getExtraPrice(extraServices: extraService) + Helper.toInt(carDetailModel.price)
       // let vat = self.vatCalculate(value: amount, percentageVal: Helper.toInt(carDetailModel.vat))
        let vatCell = CellInfo(cellType: .CarAttributesTableCell, placeHolder: "VAT".localizedString() + " \(Helper.toString(object: vatPercentage))% :", value: Helper.toString(object: vat), info: [Constants.UIKeys.isAddAED : true as AnyObject], height: Constants.CellHeightConstants.height_30)
       // let totalAmount = amount + vat
        let totalAmountCell = CellInfo(cellType: .CarAttributesTableCell, placeHolder: "Total Amount :".localizedString(), value: Helper.toString(object: totalAmount), info: [Constants.UIKeys.isAddAED : true as AnyObject], height: Constants.CellHeightConstants.height_30)
        
        priceInfoDatasource.append(contentsOf: [vehiclePriceCell, extrasCell, vatCell, totalAmountCell])
        
        //Price Information
        let priceInformationType = BuyNowInfo(title: "Price Information".localizedString(), infoArray: priceInfoDatasource)
       // let depositAmount = Helper.toInt(carDetailModel.bookingPrice)
        let depositAmountCell = CellInfo(cellType: .CarAttributesTableCell, placeHolder: "Deposit Amount :".localizedString(), value: Helper.toString(object: depositAmount), info: [Constants.UIKeys.isAddAED : true as AnyObject], height: Constants.CellHeightConstants.height_30)
       // let remainingBalance = totalAmount - depositAmount
        let remainingBalanceCell = CellInfo(cellType: .CarAttributesTableCell, placeHolder: "Remaining Balance :".localizedString(), value: Helper.toString(object: remainingBalance), info: [Constants.UIKeys.isAddAED : true as AnyObject], height: Constants.CellHeightConstants.height_30)
        
        amountInfoDatasource.append(contentsOf: [depositAmountCell, remainingBalanceCell])
        
        let amountInformationType = BuyNowInfo(title: "", infoArray: amountInfoDatasource)
        
        dataSource.append(contentsOf: [carInformationType, priceInformationType, amountInformationType])
        
       // let (vat, totalAmount, depositAmount,remainingBalance) = getTime(carDetailModel: carDetailModel, extraService: extraService)
        return dataSource
    }
    
    func getCalculateAmount(carDetailModel: CarDetailModel,extraService:[CellInfo]) -> (Double, Double, Double,Double) {
        let amount = Helper.getExtraPrice(extraServices: extraService) + Helper.toInt(carDetailModel.price)
        let vatPercentage = Helper.toDouble(UserDefaultsManager.sharedInstance.getDoubleValueFor(key:.vatPercentage))

        let vat = self.vatCalculate(value: Double(amount), percentageVal: vatPercentage)
        let totalAmount = Double(amount) + vat
        let depositAmount = Helper.toDouble(carDetailModel.bookingPrice)
        let remainingBalance = totalAmount - depositAmount
        return (vat, totalAmount, depositAmount,remainingBalance)
    }
    
    //Calucate percentage based on given values
    public func vatCalculate(value:Double,percentageVal:Double)->Double{
        let val = value * percentageVal
        return val / 100
    }
    
    func requestBookCarAPI(carDetailModel: CarDetailModel,paymentID: String,extraService:[CellInfo], type: BuyNowOptions, completion: @escaping (Bool) -> Void) {
        
        let param = getBookCarAPIParams(carDetailModel: carDetailModel, paymentID: paymentID, extraService: extraService, type: type)
           APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .payNow(param: param))) { (response, success) in
               if success {
                   if let safeResponse =  response as? [String: AnyObject],
                       let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                   {
                    completion(success)
                      // Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Notification added Successfully".localizedString())
                   }
               }
           }
       }
    
    private func getBookCarAPIParams(carDetailModel: CarDetailModel,paymentID: String,extraService:[CellInfo],type: BuyNowOptions) -> APIParams {
        var param: APIParams = APIParams()
        let (vat, totalAmount, depositAmount,remainingBalance) = getCalculateAmount(carDetailModel: carDetailModel, extraService: extraService)
        param[ConstantAPIKeys.vehicleId] = Helper.toInt(carDetailModel.id) as AnyObject
        param[ConstantAPIKeys.vehiclePrice] = Helper.toInt(carDetailModel.price) as AnyObject
        param[ConstantAPIKeys.vatAmmount] = vat as AnyObject
        param[ConstantAPIKeys.totalAmmount] = totalAmount as AnyObject
        param[ConstantAPIKeys.depositAmmount] = depositAmount as AnyObject
        param[ConstantAPIKeys.remainingAmmount] = remainingBalance as AnyObject
        param[ConstantAPIKeys.bookingType] = type.rawValue as AnyObject
        
        if paymentID != "0" {
            param[ConstantAPIKeys.paymentId] = paymentID as AnyObject
        }
        
        var extrasData = [[String: AnyObject]]()
        
        for extras in extraService {
            var extraDict  = [String: AnyObject]()
            if let extra = extras.info?[Constants.UIKeys.extras] as? Extras {
                extraDict[ConstantAPIKeys.title] = extra.title as AnyObject?
             //   extraDict[ConstantAPIKeys.free] = extra.free as AnyObject?
             //   extraDict[ConstantAPIKeys.freeValue] = extra.freeValue as AnyObject?
                extraDict[ConstantAPIKeys.price] = extra.price as AnyObject?
                extrasData.append(extraDict)
            }
        }
        param[ConstantAPIKeys.extras] = extrasData as AnyObject

        return param
    }
}
