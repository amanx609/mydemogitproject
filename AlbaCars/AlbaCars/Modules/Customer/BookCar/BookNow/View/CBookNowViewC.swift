//
//  CBookNowViewC.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 16/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CBookNowViewC: BaseViewC {
    
    //MARK:- IBOutlets
    @IBOutlet weak var selectModeLabel: UILabel!
    @IBOutlet weak var cashLabel: UILabel!
    @IBOutlet weak var financeLabel: UILabel!
    @IBOutlet weak var cashImageButton: UIButton!
    @IBOutlet weak var financeImageButton: UIButton!
    
    //MARK:- Variables
    var extraServicesDatasource = [CellInfo]()
    var carDetailModel = CarDetailModel()
    var vehicleId = 0
    var carDetails: ChooseCarModel?
    var isSentFromChooseCar = false
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }
    
    //MARK:- Private Methods
    private func setUpView() {
        self.localizeTexts()
        if self.isSentFromChooseCar {
            self.selectModeLabel.isHidden = true
            self.cashImageButton.setImage(#imageLiteral(resourceName: "tyres"), for: .normal)
            self.financeImageButton.setImage(#imageLiteral(resourceName: "rims"), for: .normal)
            self.cashLabel.text = "Tires".localizedString()
            self.financeLabel.text = "Rims".localizedString()
        } else {
            self.selectModeLabel.isHidden = false
            self.cashImageButton.setImage(#imageLiteral(resourceName: "cash_icon"), for: .normal)
            self.financeImageButton.setImage(#imageLiteral(resourceName: "finance_icon"), for: .normal)
            self.cashLabel.text = "Cash".localizedString()
            self.financeLabel.text = "Finance".localizedString()
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isSentFromChooseCar {
            self.setupNavigationBarTitle(title: "Wheels Tender".localizedString(), barColor: UIColor.white, titleColor: UIColor.black, leftBarButtonsType: [.back], rightBarButtonsType: [])
        } else {
            self.setupNavigationBarTitle(title: "Book Now".localizedString(), barColor: UIColor.redButtonColor, titleColor: UIColor.white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        }
    }
    
    private func localizeTexts() {
        
        self.selectModeLabel.text = "Select Your Mode of Car Purchase".localizedString()
        self.cashLabel.text = "Cash".localizedString()
        self.financeLabel.text = "Finance".localizedString()
    }
    
    func moveToBookCarFinanceVC() {
        if self.isSentFromChooseCar {
            let tenderRequestViewC = DIConfigurator.sharedInstance.getRimsRequestVC()
            tenderRequestViewC.vehicleId = self.vehicleId
            tenderRequestViewC.carDetails = self.carDetails
            self.navigationController?.pushViewController(tenderRequestViewC, animated: true)
            
        } else {
            let bookCarFinanceVC = DIConfigurator.sharedInstance.getCBookCarFinanceViewC()
            bookCarFinanceVC.extrasInfoDataSource = self.extraServicesDatasource
            bookCarFinanceVC.carDetailModel = self.carDetailModel
            bookCarFinanceVC.vehicleId = self.vehicleId
            self.navigationController?.pushViewController(bookCarFinanceVC, animated: true)
        }
        
    }
    
    func moveToBuyCarNowVC() {
        if self.isSentFromChooseCar {
            let tenderRequestViewC = DIConfigurator.sharedInstance.getTyresRequestVC()
            tenderRequestViewC.vehicleId = self.vehicleId
            tenderRequestViewC.carDetails = self.carDetails
            self.navigationController?.pushViewController(tenderRequestViewC, animated: true)
            
        } else {
            let buyCarNowVC = DIConfigurator.sharedInstance.getCBuyCarNowViewC()
            buyCarNowVC.type = BuyNowOptions.cash
            buyCarNowVC.extrasInfoDataSource = self.extraServicesDatasource
            buyCarNowVC.carDetailModel = self.carDetailModel
            self.navigationController?.pushViewController(buyCarNowVC, animated: true)
        }
        
    }
    
    func moveToFinanceForm() {
        
        let financeFormViewC = DIConfigurator.sharedInstance.getCFinanceFormViewC()
        financeFormViewC.vehicleId = self.vehicleId
        self.navigationController?.pushViewController(financeFormViewC, animated: true)
        
    }
    
    //MARK:- IBAction Methods
    
    @IBAction func cashButtonTapped(_ sender: UIButton) {
        self.moveToBuyCarNowVC()
    }
    
    @IBAction func financeButtonTapped(_ sender: UIButton) {
        self.moveToBookCarFinanceVC()
        
    }
}
