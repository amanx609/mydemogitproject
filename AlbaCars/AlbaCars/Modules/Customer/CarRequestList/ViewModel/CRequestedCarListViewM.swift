//
//  CRequestedCarListViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol CRequestedCarListViewModeling {
    func requestGetRequestedCarAPI(page:Int, completion: @escaping ([CRequestCarListModel], Int, Int, Bool)-> Void)
    func requestCarDetailsAPI(param: APIParams, completion: @escaping (CRequestCarListModel,Bool)-> Void)
}

class CRequestedCarListViewM: CRequestedCarListViewModeling {
    
    func requestGetRequestedCarAPI(page: Int, completion: @escaping ([CRequestCarListModel], Int, Int, Bool)-> Void) {
      let getRequestedCarListParam = getAPIParams(page: page)
    APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getRequestVehicle(param: getRequestedCarListParam))) { (response, success) in
              if success {
                if let safeResponse =  response as? [String: AnyObject], let result = safeResponse[kResult] as? [String: AnyObject], let cellData = result[kData] as? [[String: AnyObject]], let nextPageNumber = result[kNextPageNum] as? Int, let perPage = result[kPerPage] as? Int, let listArray = cellData.toJSONData() {
                      do {
                          let decoder = JSONDecoder()
                          let carList = try decoder.decode([CRequestCarListModel].self, from: listArray)
                          completion(carList, perPage, nextPageNumber, success)
                      } catch let error {
                        print(error)
                      }
                  
                }
              }
            }
      }
    
    private func getAPIParams(page: Int) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.page] = page as AnyObject
        return param
    }
    
    func requestCarDetailsAPI(param: APIParams, completion: @escaping (CRequestCarListModel,Bool)-> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getRequestedVehicleDetails(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let carList = try decoder.decode(CRequestCarListModel.self, from: resultData)
                        completion(carList, success)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
        
    }
}
