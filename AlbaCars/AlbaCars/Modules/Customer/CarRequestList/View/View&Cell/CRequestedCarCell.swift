//
//  CRequestCarCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CRequestedCarCell: BaseTableViewCell, ReusableView, NibLoadableView  {
    
    //MARK: - IBOutlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var priceRangeLabel: UILabel!
    @IBOutlet weak var mileageLabel: UILabel!
    @IBOutlet weak var yearRangeLabel: UILabel!
    @IBOutlet weak var combinedCarNameLabel: UILabel!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var carNameLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.bgView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.shadowView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
    }
    
    //MARK: - Public Methods
    func configureView(cellData: CRequestCarListModel) {
        if let brandName = cellData.brandName, let modelName = cellData.modelName {
            self.carNameLabel.text = brandName
            self.carModelLabel.text = modelName
        }
        
        if let brandName = cellData.brandName, let modelName = cellData.modelName {
            self.combinedCarNameLabel.text = "\(brandName) \(modelName) \(cellData.typeName ?? "")"
        }
        if let year = cellData.yearFrom, let currentYear = Date().getYear() {
           self.yearRangeLabel.text =  "\(year) - \(currentYear)"
        }
        
        if let mileageFrom = cellData.mileageFrom, let mileageTo = cellData.mileageTo {
            let mileageFromText = mileageFrom > 1 ? "\(mileageFrom.formattedWithSeparator) " + "Kms".localizedString() : "\(mileageFrom.formattedWithSeparator) " + "Km".localizedString()
            let mileageToText = mileageTo > 1 ? "\(mileageTo.formattedWithSeparator) " + "Kms".localizedString() : "\(mileageTo.formattedWithSeparator) " + "Km".localizedString()
            self.mileageLabel.text = "\(mileageFromText) " + "to".localizedString() + " \(mileageToText)"
        }
        
        self.priceRangeLabel.text = cellData.getPriceRange()
        
    }
    
}
