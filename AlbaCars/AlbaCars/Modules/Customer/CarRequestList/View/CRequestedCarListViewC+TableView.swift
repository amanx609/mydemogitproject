//
//  CRequestedCarListViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import Foundation
import UIKit

extension CRequestedCarListViewC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.carListDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // let document = self.documentListDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath)
    }
    
    
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    if let lastVisibleIndex = self.carListTableView.indexPathsForVisibleRows?.last?.row,
      self.perPage - lastVisibleIndex == Constants.Validations.minCountBeforAPICall,
      self.nextPageNumber != 0 {
      self.callgetRequestedCarAPI()
    }
  }
}

extension CRequestedCarListViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: CRequestedCarCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(cellData: self.carListDataSource[indexPath.row])
        return cell
    }
}
