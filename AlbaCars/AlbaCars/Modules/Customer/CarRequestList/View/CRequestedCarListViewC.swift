//
//  CRequestedCarListViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CRequestedCarListViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var carListTableView: UITableView!
    @IBOutlet weak var requestedCarLabel: UILabel!
    
    //MARK: - Variables
    var carListDataSource = [CRequestCarListModel]()
    var viewModel: CRequestedCarListViewModeling?
    var nextPageNumber = 1
    var perPage = 10
    var requestedCarDetails:Bool = false
    var vehicleId = 0
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    deinit {
        print("deinit CRequestedCarListViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupTableView()
        self.setupNavigationBarTitle(title: "Request a Car", leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.requestedCarLabel.isHidden = true
        
        if self.requestedCarDetails {
            self.requestCarDetailsAPI()
        } else {
            callgetRequestedCarAPI()
        }
        
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = CRequestedCarListViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.carListTableView.backgroundColor = UIColor.white
        self.carListTableView.delegate = self
        self.carListTableView.dataSource = self
        self.carListTableView.separatorStyle = .none
        //self.carListTableView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.carListTableView.register(CRequestedCarCell.self)
    }
    
    //MARK: - API Methods
    func callgetRequestedCarAPI() {
        self.viewModel?.requestGetRequestedCarAPI(page: self.nextPageNumber, completion: { (response, perPage, nextPageNum,  success) in
            if success {
                Threads.performTaskInMainQueue {
                    self.nextPageNumber = nextPageNum
                    self.perPage = perPage
                    self.carListDataSource = self.carListDataSource + response
                    
                    if self.carListDataSource.count != 0 {
                        self.requestedCarLabel.isHidden = false
                    }
                    self.carListTableView.reloadData()
                }
            }
        })
    }
    
    func requestCarDetailsAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.id] = Helper.toString(object: vehicleId) as AnyObject
        
        self.viewModel?.requestCarDetailsAPI(param: params, completion: { [weak self] (response,success) in
            
            guard let sSelf = self else { return }
            
            if success {
                Threads.performTaskInMainQueue {
                    sSelf.carListDataSource.append(response)
                    sSelf.requestedCarLabel.isHidden = false
                    sSelf.carListTableView.reloadData()
                }
            }
        })
    }
       
}
