//
//  CRequestCarModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/2/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class CRequestCarListModel: Codable {
    
    var brand: Int?
    var brandName: String?
    var financePrice: Int?
    var id: Int?
    var mileageFrom: Int?
    var mileageTo: Int?
    var model: Int?
    var modelName: String?
    var priceFrom: Int?
    var priceTo: Int?
    //var requestedVehicleNote: String?
    var type: Int?
    var typeName: String?
    var yearFrom: Int?
    
    func getPriceRange() -> String {
           
        return "AED".localizedString() + Helper.toString(object: self.priceFrom) + " - " + Helper.toString(object: self.priceTo)
    }
    
}



