//
//  CRequestCarViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol CRequestCarViewModeling {
    func getRequestCarFormDataSource() -> [CellInfo]
    func requestPostRequestCarAPI(arrData: [CellInfo], completion: @escaping (Bool)-> Void)
}

class CRequestCarViewM: CRequestCarViewModeling {
    
    func getRequestCarFormDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Select Car Brand Cell
        let brandCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: StringConstants.Text.selectBrand.localizedString(), height: Constants.CellHeightConstants.height_80)
        array.append(brandCell)
        
        // car name Cell
        let nameCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value:StringConstants.Text.modelName.localizedString(), height: Constants.CellHeightConstants.height_80)
        array.append(nameCell)
        
        //Select car type
        let carTypeCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "Car Type".localizedString(), height: Constants.CellHeightConstants.height_80)
        array.append(carTypeCell)
        
        //Range Title Cell
        let titleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Enter Price Range".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(titleCell)
        
        //PriceRange Cell
        let priceRangeCell = CellInfo(cellType: .PriceRangeTextFieldCell, placeHolder: "", value: "", info: nil, height: Constants.CellHeightConstants.height_80,keyboardType: .numberPad)
        array.append(priceRangeCell)
        
        //Oldest Model Year Title Cell
        let oldestCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Oldest Model Year".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(oldestCell)
        
        //Oldest Model Year
        let oldestModelYearCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "2017", height: Constants.CellHeightConstants.height_80)
        array.append(oldestModelYearCell)
        
        //Mileage Title Cell
        let mileageCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Mileage".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(mileageCell)
        
        //Mileage Range Cell
        var mileageRangeInfo = [String: AnyObject]()
        mileageRangeInfo[Constants.UIKeys.mileageFrom] = 1 as AnyObject
        mileageRangeInfo[Constants.UIKeys.mileageTo] = 15000 as AnyObject
        let mileageRangeCell = CellInfo(cellType: .MileageRangeCell, placeHolder: "", value: "Mileage".localizedString(), info: mileageRangeInfo, height: Constants.CellHeightConstants.height_120)
        array.append(mileageRangeCell)
        
        //Maximum Monthly Installments Title Cell
        let installmentCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Maximum Monthly Installments".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(installmentCell)
        
        // If taken on finance Title Cell
        let financeCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "If taken on finance".localizedString(), info: nil, height: Constants.CellHeightConstants.height_30)
        array.append(financeCell)
        
        //AED Cell
        var cardInfo = [String: AnyObject]()
        cardInfo[Constants.UIKeys.inputType] = TextInputType.mileage as AnyObject
        let cardCell = CellInfo(cellType: .TextInputCell, placeHolder: "AED".localizedString(), value: "", info: cardInfo, height: Constants.CellHeightConstants.height_80,keyboardType: .numberPad)
        array.append(cardCell)
        
        //TextView Cell
        let textViewCell = CellInfo(cellType: .TextViewCell, placeHolder: "Additional Information".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_100)
        array.append(textViewCell)
        return array
    }
    
    //MARK: - API Methods
    func requestPostRequestCarAPI(arrData: [CellInfo], completion: @escaping (Bool)-> Void) {
        if validatePostRequestCarData(arrData: arrData) {
            let requestVehicleParams = getPostRequestCarParams(arrData: arrData)
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .postRequestVehicle(param: requestVehicleParams))) { (response, success) in
                if success {
                    if let _ =  response as? [String: AnyObject] {
                        completion(success)
                    }
                }
            }
        }
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
    func validatePostRequestCarData(arrData: [CellInfo]) -> Bool {
        var isValid = true
        if let carBrand = arrData[0].placeHolder, carBrand.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select car brand.".localizedString())
            isValid = false
        } else if let carModel = arrData[1].placeHolder, carModel.isEmpty  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select car model.".localizedString())
            isValid = false
        } else if let carType = arrData[2].value, carType.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select car type.".localizedString())
            isValid = false
        } else if let minPrice = arrData[4].placeHolder, minPrice.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter minimum price.".localizedString())
            isValid = false
        } else if let maxPrice = arrData[4].value, maxPrice.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter maximum price.".localizedString())
            isValid = false
        } else if let minPrice = Int(arrData[4].placeHolder), let maxPrice = Int(arrData[4].value), minPrice > maxPrice {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Minimum price can not be greater than Maximum price".localizedString())
            isValid = false
        } else if let carYear = arrData[6].value, carYear.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select car year.".localizedString())
            isValid = false
        }
        return isValid
    }
    
    
    func getPostRequestCarParams(arrData: [CellInfo])-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.brand] = arrData[0].placeHolder as AnyObject
        params[ConstantAPIKeys.carModel] = arrData[1].placeHolder as AnyObject
        params[ConstantAPIKeys.carType] = arrData[2].placeHolder as AnyObject
        params[ConstantAPIKeys.priceFrom] = arrData[4].placeHolder as AnyObject
        params[ConstantAPIKeys.priceTo] = arrData[4].value as AnyObject
        params[ConstantAPIKeys.yearFrom] = arrData[6].value as AnyObject
        if let mileageRangeInfo = arrData[8].info {
            if let mileageFrom = mileageRangeInfo[Constants.UIKeys.mileageFrom] as? Int {
                params[ConstantAPIKeys.mileageFrom] = mileageFrom as AnyObject
            }
            if let mileageTo = mileageRangeInfo[Constants.UIKeys.mileageTo] as? Int {
                params[ConstantAPIKeys.mileageTo] = mileageTo as AnyObject
            }
        }
        
        let value = arrData[11].value.trim()
        
        if  !value.isEmpty {
            params[ConstantAPIKeys.financePrice] = value as AnyObject
        }
        
        params[ConstantAPIKeys.additionalInformation] = arrData[12].value as AnyObject
        
        return params
    }
}



