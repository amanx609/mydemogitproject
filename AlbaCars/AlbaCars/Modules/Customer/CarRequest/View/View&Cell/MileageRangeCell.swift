//
//  MileageRangeCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import WARangeSlider

protocol MileageRangeCellDelegate: class {
    func didChangeRangeValue(cell: MileageRangeCell, lowerValue: Double, upperValue: Double)
}

class MileageRangeCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var mileagePlaceholderLabel: UILabel!
    @IBOutlet weak var minimumMileageView: UIView!
    @IBOutlet weak var maximumMileageView: UIView!
    @IBOutlet weak var maximumRangeValueLabel: UILabel!
    @IBOutlet weak var minimumRangeValueLabel: UILabel!
    @IBOutlet weak var maximumMileageLabel: UILabel!
    @IBOutlet weak var minimumMileageLabel: UILabel!
    @IBOutlet weak var rangeSliderView: RangeSlider!
    
    //MARK: - Variables
    weak var delegate: MileageRangeCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupView() {
      self.bgView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.mileagePlaceholderLabel.text = "Mileage".localizedString()
        self.minimumMileageView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.maximumMileageView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.rangeSliderView.lowerValue = Double(1)
        self.rangeSliderView.upperValue = Double(15000)
        
        self.rangeSliderView.minimumValue = Double(Constants.initialMinMileage)
        self.rangeSliderView.maximumValue = Double(Constants.initialMaxMileage)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info,
            let mileageFrom = info[Constants.UIKeys.mileageFrom] as? Int,
            let mileageTo = info[Constants.UIKeys.mileageTo] as? Int {
            //MinMileage
            let minMileageText = mileageFrom > 1 ? "\(mileageFrom.formattedWithSeparator) " + "Kms".localizedString() : "\(mileageFrom.formattedWithSeparator) " + "Km".localizedString()
            self.minimumMileageLabel.text = minMileageText
            //MaxMileage
            let maxMileageText = mileageTo > 1 ? "\(mileageTo.formattedWithSeparator) " + "Kms".localizedString() : "\(mileageTo.formattedWithSeparator) " + "Km".localizedString()
            self.maximumMileageLabel.text = maxMileageText
        }
    }
        
    //MARK: - IBActions
    
    @IBAction func sliderValueChanged(_ sender: RangeSlider) {
        if let delegate = self.delegate {
            delegate.didChangeRangeValue(cell: self, lowerValue: sender.lowerValue, upperValue: sender.upperValue)
        }
    }
}
