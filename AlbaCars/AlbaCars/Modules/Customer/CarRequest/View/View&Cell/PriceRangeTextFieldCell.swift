//
//  PriceRangeTextFieldCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol PriceRangeTextFieldCellDelegate: class {
    func didChangePriceRangeText(cell: PriceRangeTextFieldCell, fromText: String, toText: String)
    func tapNextKeyboard(cell: PriceRangeTextFieldCell)
}

class PriceRangeTextFieldCell: BaseTableViewCell, NibLoadableView, ReusableView {

    //MARK: - IBOutlets
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var toTextField: UITextField!
    @IBOutlet weak var fromView: UIView!
    @IBOutlet weak var toView: UIView!
    
    //MARK: - Variables
    weak var delegate: PriceRangeTextFieldCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        self.setupTextField()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupView() {
        fromTextField.attributedPlaceholder = NSAttributedString(string: "From".localizedString(),
                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.blackColor])
        toTextField.attributedPlaceholder = NSAttributedString(string: "To".localizedString(),
                                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.blackColor])
      self.fromView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.toView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
    private func setupTextField() {
        self.fromTextField.delegate = self
        self.toTextField.delegate = self
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        fromTextField.keyboardType = cellInfo.keyboardType
        toTextField.keyboardType = cellInfo.keyboardType
    }
}

extension PriceRangeTextFieldCell: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if let delegate = self.delegate {
      delegate.tapNextKeyboard(cell: self)
    }
    return true
  }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let delegate = self.delegate {
            delegate.didChangePriceRangeText(cell: self, fromText: self.fromTextField.text ?? "", toText: self.toTextField.text ?? "")
        }
        
    }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if let text = textField.text, let textRange = Range(range, in: text), let delegate = self.delegate {
      let finalText = text.replacingCharacters(in: textRange, with: string)
       
        if finalText.count > Constants.Validations.lenth9 {
          return false
        } else {
           
            return true
        }
    }
    return true
  }
}
