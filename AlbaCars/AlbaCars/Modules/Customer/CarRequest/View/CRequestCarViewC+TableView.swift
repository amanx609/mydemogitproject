//
//  CRequestCarViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension CRequestCarViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.requestCarDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.requestCarDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.requestCarDataSource[indexPath.row].height
    }
}

extension CRequestCarViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .DropDownCell:
            let cell: DropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .TextInputCell:
            let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.placeholderView.isHidden = true
            self.setupToolBar(cell.inputTextField)
            cell.configureViewWithGreyPlaceholder(cellInfo: cellInfo)
            return cell
        case .TextViewCell:
            let cell: TextViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate =  self
            cell.configureViewWithBlackPlaceholder(cellInfo: cellInfo)
            return cell
        case .SideMenuCell:
            let cell: SideMenuCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            if indexPath.row == 10 {
                cell.configureViewWithGreyText(cellInfo: cellInfo)
                return cell
            }
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .PriceRangeTextFieldCell:
            let cell: PriceRangeTextFieldCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            self.setupToolBar(cell.fromTextField)
            self.setupToolBar(cell.toTextField)
             cell.configureView(cellInfo: cellInfo)
            return cell
        case .MileageRangeCell:
            let cell: MileageRangeCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - DropDownCellDelegate
extension CRequestCarViewC: DropDownCellDelegate {
    func didTapDropDownCell(cell: DropDownCell) {
        guard let indexPath = self.requestCarTableView.indexPath(for: cell) else { return }
        self.showDropDownAt(indexPath: indexPath)
    }
}

//MARK: - TextInputCellDelegate
extension CRequestCarViewC: TextInputCellDelegate {
    func tapNextKeyboard(cell: TextInputCell) {
        guard var indexPath = self.requestCarTableView.indexPath(for: cell) else { return }
        indexPath.row += 1
        if let cell = self.requestCarTableView.cellForRow(at: indexPath) as? TextInputCell {
            cell.inputTextField.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }
    }
    
    func didChangeText(cell: TextInputCell, text: String) {
        guard let indexPath = self.requestCarTableView.indexPath(for: cell) else { return }
        self.requestCarDataSource[indexPath.row].value = text
    }
}

//MARK: - TextViewCellDelegate
extension CRequestCarViewC: TextViewCellDelegate {
    func didChangeText(cell: TextViewCell, text: String) {
        guard let indexPath = self.requestCarTableView.indexPath(for: cell) else { return }
        self.requestCarDataSource[indexPath.row].value = text
    }
}

//MARK: - MileageRangeCell Delegate
extension CRequestCarViewC: MileageRangeCellDelegate {
    func didChangeRangeValue(cell: MileageRangeCell, lowerValue: Double, upperValue: Double) {
        guard let indexPath = self.requestCarTableView.indexPath(for: cell) else { return }
        let intMinMileage = Int(lowerValue)
        let intMaxMileage = Int(upperValue)
        if var mileageRangeinfo = self.requestCarDataSource[indexPath.row].info {
            mileageRangeinfo[Constants.UIKeys.mileageFrom] = intMinMileage as AnyObject
            mileageRangeinfo[Constants.UIKeys.mileageTo] = intMaxMileage as AnyObject
            self.requestCarDataSource[indexPath.row].info = mileageRangeinfo
            self.requestCarTableView.reloadData()
        }
    }
    
    func didChangeRangeValue(cell: MileageRangeCell) {
        guard let indexPath = self.requestCarTableView.indexPath(for: cell) else { return }
        self.requestCarDataSource[indexPath.row].placeHolder = cell.minimumMileageLabel.text
        self.requestCarDataSource[indexPath.row].value = cell.maximumMileageLabel.text
    }
}

//MARK: - PriceRangeTextFieldCell Delegate
extension CRequestCarViewC: PriceRangeTextFieldCellDelegate {
    func didChangePriceRangeText(cell: PriceRangeTextFieldCell, fromText: String, toText: String) {
        self.view.endEditing(true)
        guard let indexPath = self.requestCarTableView.indexPath(for: cell) else { return }
        self.requestCarDataSource[indexPath.row].placeHolder = fromText
        self.requestCarDataSource[indexPath.row].value = toText
    }
    
    func tapNextKeyboard(cell: PriceRangeTextFieldCell) {
         guard var indexPath = self.requestCarTableView.indexPath(for: cell) else { return }
         indexPath.row += 1
         if let cell = self.requestCarTableView.cellForRow(at: indexPath) as? TextInputCell {
             cell.inputTextField.becomeFirstResponder()
         } else {
             self.view.endEditing(true)
         }
    }    
}
