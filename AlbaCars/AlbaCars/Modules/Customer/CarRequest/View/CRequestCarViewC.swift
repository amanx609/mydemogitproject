//
//  CRequestCarViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CRequestCarViewC: BaseViewC {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var requestCarTableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    
    //MARK: - Properties
    var viewModel: CRequestCarViewModeling?
    var addCarViewModel: CAddNewCarViewModeling?
    var requestCarDataSource: [CellInfo] = []
    var vehicleBrandDataSource: [[String: AnyObject]] = []
    var vehicleTypeDataSource: [[String: AnyObject]] = []
    var vehicleModelDataSource: [[String: AnyObject]] = []
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        setupView()
        roundCorners()
        setupViewModel()
        registerNibs()
        setupTableView()
        self.setupNavigationBarTitle(title: "Request a Car".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.requestVehicleBrandAPI()
    }
    
    private func setupTableView() {
        self.requestCarTableView.separatorStyle = .none
        requestCarTableView.backgroundColor = UIColor.white
        requestCarTableView.delegate = self
        requestCarTableView.dataSource = self
        if let dataSource = self.viewModel?.getRequestCarFormDataSource() {
            self.requestCarDataSource = dataSource
        }
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = CRequestCarViewM()
        }
        if self.addCarViewModel == nil {
            self.addCarViewModel = CAddNewCarVM()
        }
    }
    
    private func roundCorners() {
        Threads.performTaskInMainQueue  {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
        }
    }
    
    private func setupView() {
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        self.submitButton.setTitle(StringConstants.Text.submit.localizedString(), for: .normal)
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
    }
    
    private func registerNibs() {
        self.requestCarTableView.register(DropDownCell.self)
        self.requestCarTableView.register(TextInputCell.self)
        self.requestCarTableView.register(TextViewCell.self)
        self.requestCarTableView.register(SideMenuCell.self)
        self.requestCarTableView.register(PriceRangeTextFieldCell.self)
        self.requestCarTableView.register(MileageRangeCell.self)
    }
    
    func setupToolBar(_ textField: UITextField) {
        textField.inputAccessoryView = Helper.getSingleButtonToolBar(buttonTitle: StringConstants.Text.done.localizedString(), selector: #selector(self.doneButtonTapped), target: self)
    }
    
    func showDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        switch indexPath.row {
        case 0:
            if self.vehicleBrandDataSource.count == 0 {
                self.requestVehicleBrandAPI()
                return
            }
            listPopupView.initializeViewWith(title: "Select Brand".localizedString(), arrayList: self.vehicleBrandDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let brandId = response[ConstantAPIKeys.id] as? Int,
                    let brandName = response[ConstantAPIKeys.title] as? String else { return }
                sSelf.requestVehicleModelAPI(brandId: brandId)
                sSelf.requestCarDataSource[indexPath.row].value = brandName
                sSelf.requestCarDataSource[indexPath.row].placeHolder = "\(brandId)"
                sSelf.requestCarTableView.reloadData()
            }
        case 1:
            if self.vehicleModelDataSource.count == 0 {
                
                return
            }
            listPopupView.initializeViewWith(title: "Model Name".localizedString(), arrayList: self.vehicleModelDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let modelName = response[ConstantAPIKeys.title] as? String,
                    let modelId = response[ConstantAPIKeys.id] as? Int else { return }
                sSelf.requestVehicleTypeAPI()
                sSelf.requestCarDataSource[indexPath.row].value = modelName
                sSelf.requestCarDataSource[indexPath.row].placeHolder = "\(modelId)"
                sSelf.requestCarTableView.reloadData()
            }
            
        case 2:
            if self.vehicleTypeDataSource.count == 0 {
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Type".localizedString(), arrayList: self.vehicleTypeDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let carType = response[ConstantAPIKeys.title] as? String,
                    let carId = response[ConstantAPIKeys.id] as? Int else { return }
                sSelf.requestCarDataSource[indexPath.row].value = carType
                sSelf.requestCarDataSource[indexPath.row].placeHolder = "\(carId)"
                sSelf.requestCarTableView.reloadData()
            }
            
        case 6:
            let yearsList = Helper.getListOfYearsFrom(2005)
            listPopupView.initializeViewWith(title: "Car Year".localizedString(), arrayList: yearsList, key: Constants.UIKeys.year) { [weak self] (response) in
                guard let sSelf = self,
                    let carType = response[Constants.UIKeys.year] as? String else { return }
                sSelf.requestCarDataSource[indexPath.row].value = carType
                sSelf.requestCarTableView.reloadData()
            }
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    @objc func doneButtonTapped() {
        self.view.endEditing(true)
    }
    
    //MARK: - API Methods
    func requestVehicleBrandAPI() {
        self.addCarViewModel?.requestVehicleBrandAPI(completion: { (success, vehicleBrands) in
            if success {
                self.vehicleBrandDataSource = vehicleBrands
            }
        })
    }
    
    func requestVehicleModelAPI(brandId: Int) {
        self.addCarViewModel?.requestVehicleModelAPI(brandId: brandId, completion: { (success, vehicleModels) in
            if success {
                self.vehicleModelDataSource = vehicleModels
            }
        })
    }
    
    func requestVehicleTypeAPI() {
        self.addCarViewModel?.requestVehicleTypeAPI(completion: { (success, vehicleTypes) in
            if success {
                self.vehicleTypeDataSource = vehicleTypes
            }
        })
    }
    
    //MARK: - IBActions
    @IBAction func submitButtonTapped(_ sender: Any) {
        self.view.endEditing(true)
        self.viewModel?.requestPostRequestCarAPI(arrData: self.requestCarDataSource, completion: { (success) in
            Threads.performTaskInMainQueue {
                
                self.navigationController?.popToRootViewController(animated: true)
                
            }
        })
    }
    
}
