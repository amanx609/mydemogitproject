//
//  ItemsCoveredCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class ItemsCoveredCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var itemsCoveredCollectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    //MARK: - Variables
    var itemsCoveredDataSource: [CellInfo] = []
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.itemsCoveredCollectionView.contentSize.height
        self.setupCollectionView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupCollectionView() {
        self.registerNibs()
        self.itemsCoveredCollectionView.dataSource = self
        self.itemsCoveredCollectionView.delegate = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //depending upon direction of collection view
        self.itemsCoveredCollectionView?.setCollectionViewLayout(layout, animated: true)
    }
    
    private func registerNibs() {
        self.itemsCoveredCollectionView.register(ItemsCoveredCollectionCell.self)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info,
            let itemsCoveredData = info[Constants.UIKeys.itemsCovered] as? [CellInfo]
        {
            self.itemsCoveredDataSource = itemsCoveredData
            Threads.performTaskInMainQueue {
                self.itemsCoveredCollectionView.reloadData()
                self.collectionViewHeight.constant = self.itemsCoveredCollectionView.contentSize.height
            }
        }
    }
    
}

extension ItemsCoveredCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemsCoveredDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellInfo = self.itemsCoveredDataSource[indexPath.row]
        return self.getCell(collectionView: collectionView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/2 - 5
        return CGSize(width: width, height: self.itemsCoveredDataSource[indexPath.row].height)
    }
    
}

extension ItemsCoveredCell {
    func getCell(collectionView: UICollectionView, indexPath: IndexPath, cellInfo: CellInfo) -> UICollectionViewCell {
        let cell: ItemsCoveredCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(cellInfo: cellInfo)
        return cell
    }
}
