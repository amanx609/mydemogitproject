//
//  ItemsCoveredCollectionCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class ItemsCoveredCollectionCell: BaseCollectionViewCell, NibLoadableView, ReusableView  {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.titleLabel.text = cellInfo.placeHolder
    }
}
