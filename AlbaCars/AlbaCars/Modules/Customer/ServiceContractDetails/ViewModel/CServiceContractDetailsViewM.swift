//
//  CServiceContractDetailsViewM.swift
//  AlbaCars
//
//  Created by Narendra on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol CServiceContractDetailsViewModeling {
    func getServiceContractDetailDataSource(serviceDetailsModel: ServiceDetailsModel) -> [CellInfo]
    func requestGetServiceDetailsAPI(warrantyServiceId: Int, completion: @escaping (Bool,ServiceDetailsModel)-> Void)
}

class CServiceContractDetailsViewM: CServiceContractDetailsViewModeling {
    
    func getServiceContractDetailDataSource(serviceDetailsModel: ServiceDetailsModel) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Service Intervals
        let serviceIntervalCell = CellInfo(cellType: .LeftRightLabelCell, placeHolder: "Service Intervals".localizedString(), value:String.appendText(obj: serviceDetailsModel.serviceIntervals, text: StringConstants.Text.Kms.localizedString()), height: Constants.CellHeightConstants.height_40)
        array.append(serviceIntervalCell)
        
        //Kms Coverage
        let coverageCell = CellInfo(cellType: .LeftRightLabelCell, placeHolder: "Kms Coverage".localizedString(), value:String.appendText(obj: serviceDetailsModel.kmsCoverage, text: StringConstants.Text.Currency.localizedString()), height: Constants.CellHeightConstants.height_40)
        array.append(coverageCell)
        
        //Claim After  Time
        let claimTimeCell = CellInfo(cellType: .LeftRightLabelCell, placeHolder: "Claim After  Time".localizedString(), value:Helper.toString(object: serviceDetailsModel.claimAfterTime), height: Constants.CellHeightConstants.height_40)
        array.append(claimTimeCell)
        
        //Engine Size
        let engineSizeCell = CellInfo(cellType: .LeftRightLabelCell, placeHolder: "Engine Size".localizedString(), value: Helper.toString(object: serviceDetailsModel.engineSize), height: Constants.CellHeightConstants.height_40)
        array.append(engineSizeCell)
        
        //Items Covered
        let itemsCoveredCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Items Covered".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(itemsCoveredCell)
        
        let itemsCell = CellInfo(cellType: .ItemsCoveredCell, placeHolder: "", value: "", info: [Constants.UIKeys.itemsCovered: self.getItemsCoveredDataSource(itemsCovered: serviceDetailsModel.ItemsCovered ?? []) as AnyObject], height: UITableView.automaticDimension)
        array.append(itemsCell)
        
        let garagesCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "List Of Approved Garages".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(garagesCell)
        let approvedGarages = (serviceDetailsModel.ApprovedGarage) ?? []
        for i in 0..<approvedGarages.count {
            
            let garageCell = CellInfo(cellType: .BottomLabelRegularTextCell, placeHolder: Helper.toString(object: approvedGarages[i].garage), value: "", info: nil, height: Constants.CellHeightConstants.height_30)
            array.append(garageCell)
            
        }
        //        //List Of Approved Garages
        
        //
        //        let garageCell = CellInfo(cellType: .BottomLabelRegularTextCell, placeHolder: "ABC Garage, Noor Bank, Dubai".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_30)
        //        array.append(garageCell)
        //
        //        let omniCell = CellInfo(cellType: .BottomLabelRegularTextCell, placeHolder: "Omni Cars, Al Quoz,  Dubai".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_30)
        //        array.append(omniCell)
        //
        //        let sellCell = CellInfo(cellType: .BottomLabelRegularTextCell, placeHolder: "Sell Cars, Umm Sequim, Dubai".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_30)
        //        array.append(sellCell)
        
        return array
    }
    
    func getItemsCoveredDataSource(itemsCovered: [ItemsCovered]) -> [CellInfo] {
        
        var array = [CellInfo]()
        for i in 0..<itemsCovered.count {
            
            var carburatorInfo = [String: AnyObject]()
            carburatorInfo[Constants.UIKeys.status] = true as AnyObject
            let itemsCell = CellInfo(cellType: .ItemsCoveredCollectionCell, placeHolder: Helper.toString(object: itemsCovered[i].item), value: "", info: carburatorInfo, height: Constants.CellHeightConstants.height_40)
            array.append(itemsCell)
        }
        
        return array
    }
    
    func requestGetServiceDetailsAPI(warrantyServiceId: Int, completion: @escaping (Bool, ServiceDetailsModel) -> Void) {
        
        var warrantyDetailsParameters = APIParams()
        warrantyDetailsParameters[ConstantAPIKeys.warrantyServiceId] = warrantyServiceId as AnyObject
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .serviceDetails(param: warrantyDetailsParameters))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData(),
                    let serviceDetails = ServiceDetailsModel(jsonData: resultData)
                    
                {
                    completion(success, serviceDetails)
                }
            }
        }
        
    }
    
}
