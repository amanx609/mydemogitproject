//
//  CUploadCarImagesViewC.swift
//  Albacarios
//
//  Created by Appventurez on 19/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CUploadCarImagesViewC: BaseViewC {
  
  //MARK:- IBOutlets
  @IBOutlet weak var uploadCarImagesCollectionView: UICollectionView!
  @IBOutlet weak var submitButton: UIButton!
  @IBOutlet weak var uploadPhotosImageView: UIImageView!
  @IBOutlet weak var gradientView: UIView!
  
  //MARK:- Variables
  var uploadCarImagesDataSource = [CellInfo]()
  var selectedImages: [UIImage] = []
  var sellCarParams = APIParams()
  var viewModel: CUploadCarImagesViewModeling?
  
  //MARK:- Life-cycle functions
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }
  
  //MARK:- Private methods
  private func setup() {
    self.setupNavigationBarTitle(title: "Upload Car Images".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
    self.recheckVM()
    self.setupCollectionView()
    self.setupView()
    self.setupTapGesture()
    if let carImagesDataSource = self.viewModel?.getCarImagesDataSource(selectedImages: self.selectedImages) {
      self.uploadCarImagesDataSource = carImagesDataSource
      self.uploadCarImagesCollectionView.reloadData()
    }
  }
  
  private func recheckVM() {
    if self.viewModel == nil {
      self.viewModel = CUploadCarImagesViewM()
    }
  }
  
  private func setupCollectionView() {
    self.registerNibs()
    self.uploadCarImagesCollectionView.dataSource = self
    self.uploadCarImagesCollectionView.delegate = self
    self.uploadCarImagesCollectionView.allowsSelection = false
  }
  
  private func registerNibs() {
    self.uploadCarImagesCollectionView.register(UploadCarImagesCell.self)
  }
  
  private func setupView() {
    self.submitButton.setTitle(StringConstants.Text.submit.localizedString(), for: .normal)
    Threads.performTaskInMainQueue {
      self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
      self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
    }
  }
  
  private func setupTapGesture() {
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showImageOptions))
    self.uploadPhotosImageView.addGestureRecognizer(tapGesture)
  }
  
  private func showRequestSubmittedPopup() {
    self.view.endEditing(true)
    if let requestSubmittedPopup = RequestSubmittedPopup.inistancefromNib() {
      requestSubmittedPopup.initializeViewWith { [weak self] (success) in
        if success {
          guard let sSelf = self else { return }
          sSelf.navigationController?.popToRootViewController(animated: true)
        }
      }
      requestSubmittedPopup.showWithAnimated(animated: true)
    }
  }
  
  //MARK: - Public Methods
  func showRemoveCarImageAlert(indexPath: IndexPath) {
    Alert.showAlertWithActionWithCancel(title: StringConstants.Text.AppName.localizedString(), message: "Are you sure you want to delete this image?".localizedString(), actionTitle: StringConstants.Text.Yes.localizedString(), cancelTitle: StringConstants.Text.cancel.localizedString(), action: { (_) in
      //RemoveImageFromSelectedArray
      self.selectedImages.remove(at: indexPath.row)
      //UpdateDataSource
      if let carImagesDataSource = self.viewModel?.getCarImagesDataSource(selectedImages: self.selectedImages) {
        self.uploadCarImagesDataSource = carImagesDataSource
        self.uploadCarImagesCollectionView.reloadData()
      }
    }, cancelAction: { (_) in
      
    })
  }
  
  //MARK:- Selector Methods
  @objc func showImageOptions() {
    ImagePickerManager.sharedInstance.getMultipleImages { (images) in
      guard let imgs = images else { return }
      let imagesCount = imgs.count
      let startImageIndex = self.selectedImages.count
      let newImagesCount = self.selectedImages.count + imagesCount
      if newImagesCount <= Constants.Validations.maxCarImagesCount {
        var index = 0
        for dataSourceIndex in startImageIndex..<newImagesCount {
          if var cellInfo = self.uploadCarImagesDataSource[dataSourceIndex].info {
            cellInfo[Constants.UIKeys.image] = imgs[index]
            cellInfo[Constants.UIKeys.imageStatus] = ImageStatus.uploading as AnyObject
            self.uploadCarImagesDataSource[dataSourceIndex].info = cellInfo
            self.selectedImages.append(imgs[index])
            index += 1
          }
        }
        self.uploadCarImagesCollectionView.reloadData()
      } else {
        Alert.showOkAlert(title: StringConstants.Text.AppName, message: "You cannot upload more than 10 images.")
      }
    }
  }
  
  //MARK: - APIMethods
  private func requestPostSellCarAPI() {
    self.viewModel?.requestPostSellCarAPI(params: self.sellCarParams, images: self.selectedImages, completion: { [weak self] (success) in
      guard let sSelf = self else { return }
      Threads.performTaskInMainQueue {
        sSelf.showRequestSubmittedPopup()
      }
    })
  }
  
  @IBAction func submitButtonTapped(_ sender: Any) {
    self.requestPostSellCarAPI()
  }
}


