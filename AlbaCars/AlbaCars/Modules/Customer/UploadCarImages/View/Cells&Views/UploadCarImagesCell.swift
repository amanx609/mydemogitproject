//
//  UploadCarImagesCollectionCell.swift
//  Albacarios
//
//  Created by Appventurez on 19/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol UploadCarImagesCellDelegate: class, BaseProtocol {
  func didTapCrossButton(cell: UploadCarImagesCell)
}

class UploadCarImagesCell: BaseCollectionViewCell, ReusableView, NibLoadableView {
  
  //MARK:- IBOutlets
  @IBOutlet weak var carImageView: UIImageView!
  @IBOutlet weak var crossButton: UIButton!
  
  //MARK:- Variables
  weak var delegate: UploadCarImagesCellDelegate?
  
  //MARK:- LifeCycle Methods
  override func awakeFromNib() {
    super.awakeFromNib()
    self.setup()
  }
  
  //MARK:- Private Methods
  private func setup() {
    self.carImageView.layer.cornerRadius = Constants.UIConstants.borderWidth_17
  }
  
  //MARK: - Public Methods
  func configureView(cellInfo: CellInfo) {
    if let info = cellInfo.info {
      if let img = info[Constants.UIKeys.image] as? UIImage {
        self.carImageView.image = img
        self.crossButton.isHidden = false
      } else if let placeholderImg = info[Constants.UIKeys.placeholderImage] as? UIImage {
        self.carImageView.image = placeholderImg
        self.crossButton.isHidden = true
      }
    }
  }
  
  //MARK:- IBActions
  @IBAction func crossButtonTapped(_ sender: Any) {
    if let delegate = self.delegate {
      delegate.didTapCrossButton(cell: self)
    }
  }
}
