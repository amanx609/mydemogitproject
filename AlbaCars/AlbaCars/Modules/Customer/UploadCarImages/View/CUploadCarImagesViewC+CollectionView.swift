//
//  CUploadCarImagesViewC+CollectionView.swift
//  Albacarios
//
//  Created by Appventurez on 19/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension CUploadCarImagesViewC: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.uploadCarImagesDataSource.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell: UploadCarImagesCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
    let cellInfo = self.uploadCarImagesDataSource[indexPath.item]
    cell.delegate = self
    cell.configureView(cellInfo: cellInfo)
    return cell
  }
}

extension CUploadCarImagesViewC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let width = self.uploadCarImagesCollectionView.bounds.size.width - 5.0
    return CGSize(width: width/2, height: 110.0)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets.zero
  }
}

extension CUploadCarImagesViewC: UploadCarImagesCellDelegate {
  func didTapCrossButton(cell: UploadCarImagesCell) {
    guard let indexPath = self.uploadCarImagesCollectionView.indexPath(for: cell) else { return }
    self.showRemoveCarImageAlert(indexPath: indexPath)
  }
}
