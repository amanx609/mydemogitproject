//
//  UploadCarImagesViewModel.swift
//  Albacarios
//
//  Created by Appventurez on 19/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol CUploadCarImagesViewModeling: BaseVModeling {
  func getCarImagesDataSource(selectedImages: [UIImage]) -> [CellInfo]
  func requestPostSellCarAPI(params: APIParams, images: [UIImage], completion: @escaping (Bool)-> Void)
}

class CUploadCarImagesViewM: BaseViewM, CUploadCarImagesViewModeling {
  func getCarImagesDataSource(selectedImages: [UIImage]) -> [CellInfo] {
    
    var array = [CellInfo]()
    
    for image in selectedImages {
      //CarImagesCell(
      var carImagesInfo = [String: AnyObject]()
      carImagesInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "upload_car_placeholder") as AnyObject
      carImagesInfo[Constants.UIKeys.image] = image as AnyObject
      let carImagesCell = CellInfo(cellType: .UploadCarImages, placeHolder: "", value: "", info: carImagesInfo, height: Constants.CellHeightConstants.height_0)
      array.append(carImagesCell)
    }
    
    //RemainingImages
    let remainingImagesCount = Constants.Validations.maxCarImagesCount - selectedImages.count
    for _ in 1...remainingImagesCount {
      //CarImagesCell(
      var carImagesInfo = [String: AnyObject]()
      carImagesInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "upload_car_placeholder") as AnyObject
      let carImagesCell = CellInfo(cellType: .UploadCarImages, placeHolder: "", value: "", info: carImagesInfo, height: Constants.CellHeightConstants.height_0)
      array.append(carImagesCell)
    }
    return array
  }
  
  //MARK: - APIMethods
  func requestPostSellCarAPI(params: APIParams, images: [UIImage], completion: @escaping (Bool)-> Void) {
    if self.validateImagesData(images: images) {
      self.uploadImagesToS3(images: images) { (imagesUrl) in
        var sellVehicleParams = params
        sellVehicleParams[ConstantAPIKeys.images] = imagesUrl as AnyObject
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .postSellVehicle(param: sellVehicleParams))) { (response, success) in
          if success {
            completion(success)
          }
        }
      }
    }
  }
  
  //MARK: - Private Methods
  //DataValidationsBeforeAPICall
  private func validateImagesData(images: [UIImage]) -> Bool {
    var isValid = true
    if images.count == 0 {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select atleast one image of car.".localizedString())
      isValid = false
    }
    return isValid
  }
  
  private func uploadImagesToS3(images: [UIImage], completion: @escaping ([String])-> Void) {
    S3Manager.sharedInstance.uploadImages(images: images, imageQuality: .medium, progress: nil) { (imagesUrl, imageName, error) in
      guard let imageUrlArray = imagesUrl as? [String] else {
        return
      }
      completion(imageUrlArray)
    }
  }
}
