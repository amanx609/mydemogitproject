//
//  CSellCarFormViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CSellCarFormViewC: BaseViewC {
  
  //MARK: - IBOutlets
  
  @IBOutlet weak var sellCarTableView: UITableView!
  @IBOutlet weak var nextButton: UIButton!
  @IBOutlet weak var gradientView: UIView!
  
  //MARK: - Properties
  var viewModel: CSellCarFormViewModeling?
  var addCarViewModel: CAddNewCarViewModeling?
  var addSellCarDataSource: [CellInfo] = []
  var vehicleBrandDataSource: [[String: AnyObject]] = []
  var vehicleTypeDataSource: [[String: AnyObject]] = []
  var vehicleModelDataSource: [[String: AnyObject]] = []
  
  //MARK: - LifeCycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setup()
  }
  
  //MARK: - Private Methods
  private func setup() {
    setupView()
    roundCorners()
    setupViewModel()
    registerNibs()
    setupTableView()
    self.setupNavigationBarTitle(title: StringConstants.Text.sellYourCar.localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
    self.requestVehicleBrandAPI()
  }
  
  private func setupTableView() {
    self.sellCarTableView.separatorStyle = .none
    sellCarTableView.backgroundColor = UIColor.white
    sellCarTableView.delegate = self
    sellCarTableView.dataSource = self
    if let dataSource = self.viewModel?.getSellCarFormDataSource() {
      self.addSellCarDataSource = dataSource
    }
  }
  
  private func setupViewModel() {
    if self.viewModel == nil {
      self.viewModel = CSellCarFormViewM()
    }
    if self.addCarViewModel == nil {
      self.addCarViewModel = CAddNewCarVM()
    }
  }
  
  private func roundCorners() {
    Threads.performTaskInMainQueue  {
      self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
    }
  }
  
  private func setupView() {
    self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
    self.nextButton.setTitle(StringConstants.Text.next.localizedString(), for: .normal)
    self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
  }
  
  private func registerNibs() {
    self.sellCarTableView.register(DropDownCell.self)
    self.sellCarTableView.register(TextInputCell.self)
    self.sellCarTableView.register(TextViewCell.self)
    
  }
  
  func showDropDownAt(indexPath: IndexPath) {
    self.view.endEditing(true)
    guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
    switch indexPath.row {
    case 0:
      if self.vehicleBrandDataSource.count == 0 {
        self.requestVehicleBrandAPI()
        return
      }
      listPopupView.initializeViewWith(title: "Select Brand".localizedString(), arrayList: self.vehicleBrandDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
        guard let sSelf = self,
          let brandId = response[ConstantAPIKeys.id] as? Int,
          let brandName = response[ConstantAPIKeys.title] as? String else { return }
        sSelf.requestVehicleModelAPI(brandId: brandId)
        sSelf.addSellCarDataSource[indexPath.row].value = brandName
        sSelf.addSellCarDataSource[indexPath.row].placeHolder = "\(brandId)"
        sSelf.sellCarTableView.reloadData()
      }
    case 1:
      if self.vehicleModelDataSource.count == 0 {
        return
      }
      listPopupView.initializeViewWith(title: "Model Name".localizedString(), arrayList: self.vehicleModelDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
        guard let sSelf = self,
          let modelName = response[ConstantAPIKeys.title] as? String,
          let modelId = response[ConstantAPIKeys.id] as? Int else { return }
        sSelf.addSellCarDataSource[indexPath.row].value = modelName
        sSelf.addSellCarDataSource[indexPath.row].placeHolder = "\(modelId)"
        sSelf.sellCarTableView.reloadData()
      }
    default:
      break
    }
    listPopupView.showWithAnimated(animated: true)
  }
  
  //MARK: - API Methods
  func requestVehicleBrandAPI() {
    self.addCarViewModel?.requestVehicleBrandAPI(completion: { (success, vehicleBrands) in
      if success {
        self.vehicleBrandDataSource = vehicleBrands
      }
    })
  }
  
  func requestVehicleModelAPI(brandId: Int) {
    self.addCarViewModel?.requestVehicleModelAPI(brandId: brandId, completion: { (success, vehicleModels) in
      if success {
        self.vehicleModelDataSource = vehicleModels
      }
    })
  }
  
  //MARK: - IBActions
  @IBAction func nextButtonTapped(_ sender: Any) {
    if let isValid = self.viewModel?.validatePostSellCarData(arrData: self.addSellCarDataSource), isValid {
      if let sellCarDataSource = self.viewModel?.getPostSellCarParams(arrData: self.addSellCarDataSource) {
        let uploadCarImagesViewC = DIConfigurator.sharedInstance.getUploadCarImagesVC()
        uploadCarImagesViewC.sellCarParams = sellCarDataSource
        self.navigationController?.pushViewController(uploadCarImagesViewC, animated: true)
      }
    }
  }
}
