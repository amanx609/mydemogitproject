//
//  CSellCarFormViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import Foundation
import UIKit

extension CSellCarFormViewC: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.addSellCarDataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellInfo = self.addSellCarDataSource[indexPath.row]
    return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return self.addSellCarDataSource[indexPath.row].height
  }
}

extension CSellCarFormViewC {
  
  func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
    guard let cellType = cellInfo.cellType else { return UITableViewCell() }
    switch cellType {
    case .DropDownCell:
      let cell: DropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate = self
      cell.configureView(cellInfo: cellInfo)
      return cell
    case .TextInputCell:
      let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate = self
      cell.placeholderView.isHidden = true
      cell.configureView(cellInfo: cellInfo)
      return cell
      case .TextViewCell:
      let cell: TextViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate =  self
      cell.configureView(cellInfo: cellInfo)
      return cell
    default:
      return UITableViewCell()
    }
  }
}

//MARK: - DropDownCellDelegate
extension CSellCarFormViewC: DropDownCellDelegate {
  func didTapDropDownCell(cell: DropDownCell) {
    guard let indexPath = self.sellCarTableView.indexPath(for: cell) else { return }
    self.showDropDownAt(indexPath: indexPath)
  }
}

//MARK: - TextInputCellDelegate
extension CSellCarFormViewC: TextInputCellDelegate {
  func tapNextKeyboard(cell: TextInputCell) {
    guard var indexPath = self.sellCarTableView.indexPath(for: cell) else { return }
    indexPath.row += 1
    if let cell = self.sellCarTableView.cellForRow(at: indexPath) as? TextInputCell {
      cell.inputTextField.becomeFirstResponder()
    } else {
      self.view.endEditing(true)
    }
  }
  
  func didChangeText(cell: TextInputCell, text: String) {
    guard let indexPath = self.sellCarTableView.indexPath(for: cell) else { return }
    self.addSellCarDataSource[indexPath.row].value = text
  }
}

//MARK: - TextViewCellDelegate
extension CSellCarFormViewC: TextViewCellDelegate {
    func didChangeText(cell: TextViewCell, text: String) {
        guard let indexPath = self.sellCarTableView.indexPath(for: cell) else { return }
        self.addSellCarDataSource[indexPath.row].value = text
    }
}
