//
//  CSellCarFormViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol CSellCarFormViewModeling: BaseVModeling {
    func getSellCarFormDataSource() -> [CellInfo]
    func validatePostSellCarData(arrData: [CellInfo]) -> Bool
    func getPostSellCarParams(arrData: [CellInfo]) -> APIParams
}

class CSellCarFormViewM: CSellCarFormViewModeling {
    
    func getSellCarFormDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Select Car Brand Cell
        let brandCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: StringConstants.Text.selectBrand.localizedString(), height: Constants.CellHeightConstants.height_80)
        array.append(brandCell)
        
        // car name Cell
        let nameCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value:StringConstants.Text.modelName.localizedString(), height: Constants.CellHeightConstants.height_80)
        array.append(nameCell)
        
        // Model Year
        var modelYearInfo = [String: AnyObject]()
        modelYearInfo[Constants.UIKeys.inputType] = TextInputType.modelYear as AnyObject
        let modelYearCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.modelYear.localizedString(), value: "", info: modelYearInfo,  height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.numberPad)
        array.append(modelYearCell)
        
        // Odometer
        var odometerInfo = [String: AnyObject]()
        odometerInfo[Constants.UIKeys.inputType] = TextInputType.odometer as AnyObject
        let odometerCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.odometer.localizedString(), value: "", info: odometerInfo, height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.numberPad)
        array.append(odometerCell)
        
        // engine Size
        var engineSizeInfo = [String: AnyObject]()
        engineSizeInfo[Constants.UIKeys.inputType] = TextInputType.mileage as AnyObject
        let engineSizeCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.engineSize.localizedString(), value: "", info: engineSizeInfo, height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.decimalPad)
        array.append(engineSizeCell)
        
        // Expected Price
        var expectedPriceInfo = [String: AnyObject]()
        expectedPriceInfo[Constants.UIKeys.inputType] = TextInputType.expectedPrice as AnyObject
        let expectedPriceCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.expectedPrice.localizedString(), value: "", info: expectedPriceInfo, height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.numberPad)
        array.append(expectedPriceCell)
       
        // Additional Information (Optional)
        var additionalInformationInfo = [String: AnyObject]()
        additionalInformationInfo[Constants.UIKeys.titlePlaceholder] = "Additional Information".localizedString() as AnyObject
        additionalInformationInfo[Constants.UIKeys.subTitlePlaceholder] = "(Optional)".localizedString() as AnyObject
        let textViewCell = CellInfo(cellType: .TextViewCell, placeHolder: "", value: "", info: additionalInformationInfo, height: UITableView.automaticDimension)
        array.append(textViewCell)
        
        return array
    }
    
    
      //DataValidationsBeforeAPICall
       func validatePostSellCarData(arrData: [CellInfo]) -> Bool {
        var isValid = true
        if let carBrand = arrData[0].placeHolder, carBrand.isEmpty {
          Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select car brand.".localizedString())
          isValid = false
        } else if let carModel = arrData[1].placeHolder, carModel.isEmpty  {
          Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select car model.".localizedString())
          isValid = false
        } else if let carYear = arrData[2].value, carYear.isEmpty {
          Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter car model year.".localizedString())
          isValid = false
        } else if let carYear = arrData[2].value, let intCarYear = Int(carYear), intCarYear < Constants.Validations.minCarYear {
            let alertText = "The model year cannot be less than".localizedString() + " \(Constants.Validations.minCarYear)."
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: alertText)
          isValid = false
        } else if let carYear = arrData[2].value, let intCarYear = Int(carYear), let validationMaxYear = Constants.Validations.maxCarYear, intCarYear > validationMaxYear + 2{
            let alertText = "The model year cannot be greater than".localizedString() + " \(validationMaxYear)."
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: alertText)
          isValid = false
        } else if let odoMeter = arrData[3].value, odoMeter.isEmpty {
          Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter odometer.".localizedString())
          isValid = false
        } else if let odoMeter = arrData[3].value, let intOdoValue = Int(odoMeter), intOdoValue < Constants.Validations.minimumMileage {
            let alertText = "The odometer cannot be less than".localizedString() + " \(Constants.Validations.minimumMileage) " + "Kms."
          Alert.showOkAlert(title: StringConstants.Text.AppName, message: alertText)
          isValid = false
        } else if let odoMeter = arrData[3].value, let intOdoValue = Int(odoMeter), intOdoValue > Constants.Validations.maximumMileage {
            let alertText = "The odometer cannot be greater than".localizedString() + " \(Constants.Validations.maximumMileage) " + "Kms."
          Alert.showOkAlert(title: StringConstants.Text.AppName, message: alertText)
          isValid = false
        } else if let engineSize = arrData[4].value, engineSize.isEmpty {
          Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter engine size.".localizedString())
          isValid = false
        } else if let engineSize = arrData[4].value, let intEngineSizeValue = Int(engineSize), intEngineSizeValue > Constants.Validations.maxEngineSize {
            let alertText = "The engine size cannot be greater than".localizedString() + " \(Constants.Validations.maxEngineSize)."
          Alert.showOkAlert(title: StringConstants.Text.AppName, message: alertText)
          isValid = false
        } else if let expectedPrice = arrData[5].value, expectedPrice.isEmpty {
          Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter expected price.".localizedString())
          isValid = false
        } else if let expectedPrice = arrData[5].value, let intExpectedPriceValue = Int(expectedPrice), intExpectedPriceValue > Constants.Validations.maxExpectedPrice {
            let alertText = "The expected price cannot be greater than AED".localizedString() + " \(Constants.Validations.maxExpectedPrice)."
          Alert.showOkAlert(title: StringConstants.Text.AppName, message: alertText)
          isValid = false
        }
        return isValid
      }
      
      
       func getPostSellCarParams(arrData: [CellInfo])-> APIParams {
       var params: APIParams = APIParams()
       params[ConstantAPIKeys.brand] = arrData[0].placeHolder as AnyObject
       params[ConstantAPIKeys.carModel] = arrData[1].placeHolder as AnyObject
       params[ConstantAPIKeys.brandName] = arrData[0].value as AnyObject
       params[ConstantAPIKeys.modelName] = arrData[1].value as AnyObject
       params[ConstantAPIKeys.carYear] = arrData[2].value as AnyObject
       params[ConstantAPIKeys.mileage] = arrData[3].value as AnyObject
       params[ConstantAPIKeys.engine] = arrData[4].value as AnyObject
       params[ConstantAPIKeys.price] = arrData[5].value as AnyObject
       params[ConstantAPIKeys.additionalInformation] = arrData[6].value as AnyObject
        return params
      }
    
}
