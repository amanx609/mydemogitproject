//
//  PackageWarrantyTableCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol PackageWarrantyTableCellDelegate: class {
    func didTapBuyNow(cell: PackageWarrantyTableCell)
    func didTapViewDetails(cell: PackageWarrantyTableCell)
    func didTapStepperButton(cell: PackageWarrantyTableCell, value: String)
    func didChangePackagePrice(cell: PackageWarrantyTableCell, value: String)
    
}

class PackageWarrantyTableCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var packageInfoButton: UIButton!
    @IBOutlet weak var packageInfoImageView: UIImageView!
    @IBOutlet weak var numberOfYearsLabel: UILabel!
    @IBOutlet weak var stepperNegativeButton: UIButton!
    @IBOutlet weak var stepperPositiveButton: UIButton!
    @IBOutlet weak var stepperValueLabel: UILabel!
    @IBOutlet weak var info1TitleLabel: UILabel!
    @IBOutlet weak var info1ValueLabel: UILabel!
    @IBOutlet weak var info2TitleLabel: UILabel!
    @IBOutlet weak var info2ValueLabel: UILabel!
    @IBOutlet weak var info3TitleLabel: UILabel!
    @IBOutlet weak var info3ValueLabel: UILabel!
    @IBOutlet weak var info4TitleLabel: UILabel!
    @IBOutlet weak var info4ValueLabel: UILabel!
    @IBOutlet weak var packagePriceLabel: UILabel!
    @IBOutlet weak var packagePriceButton: UIButton!
    @IBOutlet weak var viewDetailsButton: UIButton!
    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var info1StackView: UIStackView!
    @IBOutlet weak var vatLabel: UILabel!
    @IBOutlet weak var info2StackView: UIStackView!
    
    //MARK: - Variables
    weak var delegate: PackageWarrantyTableCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    private func setup() {
        self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_7)
               self.shadowView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        self.viewDetailsButton.makeLayer(color: .redButtonColor, boarderWidth: 1.0, round: 0.0)
        self.packagePriceButton.makeLayer(color: .redButtonColor, boarderWidth: 2.0, round: 0.0)
        
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.vatLabel.text = Helper.toString(object: Helper.getVatPercentage())

        if let info = cellInfo.info {
            if let model = info[Constants.UIKeys.warrantyModel] as? WarrantyModel {
                self.info1ValueLabel.text = Helper.toInt(model.limitPerClaim) == 0 ? Helper.toString(object: model.limitPerClaim) : "AED \(Helper.toString(object: model.limitPerClaim))"
                self.info2ValueLabel.text = Helper.toInt(model.annualClaimLimit) == 0 ? Helper.toString(object: model.annualClaimLimit) : "AED \(Helper.toString(object: model.annualClaimLimit))"
                self.info3ValueLabel.text = Helper.toInt(model.coverage) == 0 ? Helper.toString(object: model.coverage) : "\(Helper.toString(object: model.coverage)) Kms"
                self.info4ValueLabel.text = "\(Helper.toInt(model.maxKmsPerYear)) Kms"
                if Helper.toInt(model.warrantyType) == 0 {
                    self.packageInfoButton.setTitle(StringConstants.Text.gold, for: .normal)
                    self.packageInfoButton.setBackgroundImage(UIImage(named: "goldBackground"), for: .normal)

                } else if Helper.toInt(model.warrantyType) == 1 {
                    self.packageInfoButton.setTitle(StringConstants.Text.silver, for: .normal)
                    self.packageInfoButton.setBackgroundImage(UIImage(named: "silverBackground"), for: .normal)

                }
                self.packagePriceButton.setTitle("AED \(Helper.toString(object: info[ConstantAPIKeys.price]))", for: .normal)
            }
            if let image = info[Constants.UIKeys.cellIcon] as? UIImage {
                self.packageInfoImageView.image = image
            }

        }
    }
    
    func configureViewServiceContract(cellInfo: CellInfo) {
        self.vatLabel.text = Helper.toString(object: Helper.getVatPercentage())

        if let info = cellInfo.info {
            self.info1StackView.isHidden = true
            self.info2StackView.isHidden = true
            
            if let model = info[Constants.UIKeys.warrantyModel] as? WarrantyModel {
                self.info3TitleLabel.text = "Service Intervals: "
                self.info3ValueLabel.text = Helper.toInt(model.serviceIntervals) == 0 ? Helper.toString(object: model.serviceIntervals) : "\(Helper.toString(object: model.serviceIntervals)) Kms"
                self.info4TitleLabel.text = "Kms Coverage: "
                self.info4ValueLabel.text = Helper.toInt(model.kmsCoverage) == 0 ? Helper.toString(object: model.kmsCoverage) : "\(Helper.toString(object: model.kmsCoverage)) Kms"
                
                if Helper.toInt(model.serviceType) == 0 {
                    self.packageInfoButton.setTitle(StringConstants.Text.gold, for: .normal)
                    self.packageInfoButton.setBackgroundImage(UIImage(named: "goldBackground"), for: .normal)

                } else if Helper.toInt(model.serviceType) == 1 {
                    self.packageInfoButton.setTitle(StringConstants.Text.silver, for: .normal)
                    self.packageInfoButton.setBackgroundImage(UIImage(named: "silverBackground"), for: .normal)

                }
                self.packagePriceButton.setTitle("AED \(Helper.toString(object: info[ConstantAPIKeys.price]))", for: .normal)
            }
            if let image = info[Constants.UIKeys.cellIcon] as? UIImage {
                self.packageInfoImageView.image = image
            }

        }
    }
    
    //MARK: - IBActions
    
    @IBAction func tapBuyNowButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapBuyNow(cell: self)
        }
    }
    
    @IBAction func tapViewDetailsButton(_ sender: Any) {
         if let delegate = self.delegate {
                   delegate.didTapViewDetails(cell: self)
               }
    }
    
    @IBAction func tapStepperPositiveButton(_ sender: Any) {
        
            self.stepperValueLabel.text = "\(Int(self.stepperValueLabel.text ?? "0")! + 1)"
        
        if let delegate = self.delegate, let value = self.stepperValueLabel.text {
            delegate.didTapStepperButton(cell: self, value: value)
        }
    }
    
    @IBAction func tapStepperNegativeButton(_ sender: Any) {
        if Int(self.stepperValueLabel.text ?? "1") != 1 {
            self.stepperValueLabel.text = "\(Int(self.stepperValueLabel.text ?? "0")! - 1)"
        }
      if let delegate = self.delegate, let value = self.stepperValueLabel.text {
        delegate.didTapStepperButton(cell: self, value: value)
           }
    }
    
    @IBAction func tapPackagePriceButton(_ sender: Any) {
        
    }
    
}
