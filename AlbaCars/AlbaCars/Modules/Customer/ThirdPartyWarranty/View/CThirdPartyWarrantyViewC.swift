//
//  ThirdPartyWarrantyViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class CThirdPartyWarrantyViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var thirdPartyTableView: UITableView!
    // @IBOutlet weak var bottomMarkerViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var thirdPartyCollectionView: UICollectionView!
    @IBOutlet weak var noDataView: UIView!
    
    //MARK: - Variables
    var thirdPartyDataSource: [CellInfo] = []
    var thirdPartyHeaderDataSource: [[String: AnyObject]] = []
    var viewModel: CThirdPartyWarrantyVModeling?
    var chooseCarViewModel: ChooseCarFormViewM?
    var carListDataSource: [[String: AnyObject]] = []
    var selectedIndex = 0
    var carDetails: ChooseCarModel?
    
    //MARK: - LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    deinit {
        print("deinit CThirdPartyWarrantyViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Third Party Warranty".localizedString(),barColor: .redButtonColor,titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setupCollectionView()
        self.setupTableView()
        self.requestAgencyListAPI()
        self.requestChooseCarAPI()
        if self.thirdPartyDataSource.count > 0 {
            scrollAndLoadData(indexPath: IndexPath(item: 0, section: 0))
        }
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = CThirdPartyWarrantyViewM()
        }
        
        if self.chooseCarViewModel == nil {
            self.chooseCarViewModel = ChooseCarFormViewM()
        }
    }
    
    private func setupTableView() {
        self.thirdPartyTableView.delegate = self
        self.thirdPartyTableView.dataSource = self
        self.thirdPartyTableView.separatorStyle = .none
        self.thirdPartyTableView.allowsSelection = false
        
    }
    
    private func setupCollectionView() {
        self.registerNibs()
        self.thirdPartyCollectionView.delegate = self
        self.thirdPartyCollectionView.dataSource = self
        self.thirdPartyCollectionView.showsHorizontalScrollIndicator = false
        
    }
    
    private func registerNibs() {
        self.thirdPartyTableView.register(DropDownCell.self)
        self.thirdPartyTableView.register(BottomLabelCell.self)
        self.thirdPartyTableView.register(PackageWarrantyTableCell.self)
        self.thirdPartyCollectionView.register(headerCollectionCell.self)
        
    }
    
    func loadDataSource(position: Int) {
        self.selectedIndex = position
        Threads.performTaskInMainQueue {
            self.thirdPartyDataSource.removeAll()
            self.thirdPartyTableView.reloadData()
            self.requestWarrantyListAPI()
        }
    }
    
    //MARK: - API Methods
    private func requestChooseCarAPI() {
        self.chooseCarViewModel?.requestChooseCarAPI(completion: { [weak self] (success, carList) in
            guard let sSelf = self else { return }
            if success {
                sSelf.carListDataSource = carList
            }
        })
    }
    
    func requestAgencyListAPI() {
        
        self.viewModel?.requestAgencyListAPI(agencyType: AgencyType.Warranty.rawValue, makeID: Helper.toInt(carDetails?.make_id), completion: { [weak self] (success, agencyList) in
            guard let sSelf = self else { return }
            if success {
                
                sSelf.thirdPartyHeaderDataSource = agencyList
                Threads.performTaskInMainQueue {
                    sSelf.thirdPartyCollectionView.reloadData()
                }
                if agencyList.count > 0{
                    sSelf.noDataView.isHidden = true
                    sSelf.requestWarrantyListAPI()
                    
                }else{
                    sSelf.noDataView.isHidden = false
                }
            }
        })
    }
    
     func requestWarrantyListAPI() {
        
        if thirdPartyHeaderDataSource.count > 0 {
            
            let id = Helper.toInt(self.thirdPartyHeaderDataSource[selectedIndex][ConstantAPIKeys.id])
            
            self.viewModel?.requestGetServiceContractAPI(agencyId: id, makeID: Helper.toInt(carDetails?.make_id), completion: { [weak self] (success, warrantyList) in
                guard let sSelf = self else { return }
                if success {
                    //self.thirdPartyTableView = agencyList
                    Threads.performTaskInMainQueue {
                        let carName = "\(Helper.toString(object: sSelf.carDetails?.title)),  \(Helper.toString(object: sSelf.carDetails?.plateNumber))"
                        if let dataSource = sSelf.viewModel?.getThirdPartyWarrantyTableDataSource(thirdPartyModel: warrantyList, carName: carName) {
                            sSelf.thirdPartyDataSource = dataSource
                        }
                        if warrantyList.count > 0{
                            sSelf.noDataView.isHidden = true
                        }else{
                            sSelf.noDataView.isHidden = false
                        }
                        sSelf.thirdPartyTableView.reloadData()
                    }
                }
            })
        }
    }
    
    func requestBuyWarrantyAPI(params: APIParams) {
        
        self.viewModel?.requestBuyWarrantyAPI(params: params, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            if success {
                
                Threads.performTaskInMainQueue {
                    sSelf.navigationController?.popToRootViewController(animated: true)
                    Alert.showOkAlert(title: StringConstants.Text.AppName, message: StringConstants.Text.purchasedSuccessfully.localizedString())
                }
            }
        })
    }
    
}
