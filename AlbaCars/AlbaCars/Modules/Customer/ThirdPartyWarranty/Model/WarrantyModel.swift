//
//  WarrantyModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/16/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class WarrantyModel: BaseCodable {
    
    var id: Int?
    var limitPerClaim: Int?
    var annualClaimLimit: String?
    var maxKmsPerYear: Int?
    var warrantyType: Int?
    var image: String?
    var coverage: String?
    var price: Int?
    var kmsCoverage: String?
    var serviceIntervals: Int?
    var serviceType: Int?
}

class WarrantyList: BaseCodable {
    var warranty: [WarrantyModel]?
    
    private enum CodingKeys: String, CodingKey {
      case warranty = "result"
    }
}
