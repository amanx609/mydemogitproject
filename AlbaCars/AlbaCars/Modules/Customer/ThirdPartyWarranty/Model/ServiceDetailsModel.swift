//
//  ServiceDetailsModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/20/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class ServiceDetailsModel: BaseCodable {
    
    var id: Int?
    var serviceType: Int?
    var image: String?
    var price: Int?
    var serviceIntervals: Int?
    var maxCurrentKms: Int?
    var kmsCoverage: Int?
    var claimAfterTime: String?
    var engineSize: Double?
    var ItemsCovered: [ItemsCovered]?
    var ApprovedGarage: [ApprovedGarage]?
    
}

