//
//  CarFinanceModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/20/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class CCarFinanceModel: BaseCodable {
    
    var card: String?
    var cardLimit: String?
    var companyLandline: String?
    var existingLoan: String?
    var noEmployeesInCompany: String?
    var pastLegalCases: String?
    var salaryPaymentMethod: String?
    var totalInstallments: String?
    var vehicleId: String?
    
   
}
