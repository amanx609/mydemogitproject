//
//  WarrantyDetailsModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class WarrantyDetailsModel: BaseCodable {
    
    var id: Int?
    var limitPerClaim: Int?
    var annualClaimLimit: String?
    var maxKmsPerYear: Int?
    var warrantyType: Int?
    var image: String?
    var coverage: String?
    var price: Int?
    var maxCurrentKms: Int?
    var coverageKmsPerYear: Int?
    var claimAfterTime: String?
    var ItemsCovered: [ItemsCovered]?
    var ApprovedGarage: [ApprovedGarage]?
}

class ItemsCovered: BaseCodable {
    var item: String
}

class ApprovedGarage: BaseCodable {
    var garage: String?
}
