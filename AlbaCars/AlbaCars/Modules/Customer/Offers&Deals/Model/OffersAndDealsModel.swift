//
//  OffersAndDealsModel.swift
//  AlbaCars
//
//  Created by Narendra on 1/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

//OffersAndDeals

class OffersAndDeals: BaseCodable {
 
    //Variables
    var offerId: Int?
    var offerTitle: String?
    var offerImage: String?
    var offerText: String?
    var vehicleId: Int?
    var dealsLeft: Int?
    var timeLeft: Int?
    var price: Int?
    var timeLeftFormat: String?
    func getPrice()-> String {
        return "AED ".localizedString() + Helper.toString(object: price)
    }
    
    private enum CodingKeys:String, CodingKey {
      case offerId, offerTitle,offerImage,offerText,vehicleId,dealsLeft,timeLeft,price //Server keys are same as we define here
    }
     
    required init(from decoder: Decoder) throws {
      let values = try! decoder.container(keyedBy: CodingKeys.self)
      self.offerId = try values.decodeIfPresent(Int.self, forKey: .offerId)
      self.offerTitle = try values.decodeIfPresent(String.self, forKey: .offerTitle)
      self.offerImage = try values.decodeIfPresent(String.self, forKey: .offerImage)
      self.offerText = try values.decode(String.self, forKey: .offerText)
      self.vehicleId = try values.decode(Int.self, forKey: .vehicleId)
      self.dealsLeft = try values.decode(Int.self, forKey: .dealsLeft)
      self.timeLeft = try values.decode(Int.self, forKey: .timeLeft)
      self.price = try values.decode(Int.self, forKey: .price)
    }
}

class OffersAndDealsList: BaseCodable {
    var offersAndDeals: [OffersAndDeals]?
    
    private enum CodingKeys: String, CodingKey {
      case offersAndDeals = "data"
    }
}

