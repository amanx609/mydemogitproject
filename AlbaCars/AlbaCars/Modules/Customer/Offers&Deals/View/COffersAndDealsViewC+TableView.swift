//
//  COffersAndDealsViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 1/9/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension COffersAndDealsViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.offersAndDeals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let offersAndDeals = self.offersAndDeals[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath,offersAndDeals:offersAndDeals)
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//      guard let lastVisibleRow = self.offersTableView.indexPathsForVisibleRows?.last?.row else { return }
//      if (self.offersAndDeals.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
//        self.requestOffersAndDealsAPI(page: self.nextPageNumber)
//      }
//    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if abs(maximumOffset) - abs(currentOffset) <= -40 && self.nextPageNumber != 0 {
            self.requestOffersAndDealsAPI(page: self.nextPageNumber)
        }
    }

}

extension COffersAndDealsViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath,offersAndDeals:OffersAndDeals) -> UITableViewCell {
        let cell: OffersAndDealsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(offersAndDeals: offersAndDeals)
        cell.delegate = self
        return cell
    }
}

//MARK: - OffersAndDealsCellCellDelegate
extension COffersAndDealsViewC: OffersAndDealsCellCellDelegate {
    
    func didTapBuyNow(cell: OffersAndDealsCell) {
        
        guard let indexPath = self.offersTableView.indexPath(for: cell) else { return }
        let offersAndDeals = self.offersAndDeals[indexPath.row]
        
        let paymentDetails = PaymentDetailsData(viewController: self, totalAmmount: Helper.toDouble(offersAndDeals.price), referenceId: Helper.toInt(offersAndDeals.offerId), paymentServiceCategoryId: PaymentServiceCategoryId.offersDeals,vatAmmount: 0)
        PaymentManager.sharedInstance.initiatePayment(paymentDetailsData: paymentDetails) { (success, paymentID) in
            
            if success {
                Alert.showOkAlert(title: StringConstants.Text.AppName, message: StringConstants.Text.purchasedSuccessfully.localizedString())
            }
        }
    }
}
