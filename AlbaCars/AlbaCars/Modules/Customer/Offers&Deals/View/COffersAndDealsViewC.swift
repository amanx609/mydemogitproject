//
//  COffersAndDealsViewC.swift
//  AlbaCars
//
//  Created by Narendra on 1/9/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class COffersAndDealsViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var offersTableView: UITableView!
    
    //MARK: - Variables
    var viewModel: COffersAndDealsViewModeling?
    var offersAndDeals: [OffersAndDeals] = []
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    var timer:Timer?
    var tim = 4200
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer?.invalidate()
        timer =  nil
    }
    
    deinit {
        print("deinit COffersAndDealsViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupTableView()
        self.setupNavigationBarTitle(title: "Offers & Deals", leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.requestOffersAndDealsAPI(page: 1)
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(timeLeft), userInfo: nil, repeats: true)
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.offersTableView.backgroundColor = UIColor.white
        self.offersTableView.delegate = self
        self.offersTableView.dataSource = self
        self.offersTableView.separatorStyle = .none
        //self.carListTableView.allowsSelection = false
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = COffersAndDealsViewM()
        }
    }
    
    private func registerNibs() {
        self.offersTableView.register(LeftRightLabelCell.self)
        self.offersTableView.register(OffersAndDealsCell.self)
    }
    
    func timeString(time: Int) -> String {
        let hour = time / 3600
        let minute = time / 60 % 60
        let second = time % 60
        
        // return formated string
        return String(format: "%02i:%02i:%02i", hour, minute, second)
    }
    
    @objc func timeLeft() {
        
        if self.offersAndDeals.count > 0 {
            
            for (index, element) in self.offersAndDeals.enumerated() {
                if let time = element.timeLeft , time == 0, index < self.offersAndDeals.count {
                     self.offersAndDeals.remove(at: index)
                }
            }
            
            for (index, element) in self.offersAndDeals.enumerated() {
                if let time = element.timeLeft , time > 0 {
                    self.offersAndDeals[index].timeLeft =  time - 1
                    self.offersAndDeals[index].timeLeftFormat = (self.timeString(time: self.offersAndDeals[index].timeLeft ?? 0))
                }
            }
            
            Threads.performTaskInMainQueue {
                self.offersTableView.reloadData()
            }
        }
                
    }
    
    //MARK: - APIMethods
     func requestOffersAndDealsAPI(page: Int) {
        self.viewModel?.requestOffersAndDealsAPIParamAPI(page: page, completion: { [weak self] (nextPage, perPage, offersAndDealsList) in
            guard let sSelf = self,
                let offersAndDeals = offersAndDealsList.offersAndDeals else { return }
            sSelf.nextPageNumber = nextPage
           // sSelf.offersAndDeals = offersAndDeals
            sSelf.offersAndDeals = sSelf.offersAndDeals + offersAndDeals
            sSelf.timeLeft()
//            Threads.performTaskAfterDealy(10, {
//                sSelf.offersTableView.reloadData()
//            })
        })
    }
}
