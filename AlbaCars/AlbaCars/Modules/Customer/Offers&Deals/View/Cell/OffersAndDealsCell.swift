//
//  OffersAndDealsCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/9/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol OffersAndDealsCellCellDelegate: class {
    func didTapBuyNow(cell: OffersAndDealsCell)
}

class OffersAndDealsCell: BaseTableViewCell, NibLoadableView, ReusableView  {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var timeLeftLabel: UILabel!
    @IBOutlet weak var dealLeftLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var offerImageView: UIImageView!
    
    //MARK: - Variables
    weak var delegate: OffersAndDealsCellCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.bgView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
            self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
            self.priceLabel.makeLayer(color: UIColor.redButtonColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: 0)
        }
    }
    
    //MARK: - Public Methods
    func configureView(offersAndDeals: OffersAndDeals) {
        
        //Image
        if let offersAndDealsImage = offersAndDeals.offerImage{
            self.offerImageView.setImage(urlStr: offersAndDealsImage, placeHolderImage: nil)
        }
        
        self.titleLabel.text = offersAndDeals.offerTitle
        self.descLabel.text = offersAndDeals.offerText
        self.priceLabel.text = offersAndDeals.getPrice()
        self.dealLeftLabel.text = Helper.toString(object: offersAndDeals.dealsLeft)
        self.timeLeftLabel.text = Helper.toString(object: offersAndDeals.timeLeftFormat)
    }
        
    //MARK: - IBActions
    @IBAction func tapBuyNow(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapBuyNow(cell: self)
        }
    }    
}
