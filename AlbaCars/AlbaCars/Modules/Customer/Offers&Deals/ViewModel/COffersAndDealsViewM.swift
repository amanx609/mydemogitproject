//
//  COffersAndDealsViewM.swift
//  AlbaCars
//
//  Created by Narendra on 1/9/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol COffersAndDealsViewModeling: BaseVModeling {
    func requestOffersAndDealsAPIParamAPI(page: Int, completion: @escaping (Int, Int, OffersAndDealsList) -> Void)
}

class COffersAndDealsViewM: COffersAndDealsViewModeling {
    
   func requestOffersAndDealsAPIParamAPI(page: Int, completion: @escaping (Int, Int, OffersAndDealsList) -> Void) {
        let offersAndDealsAPIParam = getAPIParams(page: page)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .vehicleOffer(param: offersAndDealsAPIParam))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let resultData = result.toJSONData(),
                    let offersAndDealsList = OffersAndDealsList(jsonData: resultData) {
                    completion(nextPageNumber, perPage, offersAndDealsList)
                }
            }
        }
    }
    
    private func getAPIParams(page: Int) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.page] = page as AnyObject
        return param
    }
}
