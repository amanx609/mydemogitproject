//
//  CServiceContractViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol CServiceContractVModeling: BaseVModeling {
    func getServiceContractTableDataSource(thirdPartyModel: [WarrantyModel], carName: String) -> [CellInfo]
    func requestGetServiceContractAPI(agencyId: Int,makeID:Int, completion: @escaping (Bool, [WarrantyModel])-> Void)
    
}

class CServiceContractViewM: CServiceContractVModeling {
      
    func getServiceContractTableDataSource(thirdPartyModel: [WarrantyModel], carName: String) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Cell
        let countryCell = CellInfo(cellType: .DropDownCell, placeHolder: "Select Car Model".localizedString(), value: carName, info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: true)
        array.append(countryCell)
        
        //headingLabelCell
        var headingLabelInfo = [String: AnyObject]()
        headingLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.center.rawValue as AnyObject
        headingLabelInfo[Constants.UIKeys.placeholderText] = "Following Service Contract Packages are Available for Your Chosen Car".localizedString() as AnyObject
        
        let headingLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "", value: "", info: headingLabelInfo, height: Constants.CellHeightConstants.height_100)
        array.append(headingLabelCell)
        
        //Upcoming Auctions Cell
        var packageWarrantyCellInfo = [String: AnyObject]()
        //setting data for package warranty cell
        for i in 0..<thirdPartyModel.count {
            packageWarrantyCellInfo[Constants.UIKeys.warrantyModel] = thirdPartyModel[i] as AnyObject
            packageWarrantyCellInfo[Constants.UIKeys.stepperValue] = 1 as AnyObject
            packageWarrantyCellInfo[ConstantAPIKeys.price] = thirdPartyModel[i].price as AnyObject
            packageWarrantyCellInfo[Constants.UIKeys.cellIcon] = #imageLiteral(resourceName: "carShield")
            let packageWarrantyCell = CellInfo(cellType: .PackageWarrantyTableCell, placeHolder: "".localizedString(), value: "", info: packageWarrantyCellInfo, height: UITableView.automaticDimension)
            array.append(packageWarrantyCell)
        }
        return array
        
    }
    
    //MARK: - API Methods
    
    func requestGetServiceContractAPI(agencyId: Int,makeID:Int, completion: @escaping (Bool, [WarrantyModel]) -> Void) {
        let warrantyParams = self.getGetWarrantyAPIParams(agencyId: agencyId, makeID: makeID)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getServiceContract(param: warrantyParams))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [[String: AnyObject]],
                    let resultData = safeResponse.toJSONData(),
                    let warrantyList = WarrantyList(jsonData: resultData)
                    
                {
                    completion(success, warrantyList.warranty ?? [])
                    
                }
            }
        }
    }
    
    //MARK: - Creating API parameters
    private func getGetWarrantyAPIParams(agencyId: Int,makeID:Int) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.agencyId] = agencyId as AnyObject
        param[ConstantAPIKeys.make_id] = makeID as AnyObject
        return param
    }
    
}
