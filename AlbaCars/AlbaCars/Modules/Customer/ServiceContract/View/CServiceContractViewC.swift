//
//  CServiceContractViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class CServiceContractViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var serviceContractTableView: UITableView!
    @IBOutlet weak var serviceContractCollectionView: UICollectionView!
    
    @IBOutlet weak var noDataView: UIView!
    
    //MARK: - Variables
    var serviceContractDataSource: [CellInfo] = []
    var serviceContractHeaderDataSource: [[String: AnyObject]] = []
    var viewModel: CServiceContractVModeling?
    var warrantyViewModel: CThirdPartyWarrantyViewM?
    var selectedIndex = 0
    var chooseCarViewModel: ChooseCarFormViewM?
    var carListDataSource: [[String: AnyObject]] = []
    var carDetails: ChooseCarModel?
    
    //MARK: - LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    deinit {
        print("deinit ServiceContractViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Service Contract".localizedString(),barColor: .redButtonColor,titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setupCollectionView()
        self.setupTableView()
        self.requestAgencyListAPI()
        self.requestChooseCarAPI()
       
        if self.serviceContractDataSource.count > 0 {
            scrollAndLoadData(indexPath: IndexPath(item: 0, section: 0))
        }
        
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = CServiceContractViewM()
        }
        
        if self.warrantyViewModel == nil {
            self.warrantyViewModel = CThirdPartyWarrantyViewM()
        }
        
        if self.chooseCarViewModel == nil {
            self.chooseCarViewModel = ChooseCarFormViewM()
        }
        
    }
    
    private func setupTableView() {
        self.serviceContractTableView.delegate = self
        self.serviceContractTableView.dataSource = self
        self.serviceContractTableView.separatorStyle = .none
        self.serviceContractTableView.allowsSelection = false
    }
    
    private func setupCollectionView() {
        self.registerNibs()
        self.serviceContractCollectionView.delegate = self
        self.serviceContractCollectionView.dataSource = self
        self.serviceContractCollectionView.showsHorizontalScrollIndicator = false
        
    }
    
    private func registerNibs() {
        self.serviceContractTableView.register(DropDownCell.self)
        self.serviceContractTableView.register(BottomLabelCell.self)
        self.serviceContractTableView.register(PackageWarrantyTableCell.self)
        self.serviceContractCollectionView.register(headerCollectionCell.self)
    }
    
     func requestAgencyListAPI() {
        
        self.warrantyViewModel?.requestAgencyListAPI(agencyType: AgencyType.ServiceContract.rawValue,  makeID: Helper.toInt(carDetails?.make_id),completion: { [weak self] (success, agencyList) in
            guard let sSelf = self else { return }
            if success {
                
                sSelf.serviceContractHeaderDataSource = agencyList
                Threads.performTaskInMainQueue {
                    sSelf.serviceContractCollectionView.reloadData()
                }
                if agencyList.count > 0{
                    sSelf.requestWarrantyListAPI()
                    sSelf.noDataView.isHidden = true
                    
                }else{
                    sSelf.noDataView.isHidden = false
                }
                sSelf.requestWarrantyListAPI()
            }
        })
    }
    
    private func requestChooseCarAPI() {
        self.chooseCarViewModel?.requestChooseCarAPI(completion: { [weak self] (success, carList) in
            guard let sSelf = self else { return }
            if success {
                sSelf.carListDataSource = carList
            }
        })
        
    }
    
    private func requestWarrantyListAPI() {
        
        if serviceContractHeaderDataSource.count > 0 {
            
            let id = Helper.toInt(self.serviceContractHeaderDataSource[selectedIndex][ConstantAPIKeys.id])
            self.viewModel?.requestGetServiceContractAPI(agencyId: id, makeID: Helper.toInt(carDetails?.make_id), completion: { [weak self] (success, warrantyList) in
                guard let sSelf = self else { return }
                if success {
                    //self.thirdPartyTableView = agencyList
                    Threads.performTaskInMainQueue {
                        let carName = "\(Helper.toString(object: sSelf.carDetails?.title)), \(Helper.toString(object: sSelf.carDetails?.plateNumber))"
                        if let dataSource = sSelf.viewModel?.getServiceContractTableDataSource(thirdPartyModel: warrantyList, carName: carName) {
                            sSelf.serviceContractDataSource = dataSource
                        }
                        if warrantyList.count > 0{
                            sSelf.noDataView.isHidden = true
                        }else{
                            sSelf.noDataView.isHidden = false
                        }
                        sSelf.serviceContractTableView.reloadData()
                    }
                }
            })
        }
    }
    
    func requestBuyWarrantyAPI(params: APIParams) {
        
        self.warrantyViewModel?.requestBuyWarrantyAPI(params: params, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            if success {
                
                Threads.performTaskInMainQueue {
                    sSelf.navigationController?.popToRootViewController(animated: true)
                    Alert.showOkAlert(title: StringConstants.Text.AppName, message: StringConstants.Text.purchasedSuccessfully.localizedString())
                }
            }
        })
        
    }
    
    func loadDataSource(position: Int) {
        
        self.selectedIndex = position
        Threads.performTaskInMainQueue {
            self.serviceContractDataSource.removeAll()
            self.serviceContractTableView.reloadData()
            self.requestWarrantyListAPI()
        }
    }
    
}
