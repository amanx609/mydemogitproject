//
//  CServiceContractViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//Table View DataSource
extension CServiceContractViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.serviceContractDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.serviceContractDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.serviceContractDataSource[indexPath.row].height
    }
}

extension CServiceContractViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
            
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .DropDownCell:
            let cell: DropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .PackageWarrantyTableCell:
            let cell: PackageWarrantyTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureViewServiceContract(cellInfo: cellInfo)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - DropDownCellDelegate
extension CServiceContractViewC: DropDownCellDelegate {
    func didTapDropDownCell(cell: DropDownCell) {
        guard let indexPath = self.serviceContractTableView.indexPath(for: cell) else { return }
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        if self.carListDataSource.count == 0 {
            return
        }
        listPopupView.initializeViewWith(title: "Select Car".localizedString(), arrayList: self.carListDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
            guard let sSelf = self,
                let carName = response[ConstantAPIKeys.title] as? String,
                let carId = response[ConstantAPIKeys.id] as? Int else { return }
           if let jsonData = response.toJSONData() {
               sSelf.carDetails = ChooseCarModel(jsonData: jsonData)
           }
            sSelf.requestAgencyListAPI()
            sSelf.serviceContractDataSource[indexPath.row].placeHolder = "\(carId)"
            sSelf.serviceContractDataSource[indexPath.row].value = carName
            sSelf.serviceContractTableView.reloadData()
        }
        listPopupView.showWithAnimated(animated: true)
        
    }
}


//MARK: - PackageWarrantyCellDelegate
extension CServiceContractViewC: PackageWarrantyTableCellDelegate {
    func didChangePackagePrice(cell: PackageWarrantyTableCell, value: String) {
        
    }
    
    func didTapBuyNow(cell: PackageWarrantyTableCell) {
        guard let indexPath = self.serviceContractTableView.indexPath(for: cell) else { return }
        
        if let warrantyModel = self.serviceContractDataSource[indexPath.row].info?[Constants.UIKeys.warrantyModel] as? WarrantyModel {
            
            let paymentDetails = PaymentDetailsData(viewController: self, totalAmmount: Helper.toDouble(warrantyModel.price), referenceId: Helper.toInt(warrantyModel.id), paymentServiceCategoryId: PaymentServiceCategoryId.serviceContract,vatAmmount: 0)
            
            PaymentManager.sharedInstance.initiatePayment(paymentDetailsData: paymentDetails) { (success, paymentID) in
                
                if success {
                    
                    var params = APIParams()
                    params[ConstantAPIKeys.vehicleId] = Helper.toInt(self.carDetails?.id) as AnyObject
                    params[ConstantAPIKeys.warrantyId] = Helper.toInt(warrantyModel.id) as AnyObject
                    params[ConstantAPIKeys.noOfYears] = self.serviceContractDataSource[indexPath.row].info?[Constants.UIKeys.stepperValue] as AnyObject
                    params[ConstantAPIKeys.price] = self.serviceContractDataSource[indexPath.row].info?[ConstantAPIKeys.price] as AnyObject
                    params[ConstantAPIKeys.paymentId] = paymentID as AnyObject
                    self.requestBuyWarrantyAPI(params: params)
                }
            }
        }
        
    }
    
    func didTapViewDetails(cell: PackageWarrantyTableCell) {
        guard let indexPath = self.serviceContractTableView.indexPath(for: cell) else { return }
        if let warrantyModel = self.serviceContractDataSource[indexPath.row].info?[Constants.UIKeys.warrantyModel] as? WarrantyModel {
           
            let serviceDetailsVC = DIConfigurator.sharedInstance.getCServiceContractDetailsVC()
            serviceDetailsVC.warrantyServiceId = Helper.toInt(warrantyModel.id)
            serviceDetailsVC.vehicleId = Helper.toInt(self.carDetails?.id)
            self.navigationController?.pushViewController(serviceDetailsVC, animated: true)
        }
    }
    
    func didTapStepperButton(cell: PackageWarrantyTableCell, value: String) {
        guard let indexPath = self.serviceContractTableView.indexPath(for: cell) else { return }
        self.serviceContractDataSource[indexPath.row].info?[Constants.UIKeys.stepperValue] = value as AnyObject
        //changing price of package according to stepper value
        if let warrantyModel = self.serviceContractDataSource[indexPath.row].info?[Constants.UIKeys.warrantyModel] as? WarrantyModel {
            let price = Helper.toInt(warrantyModel.price) * Helper.toInt(value)
            self.serviceContractDataSource[indexPath.row].info?[ConstantAPIKeys.price] = price as AnyObject
            
        }
        
        Threads.performTaskInMainQueue {
            self.serviceContractTableView.reloadData()
            
        }
        
        
    }
    
    func didTapPackagePriceButton(cell: PackageWarrantyTableCell) {
        
    }
    
    
}

//MARK: - BottomLabelCellDelegate
extension CServiceContractViewC: BottomLabelCellDelegate {
    func didTapLabel(cell: BottomLabelCell) {
        if let viewControllers = self.navigationController?.viewControllers {
            //CreateAccountViewCIsPresentInNavigationStack
            for viewController in viewControllers {
                if viewController is CreateAccountViewC {
                    self.navigationController?.popToViewController(viewController, animated: true)
                    return
                }
            }
            //CreateAccountViewCNotPresentInNavigationStack
            let createAccountVC = DIConfigurator.sharedInstance.getCreateAccountVC()
            self.navigationController?.pushViewController(createAccountVC, animated: true)
        }
    }
}

//MARK: - CollectionView Datasource

extension CServiceContractViewC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.serviceContractHeaderDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellInfo = self.serviceContractHeaderDataSource[indexPath.row]
        return self.getCell(collectionView: collectionView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.bounds.width/3.0, height: Constants.CellHeightConstants.height_60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        scrollAndLoadData(indexPath: indexPath)
        
        //        self.didTapButton(position, indexPath: indexPath)
    }
    
    func scrollAndLoadData(indexPath: IndexPath) {
        //        self.thirdPartyHeaderDataSource.filter { ({$0.isS}) -> Bool in
        //
        //        }
        
        self.selectedIndex = indexPath.row
        self.serviceContractCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.serviceContractCollectionView.reloadData()
        self.loadDataSource(position: indexPath.item)
    }
    
}

extension CServiceContractViewC {
    func getCell(collectionView: UICollectionView, indexPath: IndexPath, cellInfo: [String: AnyObject]) -> UICollectionViewCell {
        let cell: headerCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        let isSelected = indexPath.row == self.selectedIndex ? true : false
        cell.configureView(agencyList: cellInfo, isSelected: isSelected)
        return cell

    }
}




