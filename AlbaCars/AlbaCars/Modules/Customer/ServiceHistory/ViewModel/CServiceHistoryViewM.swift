//
//  CServiceHistoryViewM.swift
//  AlbaCars
//
//  Created by Narendra on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//


import Foundation
import UIKit

protocol CServiceHistoryViewModeling: BaseVModeling {
    func getServiceHistoryDataSource() -> [CellInfo]
    func requestServiceHistoryAPI(cellInfo: [CellInfo], paymentID:String,price:Double, completion: @escaping (Bool) -> Void)
    func validateServiceHistoryParams(arrData: [CellInfo], ownerCertify:Bool, termsConditionSelected: Bool) -> Bool
}

class CServiceHistoryViewM: CServiceHistoryViewModeling {
    
    func getServiceHistoryDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Cell
        let carCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "Select Car".localizedString(), info: nil, height: Constants.CellHeightConstants.height_80)
        array.append(carCell)
        
        //Choose Your Car Agency
        let agencyCell = CellInfo(cellType: .LeftRightLabelCell, placeHolder: "Choose Your Car Agency".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(agencyCell)
        
        let cchooseAgencyCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "", info: nil, height: Constants.CellHeightConstants.height_80)
        array.append(cchooseAgencyCell)
        
        //Attach Ownerships Documents
        let documentTitleCell = CellInfo(cellType: .LeftRightLabelCell, placeHolder: "Attach Ownerships Documents".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(documentTitleCell)
        
        var documentCellInfo = [String: AnyObject]()
        documentCellInfo[Constants.UIKeys.emiratesID] = "" as AnyObject
        documentCellInfo[Constants.UIKeys.ownershipCertificate] = "" as AnyObject
        let documentCell = CellInfo(cellType: .ServiceHistoryUploadDocumentCell, placeHolder: "".localizedString(), value: "", info: documentCellInfo, height: UITableView.automaticDimension)
        array.append(documentCell)
        
        //Additional Information
        let infoCell = CellInfo(cellType: .LeftRightLabelCell, placeHolder: "Additional Information".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(infoCell)
        
        //TextView Cell
        let textViewCell = CellInfo(cellType: .TextViewCell, placeHolder: "Write additional information....".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(textViewCell)
        
        return array
    }
    
     func validateServiceHistoryParams(arrData: [CellInfo], ownerCertify:Bool, termsConditionSelected: Bool) -> Bool {
        
        var isValid = true
        if let carBrand = arrData[0].placeHolder, carBrand.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select car.".localizedString())
            isValid = false
        } else if let agency = arrData[2].placeHolder, agency.isEmpty  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select agency.".localizedString())
            isValid = false
        } else if let emiratesImage = arrData[4].info?[Constants.UIKeys.emiratesID] as? String, emiratesImage.isEmpty  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select emirates image.".localizedString())
            isValid = false
        } else if let ownerShipImage = arrData[4].info?[Constants.UIKeys.ownershipCertificate] as? String, ownerShipImage.isEmpty  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for  ownership certificate.".localizedString())
            isValid = false
        } else if !ownerCertify {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please certify that you are true owner of this vehicle.".localizedString())
            isValid = false
        } else if !termsConditionSelected {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please accept terms and conditions.".localizedString())
            isValid = false
        }
        
        return isValid
        
    }
    
    private func uploadImagesToS3(images: [UIImage], completion: @escaping ([String])-> Void) {
        S3Manager.sharedInstance.uploadImages(images: images, imageQuality: .medium, progress: nil) { (imagesUrl, imageName, error) in
            guard let imageUrlArray = imagesUrl as? [String] else {
                return
            }
            completion(imageUrlArray)
        }
    }
    
    
    private func getServiceHistoryParams(param: [CellInfo], paramArray: [APIParams],price:Double)-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.vehicleId] = param[0].placeHolder as AnyObject
        params[ConstantAPIKeys.agencyId] = param[2].placeHolder as AnyObject
        params[ConstantAPIKeys.document] = paramArray as AnyObject
        params[ConstantAPIKeys.additionalInformation] = param[6].value as AnyObject
        params[ConstantAPIKeys.price] =  "\(price)" as AnyObject//param[4].value as AnyObject
        return params
    }
    
    func requestServiceHistoryAPI(cellInfo: [CellInfo], paymentID:String,price:Double, completion: @escaping (Bool) -> Void) {
        
        var paramArray = [APIParams]()
        
       // if self.validateServiceHistoryParams(arrData: cellInfo, ownerCertify: ownerCertify, termsConditionSelected: termsConditionSelected) {
            
            if let img1 = cellInfo[4].info?[Constants.UIKeys.emiratesID] as? UIImage, let img2 = cellInfo[4].info?[Constants.UIKeys.ownershipCertificate] as? UIImage {
                
                self.uploadImagesToS3(images: [img1, img2]) { (names) in
                    
                    paramArray = [[ConstantAPIKeys.name: Constants.UIKeys.emiratesID as AnyObject, ConstantAPIKeys.image: names[0] as AnyObject],[ConstantAPIKeys.name: Constants.UIKeys.ownershipCertificate as AnyObject, ConstantAPIKeys.image: names[1] as AnyObject]]
                    
                    var serviceHistoryParams = self.getServiceHistoryParams(param: cellInfo, paramArray: paramArray,price:price)
                    serviceHistoryParams[ConstantAPIKeys.paymentId] = paymentID as AnyObject

                    APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .serviceHistory(param: serviceHistoryParams))) { (response, success) in
                       
                        if success {
                            if let safeResponse =  response as? [String: AnyObject],
                                let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                            {
                                completion(success)
                            }
                        }
                    }
                    
                }
           // }
            
        }
    }
}
