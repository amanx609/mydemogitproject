//
//  CServiceHistoryViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//


import Foundation
import UIKit

extension CServiceHistoryViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.serviceHistoryDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.serviceHistoryDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.serviceHistoryDataSource[indexPath.row].height
    }
}

extension CServiceHistoryViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .LeftRightLabelCell:
            let cell: LeftRightLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureServiceHistoryView(cellInfo: cellInfo)
            return cell
        case .DropDownCell:
            let cell: DropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .TextViewCell:
            let cell: TextViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate =  self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .ServiceHistoryUploadDocumentCell:
            let cell: ServiceHistoryUploadDocumentCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate =  self
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
        
        
    }
}

//MARK: - DropDownCellDelegate
extension CServiceHistoryViewC: DropDownCellDelegate {
  func didTapDropDownCell(cell: DropDownCell) {
    guard let indexPath = self.serviceHistoryTableView.indexPath(for: cell) else { return }
         self.showDropDownAt(indexPath: indexPath)
   }
}

//MARK: - TextViewCellDelegate
extension CServiceHistoryViewC: TextViewCellDelegate {
    func didChangeText(cell: TextViewCell, text: String) {
        guard let indexPath = self.serviceHistoryTableView.indexPath(for: cell) else { return }
        self.serviceHistoryDataSource[indexPath.row].value = text
    }
}

//MARK: - ServiceHistoryUploadDocumentCellDelegate
extension CServiceHistoryViewC: ServiceHistoryUploadDocumentCellDelegate {
    func didTapEmiratesIDCell(cell: ServiceHistoryUploadDocumentCell) {
        guard let indexPath = self.serviceHistoryTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.serviceHistoryDataSource[indexPath.row].info?[Constants.UIKeys.emiratesID] = image as AnyObject
                        self.serviceHistoryTableView.reloadData()
                }
            }
        }
    }
    
    func didTapownershipCertificateCell(cell: ServiceHistoryUploadDocumentCell) {
        guard let indexPath = self.serviceHistoryTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.serviceHistoryDataSource[indexPath.row].info?[Constants.UIKeys.ownershipCertificate] = image as AnyObject
                    self.serviceHistoryTableView.reloadData()
                }
            }
        }
    }
        
}
