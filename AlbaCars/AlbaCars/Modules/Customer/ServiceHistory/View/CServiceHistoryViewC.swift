//
//  CServiceHistoryViewC.swift
//  AlbaCars
//
//  Created by Narendra on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class CServiceHistoryViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var serviceHistoryTableView: UITableView!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var payNowButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var certifyOwner:UIButton!
    @IBOutlet var termsConditionButton: UIButton!
    
    //MARK: - Variables
    var viewModel: CServiceHistoryViewModeling?
    var chooseCarViewModel: ChooseCarFormViewModeling?
    var warrantyViewModel: CThirdPartyWarrantyVModeling?
    var serviceHistoryDataSource: [CellInfo] = []
    var carListDataSource: [[String: AnyObject]] = []
    var agencyListDataSource: [[String: AnyObject]] = []
    var carDetails: ChooseCarModel?
    var price: Double = 0
    var makeID = 0


    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    deinit {
        print("deinit CServiceHistoryViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupTableView()
        self.roundCorners()
        self.setupView()
        self.setupNavigationBarTitle(title: "Service History".localizedString(),barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
       makeID = Helper.toInt(carDetails?.make_id)
        self.requestChooseCarAPI()
        self.requestAgencyListAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = CServiceHistoryViewM()
        }
        
        if self.warrantyViewModel == nil {
            self.warrantyViewModel = CThirdPartyWarrantyViewM()
        }
        
        if self.chooseCarViewModel == nil {
            self.chooseCarViewModel = ChooseCarFormViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.serviceHistoryTableView.backgroundColor = UIColor.white
        self.serviceHistoryTableView.delegate = self
        self.serviceHistoryTableView.dataSource = self
        self.serviceHistoryTableView.separatorStyle = .none
        self.serviceHistoryTableView.tableFooterView = self.footerView
        self.serviceHistoryTableView.allowsSelection = false
        self.loadDataSource()
    }
    
    private func registerNibs() {
        self.serviceHistoryTableView.register(DropDownCell.self)
        self.serviceHistoryTableView.register(BottomLabelCell.self)
        self.serviceHistoryTableView.register(TextViewCell.self)
        self.serviceHistoryTableView.register(LeftRightLabelCell.self)
        self.serviceHistoryTableView.register(ServiceHistoryUploadDocumentCell.self)
        
    }
    
    private func roundCorners() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
            self.priceLabel.makeLayer(color: UIColor.redButtonColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: 0)
        }
    }
    
    private func setupView() {
        self.payNowButton.setTitle(StringConstants.Text.payNow.localizedString(), for: .normal)
    }
    
    private func requestChooseCarAPI() {
        self.chooseCarViewModel?.requestChooseCarAPI(completion: { [weak self] (success, carList) in
            guard let sSelf = self else { return }
            if success {
                sSelf.carListDataSource = carList
            }
        })
        
    }
    
    private func requestServiceHistoryAPI() {
        
        //
        //        self.viewModel?.requestServiceHistoryAPI(cellInfo: self.serviceHistoryDataSource, paymentID:"1234", completion: {[weak self] (success) in
        //            guard let sSelf = self else { return }
        //            Threads.performTaskInMainQueue {
        //                sSelf.navigationController?.popToRootViewController(animated: true)
        //            }
        //        })
        //
        //        return
        
        if let valid = self.viewModel?.validateServiceHistoryParams(arrData: self.serviceHistoryDataSource, ownerCertify: self.certifyOwner.isSelected, termsConditionSelected: self.termsConditionButton.isSelected), valid {
            
            let paymentDetails = PaymentDetailsData(viewController: self, totalAmmount: Helper.toDouble(price), referenceId: Helper.toInt(self.serviceHistoryDataSource[0].placeHolder), paymentServiceCategoryId: PaymentServiceCategoryId.serviceHistory,vatAmmount: 0)
            
            PaymentManager.sharedInstance.initiatePayment(paymentDetailsData: paymentDetails) { (success, paymentID) in
                if success {
                    Threads.performTaskAfterDealy(0.2) {
                        self.viewModel?.requestServiceHistoryAPI(cellInfo: self.serviceHistoryDataSource, paymentID:paymentID,price:self.price, completion: {[weak self] (success) in
                            guard let sSelf = self else { return }
                            Threads.performTaskInMainQueue {
                                sSelf.navigationController?.popToRootViewController(animated: true)
                            }
                        })
                    }
                }
            }
        }
    }
    
    private func requestAgencyListAPI() {
        self.warrantyViewModel?.requestAgencyListAPI(agencyType: AgencyType.ServiceHistory.rawValue, makeID:self.makeID, completion: { [weak self] (success, agencyList) in
            guard let sSelf = self else { return }
            if success {
                sSelf.agencyListDataSource = agencyList
                Threads.performTaskInMainQueue {
                    sSelf.serviceHistoryTableView.reloadData()
                }
            }
        })
    }
    
    func loadDataSource() {
        
        if let dataSource = self.viewModel?.getServiceHistoryDataSource() {
            self.serviceHistoryDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.serviceHistoryTableView.reloadData()
            }
            
            self.serviceHistoryDataSource[0].value = "\(Helper.toString(object: carDetails?.title)), \(Helper.toString(object: carDetails?.plateNumber))"
            self.serviceHistoryDataSource[0].placeHolder = Helper.toString(object: carDetails?.id)
            self.serviceHistoryTableView.reloadData()
        }
    }
    
    func showDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        switch indexPath.row {
        case 0:
            if self.carListDataSource.count == 0 {
                self.requestChooseCarAPI()
                return
            }
            listPopupView.initializeViewWith(title: "Select Car".localizedString(), arrayList: self.carListDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let brandId = response[ConstantAPIKeys.id] as? Int,
                    let brandName = response[ConstantAPIKeys.title] as? String, let vinNumber = response[ConstantAPIKeys.plateNumber] as? String else { return }
                sSelf.serviceHistoryDataSource[indexPath.row].value = "\(brandName), \(vinNumber)"
                sSelf.serviceHistoryDataSource[indexPath.row].placeHolder = "\(brandId)"
                sSelf.makeID = Helper.toInt(response[ConstantAPIKeys.make_id])
                sSelf.requestAgencyListAPI()
                sSelf.serviceHistoryDataSource[2].value = ""
                sSelf.serviceHistoryDataSource[2].placeHolder = ""
                    sSelf.price = 0.0
                sSelf.priceLabel.text = ""
                sSelf.serviceHistoryTableView.reloadData()
            }
            
        case 2:
            if self.agencyListDataSource.count == 0 {
                self.requestAgencyListAPI()
                return
            }
            listPopupView.initializeViewWith(title: "Select Agency".localizedString(), arrayList: self.agencyListDataSource, key: ConstantAPIKeys.agency) { [weak self] (response) in
                guard let sSelf = self,
                    let brandId = response[ConstantAPIKeys.id] as? Int,
                    let brandName = response[ConstantAPIKeys.agency] as? String//, let vinNumber = response[ConstantAPIKeys.plateNumber] as? String
                    else { return }
                sSelf.serviceHistoryDataSource[indexPath.row].value = "\(brandName)"
                sSelf.serviceHistoryDataSource[indexPath.row].placeHolder = "\(brandId)"
                sSelf.price = Helper.toDouble(response[ConstantAPIKeys.price])
                sSelf.priceLabel.text = "AED " + Helper.toString(object: sSelf.price)
                sSelf.serviceHistoryTableView.reloadData()
            }
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    //MARK: - IBActions
    @IBAction func tapAcceptTermsConditions(_ sender: Any) {
        self.termsConditionButton.isSelected = !self.termsConditionButton.isSelected
    }
    
    @IBAction func tapCertifyOwner(_ sender: Any) {
        self.certifyOwner.isSelected = !self.certifyOwner.isSelected
    }
    
    @IBAction func tapPayNowButton(_ sender: Any) {
        self.requestServiceHistoryAPI()
    }
}
