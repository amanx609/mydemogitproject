//
//  ServiceHistoryUploadDocumentCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol ServiceHistoryUploadDocumentCellDelegate: class {
    func didTapEmiratesIDCell(cell: ServiceHistoryUploadDocumentCell)
    func didTapownershipCertificateCell(cell: ServiceHistoryUploadDocumentCell)
}

class ServiceHistoryUploadDocumentCell: BaseTableViewCell, NibLoadableView, ReusableView  {
    
    //MARK: - IBOutlets
    @IBOutlet weak var emiratesIDImageView: UIImageView!
    @IBOutlet weak var ownershipCertificateImageView: UIImageView!
    
    //MARK: - Variables
    weak var delegate: ServiceHistoryUploadDocumentCellDelegate?
    
    //MARK: - LifeCycle Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        
        if let info = cellInfo.info {
            //Image
            if let placeholderImage = info[Constants.UIKeys.emiratesID] as? UIImage {
            
                self.emiratesIDImageView.image = placeholderImage
            }
            
            if let placeholderImage = info[Constants.UIKeys.ownershipCertificate] as? UIImage {
            
                self.ownershipCertificateImageView.image = placeholderImage
            }
        }
        
    }
    
    //MARK: - IBActions
    @IBAction func tapEmiratesID(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapEmiratesIDCell(cell: self)
        }
    }
    
    @IBAction func tapOwnershipCertificate(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapownershipCertificateCell(cell: self)
        }
    }
}
