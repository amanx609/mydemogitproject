//
//  ThirdPartyWarrantyDetailsViewM.swift
//  AlbaCars
//
//  Created by Narendra on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol ThirdPartyWarrantyDetailsViewModeling {
    func getThirdPartyWarrantyDetailDataSource(warrantyDetailsModel: WarrantyDetailsModel) -> [CellInfo]
    func requestGetWarrantyDetailsAPI(warrantyServiceId: Int, completion: @escaping (Bool,WarrantyDetailsModel)-> Void)
}

class ThirdPartyWarrantyDetailsViewM: ThirdPartyWarrantyDetailsViewModeling {
    
    func getThirdPartyWarrantyDetailDataSource(warrantyDetailsModel: WarrantyDetailsModel) -> [CellInfo] {
        
        var array = [CellInfo]()
        //Limit Per Claim
       
        let serviceIntervalCell = CellInfo(cellType: .LeftRightLabelCell, placeHolder: "Limit Per Claim".localizedString(), value: String.appendText(obj: warrantyDetailsModel.limitPerClaim, text: StringConstants.Text.Currency.localizedString()), height: Constants.CellHeightConstants.height_40)
        
        array.append(serviceIntervalCell)
        
        //Annual Claim Limit
        let annualClaimLimit = Helper.toInt(warrantyDetailsModel.annualClaimLimit) == 0 ? "AED \(Helper.toString(object: warrantyDetailsModel.annualClaimLimit))" : Helper.toString(object: warrantyDetailsModel.annualClaimLimit)
        let annualCell = CellInfo(cellType: .LeftRightLabelCell, placeHolder: "Annual Claim Limit".localizedString(), value: String.appendText(obj: warrantyDetailsModel.annualClaimLimit, text: StringConstants.Text.Currency.localizedString()), height: Constants.CellHeightConstants.height_40)
        array.append(annualCell)
        
        //Coverage Kms Per Year
        let engineSizeCell = CellInfo(cellType: .LeftRightLabelCell, placeHolder: "Coverage Kms Per Year".localizedString(), value:String.appendText(obj: warrantyDetailsModel.coverageKmsPerYear, text: StringConstants.Text.Currency.localizedString()), height: Constants.CellHeightConstants.height_40)
        array.append(engineSizeCell)
        
        //Coverage
        let coverageCell = CellInfo(cellType: .LeftRightLabelCell, placeHolder: "Coverage".localizedString(), value:String.appendText(obj: warrantyDetailsModel.coverage, text: StringConstants.Text.Currency.localizedString()), height: Constants.CellHeightConstants.height_40)
        array.append(coverageCell)
        
        //Max Current Kms
        let currentCell = CellInfo(cellType: .LeftRightLabelCell, placeHolder: "Max Current Kms".localizedString(), value:String.appendText(obj: warrantyDetailsModel.maxCurrentKms, text: StringConstants.Text.Kms.localizedString()), height: Constants.CellHeightConstants.height_40)
        array.append(currentCell)
        
        //Claim After  Time
        let claimCell = CellInfo(cellType: .LeftRightLabelCell, placeHolder: "Claim After  Time".localizedString(), value: Helper.toString(object: warrantyDetailsModel.claimAfterTime), height: Constants.CellHeightConstants.height_40)
        array.append(claimCell)
        
        //Items Covered
        let itemsCoveredCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Items Covered".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(itemsCoveredCell)
        
        let itemsCovered = warrantyDetailsModel.ItemsCovered ?? []
        let itemsCell = CellInfo(cellType: .ItemsCoveredCell, placeHolder: "".localizedString(), value: "", info: [Constants.UIKeys.itemsCovered: self.getItemsCoveredDataSource(itemsCovered: itemsCovered) as AnyObject], height: UITableView.automaticDimension)
        array.append(itemsCell)
        
        //List Of Approved Garages
        
        let garagesCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "List Of Approved Garages".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(garagesCell)
        
        let approvedGarages = (warrantyDetailsModel.ApprovedGarage as? [ApprovedGarage]) ?? []
        for i in 0..<approvedGarages.count {
            
            let garageCell = CellInfo(cellType: .BottomLabelRegularTextCell, placeHolder: Helper.toString(object: approvedGarages[i].garage), value: "", info: nil, height: Constants.CellHeightConstants.height_30)
            array.append(garageCell)
            
        }
//        let garageCell = CellInfo(cellType: .BottomLabelRegularTextCell, placeHolder: "ABC Garage, Noor Bank, Dubai".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_30)
//        array.append(garageCell)
//
//        let omniCell = CellInfo(cellType: .BottomLabelRegularTextCell, placeHolder: "Omni Cars, Al Quoz,  Dubai".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_30)
//        array.append(omniCell)
//
//        let sellCell = CellInfo(cellType: .BottomLabelRegularTextCell, placeHolder: "Sell Cars, Umm Sequim, Dubai".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_30)
//        array.append(sellCell)
        
        return array
    }
    
    
    
    func getItemsCoveredDataSource(itemsCovered: [ItemsCovered]) -> [CellInfo] {
        var array = [CellInfo]()
        for i in 0..<itemsCovered.count {
            
            var carburatorInfo = [String: AnyObject]()
            carburatorInfo[Constants.UIKeys.status] = true as AnyObject
            let itemsCell = CellInfo(cellType: .ItemsCoveredCollectionCell, placeHolder: Helper.toString(object: itemsCovered[i].item), value: "", info: carburatorInfo, height: Constants.CellHeightConstants.height_40)
            array.append(itemsCell)
        }
        
        return array
    }
    
    func requestGetWarrantyDetailsAPI(warrantyServiceId: Int, completion: @escaping (Bool, WarrantyDetailsModel) -> Void) {
        
        var warrantyDetailsParameters = APIParams()
        warrantyDetailsParameters[ConstantAPIKeys.warrantyServiceId] = warrantyServiceId as AnyObject
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .warrantyDetails(param: warrantyDetailsParameters))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData(),
                    let warrantyDetails = WarrantyDetailsModel(jsonData: resultData)
                    
                {
                    completion(success, warrantyDetails)
                }
            }
        }

    }
    
}
