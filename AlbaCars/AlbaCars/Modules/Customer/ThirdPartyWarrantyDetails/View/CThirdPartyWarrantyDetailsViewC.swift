//
//  CThirdPartyWarrantyDetailsViewC.swift
//  AlbaCars
//
//  Created by Narendra on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class CThirdPartyWarrantyDetailsViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var detailTableView: UITableView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var noOfYearLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var termsConditionButton: UIButton!
    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var warrantyTypeLabel: UILabel!
    @IBOutlet weak var warrantyTypeImageView: UIImageView!
    
    //MARK: - Variables
    var viewModel: ThirdPartyWarrantyDetailsViewModeling?
    var warrantyViewModel: CThirdPartyWarrantyViewM?
    var contractDetailDataSource: [CellInfo] = []
    var yearCount = 0
    var warrantyServiceId = 0
    var price = 0
    var vehicleId = 0
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    deinit {
        print("deinit CThirdPartyWarrantyDetailsViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupTableView()
        self.roundCorners()
        self.setupView()
        self.setupNavigationBarTitle(title: "Third Party Warranty Details".localizedString(),barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.requestWarrantyDetailsAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = ThirdPartyWarrantyDetailsViewM()
        }
        
        if self.warrantyViewModel == nil {
            self.warrantyViewModel = CThirdPartyWarrantyViewM()
            
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.detailTableView.backgroundColor = UIColor.white
        self.detailTableView.delegate = self
        self.detailTableView.dataSource = self
        self.detailTableView.separatorStyle = .none
        self.detailTableView.tableHeaderView = self.headerView
        self.detailTableView.tableFooterView = self.footerView
        self.detailTableView.allowsSelection = false
        
    }
    
    private func registerNibs() {
        self.detailTableView.register(LeftRightLabelCell.self)
        self.detailTableView.register(BottomLabelCell.self)
        self.detailTableView.register(ItemsCoveredCell.self)
    }
    
    private func roundCorners() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.priceLabel.makeLayer(color: UIColor.redButtonColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: 0)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
    }
    
    private func setupView() {
        self.buyNowButton.setTitle(StringConstants.Text.buyNow.localizedString(), for: .normal)
    }
    
    private func setupWithAPIData(warrantyDetails: WarrantyDetailsModel) {
        if Helper.toInt(warrantyDetails.warrantyType) == 0 {
            self.warrantyTypeImageView.image = #imageLiteral(resourceName: "goldBackground")
            self.warrantyTypeLabel.text = StringConstants.Text.gold.localizedString()
        } else {
            self.warrantyTypeImageView.image = #imageLiteral(resourceName: "silverBackground")
            self.warrantyTypeLabel.text = StringConstants.Text.silver.localizedString()
        }
        self.price = Helper.toInt(warrantyDetails.price)
        self.priceLabel.text = String.appendText(obj: warrantyDetails.price, text: StringConstants.Text.Currency.localizedString()) + " \(Helper.getVatPercentage())"
        
    }
    
    private func requestWarrantyDetailsAPI() {
        self.viewModel?.requestGetWarrantyDetailsAPI(warrantyServiceId: self.warrantyServiceId, completion: { [weak self] (success, warrantyDetails) in
            guard let sSelf = self else { return }
            if success {
                //self.thirdPartyTableView = agencyList
                Threads.performTaskInMainQueue {
                    sSelf.setupWithAPIData(warrantyDetails: warrantyDetails)
                    sSelf.loadDataSource(warrantyDetails: warrantyDetails)
                }
            }
        })
        
    }
    
    func loadDataSource(warrantyDetails: WarrantyDetailsModel) {
        if let dataSource = self.viewModel?.getThirdPartyWarrantyDetailDataSource(warrantyDetailsModel: warrantyDetails) {
            self.contractDetailDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.detailTableView.reloadData()
            }
        }
    }
    
    func requestBuyWarrantyAPI(params: APIParams) {
        
        self.warrantyViewModel?.requestBuyWarrantyAPI(params: params, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            if success {
                
                Threads.performTaskInMainQueue {
                    sSelf.navigationController?.popToRootViewController(animated: true)
                    Alert.showOkAlert(title: StringConstants.Text.AppName, message: StringConstants.Text.purchasedSuccessfully.localizedString())
                }
            }
        })
        
    }
    
    //MARK: - IBActions
    @IBAction func tapAcceptTermsConditions(_ sender: Any) {
        self.termsConditionButton.isSelected = !self.termsConditionButton.isSelected
    }
    
    @IBAction func tapIncreaseYear(_ sender: Any) {
        yearCount = yearCount + 1
        self.noOfYearLabel.text = Helper.toString(object: yearCount)
        self.priceLabel.text = String.appendText(obj: self.price * yearCount, text: StringConstants.Text.Currency) + " \(Helper.getVatPercentage())"
    }
    
    @IBAction func tapDecreaseYear(_ sender: Any) {
        if yearCount == 1 {
            return
        }
        yearCount = yearCount - 1
        self.noOfYearLabel.text = Helper.toString(object: yearCount)
        self.priceLabel.text = String.appendText(obj: self.price * yearCount, text: StringConstants.Text.Currency) + " \(Helper.getVatPercentage())"
    }
    
    @IBAction func tapBuyNowButton(_ sender: Any) {
        
        var params = APIParams()
        params[ConstantAPIKeys.vehicleId] = Helper.toInt(self.vehicleId) as AnyObject
        params[ConstantAPIKeys.warrantyId] = Helper.toInt(self.warrantyServiceId) as AnyObject
        params[ConstantAPIKeys.noOfYears] = Helper.toInt(self.noOfYearLabel.text) as AnyObject
        params[ConstantAPIKeys.price] = Helper.toInt(self.price) as AnyObject
        if !termsConditionButton.isSelected {
            Alert.showOkAlert(title: StringConstants.Text.AppName.localizedString(), message: "Please accept terms and conditions.".localizedString())
            return
        }
        
        let paymentDetails = PaymentDetailsData(viewController: self, totalAmmount: Helper.toDouble(self.price), referenceId: Helper.toInt(self.warrantyServiceId), paymentServiceCategoryId: PaymentServiceCategoryId.warranty,vatAmmount: 0)
        
        PaymentManager.sharedInstance.initiatePayment(paymentDetailsData: paymentDetails)  { (success, paymentID) in
            
            if success {
                params[ConstantAPIKeys.paymentId] = paymentID as AnyObject
                self.requestBuyWarrantyAPI(params: params)
            }
        }
    }
}
