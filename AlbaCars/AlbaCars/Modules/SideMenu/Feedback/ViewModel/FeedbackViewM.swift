//
//  FeedbackViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol FeedbackVModeling: BaseVModeling {
    func getFeedbackDataSource() -> [CellInfo]
    func requestFeedbackAPI(arrData: [CellInfo], completion: @escaping (Bool)-> Void)
}

class FeedbackViewM: FeedbackVModeling {
    
    func getFeedbackDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //TextView Cell
        let textViewCell = CellInfo(cellType: .TextViewCell, placeHolder: "Write Something...".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(textViewCell)

        //Submit Cell
        let submitCell = CellInfo(cellType: .BottomButtonCell, placeHolder: "Submit".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_170)
        array.append(submitCell)
        
        return array
    }
    
    //MARK: - API Methods
     func requestFeedbackAPI(arrData: [CellInfo], completion: @escaping (Bool)-> Void) {
       if self.validateFeedbackData(arrData: arrData) {
         let params = self.getFeedbackParams(arrData: arrData)
         APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .feedback(param: params))) { (response, success) in
           if success {
             if let safeResponse =  response as? [String: AnyObject],
                let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
               {
                Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Thank you for your feedback".localizedString())
                completion(success)
             }
           }
         }
       }
     }
    
    //MARK: - Private Methods
     //DataValidationsBeforeAPICall
     private func validateFeedbackData(arrData: [CellInfo]) -> Bool {
       var isValid = true
       if let email = arrData[0].value, email.isEmpty {
         Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter your feedback comments".localizedString())
         isValid = false
       }
       return isValid
     }
    
    private func getFeedbackParams(arrData: [CellInfo])-> APIParams {
      var params: APIParams = APIParams()
      params[ConstantAPIKeys.feedback] = arrData[0].value as AnyObject
      return params
    }
}
