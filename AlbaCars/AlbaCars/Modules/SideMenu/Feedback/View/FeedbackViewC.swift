//
//  FeedbackViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class FeedbackViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var feedbackTableView: UITableView!
    @IBOutlet var headerView: UIView!
    
    //MARK: - Variables
    var feedbackDataSource: [CellInfo] = []
    var viewModel: FeedbackVModeling?
    
    //MARK: - LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setup()
    }
    
    deinit {
        print("deinit FeedbackViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Feedbacks".localizedString(), leftBarButtonsType: [.hamburgerBlack], rightBarButtonsType: [])
        self.recheckVM()
        self.setupTableView()
        self.setupDataSource()
    }
    
    func setupDataSource() {
        if let dataSource = self.viewModel?.getFeedbackDataSource() {
            self.feedbackDataSource = dataSource
        }        
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = FeedbackViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.feedbackTableView.delegate = self
        self.feedbackTableView.dataSource = self
        self.feedbackTableView.separatorStyle = .none
        self.feedbackTableView.allowsSelection = false
        self.feedbackTableView.tableHeaderView = self.headerView
    }
    
    private func registerNibs() {
        self.feedbackTableView.register(TextViewCell.self)
        self.feedbackTableView.register(BottomButtonCell.self)
    }
    //MARK: - API Methods
    func requestFeedbackAPI() {
        self.viewModel?.requestFeedbackAPI(arrData: self.feedbackDataSource) { (success) in
            if success {
                Threads.performTaskInMainQueue {
                    // Alert
                     self.setupDataSource()
                     self.feedbackTableView.reloadData()
                }
            }
        }
    }
}
