//
//  cardList.swift
//  AlbaCars
//
//  Created by Narendra on 4/11/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

class CardList: Codable {
  
  //Variables
  var cardType: Int?
  var cardNumber: String?
  var token: String?
  var cardTypeImage: String?
}

