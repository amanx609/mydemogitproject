//
//  PaymentViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import Foundation
import UIKit

protocol PaymentViewModeling: BaseVModeling {
    func getPaymentDataSource() -> [CellInfo]
    func requestCardListAPI(completion: @escaping (Any)-> Void)
    func requestDeleteCardAPI(params: APIParams,completion: @escaping (Bool)-> Void)
}

class PaymentViewM: PaymentViewModeling {
    
    func getPaymentDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Title Cell
        let titleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "My Cards".localizedString(), info: nil, height: Constants.CellHeightConstants.height_30)
        array.append(titleCell)

        //Payment Cell
        let paymentCell = CellInfo(cellType: .PaymentCell, placeHolder: "", value: "", info: nil, height: Constants.CellHeightConstants.height_70)
        array.append(paymentCell)
        array.append(paymentCell)

        return array
    }
    
    //MARK: - API Methods
    func requestCardListAPI(completion: @escaping (Any)-> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .listCard)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [[String: AnyObject]],
                    let cardListDataArray = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let cardListData = try decoder.decode([CardList].self, from: cardListDataArray)
                        completion(cardListData)
                    } catch let error {
                        print(error)
                    }
                } else {
                    completion([])
                }
            }
        }
    }
    
    func requestDeleteCardAPI(params: APIParams,completion: @escaping (Bool)-> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .deleteCard(param: params))) { (response, success) in
            
            if success {
                
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                     {
                    completion(true)
                } else {
                    completion(false)
                }
            }
        }
    }
}
