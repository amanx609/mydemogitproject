//
//  PaymentViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension PaymentViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.paymentDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cardList = self.paymentDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cardList: cardList)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.CellHeightConstants.height_70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !self.isSideMenu {
            let cardList = self.paymentDataSource[indexPath.row]
            Threads.performTaskInMainQueue {
                self.completionBlock?(Helper.toString(object: cardList.token))
                self.dismiss(animated: true)
            }
        }
    }
}

extension PaymentViewC {

    func getCell(tableView: UITableView, indexPath: IndexPath, cardList: CardList) -> UITableViewCell {
        let cell: PaymentCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate =  self
        cell.deleteButton.isHidden = !self.isSideMenu
        cell.radioButton.isHidden = self.isSideMenu

        cell.configureView(cardList: cardList)
        return cell
    }
}

//MARK: - PaymentCellDelegate
extension PaymentViewC: PaymentCellDelegate {
    
    func didTapDelete(cell: PaymentCell) {
        guard let indexPath = self.paymentTableView.indexPath(for: cell) else { return }
        let cardList = self.paymentDataSource[indexPath.row]
        
        Alert.showAlertWithActionWithCancel(title: StringConstants.Text.AppName.localizedString(), message: StringConstants.Message.areYouSureYouWantToDelete.localizedString(), actionTitle: StringConstants.Text.Yes.localizedString(), cancelTitle: StringConstants.Text.No.localizedString(), action: { (_) in
            self.requestDeleteCardAPI(paymentToken: Helper.toString(object: cardList.token))
        }, cancelAction: { (_) in

        })
        
    }
}
