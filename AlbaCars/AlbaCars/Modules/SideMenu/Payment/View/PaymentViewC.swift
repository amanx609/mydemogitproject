//
//  PaymentViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class PaymentViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var paymentTableView: UITableView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var promoCodeView: UIView!
    @IBOutlet var payWithNewButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var noDataView: UIView!

    
    //MARK: - Variables
    var paymentDataSource: [CardList] = []
    var viewModel: PaymentViewModeling?
    var isSideMenu = true
    var completionBlock : ((String) -> Void)?
    
    //MARK: - LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setup()
    }
    
    deinit {
        print("deinit PaymentViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Payment".localizedString(), leftBarButtonsType: isSideMenu == true ? [.hamburgerBlack] : [.cross], rightBarButtonsType: [])
        
        self.recheckVM()
        self.setupTableView()
        /// self.requestCardListAPI()
        
        promoCodeView.isHidden = true
        // payWithNewButton.isHidden = isSideMenu
        
        if !isSideMenu {
            payWithNewButton.isHidden = false
            gradientView.isHidden = false
            self.setupTableView()
            self.setupView()
        } else {
            payWithNewButton.isHidden = true
            gradientView.isHidden = true
            self.requestCardListAPI()
            
        }
        
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = PaymentViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.paymentTableView.delegate = self
        self.paymentTableView.dataSource = self
        self.paymentTableView.separatorStyle = .none
        // self.paymentTableView.allowsSelection = false
        self.paymentTableView.tableFooterView = self.footerView
    }
    
    private func registerNibs() {
        self.paymentTableView.register(SideMenuCell.self)
        self.paymentTableView.register(PaymentCell.self)
    }
    
    private func roundCorners() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
        }
    }
    
    private func setupView() {
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        //self.addNewCardButton.setTitle(StringConstants.Text.saveCard.localizedString(), for: .normal)
    }
    
    //MARK: - API Methods
    func requestCardListAPI() {
        
        self.viewModel?.requestCardListAPI() { (responseData) in
            
            if let data = responseData as? [CardList] {
                
                self.paymentDataSource = data
                if self.paymentDataSource.count == 0 {
                    self.noDataView.isHidden = false
                    self.paymentTableView.isHidden = true
                } else {
                    self.noDataView.isHidden = true
                    self.paymentTableView.isHidden = false
                }
                Threads.performTaskInMainQueue {
                    self.paymentTableView.reloadData()
                }
            }
        }
    }
    
    func requestDeleteCardAPI(paymentToken: String) {
        var params = APIParams()
        params[ConstantAPIKeys.paymentToken] = paymentToken as AnyObject
        
        self.viewModel?.requestDeleteCardAPI(params: params) { (responseData) in
            
            Threads.performTaskInMainQueue {
                self.requestCardListAPI()
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func tapAddNewCard(_ sender: UIButton) {
        let addCardVC = DIConfigurator.sharedInstance.getAddNewCardVC()
        self.navigationController?.pushViewController(addCardVC, animated: true)
    }
    
    @IBAction func payWithNewCard(_ sender: UIButton) {
        
        Threads.performTaskInMainQueue {
            self.completionBlock?("")
            self.dismiss(animated: true)
        }
    }
    
    @IBAction func tapAddPromoCode(_ sender: UIButton) {
        
    }
}
