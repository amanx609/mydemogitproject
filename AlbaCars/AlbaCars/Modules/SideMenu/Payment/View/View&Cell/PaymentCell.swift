//
//  PaymentCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol PaymentCellDelegate: class {
    func didTapDelete(cell: PaymentCell)
}

class PaymentCell:  BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var radioButton: UIButton!

    //MARK: - Variables
    weak var delegate: PaymentCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Public Methods
    func configureView(cardList: CardList) {
        self.cardNumberLabel.text = cardList.cardNumber
    }
    
    //MARK: - IBActions
    @IBAction func tapDelete(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapDelete(cell: self)
        }
    }
    
}
