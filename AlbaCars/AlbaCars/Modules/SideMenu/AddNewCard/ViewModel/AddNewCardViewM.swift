//
//  AddNewCardViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol AddNewCardViewModeling: BaseVModeling {
    func getAddNewCardDataSource() -> [CellInfo]
}

class AddNewCardViewM: AddNewCardViewModeling {
    
    func getAddNewCardDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Title Cell
        let titleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Card Number".localizedString(), info: nil, height: Constants.CellHeightConstants.height_30)
        array.append(titleCell)
        
        //Card Cell
        var cardInfo = [String: AnyObject]()
        cardInfo[Constants.UIKeys.inputType] = TextInputType.cardNumber as AnyObject
        let cardCell = CellInfo(cellType: .TextInputCell, placeHolder: "XXXX     XXXX      XXXX      XXXX".localizedString(), value: "", info: cardInfo, height: Constants.CellHeightConstants.height_80)
        array.append(cardCell)
        
        //Expire Cell
        let expireCell = CellInfo(cellType: .CardInfoCell, placeHolder: "", value: "", info: cardInfo, height: Constants.CellHeightConstants.height_120)
        array.append(expireCell)
        
        //Country Title Cell
        let countryTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Country".localizedString(), info: nil, height: Constants.CellHeightConstants.height_30)
        array.append(countryTitleCell)
        
        //Country Cell
        let countryCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "United Arab Emirates".localizedString(), info: nil, height: Constants.CellHeightConstants.height_80)
        array.append(countryCell)
        
        return array
    }
    
}
