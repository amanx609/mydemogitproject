//
//  AddNewCardViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import Foundation
import UIKit

extension AddNewCardViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addNewCardDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.addNewCardDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.addNewCardDataSource[indexPath.row].height
    }
}

extension AddNewCardViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .SideMenuCell:
            let cell: SideMenuCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .TextInputCell:
            let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .CardInfoCell:
            let cell: CardInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            //cell.configureView(cellInfo: cellInfo)
            return cell
            case .DropDownCell:
            let cell: DropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - TextInputCellDelegate
extension AddNewCardViewC: TextInputCellDelegate {
    func tapNextKeyboard(cell: TextInputCell) {
        self.view.endEditing(true)
    }
    
    func didChangeText(cell: TextInputCell, text: String) {
        guard let indexPath = self.addNewCardTableView.indexPath(for: cell) else { return }
        self.addNewCardDataSource[indexPath.row].value = text
    }
}

//MARK: - DropDownCellDelegate
extension AddNewCardViewC: DropDownCellDelegate {
  func didTapDropDownCell(cell: DropDownCell) {
    guard let indexPath = self.addNewCardTableView.indexPath(for: cell) else { return }
  }
}
