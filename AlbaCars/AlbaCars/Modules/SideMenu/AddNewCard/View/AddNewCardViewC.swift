//
//  AddNewCardViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class AddNewCardViewC: BaseViewC {

   //MARK: - IBOutlets
    @IBOutlet weak var addNewCardTableView: UITableView!
    @IBOutlet weak var addNewCardButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    
    //MARK: - Variables
    var addNewCardDataSource: [CellInfo] = []
    var viewModel: AddNewCardViewModeling?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setup()
    }
    
    deinit {
        print("deinit AddNewCardViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Add New Card".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.recheckVM()
        self.setupTableView()
        self.setupView()
        self.roundCorners()
        self.setupDataSource()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = AddNewCardViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.addNewCardTableView.delegate = self
        self.addNewCardTableView.dataSource = self
        self.addNewCardTableView.separatorStyle = .none
        self.addNewCardTableView.allowsSelection = false
    }
    
    private func roundCorners() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
        }
    }
    
    private func setupView() {
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        self.addNewCardButton.setTitle(StringConstants.Text.saveCard.localizedString(), for: .normal)
    }
    
    private func registerNibs() {
        self.addNewCardTableView.register(SideMenuCell.self)
        self.addNewCardTableView.register(TextInputCell.self)
        self.addNewCardTableView.register(CardInfoCell.self)
        self.addNewCardTableView.register(DropDownCell.self)        
    }
    
    func setupDataSource() {
        if let dataSource = self.viewModel?.getAddNewCardDataSource() {
            self.addNewCardDataSource = dataSource
        }
    }
    
    //MARK: - API Methods
    func requestAddCardAPI() {
        
    }
    
    //MARK: - IBActions
    @IBAction func tapSaveCard(_ sender: UIButton) {
        
    }

}
