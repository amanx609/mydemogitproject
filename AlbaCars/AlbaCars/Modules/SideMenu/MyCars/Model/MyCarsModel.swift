//
//  MyCarsModel.swift
//  AlbaCars
//
//  Created by Narendra on 12/17/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

//My Cars
class MyCarsModel: Codable {
  //Variables
    var id: Int?
    var brandName: String?
    var modelName: String?
    var typeName: String?
    var type: Int?
    var plateNumber: String?
    var model: Int?
    var mileage: Int?
    var make: Int?
    var engine: Double?
    var cylinders: String?
    var year: Int?
    var serviceHistoryDocuments: [ServiceHistoryDocuments]?
}

class ServiceHistoryDocuments: Codable {
  //Variables
    var createdAt: String?
    var document: String?
}

//serviceHistoryDocuments
