//
//  MyCarsCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class MyCarsCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var carNumberView: UIView!
    @IBOutlet weak var carNumberLabel: UILabel!
    @IBOutlet weak var carBrandLabel: UILabel!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var carTypeLabel: UILabel!
    @IBOutlet weak var carCylindresLabel: UILabel!
    @IBOutlet weak var carEngineSizeLabel: UILabel!
    @IBOutlet weak var carMileageLabel: UILabel!
    @IBOutlet weak var carYearLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupView() {
        
        Threads.performTaskInMainQueue {
            self.carNumberView.roundCorners([.bottomLeft,.bottomRight], radius: Constants.UIConstants.sizeRadius_12)
            self.bgView.roundCorners(Constants.UIConstants.sizeRadius_3half)
            self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))

        }
    }
    
    //MARK: - Public Methods
    func configureView(myCarData: MyCarsModel) {
        self.carNumberLabel.text = Helper.toString(object: myCarData.plateNumber)
        self.carBrandLabel.text =  Helper.toString(object: myCarData.brandName)
        self.carModelLabel.text =  Helper.toString(object: myCarData.modelName)
        self.carTypeLabel.text =   Helper.toString(object: myCarData.typeName)
        self.carCylindresLabel.text = Helper.toString(object: myCarData.cylinders)
        self.carEngineSizeLabel.text = Helper.toString(object: myCarData.engine)
        self.carMileageLabel.text = "\(Helper.toString(object: myCarData.mileage)) KM"
        self.carYearLabel.text = Helper.toString(object: myCarData.year)
    }
}
