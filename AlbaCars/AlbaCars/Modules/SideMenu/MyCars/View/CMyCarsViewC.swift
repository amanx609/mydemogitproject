//
//  CMyCarsViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CMyCarsViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var myCarTableView: UITableView!
    @IBOutlet weak var addNewCarButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var noDataView: UIView!

    
    //MARK: - Variables
    var viewModel: MyCarsVModeling?
    var mycarDataSource: [MyCarsModel] = []
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.nextPageNumber = 1
        self.mycarDataSource = []
        self.requestMyCarsAPI()
    }
    
    deinit {
        print("deinit CMyCarsViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupTableView()
        self.roundCorners()
        self.setupView()
        self.setupNavigationBarTitle(title: StringConstants.Text.myCars.localizedString(), leftBarButtonsType: [.hamburgerBlack], rightBarButtonsType: [])
        
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = MyCarsViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.myCarTableView.delegate = self
        self.myCarTableView.dataSource = self
        self.myCarTableView.separatorStyle = .none
       // self.myCarTableView.allowsSelection = false
        self.myCarTableView.backgroundColor = .white
    }
    
    private func roundCorners() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.addNewCarButton.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
        }
    }
    
    private func setupView() {
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        self.addNewCarButton.setTitle(StringConstants.Text.addNewCar.localizedString(), for: .normal)
    }
    
    private func registerNibs() {
        self.myCarTableView.register(MyCarsCell.self)
    }
    
    //MARK: - IBActions
    
    @IBAction func didTapAddNewCarButton(_ sender: Any) {
        let addNewCarViewC = DIConfigurator.sharedInstance.getAddNewCarViewC()
        addNewCarViewC.cAddNewCarType = .add
        self.navigationController?.pushViewController(addNewCarViewC, animated: true)
    }
    
    //MARK: - API Methods
    func requestMyCarsAPI() {
        self.viewModel?.requestMyCarsAPI(page: self.nextPageNumber) { (responseData, nextPageNum, perPage) in
            if let data = responseData as? [MyCarsModel] {
                self.nextPageNumber = nextPageNum
                self.perPage = perPage
                self.mycarDataSource = self.mycarDataSource + data
                if self.mycarDataSource.count == 0 {
                    self.noDataView.isHidden = false
                    self.myCarTableView.isHidden = true
                } else {
                    self.noDataView.isHidden = true
                    self.myCarTableView.isHidden = false
                }
                Threads.performTaskInMainQueue {
                    self.myCarTableView.reloadData()
                }
            }
        }
    }
}
