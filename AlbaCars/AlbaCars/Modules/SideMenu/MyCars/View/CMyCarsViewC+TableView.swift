//
//  CMyCarsViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension CMyCarsViewC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mycarDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: self.mycarDataSource[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.CellHeightConstants.height_130
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let lastVisibleRow = self.myCarTableView.indexPathsForVisibleRows?.last?.row else { return }
        if (self.mycarDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
            self.requestMyCarsAPI()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myCarDetailsViewC = DIConfigurator.sharedInstance.getMyCarDetailsViewC()
        myCarDetailsViewC.myCarsModel = self.mycarDataSource[indexPath.row]
        self.navigationController?.pushViewController(myCarDetailsViewC, animated: true)
    }
}

extension CMyCarsViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: MyCarsModel) -> UITableViewCell {
        let cell: MyCarsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(myCarData: cellInfo)
        return cell
    }
}
