//
//  MyCarsViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol MyCarsVModeling: BaseVModeling {
    func requestMyCarsAPI(page: Int, completion: @escaping (Any, Int, Int)-> Void)
}

class MyCarsViewM: MyCarsVModeling {
    
    func requestMyCarsAPI(page: Int, completion: @escaping (Any, Int, Int) -> Void) {
        let myCarsListParam = getAPIParams(page: page)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .myCars(param: myCarsListParam))) { (response, success) in
            if success {
                
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let data = result[kData] as? [[String: AnyObject]], let nextPageNumber = result[kNextPageNum] as? Int, let perPage = result[kPerPage] as? Int,
                    let mycarDataArray = data.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let mycarData = try decoder.decode([MyCarsModel].self, from: mycarDataArray)
                        completion(mycarData, nextPageNumber, perPage)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
    private func getAPIParams(page: Int) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.page] = page as AnyObject
        return param
    }
    
}
