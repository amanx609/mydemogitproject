//
//  NotificationSettingsViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol NotificationSettingsViewModeling {
    func getNotificationSettingsDataSource(notificationSettingModel:NotificationSettingModel?) -> [CellInfo]
    func requestNotificationSettingAPI(completion: @escaping (NotificationSettingModel) -> Void)
    func requestpostNotificationSettingAPI(param: [CellInfo], notificationId: Int, completion: @escaping (NotificationSettingModel) -> Void)
    func requestVehicleBrandAPI(completion: @escaping (Bool, [VehicleBrand])-> Void)
}

class NotificationSettingsViewM: NotificationSettingsViewModeling {
    
    func getNotificationSettingsDataSource(notificationSettingModel:NotificationSettingModel?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        if let userType = UserSession.shared.userType  {
            if userType == .supplier {
                array = self.getSupplierNotificationSettingsDataSource(notificationSettingModel: notificationSettingModel)
                return array
            } else if userType == .dealer {
                array = self.getDealerNotificationSettingsDataSource(notificationSettingModel: notificationSettingModel)
                return array
            }
            
        }
        
        //question Label Cell
        
        let questionCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: StringConstants.Text.carSelectionQuestion.localizedString(), height: Constants.CellHeightConstants.height_80)
        array.append(questionCell)
        
        if let userType = UserSession.shared.userType, userType == .dealer {
            //For Auctions Label Cell
            
            let questionCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: StringConstants.Text.forAuctions.localizedString(), height: Constants.CellHeightConstants.height_80)
            array.append(questionCell)
        }
        
        // Years Cell
        var yearCellInfo = [String: AnyObject]()
        yearCellInfo[Constants.UIKeys.years] = notificationSettingModel?.years as AnyObject
        yearCellInfo[Constants.UIKeys.dropdownType] = LabelDropDownType.year as AnyObject
        
        let yearCell = CellInfo(cellType: .LabelDropDownCell, placeHolder: StringConstants.Text.selectYears.localizedString(), value: "", info: yearCellInfo, height: Constants.CellHeightConstants.height_80)
        array.append(yearCell)
        
        // Brands Cell
        var brandCellInfo = [String: AnyObject]()
        brandCellInfo[Constants.UIKeys.brands] = notificationSettingModel?.brands as AnyObject
        brandCellInfo[Constants.UIKeys.brandId] = notificationSettingModel?.brandId as AnyObject
        brandCellInfo[Constants.UIKeys.dropdownType] = LabelDropDownType.brands as AnyObject
        
        let brandCell = CellInfo(cellType: .LabelDropDownCell, placeHolder: StringConstants.Text.selectBrands.localizedString(), value: "", info: brandCellInfo,  height: Constants.CellHeightConstants.height_80)
        array.append(brandCell)
        
        // Select Mileage Cell
        let selectMileageCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: StringConstants.Text.selectMileage.localizedString(), height: Constants.CellHeightConstants.height_40)
        array.append(selectMileageCell)
        
        // Mileage RangeCell
        var rangeCellInfo = [String: AnyObject]()
        rangeCellInfo[Constants.UIKeys.minimum] = StringConstants.Text.minimum.localizedString() as AnyObject
        rangeCellInfo[Constants.UIKeys.maximum] = StringConstants.Text.maximum.localizedString() as AnyObject
        rangeCellInfo[Constants.UIKeys.minimumValue] = Helper.toInt(notificationSettingModel?.minimum_mileage) as AnyObject
        rangeCellInfo[Constants.UIKeys.maximumValue] = Helper.toInt(notificationSettingModel?.maximum_mileage) as AnyObject
        let mileageRangeCell = CellInfo(cellType: .MileageTextFieldCell, placeHolder: StringConstants.Text.carSelectionQuestion.localizedString(), value: "", info: rangeCellInfo, height: Constants.CellHeightConstants.height_110)
        array.append(mileageRangeCell)
        
        // Options Cell
        var optionsInfo = [String: AnyObject]()
        optionsInfo[Constants.UIKeys.dropdownType] = LabelDropDownType.option as AnyObject
        optionsInfo[Constants.UIKeys.value] = Helper.notificationOption(option: notificationSettingModel?.options ?? 0) as AnyObject
        let optionsCell = CellInfo(cellType: .LabelDropDownCell, placeHolder: StringConstants.Text.options.localizedString(), value: "", info: optionsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(optionsCell)
        
        if let userType = UserSession.shared.userType, userType == .dealer {
            // notification time label Cell
            let selectMileageCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: StringConstants.Text.notificationTimeBeforeAuction.localizedString(), height: Constants.CellHeightConstants.height_40)
            array.append(selectMileageCell)
            
            // Time Cell
            var timeInfo = [String: AnyObject]()
            timeInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "time-expire") as AnyObject
            timeInfo[Constants.UIKeys.time] = notificationSettingModel?.getTimeString() as AnyObject
            timeInfo[Constants.UIKeys.value] = notificationSettingModel?.time as AnyObject
            timeInfo[Constants.UIKeys.dropdownType] = LabelDropDownType.time as AnyObject
            
            let timeCell = CellInfo(cellType: .LabelDropDownCell, placeHolder: StringConstants.Text.times.localizedString(),value: "", info: timeInfo, height: Constants.CellHeightConstants.height_80)
            array.append(timeCell)
        }
        
        // Smart Tenders Cell
        var tenderCellInfo = [String: AnyObject]()
        tenderCellInfo[Constants.UIKeys.switchStatus] = notificationSettingModel?.smart_tenders as AnyObject
        let tenderCell = CellInfo(cellType: .LabelSwitchCell, value: StringConstants.Text.smartTenders.localizedString(), info: tenderCellInfo, height: Constants.CellHeightConstants.height_80)
        array.append(tenderCell)
        
        // Finance Application Cell
        var financeCellInfo = [String: AnyObject]()
        financeCellInfo[Constants.UIKeys.switchStatus] = notificationSettingModel?.finance_application as AnyObject
        let financeApplicationCell = CellInfo(cellType: .LabelSwitchCell, value: StringConstants.Text.financeApplication.localizedString(), info: financeCellInfo, height: Constants.CellHeightConstants.height_80)
        array.append(financeApplicationCell)
        
        // Finance Application Cell
        var saleCellInfo = [String: AnyObject]()
        //var sale = notificationSettingModel?.sale == nil ? notificationSettingModel
        saleCellInfo[Constants.UIKeys.switchStatus] = notificationSettingModel?.sale as AnyObject
        let saleCell = CellInfo(cellType: .LabelSwitchCell, value: StringConstants.Text.sale.localizedString(), info: saleCellInfo, height: Constants.CellHeightConstants.height_80)
        array.append(saleCell)
        
        // Offers & Deals Cell
        var offerDealsCellInfo = [String: AnyObject]()
        offerDealsCellInfo[Constants.UIKeys.switchStatus] = notificationSettingModel?.offers_deals as AnyObject
        let offerCell = CellInfo(cellType: .LabelSwitchCell, value: StringConstants.Text.offerDeals.localizedString(), info: offerDealsCellInfo, height: Constants.CellHeightConstants.height_80)
        array.append(offerCell)
        
        
        return array
    }
    
    func getDealerNotificationSettingsDataSource(notificationSettingModel:NotificationSettingModel?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        
        //question Label Cell
        
        let questionCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: StringConstants.Text.carSelectionQuestion.localizedString(), height: Constants.CellHeightConstants.height_80)
        array.append(questionCell)
        
        if let userType = UserSession.shared.userType, userType == .dealer {
            //For Auctions Label Cell
            
            let questionCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: StringConstants.Text.forAuctions.localizedString(), height: Constants.CellHeightConstants.height_80)
            array.append(questionCell)
        }
        
        // Years Cell
        var yearCellInfo = [String: AnyObject]()
        yearCellInfo[Constants.UIKeys.years] = notificationSettingModel?.years as AnyObject
        yearCellInfo[Constants.UIKeys.dropdownType] = LabelDropDownType.year as AnyObject
        
        let yearCell = CellInfo(cellType: .LabelDropDownCell, placeHolder: StringConstants.Text.selectYears.localizedString(), value: "", info: yearCellInfo, height: Constants.CellHeightConstants.height_80)
        array.append(yearCell)
        
        // Brands Cell
        var brandCellInfo = [String: AnyObject]()
        brandCellInfo[Constants.UIKeys.brands] = notificationSettingModel?.brands as AnyObject
        brandCellInfo[Constants.UIKeys.brandId] = notificationSettingModel?.brandId as AnyObject
        brandCellInfo[Constants.UIKeys.dropdownType] = LabelDropDownType.brands as AnyObject
        
        let brandCell = CellInfo(cellType: .LabelDropDownCell, placeHolder: StringConstants.Text.selectBrands.localizedString(), value: "", info: brandCellInfo,  height: Constants.CellHeightConstants.height_80)
        array.append(brandCell)
        
        // Select Mileage Cell
        let selectMileageCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: StringConstants.Text.selectMileage.localizedString(), height: Constants.CellHeightConstants.height_40)
        array.append(selectMileageCell)
        
        // Mileage RangeCell
        var rangeCellInfo = [String: AnyObject]()
        rangeCellInfo[Constants.UIKeys.minimum] = StringConstants.Text.minimum.localizedString() as AnyObject
        rangeCellInfo[Constants.UIKeys.maximum] = StringConstants.Text.maximum.localizedString() as AnyObject
        rangeCellInfo[Constants.UIKeys.minimumValue] = Helper.toInt(notificationSettingModel?.minimum_mileage) as AnyObject
        rangeCellInfo[Constants.UIKeys.maximumValue] = Helper.toInt(notificationSettingModel?.maximum_mileage) as AnyObject
        let mileageRangeCell = CellInfo(cellType: .MileageTextFieldCell, placeHolder: StringConstants.Text.carSelectionQuestion.localizedString(), value: "", info: rangeCellInfo, height: Constants.CellHeightConstants.height_110)
        array.append(mileageRangeCell)
        
        // Options Cell
        var optionsInfo = [String: AnyObject]()
        optionsInfo[Constants.UIKeys.dropdownType] = LabelDropDownType.option as AnyObject
        optionsInfo[Constants.UIKeys.value] = Helper.notificationOption(option: notificationSettingModel?.options ?? 0) as AnyObject
        let optionsCell = CellInfo(cellType: .LabelDropDownCell, placeHolder: StringConstants.Text.options.localizedString(), value: "" ,info: optionsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(optionsCell)
        
        // notification time label Cell
        let selectMileageCell1 = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: StringConstants.Text.notificationTimeBeforeAuction.localizedString(), height: Constants.CellHeightConstants.height_40)
        array.append(selectMileageCell1)
        
        // Time Cell
        var timeInfo = [String: AnyObject]()
        timeInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "time-expire") as AnyObject
        timeInfo[Constants.UIKeys.time] = notificationSettingModel?.getTimeString() as AnyObject
        timeInfo[Constants.UIKeys.value] = notificationSettingModel?.time as AnyObject
        timeInfo[Constants.UIKeys.dropdownType] = LabelDropDownType.time as AnyObject
        let timeCell = CellInfo(cellType: .LabelDropDownCell, placeHolder: StringConstants.Text.times.localizedString(),value: "", info: timeInfo, height: Constants.CellHeightConstants.height_80)
        array.append(timeCell)
        
        
        // Smart Tenders Cell
        var tenderCellInfo = [String: AnyObject]()
        tenderCellInfo[Constants.UIKeys.switchStatus] = notificationSettingModel?.smart_tenders as AnyObject
        let tenderCell = CellInfo(cellType: .LabelSwitchCell, value: StringConstants.Text.smartTenders.localizedString(), info: tenderCellInfo, height: Constants.CellHeightConstants.height_80)
        array.append(tenderCell)
        
        // Finance Application Cell
        var financeCellInfo = [String: AnyObject]()
        financeCellInfo[Constants.UIKeys.switchStatus] = Helper.toBool(notificationSettingModel?.fleet_biddings)  as AnyObject
        let financeApplicationCell = CellInfo(cellType: .LabelSwitchCell, value: StringConstants.Text.fleetBiddings.localizedString(), info: financeCellInfo, height: Constants.CellHeightConstants.height_80)
        array.append(financeApplicationCell)
        
        // Finance Application Cell
        var saleCellInfo = [String: AnyObject]()
        //var sale = notificationSettingModel?.sale == nil ? notificationSettingModel
        saleCellInfo[Constants.UIKeys.switchStatus] = Helper.toBool(notificationSettingModel?.leads) as AnyObject
        let saleCell = CellInfo(cellType: .LabelSwitchCell, value: StringConstants.Text.leads.localizedString(), info: saleCellInfo, height: Constants.CellHeightConstants.height_80)
        array.append(saleCell)
        
        
        return array
    }
    
    
    func getSupplierNotificationSettingsDataSource(notificationSettingModel:NotificationSettingModel?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        // Title Cell
        let titleCell = CellInfo(cellType: .BottomLabelRegularTextCell, placeHolder: StringConstants.Text.notificationTimeExpires.localizedString(),value: "", info: nil, height: Constants.CellHeightConstants.height_40)
        array.append(titleCell)
        
        // Time Cell
        var timeInfo = [String: AnyObject]()
        timeInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "time-expire") as AnyObject
        timeInfo[Constants.UIKeys.time] = notificationSettingModel?.getTimeString() as AnyObject
        timeInfo[Constants.UIKeys.value] = notificationSettingModel?.time as AnyObject
        timeInfo[Constants.UIKeys.dropdownType] = LabelDropDownType.time as AnyObject
        let timeCell = CellInfo(cellType: .LabelDropDownCell, placeHolder: StringConstants.Text.times.localizedString(),value: "", info: timeInfo, height: Constants.CellHeightConstants.height_80)
        array.append(timeCell)
        
        // New Tender
        var newTenderInfo = [String: AnyObject]()
        newTenderInfo[Constants.UIKeys.switchStatus] = Helper.toBool(notificationSettingModel?.new_tender) as AnyObject
        let newTenderCell = CellInfo(cellType: .LabelSwitchCell, value: StringConstants.Text.newTender.localizedString(), info: newTenderInfo, height: Constants.CellHeightConstants.height_80)
        array.append(newTenderCell)
        
        // Bids Accepted
        var bidsAcceptedInfo = [String: AnyObject]()
        bidsAcceptedInfo[Constants.UIKeys.switchStatus] = Helper.toBool(notificationSettingModel?.bids_accepted) as AnyObject
        let bidsAcceptedCell = CellInfo(cellType: .LabelSwitchCell, value: StringConstants.Text.bidsAccepted.localizedString(), info: bidsAcceptedInfo, height: Constants.CellHeightConstants.height_80)
        array.append(bidsAcceptedCell)
        
        // Bids Rejected
        var bidsRejectedInfo = [String: AnyObject]()
        bidsRejectedInfo[Constants.UIKeys.switchStatus] = Helper.toBool(notificationSettingModel?.bids_rejected) as AnyObject
        let bidsRejectedCell = CellInfo(cellType: .LabelSwitchCell, value: StringConstants.Text.bidsRejected.localizedString(), info: bidsRejectedInfo, height: Constants.CellHeightConstants.height_80)
        array.append(bidsRejectedCell)
        
        // Job Completion
        var jobCompletionInfo = [String: AnyObject]()
        jobCompletionInfo[Constants.UIKeys.switchStatus] = Helper.toBool(notificationSettingModel?.job_completion) as AnyObject
        let jobCompletionCell = CellInfo(cellType: .LabelSwitchCell, value: StringConstants.Text.jobCompletion.localizedString(), info: jobCompletionInfo, height: Constants.CellHeightConstants.height_80)
        array.append(jobCompletionCell)
        
        // Payment Completion
        var paymentCompletionInfo = [String: AnyObject]()
        paymentCompletionInfo[Constants.UIKeys.switchStatus] = Helper.toBool(notificationSettingModel?.payment_completion) as AnyObject
        let paymentCompletionCell = CellInfo(cellType: .LabelSwitchCell, value: StringConstants.Text.paymentCompletion.localizedString(), info: paymentCompletionInfo, height: Constants.CellHeightConstants.height_80)
        array.append(paymentCompletionCell)
        
        return array
        
    }
    
    func requestVehicleBrandAPI(completion: @escaping (Bool, [VehicleBrand])-> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .vehicleBrand)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let _ = safeResponse[ConstantAPIKeys.result] as? [[String: AnyObject]],
                    let vehicleBrandsData = safeResponse.toJSONData(),
                    let vehicleBrandsList = VehicleBrandList(jsonData: vehicleBrandsData) {
                    completion(success, vehicleBrandsList.vehicleBrands ?? [])
                }
            }
        }
    }
    
    
    func requestNotificationSettingAPI(completion: @escaping (NotificationSettingModel) -> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getNotificationSettings)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let notificationSettingModel = try decoder.decode(NotificationSettingModel.self, from: resultData)
                        completion(notificationSettingModel)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
    func requestpostNotificationSettingAPI(param: [CellInfo], notificationId: Int, completion: @escaping (NotificationSettingModel) -> Void) {
        var params = APIParams()
        if let userType = UserSession.shared.userType {
            switch userType {
            case .customer:
                if self.validateCMileageParameters(arrData: param) {
                    params = getCNotificationAPIParameters(arrData: param, notificationId: notificationId)
                } else {
                    return
                }
                
            case .dealer:
                if self.validateDMileageParameters(arrData: param) {
                    params = getDNotificationAPIParameters(arrData: param, notificationId: notificationId)
                    
                } else {
                    return
                }
                
            case .supplier:
                params = getSNotificationAPIParameters(arrData: param, notificationId: notificationId)
            }
        }
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .postNotificationSettings(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let notificationSettingModel = try decoder.decode(NotificationSettingModel.self, from: resultData)
                        completion(notificationSettingModel)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
    func validateCMileageParameters(arrData: [CellInfo]) -> Bool {
        var isValid = true
        if let minMileage = arrData[4].info?[Constants.UIKeys.minimumValue] as? Int, let maxMileage = arrData[4].info?[Constants.UIKeys.maximumValue] as? Int, minMileage > maxMileage {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Minimum Mileage cannot be greater than Maximum Mileage".localizedString())
            isValid = false
        }
        return isValid
    }
    
    func validateDMileageParameters(arrData: [CellInfo]) -> Bool {
        var isValid = true
        if let minMileage = arrData[5].info?[Constants.UIKeys.minimumValue] as? Int, let maxMileage = arrData[5].info?[Constants.UIKeys.maximumValue] as? Int, minMileage > maxMileage {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Minimum Mileage cannot be greater than Maximum Mileage".localizedString())
            isValid = false
        }
        return isValid
    }
    
    
    //Customer Notifications Parameters
    
    func getCNotificationAPIParameters(arrData: [CellInfo], notificationId: Int) -> APIParams {
        var params = APIParams()
        
        params[ConstantAPIKeys.notificationId] = notificationId as AnyObject
        params[ConstantAPIKeys.brandId] = arrData[2].info?[Constants.UIKeys.brandId] as AnyObject
        params[ConstantAPIKeys.brands] = arrData[2].info?[Constants.UIKeys.brands] as AnyObject
        params[ConstantAPIKeys.years] = arrData[1].info?[Constants.UIKeys.years] as AnyObject
        params[ConstantAPIKeys.minimum_mileage] =  arrData[4].info?[Constants.UIKeys.minimumValue] as AnyObject
        params[ConstantAPIKeys.maximum_mileage] = arrData[4].info?[Constants.UIKeys.maximumValue]  as AnyObject
        //for 2&3 only
        //params[ConstantAPIKeys.time] = "arrDa" as AnyObject
        params[ConstantAPIKeys.options] = Helper.toInt(arrData[5].info?[Constants.UIKeys.id]) as AnyObject
        
        params[ConstantAPIKeys.notifications] = ["smart_tenders": Helper.toBool(arrData[6].info?[Constants.UIKeys.switchStatus]),
                                                 "finance_application": Helper.toBool(arrData[7].info?[Constants.UIKeys.switchStatus]),
                                                 "sale": Helper.toBool(arrData[8].info?[Constants.UIKeys.switchStatus]),
                                                 "offer_deals": Helper.toBool(arrData[9].info?[Constants.UIKeys.switchStatus])] as AnyObject
        
        
        
        
        return params
    }
    
    func getSNotificationAPIParameters(arrData: [CellInfo], notificationId: Int) -> APIParams {
        var params = APIParams()
        
        params[ConstantAPIKeys.notificationId] = notificationId as AnyObject
        params[ConstantAPIKeys.time] = arrData[1].info?[Constants.UIKeys.value] as AnyObject as AnyObject
        
        params[ConstantAPIKeys.notifications] = ["new_tender": arrData[2].info?[Constants.UIKeys.switchStatus] ,"bids_accepted": arrData[3].info?[Constants.UIKeys.switchStatus],
                                                 "bids_rejected": arrData[4].info?[Constants.UIKeys.switchStatus],"job_completion":arrData[5].info?[Constants.UIKeys.switchStatus], "payment_completion":arrData[6].info?[Constants.UIKeys.switchStatus]] as AnyObject
        
        
        return params
    }
    
    func getDNotificationAPIParameters(arrData: [CellInfo], notificationId: Int) -> APIParams {
        var params = APIParams()
        
        params[ConstantAPIKeys.notificationId] = notificationId as AnyObject
        params[ConstantAPIKeys.brandId] = arrData[3].info?[Constants.UIKeys.brandId] as AnyObject
        params[ConstantAPIKeys.brands] = arrData[3].info?[Constants.UIKeys.brands] as AnyObject
        params[ConstantAPIKeys.years] = arrData[2].info?[Constants.UIKeys.years] as AnyObject
        params[ConstantAPIKeys.minimum_mileage] = arrData[5].info?[Constants.UIKeys.minimumValue] as AnyObject
        params[ConstantAPIKeys.maximum_mileage] = arrData[5].info?[Constants.UIKeys.maximumValue] as AnyObject as AnyObject
        //for 2&3 only
        //params[ConstantAPIKeys.time] = "arrDa" as AnyObject
        params[ConstantAPIKeys.options] = Helper.toInt(arrData[6].info?[Constants.UIKeys.id]) as AnyObject
        params[ConstantAPIKeys.time] = arrData[8].info?[Constants.UIKeys.value] as AnyObject
        params[ConstantAPIKeys.notifications] = ["smart_tenders":arrData[9].info?[Constants.UIKeys.switchStatus] ,"fleet_biddings":arrData[10].info?[Constants.UIKeys.switchStatus],
                                                 "leads":arrData[11].info?[Constants.UIKeys.switchStatus]] as AnyObject
        
        
        return params
    }
    
}

