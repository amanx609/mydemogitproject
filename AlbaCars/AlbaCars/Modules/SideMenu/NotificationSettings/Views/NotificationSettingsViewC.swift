//
//  NotificationSettingsViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

class NotificationSettingsViewC: BaseViewC {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var notificationSettingsTableView: TPKeyboardAvoidingTableView!
    
    //MARK: - Variables
    var viewModel: NotificationSettingsViewModeling?
    var notificationSettingsDataSource: [CellInfo] = []
    var cNotificationDataSource: NotificationSettingModel?
    var notificationId: Int?
    var vehicleBrandDataSource: [VehicleBrand] = []
    var vehicleYearDataSource: [[String:AnyObject]] = []
    var addCarViewModel: DAuctionFilterVModeling?
    
    //MARK: - Variables
    
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func navigationButtonTapped(_ sender: AnyObject) {
           guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
           switch buttonType {
           case .hamburgerBlack:
            self.showLeftViewAnimated(sender)
           case .apply: self.requestPostCustomerNotificationsAPI()
           default: break
           }
       }
    
    deinit {
        print("deinit NotificationSettingsViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupTableView()
        self.setupNavigationBarTitle(title: "Notification Settings".localizedString(), leftBarButtonsType: [.hamburgerBlack], rightBarButtonsType: [.apply])
        self.loadDataSource()
        if let userType = UserSession.shared.userType, userType == .supplier {
            self.requestGetCustomerNotificationsAPI()
        } else {
            self.requestVehicleBrandAPI()
        }
    }
    
    
    
    private func loadDataSource() {

                if let dataSource = self.viewModel?.getNotificationSettingsDataSource(notificationSettingModel: cNotificationDataSource) {
                    self.notificationSettingsDataSource = dataSource
                }
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = NotificationSettingsViewM()
        }
        
        if self.addCarViewModel == nil {
            self.addCarViewModel = DAuctionFilterViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.notificationSettingsTableView.backgroundColor = UIColor.white
        self.notificationSettingsTableView.delegate = self
        self.notificationSettingsTableView.dataSource = self
        self.notificationSettingsTableView.separatorStyle = .none
        self.notificationSettingsTableView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.notificationSettingsTableView.register(SideMenuCell.self)
        self.notificationSettingsTableView.register(LabelDropDownCell.self)
        self.notificationSettingsTableView.register(LabelSwitchCell.self)
        self.notificationSettingsTableView.register(MileageTextFieldCell.self)
        self.notificationSettingsTableView.register(BottomLabelCell.self)
        
    }
    
    private func showDropDownForYears(indexPath: IndexPath) {
        guard let multipleSelectListPopup = MultipleSelectListPopup.inistancefromNib() else { return }
        if self.vehicleYearDataSource.count == 0{
            self.vehicleYearDataSource = Helper.getListOfYearsFrom(2005)
        }
        multipleSelectListPopup.initializeViewForYears(years: self.vehicleYearDataSource) { (yearsList,updatedYearArray) in
            var yearArray = [Int]()
            self.vehicleYearDataSource = updatedYearArray
            if yearsList.count > 0 {
                for year in yearsList {
                    yearArray.append(Helper.toInt(year[Constants.UIKeys.year]))
                }
                self.notificationSettingsDataSource[indexPath.row].info?[Constants.UIKeys.years] = yearArray as AnyObject
            }else{
                self.notificationSettingsDataSource[indexPath.row].info?[Constants.UIKeys.years] = "" as AnyObject
            }
            self.notificationSettingsTableView.reloadRows(at: [indexPath], with: .none)
        }
        multipleSelectListPopup.showWithAnimated(animated: true)
    }
    
    private func showDropDownForBrands(indexPath: IndexPath) {
        guard let multipleSelectListPopup = MultipleSelectListPopup.inistancefromNib() else { return }
        if self.vehicleBrandDataSource.count == 0 {
            self.requestVehicleBrandAPI()
            return
        }
        
        multipleSelectListPopup.initializeViewForVehicleBrand(brands: self.vehicleBrandDataSource) { (vehicleBrands) in
            //  print(vehicleBrands)
            var brandName = [String]()
            var brandID = [Int]()
            
            if vehicleBrands.count > 0 {
                for brands in vehicleBrands {
                    brandName.append(Helper.toString(object: brands.title))
                    brandID.append(Helper.toInt(brands.id))
                }
                self.notificationSettingsDataSource[indexPath.row].info?[Constants.UIKeys.brandId] = brandID as AnyObject
                self.notificationSettingsDataSource[indexPath.row].info?[Constants.UIKeys.brands] = brandName as AnyObject
                //self.notificationSettingsDataSource[indexPath.row].value = brandName
                //  self.auctionFilterTableView.reloadData()
            }else{
                self.notificationSettingsDataSource[indexPath.row].info?[Constants.UIKeys.brandId] = "" as AnyObject
                self.notificationSettingsDataSource[indexPath.row].info?[Constants.UIKeys.brands] = "" as AnyObject
            }
            self.notificationSettingsTableView.reloadRows(at: [indexPath], with: .none)
        }
        multipleSelectListPopup.showWithAnimated(animated: true)
    }
    
    private func showDropDownForTime(indexPath: IndexPath) {
        guard let multipleSelectListPopup = MultipleSelectListPopup.inistancefromNib() else { return }
        multipleSelectListPopup.initializeViewForDuration(time: Constants.tenderDuration) { (durationList) in
            var durationArray = [String]()
            var valueArray = [String]()
            
            if durationList.count > 0 {
                for time in durationList {
                    durationArray.append(Helper.toString(object: time[Constants.UIKeys.duration]))
                    valueArray.append(Helper.toString(object: time[Constants.UIKeys.value]))
                }
                self.notificationSettingsDataSource[indexPath.row].info?[Constants.UIKeys.time] = durationArray as AnyObject
                self.notificationSettingsDataSource[indexPath.row].info?[Constants.UIKeys.value] = valueArray as AnyObject
            }
            self.notificationSettingsTableView.reloadRows(at: [indexPath], with: .none)
        }
        multipleSelectListPopup.showWithAnimated(animated: true)
    }
    
    private func showDropDownForOption(indexPath: IndexPath) {
       self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: DropDownType.notificationOption.title, arrayList: DropDownType.notificationOption.dataList, key: DropDownType.notificationOption.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let notificationOption = response[DropDownType.notificationOption.rawValue] as? String else { return }
            sSelf.notificationSettingsDataSource[indexPath.row].info?[Constants.UIKeys.value] = notificationOption as AnyObject
            sSelf.notificationSettingsDataSource[indexPath.row].info?[Constants.UIKeys.id] = response[Constants.UIKeys.id] as AnyObject

            sSelf.notificationSettingsTableView.reloadRows(at: [indexPath], with: .none)
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    //MARK: - Public Methods
    func showDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        if let userType = UserSession.shared.userType {
            switch userType {
            case .customer:
                switch indexPath.row {
                case 1:
                    self.showDropDownForYears(indexPath: indexPath)
                    break
                case 2:
                    self.showDropDownForBrands(indexPath: indexPath)
                    break
                case 5:
                    self.showDropDownForOption(indexPath: indexPath)
                    break
                default:
                    break
                }
                break
                
            case .dealer:
                switch indexPath.row {
                case 2:
                    self.showDropDownForYears(indexPath: indexPath)
                    break
                case 3:
                    self.showDropDownForBrands(indexPath: indexPath)
                    break
                case 6:
                    self.showDropDownForOption(indexPath: indexPath)
                    break
                case 8:
                    self.showDropDownForTime(indexPath: indexPath)
                    break
                default:
                    break
                }
                
                break
                
            case .supplier:
                switch indexPath.row {
                case 1:
                    self.showDropDownForTime(indexPath: indexPath)
                    break
                default:
                    break
                }
                
                break
            }
        }
    }
    func setSelectedYearDataArray(yearArray:[Int]){
        if self.vehicleYearDataSource.count == 0{
            self.vehicleYearDataSource = Helper.getListOfYearsFrom(2005)
        }
        for i in 0..<vehicleYearDataSource.count{
            let item = vehicleYearDataSource[i]
            for year in yearArray{
                if Helper.toInt(item[Constants.UIKeys.year]) == year{
                    vehicleYearDataSource[i][Constants.UIKeys.isSelected] = true as AnyObject
                }
            }
        }
    }
    func setSelectedBrandDataArray(brandArray:[String]){
        if self.vehicleBrandDataSource.count == 0{
            self.requestVehicleBrandAPI()
        }
        //for var item in self.vehicleYearDataSource{
        for i in 0..<vehicleBrandDataSource.count{
            let item = vehicleBrandDataSource[i]
            for brand in brandArray{
                if item.title == brand{
                    vehicleBrandDataSource[i].isSelected = true
                }
            }
        }
    }
    
    //APIMethods
    
    func requestVehicleBrandAPI() {
        self.viewModel?.requestVehicleBrandAPI(completion: { (success, vehicleBrandList) in
            if success {
                self.vehicleBrandDataSource = vehicleBrandList
                self.requestGetCustomerNotificationsAPI()
            }
        })
    }
    
    func requestGetCustomerNotificationsAPI() {
        self.viewModel?.requestNotificationSettingAPI(completion: { [weak self] (notificationSettingsData) in
            guard let sSelf = self else { return }
            sSelf.cNotificationDataSource = notificationSettingsData
            sSelf.notificationId = Helper.toInt(notificationSettingsData.id)
            if let yearData = sSelf.cNotificationDataSource?.years{
                if yearData.count > 0{
                    sSelf.setSelectedYearDataArray(yearArray: yearData)
                }
            }
            if let brandData = sSelf.cNotificationDataSource?.brands{
                if brandData.count > 0{
                    sSelf.setSelectedBrandDataArray(brandArray: brandData)
                }
            }
            Threads.performTaskInMainQueue {
                sSelf.loadDataSource()
                sSelf.notificationSettingsTableView.reloadData()
            }
        })
    }
    
    func requestPostCustomerNotificationsAPI() {
        self.viewModel?.requestpostNotificationSettingAPI(param: self.notificationSettingsDataSource, notificationId: self.notificationId ?? 0, completion: {[weak self] (notificationSettingsData) in
            guard let sSelf = self else { return }
            //sSelf.cNotificationDataSource = notificationSettingsData
            Threads.performTaskInMainQueue {
                sSelf.requestGetCustomerNotificationsAPI()
                //sSelf.loadDataSource()
                //sSelf.notificationSettingsTableView.reloadData()
            }
        })
    }
}
