//
//  NotificationSettingsViewC+TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension NotificationSettingsViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationSettingsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.notificationSettingsDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.notificationSettingsDataSource[indexPath.row].height
    }
    
    
    
}

//MARK: - get Cell
extension NotificationSettingsViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .SideMenuCell:
            let cell: SideMenuCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .LabelDropDownCell:
            let cell: LabelDropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .MileageTextFieldCell:
            let cell: MileageTextFieldCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .LabelSwitchCell:
            let cell: LabelSwitchCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .BottomLabelRegularTextCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureViewWithLightText(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - DropDownCell Delegate
extension NotificationSettingsViewC: DropDownCellDelegate {
    
    func didTapDropDownCell(cell: DropDownCell) {
        
    }
    
}

//MARK: - LabelDropDownCellDelegate
extension NotificationSettingsViewC: LabelDropDownCellDelegate {
    
    func didTapLabelDropDownCell(cell: LabelDropDownCell) {
        
    guard let indexPath = self.notificationSettingsTableView.indexPath(for: cell) else { return }
       showDropDownAt(indexPath: indexPath) //self.notificationSettingsDataSource[indexPath.row].info?[Constants.UIKeys.isSelected] = isActivated as AnyObject
    }
}

//MARK: - LabelSwitchDelegate
extension NotificationSettingsViewC: LabelSwitchCellDelegate {
    func switchButtonTapped(cell: LabelSwitchCell, isActivated: Bool) {
        guard let indexPath = self.notificationSettingsTableView.indexPath(for: cell) else { return }
        self.notificationSettingsDataSource[indexPath.row].info?[Constants.UIKeys.switchStatus] = isActivated as AnyObject
    }
    
    
}

extension NotificationSettingsViewC: MileageTextFieldCellDelegate {
    func didTapMinimumMileage(cell: MileageTextFieldCell) {
        
    }
    
    func didTapMaximumMileage(cell: MileageTextFieldCell) {
        
    }
    
    func didChangeMinimumMileage(cell: MileageTextFieldCell, text: String) {
        guard let indexPath = self.notificationSettingsTableView.indexPath(for: cell) else { return }
        self.notificationSettingsDataSource[indexPath.row].info?[Constants.UIKeys.minimumValue] = Helper.toInt(text) as AnyObject
    }
    
    func didChangeMaximumMileage(cell: MileageTextFieldCell, text: String) {
        guard let indexPath = self.notificationSettingsTableView.indexPath(for: cell) else { return }
        self.notificationSettingsDataSource[indexPath.row].info?[Constants.UIKeys.maximumValue] = Helper.toInt(text) as AnyObject
    }
    
    
}
