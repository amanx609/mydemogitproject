//
//  SettingsViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class SettingsViewC: BaseViewC {
  
  //MARK: - IBOutlets
  @IBOutlet weak var settingsTableView: UITableView!
  
  //MARK: - Variables
  var settingsDataSource = [String]()
  var viewModel: SettingsViewM?
  
  //MARK: - LifeCycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setup()
  }
  
  deinit {
    print("deinit SettingsViewC")
  }
  
  
  //MARK: - Private Methods
  private func setup() {
    self.recheckVM()
    self.setupTableView()
      self.setupNavigationBarTitle(title: "Settings".localizedString(), leftBarButtonsType: [.hamburgerBlack], rightBarButtonsType: [])
    
    if let dataSource = self.viewModel?.getSettingsDataSource() {
      self.settingsDataSource = dataSource
    }
  }
  
  private func recheckVM() {
    if self.viewModel == nil {
      self.viewModel = SettingsViewM()
    }
  }
  
  private func setupTableView() {
    self.registerNibs()
    self.settingsTableView.delegate = self
    self.settingsTableView.dataSource = self
    self.settingsTableView.separatorStyle = .none
    //self.settingsTableView.allowsSelection = false
    self.settingsTableView.backgroundColor = .white
  }
  
  private func registerNibs() {
    self.settingsTableView.register(SideMenuCell.self)
  }
  
    //MARK: - API Methods
    func requestLogoutAPI() {
        self.viewModel?.requestLogoutAPI() { (success) in
            if success {
                Threads.performTaskInMainQueue {
                    UserSession.shared.removeData()
                    AppDelegate.delegate.showLoginType()
                }
            }
        }
    }
}
