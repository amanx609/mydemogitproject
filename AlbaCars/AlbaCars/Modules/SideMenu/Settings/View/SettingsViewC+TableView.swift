//
//  SettingsViewC+TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension SettingsViewC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.settingsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let text = self.settingsDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, text: text)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.goToSelectedScreen(indx: indexPath.row)
    }
}

extension SettingsViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, text: String) -> UITableViewCell {
         let cell: SideMenuCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureViewWith(text: text)
        return cell
    }
}


extension SettingsViewC {
    
    func goToSelectedScreen(indx: Int) {
        
        switch indx {
        case 0:
            let changePasswordVC = DIConfigurator.sharedInstance.getChangePasswordVC()
            changePasswordVC.paramInfo[ConstantAPIKeys.senderType] = ChangePwdSenderType(rawValue: 1) as AnyObject

            self.navigationController?.pushViewController(changePasswordVC, animated: true)
        case 1:
            let webViewVC = DIConfigurator.sharedInstance.getwebViewVC()
            webViewVC.webContentType = .termsConditions
            self.navigationController?.pushViewController(webViewVC, animated: true)
        case 2:
            let webViewVC = DIConfigurator.sharedInstance.getwebViewVC()
            webViewVC.webContentType = .privacyPolicy
            self.navigationController?.pushViewController(webViewVC, animated: true)
            break
        case 3:
            Alert.showAlertWithActionWithCancel(title: StringConstants.Text.AppName.localizedString(), message: StringConstants.Message.areYouSureYouWantToSignOut.localizedString(), actionTitle: StringConstants.Text.Yes.localizedString(), cancelTitle: StringConstants.Text.No.localizedString(), action: { (_) in
                self.requestLogoutAPI()
            }, cancelAction: { (_) in

            })
            break
        default:
            break
        }
    }
}
