//
//  SettingsViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SettingsVModeling: BaseVModeling {
    func getSettingsDataSource() -> [String]
    func requestLogoutAPI(completion: @escaping (Bool)-> Void)
}

class SettingsViewM: SettingsVModeling {
    
    func getSettingsDataSource() -> [String] {
        let array = [StringConstants.Text.changePassword.localizedString(),
                     StringConstants.Text.termsConditions.localizedString(),
                     StringConstants.Text.privacyPolicy.localizedString(),
                     StringConstants.Text.signOut.localizedString().localizedString()]
        return array
    }
    
    //MARK: - API Methods
    func requestLogoutAPI(completion: @escaping (Bool)-> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .logout)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject] {
                    completion(success)
                }
            }
        }
    }
}
