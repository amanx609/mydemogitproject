//
//  SMyTransactionsViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class SMyTransactionsViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var myTransactionTableView: UITableView!
    @IBOutlet var dateTimePicker: UIDatePicker!
    @IBOutlet weak var totalEarningLabel: UILabel!
    @IBOutlet weak var jobCompletedLabel: UILabel!
    @IBOutlet weak var bidsLabel: UILabel!

    //MARK: - Variables
    var myTransactionDataSource: [CellInfo] = []
    var viewModel: MyTransactionsViewModeling?
    var isFromDate = true

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setup()
    }
    
    deinit {
        print("deinit MyTransactionsViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "My Transactions".localizedString(),barColor: .greenTopHeaderColor,titleColor: .white, leftBarButtonsType: [.hamburgerWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setupTableView()
        self.setupDataSource()
        self.requestMyTransactionsAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = MyTransactionsViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.myTransactionTableView.delegate = self
        self.myTransactionTableView.dataSource = self
        self.myTransactionTableView.separatorStyle = .none
        self.myTransactionTableView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.myTransactionTableView.register(DateRangeCell.self)
        self.myTransactionTableView.register(MyTransactionDetailCell.self)
    }
    
    func setupDataSource() {
        if let dataSource = self.viewModel?.getMyTransactionsDataSource() {
            self.myTransactionDataSource = dataSource
        }
    }

    //MARK: - Public Methods
    func setupDateTimePicker(_ textField: UITextField) {
        textField.inputView = self.dateTimePicker
        textField.inputAccessoryView = Helper.getDoubleButtonToolBar(leftButtonTitle: StringConstants.Text.Cancel.localizedString(), rightButtonTitle: StringConstants.Text.done.localizedString(), leftButtonselector: #selector(self.cancelButtonTapped), rightButtonSelector: #selector(self.doneButtonTapped), target: self)
        self.dateTimePicker.maximumDate = Date()
        self.dateTimePicker.datePickerMode = .date
    }
    
    func setupToolBar(_ textField: UITextField) {
        textField.inputAccessoryView = Helper.getSingleButtonToolBar(buttonTitle: StringConstants.Text.done.localizedString(), selector: #selector(self.doneButtonTapped), target: self)
    }
    
    //MARK: - Selectors
    @objc func cancelButtonTapped() {
        self.view.endEditing(true)
    }
    
    @objc func doneButtonTapped() {
        self.view.endEditing(true)
        let selectedDate = self.dateTimePicker.date
        let convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatWithoutSpace)
        
        if isFromDate {
            self.myTransactionDataSource[0].placeHolder = convertedValue
        } else {
            self.myTransactionDataSource[0].value = convertedValue
        }
        
        self.requestMyTransactionsAPI()
    }
    
    //MARK: - APIMethods
     func requestMyTransactionsAPI() {
        
        var params: APIParams = APIParams()
        
        if let fromDate = self.myTransactionDataSource[0].placeHolder, !fromDate.isEmpty {
            params[ConstantAPIKeys.dateFrom] = fromDate.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd) as AnyObject
        }
        
        if let toDate = self.myTransactionDataSource[0].value, !toDate.isEmpty {
            params[ConstantAPIKeys.dateTo] = toDate.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd) as AnyObject
        }
                
        self.viewModel?.requestMyTransactionsAPI(param: params, completion: { [weak self] (myTransactions) in
            guard let sSelf = self else { return }
            sSelf.totalEarningLabel.text = Helper.toString(object: myTransactions.formattedTotalEarnings())
            sSelf.jobCompletedLabel.text = "Total Jobs Completed : ".localizedString() + Helper.toString(object: myTransactions.totalJobCompleted)
            sSelf.bidsLabel.text = "Total Bids : ".localizedString() + Helper.toString(object: myTransactions.totalBids)
            sSelf.myTransactionDataSource[1].value = Helper.toString(object: myTransactions.bids)
            sSelf.myTransactionDataSource[2].value = Helper.toString(object: myTransactions.jobCompleted)
            sSelf.myTransactionDataSource[3].value = Helper.toString(object: myTransactions.paidAmount)
            sSelf.myTransactionDataSource[4].value = Helper.toString(object: myTransactions.pendingAmount)

            Threads.performTaskInMainQueue {
                sSelf.myTransactionTableView.reloadData()
            }
        })
    }
}
