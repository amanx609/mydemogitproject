//
//  DateRangeCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol DateRangeCellDelegate: class {
    func didTapFromDate(cell: DateRangeCell)
    func didTapToDate(cell: DateRangeCell)
}

class DateRangeCell:  BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var fromView: UIView!
    @IBOutlet weak var toView: UIView!
    @IBOutlet weak var fromDateTitleLabel: UILabel!
    @IBOutlet weak var toDateTitleLabel: UILabel!
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var toTextField: UITextField!
    @IBOutlet weak var toIconImageView: UIImageView!
    
    //MARK: - Variables
    weak var delegate: DateRangeCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.fromView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.toView.roundCorners(Constants.UIConstants.sizeRadius_7)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.fromTextField.text = cellInfo.placeHolder
        self.toTextField.text = cellInfo.value
        self.fromTextField.delegate = self
        self.toTextField.delegate = self
    }
    
    //MARK: - Public Methods
    func configureAuctionConfirmationViewCell(cellInfo: CellInfo) {
        
        self.toView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.fromView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.toView.backgroundColor = .white
        self.fromView.backgroundColor = .white
        self.fromTextField.delegate = self
        self.toTextField.delegate = self
        self.fromTextField.text = cellInfo.placeHolder
        self.toTextField.text = cellInfo.value
        
        if let info = cellInfo.info {
            if let toIconImage = info[Constants.UIKeys.image] as? UIImage {
                self.toIconImageView.image = toIconImage
            }
            
            if let titlePlaceholder = info[Constants.UIKeys.titlePlaceholder] as? String {
                
                self.fromDateTitleLabel.text = titlePlaceholder
            }
            
            if let subTitlePlaceholder = info[Constants.UIKeys.subTitlePlaceholder] as? String {
                self.toDateTitleLabel.text = subTitlePlaceholder
            }
        }
    }
}

extension DateRangeCell: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.fromTextField {
            if let delegate = self.delegate {
                delegate.didTapFromDate(cell: self)
            }
        } else if textField == self.toTextField {
            if let delegate = self.delegate {
                delegate.didTapToDate(cell: self)
            }
        }
    }
    
}
