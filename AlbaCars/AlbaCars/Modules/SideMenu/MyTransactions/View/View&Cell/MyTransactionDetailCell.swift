//
//  MyTransactionDetailCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class MyTransactionDetailCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.titleLabel.text = cellInfo.placeHolder
        self.amountLabel.text = cellInfo.value
        
        if let info = cellInfo.info, let bgColor = info[Constants.UIKeys.backgroundColor] as? UIColor {
            self.amountLabel.textColor = bgColor
            
            self.amountLabel.attributedText = String.getAttributedText(firstText:"AED".localizedString(), secondText:" "+Helper.toString(object: cellInfo.value), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_18, secondTextSize: .size_18, fontWeight: .Medium)
            
        } else {
            self.amountLabel.textColor = .greenColor
        }
        
    }
    
}
