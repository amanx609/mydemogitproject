//
//  MyTransactionsModel.swift
//  AlbaCars
//
//  Created by Narendra on 2/26/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class MyTransactionsModel: Codable {
    
    // Variables
    var jobCompleted: Int?
    var totalEarnings: Int?
    var pendingAmount: Int?
    var totalBids: Int?
    var bids: Int?
    var paidAmount: Int?
    var totalJobCompleted: Int?
    
    func formattedAmountPaid() -> String {
        return "AED".localizedString() + " " + Helper.toString(object: paidAmount)
    }
    
    func formattedPendingAmount() -> String {
        return "AED".localizedString() + " " + Helper.toString(object: pendingAmount)
    }
    
    func formattedTotalEarnings() -> String {
        return "AED".localizedString() + " " + Helper.toString(object: totalEarnings)
    }
    
}
