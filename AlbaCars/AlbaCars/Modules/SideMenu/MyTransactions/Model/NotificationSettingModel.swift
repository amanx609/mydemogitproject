//
//  NotificationSettingModel.swift
//  AlbaCars
//
//  Created by Narendra on 2/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

class NotificationSettingModel: Codable {
    
    // Variables
    var payment_completion: Bool?
    var new_tender: Bool?
    var bids_accepted: Bool?
    var job_completion: Bool?
    var bids_rejected: Bool?
    var id: Int?
    var user_id: Int?
    var minimum_mileage: Int?
    var maximum_mileage: Int?
    var options: Int?
    var smart_tenders: Bool?
    var finance_application: Bool?
    var fleet_biddings: Bool?
    var leads: Bool?
    var sale: Bool?
    var offers_deals: Bool?
    var created_at: String?
    var updated_at: String?
    var deleted_at: String?
    var brands: [String]?
    var brandId: [Int]?
    var time: [String]?
    var years: [Int]?
    var NotificationBrands: [NotificationBrandDetails]?
    
    func getTimeString() -> [String] {
        var timeArray = [String]()
        
        if let timeStr = self.time {
            for i in 0..<timeStr.count {
                switch timeStr[i] {
                case "00:15:00":
                   timeArray.append("15 min")
                     
                    case "00:30:00":
                        timeArray.append("30 min")
                    
                    case "01:00:00":
                        timeArray.append("1 hr")
                    
                    case "06:00:00":
                         timeArray.append("6 hrs")
                    
                    case "24:00:00":
                        timeArray.append("1 day")
                    
                
                
                default:
                    break
                }
            }
            
        }
        
        return timeArray
    }
}

class NotificationBrandDetails: BaseCodable {
    var brandName: String?
    var brandId: Int?
    var time: String?
    var years: Int?
    
}




