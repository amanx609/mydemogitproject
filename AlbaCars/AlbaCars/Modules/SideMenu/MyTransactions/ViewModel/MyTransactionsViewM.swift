//
//  MyTransactionsViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol MyTransactionsViewModeling: BaseVModeling {
    func getMyTransactionsDataSource() -> [CellInfo]
    func requestMyTransactionsAPI(param: APIParams, completion: @escaping (MyTransactionsModel) -> Void)
}

class MyTransactionsViewM: MyTransactionsViewModeling {
    
    func getMyTransactionsDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //DateRange Cell
        let currentDate = Helper.getCurrentDate(inFormat: Constants.Format.dateFormatWithoutSpace)
        let dateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: currentDate, info: nil, height: Constants.CellHeightConstants.height_100)
        array.append(dateRangeCell)
        
        // Total Bids Cell
        let totalBidsCell = CellInfo(cellType: .MyTransactionDetailCell, placeHolder: "Total Bids".localizedString(), value: "0", info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(totalBidsCell)
        
        // Total Jobs Completed
        let totalJobsCompletedCell = CellInfo(cellType: .MyTransactionDetailCell, placeHolder: "Total Jobs Completed".localizedString(), value: "0", info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(totalJobsCompletedCell)
        
        // Amount Paid
        var amountPaidInfo = [String: AnyObject]()
        amountPaidInfo[Constants.UIKeys.backgroundColor] = UIColor.redButtonColor as AnyObject
        let amountPaidCell = CellInfo(cellType: .MyTransactionDetailCell, placeHolder: "Amount Paid".localizedString(), value: "0", info: amountPaidInfo, height: Constants.CellHeightConstants.height_50)
        array.append(amountPaidCell)
        
        // Amount Pending
        let amountPendingCell = CellInfo(cellType: .MyTransactionDetailCell, placeHolder: "Amount Pending".localizedString(), value: "0", info: amountPaidInfo, height: Constants.CellHeightConstants.height_50)
        array.append(amountPendingCell)
        return array
    }
    
    func requestMyTransactionsAPI(param: APIParams, completion: @escaping (MyTransactionsModel) -> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .serviceProviderMyTransactions(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let myTransactionsModel = try decoder.decode(MyTransactionsModel.self, from: resultData)
                        completion(myTransactionsModel)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
}
