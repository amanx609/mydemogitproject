//
//  SideMenuViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SideMenuVModeling {
    func getSideMenuDataSource() -> [String]
}

class SideMenuViewM: SideMenuVModeling {
    
    func getSideMenuDataSource() -> [String] {
        var array = [String]()
        if let userType = UserSession.shared.userType {
            switch userType {
            case .customer:
                array = ["Home".localizedString(),"My Account".localizedString(),"Payment".localizedString(),"My Cars".localizedString(),"Notifications Settings".localizedString(),"Settings".localizedString(),"Help Center".localizedString(),"Feedback".localizedString()]
            case .dealer:
                array = ["Home".localizedString(),"My Account".localizedString(),"Payment".localizedString(),"My Cars".localizedString(),"Documents".localizedString(),"Notifications Settings".localizedString(),"Settings".localizedString(),"Help Center".localizedString(),"Feedback".localizedString()]
            case .supplier:
                array = ["Home".localizedString(),"My Account".localizedString(),"My Transactions".localizedString(),"Documents".localizedString(),"Notifications Settings".localizedString(),"Settings".localizedString(),"Help Center".localizedString(),"Feedback".localizedString()]
            default:
                return [""]
            }
        }
        
        return array
    }
}

