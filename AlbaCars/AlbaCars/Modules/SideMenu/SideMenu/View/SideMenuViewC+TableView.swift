//
//  SideMenuViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension SideMenuViewC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sideMenuDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let text = self.sideMenuDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, text: text)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let userType = UserSession.shared.userType {
            if userType == .dealer {
                self.goToSelectedDealerScreen(indx: indexPath.row)
            } else{
                self.goToSelectedScreen(indx: indexPath.row)
            }
        }
    }
    
}

extension SideMenuViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, text: String) -> UITableViewCell {
        let cell: SideMenuCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureViewWith(text: text)
        return cell
    }
}

extension SideMenuViewC {
    
     func goToSelectedDealerScreen(indx: Int) {
        
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as? UINavigationController
        
        switch indx {
        case 0:
            let homeVC = DIConfigurator.sharedInstance.getHomeVC()
            navigationController?.setViewControllers([homeVC], animated: false)
            break
        case 1:
            // My Account screen
            let myAccountVC = DIConfigurator.sharedInstance.getMyAccountVC()
            navigationController?.setViewControllers([myAccountVC], animated: false)
            break
        case 2:
            // Payment
            let paymentVC = DIConfigurator.sharedInstance.getPaymentVC()
            navigationController?.setViewControllers([paymentVC], animated: false)
        case 3:
            let myCarsVC = DIConfigurator.sharedInstance.getMyCarsVC()
            navigationController?.setViewControllers([myCarsVC], animated: false)
        case 4:
            let documentVC = DIConfigurator.sharedInstance.getDocumentListViewC()
            navigationController?.setViewControllers([documentVC], animated: false)
        case 5:
            let notificationSettingsVC = DIConfigurator.sharedInstance.getNotificationSettingsVC()
            navigationController?.setViewControllers([notificationSettingsVC], animated: false)
        case 6:
            let settingsVC = DIConfigurator.sharedInstance.getSettingsVC()
            navigationController?.setViewControllers([settingsVC], animated: false)
        case 7:
            let helpCenterVC = DIConfigurator.sharedInstance.getHelpCenterVC()
            navigationController?.setViewControllers([helpCenterVC], animated: false)
        case 8:
            let feedbackVC = DIConfigurator.sharedInstance.getFeedbackVC()
            navigationController?.setViewControllers([feedbackVC], animated: false)
            break
        default:
            break
        }
        
        mainViewController.rootViewController = navigationController
        mainViewController.hideLeftView(animated: true, completionHandler: nil)
    }
    
    func goToSelectedScreen(indx: Int) {
        
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as? UINavigationController
        
        switch indx {
        case 0:
            let homeVC = DIConfigurator.sharedInstance.getHomeVC()
            navigationController?.setViewControllers([homeVC], animated: false)
            break
        case 1:
            // My Account screen
            let myAccountVC = DIConfigurator.sharedInstance.getMyAccountVC()
            navigationController?.setViewControllers([myAccountVC], animated: false)
            break
        case 2:
            if self.userType == .supplier {
                // MY Trasactions screen
                let myTransactions = DIConfigurator.sharedInstance.getMyTransactionsVC()
                navigationController?.setViewControllers([myTransactions], animated: false)
                
            } else {
                // Payment
                let paymentVC = DIConfigurator.sharedInstance.getPaymentVC()
                navigationController?.setViewControllers([paymentVC], animated: false)
            }
        case 3:
            if self.userType == .customer {
                let myCarsVC = DIConfigurator.sharedInstance.getMyCarsVC()
                navigationController?.setViewControllers([myCarsVC], animated: false)
            } else {
                let documentVC = DIConfigurator.sharedInstance.getDocumentListViewC()
                navigationController?.setViewControllers([documentVC], animated: false)
            }
        case 4:
            let notificationSettingsVC = DIConfigurator.sharedInstance.getNotificationSettingsVC()
            navigationController?.setViewControllers([notificationSettingsVC], animated: false)
        //navigationController?.pushViewController(notificationSettingsVC, animated: true)
        case 5:
            let settingsVC = DIConfigurator.sharedInstance.getSettingsVC()
            navigationController?.setViewControllers([settingsVC], animated: false)
        // navigationController?.pushViewController(settingsVC, animated: true)
        case 6:
            let helpCenterVC = DIConfigurator.sharedInstance.getHelpCenterVC()
            navigationController?.setViewControllers([helpCenterVC], animated: false)
        // navigationController?.pushViewController(helpCenterVC, animated: true)
        case 7:
            let feedbackVC = DIConfigurator.sharedInstance.getFeedbackVC()
            navigationController?.setViewControllers([feedbackVC], animated: false)
        // navigationController?.pushViewController(feedbackVC, animated: true)
        case 8:
            
            break
        default:
            break
        }
        
        mainViewController.rootViewController = navigationController
        mainViewController.hideLeftView(animated: true, completionHandler: nil)
    }
}
