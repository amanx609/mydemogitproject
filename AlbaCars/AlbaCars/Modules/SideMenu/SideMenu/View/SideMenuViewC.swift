//
//  SideMenuViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class SideMenuViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var profileImageView: UIImageView!
    
    
    //MARK: - Variables
    var sideMenuDataSource = [String]()
    var viewModel: SideMenuVModeling?
    var userType = UserType.customer
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupUserData()
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    deinit {
        print("deinit SideMenuViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        if let userTypeValue = UserSession.shared.userType {
            self.userType = userTypeValue
        }
        self.recheckVM()
        self.setupTableView()
        if let dataSource = self.viewModel?.getSideMenuDataSource() {
            self.sideMenuDataSource = dataSource
        }
        self.profileImageView.makeLayer(color: .white, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_40)
        
        NotificationCenter.default.addObserver(
        self,
        selector: #selector(self.profileImageData),
        name: NSNotification.Name(rawValue: "profileImageData"),
        object: nil)
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = SideMenuViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.menuTableView.delegate = self
        self.menuTableView.dataSource = self
        self.menuTableView.separatorStyle = .none
        // self.menuTableView.allowsSelection = false
        self.menuTableView.tableHeaderView = self.headerView
        self.menuTableView.backgroundColor = .white
    }
    
    private func registerNibs() {
        self.menuTableView.register(SideMenuCell.self)
    }
    
    private func setupUserData() {
        self.nameLabel.text = UserSession.shared.name
        self.emailLabel.text = UserSession.shared.email
        if let imageName =  UserSession.shared.image {
            self.profileImageView.setImage(urlStr: imageName, placeHolderImage: #imageLiteral(resourceName: "avatar-placeholder"))
        }
    }
    
    @objc func profileImageData() {
       self.setupUserData()
    }
}
