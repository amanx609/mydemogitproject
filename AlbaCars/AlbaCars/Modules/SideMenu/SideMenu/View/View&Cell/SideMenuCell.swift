//
//  SideMenuCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class SideMenuCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
        
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Public Methods
       func configureViewWith(text: String) {
           self.titleLabel.text = text
       }
    
    func configureView(cellInfo: CellInfo) {
        self.titleLabel.text = cellInfo.value
    }
    
    func configureViewWithGreyText(cellInfo: CellInfo) {
        self.titleLabel.textColor = UIColor.lightGray
        self.titleLabel.text = cellInfo.value
    }
    
    func configureViewWithSmallerFont(cellInfo: CellInfo) {
        self.titleLabel.font = UIFont.font(name: .AirbnbCerealApp, weight: .Medium, size: .size_14)
        self.titleLabel.text = cellInfo.value
    }
}
