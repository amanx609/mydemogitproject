//
//  MainViewController.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import LGSideMenuController.LGSideMenuController
import LGSideMenuController.UIViewController_LGSideMenuController

class MainViewController: LGSideMenuController {

        private var type: UInt?
        
        func setup(type: UInt) {
            self.type = type
            leftViewWidth = 300.0;
            self.rootViewLayerShadowColor = UIColor.gray.withAlphaComponent(0.3)
            rootViewLayerShadowRadius = 50.0
            leftViewPresentationStyle = .scaleFromBig
            rightViewPresentationStyle = .scaleFromBig
            isRightViewSwipeGestureEnabled = false
            isLeftViewSwipeGestureEnabled = false

            
        }

        override func leftViewWillLayoutSubviews(with size: CGSize) {
            super.leftViewWillLayoutSubviews(with: size)

            if !isLeftViewStatusBarHidden {
                leftView?.frame = CGRect(x: 0.0, y: 20.0, width: size.width, height: size.height - 20.0)
            }
        }
        
        override func rightViewWillLayoutSubviews(with size: CGSize) {
            super.rightViewWillLayoutSubviews(with: size)
            
            if (!isRightViewStatusBarHidden ||
                (rightViewAlwaysVisibleOptions.contains(.onPadLandscape) &&
                    UI_USER_INTERFACE_IDIOM() == .pad &&
                    UIApplication.shared.statusBarOrientation.isLandscape)) {
                rightView?.frame = CGRect(x: 0.0, y: 20.0, width: size.width, height: size.height - 20.0)
            }
        }

        override var isLeftViewStatusBarHidden: Bool {
            get {
                if (type == 8) {
                    return UIApplication.shared.statusBarOrientation.isLandscape && UI_USER_INTERFACE_IDIOM() == .phone
                }

                return super.isLeftViewStatusBarHidden
            }

            set {
                super.isLeftViewStatusBarHidden = newValue
            }
        }

        override var isRightViewStatusBarHidden: Bool {
            get {
                if (type == 8) {
                    return UIApplication.shared.statusBarOrientation.isLandscape && UI_USER_INTERFACE_IDIOM() == .phone
                }

                return super.isRightViewStatusBarHidden
            }

            set {
                super.isRightViewStatusBarHidden = newValue
            }
        }

        deinit {
            print("MainViewController deinitialized")
        }


}
