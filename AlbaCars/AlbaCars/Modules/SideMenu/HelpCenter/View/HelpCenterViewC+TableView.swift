//
//  HelpCenterViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import Foundation
import UIKit

extension HelpCenterViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.helpCenterDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.helpCenterDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.helpCenterDataSource[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let urlString = Helper.toString(object: self.helpCenterDataSource[indexPath.row].value).trimSpace()
        if let url = URL(string: urlString) {
            self.openUrl(url: url)
        }
    }
}

extension HelpCenterViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .ContactusCell:
            let cell: ContactusCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            cell.delegate = self
            return cell
        case .VideoCell:
            let cell: VideoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - ContactusCellDelegate
extension HelpCenterViewC: ContactusCellDelegate {
    func didTapEmail(cell: ContactusCell) {
        guard let indexPath = self.helpCenterTableView.indexPath(for: cell) else { return }
        let emailId =  Helper.toString(object:self.helpCenterDataSource[indexPath.row].value)
        self.sendEmail(email: emailId)
    }
    
    func didTapPhone(cell: ContactusCell) {
        guard let indexPath = self.helpCenterTableView.indexPath(for: cell) else { return }
        let phone =  Helper.toString(object: self.helpCenterDataSource[indexPath.row].placeHolder)
        guard let url = URL(string: "tel://" + phone) else { return }
        self.openUrl(url: url)
    }
    
}
