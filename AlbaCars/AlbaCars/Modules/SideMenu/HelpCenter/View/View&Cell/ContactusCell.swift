//
//  ContactusCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol ContactusCellDelegate: class {
  func didTapEmail(cell: ContactusCell)
  func didTapPhone(cell: ContactusCell)
}

class ContactusCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!

    //MARK: - Variables
    weak var delegate: ContactusCellDelegate?
    
    //MARK: - LifeCycle Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupView() {
      self.gradientView.roundCorners(Constants.UIConstants.sizeRadius_7)
      self.bgView.roundCorners(Constants.UIConstants.sizeRadius_7)

      Threads.performTaskInMainQueue {
        self.gradientView.drawGradientLeftToRight(startColor: .aquaColor, endColor: .aquaLightColor)
        self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
      }
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.emailLabel.text = cellInfo.value
        self.phoneNumberLabel.text = cellInfo.placeHolder
    }
    
    //MARK: - IBActions
    @IBAction func tapEmail(_ sender: UIButton) {
      if let delegate = self.delegate {
        delegate.didTapEmail(cell: self)
      }
    }
    
    @IBAction func tapPhone(_ sender: UIButton) {
      if let delegate = self.delegate {
        delegate.didTapPhone(cell: self)
      }
    }
}
