//
//  HelpCenterViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import MessageUI
class HelpCenterViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var helpCenterTableView: UITableView!
    
    //MARK: - Variables
    var helpCenterDataSource: [CellInfo] = []
    var viewModel: HelpCenterVModeling?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Help Center".localizedString(), leftBarButtonsType: [.hamburgerBlack], rightBarButtonsType: [])
        self.recheckVM()
        self.setupTableView()
        //        if let dataSource = self.viewModel?.getHelpCenterDataSource() {
        //            self.helpCenterDataSource = dataSource
        //        }
        self.requestHelpCenterAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = HelpCenterViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.helpCenterTableView.delegate = self
        self.helpCenterTableView.dataSource = self
        self.helpCenterTableView.separatorStyle = .none
     //   self.helpCenterTableView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.helpCenterTableView.register(VideoCell.self)
        self.helpCenterTableView.register(ContactusCell.self)
    }
    
    //MARK: - API Methods
    func requestHelpCenterAPI() {
        self.viewModel?.requestHelpCenterAPI() { (responseData) in
            if let data = responseData as? [HelpCenterModel] {
                
                if let dataSource = self.viewModel?.getHelpCenterDataSource(helpCenterData: data) {
                    self.helpCenterDataSource = dataSource
                }
                
                Threads.performTaskInMainQueue {
                    self.helpCenterTableView.reloadData()
                }
            }
        }
    }
    
    func sendEmail(email: String) {
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            present(mail, animated: true)
        } else {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please login your mail on device.".localizedString())
        }
    }
    
    func openUrl(url: URL) {
        UIApplication.shared.open(url)
    }
    
    
}

extension HelpCenterViewC: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
