//
//  HelpCenterModel.swift
//  AlbaCars
//
//  Created by Narendra on 12/17/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

//Help Center
class HelpCenterModel: Codable {
  
  //Variables
  var type: Int?
  var description: String?
  var title: String?
}
