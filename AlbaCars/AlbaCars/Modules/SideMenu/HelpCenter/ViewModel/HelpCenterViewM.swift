//
//  HelpCenterViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol HelpCenterVModeling: BaseVModeling {
    func getHelpCenterDataSource(helpCenterData: [HelpCenterModel]) -> [CellInfo]
    func requestHelpCenterAPI(completion: @escaping (Any)-> Void)
}

class HelpCenterViewM: HelpCenterVModeling {
    
    func getHelpCenterDataSource(helpCenterData: [HelpCenterModel]) -> [CellInfo] {
        
        var array = [CellInfo]()
        var email = ""
        var phone = ""
        
        if helpCenterData.count > 0  {
            if let type = helpCenterData[0].type, type == HelpCenterType.email.rawValue {
                email = Helper.toString(object: helpCenterData[0].description)
            }
        }
        
        if helpCenterData.count > 1  {
            if let type = helpCenterData[1].type, type == HelpCenterType.phone.rawValue {
                phone = Helper.toString(object: helpCenterData[1].description)
            }
        }
        
        if !phone.isEmpty || !email.isEmpty  {
            
            //Contactus Cell
            let contactusCell = CellInfo(cellType: .ContactusCell, placeHolder: phone, value: email, info: nil, height: Constants.CellHeightConstants.height_156)
            array.append(contactusCell)
        }
        
        
        for (_, element) in helpCenterData.enumerated() {
          //print("Item \(index): \(element)")
            if let type = element.type, type == HelpCenterType.video.rawValue {
                //Video Cell
                let videoID = self.getYoutubeId(youtubeUrl: Helper.toString(object: element.description))
                var videoInfo = [String: AnyObject]()
                videoInfo[Constants.UIKeys.videoId] = videoID as AnyObject
                let videoCell = CellInfo(cellType: .VideoCell, placeHolder: Helper.toString(object: element.title), value: Helper.toString(object: element.description), info: videoInfo, height: UITableView.automaticDimension)
                array.append(videoCell)
            }
        }
        return array
    }
    
    func getYoutubeId(youtubeUrl: String) -> String? {
        return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
    }
    
    //MARK: - API Methods
    func requestHelpCenterAPI(completion: @escaping (Any)-> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .helpInfo)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [[String: AnyObject]],
                    let helpCenterDataArray = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let helpCenterData = try decoder.decode([HelpCenterModel].self, from: helpCenterDataArray)
                        completion(helpCenterData)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
}
