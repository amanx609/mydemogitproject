//
//  SBodyWorkTenderWonViewM.swift
//  AlbaCars
//
//  Created by Narendra on 2/20/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SBodyWorkTenderWonViewModeling {
    func getTenderWonDataSource(tenderDetails: SupplierTender?) -> [CellInfo]
}

class SBodyWorkTenderWonViewM: SBodyWorkTenderWonViewModeling {
    
    func getTenderWonDataSource(tenderDetails: SupplierTender?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Details Title
        let carTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Car Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(carTitleCell)
        
        //Car Details
        var carDetailsInfo = [String: AnyObject]()
        carDetailsInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        let carCell = CellInfo(cellType: .TenderCarDetailsCell, placeHolder:"Car Details".localizedString(), value:"" ,info:carDetailsInfo, height: Constants.CellHeightConstants.height_115)
        array.append(carCell)
        
        //Tender Details Title
        let tenderTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Tender Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(tenderTitleCell)
        
         let bodyWorkType = Helper.toInt(tenderDetails?.bodyServiceType)

              if bodyWorkType == BodyWorkType.paintlessDentRemoval.rawValue {

                  // Image
                  var imageInfo = [String: AnyObject]()
                  imageInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "bodywork.png") as AnyObject
                  let imageCell = CellInfo(cellType: .DAddAuctionImageCell, placeHolder: "", value:"" ,info: imageInfo, height: Constants.CellHeightConstants.height_230)
                  array.append(imageCell)
                  
                  if let dentDescription = tenderDetails?.dentDescription {
                      
                      var count = 1
                      for element in dentDescription {
                          let dentDescriptionCell = CellInfo(cellType: .DentDescriptionCell, placeHolder: Helper.toString(object: count), value:Helper.toString(object: element) ,info: nil, height: UITableView.automaticDimension)
                          array.append(dentDescriptionCell)
                          count = count + 1
                      }
                  }
              }
        
        
        //Service Type
        //let bodyWorkType = Helper.toInt(tenderDetails?.tenderServiceType)
        let serviceTypeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Service Type : ".localizedString(), value:Helper.toString(object: Helper.getBodyWorkType(statusCode: bodyWorkType)),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(serviceTypeCell)
        
        var firstText = ""
        var secondText = ""
        
        if bodyWorkType == BodyWorkType.panelPainting.rawValue {
            firstText = "Paint Color : ".localizedString()
            secondText = Helper.toString(object: tenderDetails?.tenderPaintColor)
            
        } else if bodyWorkType == BodyWorkType.paintlessDentRemoval.rawValue {
            
            firstText = "No.of Dents : ".localizedString()
            secondText = Helper.toString(object: tenderDetails?.tendernoOfDents)
        } else {
            
            firstText = "Paint Site : ".localizedString()
            secondText = Helper.getPaitSite(paintSiteArray: tenderDetails?.tenderPaintSiteArr)
        }
        
        let visiblityCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:firstText, value:secondText,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(visiblityCell)
        
        //Requested Date
        let requestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedDateCell)
        
        //Requested Time
        let requestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedTimeCell)
        
        // Location
        let locationCell = CellInfo(cellType: .VITenderLocationCell, placeHolder:Helper.toString(object: tenderDetails?.pickupLocation), value:Helper.toString(object: tenderDetails?.dropoffLocation) ,info:nil, height: Constants.CellHeightConstants.height_100)
        array.append(locationCell)
        
        //Additional Information
        let descriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.tenderAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(descriptionCell)
        
        if let totalImage = tenderDetails?.images, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info:imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }
        
        //Bid Details
        let bidTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Bid Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(bidTitleCell)
        
        //Days
        let daysCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Days Required for the Service :".localizedString(), value:"\(Helper.toString(object: tenderDetails?.requireDaysService)) Days" ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(daysCell)
        
        //PickUp
        let pickUpCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"I will provide PickUp and DropOff : ".localizedString(), value:Helper.toString(object: tenderDetails?.getPickupDropoff()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(pickUpCell)
        
        
        //Service Type
        let bidServiceTypeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Service Type : ".localizedString(), value:Helper.toString(object: Helper.getBodyWorkType(statusCode: bodyWorkType)),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidServiceTypeCell)
        
        let bidVisiblityCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:firstText, value:secondText,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidVisiblityCell)
        
        //Additional Information
        let bidDescriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.bidAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(bidDescriptionCell)
        
        //Requested Date
        let bidRequestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedDateCell)
        
        let bidRequestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedTimeCell)
        
        let bidPriceCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Bid Price :".localizedString(), value:Helper.toString(object: tenderDetails?.formattedPrice()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidPriceCell)
        
        //ContactInfoCell
        var contactInfoInfo = [String: AnyObject]()
        contactInfoInfo[Constants.UIKeys.image] = tenderDetails?.image as AnyObject
        let contactInfoCell = CellInfo(cellType: .RDContactInfoCell, placeHolder: Helper.toString(object: tenderDetails?.name), value: Helper.toString(object: tenderDetails?.pickupLocation), info: contactInfoInfo, height: UITableView.automaticDimension)
        array.append(contactInfoCell)
        
        let secondBidCell = CellInfo(cellType: .VIContactInfoCell, placeHolder: Helper.toString(object: tenderDetails?.mobile), value: Helper.toString(object: tenderDetails?.email), info: nil, height: Constants.CellHeightConstants.height_100)
        array.append(secondBidCell)
        
        let infoCell = CellInfo(cellType: .TenderWonInfoCell, placeHolder: "You will be able to request payment only after both the customer & you have marked job as completed".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(infoCell)
        
        let infoCell2 = CellInfo(cellType: .TenderWonInfoCell, placeHolder: "Please tap on \"Mark Job as Completed\" button only after Services/Tenders are completed".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(infoCell2)
        
        let completedCell = CellInfo(cellType: .TenderCompletedCell, placeHolder: "Mark Job as Completed".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(completedCell)
        
        return array
    }
}
