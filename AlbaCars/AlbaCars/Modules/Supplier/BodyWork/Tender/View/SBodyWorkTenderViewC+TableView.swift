//
//  SBodyWorkTenderViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 2/19/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension SBodyWorkTenderViewC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tender.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getCell(tableView: tableView, indexPath: indexPath)
    }
}

extension SBodyWorkTenderViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let tender = self.tender[indexPath.row]
        
        if self.activeTenderButton.isSelected == true {
            let cell: SCarServicingActiveTenderCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureBodyWorkView(supplierTender: tender)
            return cell
        }
        
        let cell: SCarServicingMyTenderCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.configureBodyWorkView(supplierTender: tender)
        return cell
    }
}

//SCarServicingActiveTenderCellDelegate
extension SBodyWorkTenderViewC: SCarServicingActiveTenderCellDelegate {
    func didTapViewDetailsCell(cell: SCarServicingActiveTenderCell) {
        guard let indexPath = self.tenderTableView.indexPath(for: cell) else { return }
        let tender = self.tender[indexPath.row]
        self.requestActiveTenderDetailsAPI(tenderId: Helper.toInt(tender.id))
    }
}

//SCarServicingMyTenderCellDelegate
extension SBodyWorkTenderViewC: SCarServicingMyTenderCellDelegate {
    func didTapViewDetailsCell(cell: SCarServicingMyTenderCell) {
         guard let indexPath = self.tenderTableView.indexPath(for: cell) else { return }
         let tender = self.tender[indexPath.row]
         self.requestMyTenderDetailsAPI(tenderId: Helper.toInt(tender.id))
    }
}
