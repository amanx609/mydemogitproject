//
//  SCarDetailingTenderViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 2/21/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension SCarDetailingTenderViewC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tender.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getCell(tableView: tableView, indexPath: indexPath)
    }
}

extension SCarDetailingTenderViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let tender = self.tender[indexPath.row]
        
        if self.activeTenderButton.isSelected == true {
            let cell: SCarServicingActiveTenderCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureCarDetailingView(supplierTender: tender)
            return cell
        }
        
        let cell: SCarServicingMyTenderCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.configureCarDetailingView(supplierTender: tender)
        return cell
    }
}

//SCarServicingActiveTenderCellDelegate
extension SCarDetailingTenderViewC: SCarServicingActiveTenderCellDelegate {
    func didTapViewDetailsCell(cell: SCarServicingActiveTenderCell) {
        guard let indexPath = self.tenderTableView.indexPath(for: cell) else { return }
        let tender = self.tender[indexPath.row]
        self.requestActiveTenderDetailsAPI(tenderId: Helper.toInt(tender.id))
    }
}

//SCarServicingMyTenderCellDelegate
extension SCarDetailingTenderViewC: SCarServicingMyTenderCellDelegate {
    func didTapViewDetailsCell(cell: SCarServicingMyTenderCell) {
         guard let indexPath = self.tenderTableView.indexPath(for: cell) else { return }
         let tender = self.tender[indexPath.row]
         self.requestMyTenderDetailsAPI(tenderId: Helper.toInt(tender.id))
    }
}
