//
//  SCarDetailingBidSubmittedViewM.swift
//  AlbaCars
//
//  Created by Narendra on 2/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SCarDetailingBidSubmittedViewModeling {
    func getBidSubmittedDataSource(tenderDetails: SupplierTender?) -> [CellInfo]
}

class SCarDetailingBidSubmittedViewM: SCarDetailingBidSubmittedViewModeling {
    
    func getBidSubmittedDataSource(tenderDetails: SupplierTender?) -> [CellInfo] {
        
        var array = [CellInfo]()
                
        //Car Details Title
        let carTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Car Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(carTitleCell)
        
        //Car Details
        var carDetailsInfo = [String: AnyObject]()
        carDetailsInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        let carCell = CellInfo(cellType: .TenderCarDetailsCell, placeHolder:"Car Details".localizedString(), value:"" ,info:carDetailsInfo, height: Constants.CellHeightConstants.height_115)
        array.append(carCell)
        
        //Tender Details Title
        let tenderTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Tender Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(tenderTitleCell)
        
        //Service Type
        let carDetailingServiceType = Helper.toInt(tenderDetails?.tenderCarDetailingServiceType)
        let serviceTypeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Service Type : ".localizedString(), value:Helper.toString(object: Helper.getCarDetailingServiceType(statusCode: carDetailingServiceType)),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(serviceTypeCell)
        
        if carDetailingServiceType == CarDetailingServiceType.detailing.rawValue {
            let visiblityCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Service :  ".localizedString(), value:Helper.getDetailingService(statusCode: Helper.toInt(tenderDetails?.tenderCarDetailingOptions)),info:nil, height: Constants.CellHeightConstants.height_25)
            array.append(visiblityCell)
        }
        
        //Additional Information
        let descriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.tenderAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(descriptionCell)
        
        //Requested Date
        let requestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedDateCell)
        
        //Requested Time
        let requestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedTimeCell)
        
        // Location
        let locationCell = CellInfo(cellType: .VITenderLocationCell, placeHolder:Helper.toString(object: tenderDetails?.pickupLocation), value:Helper.toString(object: tenderDetails?.dropoffLocation) ,info:nil, height: Constants.CellHeightConstants.height_100)
        array.append(locationCell)
        
        if let totalImage = tenderDetails?.images, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info:imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }
        
        //Bid Details
        let bidTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Bid Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(bidTitleCell)
        
        //Days
        let daysCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Days Required for the Service :".localizedString(), value:"\(Helper.toString(object: tenderDetails?.requireDaysService)) Days" ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(daysCell)
        
        //Providing Delivery
        let pickUpCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Providing PickUp and DropOff :".localizedString(), value:Helper.toString(object: tenderDetails?.getPickupDropoff()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(pickUpCell)
        
       //Service Type
        let bidCarDetailingServiceType = Helper.toInt(tenderDetails?.carDetailingServiceType)
        let bidServiceTypeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Service Type : ".localizedString(), value:Helper.toString(object: Helper.getCarDetailingServiceType(statusCode: carDetailingServiceType)),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidServiceTypeCell)
        
        if bidCarDetailingServiceType == CarDetailingServiceType.detailing.rawValue {
            let visiblityCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Service :  ".localizedString(), value:Helper.getDetailingService(statusCode: Helper.toInt(tenderDetails?.bidCarDetailingOptions)),info:nil, height: Constants.CellHeightConstants.height_25)
            array.append(visiblityCell)
        }
               
        //Requested Date
        let bidRequestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedDateCell)
        
        let bidRequestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedTimeCell)
        
        //Additional Information
        let bidDescriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.bidAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(bidDescriptionCell)
        
        let bidPriceCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Bid Price :".localizedString(), value:Helper.toString(object: tenderDetails?.formattedPrice()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidPriceCell)
        
        return array
    }
}
