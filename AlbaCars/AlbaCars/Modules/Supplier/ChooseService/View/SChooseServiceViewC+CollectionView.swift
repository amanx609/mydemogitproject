//
//  SChooseServiceViewC+CollectionView.swift
//  AlbaCars
//
//  Created by Narendra on 12/16/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension SChooseServiceViewC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.chooseServiceDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellInfo = self.chooseServiceDataSource[indexPath.row]
        return self.getCell(collectionView: collectionView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.row == 0 {
            return CGSize(width: collectionView.bounds.width, height: self.chooseServiceDataSource[indexPath.row].height)
        }
        
        let cellWidth = collectionView.bounds.width/3-8
        return CGSize(width: cellWidth, height: cellWidth+Constants.CellHeightConstants.height_60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        let cellInfo = self.chooseServiceDataSource[indexPath.row]
        if let info = cellInfo.info {
            if let isSelected = info[Constants.UIKeys.isSelected] as? Bool {
                self.chooseServiceDataSource[indexPath.row].info?[Constants.UIKeys.isSelected] = !isSelected as AnyObject
                Threads.performTaskInMainQueue {
                    self.chooseServiceCollectionView.reloadData()
                }
            }
        }
    }
    
}

extension SChooseServiceViewC {
    func getCell(collectionView: UICollectionView, indexPath: IndexPath, cellInfo: CellInfo) -> UICollectionViewCell {
        guard let cellType = cellInfo.cellType else { return UICollectionViewCell() }
        switch cellType {
        case .SChooseServiceCollectionCell:
            let cell: SChooseServiceCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .SChooseServiceHeaderCollectionCell:
            let cell: SChooseServiceHeaderCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UICollectionViewCell()
        }
    }
}
