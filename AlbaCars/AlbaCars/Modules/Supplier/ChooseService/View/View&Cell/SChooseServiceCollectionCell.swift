//
//  SChooseServiceCollectionCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/16/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class SChooseServiceCollectionCell: BaseCollectionViewCell,ReusableView, NibLoadableView  {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var placeholderImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    //MARK: - Private Methods
    private func setupView() {
     //   self.bgView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round:self.bgView.frame.size.width/2)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
       // self.bgView.roundCorners(self.bgView.frame.size.width/2)
       // self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        self.titleLabel.text = cellInfo.placeHolder
        self.bgView.backgroundColor = .clear
        
        if let info = cellInfo.info {
            
            if let serviceType = info[Constants.UIKeys.chooseServiceType] as? ChooseServiceType, let isSelected = info[Constants.UIKeys.isSelected] as? Bool {
                self.placeholderImageView.image = isSelected == true ? serviceType.selectedImage : serviceType.unSelectedImage
              //  self.bgView.backgroundColor = isSelected == true ? UIColor.chooseServiceSelectedColor : UIColor.chooseServiceUnSelectedColor
            }
        }
    }
    
}
