//
//  SChooseServiceViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/16/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class SChooseServiceViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var chooseServiceCollectionView: UICollectionView!
    @IBOutlet weak var payNowButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    
    //MARK: - Variables
    var chooseServiceDataSource: [CellInfo] = []
    var viewModel: SChooseServiceVModeling?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Choose Service".localizedString(),barColor: .white, leftBarButtonsType: [.back], rightBarButtonsType: [])
    }
    
    deinit {
        print("deinit")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupCollectionView()
        self.roundCorners()
        self.setupView()
        if let dataSource = self.viewModel?.getChooseServiceDataSource() {
            self.chooseServiceDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.chooseServiceCollectionView.reloadData()
            }
        }
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = SChooseServiceViewM()
        }
    }
    
    private func setupCollectionView() {
        self.registerNibs()
        self.chooseServiceCollectionView.delegate = self
        self.chooseServiceCollectionView.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //depending upon direction of collection view
      //  self.chooseServiceCollectionView?.setCollectionViewLayout(layout, animated: true)
        
        let lastRowCenteredLayout = LastRowCenteredLayout()
        lastRowCenteredLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.chooseServiceCollectionView?.setCollectionViewLayout(lastRowCenteredLayout, animated: true)

    }
    
    private func registerNibs() {
        self.chooseServiceCollectionView.register(SChooseServiceHeaderCollectionCell.self)
        self.chooseServiceCollectionView.register(SChooseServiceCollectionCell.self)
    }
    
    private func roundCorners() {
        Threads.performTaskInMainQueue  {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
        }
    }
    
    private func setupView() {
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        self.payNowButton.setTitle(StringConstants.Text.proceed.localizedString(), for: .normal)
    }
    
    //MARK: - IBActions
    
    @IBAction func tapPayNow(_ sender: Any) {
        self.requestChooseServiceAPI()
    }
    
    //MARK: - API Methods
    func requestChooseServiceAPI() {
        self.viewModel?.requestUpdateServiceListAPI(arrData: self.chooseServiceDataSource) { (success) in
            if success {
               
                Threads.performTaskInMainQueue {
                    let documentsViewC = DIConfigurator.sharedInstance.getUploadDocumentViewC()
                    self.navigationController?.pushViewController(documentsViewC, animated: true)
                    
                }
            }
        }
    }
}

