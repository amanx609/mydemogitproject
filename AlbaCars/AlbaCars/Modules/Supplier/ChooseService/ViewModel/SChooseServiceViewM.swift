//
//  SChooseServiceViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/16/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SChooseServiceVModeling: BaseVModeling {
    func getChooseServiceDataSource() -> [CellInfo]
    func requestUpdateServiceListAPI(arrData: [CellInfo], completion: @escaping (Bool)-> Void)
}

class SChooseServiceViewM: SChooseServiceVModeling {
    
    func getChooseServiceDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        let chooseServiceType = ChooseServiceType.self
        //Header Cell
        let headerCell = CellInfo(cellType: .SChooseServiceHeaderCollectionCell, placeHolder: "Choose The Type of Services Your Garage Provides".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_84)
        array.append(headerCell)
        
        //Recovery Cell
        var recoveryInfo = [String: AnyObject]()
        recoveryInfo[Constants.UIKeys.isSelected] = false as AnyObject
        recoveryInfo[Constants.UIKeys.chooseServiceType] = chooseServiceType.recovery as AnyObject
        
        let recoveryCell = CellInfo(cellType: .SChooseServiceCollectionCell, placeHolder: chooseServiceType.recovery.title, value: "", info: recoveryInfo, height: Constants.CellHeightConstants.height_80)
        array.append(recoveryCell)
        
        //Vehicle Inspection Cell
        var vehicleInspectionInfo = [String: AnyObject]()
        vehicleInspectionInfo[Constants.UIKeys.isSelected] = false as AnyObject
        vehicleInspectionInfo[Constants.UIKeys.chooseServiceType] = chooseServiceType.vehicleInspection as AnyObject
        
        let vehicleInspectionCell = CellInfo(cellType: .SChooseServiceCollectionCell, placeHolder: chooseServiceType.vehicleInspection.title, value: "", info: vehicleInspectionInfo, height: Constants.CellHeightConstants.height_80)
        array.append(vehicleInspectionCell)
        
        //Car Servicing Cell
        var carServicingInfo = [String: AnyObject]()
        carServicingInfo[Constants.UIKeys.isSelected] = false as AnyObject
        carServicingInfo[Constants.UIKeys.chooseServiceType] = chooseServiceType.carServicing as AnyObject
        
        let  carServicingCell = CellInfo(cellType: .SChooseServiceCollectionCell, placeHolder: chooseServiceType.carServicing.title, value: "", info:  carServicingInfo, height: Constants.CellHeightConstants.height_80)
        array.append(carServicingCell)
        
        //Upholstery Cell
        var upholsteryInfo = [String: AnyObject]()
        upholsteryInfo[Constants.UIKeys.isSelected] = false as AnyObject
        upholsteryInfo[Constants.UIKeys.chooseServiceType] = chooseServiceType.upholstery as AnyObject
        
        let  upholsteryCell = CellInfo(cellType: .SChooseServiceCollectionCell, placeHolder: chooseServiceType.upholstery.title, value: "", info:  upholsteryInfo, height: Constants.CellHeightConstants.height_80)
        array.append(upholsteryCell)
        
        //Spare Parts Cell
        var sparePartsInfo = [String: AnyObject]()
        sparePartsInfo[Constants.UIKeys.isSelected] = false as AnyObject
        sparePartsInfo[Constants.UIKeys.chooseServiceType] = chooseServiceType.spareParts as AnyObject
        
        let  sparePartsCell = CellInfo(cellType: .SChooseServiceCollectionCell, placeHolder: chooseServiceType.spareParts.title, value: "", info:  sparePartsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(sparePartsCell)
        
        //Wheels Cell
        var wheelsInfo = [String: AnyObject]()
        wheelsInfo[Constants.UIKeys.isSelected] = false as AnyObject
        wheelsInfo[Constants.UIKeys.chooseServiceType] = chooseServiceType.wheels as AnyObject
        
        let  wheelsCell = CellInfo(cellType: .SChooseServiceCollectionCell, placeHolder: chooseServiceType.wheels.title, value: "", info:  wheelsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(wheelsCell)
        
        
        //Body Work Cell
        var bodyWorkInfo = [String: AnyObject]()
        bodyWorkInfo[Constants.UIKeys.isSelected] = false as AnyObject
        bodyWorkInfo[Constants.UIKeys.chooseServiceType] = chooseServiceType.bodyWork as AnyObject
        
        let  bodyWorkCell = CellInfo(cellType: .SChooseServiceCollectionCell, placeHolder: chooseServiceType.bodyWork.title, value: "", info:  bodyWorkInfo, height: Constants.CellHeightConstants.height_80)
        array.append(bodyWorkCell)
        
        //Car Detailing Cell
        var carDetailingInfo = [String: AnyObject]()
        carDetailingInfo[Constants.UIKeys.isSelected] = false as AnyObject
        carDetailingInfo[Constants.UIKeys.chooseServiceType] = chooseServiceType.carDetailing as AnyObject
        
        let  carDetailingCell = CellInfo(cellType: .SChooseServiceCollectionCell, placeHolder: chooseServiceType.carDetailing.title, value: "", info:  carDetailingInfo, height: Constants.CellHeightConstants.height_80)
        array.append(carDetailingCell)
        
        
        //Window Tinting Cell
        var windowTintingInfo = [String: AnyObject]()
        windowTintingInfo[Constants.UIKeys.isSelected] = false as AnyObject
        windowTintingInfo[Constants.UIKeys.chooseServiceType] = chooseServiceType.windowTinting as AnyObject
        
        let  windowTintingCell = CellInfo(cellType: .SChooseServiceCollectionCell, placeHolder: chooseServiceType.windowTinting.title, value: "", info:  windowTintingInfo, height: Constants.CellHeightConstants.height_80)
        array.append(windowTintingCell)
        
        //Bank Valuation Cell
        var bankValuationInfo = [String: AnyObject]()
        bankValuationInfo[Constants.UIKeys.isSelected] = false as AnyObject
        bankValuationInfo[Constants.UIKeys.chooseServiceType] = chooseServiceType.bankValuation as AnyObject
        
        let  bankValuationCell = CellInfo(cellType: .SChooseServiceCollectionCell, placeHolder: chooseServiceType.bankValuation.title, value: "", info:  bankValuationInfo, height: Constants.CellHeightConstants.height_80)
        array.append(bankValuationCell)
        
        //Insurance Cell
        var insuranceInfo = [String: AnyObject]()
        insuranceInfo[Constants.UIKeys.isSelected] = false as AnyObject
        insuranceInfo[Constants.UIKeys.chooseServiceType] = chooseServiceType.insurance as AnyObject
        
        let  insuranceCell = CellInfo(cellType: .SChooseServiceCollectionCell, placeHolder:  chooseServiceType.insurance.title, value: "", info:  insuranceInfo, height: Constants.CellHeightConstants.height_80)
        array.append(insuranceCell)
        
        return array
    }
    
    //MARK: - API Methods
    func requestUpdateServiceListAPI(arrData: [CellInfo], completion: @escaping (Bool)-> Void) {
        if self.validateUpdateServiceListData(arrData: arrData) {
            let params = self.getUpdateServiceListParams(arrData: arrData)
            
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .updateServiceList(param: params))) { (response, success) in
                if success {
                    if let _ =  response as? [String: AnyObject]
                        
                    {
                        if let serviceList = params[ConstantAPIKeys.serviceList] as? [Int] {
                            UserDefaultsManager.sharedInstance.saveIntArrayValueFor(key: .selectedServiceList, value: serviceList)
//                            if let user = UserDefaultsManager.sharedInstance.getUserValueFor(key: .user) {
//                                       user.selectedServiceList = serviceList
//                                       UserDefaultsManager.sharedInstance.saveUserValueFor(key:.user, value: user)
//
//                                   }
                        }

                        completion(success)
                    }
                }
            }
        }
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
    private func validateUpdateServiceListData(arrData: [CellInfo]) -> Bool {
        var isValid = false
        for (_, element) in arrData.enumerated() {
            if let isSelected = element.info?[Constants.UIKeys.isSelected] as? Bool, isSelected == true {
                isValid = true
                break
            }
        }
        if isValid == false {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please choose service".localizedString())
        }
        return isValid
    }
    
    private func getUpdateServiceListParams(arrData: [CellInfo])-> APIParams {
        var params: APIParams = APIParams()
        var serviceList = [Int]()
        for (_, element) in arrData.enumerated() {
            if let isSelected = element.info?[Constants.UIKeys.isSelected] as? Bool, isSelected == true {
                if let serviceType = element.info?[Constants.UIKeys.chooseServiceType] as? ChooseServiceType {
                    serviceList.append(serviceType.rawValue)
                }
            }
        }
        
        params[ConstantAPIKeys.serviceList] = serviceList as AnyObject
        return params
    }
}
