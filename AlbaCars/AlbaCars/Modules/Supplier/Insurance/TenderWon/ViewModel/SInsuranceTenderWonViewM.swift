//
//  SInsuranceTenderWonViewM.swift
//  AlbaCars
//
//  Created by Narendra on 2/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SInsuranceTenderWonViewModeling {
    func getTenderWonDataSource(tenderDetails: SupplierTender?) -> [CellInfo]
}

class SInsuranceTenderWonViewM: SInsuranceTenderWonViewModeling {
    
    func getTenderWonDataSource(tenderDetails: SupplierTender?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Details Title
        let carTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Car Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(carTitleCell)
        
        //Car Details
        var carDetailsInfo = [String: AnyObject]()
        carDetailsInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        let carCell = CellInfo(cellType: .TenderCarDetailsCell, placeHolder:"Car Details".localizedString(), value:"" ,info:carDetailsInfo, height: Constants.CellHeightConstants.height_115)
        array.append(carCell)
        
        //Tender Details Title
        let tenderTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Tender Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(tenderTitleCell)
        
        //Car Mileage
        let carMileageCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Car Mileage : ".localizedString(),value:Helper.toString(object:tenderDetails?.formattedCarMileage()),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(carMileageCell)
        
        //Insurance Type
        let insuranceType = Helper.toInt(tenderDetails?.tenderTypeOfInsurance)
        let insuranceTypeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Insurance Type : ".localizedString(), value:Helper.getInsuranceType(statusCode: insuranceType),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(insuranceTypeCell)
        
        //Vehicle Value
        let vehicleValueCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Vehicle Value : ".localizedString(), value:Helper.toString(object: tenderDetails?.formattedTenderVehiclePrice()),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(vehicleValueCell)
        
        //Additional Information
        let descriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.tenderAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(descriptionCell)
        
        //Requested Date
        let requestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedDateCell)
        
        //Requested Time
        let requestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedTimeCell)
        
        if let totalImage = tenderDetails?.images, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info:imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }
        
        //Bid Details
        let bidTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Bid Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(bidTitleCell)
        
        //Days
        let daysCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Days Required for the Service :".localizedString(), value:"\(Helper.toString(object: tenderDetails?.requireDaysService)) Days" ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(daysCell)
        
        //Insurance Type
        let bidInsuranceType = Helper.toInt(tenderDetails?.bidTypeOfInsurance)
        let bidInsuranceTypeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Insurance Type : ".localizedString(), value:Helper.getInsuranceType(statusCode: bidInsuranceType),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidInsuranceTypeCell)
        
        //Vehicle Value
        let bidVehicleValueCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Vehicle Value : ".localizedString(), value:Helper.toString(object: tenderDetails?.formattedBidCarPrice()),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidVehicleValueCell)
        
        //Requested Date
        let bidRequestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedDateCell)
        
        let bidRequestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedTimeCell)
        
        //Additional Information
        let bidDescriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.bidAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(bidDescriptionCell)
        
        let bidPriceCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Bid Price :".localizedString(), value:Helper.toString(object: tenderDetails?.formattedPrice()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidPriceCell)
        
        if let totalImage = tenderDetails?.bidImages, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info:imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }
        
        //ContactInfoCell
        var contactInfoInfo = [String: AnyObject]()
        contactInfoInfo[Constants.UIKeys.image] = tenderDetails?.image as AnyObject
        let contactInfoCell = CellInfo(cellType: .RDContactInfoCell, placeHolder: Helper.toString(object: tenderDetails?.name), value: Helper.toString(object: tenderDetails?.pickupLocation), info: contactInfoInfo, height: UITableView.automaticDimension)
        array.append(contactInfoCell)
        
        let secondBidCell = CellInfo(cellType: .VIContactInfoCell, placeHolder: Helper.toString(object: tenderDetails?.mobile), value: Helper.toString(object: tenderDetails?.email), info: nil, height: Constants.CellHeightConstants.height_100)
        array.append(secondBidCell)
        
        let infoCell = CellInfo(cellType: .TenderWonInfoCell, placeHolder: "You will be able to request payment only after both the customer & you have marked job as completed".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(infoCell)
        
        let infoCell2 = CellInfo(cellType: .TenderWonInfoCell, placeHolder: "Please tap on \"Mark Job as Completed\" button only after Services/Tenders are completed".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(infoCell2)
        
        let completedCell = CellInfo(cellType: .TenderCompletedCell, placeHolder: "Mark Job as Completed".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(completedCell)
        
        return array
    }
}
