//
//  SInsuranceBidSubmittedViewM.swift
//  AlbaCars
//
//  Created by Narendra on 2/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SInsuranceBidSubmittedViewModeling {
    func getBidSubmittedDataSource(tenderDetails: SupplierTender?) -> [CellInfo]
}

class SInsuranceBidSubmittedViewM: SInsuranceBidSubmittedViewModeling {
    
    func getBidSubmittedDataSource(tenderDetails: SupplierTender?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Details Title
        let carTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Car Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(carTitleCell)
        
        //Car Details
        var carDetailsInfo = [String: AnyObject]()
        carDetailsInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        let carCell = CellInfo(cellType: .TenderCarDetailsCell, placeHolder:"Car Details".localizedString(), value:"" ,info:carDetailsInfo, height: Constants.CellHeightConstants.height_115)
        array.append(carCell)
        
        //Tender Details Title
        let tenderTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Tender Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(tenderTitleCell)
        
        //Car Mileage
        let carMileageCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Car Mileage : ".localizedString(),value:Helper.toString(object:tenderDetails?.formattedCarMileage()),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(carMileageCell)
        
        //Insurance Type
        let insuranceType = Helper.toInt(tenderDetails?.tenderTypeOfInsurance)
        let insuranceTypeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Insurance Type : ".localizedString(), value:Helper.getInsuranceType(statusCode: insuranceType),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(insuranceTypeCell)
        
        //Vehicle Value
        let vehicleValueCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Vehicle Value : ".localizedString(), value:Helper.toString(object: tenderDetails?.formattedTenderVehiclePrice()),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(vehicleValueCell)
        
        //Additional Information
        let descriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.tenderAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(descriptionCell)
        
        //Requested Date
        let requestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedDateCell)
        
        //Requested Time
        let requestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedTimeCell)
        
        if let totalImage = tenderDetails?.images, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info:imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }
        
        //Bid Details
        let bidTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Bid Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(bidTitleCell)
        
        //Days
        let daysCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Days Required for the Service :".localizedString(), value:"\(Helper.toString(object: tenderDetails?.requireDaysService)) Days" ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(daysCell)
        
        //Insurance Type
        let bidInsuranceType = Helper.toInt(tenderDetails?.bidTypeOfInsurance)
        let bidInsuranceTypeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Insurance Type : ".localizedString(), value:Helper.getInsuranceType(statusCode: bidInsuranceType),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidInsuranceTypeCell)
        
        //Vehicle Value
        let bidVehicleValueCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Vehicle Value : ".localizedString(), value:Helper.toString(object: tenderDetails?.formattedBidCarPrice()),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidVehicleValueCell)
        
        let roadSideAssistanceCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Roadside Assistance :".localizedString(), value:Helper.toString(object: tenderDetails?.getRoadsideAssistance()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(roadSideAssistanceCell)
        
        let driverPassCoverCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Driver and Passenger Cover :".localizedString(), value:Helper.toInt(tenderDetails?.bidDriverPassengerCover) == 1 ? "Yes".localizedString() : "No".localizedString() ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(driverPassCoverCell)
        
        //Requested Date
        let bidRequestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedDateCell)
        
        let bidRequestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedTimeCell)
        
        //Additional Information
        let bidDescriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.bidAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(bidDescriptionCell)
        
        let bidPriceCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Bid Price :".localizedString(), value:Helper.toString(object: tenderDetails?.formattedPrice()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidPriceCell)
        
        if let totalImage = tenderDetails?.bidImages, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info:imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }
        
        return array
    }
}
