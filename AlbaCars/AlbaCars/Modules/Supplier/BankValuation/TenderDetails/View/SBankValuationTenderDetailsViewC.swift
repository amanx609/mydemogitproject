//
//  SBankValuationTenderDetailsViewC.swift
//  AlbaCars
//
//  Created by Narendra on 2/21/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SBankValuationTenderDetailsViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var timeLeftLabel: UILabel!
    @IBOutlet weak var tenderTableView: UITableView!
    @IBOutlet weak var bidNowButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var headerView: UIView!
    
    //MARK: - Variables
    var viewModel: SBankValuationTenderDetailsViewModeling?
    var tenderDataSource: [CellInfo] = []
    var tenderDetails: SupplierTender?
    var durationTimer : Timer?
    var bankDataSource: [[String: AnyObject]] = []
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    deinit {
        self.invalidateTimer()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Bank Valuation".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setupView()
        self.setUpTableView()
        self.setupTimer()
        self.requestBankAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = SBankValuationTenderDetailsViewM()
        }
    }
    
    private func setupView() {
        
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.bidNowButton.setTitle("Bid Now".localizedString(), for: .normal)
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.tenderTableView.delegate = self
        self.tenderTableView.dataSource = self
        self.tenderTableView.separatorStyle = .none
        self.tenderTableView.tableHeaderView =  self.headerView
        
        if let dataSource = self.viewModel?.getTenderDetailsDataSource(tenderDetails: tenderDetails) {
            self.tenderDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.tenderTableView.reloadData()
            }
        }
    }
    
    private func registerNibs() {
        self.tenderTableView.register(TenderDetailsTitleCell.self)
        self.tenderTableView.register(TenderCarDetailsCell.self)
        self.tenderTableView.register(VITenderLocationCell.self)
        self.tenderTableView.register(AdditionalInfoCell.self)
        self.tenderTableView.register(EnterBidDetailsCell.self)
        self.tenderTableView.register(ThreeImageCell.self)
    }
    
    private func setupTimer() {
        self.invalidateTimer()
        self.durationTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTenderDuration), userInfo: nil, repeats: true)
    }
    
    private func invalidateTimer() {
        self.durationTimer?.invalidate()
        self.durationTimer = nil
    }
    
    //MARK: - Selector Methods
    @objc func updateTenderDuration() {
        if var timerDuration = self.tenderDetails?.timerDuration {
            if timerDuration == 0 {
                self.invalidateTimer()
                return
            }
            timerDuration -= 1
            self.timeLeftLabel.text = timerDuration.convertToTimerFormat()
            self.tenderDetails?.timerDuration = timerDuration
        }
    }
    
    func showDropDownAt(indexPath: IndexPath,selectedRow: Int) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        if self.bankDataSource.count == 0 {
            self.requestBankAPI()
            return
        }
        listPopupView.initializeViewWith(title: "Select Bank".localizedString(), arrayList: self.bankDataSource, key: ConstantAPIKeys.name) { [weak self] (response) in
            guard let sSelf = self,
                let bankName = response[ConstantAPIKeys.name] as? String else { return }
            
            let cellInfo = sSelf.tenderDataSource[indexPath.row]
            if let info = cellInfo.info,
                var itemsCoveredData = info[Constants.UIKeys.itemsCovered] as? [CellInfo]
            {
                itemsCoveredData[selectedRow].value = bankName
                itemsCoveredData[selectedRow].info?[Constants.UIKeys.id] = Helper.toInt(response[ConstantAPIKeys.bankId]) as AnyObject
                sSelf.tenderDataSource[indexPath.row].info?[Constants.UIKeys.itemsCovered] = itemsCoveredData as AnyObject
                Threads.performTaskInMainQueue {
                    sSelf.tenderTableView.reloadData()
                }
            }
        }
        
        listPopupView.showWithAnimated(animated: true)
    }
    
    //MARK: - API Methods
    func requestBankAPI() {
        self.viewModel?.requestBankAPI(completion: { (success, bankList) in
            if success {
                self.bankDataSource = bankList
            }
        })
    }
    
    func requestBidAPI() {
        
        if let info = self.tenderDataSource.last?.info,
            let itemsCoveredData = info[Constants.UIKeys.itemsCovered] as? [CellInfo]
        {
            self.viewModel?.requestBidAPI(bidDetails: itemsCoveredData, tenderDetails: self.tenderDetails, completion: { (responseData) in
                Threads.performTaskInMainQueue {
                    self.navigationController?.popViewController(animated: true)
                    Alert.showOkAlert(title:StringConstants.Text.AppName, message: "Bid Detail Submitted Successfully.".localizedString())
                }
            })
        }
    }
    
    //MARK: - IBActions
    @IBAction func bidTapped(_ sender: Any) {
        self.requestBidAPI()
    }
}
