//
//  SBankValuationJobCompletedViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 2/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension SBankValuationJobCompletedViewC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jobCompletedDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getCell(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.jobCompletedDataSource[indexPath.row].height
    }
    
}

extension SBankValuationJobCompletedViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.jobCompletedDataSource[indexPath.row]
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .TenderDetailsTitleCell:
            let cell: TenderDetailsTitleCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .TenderDetailsAttributedCell:
            let cell: TenderDetailsTitleCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureAttributedView(cellInfo: cellInfo)
            return cell
        case .TenderCarDetailsCell:
            let cell: TenderCarDetailsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .AdditionalInfoCell:
            let cell: AdditionalInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .RDContactInfoCell:
            let cell: RDContactInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureTenderDetailsView(cellInfo: cellInfo)
            return cell
        case .VIContactInfoCell:
            let cell: VIContactInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureTenderDetailsView(cellInfo: cellInfo)
            return cell
        case .TenderWonInfoCell:
            let cell: TenderWonInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .TenderCompletedCell:
            let cell: TenderCompletedCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .ThreeImageCell:
            let cell: ThreeImageCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}


//Mark: ThreeImageCellDelegate
extension SBankValuationJobCompletedViewC:ThreeImageCellDelegate {
    func didTapViewAllCell(cell: ThreeImageCell) {
        
        guard let indexPath = self.jobCompletedTableView.indexPath(for: cell) else { return }
        let cellInfo = self.jobCompletedDataSource[indexPath.row]
        if let info = cellInfo.info, let totalImage = info[Constants.UIKeys.images] as? [String] {
            let imageViewerVC = DIConfigurator.sharedInstance.getImageViewerVC()
            imageViewerVC.imageDataSource = totalImage
            self.navigationController?.pushViewController(imageViewerVC, animated: true)
        }
    }
    
}
