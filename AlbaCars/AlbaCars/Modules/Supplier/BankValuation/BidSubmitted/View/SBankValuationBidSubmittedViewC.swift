//
//  SBankValuationBidSubmittedViewC.swift
//  AlbaCars
//
//  Created by Narendra on 2/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SBankValuationBidSubmittedViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bidTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    
    //MARK: - Variables
    var viewModel: SBankValuationBidSubmittedViewModeling?
    var bidDataSource: [CellInfo] = []
    var tenderDetails: SupplierTender?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Bid Submitted".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setUpTableView()
        self.priceLabel.text = "Submitted Bid Amount : ".localizedString() + Helper.toString(object: tenderDetails?.formattedPrice())
        
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = SBankValuationBidSubmittedViewM()
        }
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.bidTableView.delegate = self
        self.bidTableView.dataSource = self
        self.bidTableView.separatorStyle = .none
        self.bidTableView.tableHeaderView =  self.headerView
        
        if let dataSource = self.viewModel?.getBidSubmittedDataSource(tenderDetails: self.tenderDetails) {
            self.bidDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.bidTableView.reloadData()
            }
        }
    }
    
    private func registerNibs() {
        self.bidTableView.register(TenderDetailsTitleCell.self)
        self.bidTableView.register(TenderCarDetailsCell.self)
        self.bidTableView.register(AdditionalInfoCell.self)
        self.bidTableView.register(ThreeImageCell.self)        
    }
}
