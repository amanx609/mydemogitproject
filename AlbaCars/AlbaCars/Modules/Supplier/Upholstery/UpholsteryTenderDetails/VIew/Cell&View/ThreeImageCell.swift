//
//  ThreeImageCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/30/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol ThreeImageCellDelegate: class {
    func didTapViewAllCell(cell: ThreeImageCell)
}

class ThreeImageCell: BaseTableViewCell, ReusableView, NibLoadableView {
    //MARK: - IBOutlets
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var carImageViewFirst: UIImageView!
    @IBOutlet weak var carImageViewSecond: UIImageView!
    @IBOutlet weak var carImageViewThird: UIImageView!
    
    //MARK: - Variables
    weak var delegate: ThreeImageCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        let radius =  Constants.UIConstants.sizeRadius_30
        self.firstView.roundCorners(radius)
        self.secondView.roundCorners(radius)
        self.thirdView.roundCorners(radius)
        
        self.carImageViewFirst.roundCorners(radius)
        self.carImageViewSecond.roundCorners(radius)
        self.carImageViewThird.roundCorners(radius)
        
        self.carImageViewFirst.makeLayer(color: .white, boarderWidth: Constants.UIConstants.borderWidth_2half, round: radius)
        self.carImageViewSecond.makeLayer(color: .white, boarderWidth: Constants.UIConstants.borderWidth_2half, round: radius)
        self.carImageViewThird.makeLayer(color: .white, boarderWidth: Constants.UIConstants.borderWidth_2half, round: radius)
        //
        
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        
        if let info = cellInfo.info, let totalImage = info[Constants.UIKeys.images] as? [String] {
            
            self.firstView.isHidden =  true
            self.secondView.isHidden =  true
            self.thirdView.isHidden =  true
            
          //  if let totalImage = images {
                
                if totalImage.count > 2 {
                    self.firstView.isHidden =  false
                    self.secondView.isHidden =  false
                    self.thirdView.isHidden =  false
                } else if totalImage.count > 1 {
                    self.firstView.isHidden =  false
                    self.secondView.isHidden =  false
                } else if totalImage.count > 0 {
                    self.firstView.isHidden =  false
                }
                
                self.carImageViewFirst.setImage(urlStr:self.getImageName(images: totalImage, index: 0), placeHolderImage: #imageLiteral(resourceName: "placeholder"))
                self.carImageViewSecond.setImage(urlStr:self.getImageName(images: totalImage, index: 1), placeHolderImage: #imageLiteral(resourceName: "placeholder"))
                self.carImageViewThird.setImage(urlStr:self.getImageName(images: totalImage, index: 2), placeHolderImage: #imageLiteral(resourceName: "placeholder"))
            }
        //}
    }
    
    func getImageName(images:[String], index:Int) -> String {
        
        if images.count > index {
            return images[index]
        }
        
        return ""
    }
    
    //MARK: - IBActions
    
    @IBAction func viewDetailsButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapViewAllCell(cell: self)
        }
    }
    
}
