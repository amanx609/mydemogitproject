//
//  SUpholsteryJobCompletedViewC.swift
//  AlbaCars
//
//  Created by Narendra on 1/30/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SUpholsteryJobCompletedViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var jobCompletedTableView: UITableView!
 //   @IBOutlet weak var headerView: UIView!
 //   @IBOutlet weak var pemdingAmountLabel: UILabel!
    
    //MARK: - Variables
    var viewModel: SUpholsteryJobCompletedViewModeling?
    var jobCompletedDataSource: [CellInfo] = []
    var tenderDetails: SupplierTender?

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Job Completed".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setUpTableView()
   //     self.pemdingAmountLabel.text = "Payment Pending : ".localizedString() + Helper.toString(object: tenderDetails?.formattedPrice())
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = SUpholsteryJobCompletedViewM()
        }
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.jobCompletedTableView.delegate = self
        self.jobCompletedTableView.dataSource = self
        self.jobCompletedTableView.separatorStyle = .none
      //  self.jobCompletedTableView.tableHeaderView =  self.headerView
        
        if let dataSource = self.viewModel?.getJobCompletedDataSource(tenderDetails: self.tenderDetails) {
            self.jobCompletedDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.jobCompletedTableView.reloadData()
            }
        }
        
        if let jobCompletedHeaderView = JobCompletedHeaderView.inistancefromNib() {
            jobCompletedHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 300)
             jobCompletedHeaderView.setupView(tenderDetails: tenderDetails)
            self.jobCompletedTableView.tableHeaderView = jobCompletedHeaderView
        }
    }
    
    private func registerNibs() {
        self.jobCompletedTableView.register(RDContactInfoCell.self)
        self.jobCompletedTableView.register(TenderDetailsTitleCell.self)
        self.jobCompletedTableView.register(TenderCarDetailsCell.self)
        self.jobCompletedTableView.register(VITenderLocationCell.self)
        self.jobCompletedTableView.register(AdditionalInfoCell.self)
        self.jobCompletedTableView.register(ThreeImageCell.self)
        self.jobCompletedTableView.register(VIContactInfoCell.self)        
    }
    
}
