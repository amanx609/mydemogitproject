//
//  SUMyTenderCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/30/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol SUMyTenderCellDelegate: class {
    func didTapViewDetailsCell(cell: SUMyTenderCell)
}

class SUMyTenderCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var viewDetailsButton: UIButton!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var requestedDateLabel: UILabel!
    @IBOutlet weak var requestedTimeLabel: UILabel!
    @IBOutlet weak var bidPriceLabel: UILabel!
    @IBOutlet weak var bidStatusLabel: UILabel!
    @IBOutlet weak var bidStatusView: UIView!
    @IBOutlet weak var jobDescLabel: UILabel!

    //MARK: - Variables
    weak var delegate: SUMyTenderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.bidStatusView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.viewDetailsButton.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.bgView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
    }
    
    //MARK: - Public Methods
    func configureView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        self.jobDescLabel.text = Helper.toString(object: supplierTender.jobDescription)
        self.requestedDateLabel.attributedText = String.getAttributedText(firstText: "Requested Date : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedDate()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.requestedTimeLabel.attributedText = String.getAttributedText(firstText: "Requested Time : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedTime()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.bidPriceLabel.text = "AED :".localizedString() + " \(Helper.toString(object: supplierTender.bidPrice))"
        let (title, color) = Helper.getTenderBidStatus(statusCode: Helper.toInt(supplierTender.status))
        self.bidStatusLabel.text = title
        self.bidStatusView.backgroundColor = color
    }
    
    //MARK: - IBActions
    
    @IBAction func viewDetailsButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapViewDetailsCell(cell: self)
        }
    }
}
