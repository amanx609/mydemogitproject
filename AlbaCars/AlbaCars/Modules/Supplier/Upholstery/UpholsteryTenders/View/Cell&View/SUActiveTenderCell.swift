//
//  SUActiveTenderCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/30/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol SUActiveTenderCellDelegate: class {
    func didTapViewDetailsCell(cell: SUActiveTenderCell)
}

class SUActiveTenderCell: BaseTableViewCell, ReusableView, NibLoadableView {

   //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var viewDetailsButton: UIButton!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var jobDescLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var liveLabel: UILabel!
    @IBOutlet weak var carImageViewFirst: UIImageView!
    @IBOutlet weak var carImageViewSecond: UIImageView!
    @IBOutlet weak var carImageViewThird: UIImageView!

    //MARK: - Variables
    weak var delegate: SUActiveTenderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.viewDetailsButton.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.bgView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        
        
        self.carImageViewFirst.roundCorners(Constants.UIConstants.sizeRadius_13)
        self.carImageViewSecond.roundCorners(Constants.UIConstants.sizeRadius_13)
        self.carImageViewThird.roundCorners(Constants.UIConstants.sizeRadius_13)
        
        self.carImageViewFirst.makeLayer(color: .white, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_13)
        self.carImageViewSecond.makeLayer(color: .white, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_13)
        self.carImageViewThird.makeLayer(color: .white, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_13)
//
//        self.carImageViewFirst.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_13, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_13))
//        self.carImageViewSecond.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_13, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_13))
//        self.carImageViewThird.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_13, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_13))

    }
    
    //MARK: - Public Methods
    func configureView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        self.jobDescLabel.text = Helper.toString(object: supplierTender.description)

        self.carImageViewFirst.isHidden =  true
        self.carImageViewSecond.isHidden =  true
        self.carImageViewThird.isHidden =  true
        
        if let totalImage =  supplierTender.images {
            
            if totalImage.count > 2 {
                self.carImageViewFirst.isHidden =  false
                self.carImageViewSecond.isHidden =  false
                self.carImageViewThird.isHidden =  false
            } else if totalImage.count > 1 {
                self.carImageViewFirst.isHidden =  false
                self.carImageViewSecond.isHidden =  false
            } else if totalImage.count > 0 {
                self.carImageViewFirst.isHidden =  false
            }
            
            self.carImageViewFirst.setImage(urlStr:self.getImageName(images: totalImage, index: 0), placeHolderImage: #imageLiteral(resourceName: "placeholder"))
            self.carImageViewSecond.setImage(urlStr:self.getImageName(images: totalImage, index: 0), placeHolderImage: #imageLiteral(resourceName: "placeholder"))
            self.carImageViewFirst.setImage(urlStr:self.getImageName(images: totalImage, index: 0), placeHolderImage: #imageLiteral(resourceName: "placeholder"))
        }
        
        //Timer
        if let timerDuration = supplierTender.timerDuration {
            self.timeLabel.text = timerDuration.convertToTimerFormat()
        }
    }
    
    func getImageName(images:[String], index:Int) -> String {
        
        if images.count > index {
            return images[index]
        }
        
        return ""
    }
    
    //MARK: - IBActions
    
    @IBAction func viewDetailsButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapViewDetailsCell(cell: self)
        }
    }
    
}
