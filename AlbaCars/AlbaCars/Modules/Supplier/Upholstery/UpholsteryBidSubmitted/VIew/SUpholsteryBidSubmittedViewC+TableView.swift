//
//  SUpholsteryBidSubmittedViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 1/30/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension SUpholsteryBidSubmittedViewC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bidDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getCell(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.bidDataSource[indexPath.row].height
    }
    
}

extension SUpholsteryBidSubmittedViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.bidDataSource[indexPath.row]
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .TenderDetailsTitleCell:
            let cell: TenderDetailsTitleCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .TenderDetailsAttributedCell:
            let cell: TenderDetailsTitleCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureAttributedView(cellInfo: cellInfo)
            return cell
        case .TenderCarDetailsCell:
            let cell: TenderCarDetailsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .VITenderLocationCell:
            let cell: VITenderLocationCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .AdditionalInfoCell:
            let cell: AdditionalInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .ThreeImageCell:
            let cell: ThreeImageCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//Mark: ThreeImageCellDelegate
extension SUpholsteryBidSubmittedViewC:ThreeImageCellDelegate {
    func didTapViewAllCell(cell: ThreeImageCell) {
        let imageViewerVC = DIConfigurator.sharedInstance.getImageViewerVC()
        imageViewerVC.imageDataSource = self.tenderDetails?.images ?? []
        self.navigationController?.pushViewController(imageViewerVC, animated: true)
    }
    
}
