//
//  SWindowTintingTenderDetailsViewM.swift
//  AlbaCars
//
//  Created by Narendra on 2/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SWindowTintingTenderDetailsViewModeling {
    func getTenderDetailsDataSource(tenderDetails: SupplierTender?) -> [CellInfo]
    func requestBidAPI(bidDetails: [CellInfo],tenderDetails: SupplierTender?, completion: @escaping (Bool) -> Void)
}

class SWindowTintingTenderDetailsViewM: SWindowTintingTenderDetailsViewModeling {
    
    func getTenderDetailsDataSource(tenderDetails: SupplierTender?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Details Title
        let carTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Car Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(carTitleCell)
        
        //Car Details
        var carDetailsInfo = [String: AnyObject]()
        carDetailsInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        let carCell = CellInfo(cellType: .TenderCarDetailsCell, placeHolder:"Car Details".localizedString(), value:"" ,info:carDetailsInfo, height: Constants.CellHeightConstants.height_115)
        array.append(carCell)
        
        //Tender Details Title
        let tenderTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Tender Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(tenderTitleCell)
        
        //Tint Color
        let tintColorCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Tint Color : ".localizedString(), value:Helper.toString(object: tenderDetails?.tintColor),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(tintColorCell)
        
        //Tint Visiblity
        let visiblityCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Tint Visiblity : ".localizedString(), value:Helper.toString(object: tenderDetails?.tintPercentage)+"%".localizedString(),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(visiblityCell)
        
        //Additional Information
        let descriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.additionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(descriptionCell)
        
        //Requested Date
        let requestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedDateCell)
        
        //Requested Time
        let requestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedTimeCell)
        
        // Location
        let locationCell = CellInfo(cellType: .VITenderLocationCell, placeHolder:Helper.toString(object: tenderDetails?.pickupLocation), value:Helper.toString(object: tenderDetails?.dropoffLocation) ,info:nil, height: Constants.CellHeightConstants.height_100)
        array.append(locationCell)
        
        //Enter Bid Details
        let bidTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Enter Bid Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(bidTitleCell)
        
        let bidDataSource = self.getEnterBidDataSource()
        var height: CGFloat = 35.0
        for bidData in bidDataSource {
            height = height + bidData.height
        }
        
        var enterBidInfo = [String: AnyObject]()
        enterBidInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        enterBidInfo[Constants.UIKeys.itemsCovered] = bidDataSource as AnyObject
        let enterBidCell = CellInfo(cellType: .EnterBidDetailsCell, placeHolder:"", value:"" ,info:enterBidInfo, height: height)
        array.append(enterBidCell)
        return array
    }
    
    func getEnterBidDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        let daysRequiredCell = CellInfo(cellType: .DaysRequiredCell, placeHolder:"", value:"1" ,info:nil, height:Constants.CellHeightConstants.height_50)
        array.append(daysRequiredCell)
        
        var dateInfo = [String: AnyObject]()
        dateInfo[Constants.UIKeys.date] = true as AnyObject
        let dateCell = CellInfo(cellType: .JobCell, placeHolder:"Date for Job".localizedString(), value:"" ,info:dateInfo, height:Constants.CellHeightConstants.height_60)
        array.append(dateCell)
        
        var timeInfo = [String: AnyObject]()
        timeInfo[Constants.UIKeys.date] = false as AnyObject
        let timeCell = CellInfo(cellType: .JobCell, placeHolder:"Time for Job".localizedString(), value:"" ,info:timeInfo, height:Constants.CellHeightConstants.height_60)
        array.append(timeCell)
        
        //Tint Color
        var tintColorInfo = [String: AnyObject]()
        tintColorInfo[Constants.UIKeys.inputType] = TextInputType.tintColor as AnyObject
        let typeCell = CellInfo(cellType: .RecoveryTypeCell, placeHolder:"Tint Color".localizedString(), value:"" ,info:tintColorInfo, height:Constants.CellHeightConstants.height_60)
        array.append(typeCell)
        
        //Tint %age
        var tintInfo = [String: AnyObject]()
        tintInfo[Constants.UIKeys.cellInfo] =  DropDownType.tintPercentage as AnyObject
        let serviceCell = CellInfo(cellType: .RecoveryTypeCell, placeHolder:"Tint %age".localizedString(), value:"" ,info:tintInfo, height:Constants.CellHeightConstants.height_60)
        array.append(serviceCell)
        
        let additionalInfoCell = CellInfo(cellType: .EnterAdditionalInfoCell, placeHolder:"Additional Information".localizedString(), value:"" ,info:nil, height:Constants.CellHeightConstants.height_110)
        array.append(additionalInfoCell)
        
        var completedInfo = [String: AnyObject]()
        completedInfo[Constants.UIKeys.firstValue] = true as AnyObject
        completedInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "checked_circle")
        let completedCell = CellInfo(cellType: .TenderCompletedCell, placeHolder: "I will provide PickUp and DropOff".localizedString(), value: "", info: completedInfo, height: Constants.CellHeightConstants.height_30)
        array.append(completedCell)
        
        let amountCell = CellInfo(cellType: .BidAmountCell, placeHolder:"Enter Bid Amount".localizedString(), value:"" ,info:nil, height:Constants.CellHeightConstants.height_70)
        array.append(amountCell)
        
        return array
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
    func validateBidData(bidDetails: [CellInfo]) -> Bool {
        var isValid = true
        var message = ""
        if let days = bidDetails[0].value, days.isEmpty {
            message = "Please add required day for the Service"
            isValid = false
        } else if let date = bidDetails[1].value, date.isEmpty  {
            message = "Please select date."
            isValid = false
        } else if let time = bidDetails[2].value, time.isEmpty {
            message = "Please select time."
            isValid = false
        } else if let color = bidDetails[3].value, color.isEmpty {
            message = "Please enter tint color"
            isValid = false
        }  else if let recovery = bidDetails[4].value, recovery.isEmpty {
            message = "Please select tint age"
            isValid = false
        } else if let recovery = bidDetails[7].value, recovery.isEmpty {
            message = "Please enter bid amount."
            isValid = false
        }
        
        if !isValid {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: message.localizedString())
        }
        return isValid
    }
    
    func getBidParams(bidDetails: [CellInfo],tenderDetails: SupplierTender?)-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.tenderId] = Helper.toInt(tenderDetails?.id) as AnyObject
        params[ConstantAPIKeys.vehicleId] = Helper.toInt(tenderDetails?.vehicleId) as AnyObject
        params[ConstantAPIKeys.serviceDays] = bidDetails[0].value as AnyObject
        params[ConstantAPIKeys.jobDate] = bidDetails[1].info?[Constants.UIKeys.firstValue] as AnyObject
        params[ConstantAPIKeys.jobTime] = bidDetails[2].info?[Constants.UIKeys.firstValue] as AnyObject
        params[ConstantAPIKeys.tintColor] = bidDetails[3].value as AnyObject
        params[ConstantAPIKeys.tintPercentage] = bidDetails[4].value as AnyObject
        params[ConstantAPIKeys.additionalInfo] = bidDetails[5].value as AnyObject
        params[ConstantAPIKeys.isPickupDropoff] = Helper.toInt(bidDetails[6].info?[Constants.UIKeys.firstValue]) as AnyObject
        params[ConstantAPIKeys.bidAmt] = bidDetails[7].value as AnyObject
        return params
    }
    
    func requestBidAPI(bidDetails: [CellInfo],tenderDetails: SupplierTender?, completion: @escaping (Bool) -> Void) {
        if self.validateBidData(bidDetails: bidDetails) {
            let params = self.getBidParams(bidDetails: bidDetails, tenderDetails: tenderDetails)
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .submitBid(param: params))) { (response, success) in
                if success {
                    if let safeResponse =  response as? [String: AnyObject],
                        let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                    {
                        completion(true)
                    }
                }
            }
        }
    }
}
