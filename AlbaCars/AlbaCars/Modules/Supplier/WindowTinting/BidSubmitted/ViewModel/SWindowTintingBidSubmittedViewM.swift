//
//  SWindowTintingBidSubmittedViewM.swift
//  AlbaCars
//
//  Created by Narendra on 2/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SWindowTintingBidSubmittedViewModeling {
    func getBidSubmittedDataSource(tenderDetails: SupplierTender?) -> [CellInfo]
}

class SWindowTintingBidSubmittedViewM: SWindowTintingBidSubmittedViewModeling {
    
    func getBidSubmittedDataSource(tenderDetails: SupplierTender?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Details Title
        let carTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Car Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(carTitleCell)
        
        //Car Details
        var carDetailsInfo = [String: AnyObject]()
        carDetailsInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        let carCell = CellInfo(cellType: .TenderCarDetailsCell, placeHolder:"Car Details".localizedString(), value:"" ,info:carDetailsInfo, height: Constants.CellHeightConstants.height_115)
        array.append(carCell)
        
        //Tender Details Title
        let tenderTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Tender Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(tenderTitleCell)
        
        //Tint Color
        let tintColorCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Tint Color : ".localizedString(), value:Helper.toString(object: tenderDetails?.tenderTintColor),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(tintColorCell)
        
        //Tint Visiblity
        let visiblityCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Tint Visiblity : ".localizedString(), value:Helper.toString(object: tenderDetails?.tenderTintPercentage) + "%".localizedString(),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(visiblityCell)
        
        //Additional Information
        let descriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.tenderAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(descriptionCell)
        
        //Requested Date
        let requestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedDateCell)
        
        //Requested Time
        let requestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedTimeCell)
        
        // Location
        let locationCell = CellInfo(cellType: .VITenderLocationCell, placeHolder:Helper.toString(object: tenderDetails?.pickupLocation), value:Helper.toString(object: tenderDetails?.dropoffLocation) ,info:nil, height: Constants.CellHeightConstants.height_100)
        array.append(locationCell)
                
        //Bid Details
        let bidTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Bid Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(bidTitleCell)
        
        //Days
        let daysCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Days Required for the Service :".localizedString(), value:"\(Helper.toString(object: tenderDetails?.requireDaysService)) Days" ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(daysCell)
        
        //PickUp
        let pickUpCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"I will provide PickUp and DropOff : ".localizedString(), value:Helper.toString(object: tenderDetails?.getPickupDropoff()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(pickUpCell)
        
        //Tint Color
        let bidTintColorCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Tint Color : ".localizedString(), value:Helper.toString(object: tenderDetails?.bidTintColor),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidTintColorCell)
        
        //Tint Visiblity
        let bidVisiblityCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Tint Visiblity : ".localizedString(), value:Helper.toString(object: tenderDetails?.bidTintPercentage) + "%".localizedString(),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidVisiblityCell)
        
        //Additional Information
        let bidDescriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.bidAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(bidDescriptionCell)
        
        //Requested Date
        let bidRequestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedDateCell)
        
        let bidRequestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedTimeCell)
        
        let bidPriceCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Bid Price :".localizedString(), value:Helper.toString(object: tenderDetails?.formattedPrice()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidPriceCell)
        
        return array
    }
}
