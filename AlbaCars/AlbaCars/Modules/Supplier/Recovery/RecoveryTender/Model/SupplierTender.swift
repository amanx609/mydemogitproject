//
//  ActiveTender.swift
//  AlbaCars
//
//  Created by Narendra on 2/3/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

// ActiveTender
class SupplierTender: BaseCodable {
    
    //Recovery Active Tender Variables
    var id: Int?
    var bidsCount: Int?
    var vehicleId: Int?
    var pickupLocation: String?
    var pickupLocationLatitude: Double?
    var pickupLocationLongitude: Double?
    var dropoffLocation: String?
    var duration: String?
    var requestedTime: String?
    var requestedDate: String?
    var dropLocationLongitude: Double?
    var dropoffLocationLatitude: Double?
    var status: Int?
    var description: String?
    var startDate: String?
    var endDate: String?
    var year: Int?
    var brandName: String?
    var modelName: String?
    var typeName: String?
    var currentDate: String?
    var recoveryType: String?
    
    //Recovery My Tender Variables
    var bidPrice: Int?
    
    //Recovery Active Tender Details
    var additionalInformation: String?
    var plateNumber: String?
    
    //Recovery submit Tender Details
    var tenderId: Int?
    var requireDaysService: Int?
    var bidAdditionalInformation: String?
    var jobDate: String?
    var jobTime: String?
    var tenderAdditionalInformation: String?
    var winAmount: Int?
    var mobile: String?
    var email: String?
    var name: String?
    var pickupCode: String?
    var dropoffCode: String?
    var bidRecoveryType: String?
    var tenderRecoveryType: String?
    var isPickupDropoff: Int?
    var jobDescription: String?
    var tenderJobDescription: String?
    var bidJobDescription: String?
    var paymentStatus: Int?
    var image: String?
    var engine: Double?
    var cylinders: String?
    
    //Timer
    var timerDuration: Double?
    
    //Service
    var typeOfService: String?
    var serviceOptions: String?
    var tenderTypeOfService: String?
    var bidTypeOfService: String?
    var partType: Int?
    var partName: String?
    var partNumber: String?
    var partDescription: String?
    var deliveryLocation: String?
    var wheelType: Int?
    var rimServiceType: Int?
    var wheelSize: String?
    var bodyServiceType: Int?
    var paintColor: String?
    var paintSiteBody: Int?
    var paintSiteInterior: Int?
    var paintSiteGlass: Int?
    var paintSiteRims: Int?
    var tenderPartDescription: String?
    var tenderPartName: String?
    var tenderPartNumber: String?
    var tenderPartType: Int?
    var bidPartName: String?
    var bidPartType: Int?
    var bidRimService: Int?
    var bidWheelServiceType: Int?
    var bidWheelSize: String?
    var tenderDeliveryLocation: String?
    var bidDeliveryProvided: Int?
    var wheelServiceType: Int?
    var tenderRimServiceType: Int?
    var tenderServiceType: Int?
    var tenderWheelSize: String?
    var tintColor: String?
    var tintPercentage: Int?
    var tenderTintColor: String?
    var tenderTintPercentage: Int?
    var bidTintColor: String?
    var bidTintPercentage: Int?
    var noOfDents: Int?
    var paintSiteArr: [Int]?
    var tenderPaintColor: String?
    var tenderPaintSiteArr: [Int]?
    var tendernoOfDents: Int?
    var tenderBodyServiceType: Int?
    var carDetailingServiceType: Int?
    var carDetailingOptions: Int?
    var bankName: String?
    var carPrice: Int?
    var typeOfInsurance: Int?
    var vehiclePrice: Int?
    var carMileage: Int?
    var bidCarDetailingServiceType: Int?
    var tenderCarDetailingServiceType: Int?
    var tenderCarDetailingOptions: Int?
    var bidCarDetailingOptions: Int?
    var bidTypeOfInsurance: Int?
    var tenderTypeOfInsurance: Int?
    var bidCarPrice: Int?
    var tenderVehiclePrice: Int?
    var tenderBankName: String?
    var tenderBankId: String?
    var bidBankName: String?
    var bidBankId: String?
    var tenderRequestedCarValue: Int?
    var bankId: String?
    //Upholstrey Tender Details
    var images: [String]?
    var bidImages: [String]?
    var dentDescription: [String]?
    var bidDriverPassengerCover: Int?
    var bidRoadsideAssistance: String?
    var bidPartWarranty: String?

    required init(from decoder: Decoder) throws {
        let values = try! decoder.container(keyedBy: CodingKeys.self)
        self.id = try values.decodeIfPresent(Int.self, forKey: .id)
        self.bidsCount = try values.decodeIfPresent(Int.self, forKey: .bidsCount)
        self.vehicleId = try values.decodeIfPresent(Int.self, forKey: .vehicleId)
        self.pickupLocation = try values.decodeIfPresent(String.self, forKey: .pickupLocation)
        self.pickupLocationLatitude = try values.decodeIfPresent(Double.self, forKey: .pickupLocationLatitude)
        self.pickupLocationLongitude = try values.decodeIfPresent(Double.self, forKey: .pickupLocationLongitude)
        self.dropoffLocation = try values.decodeIfPresent(String.self, forKey: .dropoffLocation)
        self.duration = try values.decodeIfPresent(String.self, forKey: .duration)
        self.requestedTime = try values.decodeIfPresent(String.self, forKey: .requestedTime)
        self.requestedDate = try values.decodeIfPresent(String.self, forKey: .requestedDate)
        self.dropLocationLongitude = try values.decodeIfPresent(Double.self, forKey: .dropLocationLongitude)
        self.dropoffLocationLatitude = try values.decodeIfPresent(Double.self, forKey: .dropoffLocationLatitude)
        self.status = try values.decodeIfPresent(Int.self, forKey: .status)
        self.description = try values.decodeIfPresent(String.self, forKey: .description)
        self.startDate = try values.decodeIfPresent(String.self, forKey: .startDate)
        self.endDate = try values.decodeIfPresent(String.self, forKey: .endDate)
        self.year = try values.decodeIfPresent(Int.self, forKey: .year)
        self.brandName = try values.decodeIfPresent(String.self, forKey: .brandName)
        self.modelName = try values.decodeIfPresent(String.self, forKey: .modelName)
        self.typeName = try values.decodeIfPresent(String.self, forKey: .typeName)
        self.currentDate = try values.decodeIfPresent(String.self, forKey: .currentDate)
        self.recoveryType = try values.decodeIfPresent(String.self, forKey: .recoveryType)
        
        //Recovery My Tender Variables
        self.bidPrice = try values.decodeIfPresent(Int.self, forKey: .bidPrice)
        
        //Recovery Active Tender Details
        self.additionalInformation = try values.decodeIfPresent(String.self, forKey: .additionalInformation)
        self.plateNumber = try values.decodeIfPresent(String.self, forKey: .plateNumber)
        
        //Recovery submit Tender Details
        self.tenderId = try values.decodeIfPresent(Int.self, forKey: .tenderId)
        self.requireDaysService = try values.decodeIfPresent(Int.self, forKey: .requireDaysService)
        self.bidAdditionalInformation = try values.decodeIfPresent(String.self, forKey: .bidAdditionalInformation)
        self.jobDate = try values.decodeIfPresent(String.self, forKey: .jobDate)
        self.jobTime = try values.decodeIfPresent(String.self, forKey: .jobTime)
        self.tenderAdditionalInformation = try values.decodeIfPresent(String.self, forKey: .tenderAdditionalInformation)
        self.winAmount = try values.decodeIfPresent(Int.self, forKey: .winAmount)
        self.mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
        self.email = try values.decodeIfPresent(String.self, forKey: .email)
        self.name = try values.decodeIfPresent(String.self, forKey: .name)
        self.pickupCode = try values.decodeIfPresent(String.self, forKey: .pickupCode)
        self.dropoffCode = try values.decodeIfPresent(String.self, forKey: .dropoffCode)
        self.bidRecoveryType = try values.decodeIfPresent(String.self, forKey: .bidRecoveryType)
        self.tenderRecoveryType = try values.decodeIfPresent(String.self, forKey: .tenderRecoveryType)
        self.isPickupDropoff = try values.decodeIfPresent(Int.self, forKey: .isPickupDropoff)
        self.jobDescription = try values.decodeIfPresent(String.self, forKey: .jobDescription)
        self.tenderJobDescription = try values.decodeIfPresent(String.self, forKey: .tenderJobDescription)
        self.bidJobDescription = try values.decodeIfPresent(String.self, forKey: .bidJobDescription)
        self.paymentStatus = try values.decodeIfPresent(Int.self, forKey: .paymentStatus)
        self.image = try values.decodeIfPresent(String.self, forKey: .image)
        
        //Service
        self.typeOfService = try values.decodeIfPresent(String.self, forKey: .typeOfService)
        self.serviceOptions = try values.decodeIfPresent(String.self, forKey: .serviceOptions)
        self.engine = try values.decodeIfPresent(Double.self, forKey: .engine)
        self.cylinders = try values.decodeIfPresent(String.self, forKey: .cylinders)
        self.tenderTypeOfService = try values.decodeIfPresent(String.self, forKey: .tenderTypeOfService)
        self.partType = try values.decodeIfPresent(Int.self, forKey: .partType)
        self.partName = try values.decodeIfPresent(String.self, forKey: .partName)
        self.partNumber = try values.decodeIfPresent(String.self, forKey: .partNumber)
        self.partDescription = try values.decodeIfPresent(String.self, forKey: .partDescription)
        self.deliveryLocation = try values.decodeIfPresent(String.self, forKey: .deliveryLocation)
        
        self.wheelType = try values.decodeIfPresent(Int.self, forKey: .wheelType)
        self.rimServiceType = try values.decodeIfPresent(Int.self, forKey: .rimServiceType)
        self.wheelSize = try values.decodeIfPresent(String.self, forKey: .wheelSize)
        self.bodyServiceType = try values.decodeIfPresent(Int.self, forKey: .bodyServiceType)
        self.paintSiteBody = try values.decodeIfPresent(Int.self, forKey: .paintSiteBody)
        self.paintColor = try values.decodeIfPresent(String.self, forKey: .paintColor)
        self.paintSiteInterior = try values.decodeIfPresent(Int.self, forKey: .paintSiteInterior)
        self.paintSiteGlass = try values.decodeIfPresent(Int.self, forKey: .paintSiteGlass)
        self.paintSiteRims = try values.decodeIfPresent(Int.self, forKey: .paintSiteRims)
        self.tenderPartDescription = try values.decodeIfPresent(String.self, forKey: .tenderPartDescription)
        self.tenderPartNumber = try values.decodeIfPresent(String.self, forKey: .tenderPartNumber)
        self.tenderPartName = try values.decodeIfPresent(String.self, forKey: .tenderPartName)
        self.tenderPartType = try values.decodeIfPresent(Int.self, forKey: .tenderPartType)
        self.bidPartType = try values.decodeIfPresent(Int.self, forKey: .bidPartType)
        self.bidPartName = try values.decodeIfPresent(String.self, forKey: .bidPartName)
        self.bidRimService = try values.decodeIfPresent(Int.self, forKey: .bidRimService)
        self.bidTypeOfService = try values.decodeIfPresent(String.self, forKey: .bidTypeOfService)
        self.bidWheelServiceType = try values.decodeIfPresent(Int.self, forKey: .bidWheelServiceType)
        self.bidWheelSize = try values.decodeIfPresent(String.self, forKey: .bidWheelSize)
        self.tenderDeliveryLocation = try values.decodeIfPresent(String.self, forKey: .tenderDeliveryLocation)
        self.bidDeliveryProvided = try values.decodeIfPresent(Int.self, forKey: .bidDeliveryProvided)
        self.images = try values.decodeIfPresent([String].self, forKey: .images)
        self.bidImages = try values.decodeIfPresent([String].self, forKey: .bidImages)
        self.wheelServiceType = try values.decodeIfPresent(Int.self, forKey: .wheelServiceType)
        self.tenderRimServiceType = try values.decodeIfPresent(Int.self, forKey: .tenderRimServiceType)
        self.tenderServiceType = try values.decodeIfPresent(Int.self, forKey: .tenderServiceType)
        self.tenderWheelSize = try values.decodeIfPresent(String.self, forKey: .tenderWheelSize)
        self.tintPercentage = try values.decodeIfPresent(Int.self, forKey: .tintPercentage)
        self.tintColor = try values.decodeIfPresent(String.self, forKey: .tintColor)
        self.tenderTintPercentage = try values.decodeIfPresent(Int.self, forKey: .tenderTintPercentage)
        self.tenderTintColor = try values.decodeIfPresent(String.self, forKey: .tenderTintColor)
        self.bidTintPercentage = try values.decodeIfPresent(Int.self, forKey: .bidTintPercentage)
        self.bidTintColor = try values.decodeIfPresent(String.self, forKey: .bidTintColor)
        self.noOfDents = try values.decodeIfPresent(Int.self, forKey: .noOfDents)
        self.paintSiteArr = try values.decodeIfPresent([Int].self, forKey: .paintSiteArr)
        self.tendernoOfDents = try values.decodeIfPresent(Int.self, forKey: .tendernoOfDents)
        self.tenderPaintColor = try values.decodeIfPresent(String.self, forKey: .tenderPaintColor)
        self.tenderPaintSiteArr = try values.decodeIfPresent([Int].self, forKey: .tenderPaintSiteArr)
        self.tenderBodyServiceType = try values.decodeIfPresent(Int.self, forKey: .tenderBodyServiceType)
        self.carDetailingServiceType = try values.decodeIfPresent(Int.self, forKey: .carDetailingServiceType)
        self.carDetailingOptions = try values.decodeIfPresent(Int.self, forKey: .carDetailingOptions)
        self.carPrice = try values.decodeIfPresent(Int.self, forKey: .carPrice)
        self.bankName = try values.decodeIfPresent(String.self, forKey: .bankName)
        self.typeOfInsurance = try values.decodeIfPresent(Int.self, forKey: .typeOfInsurance)
        self.vehiclePrice = try values.decodeIfPresent(Int.self, forKey: .vehiclePrice)
        self.carMileage = try values.decodeIfPresent(Int.self, forKey: .carMileage)
        self.bidCarDetailingServiceType = try values.decodeIfPresent(Int.self, forKey: .bidCarDetailingServiceType)
        self.tenderCarDetailingServiceType = try values.decodeIfPresent(Int.self, forKey: .tenderCarDetailingServiceType)
        self.tenderCarDetailingOptions = try values.decodeIfPresent(Int.self, forKey: .tenderCarDetailingOptions)
        self.bidCarDetailingOptions = try values.decodeIfPresent(Int.self, forKey: .bidCarDetailingOptions)
        self.bidTypeOfInsurance = try values.decodeIfPresent(Int.self, forKey: .bidTypeOfInsurance)
        self.tenderTypeOfInsurance = try values.decodeIfPresent(Int.self, forKey: .tenderTypeOfInsurance)
        self.bidCarPrice = try values.decodeIfPresent(Int.self, forKey: .bidCarPrice)
        self.tenderVehiclePrice = try values.decodeIfPresent(Int.self, forKey: .tenderVehiclePrice)
        self.tenderBankId = try values.decodeIfPresent(String.self, forKey: .tenderBankId)
        self.bidBankId = try values.decodeIfPresent(String.self, forKey: .bidBankId)
        self.tenderBankName = try values.decodeIfPresent(String.self, forKey: .tenderBankName)
        self.bidBankName = try values.decodeIfPresent(String.self, forKey: .bidBankName)
        self.tenderRequestedCarValue = try values.decodeIfPresent(Int.self, forKey: .tenderRequestedCarValue)
        self.bankId = try values.decodeIfPresent(String.self, forKey: .bankId)
        self.dentDescription = try values.decodeIfPresent([String].self, forKey: .dentDescription)
        self.bidDriverPassengerCover = try values.decodeIfPresent(Int.self, forKey: .bidDriverPassengerCover)
        self.bidRoadsideAssistance = try values.decodeIfPresent(String.self, forKey: .bidRoadsideAssistance)
        self.bidPartWarranty = try values.decodeIfPresent(String.self, forKey: .bidPartWarranty)

        if let strCurrentDate = self.currentDate,
            let strEndDate = self.endDate,
            let currentDate = strCurrentDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss"),
            let endDate = strEndDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss") {
            self.timerDuration = endDate.timeIntervalSince(currentDate)
        }
    }
        
    func carTitle() -> String {
        return Helper.toString(object: brandName) + " " + Helper.toString(object: modelName) + " " + Helper.toString(object: year)
    }
    
    func formattedPrice() -> String {
        return "AED".localizedString() + " " + Helper.toString(object: bidPrice)
    }
    
    func winningAmount() -> String {
        return "AED".localizedString() + " " + Helper.toString(object: winAmount)
    }
    
    func formattedCarPrice() -> String {
        return "AED".localizedString() + " " + Helper.toString(object: carPrice)
    }
    
    func formattedVehiclePrice() -> String {
        return "AED".localizedString() + " " + Helper.toString(object: vehiclePrice)
    }
    
    func formattedTenderRequestedCarValue() -> String {
        return "AED".localizedString() + " " + Helper.toString(object: tenderRequestedCarValue)
    }
    
    func formattedTenderVehiclePrice() -> String {
        return "AED".localizedString() + " " + Helper.toString(object: tenderVehiclePrice)
    }
    
    func formattedBidCarPrice() -> String {
        return "AED".localizedString() + " " + Helper.toString(object: bidCarPrice)
    }
    
    func formattedCarMileage() -> String {
        return Helper.toString(object: carMileage) + " " + "Km".localizedString()
    }
    
    func getRequestedDate() -> String {
        return  Helper.toString(object: requestedDate?.getDateStringFrom(inputFormat: Constants.Format.dateFormatyyyyMMdd, outputFormat: Constants.Format.dateFormatdMMMyyyy))
    }
        
    func getRequestedddMMYYYDate() -> String {
        return  Helper.toString(object: requestedDate?.getDateStringFrom(inputFormat: Constants.Format.dateFormatyyyyMMdd, outputFormat: Constants.Format.dateFormatWithoutSpace))
    }
    
    func getRequestedTime() -> String {
        var time = Helper.toString(object: requestedTime?.getDateStringFrom(inputFormat: Constants.Format.apiTimeFormat, outputFormat: Constants.Format.timeFormathmma))
        if time.isEmpty {
            time = Helper.toString(object: requestedTime?.getDateStringFrom(inputFormat: Constants.Format.timeFormat24hhmmss, outputFormat: Constants.Format.timeFormathmma))
        }
        return  time
    }
    
    func getJobDate() -> String {
        return  Helper.toString(object: jobDate?.getDateStringFrom(inputFormat: Constants.Format.dateFormatyyyyMMdd, outputFormat: Constants.Format.dateFormatdMMMyyyy))
    }
    
    func getJobDateMMYYYDate() -> String {
        return  Helper.toString(object: jobDate?.getDateStringFrom(inputFormat: Constants.Format.dateFormatyyyyMMdd, outputFormat: Constants.Format.dateFormatWithoutSpace))
    }
    
    func getJobTime() -> String {
        var time = Helper.toString(object: jobTime?.getDateStringFrom(inputFormat: Constants.Format.apiTimeFormat, outputFormat: Constants.Format.timeFormathmma))
        if time.isEmpty {
            time = Helper.toString(object: jobTime?.getDateStringFrom(inputFormat: Constants.Format.timeFormat24hhmmss, outputFormat: Constants.Format.timeFormathmma))
        }
        return time
    }
    
    func getPickupDropoff() -> String {
        return Helper.toInt(isPickupDropoff) == 0 ? "No".localizedString() : "Yes".localizedString()
    }
    
    func getBidDeliveryProvided() -> String {
        return Helper.toInt(bidDeliveryProvided) == 0 ? "No".localizedString() : "Yes".localizedString()
    }
    
    func getServiceOptions() -> String {
        
        var allService = ""
        
        if let serviceOptions = self.serviceOptions {
            
            let array = serviceOptions.components(separatedBy: ",")
            for serviceOption in  array {
                var service = ""
                switch serviceOption {
                case "1":
                    service = "Front Brake Pads".localizedString()
                case "2":
                    service = "Rear Brake Pads".localizedString()
                case "3":
                    service = "Brake Discs".localizedString()
                default:
                    print("")
                }
                
                if allService.isEmpty {
                    allService = service
                } else{
                    allService = allService + ", " + service
                }
            }
        }
        return allService
    }
    
    func getRoadsideAssistance() -> String {
           
           let pointsArr = Helper.toString(object: self.bidRoadsideAssistance).components(separatedBy: ",")
           var allRoadsideAssistance = ""
           
           if pointsArr.count > 0 {
               
               for status in pointsArr {
                   var roadsideAssistance = ""
                   switch Int(status){
                   case RoadsideAssistanceType.oman.rawValue:
                       roadsideAssistance = RoadsideAssistanceType.oman.title
                   case RoadsideAssistanceType.offRoad.rawValue:
                       roadsideAssistance = RoadsideAssistanceType.offRoad.title
                   case RoadsideAssistanceType.carHire.rawValue:
                       roadsideAssistance = RoadsideAssistanceType.carHire.title
                   default:
                       print("")
                   }
                   
                   if allRoadsideAssistance.isEmpty {
                       allRoadsideAssistance = roadsideAssistance
                   } else{
                       allRoadsideAssistance = allRoadsideAssistance + ", " + roadsideAssistance
                   }
               }
               
               return allRoadsideAssistance
               
           } else {
               return "N/A"
           }
           
       }
    
    func getServiceStatus(value: String) -> Bool {
        if let serviceOptions = self.serviceOptions, serviceOptions.contains(value) {
            return true
        }
        return false
    }
    
    func getPartWarranty() -> String {
        if let warranty = self.bidPartWarranty, !warranty.isEmpty {
            return warranty
        }
        return "N/A"
    }
}

class SupplierTenderList: BaseCodable {
    var supplierTender: [SupplierTender]?
    
    private enum CodingKeys: String, CodingKey {
        case supplierTender = "data"
    }
}
