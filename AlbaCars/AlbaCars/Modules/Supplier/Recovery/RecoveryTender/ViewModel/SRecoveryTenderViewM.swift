//
//  SRecoveryTenderViewM.swift
//  AlbaCars
//
//  Created by Narendra on 1/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SRecoveryTenderViewModeling {
    func requestActiveTenderListAPI(params: APIParams, completion: @escaping (Int, Int, [SupplierTender]) -> Void)
    func requestMyTenderListAPI(params: APIParams, completion: @escaping (Int, Int, [SupplierTender]) -> Void)
    func requestActiveTenderDetailsAPI(params: APIParams, completion: @escaping (SupplierTender) -> Void)
    func requestMyTenderDetailsAPI(params: APIParams, completion: @escaping (SupplierTender) -> Void)
}

class SRecoveryTenderViewM: SRecoveryTenderViewModeling {
    
    func requestActiveTenderListAPI(params: APIParams, completion: @escaping (Int, Int, [SupplierTender]) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .activeTenderListServiceProvider(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let resultData = result.toJSONData(),
                    let tenderList = SupplierTenderList(jsonData: resultData) {
                    completion(nextPageNumber, perPage,tenderList.supplierTender ?? [])
                }
            }
        }
    }
    
    func requestMyTenderListAPI(params: APIParams, completion: @escaping (Int, Int, [SupplierTender]) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .myTenderListServiceProvider(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let resultData = result.toJSONData(),
                    let tenderList = SupplierTenderList(jsonData: resultData) {
                    completion(nextPageNumber, perPage,tenderList.supplierTender ?? [])
                }
            }
        }
    }
    
    func requestActiveTenderDetailsAPI(params: APIParams, completion: @escaping (SupplierTender) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .activeTenderListServiceProviderDetails(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData(),
                    let supplierTender = SupplierTender(jsonData: resultData) {
                    completion(supplierTender)
                }
            }
        }
    }
    
    func requestMyTenderDetailsAPI(params: APIParams, completion: @escaping (SupplierTender) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .myTenderDetailServiceProvider(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData(),
                    let supplierTender = SupplierTender(jsonData: resultData) {
                    completion(supplierTender)
                }
            }
        }
    }
    
   
    
}
