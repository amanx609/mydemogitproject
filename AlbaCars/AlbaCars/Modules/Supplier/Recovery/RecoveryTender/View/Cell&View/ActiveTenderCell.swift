//
//  ActiveTenderCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol ActiveTenderCellDelegate: class {
    func didTapViewDetailsCell(cell: ActiveTenderCell)
}

class ActiveTenderCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var viewDetailsButton: UIButton!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var location1Label: UILabel!
    @IBOutlet weak var location2Label: UILabel!
    @IBOutlet weak var recoveryTypeLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var liveLabel: UILabel!
    
    //MARK: - Variables
    weak var delegate: ActiveTenderCellDelegate?
    var durationTimer : Timer?
    var timeDifference: TimeInterval = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.viewDetailsButton.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.bgView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
    }
    
    //MARK: - Public Methods
    func configureView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        self.location1Label.text = Helper.toString(object: supplierTender.pickupLocation)
        self.location2Label.text = Helper.toString(object: supplierTender.dropoffLocation)
        self.timeLabel.text = Helper.toString(object: supplierTender.duration)
        self.recoveryTypeLabel.text = "Recovery Type :".localizedString() + " \(Helper.toString(object: supplierTender.recoveryType))"
        //Timer
        if let timerDuration = supplierTender.timerDuration {
            self.timeLabel.text = timerDuration.convertToTimerFormat()
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func viewDetailsButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapViewDetailsCell(cell: self)
        }
    }
}
