//
//  SRecoveryTenderViewC.swift
//  AlbaCars
//
//  Created by Narendra on 1/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SRecoveryTenderViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var activeTenderButton: UIButton!
    @IBOutlet weak var myTenderButton: UIButton!
    @IBOutlet weak var bottomMarkerView: UIView!
    @IBOutlet weak var tenderTableView: UITableView!
    @IBOutlet weak var activeTenderHeaderView: UIView!
    @IBOutlet weak var myTenderHeaderView: UIView!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var filterLabel: UILabel!
    
    //ConstraintOutlets
    @IBOutlet weak var bottomMarkerViewLeadingConstraint: NSLayoutConstraint!
    
    //MARK: - Variables
    var viewModel: SRecoveryTenderViewModeling?
    var tender: [SupplierTender] = []
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    var bidStatus = "0"
    
    var durationTimer : Timer?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    deinit {
        self.invalidateTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.activeTenderButton.isSelected {
            self.tenderTableView.tableHeaderView =  self.activeTenderHeaderView
            self.loadActiveTenderDataSource()
        }
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Recovery".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setupView()
        self.setUpTableView()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = SRecoveryTenderViewM()
        }
    }
    
    private func setupView() {
        //self.bottomMarkerViewWidthConstraint.constant = Constants.Devices.ScreenWidth/2
        self.activeTenderButton.setTitle("Active Tenders".localizedString(), for: .normal)
        self.myTenderButton.setTitle("My Tenders".localizedString(), for: .normal)
       // self.didTapButton(self.activeTenderButton, isCalledOnViewDidLoad: true)
        self.activeTenderButton.isSelected = true
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.tenderTableView.delegate = self
        self.tenderTableView.dataSource = self
        self.tenderTableView.separatorStyle = .none
        self.dropDownView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.dropDownView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
    }
    
    private func registerNibs() {
        self.tenderTableView.register(ActiveTenderCell.self)
        self.tenderTableView.register(MyTenderCell.self)
        // self.leadsTableView.register(BuyLeadCell.self)
    }
    
    private func didTapButton(_ sender: UIButton, isCalledOnViewDidLoad: Bool = false) {
        var leadingConstraint: CGFloat = 0.0
        switch sender {
        case self.activeTenderButton:
            //ChangeBottomMarkerConstraint
            leadingConstraint = 0.0
            //ChangeButtonTitleColor
            self.activeTenderButton.setTitleColor(.white, for: .normal)
            self.myTenderButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            self.activeTenderButton.isSelected = true
            self.myTenderButton.isSelected = false
            self.tenderTableView.tableHeaderView =  self.activeTenderHeaderView
            self.loadActiveTenderDataSource()
        case self.myTenderButton:
            //ChangeBottomMarkerConstraint
            leadingConstraint = Constants.Devices.ScreenWidth/2
            //ChangeButtonTitleColor
            self.activeTenderButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            self.myTenderButton.setTitleColor(.white, for: .normal)
            self.activeTenderButton.isSelected = false
            self.myTenderButton.isSelected = true
            self.tenderTableView.tableHeaderView =  self.myTenderHeaderView
            self.loadMyTenderDataSource()
        default: break
        }
        
        if isCalledOnViewDidLoad {
            self.bottomMarkerViewLeadingConstraint.constant = leadingConstraint
            self.view.layoutIfNeeded()
        } else {
            //AnimateTheBottomViewMarker
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseOut], animations: {
                self.bottomMarkerViewLeadingConstraint.constant = leadingConstraint
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
        
        Threads.performTaskInMainQueue {
            self.tenderTableView.reloadData()
        }
        
    }
    
    private func loadActiveTenderDataSource() {
        
        Threads.performTaskInMainQueue {
            self.tender.removeAll()
            self.nextPageNumber = 1
            self.tenderTableView.reloadData()
            self.requestActiveTenderListAPI()
        }
    }
    
    private func loadMyTenderDataSource() {
        Threads.performTaskInMainQueue {
            self.nextPageNumber = 1
            self.tender.removeAll()
            self.tenderTableView.reloadData()
            self.requestMyTenderListAPI()
        }
    }
    
    private func setupTimer() {
        self.invalidateTimer()
        self.durationTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimeValueInDataSource), userInfo: nil, repeats: true)
    }
    
    private func invalidateTimer() {
        self.durationTimer?.invalidate()
        self.durationTimer = nil
    }
    
    //MARK: - Selector Methods
    @objc func updateTimeValueInDataSource() {
        var index = 0
        for tenderData in self.tender {
            if let timerDuration = tenderData.timerDuration {
                tenderData.timerDuration = timerDuration - 1
                //RemoveDataIfDurationIsFinished
                if timerDuration == 0 {
                    self.tender.remove(at: index)
                    continue
                }
                self.tender[index] = tenderData
                index += 1
            }
        }
        self.tenderTableView.reloadData()
    }
    
    //MARK: - Public Methods
    func selectedBidStatus() {
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: "Bid Status".localizedString(), arrayList: TenderBidStatus.submitted.dataList, key: ConstantAPIKeys.name) { [weak self] (response) in
            guard let sSelf = self,
                // let brandId = response[ConstantAPIKeys.id] as? Int,
                let leadsStatus = response[ConstantAPIKeys.name] as? String else { return }
            sSelf.filterLabel.text = leadsStatus
            sSelf.bidStatus = Helper.toString(object: response[ConstantAPIKeys.id])
            sSelf.loadMyTenderDataSource()
        }
        
        listPopupView.showWithAnimated(animated: true)
    }
    
    func goToTenderDetailsScreen(statusCode: Int,tenderDetails:SupplierTender) {
        
        switch statusCode {
        case TenderBidStatus.accepted.rawValue:
            let tenderWonVC = DIConfigurator.sharedInstance.getSTenderWonVC()
            tenderWonVC.tenderDetails = tenderDetails
            self.navigationController?.pushViewController(tenderWonVC, animated: true)
        case TenderBidStatus.submitted.rawValue:
            let submittedVC = DIConfigurator.sharedInstance.getSBidSubmittedVC()
            submittedVC.tenderDetails = tenderDetails
            self.navigationController?.pushViewController(submittedVC, animated: true)
        case TenderBidStatus.rejected.rawValue:
            let rejectedVC = DIConfigurator.sharedInstance.getSBidsRejectedVC()
            rejectedVC.tenderDetails = tenderDetails
            self.navigationController?.pushViewController(rejectedVC, animated: true)
        case TenderBidStatus.completed.rawValue:
            let completedVC = DIConfigurator.sharedInstance.getSJobCompletedVC()
            completedVC.tenderDetails = tenderDetails
            self.navigationController?.pushViewController(completedVC, animated: true)
        default:
            print("")
        }
    }
    
    
    //MARK: - API Methods
    
    func requestActiveTenderListAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        params[ConstantAPIKeys.type] = ServiceTenderType.recovery.rawValue as AnyObject
        self.viewModel?.requestActiveTenderListAPI(params: params, completion: { [weak self] (nextPageNumber, perPage, tenderList) in
            guard let sSelf = self else { return }
            sSelf.tender = sSelf.nextPageNumber == 1 ? tenderList : sSelf.tender + tenderList
            if sSelf.nextPageNumber == 1, sSelf.tender.count > 0 {
                sSelf.setupTimer()
            }
            sSelf.nextPageNumber = nextPageNumber
            sSelf.perPage = perPage
            Threads.performTaskInMainQueue {
                sSelf.tenderTableView.reloadData()
            }
            print(tenderList)
        })
    }
    
    func requestMyTenderListAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        params[ConstantAPIKeys.type] = ServiceTenderType.recovery.rawValue as AnyObject
        
        if bidStatus != "0"   {
            params[ConstantAPIKeys.status] = Helper.toInt(bidStatus) as AnyObject
        }
        
        self.viewModel?.requestMyTenderListAPI(params: params, completion: { [weak self] (nextPageNumber, perPage, tenderList) in
            guard let sSelf = self else { return }
            sSelf.nextPageNumber = nextPageNumber
            sSelf.perPage = perPage
            sSelf.tender = sSelf.tender + tenderList
            Threads.performTaskInMainQueue {
                sSelf.tenderTableView.reloadData()
            }
            print(tenderList)
        })
    }
    
    func requestActiveTenderDetailsAPI(tenderId: Int) {
        var params = APIParams()
        params[ConstantAPIKeys.tenderId] = tenderId as AnyObject
        
        self.viewModel?.requestActiveTenderDetailsAPI(params: params, completion: { [weak self] (tender) in
            guard let sSelf = self else { return }
            Threads.performTaskInMainQueue {
                let recoveryTenderDetailsVC = DIConfigurator.sharedInstance.getSRecoveryTenderDetailsVC()
                recoveryTenderDetailsVC.tenderDetails = tender
                sSelf.navigationController?.pushViewController(recoveryTenderDetailsVC, animated: true)
            }
        })
    }
    
    func requestMyTenderDetailsAPI(tenderId: Int) {
        var params = APIParams()
        params[ConstantAPIKeys.tenderId] = tenderId as AnyObject
        
        self.viewModel?.requestMyTenderDetailsAPI(params: params, completion: { [weak self] (tender) in
            guard let sSelf = self else { return }
            Threads.performTaskInMainQueue {
                sSelf.goToTenderDetailsScreen(statusCode: Helper.toInt(tender.status), tenderDetails: tender)
            }
        })
    }
    
    //MARK: - IBActions
    @IBAction func tapActiveTenderButton(_ sender: UIButton) {
        self.didTapButton(sender)
    }
    
    @IBAction func tapMyTenderButton(_ sender: UIButton) {
        self.didTapButton(sender)
    }
    
    @IBAction func tapDropDownButton(_ sender: UIButton) {
        self.selectedBidStatus()
    }
}
