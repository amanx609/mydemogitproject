//
//  SRecoveryTenderViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 1/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension SRecoveryTenderViewC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tender.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getCell(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      guard let lastVisibleRow = self.tenderTableView.indexPathsForVisibleRows?.last?.row else { return }
      if (self.tender.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
        
        if self.activeTenderButton.isSelected {
            self.requestActiveTenderListAPI()
        } else {
            self.requestMyTenderListAPI()
        }
      }
    }
}

extension SRecoveryTenderViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {

        let tender = self.tender[indexPath.row]
        if self.activeTenderButton.isSelected == true {
            let cell: ActiveTenderCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(supplierTender: tender)
            return cell
        }
        let cell: MyTenderCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.configureView(supplierTender: tender)
        return cell
    }
}

//ActiveTenderCellDelegate
extension SRecoveryTenderViewC: ActiveTenderCellDelegate {
    
    func didTapViewDetailsCell(cell: ActiveTenderCell) {
        guard let indexPath = self.tenderTableView.indexPath(for: cell) else { return }
        let tender = self.tender[indexPath.row]
        self.requestActiveTenderDetailsAPI(tenderId: Helper.toInt(tender.id))
    }
}

//ActiveTenderCellDelegate
extension SRecoveryTenderViewC: MyTenderCellDelegate {
    func didTapViewDetailsCell(cell: MyTenderCell) {
        guard let indexPath = self.tenderTableView.indexPath(for: cell) else { return }
        let tender = self.tender[indexPath.row]
        self.requestMyTenderDetailsAPI(tenderId: Helper.toInt(tender.id))
    }
}

 

