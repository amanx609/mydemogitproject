//
//  JobCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol JobCellDelegate: class {
    func didTapDate(cell: JobCell)
    func didTapSameAsTenderDate(cell: JobCell,isSameAsTender: Bool)
}

class JobCell: BaseTableViewCell, ReusableView, NibLoadableView  {

    //MARK: - IBOutlets
    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var checkedButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - Variables
    weak var delegate: JobCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.dateTextField.delegate = self
        self.textFieldView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.textFieldView.makeLayer(color: .borderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.titleLabel.text = cellInfo.placeHolder
        self.dateTextField.text = cellInfo.value
        
        if let isSameAsTender = cellInfo.info?[Constants.UIKeys.isSameAsTender] as? Bool, isSameAsTender == true {
            self.dateTextField.isUserInteractionEnabled = false
        } else {
            self.dateTextField.isUserInteractionEnabled = true
        }
        
    }
    
    //MARK: - IBActions
    @IBAction func sameAsTenderButtonTapped(_ sender: Any) {
        self.checkedButton.isSelected = !self.checkedButton.isSelected
        if let delegate = self.delegate {
            delegate.didTapSameAsTenderDate(cell: self, isSameAsTender: self.checkedButton.isSelected)
        }
    }
}

extension JobCell: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.dateTextField {
            if let delegate = self.delegate {
                delegate.didTapDate(cell: self)
            }
        }
    }
    
}
