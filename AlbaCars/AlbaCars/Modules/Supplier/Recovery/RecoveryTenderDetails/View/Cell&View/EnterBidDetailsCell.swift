//
//  EnterBidDetailsCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol EnterBidDetailsCellDelegate: class {
    func didTapBidDetailsCell(cell: EnterBidDetailsCell, cellInfo:[CellInfo], shouldReload:Bool)
    func didTapHideKeyboard()
}

class EnterBidDetailsCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bidDetailsTableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet var dateTimePicker: UIDatePicker!
    
    //MARK: - Variables
    weak var delegate: EnterBidDetailsCellDelegate?
    var enterBidDataSource = [CellInfo]()
    var isDate = true
    var selectBankName:((EnterBidDetailsCell,Int)-> Void)?
    var tenderDetails: SupplierTender?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.setUpTableView()
        dateTimePicker = UIDatePicker()
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.bgView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.bidDetailsTableView.delegate = self
        self.bidDetailsTableView.dataSource = self
        self.bidDetailsTableView.separatorStyle = .none
        self.bidDetailsTableView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.bidDetailsTableView.register(DaysRequiredCell.self)
        self.bidDetailsTableView.register(JobCell.self)
        self.bidDetailsTableView.register(RecoveryTypeCell.self)
        self.bidDetailsTableView.register(BidAmountCell.self)
        self.bidDetailsTableView.register(EnterAdditionalInfoCell.self)
        self.bidDetailsTableView.register(TenderCompletedCell.self)
        self.bidDetailsTableView.register(UploadVehicleImagesCell.self)
    }
    
    //MARK: - Public Methods
    func setupDateTimePicker(_ textField: UITextField) {
        textField.inputView = self.dateTimePicker
        textField.inputAccessoryView = Helper.getDoubleButtonToolBar(leftButtonTitle: StringConstants.Text.Cancel.localizedString(), rightButtonTitle: StringConstants.Text.done.localizedString(), leftButtonselector: #selector(self.cancelButtonTapped), rightButtonSelector: #selector(self.doneButtonTapped), target: self)
        if isDate {
            self.dateTimePicker.datePickerMode = .date
        } else {
            self.dateTimePicker.datePickerMode = .time
        }
        self.dateTimePicker.minimumDate = Date()
    }
    
    //MARK: - Selectors
    @objc func cancelButtonTapped() {
        
        if let delegate = self.delegate {
            delegate.didTapHideKeyboard()
        }
    }
    
    @objc func doneButtonTapped() {
        self.cancelButtonTapped()
        let selectedDate = self.dateTimePicker.date
        if isDate {
            self.enterBidDataSource[1].value = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatWithoutSpace)
            self.enterBidDataSource[1].info?[Constants.UIKeys.firstValue] = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatyyyyMMdd) as AnyObject
            print(selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatyyyyMMdd))
            
        } else {
            self.enterBidDataSource[2].value = selectedDate.dateStringWith(strFormat: Constants.Format.timeFormathmma)
            self.enterBidDataSource[2].info?[Constants.UIKeys.firstValue] = selectedDate.dateStringWith(strFormat: Constants.Format.timeFormathhmm) as AnyObject
        }
        //        timeInfo[Constants.UIKeys.date] = false as AnyObject
        
        self.updateDataSource()
        self.bidDetailsTableView.reloadData()
    }
    
    func updateDataSource(shouldReloadTable: Bool = true) {
        if let delegate = self.delegate {
            delegate.didTapBidDetailsCell(cell: self, cellInfo: self.enterBidDataSource, shouldReload: shouldReloadTable)
        }
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        
        if let info = cellInfo.info,
            let itemsCoveredData = info[Constants.UIKeys.itemsCovered] as? [CellInfo] {
            self.enterBidDataSource = itemsCoveredData
            self.bidDetailsTableView.reloadData()
        }
        
        if let tender = cellInfo.info?[Constants.UIKeys.cellInfo] as? SupplierTender {
            self.tenderDetails = tender
        }
    }
    
}


//MARK:- UITableView Delegates & Datasource Methods
extension EnterBidDetailsCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.enterBidDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getCell(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.enterBidDataSource[indexPath.row].height
    }
}

extension EnterBidDetailsCell {
    
    func getCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cellType = self.enterBidDataSource[indexPath.row].cellType else { return UITableViewCell() }
        switch cellType {
        case .DaysRequiredCell:
            let cell: DaysRequiredCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.daysRequired = { cell, days in
                guard let indexPath = self.bidDetailsTableView.indexPath(for: cell) else { return }
                self.enterBidDataSource[indexPath.row].value = Helper.toString(object: days)
                self.updateDataSource()
            }
            return cell
        case .JobCell:
            let cell: JobCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: self.enterBidDataSource[indexPath.row])
            return cell
        case .RecoveryTypeCell:
            let cell: RecoveryTypeCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.recoveryType = { cell,text in
                guard let indexPath = self.bidDetailsTableView.indexPath(for: cell) else { return }
                self.selectedDropDown(indexPath: indexPath, text: text)
            }
            cell.configureView(cellInfo: self.enterBidDataSource[indexPath.row])
            return cell
        case .BidAmountCell:
            let cell: BidAmountCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.bidAmount = { [weak self] cell, amount in
                guard let sSelf = self else { return }
                guard let indexPath = sSelf.bidDetailsTableView.indexPath(for: cell) else { return }
                sSelf.enterBidDataSource[indexPath.row].value = Helper.toString(object: amount)
                sSelf.updateDataSource(shouldReloadTable: false)
            }
            cell.configureView(cellInfo: self.enterBidDataSource[indexPath.row])
            return cell
        case .EnterAdditionalInfoCell:
            let cell: EnterAdditionalInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: self.enterBidDataSource[indexPath.row])
            return cell
        case .TenderCompletedCell:
            let cell: TenderCompletedCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.tenderCompleted = { [weak self] cell, isSelected in
                
                guard let sSelf = self else { return }
                
                guard let indexPath = sSelf.bidDetailsTableView.indexPath(for: cell) else { return }
                
                if sSelf.enterBidDataSource.count > 4, sSelf.enterBidDataSource[3].placeHolder == "Job Description".localizedString()  {
               
                    if isSelected  {
                        sSelf.enterBidDataSource[3].value = Helper.toString(object: sSelf.tenderDetails?.description)
                    } else {
                        sSelf.enterBidDataSource[3].value = ""
                    }
                }
                                
                sSelf.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.firstValue] = isSelected as AnyObject
                sSelf.bidDetailsTableView.reloadData()
                sSelf.updateDataSource()
            }
            cell.configureView(cellInfo: self.enterBidDataSource[indexPath.row])
            return cell
        case .UploadVehicleImagesCell:
            let cell: UploadVehicleImagesCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate =  self
            cell.configureBidDetailsView(cellInfo: self.enterBidDataSource[indexPath.row])
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}

//EnterAdditionalInfoCellDelegate
extension EnterBidDetailsCell: EnterAdditionalInfoCellDelegate {
    
    func didChangeText(cell: EnterAdditionalInfoCell, text: String) {
        guard let indexPath = self.bidDetailsTableView.indexPath(for: cell) else { return }
        self.enterBidDataSource[indexPath.row].value = text
        self.updateDataSource(shouldReloadTable: false)
    }
}

//DaysRequiredCellDelegate
extension EnterBidDetailsCell: JobCellDelegate {
    
    func didTapSameAsTenderDate(cell: JobCell, isSameAsTender: Bool) {
        guard let indexPath = self.bidDetailsTableView.indexPath(for: cell) else { return }
        let date = Helper.toBool(self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.date])
        
        if isSameAsTender == true {
            
            if date {
                self.enterBidDataSource[indexPath.row].value = Helper.toString(object: tenderDetails?.getJobDateMMYYYDate())
                self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.firstValue] = Helper.toString(object: tenderDetails?.jobDate) as AnyObject
                
            } else {
                self.enterBidDataSource[indexPath.row].value = Helper.toString(object: tenderDetails?.getJobTime())
                self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.firstValue] = Helper.toString(object: tenderDetails?.jobTime) as AnyObject
            }
            
        } else {
            self.enterBidDataSource[indexPath.row].value = ""
        }
        self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.isSameAsTender] = isSameAsTender as AnyObject
        self.cancelButtonTapped()
        self.updateDataSource()
        self.bidDetailsTableView.reloadData()
    }
    
    func didTapDate(cell: JobCell) {
        guard let indexPath = self.bidDetailsTableView.indexPath(for: cell) else { return }
        self.isDate = Helper.toBool(self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.date])
        self.setupDateTimePicker(cell.dateTextField)
    }
}

//RecoveryTypeCellDelegate
extension EnterBidDetailsCell: RecoveryTypeCellDelegate {
    func didTapSameAsTenderDate(cell: RecoveryTypeCell, isSameAsTender: Bool) {
        guard let indexPath = self.bidDetailsTableView.indexPath(for: cell) else { return }
        self.updateSameAsTenderDetails(indexPath: indexPath, isSameAsTender: isSameAsTender)
        self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.isSameAsTender] = isSameAsTender as AnyObject
        self.cancelButtonTapped()
        self.updateDataSource()
        self.bidDetailsTableView.reloadData()
    }
    
}

extension EnterBidDetailsCell: UploadVehicleImagesCellDelegate {
    func didTapFirstImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.bidDetailsTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.firstImg] = image as AnyObject
                    self.bidDetailsTableView.reloadData()
                    self.updateDataSource()
                }
            }
        }
        
    }
    
    func didTapSecondImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.bidDetailsTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.secondImg] = image as AnyObject
                    self.bidDetailsTableView.reloadData()
                    self.updateDataSource()
                }
            }
        }
        
    }
    
    func didTapThirdImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.bidDetailsTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.thirdImg] = image as AnyObject
                    self.bidDetailsTableView.reloadData()
                    self.updateDataSource()
                }
            }
        }
    }
    
    func didTapFourthImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.bidDetailsTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.fourthImg] = image as AnyObject
                    self.bidDetailsTableView.reloadData()
                    self.updateDataSource()
                }
            }
        }
    }
}

//DaysRequiredCellDelegate
extension EnterBidDetailsCell {
    
    func selectedDropDown(indexPath: IndexPath, text:String) {
        
        if let recoveryType = self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.cellInfo] as? DropDownType {
            
            guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
            
            if recoveryType == .recoveryType {
                listPopupView.initializeViewWith(title: DropDownType.recoveryType.title, arrayList: DropDownType.recoveryType.dataList, key: DropDownType.recoveryType.rawValue) { [weak self] (response) in
                    guard let sSelf = self,
                        let employees = response[DropDownType.recoveryType.rawValue] as? String else { return }
                    sSelf.enterBidDataSource[indexPath.row].value = employees
                    sSelf.bidDetailsTableView.reloadRows(at: [indexPath], with: .none)
                    sSelf.updateDataSource()
                }
            } else if recoveryType == .typeOfService {
                listPopupView.initializeViewWith(title: DropDownType.typeOfService.title, arrayList: DropDownType.typeOfService.dataList, key: DropDownType.typeOfService.rawValue) { [weak self] (response) in
                    guard let sSelf = self,
                        let typeOfService = response[DropDownType.typeOfService.rawValue] as? String else { return }
                    sSelf.enterBidDataSource[indexPath.row].value = typeOfService
                    sSelf.bidDetailsTableView.reloadRows(at: [indexPath], with: .none)
                    sSelf.updateDataSource()
                }
            } else if recoveryType == .partType {
                listPopupView.initializeViewWith(title: DropDownType.partType.title, arrayList: DropDownType.partType.dataList, key: DropDownType.partType.rawValue) { [weak self] (response) in
                    guard let sSelf = self,
                        let partType = response[DropDownType.partType.rawValue] as? String else { return }
                    sSelf.enterBidDataSource[indexPath.row].value = partType
                    sSelf.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = Helper.toInt(response[Constants.UIKeys.id]) as AnyObject
                    sSelf.bidDetailsTableView.reloadRows(at: [indexPath], with: .none)
                    sSelf.updateDataSource()
                }
            } else if recoveryType == .wheelsService {
                
                listPopupView.initializeViewWith(title: DropDownType.wheelsService.title, arrayList: DropDownType.wheelsService.dataList, key: DropDownType.wheelsService.rawValue) { [weak self] (response) in
                    guard let sSelf = self,
                        let wheelsService = response[DropDownType.wheelsService.rawValue] as? String else { return }
                    sSelf.enterBidDataSource[indexPath.row].value = wheelsService
                    let serviceID = Helper.toInt(response[Constants.UIKeys.id])
                    sSelf.enterBidDataSource[4].height =  serviceID == 1 ? Constants.CellHeightConstants.height_60 : 0
                    sSelf.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = serviceID as AnyObject
                    sSelf.updateDataSource()
                }
            }  else if recoveryType == .serviceType {
                listPopupView.initializeViewWith(title: DropDownType.serviceType.title, arrayList: DropDownType.serviceType.dataList, key: DropDownType.serviceType.rawValue) { [weak self] (response) in
                    guard let sSelf = self,
                        let serviceType = response[DropDownType.serviceType.rawValue] as? String else { return }
                    sSelf.enterBidDataSource[indexPath.row].value = serviceType
                    sSelf.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = Helper.toInt(response[Constants.UIKeys.id]) as AnyObject
                    sSelf.bidDetailsTableView.reloadRows(at: [indexPath], with: .none)
                    sSelf.updateDataSource()
                }
            }   else if recoveryType == .tintPercentage {
                listPopupView.initializeViewWith(title: DropDownType.tintPercentage.title, arrayList: DropDownType.tintPercentage.dataList, key: DropDownType.tintPercentage.rawValue) { [weak self] (response) in
                    guard let sSelf = self,
                        let tintPercentage = response[DropDownType.tintPercentage.rawValue] as? String else { return }
                    sSelf.enterBidDataSource[indexPath.row].value = tintPercentage
                    sSelf.bidDetailsTableView.reloadRows(at: [indexPath], with: .none)
                    sSelf.updateDataSource()
                }
            } else if recoveryType == .bodyServiceType {
                listPopupView.initializeViewWith(title: DropDownType.bodyServiceType.title, arrayList: DropDownType.bodyServiceType.dataList, key: DropDownType.bodyServiceType.rawValue) { [weak self] (response) in
                    guard let sSelf = self,
                        let bodyServiceType = response[DropDownType.bodyServiceType.rawValue] as? String else { return }
                    sSelf.enterBidDataSource[indexPath.row].value = bodyServiceType
                    sSelf.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = Helper.toInt(response[Constants.UIKeys.id]) as AnyObject
                    sSelf.bidDetailsTableView.reloadRows(at: [indexPath], with: .none)
                    sSelf.updateDataSource()
                }
            }  else if recoveryType == .insuranceType {
                listPopupView.initializeViewWith(title: DropDownType.insuranceType.title, arrayList: DropDownType.insuranceType.dataList, key: DropDownType.insuranceType.rawValue) { [weak self] (response) in
                    guard let sSelf = self,
                        let insuranceType = response[DropDownType.insuranceType.rawValue] as? String else { return }
                    sSelf.enterBidDataSource[indexPath.row].value = insuranceType
                    sSelf.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = Helper.toInt(response[Constants.UIKeys.id]) as AnyObject
                    sSelf.bidDetailsTableView.reloadRows(at: [indexPath], with: .none)
                    sSelf.updateDataSource()
                }
            } else if recoveryType == .carDetailingServiceType {
                
                listPopupView.initializeViewWith(title: DropDownType.carDetailingServiceType.title, arrayList: DropDownType.carDetailingServiceType.dataList, key: DropDownType.carDetailingServiceType.rawValue) { [weak self] (response) in
                    guard let sSelf = self,
                        let carDetailingServiceType = response[DropDownType.carDetailingServiceType.rawValue] as? String else { return }
                    sSelf.enterBidDataSource[indexPath.row].value = carDetailingServiceType
                    let serviceID = Helper.toInt(response[Constants.UIKeys.id])
                    sSelf.enterBidDataSource[4].height =  serviceID == 2 ? Constants.CellHeightConstants.height_60 : 0
                    sSelf.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = serviceID as AnyObject
                    sSelf.updateDataSource()
                }
            } else if recoveryType == .detailingService {
                listPopupView.initializeViewWith(title: DropDownType.detailingService.title, arrayList: DropDownType.detailingService.dataList, key: DropDownType.detailingService.rawValue) { [weak self] (response) in
                    guard let sSelf = self,
                        let detailingService = response[DropDownType.detailingService.rawValue] as? String else { return }
                    sSelf.enterBidDataSource[indexPath.row].value = detailingService
                    sSelf.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = Helper.toInt(response[Constants.UIKeys.id]) as AnyObject
                    sSelf.bidDetailsTableView.reloadRows(at: [indexPath], with: .none)
                    sSelf.updateDataSource()
                }
            }
            listPopupView.showWithAnimated(animated: true)
            
        }  else  if let isBank = self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.isBank] as? Bool, isBank == true {
            self.selectBankName?(self,indexPath.row)
            
        } else {
            self.enterBidDataSource[indexPath.row].value = text
            //self.updateDataSource()
            self.updateDataSource(shouldReloadTable: false)
        }
        
    }
}

extension EnterBidDetailsCell {
    
    func updateSameAsTenderDetails(indexPath: IndexPath, isSameAsTender: Bool) {
        
        if let recoveryType = self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.cellInfo] as? DropDownType {
            
            if recoveryType == .recoveryType {
                self.enterBidDataSource[indexPath.row].value = isSameAsTender == true ? Helper.toString(object: tenderDetails?.recoveryType) : ""
            } else if recoveryType == .typeOfService {
                self.enterBidDataSource[indexPath.row].value = isSameAsTender == true ? Helper.toString(object: tenderDetails?.typeOfService) : ""
            } else if recoveryType == .partType {
                let partType = Helper.toInt(tenderDetails?.partType)
                self.enterBidDataSource[indexPath.row].value = isSameAsTender == true ? Helper.getPartType(statusCode: partType) : ""
                self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = isSameAsTender == true ? partType as AnyObject : 0 as AnyObject
            } else if recoveryType == .wheelsService {
                
                if isSameAsTender ==  true {
                    
                    let wheelType = Helper.toInt(tenderDetails?.wheelType)
                    self.enterBidDataSource[indexPath.row].value = Helper.getWheelType(statusCode: Helper.toInt(tenderDetails?.wheelType))
                    
                    if wheelType == 1 {
                        self.enterBidDataSource[4].height = Constants.CellHeightConstants.height_60
                    }
                    self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = wheelType as AnyObject
                    
                } else {
                    self.enterBidDataSource[indexPath.row].value = ""
                    self.enterBidDataSource[4].height = 0
                }
                
            } else if recoveryType == .serviceType {
                
                let rimServiceType = Helper.toInt(tenderDetails?.rimServiceType)
                self.enterBidDataSource[indexPath.row].value = isSameAsTender == true ? Helper.getRimType(statusCode:rimServiceType) : ""
                self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = isSameAsTender == true ? rimServiceType as AnyObject : 0 as AnyObject
                
            } else if recoveryType == .tintPercentage {
                
                self.enterBidDataSource[indexPath.row].value = isSameAsTender == true ? Helper.toString(object: tenderDetails?.tintPercentage) : ""
            } else if recoveryType == .bodyServiceType {
                
                let bodyWorkType = Helper.toInt(tenderDetails?.bodyServiceType)
                self.enterBidDataSource[indexPath.row].value = isSameAsTender == true ? Helper.toString(object: Helper.getBodyWorkType(statusCode: bodyWorkType)) : ""
                self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = isSameAsTender == true ? bodyWorkType as AnyObject : 0 as AnyObject
                
            }  else if recoveryType == .insuranceType {
                let insuranceType = Helper.toInt(tenderDetails?.typeOfInsurance)
                self.enterBidDataSource[indexPath.row].value = isSameAsTender == true ? Helper.getInsuranceType(statusCode: insuranceType) : ""
                self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = isSameAsTender == true ? insuranceType as AnyObject : 0 as AnyObject
                
            } else if recoveryType == .carDetailingServiceType {
                
                if isSameAsTender ==  true {
                    
                    let carDetailingServiceType = Helper.toInt(tenderDetails?.carDetailingServiceType)
                    self.enterBidDataSource[indexPath.row].value = Helper.getCarDetailingServiceType(statusCode: carDetailingServiceType)
                    
                    if carDetailingServiceType ==  CarDetailingServiceType.detailing.rawValue {
                        self.enterBidDataSource[4].height = Constants.CellHeightConstants.height_60
                    }
                    
                    self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = carDetailingServiceType as AnyObject
                    
                } else {
                    self.enterBidDataSource[indexPath.row].value = ""
                    self.enterBidDataSource[4].height = 0
                }
                
            } else if recoveryType == .detailingService {
                let carDetailingOptions = Helper.toInt(tenderDetails?.carDetailingOptions)
                self.enterBidDataSource[indexPath.row].value = isSameAsTender == true ? Helper.getDetailingService(statusCode: carDetailingOptions) : ""
                self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = isSameAsTender == true ? carDetailingOptions as AnyObject : 0 as AnyObject
            }
        } else {
            
            if let inputType = self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.inputType] as? TextInputType {
                
                if inputType == .partName {
                    self.enterBidDataSource[indexPath.row].value = isSameAsTender == true ? Helper.toString(object: tenderDetails?.partName) : ""
                } else if inputType == .wheelSize {
                    self.enterBidDataSource[indexPath.row].value = isSameAsTender == true ? Helper.toString(object: tenderDetails?.wheelSize) : ""
                } else if inputType == .tintColor {
                    self.enterBidDataSource[indexPath.row].value = isSameAsTender == true ? Helper.toString(object: tenderDetails?.tintColor) : ""
                } else if inputType == .vehicleValue {
                    self.enterBidDataSource[indexPath.row].value = isSameAsTender == true ? Helper.toString(object: tenderDetails?.vehiclePrice) : ""
                } else if inputType == .carPrice {
                    self.enterBidDataSource[indexPath.row].value = isSameAsTender == true ? Helper.toString(object: tenderDetails?.carPrice) : ""
                } else if inputType == .bankName {
                    self.enterBidDataSource[indexPath.row].value = isSameAsTender == true ? Helper.toString(object: tenderDetails?.bankName) : ""
                    self.enterBidDataSource[indexPath.row].info?[Constants.UIKeys.id] = isSameAsTender == true ? Helper.toString(object: tenderDetails?.bankId) as AnyObject : "" as AnyObject
                }
            }
        }
        
    }
}
