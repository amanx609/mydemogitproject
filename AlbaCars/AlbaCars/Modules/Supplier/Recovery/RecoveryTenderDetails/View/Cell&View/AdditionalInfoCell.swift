//
//  AdditionalInfoCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class AdditionalInfoCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    @IBOutlet weak var additionalView: AdditionalInfoView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.additionalView.descTitleLabel.text = cellInfo.placeHolder
        self.additionalView.descDetailLabel.text = cellInfo.value
    }
    
}
