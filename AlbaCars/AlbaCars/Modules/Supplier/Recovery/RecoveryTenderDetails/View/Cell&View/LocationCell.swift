//
//  LocationCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class LocationCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
    @IBOutlet weak var dashedLineView: UIView!
    @IBOutlet weak var pickupLocationLabel: UILabel!
    @IBOutlet weak var dropoffLocationLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.dashedLineView.addDashedLine(horizontal: false)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.pickupLocationLabel.text = cellInfo.placeHolder
        self.dropoffLocationLabel.text = cellInfo.value
    }
}
