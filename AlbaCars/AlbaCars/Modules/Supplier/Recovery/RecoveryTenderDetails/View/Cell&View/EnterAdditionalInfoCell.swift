//
//  EnterAdditionalInfoCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
import UITextView_Placeholder

protocol EnterAdditionalInfoCellDelegate: class {
    func didChangeText(cell: EnterAdditionalInfoCell, text: String)
}

class EnterAdditionalInfoCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var textView: UITextView!
    
    
    //MARK: - Variables
    weak var delegate: EnterAdditionalInfoCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.textView.delegate = self
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.bgView.makeLayer(color: .borderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.textView.placeholder = cellInfo.placeHolder
        self.textView.text = cellInfo.value
    }
}

extension EnterAdditionalInfoCell: UITextViewDelegate {
    
    // Use this if you have a UITextView
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // get the current text, or use an empty string if that failed
        let currentText = textView.text ?? ""
        
        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        if let delegate = self.delegate {
            delegate.didChangeText(cell: self, text: updatedText)
        }
        return true
    }
    
}
