//
//  TenderCarDetailsCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class TenderCarDetailsCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var carTypeLabel: UILabel!
    @IBOutlet weak var carPlateNumberLabel: UILabel!
    @IBOutlet weak var carModelNameLabel: UILabel!
    @IBOutlet weak var carModelYearLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {

        if let tender = cellInfo.info?[Constants.UIKeys.cellInfo] as? SupplierTender {
            self.carNameLabel.text = tender.carTitle()
            self.brandNameLabel.attributedText = String.getAttributedText(firstText: "Brand : ".localizedString(), secondText:Helper.toString(object: tender.brandName), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.carModelNameLabel.attributedText = String.getAttributedText(firstText: "Model : ".localizedString(), secondText:Helper.toString(object: tender.modelName), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.carTypeLabel.attributedText = String.getAttributedText(firstText: "Car Type : ".localizedString(), secondText:Helper.toString(object: tender.typeName), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.carPlateNumberLabel.attributedText = String.getAttributedText(firstText: "Car Plate / VIN Number : ".localizedString(), secondText:Helper.toString(object: tender.plateNumber), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.carModelYearLabel.attributedText = String.getAttributedText(firstText: "Model Year : ".localizedString(), secondText:Helper.toString(object: tender.year), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        }
    }
}
