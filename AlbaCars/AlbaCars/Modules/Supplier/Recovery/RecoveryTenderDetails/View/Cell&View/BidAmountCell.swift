//
//  BidAmountCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class BidAmountCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var amountTextField: UITextField!
    
    //MARK: - Variables
    var bidAmount:((BidAmountCell,String)-> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.amountTextField.delegate = self
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.bgView.makeLayer(color: .borderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.amountTextField.placeholder = cellInfo.placeHolder
        self.amountTextField.keyboardType = cellInfo.keyboardType
        
    }
    
}


extension BidAmountCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
            self.bidAmount?(self,text)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let finalText = text.replacingCharacters(in: textRange, with: string)
            if finalText.count > Constants.Validations.priceMaxLength {
                return false
            }
            self.bidAmount?(self,finalText)
        }
        return true
    }
}
