//
//  TenderDetailsTitleCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class TenderDetailsTitleCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.titleLabel.text = cellInfo.placeHolder
        self.lineView.isHidden = false
        self.titleLabel.font = UIFont.font(name: .AirbnbCerealApp, weight: .Medium, size: .size_18)

    }
    
    //MARK: - Public Methods
    func configureAttributedView(cellInfo: CellInfo) {
        self.lineView.isHidden = true
        self.titleLabel.font = UIFont.font(name: .AirbnbCerealApp, weight: .Medium, size: .size_11)
        //SetupTextAttributes
        let placeHolder = cellInfo.placeHolder + " \(Helper.toString(object: cellInfo.value))"
        let attriString = NSMutableAttributedString(string: placeHolder)
        let textRange = (placeHolder as NSString).range(of: cellInfo.value)
        attriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.redButtonColor, range: textRange)
        self.titleLabel.attributedText = attriString
    }
}
