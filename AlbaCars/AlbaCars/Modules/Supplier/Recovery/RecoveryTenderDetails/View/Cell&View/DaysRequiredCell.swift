//
//  DaysRequiredCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

//protocol DaysRequiredCellDelegate: class {
//    func didTapDaysRequiredCell(cell: DaysRequiredCell, days:Int)
//}

class DaysRequiredCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
    
    @IBOutlet weak var countLabel: UILabel!
    //MARK: - Variables
   // weak var delegate: DaysRequiredCellDelegate?
    var count = 1
    var daysRequired:((DaysRequiredCell,Int)-> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK: - IBActions
    
    @IBAction func plusButtonTapped(_ sender: Any) {
        count = count + 1
        self.countLabel.text = Helper.toString(object: count)
        self.daysRequired?(self,count)

    }
    
    @IBAction func minusButtonTapped(_ sender: Any) {
        
        if count == 1 {
            return
        }
        count = count - 1
        self.countLabel.text = Helper.toString(object: count)
        self.daysRequired?(self,count)
        
//        if let delegate = self.delegate {
//            delegate.didTapDaysRequiredCell(cell: self, days: count)
//        }
    }
    
}
