//
//  RecoveryTypeCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol RecoveryTypeCellDelegate: class {
    func didTapSameAsTenderDate(cell: RecoveryTypeCell,isSameAsTender: Bool)
}

class RecoveryTypeCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var checkedButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var recoveryTypeLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var dropDownButton: UIButton!
    
    //MARK: - Variables
    var recoveryType:((RecoveryTypeCell,String)-> Void)?
    weak var delegate: RecoveryTypeCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.inputTextField.delegate = self
        self.textFieldView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.textFieldView.makeLayer(color: .borderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.titleLabel.text = cellInfo.placeHolder
        self.recoveryTypeLabel.text = cellInfo.value
        
       // if let isBank = cellInfo.info?[Constants.UIKeys.isBank] as? Bool, isBank == true
        
        if let recoveryType = cellInfo.info?[Constants.UIKeys.cellInfo] as? DropDownType {
            self.inputTextField.isHidden =  true
        } else if let isBank = cellInfo.info?[Constants.UIKeys.isBank] as? Bool, isBank == true {
            self.inputTextField.isHidden =  true
        }
        else {
            self.inputTextField.isHidden =  false
            self.inputTextField.text = cellInfo.placeHolder
            self.inputTextField.text = cellInfo.value
        }
        
         if let isSameAsTender = cellInfo.info?[Constants.UIKeys.isSameAsTender] as? Bool, isSameAsTender == true, !cellInfo.value.isEmpty {
            self.dropDownButton.isUserInteractionEnabled = false
            self.inputTextField.isUserInteractionEnabled = false
         } else {
            self.dropDownButton.isUserInteractionEnabled = true
            self.inputTextField.isUserInteractionEnabled = true
        }
        
    }
    
    //MARK: - IBActions
    @IBAction func selectDropDownTapped(_ sender: Any) {
        self.recoveryType?(self,"")
    }
    
    //MARK: - IBActions
    @IBAction func sameAsTenderButtonTapped(_ sender: Any) {
        self.checkedButton.isSelected = !self.checkedButton.isSelected
        if let delegate = self.delegate {
            delegate.didTapSameAsTenderDate(cell: self, isSameAsTender: self.checkedButton.isSelected)
        }
    }
}


extension RecoveryTypeCell: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    return true
  }
 
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    if let text = textField.text, let textRange = Range(range, in: text) {
      let finalText = text.replacingCharacters(in: textRange, with: string)
      
        if finalText.count > Constants.Validations.maxaddressLength {
          return false
        }
        
        self.recoveryType?(self,finalText)

    }
    return true
  }
}
