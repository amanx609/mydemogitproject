//
//  SRecoveryTenderDetailsViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 1/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension SRecoveryTenderDetailsViewC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tenderDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getCell(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tenderDataSource[indexPath.row].height
    }
    
}

extension SRecoveryTenderDetailsViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.tenderDataSource[indexPath.row]
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .TenderDetailsTitleCell:
            let cell: TenderDetailsTitleCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .TenderDetailsAttributedCell:
            let cell: TenderDetailsTitleCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureAttributedView(cellInfo: cellInfo)
            return cell
        case .TenderCarDetailsCell:
            let cell: TenderCarDetailsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .LocationCell:
            let cell: LocationCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .AdditionalInfoCell:
            let cell: AdditionalInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .EnterBidDetailsCell:
            let cell: EnterBidDetailsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
        
    }
}

//EnterBidDetailsCellDelegate
extension SRecoveryTenderDetailsViewC: EnterBidDetailsCellDelegate {
   
    func didTapBidDetailsCell(cell: EnterBidDetailsCell, cellInfo: [CellInfo], shouldReload: Bool) {
        guard let indexPath = self.tenderTableView.indexPath(for: cell) else { return }
        self.tenderDataSource[indexPath.row].info?[Constants.UIKeys.itemsCovered] = cellInfo as AnyObject
    }
    
    func didTapHideKeyboard() {
        self.view.endEditing(true)
    }
    
}
