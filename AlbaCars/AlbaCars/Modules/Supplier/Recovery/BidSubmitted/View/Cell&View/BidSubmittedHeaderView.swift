//
//  BidSubmittedHeaderView.swift
//  AlbaCars
//
//  Created by Narendra on 2/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class BidSubmittedHeaderView: UIView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var amountLabel: UILabel!
    
    //MARK: - Public Methods
    class func inistancefromNib() -> BidSubmittedHeaderView? {
        if let view = UINib(nibName:BidSubmittedHeaderView.className, bundle:nil).instantiate(withOwner:nil, options:nil)[0] as? BidSubmittedHeaderView {
            view.frame = CGRect(x: 0, y: 0, width:Constants.Devices.ScreenWidth, height: 150)
            return view
        }
        return nil
    }
    
    func setupView(tenderDetails: SupplierTender?) {
        
        if let tenderDetail = tenderDetails {
            self.amountLabel.text = "Submitted Bid Amount : ".localizedString() + Helper.toString(object: tenderDetail.formattedPrice())
        }
    }
}
