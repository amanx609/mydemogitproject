//
//  SBidSubmittedViewC.swift
//  AlbaCars
//
//  Created by Narendra on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SBidSubmittedViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bidTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    
    //MARK: - Variables
    var viewModel: SBidSubmittedViewModeling?
    var bidDataSource: [CellInfo] = []
    var tenderDetails: SupplierTender?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Bid Submitted".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setUpTableView()
        // self.priceLabel.text = "Submitted Bid Amount : ".localizedString() + Helper.toString(object: tenderDetails?.formattedPrice())
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = SBidSubmittedViewM()
        }
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.bidTableView.delegate = self
        self.bidTableView.dataSource = self
        self.bidTableView.separatorStyle = .none
        //self.bidTableView.tableHeaderView =  self.headerView
        
        if let dataSource = self.viewModel?.getBidSubmittedDataSource(tenderDetails: tenderDetails) {
            self.bidDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.bidTableView.reloadData()
            }
        }
        
        if let bidSubmittedHeaderView = BidSubmittedHeaderView.inistancefromNib() {
            // bidSubmittedHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 105)
            bidSubmittedHeaderView.setupView(tenderDetails: tenderDetails)
            self.bidTableView.tableHeaderView = bidSubmittedHeaderView
        }
    }
    
    private func registerNibs() {
        self.bidTableView.register(TenderDetailsTitleCell.self)
        self.bidTableView.register(TenderCarDetailsCell.self)
        self.bidTableView.register(LocationCell.self)
        self.bidTableView.register(AdditionalInfoCell.self)
    }
}
