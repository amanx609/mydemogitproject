//
//  BidsRejectedHeaderView.swift
//  AlbaCars
//
//  Created by Narendra on 2/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class BidsRejectedHeaderView: UIView {

    @IBOutlet weak var priceLabel: UILabel!

    //MARK: - Public Methods
       class func inistancefromNib() -> BidsRejectedHeaderView? {
           
           if let view = UINib(nibName:BidsRejectedHeaderView.className, bundle:nil).instantiate(withOwner:nil, options:nil)[0] as? BidsRejectedHeaderView {
               return view
           }
           return nil
       }
       
    
}
