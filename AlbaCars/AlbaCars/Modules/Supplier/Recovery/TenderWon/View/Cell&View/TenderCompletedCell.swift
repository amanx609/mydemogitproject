//
//  TenderCompletedCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/29/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class TenderCompletedCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!

    //MARK: - Variables
    var tenderCompleted:((TenderCompletedCell, Bool)-> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.titleLabel.text = cellInfo.placeHolder
        
        if let info = cellInfo.info {
            //Image
            if let placeholderImage = info[Constants.UIKeys.placeholderImage] as? UIImage {
                self.checkButton.setImage(placeholderImage, for: .selected)
            }
            
            if let isSelected = info[Constants.UIKeys.firstValue] as? Bool {
                self.checkButton.isSelected = isSelected
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func tapCheckButton(_ sender: UIButton) {
        self.checkButton.isSelected = !self.checkButton.isSelected
        self.tenderCompleted?(self,self.checkButton.isSelected)
    }
}
