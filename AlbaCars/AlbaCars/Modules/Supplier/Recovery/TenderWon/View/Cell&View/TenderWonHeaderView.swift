//
//  TenderWonHeaderView.swift
//  AlbaCars
//
//  Created by Narendra on 2/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class TenderWonHeaderView: UIView {

    //MARK: - Public Methods
    class func inistancefromNib() -> TenderWonHeaderView? {
        if let view = UINib(nibName:TenderWonHeaderView.className, bundle:nil).instantiate(withOwner:nil, options:nil)[0] as? TenderWonHeaderView {
            return view
        }
        return nil
    }

}
