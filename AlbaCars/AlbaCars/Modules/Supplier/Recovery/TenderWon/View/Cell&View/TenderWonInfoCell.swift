//
//  TenderWonInfoCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/29/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class TenderWonInfoCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.titleLabel.text = cellInfo.placeHolder
    }
    
}
