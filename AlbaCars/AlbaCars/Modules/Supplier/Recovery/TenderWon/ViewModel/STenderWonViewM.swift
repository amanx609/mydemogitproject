//
//  STenderWonViewM.swift
//  AlbaCars
//
//  Created by Narendra on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol STenderWonViewModeling {
    func getTenderWonDataSource(tenderDetails: SupplierTender?) -> [CellInfo]
    func requestVerifyPickUpCodeAPI(param: [String:AnyObject], completion: @escaping (Bool) -> Void)
    func requestPaymentAPI(param: [String:AnyObject], completion: @escaping (Bool) -> Void)
}

class STenderWonViewM: STenderWonViewModeling {
    
    func getTenderWonDataSource(tenderDetails: SupplierTender?) -> [CellInfo] {
        
        var array = [CellInfo]()
        let carTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Car Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(carTitleCell)
        
        var carDetailsInfo = [String: AnyObject]()
        carDetailsInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        let carCell = CellInfo(cellType: .TenderCarDetailsCell, placeHolder:"Car Details".localizedString(), value:"" ,info:carDetailsInfo, height: Constants.CellHeightConstants.height_115)
        array.append(carCell)
        
        let tenderTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Tender Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(tenderTitleCell)
        
        let locationCell = CellInfo(cellType: .LocationCell, placeHolder:Helper.toString(object: tenderDetails?.pickupLocation), value:Helper.toString(object: tenderDetails?.dropoffLocation) ,info:nil, height: UITableView.automaticDimension)
        array.append(locationCell)
        
        let recoveryTypeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Recovery Type :".localizedString(), value:Helper.toString(object: tenderDetails?.tenderRecoveryType) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(recoveryTypeCell)
        
        //Additional Information
        let descriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.tenderAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(descriptionCell)
        
        //Requested Date
        let requestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedDateCell)
        
        let requestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedTimeCell)
        
        let bidTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Bid Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(bidTitleCell)
        
        let daysCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Days Required for the Service :".localizedString(), value:"\(Helper.toString(object: tenderDetails?.requireDaysService)) Days" ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(daysCell)
        
        let bidLocationCell = CellInfo(cellType: .LocationCell, placeHolder:Helper.toString(object: tenderDetails?.pickupLocation), value:Helper.toString(object: tenderDetails?.dropoffLocation) ,info:nil, height: UITableView.automaticDimension)
        array.append(bidLocationCell)
        
        let bidRecoveryTypeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Recovery Type :".localizedString(), value:Helper.toString(object: tenderDetails?.bidRecoveryType) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRecoveryTypeCell)
        
        //Additional Information
        let bidDescriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.bidAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(bidDescriptionCell)
        
        //Requested Date
        let bidRequestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedDateCell)
        
        let bidRequestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedTimeCell)
        
        let bidPriceCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Bid Price :".localizedString(), value:Helper.toString(object: tenderDetails?.formattedPrice()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidPriceCell)
        
        let customerDetailsTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Customer Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(customerDetailsTitleCell)
        
        //ContactInfoCell
        var contactInfoInfo = [String: AnyObject]()
        contactInfoInfo[Constants.UIKeys.image] = tenderDetails?.image as AnyObject
        let contactInfoCell = CellInfo(cellType: .RDContactInfoCell, placeHolder: Helper.toString(object: tenderDetails?.name), value: Helper.toString(object: tenderDetails?.pickupLocation), info: contactInfoInfo, height: UITableView.automaticDimension)
        array.append(contactInfoCell)
        
        let secondBidCell = CellInfo(cellType: .VIContactInfoCell, placeHolder: Helper.toString(object: tenderDetails?.mobile), value: Helper.toString(object: tenderDetails?.email), info: nil, height: Constants.CellHeightConstants.height_100)
        array.append(secondBidCell)
        
        //Pickup Code Cell
        var codeTitleInfo = [String: AnyObject]()
        codeTitleInfo[Constants.UIKeys.placeholderText] = "Pickup Code".localizedString() as AnyObject
        codeTitleInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.center as AnyObject
        let codeTitleCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "".localizedString(), value: "", info: codeTitleInfo, height: Constants.CellHeightConstants.height_40)
        array.append(codeTitleCell)
        
        //Coupon code cell
        var codeCellInfo = [String: AnyObject]()
        codeCellInfo[Constants.UIKeys.backgroundColor] = UIColor.pinkButtonColor as AnyObject
        codeCellInfo[Constants.UIKeys.boundaryColor] = UIColor.redButtonColor as AnyObject
        let codeCell = CellInfo(cellType: .DashedTextFieldCell, placeHolder: "To be provided by customer at the start of recovery".localizedString(), value: "", info: codeCellInfo, height: Constants.CellHeightConstants.height_60)
        array.append(codeCell)
        
        //Drop Off Cell
        var dropTitleInfo = [String: AnyObject]()
        dropTitleInfo[Constants.UIKeys.placeholderText] = "Drop off Code".localizedString() as AnyObject
        dropTitleInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.center as AnyObject
        let dropTitleCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "".localizedString(), value: "", info: dropTitleInfo, height: Constants.CellHeightConstants.height_40)
        array.append(dropTitleCell)
        
        //Coupon code2 cell
        var codeCell2Info = [String: AnyObject]()
        codeCell2Info[Constants.UIKeys.backgroundColor] = UIColor.lightGreenColor as AnyObject
        codeCell2Info[Constants.UIKeys.boundaryColor] = UIColor.greenTopHeaderColor as AnyObject
        let codeCell2 = CellInfo(cellType: .DashedTextFieldCell, placeHolder: "", value: Helper.toString(object: tenderDetails?.dropoffCode), info: codeCell2Info, height: Constants.CellHeightConstants.height_60)
        array.append(codeCell2)
        
        let infoCell = CellInfo(cellType: .TenderWonInfoCell, placeHolder: "You will be able to request payment only after both the customer & you have marked job as completed".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(infoCell)
        
        let infoCell2 = CellInfo(cellType: .TenderWonInfoCell, placeHolder: "Please tap on \"Mark Job as Completed\" button only after Services/Tenders are completed".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(infoCell2)
        
        let completedCell = CellInfo(cellType: .TenderCompletedCell, placeHolder: "Mark Job as Completed".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(completedCell)
        
        return array
    }
    
    func requestVerifyPickUpCodeAPI(param: [String:AnyObject], completion: @escaping (Bool) -> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .verifyPickupCode(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let _ = safeResponse[ConstantAPIKeys.result] as? String
                {
                    completion(success)
                }
            }
        }
    }
    
    func requestPaymentAPI(param: [String:AnyObject], completion: @escaping (Bool) -> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .requestPayment(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                {
                    completion(success)
                }
            }
        }
    }
}
