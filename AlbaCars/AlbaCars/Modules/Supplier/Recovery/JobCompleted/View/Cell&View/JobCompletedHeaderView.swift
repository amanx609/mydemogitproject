//
//  JobCompletedHeaderView.swift
//  AlbaCars
//
//  Created by Narendra on 2/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class JobCompletedHeaderView: UIView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var pendingView: UIView!
    @IBOutlet weak var pendingAmountLabel: UILabel!
    @IBOutlet weak var completedView: UIView!
    @IBOutlet weak var completedAmountLabel: UILabel!
        
    //MARK: - Public Methods
    class func inistancefromNib() -> JobCompletedHeaderView? {
        if let view = UINib(nibName:JobCompletedHeaderView.className, bundle:nil).instantiate(withOwner:nil, options:nil)[0] as? JobCompletedHeaderView {
            return view
        }
        return nil
    }
    
    func setupView(tenderDetails: SupplierTender?) {

        if let tenderDetail = tenderDetails {
            
            if tenderDetail.paymentStatus == PaymentStatus.success.rawValue {
                self.completedView.isHidden = false
                self.pendingView.isHidden = true
                self.completedAmountLabel.text = "Payment Completed".localizedString() + "\n\n" +  Helper.toString(object: tenderDetails?.formattedPrice())
            } else {
                
                self.completedView.isHidden = true
                self.pendingView.isHidden = false
                self.pendingAmountLabel.text = "Payment Pending : ".localizedString() + Helper.toString(object: tenderDetails?.formattedPrice())
            }
        }
    }
    
}
