//
//  SVehicleInspectionTendersViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 1/29/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension SVehicleInspectionTendersViewC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tender.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getCell(tableView: tableView, indexPath: indexPath)
    }
}

extension SVehicleInspectionTendersViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let tender = self.tender[indexPath.row]
        
        if self.activeTenderButton.isSelected == true {
            let cell: VIActiveTenderCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(supplierTender: tender)
            return cell
        }
        
        let cell: VIMyTenderCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.configureView(supplierTender: tender)
        return cell
    }
}

//ActiveTenderCellDelegate
extension SVehicleInspectionTendersViewC: VIActiveTenderCellDelegate {
    func didTapViewDetailsCell(cell: VIActiveTenderCell) {
        guard let indexPath = self.tenderTableView.indexPath(for: cell) else { return }
        let tender = self.tender[indexPath.row]
        self.requestActiveTenderDetailsAPI(tenderId: Helper.toInt(tender.id))
    }
}

//ActiveTenderCellDelegate
extension SVehicleInspectionTendersViewC: VIMyTenderCellDelegate {
    func didTapViewDetailsCell(cell: VIMyTenderCell) {
        guard let indexPath = self.tenderTableView.indexPath(for: cell) else { return }
        let tender = self.tender[indexPath.row]
        self.requestMyTenderDetailsAPI(tenderId: Helper.toInt(tender.id))
    }
}
