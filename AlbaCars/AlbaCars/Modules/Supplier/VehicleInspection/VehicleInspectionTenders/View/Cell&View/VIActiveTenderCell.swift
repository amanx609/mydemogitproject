//
//  VIActiveTenderCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/29/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol VIActiveTenderCellDelegate: class {
    func didTapViewDetailsCell(cell: VIActiveTenderCell)
}

class VIActiveTenderCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var viewDetailsButton: UIButton!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var liveLabel: UILabel!
    @IBOutlet weak var timeCountLabel: UILabel!

    //MARK: - Variables
    weak var delegate: VIActiveTenderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.viewDetailsButton.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.bgView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
    }
    
    //MARK: - Public Methods
    func configureView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        self.dateLabel.attributedText = String.getAttributedText(firstText: "Date of Inspection : ".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.getRequestedDate()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.timeLabel.attributedText = String.getAttributedText(firstText: "Time of Inspection : ".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.getRequestedTime()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        
        //Timer
        if let timerDuration = supplierTender.timerDuration {
            self.timeCountLabel.text = timerDuration.convertToTimerFormat()
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func viewDetailsButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapViewDetailsCell(cell: self)
        }
    }
    
}
