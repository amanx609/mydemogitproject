//
//  SSparePartsBidsRejectedViewC.swift
//  AlbaCars
//
//  Created by Narendra on 2/12/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SSparePartsBidsRejectedViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bidTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    
    //MARK: - Variables
    var viewModel: SSparePartsBidSubmittedViewModeling?
    var bidDataSource: [CellInfo] = []
    var tenderDetails: SupplierTender?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Bid Rejected".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setUpTableView()
        self.priceLabel.text = "Winning Bid Amount : ".localizedString() + Helper.toString(object: tenderDetails?.winningAmount())

    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = SSparePartsBidSubmittedViewM()
        }
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.bidTableView.delegate = self
        self.bidTableView.dataSource = self
        self.bidTableView.separatorStyle = .none
        self.bidTableView.tableHeaderView =  self.headerView
        
        if let dataSource = self.viewModel?.getBidSubmittedDataSource(tenderDetails: self.tenderDetails) {
            self.bidDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.bidTableView.reloadData()
            }
        }
    }
    
    private func registerNibs() {
        self.bidTableView.register(TenderDetailsTitleCell.self)
        self.bidTableView.register(TenderCarDetailsCell.self)
        self.bidTableView.register(AdditionalInfoCell.self)
        self.bidTableView.register(SServiceDetailsCell.self)
        self.bidTableView.register(ThreeImageCell.self)        
    }
    
}
