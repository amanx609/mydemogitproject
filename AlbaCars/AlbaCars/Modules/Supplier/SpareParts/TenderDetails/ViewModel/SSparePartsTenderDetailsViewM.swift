//
//  SSparePartsTenderDetailsViewM.swift
//  AlbaCars
//
//  Created by Narendra on 2/12/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//


import Foundation
import UIKit

protocol SSparePartsTenderDetailsViewModeling {
    func getTenderDetailsDataSource(tenderDetails: SupplierTender?) -> [CellInfo]
    func requestBidAPI(bidDetails: [CellInfo],tenderDetails: SupplierTender?, completion: @escaping (Bool) -> Void)
}

class SSparePartsTenderDetailsViewM: SSparePartsTenderDetailsViewModeling {
    
    func getTenderDetailsDataSource(tenderDetails: SupplierTender?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Details Title
        let carTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Car Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(carTitleCell)
        
        //Car Details
        var carDetailsInfo = [String: AnyObject]()
        carDetailsInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        let carCell = CellInfo(cellType: .TenderCarDetailsCell, placeHolder:"Car Details".localizedString(), value:"" ,info:carDetailsInfo, height: Constants.CellHeightConstants.height_115)
        array.append(carCell)
        
        //Tender Details Title
        let tenderTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Tender Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(tenderTitleCell)
        
        //Part Type
        let partTypeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Part Type :".localizedString(), value:Helper.getPartType(statusCode: Helper.toInt(tenderDetails?.partType)),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(partTypeCell)
        
        //Part Number
        let partNumberCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Part Number : ".localizedString(), value:Helper.toString(object: tenderDetails?.partNumber),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(partNumberCell)
        
        //Part Name
        let partNameCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Part Name : ".localizedString(), value:Helper.toString(object: tenderDetails?.partName),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(partNameCell)
        
        //Part Description
        let descriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Part Description".localizedString(), value: Helper.toString(object: tenderDetails?.partDescription), info: nil, height: UITableView.automaticDimension)
        array.append(descriptionCell)
        
        //Requested Date
        let requestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedDateCell)
        
        //Requested Time
        let requestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedTimeCell)
        
        //Delivery Location
        let deliveryLocationCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Delivery Location".localizedString(), value: Helper.toString(object: tenderDetails?.deliveryLocation), info: nil, height: UITableView.automaticDimension)
        array.append(deliveryLocationCell)
        
        if let totalImage = tenderDetails?.images, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info:imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }
        
        //Enter Bid Details
        let bidTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Enter Bid Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(bidTitleCell)
        
        let bidDataSource = self.getEnterBidDataSource()
        var height: CGFloat = 35.0
        for bidData in bidDataSource {
            height = height + bidData.height
        }
        
        var enterBidInfo = [String: AnyObject]()
        enterBidInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        enterBidInfo[Constants.UIKeys.itemsCovered] = bidDataSource as AnyObject
        let enterBidCell = CellInfo(cellType: .EnterBidDetailsCell, placeHolder:"", value:"" ,info:enterBidInfo, height: height)
        array.append(enterBidCell)
        
        return array
    }
    
    func getEnterBidDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        let daysRequiredCell = CellInfo(cellType: .DaysRequiredCell, placeHolder:"", value:"1" ,info:nil, height:Constants.CellHeightConstants.height_50)
        array.append(daysRequiredCell)
        
        var dateInfo = [String: AnyObject]()
        dateInfo[Constants.UIKeys.date] = true as AnyObject
        let dateCell = CellInfo(cellType: .JobCell, placeHolder:"Date for Job".localizedString(), value:"" ,info:dateInfo, height:Constants.CellHeightConstants.height_60)
        array.append(dateCell)
        
        var timeInfo = [String: AnyObject]()
        timeInfo[Constants.UIKeys.date] = false as AnyObject
        let timeCell = CellInfo(cellType: .JobCell, placeHolder:"Time for Job".localizedString(), value:"" ,info:timeInfo, height:Constants.CellHeightConstants.height_60)
        array.append(timeCell)
        
        //Part Type
        var typeInfo = [String: AnyObject]()
        typeInfo[Constants.UIKeys.cellInfo] =  DropDownType.partType as AnyObject
        let typeCell = CellInfo(cellType: .RecoveryTypeCell, placeHolder:"Part Type".localizedString(), value:"" ,info:typeInfo, height:Constants.CellHeightConstants.height_60)
        array.append(typeCell)
        
        //Part Name
        var partNameInfo = [String: AnyObject]()
        partNameInfo[Constants.UIKeys.inputType] = TextInputType.partName as AnyObject
        let partNameCell = CellInfo(cellType: .RecoveryTypeCell, placeHolder:"Part Name".localizedString(), value:"" ,info:partNameInfo, height:Constants.CellHeightConstants.height_60)
        array.append(partNameCell)
        
        //Vehicle Images Cell
        let vehicleImagesCell = CellInfo(cellType: .UploadVehicleImagesCell, placeHolder: "", value: StringConstants.Text.selectCarModel.localizedString(), info: [:], height: Constants.CellHeightConstants.height_360)
        array.append(vehicleImagesCell)
        
        let additionalInfoCell = CellInfo(cellType: .EnterAdditionalInfoCell, placeHolder:"Additional Information".localizedString(), value:"" ,info:nil, height:Constants.CellHeightConstants.height_110)
        array.append(additionalInfoCell)
        
        var completedInfo = [String: AnyObject]()
        completedInfo[Constants.UIKeys.firstValue] = true as AnyObject
        completedInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "checked_circle")
        let completedCell = CellInfo(cellType: .TenderCompletedCell, placeHolder: "I will provide Delivery".localizedString(), value: "", info: completedInfo, height: Constants.CellHeightConstants.height_30)
        array.append(completedCell)
        
        let amountCell = CellInfo(cellType: .BidAmountCell, placeHolder:"Enter Bid Amount".localizedString(), value:"" ,info:nil, height:Constants.CellHeightConstants.height_70)
        array.append(amountCell)
        
        return array
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
    func validateBidData(bidDetails: [CellInfo]) -> Bool {
        var isValid = true
        var message = ""
        if let days = bidDetails[0].value, days.isEmpty {
            message = "Please add required day for the Service"
            isValid = false
        } else if let date = bidDetails[1].value, date.isEmpty  {
            message = "Please select date."
            isValid = false
        } else if let time = bidDetails[2].value, time.isEmpty {
            message = "Please select time."
            isValid = false
        }  else if let recovery = bidDetails[3].value, recovery.isEmpty {
            message = "Please select part type"
            isValid = false
        }  else if let recovery = bidDetails[4].value, recovery.isEmpty {
            message = "Please enter part name"
            isValid = false
        } else if let recovery = bidDetails[8].value, recovery.isEmpty {
            message = "Please enter bid amount."
            isValid = false
        }
        
        if !isValid {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: message.localizedString())
        }
        return isValid
    }
    
    private func uploadImages(arrData: [CellInfo], completion: @escaping(([String]) -> Void)) {
        var imagesToUpload: [UIImage] = []
        if let imagesInfo = arrData[5].info {
            if let firstImage = imagesInfo[Constants.UIKeys.firstImg] as? UIImage {
                imagesToUpload.append(firstImage)
            }
            if let secondImage = imagesInfo[Constants.UIKeys.secondImg] as? UIImage {
                imagesToUpload.append(secondImage)
            }
            if let thirdImage = imagesInfo[Constants.UIKeys.thirdImg] as? UIImage {
                imagesToUpload.append(thirdImage)
            }
            if let fourthImage = imagesInfo[Constants.UIKeys.fourthImg] as? UIImage {
                imagesToUpload.append(fourthImage)
            }
            if imagesToUpload.count > 0 {
                S3Manager.sharedInstance.uploadImages(images: imagesToUpload, imageQuality: .medium, progress: nil) { (imageUrls, imageNames, error) in
                    if let uploadedImages = imageUrls as? [String] {
                        completion(uploadedImages)
                    }
                }
            } else {
                completion([])
            }
        } else {
            completion([])
        }
    }
    
    func getBidParams(bidDetails: [CellInfo],tenderDetails: SupplierTender?,images: [String])-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.tenderId] = Helper.toInt(tenderDetails?.id) as AnyObject
        params[ConstantAPIKeys.vehicleId] = Helper.toInt(tenderDetails?.vehicleId) as AnyObject
        params[ConstantAPIKeys.serviceDays] = bidDetails[0].value as AnyObject
        params[ConstantAPIKeys.jobDate] = bidDetails[1].info?[Constants.UIKeys.firstValue] as AnyObject
        params[ConstantAPIKeys.jobTime] = bidDetails[2].info?[Constants.UIKeys.firstValue] as AnyObject
        params[ConstantAPIKeys.partType] = bidDetails[3].info?[Constants.UIKeys.id] as AnyObject
        params[ConstantAPIKeys.partName] = bidDetails[4].value as AnyObject
        params[ConstantAPIKeys.images] = images as AnyObject
        params[ConstantAPIKeys.additionalInfo] = bidDetails[6].value as AnyObject
        params[ConstantAPIKeys.isDelivery] = Helper.toInt(bidDetails[7].info?[Constants.UIKeys.firstValue]) as AnyObject
        params[ConstantAPIKeys.bidAmt] = bidDetails[8].value as AnyObject
        return params
    }
    
    func requestBidAPI(bidDetails: [CellInfo],tenderDetails: SupplierTender?, completion: @escaping (Bool) -> Void) {
        if self.validateBidData(bidDetails: bidDetails) {
            
            self.uploadImages(arrData: bidDetails) { (uploadedImages) in
                let params = self.getBidParams(bidDetails: bidDetails, tenderDetails: tenderDetails, images: uploadedImages)
                print(params)
                APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .submitBid(param: params))) { (response, success) in
                    if success {
                        if let safeResponse =  response as? [String: AnyObject],
                            let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                        {
                            completion(true)
                        }
                    }
                }
            }
            
        }
    }
}
