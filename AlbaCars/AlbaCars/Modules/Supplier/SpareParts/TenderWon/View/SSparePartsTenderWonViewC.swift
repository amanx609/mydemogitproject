//
//  SSparePartsTenderWonViewC.swift
//  AlbaCars
//
//  Created by Narendra on 2/12/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SSparePartsTenderWonViewC: BaseViewC {

    //MARK: - IBOutlets
    @IBOutlet weak var tenderWonTableView: UITableView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var requestPaymentButton: UIButton!
    
    //MARK: - Variables
    var viewModel: SSparePartsTenderWonViewModeling?
    var tenderWonDataSource: [CellInfo] = []
    var tenderDetails: SupplierTender?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Tender Won".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setUpTableView()
        self.setupView()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = SSparePartsTenderWonViewM()
        }
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.tenderWonTableView.delegate = self
        self.tenderWonTableView.dataSource = self
        self.tenderWonTableView.separatorStyle = .none
        self.tenderWonTableView.tableHeaderView = TenderWonHeaderView.inistancefromNib()

        if let dataSource = self.viewModel?.getTenderWonDataSource(tenderDetails: self.tenderDetails) {
            self.tenderWonDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.tenderWonTableView.reloadData()
            }
        }
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.requestPaymentButton.setTitle("Request Payment".localizedString(), for: .normal)
    }
    
    private func registerNibs() {
        self.tenderWonTableView.register(RDContactInfoCell.self)
        self.tenderWonTableView.register(VIContactInfoCell.self)
        self.tenderWonTableView.register(TenderDetailsTitleCell.self)
        self.tenderWonTableView.register(TenderCarDetailsCell.self)
        self.tenderWonTableView.register(AdditionalInfoCell.self)
        self.tenderWonTableView.register(TenderWonInfoCell.self)
        self.tenderWonTableView.register(TenderCompletedCell.self)
        self.tenderWonTableView.register(ThreeImageCell.self)
    }
    
    //MARK: - API Methods
    func requestPaymnetAPI() {
        var params = APIParams()
//        params[ConstantAPIKeys.totalAmmount] = Helper.toString(object:tenderDetails?.bidPrice) as AnyObject
//        params[ConstantAPIKeys.serviceCategoryId] = Helper.toString(object:PaymentServiceCategoryId.smartTenders.rawValue) as AnyObject
        params[ConstantAPIKeys.bidId] = Helper.toString(object: tenderDetails?.id) as AnyObject
        let tenderWonViewM:STenderWonViewM = STenderWonViewM()
        tenderWonViewM.requestPaymentAPI(param: params, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            sSelf.navigationController?.popViewController(animated: true)
            // Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Payment added Successfully".localizedString())
        })
    }
    
    //MARK: - IBActions
    @IBAction func tapRequestPaymentButton(_ sender: UIButton) {
        self.requestPaymnetAPI()
    }

}
