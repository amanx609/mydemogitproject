//
//  SSparePartsJobCompletetViewM.swift
//  AlbaCars
//
//  Created by Narendra on 2/12/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SSparePartsJobCompletetViewModeling {
    func getJobCompletedDataSource(tenderDetails: SupplierTender?) -> [CellInfo]
}

class SSparePartsJobCompletetViewM: SSparePartsJobCompletetViewModeling {
    
    func getJobCompletedDataSource(tenderDetails: SupplierTender?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Details Title
        let carTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Car Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(carTitleCell)
        
        //Car Details
        var carDetailsInfo = [String: AnyObject]()
        carDetailsInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        let carCell = CellInfo(cellType: .TenderCarDetailsCell, placeHolder:"Car Details".localizedString(), value:"" ,info:carDetailsInfo, height: Constants.CellHeightConstants.height_115)
        array.append(carCell)
        
        //Tender Details Title
        let tenderTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Tender Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(tenderTitleCell)
        
        //Part Type
        let partTypeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Part Type :".localizedString(), value:Helper.getPartType(statusCode: Helper.toInt(tenderDetails?.tenderPartType)),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(partTypeCell)
        
        //Part Number
        let partNumberCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Part Number : ".localizedString(), value:Helper.toString(object: tenderDetails?.tenderPartNumber),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(partNumberCell)
        
        //Part Name
        let partNameCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Part Name : ".localizedString(), value:Helper.toString(object: tenderDetails?.tenderPartName),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(partNameCell)
        
        //Part Description
        let descriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Part Description".localizedString(), value: Helper.toString(object: tenderDetails?.tenderPartDescription), info: nil, height: UITableView.automaticDimension)
        array.append(descriptionCell)
        
        //Requested Date
        let requestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedDateCell)
        
        //Requested Time
        let requestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedTimeCell)
        
        //Delivery Location
        let deliveryLocationCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Delivery Location".localizedString(), value: Helper.toString(object: tenderDetails?.tenderDeliveryLocation), info: nil, height: UITableView.automaticDimension)
        array.append(deliveryLocationCell)
        
        if let totalImage = tenderDetails?.images, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info:imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }
        
        //Bid Details
        let bidTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Bid Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(bidTitleCell)
        
        //Days
        let daysCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Days Required for the Service :".localizedString(), value:"\(Helper.toString(object: tenderDetails?.requireDaysService)) Days" ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(daysCell)
        
         //Providing Delivery
               let pickUpCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Providing Delivery : ".localizedString(), value:Helper.toString(object: tenderDetails?.getBidDeliveryProvided()) ,info:nil, height: Constants.CellHeightConstants.height_25)
               array.append(pickUpCell)
        
        //Part Type
        let bidPartTypeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Part Type :".localizedString(), value:Helper.getPartType(statusCode: Helper.toInt(tenderDetails?.bidPartType)),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidPartTypeCell)
        
        //Part Name
        let bidPartNameCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Part Name : ".localizedString(), value:Helper.toString(object: tenderDetails?.bidPartName),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidPartNameCell)
        
        //Requested Date
        let bidRequestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedDateCell)
        
        let bidRequestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedTimeCell)
        
        //Additional Information
        let bidDescriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.bidAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(bidDescriptionCell)
        
        let bidPriceCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Bid Price :".localizedString(), value:Helper.toString(object: tenderDetails?.formattedPrice()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidPriceCell)
        
        if let totalImage = tenderDetails?.bidImages, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info:imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }
        
        //ContactInfoCell
        var contactInfoInfo = [String: AnyObject]()
        contactInfoInfo[Constants.UIKeys.image] = tenderDetails?.image as AnyObject
        let contactInfoCell = CellInfo(cellType: .RDContactInfoCell, placeHolder: Helper.toString(object: tenderDetails?.name), value: Helper.toString(object: tenderDetails?.pickupLocation), info: contactInfoInfo, height: UITableView.automaticDimension)
        array.append(contactInfoCell)
        
        let secondBidCell = CellInfo(cellType: .VIContactInfoCell, placeHolder: Helper.toString(object: tenderDetails?.mobile), value: Helper.toString(object: tenderDetails?.email), info: nil, height: Constants.CellHeightConstants.height_100)
        array.append(secondBidCell)
        
        return array
    }
}
