//
//  SCarServicingJobCompletedViewM.swift
//  AlbaCars
//
//  Created by Narendra on 2/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SCarServicingJobCompletedViewModeling {
    func getJobCompletedDataSource(tenderDetails: SupplierTender?) -> [CellInfo]
}

class SCarServicingJobCompletedViewM: SCarServicingJobCompletedViewModeling {
    
    func getJobCompletedDataSource(tenderDetails: SupplierTender?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Details Title
        let carTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Car Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(carTitleCell)
        
        //Car Details
        var carDetailsInfo = [String: AnyObject]()
        carDetailsInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        let carCell = CellInfo(cellType: .TenderCarDetailsCell, placeHolder:"Car Details".localizedString(), value:"" ,info:carDetailsInfo, height: Constants.CellHeightConstants.height_115)
        array.append(carCell)
        
        //Tender Details Title
        let tenderTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Tender Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(tenderTitleCell)
        
        // Service Details
        var serviceDetailsInfo = [String: AnyObject]()
        serviceDetailsInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        let serviceDetailsCell = CellInfo(cellType: .SServiceDetailsCell, placeHolder:"", value:"" ,info:serviceDetailsInfo, height: UITableView.automaticDimension)
        array.append(serviceDetailsCell)
        
        //Engine Size
        let engineSizCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Engine Size :".localizedString(), value:Helper.toString(object: tenderDetails?.engine) + " C",info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(engineSizCell)
        
        //Front Brake Pads
        let frontBrakePads = Helper.toBool(tenderDetails?.getServiceStatus(value: Helper.toString(object:ServiceOptions.frontBrakePads.rawValue)))
        let frontBrakeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Front Brake Pads :".localizedString(), value: frontBrakePads == true ? "Yes".localizedString() : "No".localizedString() ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(frontBrakeCell)
        
        //Requested Date
        let requestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedDateCell)
        
        //Requested Time
        let requestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedTimeCell)
        
        //Additional Information
        let descriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.tenderAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(descriptionCell)
        
        //Location
        let locationCell = CellInfo(cellType: .VITenderLocationCell, placeHolder:Helper.toString(object: tenderDetails?.pickupLocation), value:Helper.toString(object: tenderDetails?.dropoffLocation) ,info:nil, height: Constants.CellHeightConstants.height_100)
        array.append(locationCell)
        
        //Bid Details
        let bidTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Bid Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(bidTitleCell)
        
        //Days
        let daysCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Days Required for the Service :".localizedString(), value:"\(Helper.toString(object: tenderDetails?.requireDaysService)) Days" ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(daysCell)
        
        //PickUp
        let pickUpCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Providing PickUp and DropOff :".localizedString(), value:Helper.toString(object: tenderDetails?.getPickupDropoff()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(pickUpCell)
        
        //Type of Service
        let typeofServiceCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Type of Service : ".localizedString(), value:Helper.toString(object: tenderDetails?.bidTypeOfService) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(typeofServiceCell)
        
        //Requested Date
        let bidRequestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedDateCell)
        
        let bidRequestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getJobTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidRequestedTimeCell)
        
        //Additional Information
        let bidDescriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.bidAdditionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(bidDescriptionCell)
        
        let bidPriceCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Bid Price :".localizedString(), value:Helper.toString(object: tenderDetails?.formattedPrice()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(bidPriceCell)
        
        //ContactInfoCell
        var contactInfoInfo = [String: AnyObject]()
        contactInfoInfo[Constants.UIKeys.image] = tenderDetails?.image as AnyObject
        let contactInfoCell = CellInfo(cellType: .RDContactInfoCell, placeHolder: Helper.toString(object: tenderDetails?.name), value: Helper.toString(object: tenderDetails?.pickupLocation), info: contactInfoInfo, height: UITableView.automaticDimension)
        array.append(contactInfoCell)
        
        let secondBidCell = CellInfo(cellType: .VIContactInfoCell, placeHolder: Helper.toString(object: tenderDetails?.mobile), value: Helper.toString(object: tenderDetails?.email), info: nil, height: Constants.CellHeightConstants.height_100)
        array.append(secondBidCell)
        
        return array
    }
}
