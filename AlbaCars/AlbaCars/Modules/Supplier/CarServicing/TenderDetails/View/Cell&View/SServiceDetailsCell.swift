//
//  SServiceDetailsCell.swift
//  AlbaCars
//
//  Created by Narendra on 2/8/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SServiceDetailsCell: BaseTableViewCell, ReusableView, NibLoadableView  {
    
    //MARK: - IBOutlets
    @IBOutlet weak var typeOfServiceLabel: UILabel!
    @IBOutlet weak var brakeDiscsLabel: UILabel!
    @IBOutlet weak var numberOfCyllindersLabel: UILabel!
    @IBOutlet weak var rearBrakeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        
        if let tender = cellInfo.info?[Constants.UIKeys.cellInfo] as? SupplierTender {
            var typeOfService = Helper.toString(object: tender.typeOfService)
            if typeOfService.isEmpty {
                 typeOfService = Helper.toString(object: tender.tenderTypeOfService)
            }
            
            self.typeOfServiceLabel.attributedText = String.getAttributedText(firstText: "Type of Service : ".localizedString(), secondText:typeOfService, firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.numberOfCyllindersLabel.attributedText = String.getAttributedText(firstText: "Number of Cyllinders : ".localizedString(), secondText:Helper.toString(object: tender.cylinders), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
            let rearBrakePads = Helper.toBool(tender.getServiceStatus(value: Helper.toString(object:ServiceOptions.rearBrakePads.rawValue)))
            self.rearBrakeLabel.attributedText = String.getAttributedText(firstText: "Rear Brake Pads : ".localizedString(), secondText:rearBrakePads == true ? "Yes".localizedString() : "No".localizedString(), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
            let brakeDiscs = Helper.toBool(tender.getServiceStatus(value: Helper.toString(object:ServiceOptions.brakeDiscs.rawValue)))
            self.brakeDiscsLabel.attributedText = String.getAttributedText(firstText: "Brake Discs : ".localizedString(), secondText:brakeDiscs == true ? "Yes".localizedString() : "No".localizedString(), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        }
    }
}
