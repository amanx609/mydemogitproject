//
//  SCarServicingActiveTenderCell.swift
//  AlbaCars
//
//  Created by Narendra on 2/8/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol SCarServicingActiveTenderCellDelegate: class {
    func didTapViewDetailsCell(cell: SCarServicingActiveTenderCell)
}

class SCarServicingActiveTenderCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var viewDetailsButton: UIButton!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var serviceTypeLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var liveLabel: UILabel!
    
    //MARK: - Variables
    weak var delegate: SCarServicingActiveTenderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.viewDetailsButton.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.bgView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
    }
    
    //MARK: - Public Methods
    func configureView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        
        //Timer
        if let timerDuration = supplierTender.timerDuration {
            self.timeLabel.text = timerDuration.convertToTimerFormat()
        }
        
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Service Type".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.typeOfService), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.serviceLabel.attributedText = String.getAttributedText(firstText:"Service".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.getServiceOptions()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
    }
    
    func configureSparePartsView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        
        //Timer
        if let timerDuration = supplierTender.timerDuration {
            self.timeLabel.text = timerDuration.convertToTimerFormat()
        }
        
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Part Type".localizedString(), secondText:"\n"+Helper.getPartType(statusCode: Helper.toInt(supplierTender.partType)), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.serviceLabel.attributedText = String.getAttributedText(firstText:"Part Name".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.partName), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
    }
    
    func configureWheelsView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        
        //Timer
        if let timerDuration = supplierTender.timerDuration {
            self.timeLabel.text = timerDuration.convertToTimerFormat()
        }
        
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Service Type".localizedString(), secondText:"\n"+Helper.getWheelType(statusCode: Helper.toInt(supplierTender.wheelType)), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        
        if Helper.toInt(supplierTender.wheelType) == 0 {
            
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"Wheel Size".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.wheelSize), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        } else {
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"Service".localizedString(), secondText:"\n"+Helper.getRimType(statusCode: Helper.toInt(supplierTender.rimServiceType)), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        }
        
    }
    
    func configureWindowTintingView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        
        //Timer
        if let timerDuration = supplierTender.timerDuration {
            self.timeLabel.text = timerDuration.convertToTimerFormat()
        }
        
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Tint Color".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.tintColor), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.serviceLabel.attributedText = String.getAttributedText(firstText:"Tint %age".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.tintPercentage), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
    }
    
    func configureBodyWorkView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        
        //Timer
        if let timerDuration = supplierTender.timerDuration {
            self.timeLabel.text = timerDuration.convertToTimerFormat()
        }
        
        let bodyWorkType = Helper.toInt(supplierTender.bodyServiceType)
        
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Service Type".localizedString(), secondText:"\n"+Helper.getBodyWorkType(statusCode: bodyWorkType), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        
        if bodyWorkType == BodyWorkType.panelPainting.rawValue {
            
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"Paint Color".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.paintColor), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
            
        } else if bodyWorkType == BodyWorkType.paintlessDentRemoval.rawValue {
            
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"No.of Dents".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.noOfDents), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        } else {
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"Paint Site".localizedString(), secondText:"\n"+Helper.getPaitSite(paintSiteArray: supplierTender.paintSiteArr), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        }
    }
    
    func configureCarDetailingView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        
        //Timer
        if let timerDuration = supplierTender.timerDuration {
            self.timeLabel.text = timerDuration.convertToTimerFormat()
        }
        
        let carDetailingServiceType = Helper.toInt(supplierTender.carDetailingServiceType)
        
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Service Type".localizedString(), secondText:"\n"+Helper.getCarDetailingServiceType(statusCode: carDetailingServiceType), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        
        if carDetailingServiceType == CarDetailingServiceType.detailing.rawValue {
            
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"Service".localizedString(), secondText:"\n"+Helper.getDetailingService(statusCode: Helper.toInt(supplierTender.carDetailingOptions)), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        } else {
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"Additional Information".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.additionalInformation), firstTextColor: .blackTextColor, secondTextColor: .blackTextColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        }

    }
        
    func configureBankValuatioView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        
        //Timer
        if let timerDuration = supplierTender.timerDuration {
            self.timeLabel.text = timerDuration.convertToTimerFormat()
        }
        
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Bank Name".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.bankName), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.serviceLabel.attributedText = String.getAttributedText(firstText:"Price".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.formattedCarPrice()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        
    }
    
    func configureInsuranceView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        
        //Timer
        if let timerDuration = supplierTender.timerDuration {
            self.timeLabel.text = timerDuration.convertToTimerFormat()
        }
        
        let insuranceType = Helper.toInt(supplierTender.typeOfInsurance)
        
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Insurance Type".localizedString(), secondText:"\n"+Helper.getInsuranceType(statusCode: insuranceType), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        
        self.serviceLabel.attributedText = String.getAttributedText(firstText:"Value of Vehicle".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.formattedVehiclePrice()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
    }
    
    //MARK: - IBActions
    @IBAction func viewDetailsButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapViewDetailsCell(cell: self)
        }
    }
    
}
