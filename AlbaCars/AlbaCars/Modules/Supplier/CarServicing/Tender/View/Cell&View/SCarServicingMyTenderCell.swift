//
//  SCarServicingMyTenderCell.swift
//  AlbaCars
//
//  Created by Narendra on 2/8/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol SCarServicingMyTenderCellDelegate: class {
    func didTapViewDetailsCell(cell: SCarServicingMyTenderCell)
}

class SCarServicingMyTenderCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var viewDetailsButton: UIButton!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var requestedDateLabel: UILabel!
    @IBOutlet weak var requestedTimeLabel: UILabel!
    @IBOutlet weak var bidPriceLabel: UILabel!
    @IBOutlet weak var bidStatusLabel: UILabel!
    @IBOutlet weak var bidStatusView: UIView!
    @IBOutlet weak var serviceTypeLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var bidDetailsView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceTitleLabel: UILabel!
    
    //MARK: - Variables
    weak var delegate: SCarServicingMyTenderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.bidStatusView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.viewDetailsButton.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.bgView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        self.bidDetailsView.isHidden = true
    }
    
    //MARK: - Public Methods
    func configureView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        self.requestedDateLabel.attributedText = String.getAttributedText(firstText: "Requested Date : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedDate()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.requestedTimeLabel.attributedText = String.getAttributedText(firstText: "Requested Time : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedTime()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.bidPriceLabel.text = "AED :".localizedString() + " \(Helper.toString(object: supplierTender.bidPrice))"
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Service Type : ".localizedString(), secondText:Helper.toString(object: supplierTender.typeOfService), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.serviceLabel.attributedText = String.getAttributedText(firstText:"Service : ".localizedString(), secondText:Helper.toString(object: supplierTender.getServiceOptions()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        let (title, color) = Helper.getTenderBidStatus(statusCode: Helper.toInt(supplierTender.status))
        self.bidStatusLabel.text = title
        self.bidStatusView.backgroundColor = color
    }
    
    //MARK: - Public Methods
    func configureSparePartsView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        self.requestedDateLabel.attributedText = String.getAttributedText(firstText: "Requested Date : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedDate()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.requestedTimeLabel.attributedText = String.getAttributedText(firstText: "Requested Time : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedTime()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.bidPriceLabel.text = "AED :".localizedString() + " \(Helper.toString(object: supplierTender.bidPrice))"
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Part Type : ".localizedString(), secondText:Helper.getPartType(statusCode: Helper.toInt(supplierTender.partType)), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.serviceLabel.attributedText = String.getAttributedText(firstText:"Part Name : ".localizedString(), secondText:Helper.toString(object: supplierTender.partName), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        let (title, color) = Helper.getTenderBidStatus(statusCode: Helper.toInt(supplierTender.status))
        self.bidStatusLabel.text = title
        self.bidStatusView.backgroundColor = color
        
    }
    
    //MARK: - Public Methods
    func configureWheelsView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        self.requestedDateLabel.attributedText = String.getAttributedText(firstText: "Requested Date : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedDate()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.requestedTimeLabel.attributedText = String.getAttributedText(firstText: "Requested Time : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedTime()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.bidPriceLabel.text = "AED :".localizedString() + " \(Helper.toString(object: supplierTender.bidPrice))"
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Service Type : ".localizedString(), secondText:Helper.getWheelType(statusCode: Helper.toInt(supplierTender.wheelServiceType)), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
       // self.serviceLabel.attributedText = String.getAttributedText(firstText:"Wheel Size : ".localizedString(), secondText:Helper.toString(object: supplierTender.wheelSize), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        let (title, color) = Helper.getTenderBidStatus(statusCode: Helper.toInt(supplierTender.status))
        self.bidStatusLabel.text = title
        self.bidStatusView.backgroundColor = color
        
        if Helper.toInt(supplierTender.wheelType) == 0 {
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"Wheel Size : ".localizedString(), secondText:"\n"+Helper.toString(object: supplierTender.wheelSize), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        } else {
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"Service : ".localizedString(), secondText:"\n"+Helper.getRimType(statusCode: Helper.toInt(supplierTender.rimServiceType)), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        }
        
    }
    
    func configureWindowTintingView(supplierTender: SupplierTender) {
        self.carNameLabel.text = supplierTender.carTitle()
        self.requestedDateLabel.attributedText = String.getAttributedText(firstText: "Requested Date : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedDate()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.requestedTimeLabel.attributedText = String.getAttributedText(firstText: "Requested Time : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedTime()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.bidPriceLabel.text = "AED :".localizedString() + " \(Helper.toString(object: supplierTender.bidPrice))"
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Tint Color : ".localizedString(), secondText:Helper.toString(object: supplierTender.tintColor), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.serviceLabel.attributedText = String.getAttributedText(firstText:"Tint %age : ".localizedString(), secondText:Helper.toString(object: supplierTender.tintPercentage), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        let (title, color) = Helper.getTenderBidStatus(statusCode: Helper.toInt(supplierTender.status))
        self.bidStatusLabel.text = title
        self.bidStatusView.backgroundColor = color
        
    }
    
    func configureBodyWorkView(supplierTender: SupplierTender) {
    
        self.bidDetailsView.isHidden = true

        self.carNameLabel.text = supplierTender.carTitle()
        self.requestedDateLabel.attributedText = String.getAttributedText(firstText: "Requested Date : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedDate()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.requestedTimeLabel.attributedText = String.getAttributedText(firstText: "Requested Time : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedTime()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.bidPriceLabel.text = "AED :".localizedString() + " \(Helper.toString(object: supplierTender.bidPrice))"
        let (title, color) = Helper.getTenderBidStatus(statusCode: Helper.toInt(supplierTender.status))
        self.bidStatusLabel.text = title
        self.bidStatusView.backgroundColor = color
        
        let bodyWorkType = Helper.toInt(supplierTender.bodyServiceType)
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Service Type :  ".localizedString(), secondText:Helper.getBodyWorkType(statusCode: bodyWorkType), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        
        if bodyWorkType == BodyWorkType.panelPainting.rawValue {
            
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"Paint Color : ".localizedString(), secondText:Helper.toString(object: supplierTender.paintColor), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
            
        } else if bodyWorkType == BodyWorkType.paintlessDentRemoval.rawValue {
            
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"No.of Dents : ".localizedString(), secondText:Helper.toString(object: supplierTender.noOfDents), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        } else {
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"Paint Site : ".localizedString(), secondText:Helper.getPaitSite(paintSiteArray: supplierTender.paintSiteArr), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        }
        
        switch Helper.toInt(supplierTender.status) {
        case TenderBidStatus.rejected.rawValue:
            self.bidDetailsView.isHidden = false
            self.priceTitleLabel.text = "Winning Bid".localizedString()
            self.priceLabel.text = "AED ".localizedString() + " \(Helper.toString(object: supplierTender.winAmount))"

        case TenderBidStatus.completed.rawValue:
            self.bidDetailsView.isHidden = false
            self.priceTitleLabel.text = "Payment Pending".localizedString()
            self.priceLabel.text = "AED ".localizedString() + " \(Helper.toString(object: supplierTender.bidPrice))"
            
        default:
            print("")
        }
        
    }
    
    func configureCarDetailingView(supplierTender: SupplierTender) {
    
        self.bidDetailsView.isHidden = true

        self.carNameLabel.text = supplierTender.carTitle()
        self.requestedDateLabel.attributedText = String.getAttributedText(firstText: "Requested Date : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedDate()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.requestedTimeLabel.attributedText = String.getAttributedText(firstText: "Requested Time : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedTime()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.bidPriceLabel.text = "AED :".localizedString() + " \(Helper.toString(object: supplierTender.bidPrice))"
        let (title, color) = Helper.getTenderBidStatus(statusCode: Helper.toInt(supplierTender.status))
        self.bidStatusLabel.text = title
        self.bidStatusView.backgroundColor = color
        
        let carDetailingServiceType = Helper.toInt(supplierTender.carDetailingServiceType)
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Service Type : ".localizedString(), secondText:Helper.getCarDetailingServiceType(statusCode: carDetailingServiceType), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)

       if carDetailingServiceType == CarDetailingServiceType.detailing.rawValue {
            
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"Service : ".localizedString(), secondText:Helper.getDetailingService(statusCode: Helper.toInt(supplierTender.carDetailingOptions)), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        } else {
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"Additional Information : ".localizedString(), secondText:Helper.toString(object: supplierTender.additionalInformation), firstTextColor: .blackTextColor, secondTextColor: .blackTextColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        }
        
        switch Helper.toInt(supplierTender.status) {
        case TenderBidStatus.rejected.rawValue:
            self.bidDetailsView.isHidden = false
            self.priceTitleLabel.text = "Winning Bid".localizedString()
            self.priceLabel.text = "AED ".localizedString() + " \(Helper.toString(object: supplierTender.winAmount))"

        case TenderBidStatus.completed.rawValue:
            self.bidDetailsView.isHidden = false
            self.priceTitleLabel.text = "Payment Pending".localizedString()
            self.priceLabel.text = "AED ".localizedString() + " \(Helper.toString(object: supplierTender.bidPrice))"
        default:
            print("")
        }
        
    }
    
    
    func configureBankValuatioView(supplierTender: SupplierTender) {
        
        self.carNameLabel.text = supplierTender.carTitle()
        self.requestedDateLabel.attributedText = String.getAttributedText(firstText: "Requested Date : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedDate()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.requestedTimeLabel.attributedText = String.getAttributedText(firstText: "Requested Time : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedTime()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.bidPriceLabel.text = "AED :".localizedString() + " \(Helper.toString(object: supplierTender.bidPrice))"
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Bank Name : ".localizedString(), secondText:Helper.toString(object: supplierTender.bankName), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.serviceLabel.attributedText = String.getAttributedText(firstText:"Requested Car Value : ".localizedString(), secondText:Helper.toString(object: supplierTender.formattedCarPrice()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        let (title, color) = Helper.getTenderBidStatus(statusCode: Helper.toInt(supplierTender.status))
        self.bidStatusLabel.text = title
        self.bidStatusView.backgroundColor = color
        
    }
    
    func configureInsuranceView(supplierTender: SupplierTender) {
        
        self.carNameLabel.text = supplierTender.carTitle()
        self.requestedDateLabel.attributedText = String.getAttributedText(firstText: "Requested Date : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedDate()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.requestedTimeLabel.attributedText = String.getAttributedText(firstText: "Requested Time : ".localizedString(), secondText:Helper.toString(object: supplierTender.getRequestedTime()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.bidPriceLabel.text = "AED :".localizedString() + " \(Helper.toString(object: supplierTender.bidPrice))"
        let insuranceType = Helper.toInt(supplierTender.typeOfInsurance)
        self.serviceTypeLabel.attributedText = String.getAttributedText(firstText:"Insurance Type : ".localizedString(), secondText:Helper.getInsuranceType(statusCode: insuranceType), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        self.serviceLabel.attributedText = String.getAttributedText(firstText:"Value of Vehicle : ".localizedString(), secondText:Helper.toString(object: supplierTender.formattedCarPrice()), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_13, secondTextSize: .size_13, fontWeight: .Medium)
        let (title, color) = Helper.getTenderBidStatus(statusCode: Helper.toInt(supplierTender.status))
        self.bidStatusLabel.text = title
        self.bidStatusView.backgroundColor = color
        self.bidDetailsView.isHidden = true

        switch Helper.toInt(supplierTender.status) {
        case TenderBidStatus.rejected.rawValue:
            self.bidDetailsView.isHidden = false
            self.priceTitleLabel.text = "Winning Bid".localizedString()
            self.priceLabel.text = "AED ".localizedString() + " \(Helper.toString(object: supplierTender.winAmount))"

        case TenderBidStatus.completed.rawValue:
            self.bidDetailsView.isHidden = false
            self.priceTitleLabel.text = "Payment Pending".localizedString()
            self.priceLabel.text = "AED ".localizedString() + " \(Helper.toString(object: supplierTender.bidPrice))"
        default:
            print("")
        }
        
    }
        
    //MARK: - IBActions
    
    @IBAction func viewDetailsButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapViewDetailsCell(cell: self)
        }
    }
    
}
