//
//  SWheelsTenderDetailsViewC.swift
//  AlbaCars
//
//  Created by Narendra on 2/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SWheelsTenderDetailsViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var timeLeftLabel: UILabel!
    @IBOutlet weak var tenderTableView: UITableView!
    @IBOutlet weak var bidNowButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var headerView: UIView!
    
    //MARK: - Variables
    var viewModel: SWheelsTenderDetailsViewModeling?
    var tenderDataSource: [CellInfo] = []
    var tenderDetails: SupplierTender?
    var durationTimer : Timer?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    deinit {
        self.invalidateTimer()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Tender Details".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setupView()
        self.setUpTableView()
        self.setupTimer()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = SWheelsTenderDetailsViewM()
        }
    }
    
    private func setupView() {
        
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.bidNowButton.setTitle("Bid Now".localizedString(), for: .normal)
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.tenderTableView.delegate = self
        self.tenderTableView.dataSource = self
        self.tenderTableView.separatorStyle = .none
        self.tenderTableView.tableHeaderView =  self.headerView
        
        if let dataSource = self.viewModel?.getTenderDetailsDataSource(tenderDetails: tenderDetails) {
            self.tenderDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.tenderTableView.reloadData()
            }
        }
    }
    
    private func registerNibs() {
        self.tenderTableView.register(TenderDetailsTitleCell.self)
        self.tenderTableView.register(TenderCarDetailsCell.self)
        self.tenderTableView.register(VITenderLocationCell.self)
        self.tenderTableView.register(AdditionalInfoCell.self)
        self.tenderTableView.register(EnterBidDetailsCell.self)
        self.tenderTableView.register(ThreeImageCell.self)
    }
    
    private func setupTimer() {
        self.invalidateTimer()
        self.durationTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTenderDuration), userInfo: nil, repeats: true)
    }
    
    private func invalidateTimer() {
        self.durationTimer?.invalidate()
        self.durationTimer = nil
    }
    
    //MARK: - Selector Methods
    @objc func updateTenderDuration() {
        if var timerDuration = self.tenderDetails?.timerDuration {
            if timerDuration == 0 {
                self.invalidateTimer()
                return
            }
            timerDuration -= 1
            self.timeLeftLabel.text = timerDuration.convertToTimerFormat()
            self.tenderDetails?.timerDuration = timerDuration
        }
    }
    
    //MARK: - API Methods
    func requestBidAPI() {
        
        if let info = self.tenderDataSource.last?.info,
            let itemsCoveredData = info[Constants.UIKeys.itemsCovered] as? [CellInfo]
        {
            self.viewModel?.requestBidAPI(bidDetails: itemsCoveredData, tenderDetails: self.tenderDetails, completion: { (responseData) in
                Threads.performTaskInMainQueue {
                    self.navigationController?.popViewController(animated: true)
                    Alert.showOkAlert(title:StringConstants.Text.AppName, message: "Bid Detail Submitted Successfully.".localizedString())
                }
            })
        }
    }
    
    //MARK: - IBActions
    @IBAction func bidTapped(_ sender: Any) {
        self.requestBidAPI()
    }
}
