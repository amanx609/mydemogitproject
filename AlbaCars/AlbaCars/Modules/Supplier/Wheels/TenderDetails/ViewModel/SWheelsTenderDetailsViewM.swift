//
//  SWheelsTenderDetailsViewM.swift
//  AlbaCars
//
//  Created by Narendra on 2/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SWheelsTenderDetailsViewModeling {
    func getTenderDetailsDataSource(tenderDetails: SupplierTender?) -> [CellInfo]
    func requestBidAPI(bidDetails: [CellInfo],tenderDetails: SupplierTender?, completion: @escaping (Bool) -> Void)
}

class SWheelsTenderDetailsViewM: SWheelsTenderDetailsViewModeling {
    
    func getTenderDetailsDataSource(tenderDetails: SupplierTender?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Details Title
        let carTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Car Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(carTitleCell)
        
        //Car Details
        var carDetailsInfo = [String: AnyObject]()
        carDetailsInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        let carCell = CellInfo(cellType: .TenderCarDetailsCell, placeHolder:"Car Details".localizedString(), value:"" ,info:carDetailsInfo, height: Constants.CellHeightConstants.height_115)
        array.append(carCell)
        
        //Tender Details Title
        let tenderTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Tender Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(tenderTitleCell)
        
        //Service Type
        var serviceType = ""
        let wheelType = Helper.toInt(tenderDetails?.wheelType)
        if wheelType == 0 {
            serviceType = Helper.getWheelType(statusCode: Helper.toInt(tenderDetails?.wheelType))
        } else {
           serviceType = Helper.getRimType(statusCode: Helper.toInt(tenderDetails?.rimServiceType))
        }
        
        let serviceTypeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Service Type : ".localizedString(), value:serviceType,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(serviceTypeCell)
        
        //Wheel Size
        let wheelSizeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Wheel Size : ".localizedString(), value:Helper.toString(object: tenderDetails?.wheelSize),info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(wheelSizeCell)
        
        //Additional Information
        let descriptionCell = CellInfo(cellType: .AdditionalInfoCell, placeHolder: "Additional Information".localizedString(), value: Helper.toString(object: tenderDetails?.additionalInformation), info: nil, height: UITableView.automaticDimension)
        array.append(descriptionCell)
        
        //Requested Date
        let requestedDateCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Date :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedDate()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedDateCell)
        
        //Requested Time
        let requestedTimeCell = CellInfo(cellType: .TenderDetailsAttributedCell, placeHolder:"Requested Time :".localizedString(), value:Helper.toString(object: tenderDetails?.getRequestedTime()) ,info:nil, height: Constants.CellHeightConstants.height_25)
        array.append(requestedTimeCell)
        
        // Location
        let locationCell = CellInfo(cellType: .VITenderLocationCell, placeHolder:Helper.toString(object: tenderDetails?.pickupLocation), value:Helper.toString(object: tenderDetails?.dropoffLocation) ,info:nil, height: Constants.CellHeightConstants.height_100)
        array.append(locationCell)
        
        if let totalImage = tenderDetails?.images, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info:imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }
        
        //Enter Bid Details
        let bidTitleCell = CellInfo(cellType: .TenderDetailsTitleCell, placeHolder:"Enter Bid Details".localizedString(), value:"" ,info:nil, height: Constants.CellHeightConstants.height_50)
        array.append(bidTitleCell)
        
        let bidDataSource = self.getEnterBidDataSource()
        var height: CGFloat = 35.0
        for bidData in bidDataSource {
            height = height + bidData.height
        }
        
        var enterBidInfo = [String: AnyObject]()
        enterBidInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        enterBidInfo[Constants.UIKeys.itemsCovered] = bidDataSource as AnyObject
        let enterBidCell = CellInfo(cellType: .EnterBidDetailsCell, placeHolder:"", value:"" ,info:enterBidInfo, height: height)
        array.append(enterBidCell)
        return array
    }
    
    func getEnterBidDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        let daysRequiredCell = CellInfo(cellType: .DaysRequiredCell, placeHolder:"", value:"1" ,info:nil, height:Constants.CellHeightConstants.height_50)
        array.append(daysRequiredCell)
        
        var dateInfo = [String: AnyObject]()
        dateInfo[Constants.UIKeys.date] = true as AnyObject
        let dateCell = CellInfo(cellType: .JobCell, placeHolder:"Date for Job".localizedString(), value:"" ,info:dateInfo, height:Constants.CellHeightConstants.height_60)
        array.append(dateCell)
        
        var timeInfo = [String: AnyObject]()
        timeInfo[Constants.UIKeys.date] = false as AnyObject
        let timeCell = CellInfo(cellType: .JobCell, placeHolder:"Time for Job".localizedString(), value:"" ,info:timeInfo, height:Constants.CellHeightConstants.height_60)
        array.append(timeCell)
        
        //Service Type
        var typeInfo = [String: AnyObject]()
        typeInfo[Constants.UIKeys.cellInfo] =  DropDownType.wheelsService as AnyObject
        let typeCell = CellInfo(cellType: .RecoveryTypeCell, placeHolder:"Service Type".localizedString(), value:"" ,info:typeInfo, height:Constants.CellHeightConstants.height_60)
        array.append(typeCell)
        
        var serviceInfo = [String: AnyObject]()
        serviceInfo[Constants.UIKeys.cellInfo] =  DropDownType.serviceType as AnyObject
        let serviceCell = CellInfo(cellType: .RecoveryTypeCell, placeHolder:"Service".localizedString(), value:"" ,info:serviceInfo, height:0)
        array.append(serviceCell)
        
        // Wheel Size
        var wheelSizeInfo = [String: AnyObject]()
        wheelSizeInfo[Constants.UIKeys.inputType] = TextInputType.wheelSize as AnyObject
        let wheelSizeCell = CellInfo(cellType: .RecoveryTypeCell, placeHolder:"Wheel Size".localizedString(), value:"" ,info:wheelSizeInfo, height:Constants.CellHeightConstants.height_60)
        array.append(wheelSizeCell)
        
        //Vehicle Images Cell
        let vehicleImagesCell = CellInfo(cellType: .UploadVehicleImagesCell, placeHolder: "", value: StringConstants.Text.selectCarModel.localizedString(), info: [:], height: Constants.CellHeightConstants.height_360)
        array.append(vehicleImagesCell)
        
        let additionalInfoCell = CellInfo(cellType: .EnterAdditionalInfoCell, placeHolder:"Additional Information".localizedString(), value:"" ,info:nil, height:Constants.CellHeightConstants.height_110)
        array.append(additionalInfoCell)
        
        var completedInfo = [String: AnyObject]()
        completedInfo[Constants.UIKeys.firstValue] = true as AnyObject
        completedInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "checked_circle")
        let completedCell = CellInfo(cellType: .TenderCompletedCell, placeHolder: "I will provide PickUp and DropOff".localizedString(), value: "", info: completedInfo, height: Constants.CellHeightConstants.height_30)
        array.append(completedCell)
        
        let amountCell = CellInfo(cellType: .BidAmountCell, placeHolder:"Enter Bid Amount".localizedString(), value:"" ,info:nil, height:Constants.CellHeightConstants.height_70)
        array.append(amountCell)
        
        return array
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
    func validateBidData(bidDetails: [CellInfo]) -> Bool {
        var isValid = true
        var message = ""
        
        let serviceID = Helper.toInt(bidDetails[3].info?[Constants.UIKeys.id])

        if let days = bidDetails[0].value, days.isEmpty {
            message = "Please add required day for the Service"
            isValid = false
        } else if let date = bidDetails[1].value, date.isEmpty  {
            message = "Please select date."
            isValid = false
        } else if let time = bidDetails[2].value, time.isEmpty {
            message = "Please select time."
            isValid = false
        } else if let service = bidDetails[3].value, service.isEmpty {
            message = "Please select service type"
            isValid = false
        } else if serviceID == 1,let service = bidDetails[4].value, service.isEmpty  {
            message = "Please select service name"
            isValid = false
        } else if let size = bidDetails[5].value, size.isEmpty {
            message = "Please enter wheel size"
            isValid = false
        } else if let recovery = bidDetails[9].value, recovery.isEmpty {
            message = "Please enter bid amount."
            isValid = false
        }

        if !isValid {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: message.localizedString())
        }
        return isValid
    }
    
    private func uploadImages(arrData: [CellInfo], completion: @escaping(([String]) -> Void)) {
        var imagesToUpload: [UIImage] = []
        
        if let imagesInfo = arrData.count == 10 ? arrData[6].info : arrData[5].info  {
            if let firstImage = imagesInfo[Constants.UIKeys.firstImg] as? UIImage {
                imagesToUpload.append(firstImage)
            }
            if let secondImage = imagesInfo[Constants.UIKeys.secondImg] as? UIImage {
                imagesToUpload.append(secondImage)
            }
            if let thirdImage = imagesInfo[Constants.UIKeys.thirdImg] as? UIImage {
                imagesToUpload.append(thirdImage)
            }
            if let fourthImage = imagesInfo[Constants.UIKeys.fourthImg] as? UIImage {
                imagesToUpload.append(fourthImage)
            }
            if imagesToUpload.count > 0 {
                S3Manager.sharedInstance.uploadImages(images: imagesToUpload, imageQuality: .medium, progress: nil) { (imageUrls, imageNames, error) in
                    if let uploadedImages = imageUrls as? [String] {
                        completion(uploadedImages)
                    }
                }
            } else {
                completion([])
            }
        } else {
            completion([])
        }
    }
    
    func getBidParams(bidDetails: [CellInfo],tenderDetails: SupplierTender?,images: [String])-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.tenderId] = Helper.toInt(tenderDetails?.id) as AnyObject
        params[ConstantAPIKeys.vehicleId] = Helper.toInt(tenderDetails?.vehicleId) as AnyObject
        params[ConstantAPIKeys.serviceDays] = bidDetails[0].value as AnyObject
        params[ConstantAPIKeys.jobDate] = bidDetails[1].info?[Constants.UIKeys.firstValue] as AnyObject
        params[ConstantAPIKeys.jobTime] = bidDetails[2].info?[Constants.UIKeys.firstValue] as AnyObject
        params[ConstantAPIKeys.images] = images as AnyObject

        let serviceID = Helper.toInt(bidDetails[3].info?[Constants.UIKeys.id])
        params[ConstantAPIKeys.wheelServiceType] = serviceID as AnyObject
        if serviceID == 1 {
            params[ConstantAPIKeys.rimService] = bidDetails[4].info?[Constants.UIKeys.id] as AnyObject
        }
        params[ConstantAPIKeys.wheelSize] = bidDetails[5].value as AnyObject
        params[ConstantAPIKeys.additionalInfo] = bidDetails[7].value as AnyObject
        params[ConstantAPIKeys.isPickupDropoff] = Helper.toInt(bidDetails[8].info?[Constants.UIKeys.firstValue]) as AnyObject
        params[ConstantAPIKeys.bidAmt] = bidDetails[9].value as AnyObject
        return params
    }
    
    func requestBidAPI(bidDetails: [CellInfo],tenderDetails: SupplierTender?, completion: @escaping (Bool) -> Void) {
        
        if self.validateBidData(bidDetails: bidDetails) {
            self.uploadImages(arrData: bidDetails) { (uploadedImages) in
                let params = self.getBidParams(bidDetails: bidDetails, tenderDetails: tenderDetails, images: uploadedImages)
                APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .submitBid(param: params))) { (response, success) in
                    if success {
                        if let safeResponse =  response as? [String: AnyObject],
                            let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                        {
                            completion(true)
                        }
                    }
                }
            }
            
        }
    }
}
