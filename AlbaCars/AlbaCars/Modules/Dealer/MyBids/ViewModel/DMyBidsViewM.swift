//
//  DMyBidsViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 27/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

protocol DMyBidsVodeling: BaseVModeling {
  func requestMyBidsAPI(bidStatus: Int, page: Int, completion: @escaping ([FleetCar], Int, Int) -> Void)
  func requestBuyFleetBidAPI(bidId: Int, completion: @escaping (Bool) -> Void)
}

class DMyBidsViewM: BaseViewM, DMyBidsVodeling {
  func requestMyBidsAPI(bidStatus: Int, page: Int, completion: @escaping ([FleetCar], Int, Int) -> Void) {
    let myBidsParams = getAPIParams(bidStatus: bidStatus, page: page)
    APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .myBids(param: myBidsParams))) { (response, success) in
      if success {
        if let safeResponse =  response as? [String: AnyObject],
          let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject], let data = result[kData] as? [[String: AnyObject]], let nextPageNumber = result[kNextPageNum] as? Int,
          let perPage = result[kPerPage] as? Int,
          let fleetCarData = data.toJSONData() {
          print(result)
          do {
            let decoder = JSONDecoder()
            let fleetCars = try decoder.decode([FleetCar].self, from: fleetCarData)
            completion(fleetCars, nextPageNumber, perPage)
          } catch let error {
            print(error)
          }
        }
      }
    }
  }
  
  func requestBuyFleetBidAPI(bidId: Int, completion: @escaping (Bool) -> Void) {
    let buyFleetBidParams = getBuyFleetBidAPIParams(bidId: bidId)
    APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .buyFleetBid(param: buyFleetBidParams))) { (response, success) in
      if success {
        if let _ =  response as? [String: AnyObject]{
          completion(success)
        }
      }
    }
  }
  
  private func getAPIParams(bidStatus: Int, page: Int) -> APIParams {
    var param: APIParams = APIParams()
    param[ConstantAPIKeys.page] = page as AnyObject
    param[ConstantAPIKeys.bidStatus] = bidStatus as AnyObject
    return param
  }
  
  private func getBuyFleetBidAPIParams(bidId: Int) -> APIParams {
    var param: APIParams = APIParams()
    param[ConstantAPIKeys.bidId] = bidId as AnyObject
    return param
  }
  
}
