//
//  MyBidsListCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 27/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol MyBidsListCellDelegate: class, BaseProtocol {
  func didTapPayButton(cell: MyBidsListCell)
}

class MyBidsListCell: BaseTableViewCell, NibLoadableView, ReusableView {
  
  //MARK: - IBOutlets
  @IBOutlet weak var shadowView: UIView!
  @IBOutlet weak var containerView: UIView!
  //FleetAndBidStatusInfo
  @IBOutlet weak var fleetBrandPlaceholderLabel: UILabel!
  @IBOutlet weak var fleetBrandImageView: UIImageView!
  @IBOutlet weak var fleetBrandNameLabel: UILabel!
  @IBOutlet weak var fleetListNameLabel: UILabel!
  @IBOutlet weak var bidStatusImageView: UIImageView!
  @IBOutlet weak var bidStatusLabel: UILabel!
  //CarInfo
  @IBOutlet weak var fleetCarNameLabel: UILabel!
  @IBOutlet weak var vinLabel: UILabel!
  @IBOutlet weak var brandLabel: UILabel!
  @IBOutlet weak var odometerLabel: UILabel!
  @IBOutlet weak var modelLabel: UILabel!
  @IBOutlet weak var specsLabel: UILabel!
  @IBOutlet weak var yearLabel: UILabel!
  @IBOutlet weak var carTypeLabel: UILabel!
  //BidAmountView
  @IBOutlet weak var bidAmountLabel: UILabel!
  @IBOutlet weak var payBidButton: UIButton!
  
  //MARK: - Variables
  weak var delegate: MyBidsListCellDelegate?
  
  //MARK: - LifeCycle Methods
  override func awakeFromNib() {
    super.awakeFromNib()
    self.setup()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  //MARK: - Private Methods
  private func setup() {
    self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_3half)
    self.shadowView.addShadow(Constants.UIConstants.sizeRadius_3half, shadowOpacity: Constants.UIConstants.shadowOpacity_15)
    self.containerView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
  }
  
  private func setupViewForBidStatus(data: FleetCar) {
    guard let bidStatusValue = data.bidStatus,
          let bidStatus = BidStatus(rawValue: bidStatusValue) else { return }
    self.bidStatusImageView.image = bidStatus.image
    self.bidStatusLabel.text = bidStatus.title
    self.bidStatusLabel.textColor = bidStatus.titleColor
    switch bidStatus {
    case .won:
      if let winningAmount = data.winningAmount {
        self.payBidButton.isHidden = false
        self.payBidButton.isUserInteractionEnabled = true
        let payBidButtonText = "Pay".localizedString() + " AED \(winningAmount)"
        self.payBidButton.setTitle(payBidButtonText, for: .normal)
        self.payBidButton.backgroundColor = .redButtonColor
      }
    case .carBought:
      if let winningAmount = data.winningAmount {
        self.payBidButton.isHidden = false
        self.payBidButton.isUserInteractionEnabled = false
        let payBidButtonText = "Paid".localizedString() + " AED \(winningAmount)"
        self.payBidButton.setTitle(payBidButtonText, for: .normal)
        self.payBidButton.backgroundColor = .greenTopHeaderColor
      }
    default:
      self.payBidButton.isHidden = true
    }
  }
  
  //MARK: - Public Methods
  func configureView(data: FleetCar) {
    if let companyLogo = data.companyLogo {
      self.fleetBrandImageView.setImage(urlStr: companyLogo, placeHolderImage: nil)
    }
    self.fleetBrandNameLabel.text = data.companyName
    self.fleetListNameLabel.text = data.fleetName
    self.fleetCarNameLabel.text = data.title
    if let plateNumber = data.plateNumber {
      let plateNumberText = String.getAttributedText(firstText: "VIN :".localizedString(), secondText: " \(plateNumber)", firstTextColor: .black, secondTextColor: .greenTopHeaderColor, firstTextSize: .size_12, secondTextSize: .size_12)
      self.vinLabel.attributedText = plateNumberText
    }
    if let brandName = data.brandName {
      let brandNameText = String.getAttributedText(firstText: "Brand :".localizedString(), secondText: " \(brandName)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
      self.brandLabel.attributedText = brandNameText
    }
    if let model = data.modelName {
      let modelText = String.getAttributedText(firstText: "Model :".localizedString(), secondText: " \(model)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
      self.modelLabel.attributedText = modelText
    }
    if let modelYear = data.year {
      let modelYearText = String.getAttributedText(firstText: "Model Year :".localizedString(), secondText: " \(modelYear)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
      self.yearLabel.attributedText = modelYearText
    }
    if let odometer = data.mileage {
      let odometerText = String.getAttributedText(firstText: "Odometer :".localizedString(), secondText: " \(odometer.formattedWithSeparator) Km", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
      self.odometerLabel.attributedText = odometerText
    }
    if let specs = data.specs {
      let specsText = String.getAttributedText(firstText: "Specs :".localizedString(), secondText: " \(specs)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
      self.specsLabel.attributedText = specsText
    }
    if let carType = data.typeName {
      let carTypeText = String.getAttributedText(firstText: "Car Type :".localizedString(), secondText: " \(carType)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
      self.carTypeLabel.attributedText = carTypeText
    }
    if let bidAmount = data.bidAmount {
      self.bidAmountLabel.text = "AED  \(bidAmount.formattedWithSeparator)"
    }
    self.setupViewForBidStatus(data: data)
  }
  
  //MARK: - IBActions
  @IBAction func tapPayButton(_ sender: UIButton) {
    if let delegate = self.delegate {
      delegate.didTapPayButton(cell: self)
    }
  }
}
