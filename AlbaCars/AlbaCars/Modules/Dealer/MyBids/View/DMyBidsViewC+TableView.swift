//
//  DMyBidsViewC+TableView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 27/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension DMyBidsViewC: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.fleetCarsDataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return getCell(tableView, indexPath: indexPath)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return Constants.CellHeightConstants.height_175
  }
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    guard let lastVisibleRow = self.myBidsTableView.indexPathsForVisibleRows?.last?.row else { return }
    if (self.fleetCarsDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
      self.requestMyBidsAPI()
    }
  }
}

extension DMyBidsViewC {
  func getCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
    let cell: MyBidsListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
    let fleetCar = self.fleetCarsDataSource[indexPath.row]
    cell.delegate = self
    cell.configureView(data: fleetCar)
    return cell
  }
}

//MARK: - MyBidsListCellDelegate
extension DMyBidsViewC: MyBidsListCellDelegate {

    func didTapPayButton(cell: MyBidsListCell) {

    guard let indexPath = self.myBidsTableView.indexPath(for: cell) else { return }

    let bid = self.fleetCarsDataSource[indexPath.row]

    if let bidId = bid.id {
        
        let paymentDetails = PaymentDetailsData(viewController: self, totalAmmount: Helper.toDouble(self.fleetCarsDataSource[indexPath.row].winningAmount), referenceId: bidId, paymentServiceCategoryId: PaymentServiceCategoryId.fleetBidding,vatAmmount: 0)
       
        PaymentManager.sharedInstance.initiatePayment(paymentDetailsData: paymentDetails) { (success, paymentID) in
            
            if success {
                self.requestBuyFleetBidAPI(bidId: bidId, indexPath: indexPath)
            }
        }
    }
  }
}
