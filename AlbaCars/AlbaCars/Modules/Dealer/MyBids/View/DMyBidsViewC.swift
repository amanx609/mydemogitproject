//
//  DMyBidsViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 27/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DMyBidsViewC: BaseViewC {
  
  //MARK: - IBOutlets
  @IBOutlet weak var myBidsTableView: UITableView!
  
  //MARK: - Variables
  var viewModel: DMyBidsVodeling?
  var fleetCarsDataSource: [FleetCar] = []
  var bidStatus: BidStatus = .pending
  var nextPageNumber = 1
  var perPage = Constants.Validations.perPage
  
  //MARK: - LifeCycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setup()
  }
  
  //MARK: - Private Methods
  private func setup() {
    self.setupNavigationBarTitle(title: "My Bids".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
    self.recheckVM()
    self.setUpTableView()
    self.requestMyBidsAPI()
  }
  
  private func recheckVM() {
    if self.viewModel == nil {
      self.viewModel = DMyBidsViewM()
    }
  }
  
  private func setUpTableView() {
      self.registerNibs()
      self.myBidsTableView.delegate = self
      self.myBidsTableView.dataSource = self
      self.myBidsTableView.separatorStyle = .none
  }
  
  private func registerNibs() {
    self.myBidsTableView.register(MyBidsListCell.self)
  }
  
  private func updateBidStatusAt(indexPath: IndexPath, bidStatus: BidStatus) {
    if self.bidStatus == .won {
        self.fleetCarsDataSource.remove(at: indexPath.row)
        self.myBidsTableView.reloadData()
    } else {
        self.fleetCarsDataSource[indexPath.row].bidStatus = bidStatus.rawValue
        self.myBidsTableView.reloadRows(at: [indexPath], with: .automatic)
    }
  }
  
  //MARK: - APIMethods
  func requestMyBidsAPI() {
    self.viewModel?.requestMyBidsAPI(bidStatus: self.bidStatus.rawValue, page: self.nextPageNumber, completion: { (fleetCars, nextPageNum, perPage)  in
        
        self.nextPageNumber = nextPageNum
        self.perPage = perPage
       self.fleetCarsDataSource = self.fleetCarsDataSource + fleetCars
      Threads.performTaskInMainQueue {
        self.myBidsTableView.reloadData()
      }
    })
  }
  
  func requestBuyFleetBidAPI(bidId: Int, indexPath: IndexPath) {
    self.viewModel?.requestBuyFleetBidAPI(bidId: bidId, completion: { [weak self] (success) in
      guard let sSelf = self else { return }
      if success {
        sSelf.updateBidStatusAt(indexPath: indexPath, bidStatus: .carBought)
      }
    })
  }
}
