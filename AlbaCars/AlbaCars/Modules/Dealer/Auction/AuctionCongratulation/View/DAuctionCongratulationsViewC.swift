//
//  DAuctionCongratulationsViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/4/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit



class DAuctionCongratulationsViewC: UIViewController {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var congratsLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var mainMenuButton: UIButton!
    
    //MARK: - Variables
    var viewModel: DAuctionCongratulationsViewModeling?
    var auctionModel = DAuctionModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.congratsLabel.text = "Congratulations!!".localizedString()
        self.descriptionLabel.text = "Your car has been submitted for approval. once approved, you shall receive a notification and your car will be added in your auction room".localizedString()
        self.mainMenuButton.setTitle("Main Menu".localizedString(), for: .normal)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        setupView()
        self.recheckVM()
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners(Constants.UIConstants.sizeRadius_7)
            self.mainMenuButton.roundCorners(Constants.UIConstants.sizeRadius_7)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .redButtonColor)
        }
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = DAuctionCongratulationsViewM()
        }
    }
    
    //MARK: - API Methods
    func requestCancelAPI() {
        self.viewModel?.requestCancelAuctionAPI(auctionId: Helper.toInt(self.auctionModel.basicDetails?.id)) { (success) in
            if success {
                
                Threads.performTaskInMainQueue {
                    Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Post cancel successfully".localizedString())
                    
                    if let viewController = self.navigationController?.viewControllers {
                        for controller in viewController {
                            if controller.isKind(of: DAuctionViewC.self) {
                                self.navigationController?.setNavigationBarHidden(false, animated: false)
                                self.navigationController?.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                }
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func didTapCancelPostButton(_ sender: Any) {
        Alert.showAlertWithActionWithCancel(title: StringConstants.Text.AppName.localizedString(), message: StringConstants.Message.cancelPostRequest.localizedString(), actionTitle: StringConstants.Text.Yes.localizedString(), cancelTitle: StringConstants.Text.No.localizedString(), action: { (_) in
            self.requestCancelAPI()
        }, cancelAction: { (_) in
            
        })
    }
    
    @IBAction func didTapMainMenuButton(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: false)
    }
}
