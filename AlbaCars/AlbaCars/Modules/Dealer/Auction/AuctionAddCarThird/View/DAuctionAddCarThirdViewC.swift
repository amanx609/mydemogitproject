//
//  DAuctionAddCarThirdViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DAuctionAddCarThirdViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var addCarTableView: UITableView!
    
    //MARK: - Properties
    var viewModel: DAuctionAddCarThirdViewModeling?
    var addCarDataSource: [CellInfo] = []
    var auctionModel = DAuctionModel()
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Add Car Details".localizedString(),barColor: .redButtonColor,titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.loadDataSource()
    }
    
    //MARK: - Private Methods
    private func setup() {
        setupViewModel()
        registerNibs()
        setupTableView()
    }
    
    private func setupTableView() {
        self.addCarTableView.separatorStyle = .none
        self.addCarTableView.backgroundColor = UIColor.white
        self.addCarTableView.delegate = self
        self.addCarTableView.dataSource = self
        self.addCarTableView.allowsSelection =  false
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = DAuctionAddCarThirdViewM()
        }
    }
    
    private func registerNibs() {
        self.addCarTableView.register(DAuctionAddCarDropDownCell.self)
        self.addCarTableView.register(BottomButtonCell.self)
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getAddCarFormDataSource(option: self.auctionModel.options) {
            self.addCarDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.addCarTableView.reloadData()
            }
        }
    }
    
    func showDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        switch indexPath.row {
        case 0:
            listPopupView.initializeViewWith(title: DropDownType.seats.title, arrayList: DropDownType.seats.dataList, key: DropDownType.seats.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let seats = response[DropDownType.seats.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = seats
                sSelf.addCarTableView.reloadData()
            }
        case 1:
            listPopupView.initializeViewWith(title: DropDownType.sunroof.title, arrayList: DropDownType.sunroof.dataList, key: DropDownType.sunroof.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let sunroof = response[DropDownType.sunroof.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = sunroof
                sSelf.addCarTableView.reloadData()
            }
        case 2:
            listPopupView.initializeViewWith(title: DropDownType.navigation.title, arrayList: DropDownType.navigation.dataList, key: DropDownType.navigation.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let navigation = response[DropDownType.navigation.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = navigation
                sSelf.addCarTableView.reloadData()
            }
        case 3:
            listPopupView.initializeViewWith(title: DropDownType.camera.title, arrayList: DropDownType.camera.dataList, key: DropDownType.camera.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let camera = response[DropDownType.camera.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = camera
                sSelf.addCarTableView.reloadData()
            }
        case 4:
            listPopupView.initializeViewWith(title: DropDownType.wheels.title, arrayList: DropDownType.wheels.dataList, key: DropDownType.wheels.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let wheels = response[DropDownType.wheels.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = wheels
                sSelf.addCarTableView.reloadData()
            }
        case 5:
            listPopupView.initializeViewWith(title: DropDownType.cruiseControl.title, arrayList: DropDownType.cruiseControl.dataList, key: DropDownType.cruiseControl.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let cruiseControl = response[DropDownType.cruiseControl.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = cruiseControl
                sSelf.addCarTableView.reloadData()
            }
        case 6:
            listPopupView.initializeViewWith(title: DropDownType.windows.title, arrayList: DropDownType.windows.dataList, key: DropDownType.windows.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let windows = response[DropDownType.windows.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = windows
                sSelf.addCarTableView.reloadData()
            }
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    //MARK: - API Methods
      func requestAuctionAPI() {
        self.viewModel?.requestAuctionAPI(arrData: self.addCarDataSource, auctionId: Helper.toInt(self.auctionModel.options?.id)) { (responseData) in
              self.auctionModel = responseData
              Threads.performTaskInMainQueue {
                  let auctionAddCarFourthVC = DIConfigurator.sharedInstance.getDAuctionAddCarFourthVC()
                  auctionAddCarFourthVC.auctionModel = self.auctionModel
                  self.navigationController?.pushViewController(auctionAddCarFourthVC, animated: true)
              }
          }
      }
    
}
