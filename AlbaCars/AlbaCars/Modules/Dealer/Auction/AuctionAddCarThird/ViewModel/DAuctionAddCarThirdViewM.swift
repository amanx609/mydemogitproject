//
//  DAuctionAddCarThirdViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DAuctionAddCarThirdViewModeling {
    func getAddCarFormDataSource(option: Options?) -> [CellInfo]
    func validatePostCarData(arrData: [CellInfo]) -> Bool
    func requestAuctionAPI(arrData: [CellInfo],auctionId:Int, completion: @escaping (DAuctionModel)-> Void)
}

class DAuctionAddCarThirdViewM: DAuctionAddCarThirdViewModeling {
    
    func getAddCarFormDataSource(option: Options?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        // Seats Cell
        var seatsInfo = [String: AnyObject]()
        seatsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "seats")
        let seatsCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Seats".localizedString(), value:Helper.toString(object: option?.seats) ,info: seatsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(seatsCell)
        
        // Sunroof Cell
        var sunroofInfo = [String: AnyObject]()
        sunroofInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "sunroof")
        let sunroofCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Sunroof".localizedString(), value:Helper.getDefaultString(object: option?.sunroof, defaultString: "None".localizedString()) ,info: sunroofInfo, height: Constants.CellHeightConstants.height_80)
        array.append(sunroofCell)
        
        // Navigation Cell
        var navigationInfo = [String: AnyObject]()
        navigationInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "navigation")
        let navigationCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Navigation".localizedString(), value:Helper.toString(object: option?.navigation) ,info: navigationInfo, height: Constants.CellHeightConstants.height_80)
        array.append(navigationCell)
                
        // Camera Cell
        var cameraInfo = [String: AnyObject]()
        cameraInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "camera360")
        let cameraCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Camera".localizedString(), value:Helper.toString(object: option?.camera) ,info: cameraInfo, height: Constants.CellHeightConstants.height_80)
        array.append(cameraCell)
        
        // Wheels Cell
        var wheelsInfo = [String: AnyObject]()
        wheelsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "wheels-1")
        let wheelsCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Wheels".localizedString(), value:Helper.toString(object: option?.wheels) ,info: wheelsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(wheelsCell)
        
        // Cruise Control Cell
        var cruiseControlInfo = [String: AnyObject]()
        cruiseControlInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "cruise_control")
        let cruiseControlCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Cruise Control".localizedString(), value:Helper.getDefaultString(object: option?.cruiseControl, defaultString:"Yes".localizedString()) ,info: cruiseControlInfo, height: Constants.CellHeightConstants.height_80)
        array.append(cruiseControlCell)
        
        // Windows Cell
        var windowsInfo = [String: AnyObject]()
        windowsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "windows")
        let windowsCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Windows".localizedString(), value:Helper.getDefaultString(object: option?.windows, defaultString: "Electric".localizedString()) ,info: windowsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(windowsCell)
                
        //Next Cell
        let submitCell = CellInfo(cellType: .BottomButtonCell, placeHolder: "Next".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_100)
        array.append(submitCell)
        
        return array
    }
    
    //MARK: - API Methods
    func requestAuctionAPI(arrData: [CellInfo],auctionId:Int, completion: @escaping (DAuctionModel)-> Void) {
      if self.validatePostCarData(arrData: arrData) {
        let params = self.getPostAddCarParams(arrData: arrData, auctionId: auctionId)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .postAuctionCar(param: params))) { (response, success) in
          if success {
              if let safeResponse =  response as? [String: AnyObject],
                  let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                  let auctionData = result.toJSONData() {
                  do {
                      let decoder = JSONDecoder()
                      let auctionModelData = try decoder.decode(DAuctionModel.self, from: auctionData)
                      completion(auctionModelData)
                  } catch let error {
                      print(error)
                  }
              }
          }
        }
      }
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
     func validatePostCarData(arrData: [CellInfo]) -> Bool {
        var isValid = true
        var message = ""
        if let seats = arrData[0].value, seats.isEmpty {
            message = "Please select seats."
            isValid = false
        } else if let sunroof = arrData[1].value, sunroof.isEmpty  {
            message = "Please select sunroof."
            isValid = false
        } else if let navigation = arrData[2].value, navigation.isEmpty {
            message = "Please select navigation."
            isValid = false
        }  else if let camera = arrData[3].value, camera.isEmpty {
            message = "Please enter camera."
            isValid = false
        } else if let wheels = arrData[4].value,wheels.isEmpty {
            message = "Please select wheels"
            isValid = false
        } else if let cruise = arrData[5].value, cruise.isEmpty {
            message = "Please enter cruise control."
            isValid = false
        } else if let windows = arrData[6].value, windows.isEmpty {
            message = "Please select windows"
            isValid = false
        }
       
        if !isValid {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: message.localizedString())
        }
        return isValid
    }
    
    func getPostAddCarParams(arrData: [CellInfo],auctionId:Int)-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.auctionPage] = ConstantAPIKeys.options as AnyObject
        params[ConstantAPIKeys.seats] = arrData[0].value as AnyObject
        params[ConstantAPIKeys.sunroof] = arrData[1].value as AnyObject
        params[ConstantAPIKeys.navigation] = arrData[2].value as AnyObject
        params[ConstantAPIKeys.camera] = arrData[3].value as AnyObject
        params[ConstantAPIKeys.wheels] = arrData[4].value as AnyObject
        params[ConstantAPIKeys.cruiseControl] = arrData[5].value as AnyObject
        params[ConstantAPIKeys.windows] = arrData[6].value as AnyObject
        params[ConstantAPIKeys.id] = auctionId as AnyObject

        return params
    }
}

