//
//  AuctionUploadCarImageViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol AuctionUploadCarImageViewModeling: BaseVModeling {
    func getAuctionUploadCarImageDataSource(carImages: CarImages?) -> [CellInfo]
    func validateUploadCarImagesParams(arrData: [CellInfo]) -> Bool
    func requestAuctionUploadCarImages(arrData: [CellInfo],auctionId:Int,completion: @escaping (APIParams)-> Void)
    func requestAuctionAPI(params: APIParams, completion: @escaping (DAuctionModel)-> Void)
}

class AuctionUploadCarImageViewM: AuctionUploadCarImageViewModeling {
    
    func getAuctionUploadCarImageDataSource(carImages: CarImages?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Header Cell
        let headerCell = CellInfo(cellType: .DAuctionUploadHeaderCollectionCell, placeHolder: "Upload Car Photos".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_60)
        array.append(headerCell)
        
        //Upload Car Front Image
        var uploadImageInfo = [String: AnyObject]()
        uploadImageInfo[Constants.UIKeys.placeholderImage] = self.getUrl(images: carImages?.images, index: 0) as AnyObject
        let uploadImageCell = CellInfo(cellType: .DAuctionUploadCarImageCollectionCell, placeHolder: "Car Front".localizedString(), value: "", info: uploadImageInfo, height: Constants.CellHeightConstants.height_120)
        array.append(uploadImageCell)
        
        //Upload Car Rear Image
        var carRearInfo = [String: AnyObject]()
        carRearInfo[Constants.UIKeys.placeholderImage] = self.getUrl(images: carImages?.images, index: 1) as AnyObject
        let carRearCell = CellInfo(cellType: .DAuctionUploadCarImageCollectionCell, placeHolder: "Car Rear".localizedString(), value: "", info: carRearInfo, height: Constants.CellHeightConstants.height_120)
        array.append(carRearCell)
        
        //Upload Car Left Side Image
        var carLeftInfo = [String: AnyObject]()
        carLeftInfo[Constants.UIKeys.placeholderImage] = self.getUrl(images: carImages?.images, index: 2) as AnyObject
        let carLeftSideCell = CellInfo(cellType: .DAuctionUploadCarImageCollectionCell, placeHolder: "Car Left Side".localizedString(), value: "", info: carLeftInfo, height: Constants.CellHeightConstants.height_120)
        array.append(carLeftSideCell)
        
        //Upload Car Right Side Image
        var carRightInfo = [String: AnyObject]()
        carRightInfo[Constants.UIKeys.placeholderImage] = self.getUrl(images: carImages?.images, index: 3) as AnyObject
        let carRightSideCell = CellInfo(cellType: .DAuctionUploadCarImageCollectionCell, placeHolder: "Car Right Side".localizedString(), value: "", info: carRightInfo, height: Constants.CellHeightConstants.height_120)
        array.append(carRightSideCell)
        
        //Upload Front Seats Image
        var frontSeatsInfo = [String: AnyObject]()
        frontSeatsInfo[Constants.UIKeys.placeholderImage] = self.getUrl(images: carImages?.images, index: 4) as AnyObject
        let frontSeatsCell = CellInfo(cellType: .DAuctionUploadCarImageCollectionCell, placeHolder: "Front Seats".localizedString(), value: "", info: frontSeatsInfo, height: Constants.CellHeightConstants.height_120)
        array.append(frontSeatsCell)
        
        //Upload Rear Seats Image
        var rearSeatsInfo = [String: AnyObject]()
        rearSeatsInfo[Constants.UIKeys.placeholderImage] = self.getUrl(images: carImages?.images, index: 5) as AnyObject
        let rearSeatsCell = CellInfo(cellType: .DAuctionUploadCarImageCollectionCell, placeHolder: "Rear Seats".localizedString(), value: "", info: rearSeatsInfo, height: Constants.CellHeightConstants.height_120)
        array.append(rearSeatsCell)
        
        //Upload Car Mileage (Odometer) Image
        var odometerInfo = [String: AnyObject]()
        odometerInfo[Constants.UIKeys.placeholderImage] = self.getUrl(images: carImages?.images, index: 6) as AnyObject
        let carMileageCell = CellInfo(cellType: .DAuctionUploadCarImageCollectionCell, placeHolder: "Car Mileage (Odometer)".localizedString(), value: "", info: odometerInfo, height: Constants.CellHeightConstants.height_120)
        array.append(carMileageCell)
        
        //Upload Dashboard Image
        var dashboardInfo = [String: AnyObject]()
        dashboardInfo[Constants.UIKeys.placeholderImage] = self.getUrl(images: carImages?.images, index: 7) as AnyObject
        let dashboardCell = CellInfo(cellType: .DAuctionUploadCarImageCollectionCell, placeHolder: "Dashboard".localizedString(), value: "", info: dashboardInfo, height: Constants.CellHeightConstants.height_120)
        array.append(dashboardCell)
        
        //Upload GCC Sticker (If GCC) Image
        var stickerInfo = [String: AnyObject]()
        stickerInfo[Constants.UIKeys.placeholderImage] = self.getUrl(images: carImages?.images, index: 8) as AnyObject
        let stickerCell = CellInfo(cellType: .DAuctionUploadCarImageCollectionCell, placeHolder: "GCC Sticker (If GCC)".localizedString(), value: "", info: stickerInfo, height: Constants.CellHeightConstants.height_120)
        array.append(stickerCell)
        
        //Upload Other (Optional) Image
        var otherInfo = [String: AnyObject]()
        otherInfo[Constants.UIKeys.placeholderImage] = self.getUrl(images: carImages?.images, index: 9) as AnyObject
        let otherCell = CellInfo(cellType: .DAuctionUploadCarImageCollectionCell, placeHolder: "Other (Optional)".localizedString(), value: "", info: otherInfo, height: Constants.CellHeightConstants.height_120)
        array.append(otherCell)
        
        //Footer Cell
        let footerCell = CellInfo(cellType: .DAuctionUploadFooterCollectionCell, placeHolder: "Upload the images of your car in respective fields".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_60)
        array.append(footerCell)
        
        return array
    }
    
    func getUrl(images: [Images]?, index: Int) -> String {
        
        if images?.count ?? 0 > index {
            return images?[index].image ?? ""
        }
        return ""
    }
    
    
    //self.uploadDataSource[indexPath.row].info?[Constants.UIKeys.placeholderImage]
    func validateUploadCarImagesParams(arrData: [CellInfo]) -> Bool {
        var isValid = true
        if let carFront = arrData[1].info, carFront[Constants.UIKeys.placeholderImage] as? String == "" {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for car front.".localizedString())
            isValid = false
        } else if let carRear = arrData[2].info, carRear[Constants.UIKeys.placeholderImage] as? String == "" {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for Car rear.".localizedString())
            isValid = false
        } else if let carLeftSide = arrData[3].info, carLeftSide[Constants.UIKeys.placeholderImage] as? String == "" {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for Car Left Side.".localizedString())
            isValid = false
        } else if let carRightSide = arrData[4].info, carRightSide[Constants.UIKeys.placeholderImage] as? String == "" {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for Car Right Side.".localizedString())
            isValid = false
        } else if let frontSeats = arrData[5].info, frontSeats[Constants.UIKeys.placeholderImage] as? String == "" {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for Front Seats.".localizedString())
            isValid = false
        } else if let rearSeats = arrData[6].info, rearSeats[Constants.UIKeys.placeholderImage] as? String == "" {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for Rear Seats.".localizedString())
            isValid = false
        } else if let carMileage = arrData[7].info, carMileage[Constants.UIKeys.placeholderImage] as? String == "" {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for Car Mileage (Odometer).".localizedString())
            isValid = false
        } else if let dashboard = arrData[8].info, dashboard[Constants.UIKeys.placeholderImage] as? String == "" {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for Dashboard.".localizedString())
            isValid = false
        } else if let dashboard = arrData[9].info, dashboard[Constants.UIKeys.placeholderImage] as? String == "" {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for GCC Sticker (If GCC)".localizedString())
            isValid = false
        }
        
        return isValid
    }
    
    //MARK: - API Methods
    func requestAuctionAPI(params: APIParams, completion: @escaping (DAuctionModel)-> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .postAuctionCar(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let auctionData = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let auctionModelData = try decoder.decode(DAuctionModel.self, from: auctionData)
                        completion(auctionModelData)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
    //MARK: - API Methods
    func requestAuctionUploadCarImages(arrData: [CellInfo],auctionId:Int,completion: @escaping (APIParams)-> Void) {
        var imagesArray = [UIImage]()
        var imagesNameArray = [String]()
        
        for i in 0..<arrData.count {
            if let images = arrData[i].info?[Constants.UIKeys.placeholderImage] as? UIImage {
                imagesArray.append(images)
            } else if let imageUrl = arrData[i].info?[Constants.UIKeys.placeholderImage] as? String {
                imagesNameArray.append(imageUrl)
            }
        }
        
        if imagesArray.count == 0 {
            let uploadCarParams = self.getAuctionUploadCarImagesParams(imagesURL: imagesNameArray, auctionId: auctionId)
            completion(uploadCarParams)
            return
        }
            S3Manager.sharedInstance.uploadImages(images: imagesArray, imageQuality: .medium, progress: nil) { (imagesUrl, imagesName, error) in
                if let imgsUrl = imagesUrl as? [String] {
                    let uploadCarParams = self.getAuctionUploadCarImagesParams(imagesURL: imgsUrl, auctionId: auctionId)
                    completion(uploadCarParams)
                }
            }
        

    }
    
    func getAuctionUploadCarImagesParams(imagesURL: [String],auctionId:Int) -> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.images] = imagesURL as AnyObject
        params[ConstantAPIKeys.id] = auctionId as AnyObject
        params[ConstantAPIKeys.auctionPage] = ConstantAPIKeys.images as AnyObject
        return params
    }
    
    
}
