//
//  DAuctionUploadCarImageCollectionCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DAuctionUploadCarImageCollectionCell: BaseCollectionViewCell,ReusableView, NibLoadableView  {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var placeholderImageView: UIImageView!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        
        self.titleLabel.text = cellInfo.placeHolder
        if let info = cellInfo.info {
            //Image
            if let placeholderImage = info[Constants.UIKeys.placeholderImage] as? UIImage {
                self.placeholderImageView.image = placeholderImage
            } else if let placeholderImage = info[Constants.UIKeys.placeholderImage] as? String {
                self.placeholderImageView.setImage(urlStr: placeholderImage, placeHolderImage: nil)
            } else {
                self.placeholderImageView.image =  nil
            }
        }
    }
    
}
