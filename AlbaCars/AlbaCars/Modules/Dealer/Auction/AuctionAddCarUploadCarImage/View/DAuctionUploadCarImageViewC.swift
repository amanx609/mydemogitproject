//
//  DAuctionUploadCarImageViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DAuctionUploadCarImageViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var uploadImageCollectionView: UICollectionView!
    
    //MARK: - Variables
    var uploadDataSource: [CellInfo] = []
    var viewModel: AuctionUploadCarImageViewModeling?
    var auctionModel = DAuctionModel()
    //var imagesArray = [UIImage]()
    var params: APIParams = APIParams()

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Add Car Details".localizedString(),barColor: .redButtonColor,titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupView()
        self.recheckVM()
        self.setupCollectionView()
        self.loadDataSource()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = AuctionUploadCarImageViewM()
        }
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.submitButton.setTitle("Submit".localizedString(), for: .normal)
    }
    
    private func setupCollectionView() {
        self.registerNibs()
        self.uploadImageCollectionView.delegate = self
        self.uploadImageCollectionView.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //depending upon direction of collection view
        self.uploadImageCollectionView?.setCollectionViewLayout(layout, animated: true)
    }
    
    private func registerNibs() {
        self.uploadImageCollectionView.register(DAuctionUploadCarImageCollectionCell.self)
        self.uploadImageCollectionView.register(DAuctionUploadFooterCollectionCell.self)
        self.uploadImageCollectionView.register(DAuctionUploadHeaderCollectionCell.self)
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getAuctionUploadCarImageDataSource(carImages: self.auctionModel.carImages) {
            self.uploadDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.uploadImageCollectionView.reloadData()
            }
        }
    }
    
    @IBAction func tapSubmitButton(_ sender: UIButton) {
        // call api
        
        if self.viewModel?.validateUploadCarImagesParams(arrData: self.uploadDataSource) ?? false {
            
            self.viewModel?.requestAuctionUploadCarImages(arrData: self.uploadDataSource, auctionId: Helper.toInt(self.auctionModel.basicDetails?.id), completion: { (params) in
                
                self.viewModel?.requestAuctionAPI(params: params, completion: { (responseData) in
                    self.auctionModel = responseData
                    Threads.performTaskInMainQueue {
                        let chooseAuctionTypeVC = DIConfigurator.sharedInstance.getDChooseAuctionTypeVC()
                        //let auctionConfirmationVC = DIConfigurator.sharedInstance.getDAuctionConfirmationVC()
                        chooseAuctionTypeVC.auctionModel = self.auctionModel
                        self.navigationController?.pushViewController(chooseAuctionTypeVC, animated: true)
                    }
                })
            })
            
           // let auctionConfirmationVC = DIConfigurator.sharedInstance.getDAuctionConfirmationVC()
           // self.navigationController?.pushViewController(auctionConfirmationVC, animated: true)
        }
        
    }
    
}
