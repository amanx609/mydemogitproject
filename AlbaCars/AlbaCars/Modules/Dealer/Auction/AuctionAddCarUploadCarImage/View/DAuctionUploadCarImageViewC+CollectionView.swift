//
//  DAuctionUploadCarImageViewC+CollectionView.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension DAuctionUploadCarImageViewC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.uploadDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellInfo = self.uploadDataSource[indexPath.row]
        return self.getCell(collectionView: collectionView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.row == 0 || indexPath.row == self.uploadDataSource.count - 1{
            return CGSize(width: collectionView.frame.size.width, height: 60)
        }
        
        let width = collectionView.bounds.width/2-5
        return CGSize(width: width, height: width+38)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.uploadDataSource[indexPath.row].info?[Constants.UIKeys.placeholderImage] = image
                    //self.imagesArray.append(image)
                    self.uploadImageCollectionView.reloadData()
                }
            }
        }
        
    }
    
}

extension DAuctionUploadCarImageViewC {
    func getCell(collectionView: UICollectionView, indexPath: IndexPath, cellInfo: CellInfo) -> UICollectionViewCell {
        guard let cellType = cellInfo.cellType else { return UICollectionViewCell() }
        switch cellType {
        case .DAuctionUploadCarImageCollectionCell:
            let cell: DAuctionUploadCarImageCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
              cell.configureView(cellInfo: cellInfo)
            return cell
        case .DAuctionUploadHeaderCollectionCell:
            let cell: DAuctionUploadHeaderCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
            return cell
        case .DAuctionUploadFooterCollectionCell:
            let cell: DAuctionUploadFooterCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
            return cell
        default:
            return UICollectionViewCell()
        }
    }
}
