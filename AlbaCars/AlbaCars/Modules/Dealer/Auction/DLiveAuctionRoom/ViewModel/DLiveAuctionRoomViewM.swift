//
//  DLiveAuctionRoomViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 11/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol DLiveAuctionRoomVModeling: BaseVModeling {
    func getLiveAuctionRoomDataSource(auctionModel: DAuctionModel,vehicle: Vehicle?) -> [[String: AnyObject]]
    func requestAuctionDetailsAPI(auctionId:Int, completion: @escaping (DAuctionModel)-> Void)
    func requestAuctionHighestBidAPI(vehicleId:Int, completion: @escaping (HighestBidModel)-> Void)
    func requestSubmitBidAPI(param: APIParams, completion: @escaping (AuctionPlaceBidModel)-> Void)
    func requestrequestBuyNowAPI(vehicleId: Int, completion: @escaping (Bool)-> Void)
    func requestWinBidAPI(vehicleId: Int, completion: @escaping (Bool)-> Void)
    
}

class DLiveAuctionRoomViewM: BaseViewM, DLiveAuctionRoomVModeling {
    
    
    func getLiveAuctionRoomDataSource(auctionModel: DAuctionModel,vehicle: Vehicle?) -> [[String: AnyObject]] {
        
        var dataSource = [[String: AnyObject]]()
        
        //FirstSectionData
        var firstDataDict = [String: AnyObject]()
        var firstDataArray = [CellInfo]()
        
        firstDataDict[Constants.UIKeys.sectionTitle] = "" as AnyObject
        firstDataDict[Constants.UIKeys.isExpanded] = true as AnyObject
        
        //MultipleCarImagesCell
        var carImagesDataDict = [String: AnyObject]()
        carImagesDataDict[Constants.UIKeys.images] = auctionModel.carImages?.images as AnyObject
        let carImagesCell = CellInfo(cellType: .DLAMultipleCarImagesCell, placeHolder: "", value: "", info: carImagesDataDict, height: Constants.CellHeightConstants.height_220)
        firstDataArray.append(carImagesCell)
        
        //CarInfoCell
        var carInfoDataDict = [String: AnyObject]()
        carInfoDataDict[Constants.UIKeys.cellInfo] = auctionModel as AnyObject
        let carInfoCell = CellInfo(cellType: .DLACarInfoCell, placeHolder: "", value: "", info: carInfoDataDict, height: Constants.CellHeightConstants.height_120)
        firstDataArray.append(carInfoCell)
        
        //BidInfoCell
        var bidInfoDataDict = [String: AnyObject]()
        bidInfoDataDict[Constants.UIKeys.cellInfo] = auctionModel as AnyObject
        bidInfoDataDict[Constants.UIKeys.vehicleValue] = vehicle as AnyObject
        let bidInfoCell = CellInfo(cellType: .DLABidInfoCell, placeHolder: "", value: "", info: bidInfoDataDict, height: Constants.CellHeightConstants.height_245,keyboardType: .numberPad)
        firstDataArray.append(bidInfoCell)
        
        firstDataDict[Constants.UIKeys.cells] = firstDataArray as AnyObject
        dataSource.append(firstDataDict)
        
        //SecondSectionData
        var secondDataDict = [String: AnyObject]()
        var secondDataArray = [CellInfo]()
        
        secondDataDict[Constants.UIKeys.sectionTitle] = "Additional Car Details".localizedString() as AnyObject
        secondDataDict[Constants.UIKeys.isExpanded] = true as AnyObject
        secondDataDict[Constants.UIKeys.sectionIcon] = #imageLiteral(resourceName: "car_white") 
        
        //TrimCell
        var trimCellInfo = [String: AnyObject]()
        trimCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let trimCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Trim :".localizedString(), value: Helper.toString(object: auctionModel.basicDetails?.trim), info: trimCellInfo, height: Constants.CellHeightConstants.height_40)
        secondDataArray.append(trimCell)
        
        //CylindersCell
        var cylinderCellInfo = [String: AnyObject]()
        cylinderCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let cylinderCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Cylinders :".localizedString(), value: Helper.toString(object: auctionModel.basicDetails?.cylinders), info: cylinderCellInfo, height: Constants.CellHeightConstants.height_40)
        secondDataArray.append(cylinderCell)
        
        //EngineSizeCell
        var engineSizeCellInfo = [String: AnyObject]()
        engineSizeCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let engineSizeCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Engine Size : ".localizedString(), value: Helper.toString(object: auctionModel.basicDetails?.engine), info: engineSizeCellInfo, height: Constants.CellHeightConstants.height_40)
        secondDataArray.append(engineSizeCell)
        
        //ExteriorColorCell
        var exteriorColorCellInfo = [String: AnyObject]()
        exteriorColorCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let exteriorColorCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Exterior Color :".localizedString(), value: Helper.toString(object: auctionModel.basicDetails?.extColor), info: exteriorColorCellInfo, height: Constants.CellHeightConstants.height_40)
        secondDataArray.append(exteriorColorCell)
        
        //InteriorColorCell
        var interiorColorCellInfo = [String: AnyObject]()
        interiorColorCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let interiorColorCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Interior Color :".localizedString(), value: Helper.toString(object: auctionModel.basicDetails?.innerColor), info: interiorColorCellInfo, height: Constants.CellHeightConstants.height_40)
        secondDataArray.append(interiorColorCell)
        
        //TransmissionCell
        var transmissionCellInfo = [String: AnyObject]()
        transmissionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let transmissionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Transmission :".localizedString(), value: Helper.toString(object: auctionModel.basicDetails?.transmission), info: transmissionCellInfo, height: Constants.CellHeightConstants.height_40)
        secondDataArray.append(transmissionCell)
        
        //FuelTypeCell
        var fuelTypeCellInfo = [String: AnyObject]()
        fuelTypeCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let fuelTypeCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Fuel Type :".localizedString(), value: Helper.toString(object: auctionModel.basicDetails?.fuelType), info: fuelTypeCellInfo, height: Constants.CellHeightConstants.height_40)
        secondDataArray.append(fuelTypeCell)
        
        //DoorsCell
        var doorsCellInfo = [String: AnyObject]()
        doorsCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let doorsCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "No. Of Doors :".localizedString(), value: Helper.toString(object: auctionModel.basicDetails?.noOfDoors), info: doorsCellInfo, height: Constants.CellHeightConstants.height_40)
        secondDataArray.append(doorsCell)
        
        //DriveCell
        var driveCellInfo = [String: AnyObject]()
        driveCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let driveCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Drive :".localizedString(), value: Helper.toString(object: auctionModel.basicDetails?.drive), info: driveCellInfo, height: Constants.CellHeightConstants.height_40)
        secondDataArray.append(driveCell)
        
        secondDataDict[Constants.UIKeys.cells] = secondDataArray as AnyObject
        
        dataSource.append(secondDataDict)
        
        //ThirdSectionData
        var thirdDataDict = [String: AnyObject]()
        var thirdDataArray = [CellInfo]()
        
        thirdDataDict[Constants.UIKeys.sectionTitle] = "Car History".localizedString() as AnyObject
        thirdDataDict[Constants.UIKeys.isExpanded] = false as AnyObject
        thirdDataDict[Constants.UIKeys.sectionIcon] = #imageLiteral(resourceName: "history_white")
        
        //ServiceCell
        var serviceCellInfo = [String: AnyObject]()
        serviceCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let serviceCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Service :".localizedString(), value: Helper.toString(object: auctionModel.historyDetails?.service), info: serviceCellInfo, height: Constants.CellHeightConstants.height_40)
        thirdDataArray.append(serviceCell)
        
        //AccidentCell
        var accidentCellInfo = [String: AnyObject]()
        accidentCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let accidentCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Accident :".localizedString(), value: Helper.toString(object: auctionModel.historyDetails?.accident), info: accidentCellInfo, height: Constants.CellHeightConstants.height_40)
        thirdDataArray.append(accidentCell)
        
        //ChasisDamageCell
        var chasisDamageCellInfo = [String: AnyObject]()
        chasisDamageCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let chasisDamageCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Chasis/Frame Damage :".localizedString(), value: Helper.toString(object: auctionModel.historyDetails?.chasisDamage), info: chasisDamageCellInfo, height: Constants.CellHeightConstants.height_40)
        thirdDataArray.append(chasisDamageCell)
        
        //CarHistoryOtherInfoCell
        var carHistoryOtherCellInfo = [String: AnyObject]()
        carHistoryOtherCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let carHistoryOtherCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Other Information".localizedString(), value: "", info: carHistoryOtherCellInfo, height: Constants.CellHeightConstants.height_40)
        thirdDataArray.append(carHistoryOtherCell)
        
        //CarHistoryDescriptionCell
        let carHistoryDescCell = CellInfo(cellType: .DLAOtherInfoCell, placeHolder: "".localizedString(), value: Helper.toString(object: auctionModel.historyDetails?.carHistoryInfo), info: nil, height: UITableView.automaticDimension)
        thirdDataArray.append(carHistoryDescCell)
        
        thirdDataDict[Constants.UIKeys.cells] = thirdDataArray as AnyObject
        
        dataSource.append(thirdDataDict)
        
        //FourthSectionData
        var fourthDataDict = [String: AnyObject]()
        var fourthDataArray = [CellInfo]()
        
        fourthDataDict[Constants.UIKeys.sectionTitle] = "Options".localizedString() as AnyObject
        fourthDataDict[Constants.UIKeys.isExpanded] = false as AnyObject
        fourthDataDict[Constants.UIKeys.sectionIcon] = #imageLiteral(resourceName: "options_white")
        
        //SeatsCell
        var seatsCellInfo = [String: AnyObject]()
        seatsCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let seatsCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Seats :".localizedString(), value: Helper.toString(object: auctionModel.options?.seats), info: seatsCellInfo, height: Constants.CellHeightConstants.height_40)
        fourthDataArray.append(seatsCell)
        
        //SunroofCell
        var sunroofCellInfo = [String: AnyObject]()
        sunroofCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let sunroofCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Sunroof :".localizedString(), value: Helper.toString(object: auctionModel.options?.sunroof), info: sunroofCellInfo, height: Constants.CellHeightConstants.height_40)
        fourthDataArray.append(sunroofCell)
        
        //NavigationCell
        var navigationCellInfo = [String: AnyObject]()
        navigationCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let navigationCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Navigation :".localizedString(), value:Helper.toString(object: auctionModel.options?.navigation), info: navigationCellInfo, height: Constants.CellHeightConstants.height_40)
        fourthDataArray.append(navigationCell)
        
        //CameraCell
        var cameraCellInfo = [String: AnyObject]()
        cameraCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let cameraCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Camera :".localizedString(), value: Helper.toString(object: auctionModel.options?.camera), info: cameraCellInfo, height: Constants.CellHeightConstants.height_40)
        fourthDataArray.append(cameraCell)
        
        //WheelsCell
        var wheelsCellInfo = [String: AnyObject]()
        wheelsCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let wheelsCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Wheels :".localizedString(), value: Helper.toString(object: auctionModel.options?.wheels), info: wheelsCellInfo, height: Constants.CellHeightConstants.height_40)
        fourthDataArray.append(wheelsCell)
        
        //CruiseControlCell
        var cruiseControlCellInfo = [String: AnyObject]()
        cruiseControlCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let cruiseControlCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Cruise Control :".localizedString(), value: Helper.toString(object: auctionModel.options?.cruiseControl), info: cruiseControlCellInfo, height: Constants.CellHeightConstants.height_40)
        fourthDataArray.append(cruiseControlCell)
        
        //WindowsCell
        var windowsCellInfo = [String: AnyObject]()
        windowsCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let windowsCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Windows :".localizedString(), value: Helper.toString(object: auctionModel.options?.windows), info: windowsCellInfo, height: Constants.CellHeightConstants.height_40)
        fourthDataArray.append(windowsCell)
        
        fourthDataDict[Constants.UIKeys.cells] = fourthDataArray as AnyObject
        
        dataSource.append(fourthDataDict)
        
        //FifthSectionData
        var fifthDataDict = [String: AnyObject]()
        var fifthDataArray = [CellInfo]()
        
        fifthDataDict[Constants.UIKeys.sectionTitle] = "Mechnical Inspection".localizedString() as AnyObject
        fifthDataDict[Constants.UIKeys.isExpanded] = false as AnyObject
        fifthDataDict[Constants.UIKeys.sectionIcon] = #imageLiteral(resourceName: "mechanical_white")
        
        //EngineConditionCell
        var engineConditionCellInfo = [String: AnyObject]()
        engineConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        engineConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions(rawValue: "") as AnyObject
        let engineConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Engine".localizedString(), value: Helper.toString(object: auctionModel.mechincalInspection?.engineInspection), info: engineConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        fifthDataArray.append(engineConditionCell)
        
        //EngineDamageDescriptionCell
        let engineDamageDescCell = CellInfo(cellType: .DLAOtherInfoCell, placeHolder: "".localizedString(), value: Helper.toString(object: auctionModel.mechincalInspection?.engineInspectionInfo), info: nil, height: UITableView.automaticDimension)
        fifthDataArray.append(engineDamageDescCell)
        
        //GearConditionCell
        var gearConditionCellInfo = [String: AnyObject]()
        gearConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        gearConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.damaged as AnyObject
        let gearConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Gear :".localizedString(), value: Helper.toString(object: auctionModel.mechincalInspection?.gear), info: gearConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        fifthDataArray.append(gearConditionCell)
        
        //SuspensionConditionCell
        var suspensionConditionCellInfo = [String: AnyObject]()
        suspensionConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        suspensionConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let suspensionConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Suspension :".localizedString(), value: Helper.toString(object: auctionModel.mechincalInspection?.suspension), info: suspensionConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        fifthDataArray.append(suspensionConditionCell)
        
        //ACConditionCell
        var acConditionCellInfo = [String: AnyObject]()
        acConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        acConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let acConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "AC :".localizedString(), value: Helper.toString(object: auctionModel.mechincalInspection?.ac), info: acConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        fifthDataArray.append(acConditionCell)
        
        //DoorsConditionCell
        var doorsConditionCellInfo = [String: AnyObject]()
        doorsConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        doorsConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.damaged as AnyObject
        let doorsConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Doors :".localizedString(), value: Helper.toString(object: auctionModel.mechincalInspection?.doors), info: doorsConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        fifthDataArray.append(doorsConditionCell)
        
        //WindowsConditionCell
        var windowsConditionCellInfo = [String: AnyObject]()
        windowsConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        windowsConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let windowsConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Windows :".localizedString(), value: Helper.toString(object: auctionModel.mechincalInspection?.windowsInspection), info: windowsConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        fifthDataArray.append(windowsConditionCell)
        
        //AudioConditionCell
        var audioConditionCellInfo = [String: AnyObject]()
        audioConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        audioConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let audioConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Audio System :".localizedString(), value: Helper.toString(object: auctionModel.mechincalInspection?.audioSystem), info: audioConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        fifthDataArray.append(audioConditionCell)
        
        //FeaturesConditionCell
        var featuresConditionCellInfo = [String: AnyObject]()
        featuresConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        featuresConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let featuresConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Features :".localizedString(), value: Helper.toString(object: auctionModel.mechincalInspection?.features), info: featuresConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        fifthDataArray.append(featuresConditionCell)
        
        //TyreFRCell
        var tyreFRCellInfo = [String: AnyObject]()
        tyreFRCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.weekCell as AnyObject
        let fr = auctionModel.mechincalInspection?.getTyreCondition(key: ConstantAPIKeys.fl)
        tyreFRCellInfo[Constants.UIKeys.firstValue] = Helper.toString(object: fr?.week) as AnyObject
        tyreFRCellInfo[Constants.UIKeys.secondValue] = Helper.toString(object: fr?.year) as AnyObject
        let tyreFRCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Tire (Front Right) :".localizedString(), value: "", info: tyreFRCellInfo, height: Constants.CellHeightConstants.height_40)
        fifthDataArray.append(tyreFRCell)
        
        //TyreFLCell
        var tyreFLCellInfo = [String: AnyObject]()
        tyreFLCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.weekCell as AnyObject
        let fl = auctionModel.mechincalInspection?.getTyreCondition(key: ConstantAPIKeys.fl)
        tyreFLCellInfo[Constants.UIKeys.firstValue] = Helper.toString(object: fl?.week) as AnyObject
        tyreFLCellInfo[Constants.UIKeys.secondValue] = Helper.toString(object: fl?.year) as AnyObject
        let tyreFLCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Tire (Front Left) :".localizedString(), value: "", info: tyreFLCellInfo, height: Constants.CellHeightConstants.height_40)
        fifthDataArray.append(tyreFLCell)
        
        //TyreRRCell
        var tyreRRCellInfo = [String: AnyObject]()
        let rr = auctionModel.mechincalInspection?.getTyreCondition(key: ConstantAPIKeys.fl)
        tyreRRCellInfo[Constants.UIKeys.firstValue] = Helper.toString(object: rr?.week) as AnyObject
        tyreRRCellInfo[Constants.UIKeys.secondValue] = Helper.toString(object: rr?.year) as AnyObject
        tyreRRCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.weekCell as AnyObject
        let tyreRRCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Tire (Rear Right) :".localizedString(), value: "", info: tyreRRCellInfo, height: Constants.CellHeightConstants.height_40)
        fifthDataArray.append(tyreRRCell)
        
        //TyreRLCell
        var tyreRLCellInfo = [String: AnyObject]()
        tyreRLCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.weekCell as AnyObject
        let rl = auctionModel.mechincalInspection?.getTyreCondition(key: ConstantAPIKeys.fl)
        tyreRLCellInfo[Constants.UIKeys.firstValue] = Helper.toString(object: rl?.week) as AnyObject
        tyreRLCellInfo[Constants.UIKeys.secondValue] = Helper.toString(object: rl?.year) as AnyObject
        let tyreRLCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Tire (Rear Left) :".localizedString(), value: "", info: tyreRLCellInfo, height: Constants.CellHeightConstants.height_40)
        fifthDataArray.append(tyreRLCell)
        
        //MecInspectionOtherInfoCell
        var mecInspectionOtherCellInfo = [String: AnyObject]()
        mecInspectionOtherCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let mecInspectionOtherCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Other Information".localizedString(), value: "", info: mecInspectionOtherCellInfo, height: Constants.CellHeightConstants.height_40)
        fifthDataArray.append(mecInspectionOtherCell)
        
        //MecInspectionDescriptionCell
        let mecInspectioDescCell = CellInfo(cellType: .DLAOtherInfoCell, placeHolder: "".localizedString(), value: Helper.toString(object: auctionModel.mechincalInspection?.mechincalInspectionInfo), info: nil, height: UITableView.automaticDimension)
        fifthDataArray.append(mecInspectioDescCell)
        
        fifthDataDict[Constants.UIKeys.cells] = fifthDataArray as AnyObject
        
        dataSource.append(fifthDataDict)
        
        //SixthSectionData
        var sixthDataDict = [String: AnyObject]()
        var sixthDataArray = [CellInfo]()
        
        sixthDataDict[Constants.UIKeys.sectionTitle] = "Interior Inspection".localizedString() as AnyObject
        sixthDataDict[Constants.UIKeys.isExpanded] = false as AnyObject
        sixthDataDict[Constants.UIKeys.sectionIcon] = #imageLiteral(resourceName: "interior_white")
        
        //SeatsConditionCell
        var seatsConditionCellInfo = [String: AnyObject]()
        seatsConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        seatsConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let seatsConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Seats :".localizedString(), value: Helper.toString(object: auctionModel.interiorInspection?.seatsInspection), info: seatsConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        sixthDataArray.append(seatsConditionCell)
        
        //DashboardConditionCell
        var dashboardConditionCellInfo = [String: AnyObject]()
        dashboardConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        dashboardConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.damaged as AnyObject
        let dashboardConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Dashboard :".localizedString(), value: Helper.toString(object: auctionModel.interiorInspection?.dashboard), info: dashboardConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        sixthDataArray.append(dashboardConditionCell)
        
        //RoofConditionCell
        var roofConditionCellInfo = [String: AnyObject]()
        roofConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        roofConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let roofConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Roof :".localizedString(), value: Helper.toString(object: auctionModel.interiorInspection?.roof), info: roofConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        fifthDataArray.append(roofConditionCell)
        
        //ButtonsConditionCell
        var buttonsConditionCellInfo = [String: AnyObject]()
        buttonsConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        buttonsConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let buttonsConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Buttons :".localizedString(), value: Helper.toString(object: auctionModel.interiorInspection?.buttons), info: buttonsConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        sixthDataArray.append(buttonsConditionCell)
        
        //CurtainsConditionCell
        var curtainsConditionCellInfo = [String: AnyObject]()
        curtainsConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        curtainsConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.damaged as AnyObject
        let curtainsConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Curtains :".localizedString(), value: Helper.toString(object: auctionModel.interiorInspection?.curtains), info: curtainsConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        sixthDataArray.append(curtainsConditionCell)
        
        //MirrorsConditionCell
        var mirrorsConditionCellInfo = [String: AnyObject]()
        mirrorsConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        mirrorsConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let mirrorsConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Mirrors :".localizedString(), value: Helper.toString(object: auctionModel.interiorInspection?.mirrors), info: mirrorsConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        sixthDataArray.append(mirrorsConditionCell)
        
        //LightsConditionCell
        var lightsConditionCellInfo = [String: AnyObject]()
        lightsConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        lightsConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let lightsConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Lights :".localizedString(), value: Helper.toString(object: auctionModel.interiorInspection?.lights), info: lightsConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        sixthDataArray.append(lightsConditionCell)
        
        //IntInspectionOtherInfoCell
        var intInspectionOtherCellInfo = [String: AnyObject]()
        intInspectionOtherCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.specsCell as AnyObject
        let intInspectionOtherCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Other Information".localizedString(), value: "", info: intInspectionOtherCellInfo, height: Constants.CellHeightConstants.height_40)
        sixthDataArray.append(intInspectionOtherCell)
        
        //IntInspectionDescriptionCell
        let intInspectioDescCell = CellInfo(cellType: .DLAOtherInfoCell, placeHolder: "".localizedString(), value: Helper.toString(object: auctionModel.interiorInspection?.interiorInspectionInfo), info: nil, height: UITableView.automaticDimension)
        sixthDataArray.append(intInspectioDescCell)
        
        sixthDataDict[Constants.UIKeys.cells] = sixthDataArray as AnyObject
        
        dataSource.append(sixthDataDict)
        
        //SeventhSectionData
        var seventhDataDict = [String: AnyObject]()
        var seventhDataArray = [CellInfo]()
        
        seventhDataDict[Constants.UIKeys.sectionTitle] = "Body Inspection".localizedString() as AnyObject
        seventhDataDict[Constants.UIKeys.isExpanded] = false as AnyObject
        seventhDataDict[Constants.UIKeys.sectionIcon] = #imageLiteral(resourceName: "body_white")
        
        // Image
        var imageInfo = [String: AnyObject]()
        imageInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "bodyimg") as AnyObject
        let imageCell = CellInfo(cellType: .DAddAuctionImageCell, placeHolder: "", value:"" ,info: imageInfo, height: Constants.CellHeightConstants.height_260)
        seventhDataArray.append(imageCell)
        
        //BFConditionCell
        var bfConditionCellInfo = [String: AnyObject]()
        bfConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        bfConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let bfConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Bumper (Front)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.frontBumper), info: bfConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(bfConditionCell)
        
        //BRConditionCell
        var brConditionCellInfo = [String: AnyObject]()
        brConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        brConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.damaged as AnyObject
        let brConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Bumper (Rear)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.rearBumper), info: brConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(brConditionCell)
        
        //DFRConditionCell
        var dfrConditionCellInfo = [String: AnyObject]()
        dfrConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        dfrConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let dfrConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Doors (Front Right)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.frontRightDoors), info: dfrConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(dfrConditionCell)
        
        //DFLConditionCell
        var dflConditionCellInfo = [String: AnyObject]()
        dflConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        dflConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let dflConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Doors (Front Left".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.frontLeftDoors), info: dflConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(dflConditionCell)
        
        //DRRConditionCell
        var drrConditionCellInfo = [String: AnyObject]()
        drrConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        drrConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.damaged as AnyObject
        let drrConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Doors (Rear Right)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.rearRightDoors), info: drrConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(drrConditionCell)
        
        //DRLConditionCell
        var drlConditionCellInfo = [String: AnyObject]()
        drlConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        drlConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let drlConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Doors (Rear Left)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.rearLeftDoors), info: drlConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(drlConditionCell)
        
        //FFRConditionCell
        var ffrConditionCellInfo = [String: AnyObject]()
        ffrConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        ffrConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let ffrConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Fenders (Front Right)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.frontRightFenders), info: ffrConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(ffrConditionCell)
        
        //FFLConditionCell
        var fflConditionCellInfo = [String: AnyObject]()
        fflConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        fflConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let fflConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Fenders (Front Left)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.frontLeftFenders), info: fflConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(fflConditionCell)
        
        //FRRConditionCell
        var frrConditionCellInfo = [String: AnyObject]()
        frrConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        frrConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.damaged as AnyObject
        let frrConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Fenders (Rear Right)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.rearRightFenders), info: frrConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(frrConditionCell)
        
        //FRLConditionCell
        var frlConditionCellInfo = [String: AnyObject]()
        frlConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        frlConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let frlConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Fenders (Rear Left)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.rearLeftFenders), info: frlConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(frlConditionCell)
        
        //SSRConditionCell
        var ssrConditionCellInfo = [String: AnyObject]()
        ssrConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        ssrConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let ssrConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Side Skirt (Right)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.rightSideskirt), info: ssrConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(ssrConditionCell)
        
        //SSLConditionCell
        var sslConditionCellInfo = [String: AnyObject]()
        sslConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        sslConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.damaged as AnyObject
        let sslConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Side Skirt (Left)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.leftSideskirt), info: sslConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(sslConditionCell)
        
        //BonnetConditionCell
        var bonnetConditionCellInfo = [String: AnyObject]()
        bonnetConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        bonnetConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let bonnetConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Bonnet".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.bonnet), info: bonnetConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(bonnetConditionCell)
        
        //BootConditionCell
        var bootConditionCellInfo = [String: AnyObject]()
        bootConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        bootConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let bootConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Boot".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.boot), info: bootConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(bootConditionCell)
        
        //BIRoofConditionCell
        var biRoofConditionCellInfo = [String: AnyObject]()
        biRoofConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        biRoofConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let biRoofConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Roof".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.roofInspection), info: biRoofConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(biRoofConditionCell)
        
        //WFRConditionCell
        var wfrConditionCellInfo = [String: AnyObject]()
        wfrConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        wfrConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.damaged as AnyObject
        let wfrConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Wheels (Front Right)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.frontRightWheels), info: wfrConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(wfrConditionCell)
        
        //WFLConditionCell
        var wflConditionCellInfo = [String: AnyObject]()
        wflConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        wflConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let wflConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Wheels (Front Left)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.frontLeftWheels), info: wflConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(wflConditionCell)
        
        //WRRConditionCell
        var wrrConditionCellInfo = [String: AnyObject]()
        wrrConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        wrrConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let wrrConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Wheels (Rear Right)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.rearRightWheels), info: wrrConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(wrrConditionCell)
        
        //WRLConditionCell
        var wrlConditionCellInfo = [String: AnyObject]()
        wrlConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        wrlConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.damaged as AnyObject
        let wrlConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Wheels (Rear Left)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.rearLeftWheels), info: wrlConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(wrlConditionCell)
        
        //GFConditionCell
        var gfConditionCellInfo = [String: AnyObject]()
        gfConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        gfConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let gfConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Glass (Front)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.frontGlass), info: gfConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(gfConditionCell)
        
        //GRConditionCell
        var grConditionCellInfo = [String: AnyObject]()
        grConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        grConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let grConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Glass (Rear)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.rearGlass), info: grConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(grConditionCell)
        
        //GFRConditionCell
        var gfrConditionCellInfo = [String: AnyObject]()
        gfrConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        gfrConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let gfrConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Glass (Front Right)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.frontRightGlass), info: gfrConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(gfrConditionCell)
        
        //GFLConditionCell
        var gflConditionCellInfo = [String: AnyObject]()
        gflConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        gflConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.damaged as AnyObject
        let gflConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Glass (Front Left)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.frontLeftGlass), info: gflConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(gflConditionCell)
        
        //GRRConditionCell
        var grrConditionCellInfo = [String: AnyObject]()
        grrConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        grrConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let grrConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Glass (Rear Right)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.rearRightGlass), info: grrConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(grrConditionCell)
        
        //GRLConditionCell
        var grlConditionCellInfo = [String: AnyObject]()
        grlConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        grlConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let grlConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Glass (Rear Left)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.rearLeftGlass), info: grlConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(grlConditionCell)
        
        //GRRCConditionCell
        var grrcConditionCellInfo = [String: AnyObject]()
        grrcConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        grrcConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.damaged as AnyObject
        let grrcConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Glass (Rear Right Corner)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.rearRightCornerGlass), info: grrcConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(grrcConditionCell)
        
        //GRLCConditionCell
        var grlcConditionCellInfo = [String: AnyObject]()
        grlcConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        grlcConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let grlcConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Glass (Rear Left Corner)".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.rearLeftCornerGlass), info: grlcConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(grlcConditionCell)
        
        //SMConditionCell
        var smConditionCellInfo = [String: AnyObject]()
        smConditionCellInfo[Constants.UIKeys.dlaCellType] = DLACellType.imageLabelCell as AnyObject
        smConditionCellInfo[Constants.UIKeys.carConditionType] = CarConditions.perfect as AnyObject
        let smConditionCell = CellInfo(cellType: .DLACarDetailCell, placeHolder: "Side Mirrors".localizedString(), value: Helper.toString(object: auctionModel.bodyInspection?.sidemirrors), info: smConditionCellInfo, height: Constants.CellHeightConstants.height_40)
        seventhDataArray.append(smConditionCell)
        
        seventhDataDict[Constants.UIKeys.cells] = seventhDataArray as AnyObject
        
        dataSource.append(seventhDataDict)
        
        return dataSource
    }
    
    func requestAuctionDetailsAPI(auctionId:Int, completion: @escaping (DAuctionModel)-> Void) {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.vehicleId] = auctionId as AnyObject
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .acrCarDetail(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let auctionData = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let auctionModelData = try decoder.decode(DAuctionModel.self, from: auctionData)
                        completion(auctionModelData)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
    func requestAuctionHighestBidAPI(vehicleId:Int, completion: @escaping (HighestBidModel)-> Void) {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getAuctionHighestBid(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let auctionData = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let highestBidModel = try decoder.decode(HighestBidModel.self, from: auctionData)
                        completion(highestBidModel)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
    func requestSubmitBidAPI(param: APIParams, completion: @escaping (AuctionPlaceBidModel)-> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .auctionPlaceBid(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let auctionData = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let auctionPlaceBidModel = try decoder.decode(AuctionPlaceBidModel.self, from: auctionData)
                        completion(auctionPlaceBidModel)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
    func requestrequestBuyNowAPI(vehicleId: Int, completion: @escaping (Bool)-> Void) {
        
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .auctionPlaceBid(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                {
                    completion(true)
                }
            }
        }
    }
    
    func requestWinBidAPI(vehicleId: Int, completion: @escaping (Bool)-> Void) {
        
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .winBid(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                {
                    completion(true)
                }
            }
        }
    }
    
}
