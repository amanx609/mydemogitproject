//
//  DLABidInfoCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 11/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol DLABidInfoCellDelegate: class, BaseProtocol {
    func didTapBuyNow(cell: DLABidInfoCell)
    func didTapSubmitBid(cell: DLABidInfoCell)
    func didTapFirstQuickBid(cell: DLABidInfoCell)
    func didTapSecondQuickBid(cell: DLABidInfoCell)
    func didTapThirdQuickBid(cell: DLABidInfoCell)
    func tapNextKeyboard(cell: DLABidInfoCell)
    func didChangeText(cell: DLABidInfoCell, text: String)
}

class DLABidInfoCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var currentBidLabel: UILabel!
    @IBOutlet weak var lastBidLabel: UILabel!
    @IBOutlet weak var reservePriceLabel: UILabel!
    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var timeLeftProgressView: UIProgressView!
    @IBOutlet weak var timeLeftLabel: UILabel!
    @IBOutlet weak var enterBidView: UIView!
    @IBOutlet weak var enterBidTextField: UITextField!
    @IBOutlet weak var submitBidButton: UIButton!
    @IBOutlet weak var firstQuickBidLabel: UILabel!
    @IBOutlet weak var secondQuickBidLabel: UILabel!
    @IBOutlet weak var thirdQuickBidLabel: UILabel!
    
    //MARK: - Variables
    weak var delegate: DLABidInfoCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.timeLeftProgressView.progressTintColor = UIColor.greenTopHeaderColor
        self.timeLeftProgressView.transform = CGAffineTransform(rotationAngle: .pi)
        self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.shadowView.addShadow(ofColor:  UIColor.shadowColor, radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        self.containerView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.buyNowButton.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.enterBidView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.submitBidButton.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.firstQuickBidLabel.roundCorners(Constants.UIConstants.sizeRadius_13)
        self.secondQuickBidLabel.roundCorners(Constants.UIConstants.sizeRadius_13)
        self.thirdQuickBidLabel.roundCorners(Constants.UIConstants.sizeRadius_13)
        self.enterBidTextField.delegate =  self
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.enterBidTextField.keyboardType = cellInfo.keyboardType

        if let auctionModel = cellInfo.info?[Constants.UIKeys.cellInfo] as? DAuctionModel {
            self.reservePriceLabel.text = auctionModel.auctionDetails?.reservedPrice()
        }
        
        if let firstValue = cellInfo.info?[Constants.UIKeys.firstValue] as? String, !firstValue.isEmpty {
            self.currentBidLabel.text = "AED".localizedString() + " " + firstValue
        }
        
        if let secondValue = cellInfo.info?[Constants.UIKeys.secondValue] as? String,!secondValue.isEmpty {
            self.lastBidLabel.text = "AED".localizedString() + " " + secondValue
        }
        
        self.enterBidTextField.text = cellInfo.value

        //Timer
//        if let timerDuration = cellInfo.info?[Constants.UIKeys.duration] as? Double {
//            self.timeLeftLabel.text = timerDuration.convertToTimerFormat()
//        }
        
//        if let vehicle = cellInfo.info?[Constants.UIKeys.vehicleValue] as? Vehicle , let timerDuration = vehicle.timerDuration as? Double {
//            self.timeLeftLabel.text = timerDuration.convertToTimerFormat()
//            let totalTime = vehicle.totalTime ?? 0
//            let remainingTime = totalTime - timerDuration
//            timeLeftProgressView.progress = Float(remainingTime / totalTime)
//        }
    }
    
    //MARK: - IBActions
    @IBAction func tapBuyNow(_ sender: UIButton) {
        
        if let delegate = self.delegate {
            delegate.didTapBuyNow(cell: self)
        }
    }
    
    @IBAction func tapSubmitBid(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapSubmitBid(cell: self)
        }
    }
    
    @IBAction func tapFirstQuickBid(_ sender: UIButton) {
        
        if let delegate = self.delegate {
            delegate.didTapFirstQuickBid(cell: self)
        }
    }
    
    @IBAction func tapSecondQuickBid(_ sender: UIButton) {
        
        if let delegate = self.delegate {
            delegate.didTapSecondQuickBid(cell: self)
        }
    }
    
    @IBAction func tapThirdQuickBid(_ sender: UIButton) {
        
        if let delegate = self.delegate {
            delegate.didTapThirdQuickBid(cell: self)
        }
    }
}


extension DLABidInfoCell: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if let delegate = self.delegate {
      delegate.tapNextKeyboard(cell: self)
    }
    return true
  }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let delegate = delegate, let text = textField.text {
            delegate.didChangeText(cell: self, text: text)
        }
    }
 
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if let text = textField.text, let textRange = Range(range, in: text), let delegate = self.delegate {
      let finalText = text.replacingCharacters(in: textRange, with: string)
      delegate.didChangeText(cell: self, text: finalText)
    }
    return true
  }
}
