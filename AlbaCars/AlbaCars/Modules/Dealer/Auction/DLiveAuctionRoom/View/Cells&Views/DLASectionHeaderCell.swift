//
//  DLASectionHeaderCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 11/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DLASectionHeaderCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var placeholderImageView: UIImageView!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    //MARK: - Variables
    var buttonTappedCompletion: ((Bool) -> Void)?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    func setupView() {
        self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.shadowView.addShadow(ofColor:  UIColor.shadowColor, radius: Constants.UIConstants.sizeRadius_3half)
        self.containerView.roundCorners(Constants.UIConstants.sizeRadius_3half)
    }
    
    //MARK: - Public Methods
    func configureView(data: [String: AnyObject]) {
        if let sectionIcon = data[Constants.UIKeys.sectionIcon] as? UIImage {
            self.placeholderImageView.image = sectionIcon
        }
        if let sectionName = data[Constants.UIKeys.sectionTitle] as? String {
            self.placeholderLabel.text = sectionName
        }
        if let isExpanded = data[Constants.UIKeys.isExpanded] as? Bool {
            self.arrowImageView.image = isExpanded ? #imageLiteral(resourceName: "down_arrow_white") : #imageLiteral(resourceName: "up_arrow_white")
        }
    }
    
    
    //MARK: - IBActions
    @IBAction func tapSection(_ sender: UIButton) {
        if self.buttonTappedCompletion != nil {
            self.buttonTappedCompletion!(true)
        }
    }
}
