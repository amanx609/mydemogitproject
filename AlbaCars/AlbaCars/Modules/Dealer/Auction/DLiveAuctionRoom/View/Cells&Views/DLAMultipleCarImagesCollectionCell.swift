//
//  DLAMultipleCarImagesCollectionCell.swift
//  AlbaCars
//
//  Created by Narendra on 2/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DLAMultipleCarImagesCollectionCell: BaseCollectionViewCell,ReusableView, NibLoadableView {

    @IBOutlet weak var placeholderImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK: - Public Methods
    func configureView(image: String) {
        self.placeholderImageView.setImage(urlStr: image, placeHolderImage: UIImage(named: "placeholder"))
    }

}
