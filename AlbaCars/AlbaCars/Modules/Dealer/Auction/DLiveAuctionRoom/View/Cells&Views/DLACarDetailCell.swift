//
//  DLACarDetailCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 12/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DLACarDetailCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var placeholderLabel: UILabel!
    //ImageLabelView
    @IBOutlet weak var imageLabelView: UIView!
    @IBOutlet weak var checkmarkImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    //SpecsView
    @IBOutlet weak var specsView: UIView!
    @IBOutlet weak var specsLabel: UILabel!
    //WeekView
    @IBOutlet weak var weekView: UIView!
    @IBOutlet weak var weekLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupImageLabelView(cellInfo: CellInfo) {
        self.imageLabelView.isHidden = false
        self.specsView.isHidden = true
        self.weekView.isHidden = true
        self.placeholderLabel.text = cellInfo.placeHolder
        
//        if let cellInformation = cellInfo.info,
//            let carCondition = cellInformation[Constants.UIKeys.carConditionType] as? CarConditions {
//            self.checkmarkImageView.image = carCondition.icon
//            self.statusLabel.text = carCondition.title
//        }
        
        let value = cellInfo.value
        self.statusLabel.text = value
        
        if value == "Perfect".localizedString() {
            self.checkmarkImageView.image = CarConditions.perfect.icon
        } else if value == "Original".localizedString() {
            self.checkmarkImageView.image = CarConditions.perfect.icon
        } else {
            self.checkmarkImageView.image = CarConditions.damaged.icon
        }
        
    }
    
    private func setupSpecsView(cellInfo: CellInfo) {
        self.imageLabelView.isHidden = true
        self.specsView.isHidden = false
        self.weekView.isHidden = true
        self.placeholderLabel.text = cellInfo.placeHolder
        self.specsLabel.text = cellInfo.value
    }
    
    private func setupWeekView(cellInfo: CellInfo) {
        self.imageLabelView.isHidden = true
        self.specsView.isHidden = true
        self.weekView.isHidden = false
        self.placeholderLabel.text = cellInfo.placeHolder

        var firstValue = ""
        if let value = cellInfo.info?[Constants.UIKeys.firstValue] as? String, !value.isEmpty {
            firstValue = " " + value + " "
        }
        
        var secondValue = ""
        if let value = cellInfo.info?[Constants.UIKeys.secondValue] as? String,!value.isEmpty {
            secondValue = " " + value + " "
        }
        
        self.weekLabel.text = "Week".localizedString() + firstValue + "Year".localizedString() + secondValue
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        if let cellInformation = cellInfo.info,
           let cellType = cellInformation[Constants.UIKeys.dlaCellType] as? DLACellType {
            switch cellType {
            case .imageLabelCell:
                self.setupImageLabelView(cellInfo: cellInfo)
            case .specsCell:
                self.setupSpecsView(cellInfo: cellInfo)
            case .weekCell:
                self.setupWeekView(cellInfo: cellInfo)
            }
        }
    }
    
    func configureMyCarDetailsView(cellInfo: CellInfo) {
        self.imageLabelView.isHidden = true
        self.specsView.isHidden = false
        self.weekView.isHidden = true
        self.placeholderLabel.text = cellInfo.placeHolder
        self.specsLabel.text = cellInfo.value
    }
}
