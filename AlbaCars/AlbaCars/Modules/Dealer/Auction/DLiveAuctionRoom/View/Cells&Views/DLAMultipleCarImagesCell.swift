//
//  DLAMultipleCarImagesCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 11/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DLAMultipleCarImagesCell: BaseTableViewCell, NibLoadableView, ReusableView {

    //MARK: - IBOutlets
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var images: [Images] = []
    var currentPage = 0

    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setup() {
        Threads.performTaskInMainQueue {
            //self.gradientView.drawGradient(startColor: .blackGradientStartColor, endColor: .blackGradientEndColor)
        }
        self.setupCollectionView()
    }
    
    private func setupCollectionView() {
        self.registerNibs()
        self.imagesCollectionView.delegate = self
        self.imagesCollectionView.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal //depending upon direction of collection view
        self.imagesCollectionView?.setCollectionViewLayout(layout, animated: true)
    }
    
    private func registerNibs() {
        self.imagesCollectionView.register(DLAMultipleCarImagesCollectionCell.self)
    }
    
    //MARK: - Public Methods
     func configureView(cellInfo: CellInfo) {
        
           if let info = cellInfo.info,
              let images = info[Constants.UIKeys.images] as? [Images] {
            Threads.performTaskInMainQueue {
                self.images = images
                self.pageControl.numberOfPages = self.images.count
                self.imagesCollectionView.reloadData()
            }
           }
       }
    
    //MARK: - IBActions
    @IBAction func tapPreviousImage(_ sender: UIButton) {
        
        if currentPage == 0 {
            return
        }
        currentPage = currentPage - 1

        let indexToScrollTo = IndexPath(item: currentPage, section: 0)
        self.imagesCollectionView.scrollToItem(at: indexToScrollTo, at: .left, animated: true)
        
    }
    
    @IBAction func tapNextImage(_ sender: UIButton) {
        
        if images.count - 1 > currentPage  {
            currentPage = currentPage + 1
            let indexToScrollTo = IndexPath(item: currentPage, section: 0)
            self.imagesCollectionView.scrollToItem(at: indexToScrollTo, at: .right, animated: true)
        }
    }
}

//MARK:- UICollectionView Delegates & Datasource Methods
extension DLAMultipleCarImagesCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DLAMultipleCarImagesCollectionCell.className, for: indexPath) as? DLAMultipleCarImagesCollectionCell else {
            return UICollectionViewCell()
        }
        let image = images[indexPath.row].image
        cell.configureView(image: Helper.toString(object: image))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                           layout collectionViewLayout: UICollectionViewLayout,
                           sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
       }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in imagesCollectionView.visibleCells {
            let indexPath = imagesCollectionView.indexPath(for: cell)
            self.pageControl.currentPage = indexPath?.row ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
          self.pageControl.currentPage = indexPath.item
    }

}
