//
//  DLACarInfoCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 11/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DLACarInfoCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBActions
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var submodelLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var mileageLabel: UILabel!
    @IBOutlet weak var specsLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
                
        if let auctionModel = cellInfo.info?[Constants.UIKeys.cellInfo] as? DAuctionModel {
            self.nameLabel.text = auctionModel.basicDetails?.carTitle()
            
            self.brandLabel.attributedText = String.getAttributedText(firstText: "Brand : ".localizedString(), secondText:Helper.toString(object: auctionModel.basicDetails?.brandName), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.modelLabel.attributedText = String.getAttributedText(firstText: "Model : ".localizedString(), secondText:Helper.toString(object: auctionModel.basicDetails?.modelName), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.submodelLabel.attributedText = String.getAttributedText(firstText: "Sub Model : ".localizedString(), secondText:Helper.toString(object: auctionModel.basicDetails?.subModelName), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.yearLabel.attributedText = String.getAttributedText(firstText: "Year : ".localizedString(), secondText:Helper.toString(object: auctionModel.basicDetails?.year), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
            self.specsLabel.attributedText = String.getAttributedText(firstText: "Specs : ".localizedString(), secondText:Helper.toString(object: auctionModel.basicDetails?.specs), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
            self.mileageLabel.attributedText = String.getAttributedText(firstText: "Mileage : ".localizedString(), secondText:Helper.toString(object: auctionModel.basicDetails?.mileage) + " Km", firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)

        }
    }
    
}
