//
//  DLAOtherInfoCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 13/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DLAOtherInfoCell: BaseTableViewCell, NibLoadableView, ReusableView {

    //MARK: - IBOutlets
    @IBOutlet weak var descriptionLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.descriptionLabel.text = cellInfo.value
    }
}
