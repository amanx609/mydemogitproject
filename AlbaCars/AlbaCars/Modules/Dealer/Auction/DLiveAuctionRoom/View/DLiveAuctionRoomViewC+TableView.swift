//
//  DLiveAuctionRoomViewC+TableView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 11/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit


extension DLiveAuctionRoomViewC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.liveAuctionDataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionData = self.liveAuctionDataSource[section]
        if let cells = sectionData[Constants.UIKeys.cells] as? [CellInfo],
            let isExpanded = sectionData[Constants.UIKeys.isExpanded] as? Bool, isExpanded {
            return cells.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let cells = self.liveAuctionDataSource[indexPath.section][Constants.UIKeys.cells] as? [CellInfo] {
            return cells[indexPath.row].height
        }
        return Constants.CellHeightConstants.height_0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //NoHeaderForFirstSection
        if section == 0 { return nil }
        let sectionData = self.liveAuctionDataSource[section]
        if let cell = tableView.dequeueReusableCell(withIdentifier: DLASectionHeaderCell.className) as? DLASectionHeaderCell {
            cell.configureView(data: sectionData)
            cell.buttonTappedCompletion = { (success) in
                if let isExpanded = self.liveAuctionDataSource[section][Constants.UIKeys.isExpanded] as? Bool {
                    self.liveAuctionDataSource[section][Constants.UIKeys.isExpanded] = !isExpanded as AnyObject
                    Threads.performTaskInMainQueue {
                        self.liveAuctionTableView.reloadData()
                    }
                }
            }
            return cell
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //NoHeaderForFirstSection
        if section == 0 { return Constants.CellHeightConstants.height_0 }
        return Constants.CellHeightConstants.height_55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionData = self.liveAuctionDataSource[indexPath.section]
        if let cells = sectionData[Constants.UIKeys.cells] as? [CellInfo] {
            let cellInfo = cells[indexPath.row]
            return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
        }
        return UITableViewCell()
    }
}

extension DLiveAuctionRoomViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .DLAMultipleCarImagesCell:
            //            guard let cell = tableView.dequeueReusableCell(withIdentifier: DLAMultipleCarImagesCell.className) as? DLAMultipleCarImagesCell else {
            //                return UITableViewCell()
            //            }
            
            let cell: DLAMultipleCarImagesCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .DLACarInfoCell:
            //            guard let cell = tableView.dequeueReusableCell(withIdentifier: DLACarInfoCell.className) as? DLACarInfoCell else {
            //                return UITableViewCell()
            //            }
            let cell: DLACarInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .DLABidInfoCell:
            //            guard let cell = tableView.dequeueReusableCell(withIdentifier: DLABidInfoCell.className) as? DLABidInfoCell else {
            //                return UITableViewCell()
            //            }
            let cell: DLABidInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate =  self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .DLACarDetailCell:
            //            guard let cell = tableView.dequeueReusableCell(withIdentifier: DLACarDetailCell.className) as? DLACarDetailCell else {
            //                return UITableViewCell()
            //            }
            let cell: DLACarDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .DLAOtherInfoCell:
            //            guard let cell = tableView.dequeueReusableCell(withIdentifier: DLAOtherInfoCell.className) as? DLAOtherInfoCell else {
            //                return UITableViewCell()
            //            }
            let cell: DLAOtherInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .DAddAuctionImageCell:
            //            guard let cell = tableView.dequeueReusableCell(withIdentifier: DAddAuctionImageCell.className) as? DAddAuctionImageCell else {
            //                           return UITableViewCell()
            //                       }
            let cell: DAddAuctionImageCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

// DLABidInfoCellDelegate
extension DLiveAuctionRoomViewC : DLABidInfoCellDelegate {
    
    func tapNextKeyboard(cell: DLABidInfoCell) {
        self.view.endEditing(true)
    }
    
    func didChangeText(cell: DLABidInfoCell, text: String) {
        self.enterValue = text
    }
    
    func didTapBuyNow(cell: DLABidInfoCell) {
        
        
        if UserSession.shared.getUserId() == Helper.toInt(self.vehicle?.userId) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "You can't be a bidder on your auction.".localizedString())
            return
        }
        
        if let timerDuration = self.vehicle?.timerDuration {
            if timerDuration == 0 {
                return
            }
        }
        
        let price = Helper.toInt( self.auctionModel.auctionDetails?.price)
        if price > self.currentBid {
            self.requestBuyNowAPI(vehicleId:Helper.toInt(enterAuctionRoomModel.vehicle_id))
        } else {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Current bid is higher than reserved bid.".localizedString())
        }
    }
    
    func didTapSubmitBid(cell: DLABidInfoCell) {
        
//        print(UserSession.shared.getUserId())
//        print(self.vehicle?.userId)
        
        if UserSession.shared.getUserId() == Helper.toInt(self.vehicle?.userId) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "You can't be a bidder on your auction.".localizedString())
            return
        }
        
        if let timerDuration = self.vehicle?.timerDuration {
            if timerDuration == 0 {
                return
            }
        }
        self.submitBid(price: Helper.toInt(self.enterValue))
    }
    
    func didTapFirstQuickBid(cell: DLABidInfoCell) {
        let price =  self.currentBid + 500
        self.quickBidBid(price: price)
        self.enterValue = Helper.toString(object: price)
    }
    
    func didTapSecondQuickBid(cell: DLABidInfoCell) {
        let price =  self.currentBid + 1000
        self.quickBidBid(price: price)
        self.enterValue = Helper.toString(object: price)
    }
    
    func didTapThirdQuickBid(cell: DLABidInfoCell) {
        let price =  self.currentBid + 5000
        self.quickBidBid(price: price)
        self.enterValue = Helper.toString(object: price)
    }
    
}
