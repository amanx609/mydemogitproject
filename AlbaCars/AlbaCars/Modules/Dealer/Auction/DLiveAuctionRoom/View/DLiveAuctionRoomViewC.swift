//
//  DLiveAuctionRoomViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 11/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DLiveAuctionRoomViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var liveAuctionTableView: UITableView!
    
    //MARK: - Variables
    var viewModel: DLiveAuctionRoomVModeling?
    var liveAuctionDataSource: [[String: AnyObject]] = []
    var enterAuctionRoomModel = EnterAuctionRoomModel()
    var auctionModel = DAuctionModel()
    
    var vehicle:Vehicle?
    var currentBid = 0
    var lastBid = 0
    var enterValue = ""
    var images: [Images] = []
    var currentPage = 0
    var durationTimer : Timer?
    let refresh = UIRefreshControl()
    var timerDuration: Double?
    var totalTime: Double?

    //MARK:- LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.invalidateTimer()
    }
    
    deinit {
        self.invalidateTimer()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Alba Cars Auction".localizedString(), barColor: UIColor.redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setupTableView()
        self.requestAuctionAPI(auctionId: Helper.toInt(enterAuctionRoomModel.vehicle_id))
        self.setupTimer()
        self.initializeRefresh()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = DLiveAuctionRoomViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.liveAuctionTableView.delegate = self
        self.liveAuctionTableView.dataSource = self
        self.liveAuctionTableView.separatorStyle = .none
    }
    
    private func registerNibs() {
        self.liveAuctionTableView.register(DLAMultipleCarImagesCell.self)
        self.liveAuctionTableView.register(DLACarInfoCell.self)
        self.liveAuctionTableView.register(DLABidInfoCell.self)
        self.liveAuctionTableView.register(DLASectionHeaderCell.self)
        self.liveAuctionTableView.register(DLACarDetailCell.self)
        self.liveAuctionTableView.register(DLAOtherInfoCell.self)
        self.liveAuctionTableView.register(DAddAuctionImageCell.self)
    }
    
    private func initializeRefresh() {
        refresh.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        self.liveAuctionTableView.refreshControl = refresh
    }
    
    @objc func pullToRefresh() {
        refresh.endRefreshing()
        self.requestAuctionHighestBidAPI(vehicleId: Helper.toInt(self.enterAuctionRoomModel.vehicle_id))
    }
    
    func loadDataSource(auctionModel: DAuctionModel) {
        
        if let dataSource = self.viewModel?.getLiveAuctionRoomDataSource(auctionModel: auctionModel, vehicle: self.vehicle) {
            self.liveAuctionDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.liveAuctionTableView.reloadData()
            }
        }
    }
    
    private func setupTimer() {
        self.invalidateTimer()
        self.durationTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimeValueInDataSource), userInfo: nil, repeats: true)
    }
    
    private func invalidateTimer() {
        self.durationTimer?.invalidate()
        self.durationTimer = nil
    }
    
    //MARK: - Selector Methods
    @objc func updateTimeValueInDataSource() {
        
        if self.liveAuctionDataSource.count == 0 {
           return
        }
        
        if var timerDuration = self.timerDuration {
       // if var timerDuration = self.vehicle?.timerDuration {
           
            if timerDuration == 0 {
                self.invalidateTimer()
                if currentBid == lastBid,  currentBid > 0 {
                    self.requestWinBidAPI(vehicleId:Helper.toInt(enterAuctionRoomModel.vehicle_id))
                }
                return
            }
            
            timerDuration -= 1
            self.timerDuration = timerDuration
        }
        
        let sectionData = self.liveAuctionDataSource[0]
        
        if var cells = sectionData[Constants.UIKeys.cells] as? [CellInfo] {
//            var info = cells[2].info
//            info?[Constants.UIKeys.vehicleValue] = self.vehicle as AnyObject
//            cells[2].info = info
//            self.liveAuctionDataSource[0][Constants.UIKeys.cells] = cells as AnyObject
            let indexPath = IndexPath(row: 2, section: 0)
            
            if let cell = self.liveAuctionTableView.cellForRow(at: indexPath) as? DLABidInfoCell {
                
                if let timerDuration = self.timerDuration {
                    cell.timeLeftLabel.text = timerDuration.convertToTimerFormat()
                    let totalTime = self.totalTime ?? 0
                    let remainingTime = totalTime - timerDuration
                    cell.timeLeftProgressView.progress = Float(remainingTime / totalTime)
                }
            }
        }
        
    }
    
    public func calculatePercentage(value:Int,percentageVal:Int)->Int {
        let val = value * percentageVal
        return val / 100
    }
    
    func quickBidBid(price: Int) {
        
        let sectionData = self.liveAuctionDataSource[0]
        
        if var cells = sectionData[Constants.UIKeys.cells] as? [CellInfo] {
            cells[2].value = Helper.toString(object: price)
            self.liveAuctionDataSource[0][Constants.UIKeys.cells] = cells as AnyObject
            let indexPath = IndexPath(row: 2, section: 0)
            self.liveAuctionTableView.reloadRows(at:[indexPath], with: UITableView.RowAnimation.none)
        }
    }
    
    func updateCurrentBid(price: Int, vehicleId: Int) {
        
        if Helper.toInt(enterAuctionRoomModel.vehicle_id) == vehicleId {
            self.currentBid = price
            self.reloadBidDetails()
        }
    }
    
    func reloadBidDetails() {
        
        let sectionData = self.liveAuctionDataSource[0]
        
        if var cells = sectionData[Constants.UIKeys.cells] as? [CellInfo] {
            
            var info = cells[2].info
            info?[Constants.UIKeys.firstValue] = Helper.toString(object: self.currentBid) as AnyObject
            info?[Constants.UIKeys.secondValue] = Helper.toString(object: self.lastBid) as AnyObject
            cells[2].info = info
            cells[2].value = ""
            
            self.liveAuctionDataSource[0][Constants.UIKeys.cells] = cells as AnyObject
            let indexPath = IndexPath(row: 2, section: 0)
            self.liveAuctionTableView.reloadRows(at:[indexPath], with: UITableView.RowAnimation.none)
        }
    }
    
    func showAuctionPopupAt(liveAuctionPopupType: LiveAuctionPopupType,price: Int) {
        
        //LiveAuctionPopupType
        guard let liveAuctionPopup = LiveAuctionPopup.inistancefromNib() else { return }
        liveAuctionPopup.initializeViewWith(liveAuctionPopupType: liveAuctionPopupType, price: Helper.toString(object: price), handler: { (liveAuctionPopupType) in
            //guard let sSelf = self else { return }
            
            switch liveAuctionPopupType {
            case .confirmBid,.highestBidder:
                self.requestSubmitBidAPI(price: price)
            default:
                print("")
            }
        })
        
        liveAuctionPopup.showWithAnimated(animated: true)
    }
    
    
    //MARK: - API Methods
    func requestAuctionAPI(auctionId: Int) {
        
        self.viewModel?.requestAuctionDetailsAPI(auctionId: auctionId) { (responseData) in
            self.auctionModel = responseData
            self.timerDuration = self.auctionModel.auctionDetails?.getTimerDuration()
            self.totalTime = self.auctionModel.auctionDetails?.getTotalTime()

            self.loadDataSource(auctionModel: responseData)
            self.requestAuctionHighestBidAPI(vehicleId: Helper.toInt(self.enterAuctionRoomModel.vehicle_id))
        }
    }
    
    func requestBuyNowAPI(vehicleId: Int) {
        
        self.viewModel?.requestrequestBuyNowAPI(vehicleId: vehicleId) { (responseData) in
            Threads.performTaskInMainQueue {
                self.navigationController?.popToRootViewController(animated: true)
                Alert.showOkAlert(title: StringConstants.Text.AppName, message: StringConstants.Text.purchasedSuccessfully.localizedString())
            }
        }
    }
    
    func requestWinBidAPI(vehicleId: Int) {
        
        self.viewModel?.requestWinBidAPI(vehicleId: vehicleId) { (responseData) in
            Threads.performTaskInMainQueue {
                self.navigationController?.popToRootViewController(animated: true)
                Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Win Successfully".localizedString())

            }
        }
    }
    
    func requestAuctionHighestBidAPI(vehicleId: Int) {
        
        self.viewModel?.requestAuctionHighestBidAPI(vehicleId: vehicleId) { (responseData) in
            self.currentBid = Helper.toInt(responseData.price)
            self.lastBid = Helper.toInt(responseData.lastBidPrice)
            self.reloadBidDetails()
        }
    }
    
    func submitBid(price: Int) {
        
        if currentBid == lastBid, currentBid > 0, price > currentBid {
            self.showAuctionPopupAt(liveAuctionPopupType:.highestBidder, price: price)
        }  else if price > currentBid, price > 0 {
            self.showAuctionPopupAt(liveAuctionPopupType:.confirmBid, price: price)
        } else if price > 0 {
            self.showAuctionPopupAt(liveAuctionPopupType:.lowerBid, price: price)
        }
        //if  price < currentBid, price > 0
    }
    
    //MARK: - API Methods
    func requestSubmitBidAPI(price: Int) {
        
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.auctionBidRoomId] = Helper.toInt(enterAuctionRoomModel.id) as AnyObject
        param[ConstantAPIKeys.vehicleId] = Helper.toInt(enterAuctionRoomModel.vehicle_id) as AnyObject
        param[ConstantAPIKeys.price] = price as AnyObject
        
        self.viewModel?.requestSubmitBidAPI(param: param)  { (responseData) in
            
            self.currentBid = Helper.toInt(responseData.price)
            self.lastBid = Helper.toInt(responseData.price)
            self.enterValue = ""
            self.reloadBidDetails()
        }
    }
  
}

