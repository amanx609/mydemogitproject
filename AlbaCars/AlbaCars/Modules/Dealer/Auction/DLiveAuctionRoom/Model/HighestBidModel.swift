//
//  HighestBidModel.swift
//  AlbaCars
//
//  Created by Narendra on 2/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

class HighestBidModel: Codable {
    
    // Variables
    var bid_room_id: Int?
    var vehicle_id: Int?
    var price: Int?
    var lastBidPrice: Int?
}

