//
//  AuctionPlaceBidModel.swift
//  AlbaCars
//
//  Created by Narendra on 2/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class AuctionPlaceBidModel: Codable {
    
    // Variables
    var id: Int?
    var bid_room_id: Int?
    var vehicle_id: Int?
    var user_id: Int?
    var price: Int?
    var status: Int?
}

//* id : 4
//* bid_room_id : 2
//* vehicle_id : 1776
//* user_id : 9
//* price : 120
//* status : 0
//* updated_at : 2020-02-26T13:57:04.175Z
//* created_at : 2020-02-26T13:57:04.175Z

