//
//  DChooseAuctionTypeViewC.swift
//  AlbaCars
//
//  Created by Narendra on 3/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DChooseAuctionTypeViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var chooseAuctionTableView: UITableView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    
    //MARK: - Variables
    var viewModel: DChooseAuctionTypeVModeling?
    var auctionTypeDataSource: [CellInfo] = []
    var auctionModel = DAuctionModel()
    
    //MARK:- LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Auction Type".localizedString(), barColor: UIColor.redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setupTableView()
        self.setupView()
        self.requestAuctionTypeAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = DChooseAuctionTypeViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.chooseAuctionTableView.delegate = self
        self.chooseAuctionTableView.dataSource = self
        self.chooseAuctionTableView.separatorStyle = .none
        self.loadDataSource(auctionOfdayModel: nil)
    }
    
    private func registerNibs() {
        self.chooseAuctionTableView.register(DChooseAuctionTypeCell.self)
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        
        self.nextButton.setTitle("Next".localizedString(), for: .normal)
        self.nextButton.isHidden =  true
        self.gradientView.isHidden =  true
    }
    
    func loadDataSource(auctionOfdayModel: AuctionOfdayModel?) {
        
        if let dataSource = self.viewModel?.getAuctionTypeDataSource(auctionOfdayModel: auctionOfdayModel) {
            self.auctionTypeDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.chooseAuctionTableView.reloadData()
            }
        }
    }
    
    
    func payNow(chooseAuctionType: ChooseAuctionType,auctionOfday: AuctionOfday?) {
        
        let paymentDetails = PaymentDetailsData(viewController: self, totalAmmount: Helper.toDouble(auctionOfday?.price), referenceId: Helper.toInt(self.auctionModel.basicDetails?.id), paymentServiceCategoryId: PaymentServiceCategoryId.auctions,vatAmmount: 0)
        
        PaymentManager.sharedInstance.initiatePayment(paymentDetailsData: paymentDetails) { (success, paymentID) in
            if success {
                Threads.performTaskInMainQueue {
                    self.goToNextPage(chooseAuctionType: chooseAuctionType, auctionOfday: auctionOfday)
                }
            }
        }
    }
    
    func goToNextPage(chooseAuctionType: ChooseAuctionType,auctionOfday: AuctionOfday?) {
        let auctionConfirmationVC = DIConfigurator.sharedInstance.getDAuctionConfirmationVC()
        auctionConfirmationVC.auctionModel = self.auctionModel
        auctionConfirmationVC.chooseAuctionType = chooseAuctionType
        auctionConfirmationVC.auctionOfday = auctionOfday
        self.navigationController?.pushViewController(auctionConfirmationVC, animated: true)
    }
    
    //MARK: - API Methods
    
    func requestAuctionTypeAPI() {
        
        self.viewModel?.requestAuctionTypeAPI() { (auctionOfday) in
            self.loadDataSource(auctionOfdayModel: auctionOfday)
        }
    }
    
    @IBAction func tapNextButton(_ sender: UIButton) {
        self.goToNextPage(chooseAuctionType: .normal, auctionOfday: nil)
    }
    
}
