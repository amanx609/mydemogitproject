//
//  DChooseAuctionTypeCell.swift
//  AlbaCars
//
//  Created by Narendra on 3/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

import UIKit

protocol DChooseAuctionTypeCellDelegate: class, BaseProtocol {
    func didTapRadioButton(cell: DChooseAuctionTypeCell)
    func didTapPayButton(cell: DChooseAuctionTypeCell,auctionOfday: AuctionOfday)
}

class DChooseAuctionTypeCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBActions
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var radioButton: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var notFoundLabel: UILabel!

    
    //MARK: - Variables
    var dataSource = [AuctionOfday]()
    weak var delegate: DChooseAuctionTypeCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setup() {
        Threads.performTaskInMainQueue {
            self.bgView.makeLayer(color: .gray, boarderWidth: 1, round: 10)
        }
        self.setUpTableView()
        
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.tableView.register(AuctionPriceCell.self)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo,indexPath: IndexPath) {
        
        self.titleLabel.text = cellInfo.placeHolder
        
        if let info = cellInfo.info {
            self.radioButton.isSelected = Helper.toBool(info[Constants.UIKeys.isSelected])
          
            if let itemsCoveredData = info[Constants.UIKeys.cellInfo] as? [AuctionOfday] {
                self.notFoundLabel.isHidden = true
                self.dataSource = itemsCoveredData
                self.tableView.reloadData()
            } else {
                self.notFoundLabel.isHidden = false
            }
            
            if self.radioButton.isSelected == true, indexPath.row != 2 {
                self.bgView.isHidden = false
            } else {
                self.bgView.isHidden = true
            }
            
        } else {
            self.notFoundLabel.isHidden = false
            self.bgView.isHidden = true
        }
    }
    
    //MARK: - IBActions
    @IBAction func tapChooseAuction(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapRadioButton(cell: self)
        }
    }
}


//MARK:- UITableView Delegates & Datasource Methods
extension DChooseAuctionTypeCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getCell(tableView, indexPath: indexPath)
    }
}

extension DChooseAuctionTypeCell {
    
    func getCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: AuctionPriceCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.configureView(auctionOfday: self.dataSource[indexPath.row])
        return cell
    }
    
}

// Mark: AuctionPriceCellDelegate
extension DChooseAuctionTypeCell:AuctionPriceCellDelegate {
    
    func didTapPayButton(cell: AuctionPriceCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else { return }
        let auctionOfday = self.dataSource[indexPath.row]
        if let delegate = self.delegate {
            delegate.didTapPayButton(cell: self, auctionOfday: auctionOfday)
        }
    }
        
}
