//
//  AuctionPriceCell.swift
//  AlbaCars
//
//  Created by Narendra on 3/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol AuctionPriceCellDelegate: class, BaseProtocol {
    func didTapPayButton(cell: AuctionPriceCell)
}

class AuctionPriceCell: BaseTableViewCell, NibLoadableView, ReusableView {

    //MARK: - IBActions
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    //MARK: - Variables
    weak var delegate: AuctionPriceCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setup() {
        Threads.performTaskInMainQueue {
            self.priceLabel.makeLayer(color: .gray, boarderWidth: 1, round: 10)
        }
    }
    
    //MARK: - Public Methods
    func configureView(auctionOfday: AuctionOfday) {
        let dateStr = Helper.toString(object: auctionOfday.time).getDateStringFrom(inputFormat: Constants.Format.timeFormat24hhmmss, outputFormat: Constants.Format.timeFormathmma)
        self.timeLabel.text = dateStr
        self.priceLabel.text = Helper.toString(object: auctionOfday.getFormattedPrice())
    }
    
    //MARK: - IBActions
     @IBAction func tapPay(_ sender: UIButton) {
         
        if let delegate = self.delegate {
            delegate.didTapPayButton(cell: self)
        }
     }
    
}
