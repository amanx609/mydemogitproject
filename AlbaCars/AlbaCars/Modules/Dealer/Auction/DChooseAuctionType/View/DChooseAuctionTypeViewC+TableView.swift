//
//  DChooseAuctionTypeViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 3/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension DChooseAuctionTypeViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.auctionTypeDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.auctionTypeDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 2 {
            return self.auctionTypeDataSource[indexPath.row].height
        } else {
            
            let cellInfo = self.auctionTypeDataSource[indexPath.row]
            let isSelected = Helper.toBool(cellInfo.info?[Constants.UIKeys.isSelected])
            return isSelected == true ? self.auctionTypeDataSource[indexPath.row].height : Constants.CellHeightConstants.height_70
        }
    }
}

extension DChooseAuctionTypeViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .DChooseAuctionTypeCell:
            let cell: DChooseAuctionTypeCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo, indexPath: indexPath)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

// Mark: DChooseAuctionTypeCellDelegate
extension DChooseAuctionTypeViewC: DChooseAuctionTypeCellDelegate {
    
    func didTapRadioButton(cell: DChooseAuctionTypeCell) {
        var index = 0
        for _ in self.auctionTypeDataSource {
            self.auctionTypeDataSource[index].info?[Constants.UIKeys.isSelected] = false as AnyObject
            index = index + 1
        }
        
        guard let indexPath = self.chooseAuctionTableView.indexPath(for: cell) else { return }
        self.auctionTypeDataSource[indexPath.row].info?[Constants.UIKeys.isSelected] = true as AnyObject
        Threads.performTaskInMainQueue {
            self.chooseAuctionTableView.reloadData()
        }
        
        if indexPath.row == 2 {
            self.nextButton.isHidden =  false
            self.gradientView.isHidden =  false
        } else {
            self.nextButton.isHidden =  true
            self.gradientView.isHidden =  true
        }
    }
    
    func didTapPayButton(cell: DChooseAuctionTypeCell, auctionOfday: AuctionOfday) {
        guard let indexPath = self.chooseAuctionTableView.indexPath(for: cell) else { return }
        
        if Helper.compareTime(currentTime: Helper.toString(object: auctionOfday.time), selectedTime: Helper.getCurrentTime(inFormat: Constants.Format.timeFormat24hhmmss)) {
            if indexPath.row == 0 {
                self.payNow(chooseAuctionType: .auctionDay, auctionOfday: auctionOfday)
            } else if indexPath.row == 1 {
                self.payNow(chooseAuctionType: .featured, auctionOfday: auctionOfday)
            }
        } else {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Invalid time.".localizedString())
        }
        
    }
    
}
