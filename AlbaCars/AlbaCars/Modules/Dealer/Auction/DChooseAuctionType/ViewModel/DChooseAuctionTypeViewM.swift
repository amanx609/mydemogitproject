//
//  DChooseAuctionTypeViewM.swift
//  AlbaCars
//
//  Created by Narendra on 3/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
import Foundation

protocol DChooseAuctionTypeVModeling: BaseVModeling {
    func getAuctionTypeDataSource(auctionOfdayModel: AuctionOfdayModel?) -> [CellInfo]
    func requestAuctionTypeAPI(completion: @escaping (AuctionOfdayModel) -> Void)
}

class DChooseAuctionTypeViewM: BaseViewM, DChooseAuctionTypeVModeling {
        
    func getAuctionTypeDataSource(auctionOfdayModel: AuctionOfdayModel?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Auction of the day Cell
        var auctionDayHeight = Constants.CellHeightConstants.height_80
        
        if let auctionOfday = auctionOfdayModel?.AuctionOfday, auctionOfday.count > 0 {
            auctionDayHeight = auctionDayHeight + CGFloat((auctionOfday.count * 50))
        } else {
            auctionDayHeight = auctionDayHeight + 100
        }
                
        var auctionDayInfo = [String: AnyObject]()
        auctionDayInfo[Constants.UIKeys.isSelected] = false as AnyObject
        auctionDayInfo[Constants.UIKeys.cellInfo] = auctionOfdayModel?.AuctionOfday as AnyObject
        let auctionDayCell = CellInfo(cellType: .DChooseAuctionTypeCell, placeHolder: "Auction of the day", value: "", info: auctionDayInfo, height: auctionDayHeight)
        array.append(auctionDayCell)
        
        // Featured Cell
        
        var featuredHeight = Constants.CellHeightConstants.height_80
        
        if let featuredAuction = auctionOfdayModel?.FeaturedAuction, featuredAuction.count > 0 {
            featuredHeight = featuredHeight + CGFloat((featuredAuction.count * 50))
        } else {
            featuredHeight = featuredHeight + 100
        }
        
        var featuredInfo = [String: AnyObject]()
        featuredInfo[Constants.UIKeys.isSelected] = false as AnyObject
        featuredInfo[Constants.UIKeys.cellInfo] = auctionOfdayModel?.FeaturedAuction as AnyObject
        let featuredCell = CellInfo(cellType: .DChooseAuctionTypeCell, placeHolder: "Featured".localizedString(), value:"" ,info: featuredInfo, height: featuredHeight)
        array.append(featuredCell)
        
        var normalInfo = [String: AnyObject]()
        normalInfo[Constants.UIKeys.isSelected] = false as AnyObject
        let normalCell = CellInfo(cellType: .DChooseAuctionTypeCell, placeHolder: "Normal".localizedString(), value:"" ,info: normalInfo
            , height: Constants.CellHeightConstants.height_70)
        array.append(normalCell)
        return array
    }
   
    func requestAuctionTypeAPI(completion: @escaping (AuctionOfdayModel) -> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .auctionPrices)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let auctionOfdayModel = try decoder.decode(AuctionOfdayModel.self, from: resultData)
                        completion(auctionOfdayModel)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
        
    }
    
    
}
