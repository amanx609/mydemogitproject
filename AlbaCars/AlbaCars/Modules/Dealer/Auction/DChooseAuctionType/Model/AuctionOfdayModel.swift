//
//  AuctionOfdayModel.swift
//  AlbaCars
//
//  Created by Narendra on 3/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class AuctionOfdayModel: Codable {
    
    // Variables
    var AuctionOfday: [AuctionOfday]?
    var FeaturedAuction: [AuctionOfday]?
}

class AuctionOfday: Codable {
    
    // Variables
    var price: Int?
    var time: String?
    
    func getFormattedPrice()-> String {
        
        return "Pay AED " + Helper.toString(object: price)
    }
}


