//
//  DAuctionFilterViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DAuctionFilterVModeling: BaseVModeling {
    func getAuctionFilterDataSource(selectedData:[String:AnyObject]) -> [CellInfo]
    func requestVehicleBrandAPI(completion: @escaping (Bool, VehicleBrandList)-> Void)
    func getFilterParams(dataSource: [CellInfo]) -> APIParams
}

class DAuctionFilterViewM: BaseViewM, DAuctionFilterVModeling {
    
    func getAuctionFilterDataSource(selectedData:[String:AnyObject]) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //ChooseBrandsCell
        var chooseBrandsInfo = [String: AnyObject]()
        chooseBrandsInfo[Constants.UIKeys.headerTitle] = StringConstants.Text.chooseBrands.localizedString() as AnyObject
        chooseBrandsInfo[Constants.UIKeys.placeholderText] = StringConstants.Text.chooseBrands.localizedString() as AnyObject
        chooseBrandsInfo[Constants.UIKeys.textColor] = UIColor.redButtonColor as AnyObject
        let chooseBrandsCell = CellInfo(cellType: .SelectYearTableCell, placeHolder: StringConstants.Text.chooseBrands.localizedString(), value: "", info: chooseBrandsInfo, height: Constants.CellHeightConstants.height_120)
        array.append(chooseBrandsCell)
        
        //MinimumYearCell
        var minYearsInfo = [String: AnyObject]()
        minYearsInfo[Constants.UIKeys.headerTitle] = StringConstants.Text.selectMinimumYear.localizedString() as AnyObject
        minYearsInfo[Constants.UIKeys.placeholderText] = StringConstants.Text.selectMinimumYear.localizedString() as AnyObject
        minYearsInfo[Constants.UIKeys.textColor] = UIColor.black as AnyObject
        let minYearsCell =  CellInfo(cellType: .SelectYearTableCell, placeHolder: StringConstants.Text.chooseBrands.localizedString(), value: Helper.toString(object: selectedData[ConstantAPIKeys.year]), info: minYearsInfo, height: Constants.CellHeightConstants.height_120)
        array.append(minYearsCell)
        
        //SelectMileageCell
        let selectMileageCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: StringConstants.Text.selectMileage.localizedString(), height: Constants.CellHeightConstants.height_50)
        array.append(selectMileageCell)
        
        // Mileage RangeCell
        var rangeCellInfo = [String: AnyObject]()
        rangeCellInfo[Constants.UIKeys.minimum] = StringConstants.Text.minimum.localizedString() as AnyObject
        rangeCellInfo[Constants.UIKeys.maximum] = StringConstants.Text.maximum.localizedString() as AnyObject
        rangeCellInfo[Constants.UIKeys.textColor] = UIColor.black as AnyObject
        rangeCellInfo[Constants.UIKeys.firstValue] = Helper.toString(object:selectedData[ConstantAPIKeys.minMileage]) as AnyObject
        rangeCellInfo[Constants.UIKeys.secondValue] = Helper.toString(object:selectedData[ConstantAPIKeys.maxMileage]) as AnyObject
        let mileageRangeCell = CellInfo(cellType: .MileageTextFieldCell, placeHolder: StringConstants.Text.carSelectionQuestion.localizedString(), value: "", info: rangeCellInfo, height: Constants.CellHeightConstants.height_120)
        array.append(mileageRangeCell)
        
        //SpecsCell
        var specsInfo = [String: AnyObject]()
        specsInfo[Constants.UIKeys.headerTitle] = StringConstants.Text.specs.localizedString() as AnyObject
        specsInfo[Constants.UIKeys.placeholderText] = StringConstants.Text.specs.localizedString() as AnyObject
        specsInfo[Constants.UIKeys.textColor] = UIColor.black as AnyObject
        let specsCell = CellInfo(cellType: .SelectYearTableCell, placeHolder:  StringConstants.Text.specs.localizedString(), value: Helper.toString(object: selectedData[ConstantAPIKeys.specs]), info: specsInfo, height: Constants.CellHeightConstants.height_120)
        array.append(specsCell)
        
        return array
    }
    
    //MARK: - API Methods
    func requestVehicleBrandAPI(completion: @escaping (Bool, VehicleBrandList)-> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .vehicleBrand)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let _ = safeResponse[ConstantAPIKeys.result] as? [[String: AnyObject]],
                    let vehicleBrandsData = safeResponse.toJSONData(),
                    let vehicleBrandsList = VehicleBrandList(jsonData: vehicleBrandsData) {
                    completion(success, vehicleBrandsList)
                }
            }
        }
    }
    
    func getFilterParams(dataSource: [CellInfo]) -> APIParams {
        var params: APIParams = APIParams()
        
        let brandIDs = Helper.toString(object: dataSource[0].info?[Constants.UIKeys.id])
        
        if !brandIDs.isEmpty {
            params[ConstantAPIKeys.brand] = brandIDs as AnyObject
        }
        
        let year = Helper.toString(object: dataSource[1].value)
        
        if !year.isEmpty {
            params[ConstantAPIKeys.year] = year as AnyObject
        }
        
        let minMileage = Helper.toString(object: dataSource[3].info?[Constants.UIKeys.firstValue])
        
        if !minMileage.isEmpty {
            params[ConstantAPIKeys.minMileage] = minMileage as AnyObject
        }
        
        let maxMileage = Helper.toString(object: dataSource[3].info?[Constants.UIKeys.secondValue])
        
        if !maxMileage.isEmpty {
            params[ConstantAPIKeys.maxMileage] = maxMileage as AnyObject
        }
        
        let specs = Helper.toString(object: dataSource[4].value)
        
        if !specs.isEmpty {
            params[ConstantAPIKeys.specs] = specs as AnyObject
        }
        
        return params
    }
}
