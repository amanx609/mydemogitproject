//
//  DAuctionFilterViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DAuctionFilterViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var auctionFilterTableView: UITableView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var saveButton: UIButton!
    
    //MARK: - Variables
    var viewModel: DAuctionFilterVModeling?
    var auctionFilterDataSource: [CellInfo] = []
    var selectedParams: APIParams = APIParams()
    var selectParams:((APIParams?)-> Void)?
    var vehicleBrandDataSource: [VehicleBrand] = []
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func navigationButtonTapped(_ sender: AnyObject) {
        guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
        switch buttonType {
        case .cross:
            self.dismiss(animated: true, completion: nil)
        case .reset:
            self.resetFilter()
        default: break
        }
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Filter".localizedString(), leftBarButtonsType: [.cross], rightBarButtonsType: [.reset])
        self.recheckVM()
        self.setUpTableView()
        self.setUpButton()
        self.loadDataSource()
        self.requestVehicleBrandAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = DAuctionFilterViewM()
        }
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.auctionFilterTableView.delegate = self
        self.auctionFilterTableView.dataSource = self
        self.auctionFilterTableView.separatorStyle = .none
        self.auctionFilterTableView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.auctionFilterTableView.register(SelectYearTableCell.self)
        self.auctionFilterTableView.register(SideMenuCell.self)
        self.auctionFilterTableView.register(MileageTextFieldCell.self)
    }
    
    private func setUpButton() {
        self.saveButton.setTitle("Apply & Save".localizedString(), for: .normal)
        self.saveButton.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.gradientView.roundCorners(Constants.UIConstants.sizeRadius_7)
        Threads.performTaskInMainQueue {
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getAuctionFilterDataSource(selectedData: self.selectedParams) {
            self.auctionFilterDataSource = dataSource
            self.auctionFilterTableView.reloadData()
        }
    }
    
    func showDropDownAt(indexPath: IndexPath, title: String) {
        self.view.endEditing(true)
        switch indexPath.row {
        case 0:
            if self.vehicleBrandDataSource.count == 0 {
                self.requestVehicleBrandAPI()
                return
            }
            
            guard let multipleSelectListPopup = MultipleSelectListPopup.inistancefromNib() else { return }
            multipleSelectListPopup.initializeViewForVehicleBrand(brands: self.vehicleBrandDataSource) { (vehicleBrands) in
                //  print(vehicleBrands)
                var brandName = ""
                var brandID = ""
                
                if vehicleBrands.count > 0 {
                    for brands in vehicleBrands {
                        brandName = brandName == "" ? Helper.toString(object:  brands.title) : brandName + ", \(Helper.toString(object:  brands.title))"
                        brandID = brandID == "" ? Helper.toString(object:  brands.id) : brandID + ",\(Helper.toString(object:  brands.id))"
                    }
                    self.auctionFilterDataSource[indexPath.row].info?[Constants.UIKeys.id] = brandID as AnyObject
                    self.auctionFilterDataSource[indexPath.row].value = brandName
                  //  self.auctionFilterTableView.reloadData()
                } else {
                    self.auctionFilterDataSource[indexPath.row].info?[Constants.UIKeys.id] = "" as AnyObject
                    self.auctionFilterDataSource[indexPath.row].value = ""
                }
                self.auctionFilterTableView.reloadData()                
            }
            multipleSelectListPopup.showWithAnimated(animated: true)
            
        case 1:
            guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
            let yearsList = Helper.getListOfYearsFrom(2010)
            listPopupView.initializeViewWith(title: title, arrayList: yearsList, key: Constants.UIKeys.year) { [weak self] (response) in
                guard let sSelf = self,
                    let year = response[Constants.UIKeys.year] as? String else { return }
                sSelf.auctionFilterDataSource[indexPath.row].value = year
                sSelf.auctionFilterTableView.reloadData()
                //  sSelf.selectedYear = year == "None".localizedString() ? "" : year
            }
            listPopupView.showWithAnimated(animated: true)
        case 4:
            guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
            listPopupView.initializeViewWith(title: title, arrayList: DropDownType.specs.dataList, key: DropDownType.specs.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let specs = response[DropDownType.specs.rawValue] as? String else { return }
                sSelf.auctionFilterDataSource[indexPath.row].value = specs
                sSelf.auctionFilterTableView.reloadData()
            }
            listPopupView.showWithAnimated(animated: true)
            
        default:
            break
        }
    }
    
    func resetFilter() {
        self.selectedParams = APIParams()
        for i in 0..<vehicleBrandDataSource.count {
            self.vehicleBrandDataSource[i].isSelected = false
        }
        self.loadDataSource()
        Threads.performTaskInMainQueue {
            self.auctionFilterTableView.reloadData()
        }
    }
    
    //MARK: - API Methods
    func requestVehicleBrandAPI() {
        self.viewModel?.requestVehicleBrandAPI(completion: { (success, vehicleBrandList) in
            if success {
                
                if let vehicleBrands = vehicleBrandList.vehicleBrands {
                    //self.vehicleBrandDataSource = vehicleBrands
                    var brandName = ""
                    var brandIDs = ""
                    
                    let selectedBrand = Helper.toString(object: self.selectedParams[ConstantAPIKeys.brand])
                    if !selectedBrand.isEmpty {
                        for vehicleBrand in vehicleBrands {
                            
                            let brandID = Helper.toString(object: vehicleBrand.id)
                            if selectedBrand.contains(brandID) {
                                vehicleBrand.isSelected = true
                                self.vehicleBrandDataSource.append(vehicleBrand)
                                brandName = brandName == "" ? Helper.toString(object:  vehicleBrand.title) : brandName + ", \(Helper.toString(object:  vehicleBrand.title))"
                                brandIDs = brandIDs == "" ? Helper.toString(object:  vehicleBrand.id) : brandIDs + ",\(Helper.toString(object:  vehicleBrand.id))"
                            }
                            else {
                                self.vehicleBrandDataSource.append(vehicleBrand)
                            }
                        }
                    } else {
                        self.vehicleBrandDataSource = vehicleBrands
                    }
                    
                    self.auctionFilterDataSource[0].info?[Constants.UIKeys.id] = brandIDs as AnyObject
                    self.auctionFilterDataSource[0].value = brandName
                    self.auctionFilterTableView.reloadData()
                }
                
                //                self.getDatasourceForFilterOptions(self.filterType)
            }
        })
    }
        
    //MARK: - IBActions
    @IBAction func tapSave(_ sender: UIButton) {
        let param = self.viewModel?.getFilterParams(dataSource: self.auctionFilterDataSource)
        self.selectParams?(param)
        self.dismiss(animated: true)
    }
}
