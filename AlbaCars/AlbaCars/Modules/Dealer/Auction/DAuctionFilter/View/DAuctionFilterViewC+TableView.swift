//
//  DAuctionFilterViewC+TableView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension DAuctionFilterViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.auctionFilterDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.auctionFilterDataSource[indexPath.row]
        return self.getCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.auctionFilterDataSource[indexPath.row].height
    }
}

extension DAuctionFilterViewC {
    
    func getCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
      guard let cellType = cellInfo.cellType else { return UITableViewCell() }
      switch cellType {
      case .SelectYearTableCell:
        let cell: SelectYearTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.configureAuctionFilterView(cellInfo: cellInfo)
        return cell
      case .SideMenuCell:
        let cell: SideMenuCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureViewWithSmallerFont(cellInfo: cellInfo)
        return cell
      case .MileageTextFieldCell:
        let cell: MileageTextFieldCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.configureAuctionFilterView(cellInfo: cellInfo)
        return cell
      default:
        return UITableViewCell()
      }
    }
}

// SelectYearTableCellDelegate
extension DAuctionFilterViewC:SelectYearTableCellDelegate {
    func didTapOnLabel(cell: SelectYearTableCell) {
        guard let indexPath = self.auctionFilterTableView.indexPath(for: cell) else { return }
        let cellInfo = self.auctionFilterDataSource[indexPath.row]
        self.showDropDownAt(indexPath: indexPath, title: cellInfo.placeHolder)
    }
}

// MileageTextFieldCellDelegate
extension DAuctionFilterViewC:MileageTextFieldCellDelegate {
    func didChangeMinimumMileage(cell: MileageTextFieldCell, text: String) {
        
    }
    
    func didChangeMaximumMileage(cell: MileageTextFieldCell, text: String) {
        
    }
    
    func didTapMinimumMileage(cell: MileageTextFieldCell) {
        guard let indexPath = self.auctionFilterTableView.indexPath(for: cell) else { return }
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: DropDownType.minMileageRange.title, arrayList: DropDownType.minMileageRange.dataList, key: DropDownType.minMileageRange.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let minMileageRange = response[DropDownType.minMileageRange.rawValue] as? String else { return }
           // sSelf.auctionFilterDataSource[indexPath.row].value = minMileageRange
            sSelf.auctionFilterDataSource[indexPath.row].info?[Constants.UIKeys.firstValue] = minMileageRange as AnyObject
            sSelf.auctionFilterTableView.reloadData()
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    func didTapMaximumMileage(cell: MileageTextFieldCell) {
        guard let indexPath = self.auctionFilterTableView.indexPath(for: cell) else { return }
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: DropDownType.maxMileageRange.title, arrayList: DropDownType.maxMileageRange.dataList, key: DropDownType.maxMileageRange.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let maxMileageRange = response[DropDownType.maxMileageRange.rawValue] as? String else { return }
            sSelf.auctionFilterDataSource[indexPath.row].info?[Constants.UIKeys.secondValue] = maxMileageRange as AnyObject
            sSelf.auctionFilterTableView.reloadData()
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
}

