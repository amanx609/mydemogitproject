//
//  DSavedAuctionViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/28/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DSavedAuctionViewModeling: BaseVModeling {
    func requestSavedAuctionAPI(page: Int, completion: @escaping ([DAuctionModel]?, Int, Int)-> Void)
    func postSavedAuctionAPI(auctionId: Int, completion: @escaping (Bool) -> Void)
}

class DSavedAuctionViewM: DSavedAuctionViewModeling {
    
    func requestSavedAuctionAPI(page: Int, completion: @escaping ([DAuctionModel]?, Int, Int) -> Void) {
        let param = getAPIParams(page: page)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getSavedAuctionCar(param: param))) { (response, success) in
            if success {
                
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let data = result[kData] as? [[String: AnyObject]], let nextPageNumber = result[kNextPageNum] as? Int, let perPage = result[kPerPage] as? Int,
                    let mycarDataArray = data.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let auctionData = try decoder.decode([DAuctionModel].self, from: mycarDataArray)
                        completion(auctionData, nextPageNumber, perPage)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
    private func getAPIParams(page: Int) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.page] = page as AnyObject
        return param
    }
    
    func postSavedAuctionAPI(auctionId: Int, completion: @escaping (Bool) -> Void) {
        let params = self.getPostAddCarParams(auctionId:auctionId)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .postSavedAuctionCar(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]{
                    completion(success)
                }
            }
        }
    }
    
    func getPostAddCarParams(auctionId:Int)-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.id] = auctionId as AnyObject
        return params
    }
}
