//
//  DSavedAuctionViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/28/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DSavedAuctionViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var savedCarTableView: UITableView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var postNewAuctionButton: UIButton!
    
    //MARK: - Properties
    var savedAuctionDataSource: [DAuctionModel] = []
    var viewModel: DSavedAuctionViewModeling?
    var congratulationsViewModel: DAuctionCongratulationsViewModeling?
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        setupViewModel()
        registerNibs()
        setupTableView()
        self.setupView()
        self.setupNavigationBarTitle(title: "Saved For Auction".localizedString(),barColor: .redButtonColor,titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.requestSavedAuctionAPI()
    }
    
    private func setupTableView() {
        self.savedCarTableView.separatorStyle = .none
        self.savedCarTableView.backgroundColor = UIColor.white
        self.savedCarTableView.delegate = self
        self.savedCarTableView.dataSource = self
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.postNewAuctionButton.setTitle("Post New Auction".localizedString(), for: .normal)
    }
    
    private func setupViewModel() {
        
        if self.viewModel == nil {
            self.viewModel = DSavedAuctionViewM()
        }
        
        if self.congratulationsViewModel == nil {
            self.congratulationsViewModel = DAuctionCongratulationsViewM()
        }
    }
    
    private func registerNibs() {
        self.savedCarTableView.register(DSavedAuctionCell.self)
    }
    
    func initializeDAuctionAddCarFirstViewC(auctionModel: DAuctionModel)-> DAuctionAddCarFirstViewC {
        let auctionAddCarFirstVC = DIConfigurator.sharedInstance.getDAuctionAddCarFirstVC()
        auctionAddCarFirstVC.auctionModel = auctionModel
        return auctionAddCarFirstVC
    }
    
    func initializeDAuctionAddCarSecondViewC(auctionModel: DAuctionModel)-> DAuctionAddCarSecondViewC {
        let auctionAddCarSecondVC = DIConfigurator.sharedInstance.getDAuctionAddCarSecondVC()
        auctionAddCarSecondVC.auctionModel = auctionModel
        return auctionAddCarSecondVC
    }
    
    func initializeDAuctionAddCarThirdViewC(auctionModel: DAuctionModel)-> DAuctionAddCarThirdViewC {
        let auctionAddCarThirdVC = DIConfigurator.sharedInstance.getDAuctionAddCarThirdVC()
        auctionAddCarThirdVC.auctionModel = auctionModel
        return auctionAddCarThirdVC
    }
    
    func initializeDAuctionAddCarFourthViewC(auctionModel: DAuctionModel)-> DAuctionAddCarFourthViewC {
        let auctionAddCarFourthViewC = DIConfigurator.sharedInstance.getDAuctionAddCarFourthVC()
        auctionAddCarFourthViewC.auctionModel = auctionModel
        return auctionAddCarFourthViewC
    }
    
    func initializeDAuctionAddCarFifthViewC(auctionModel: DAuctionModel)-> DAuctionAddCarFifthViewC {
        let auctionAddCarFifthVC = DIConfigurator.sharedInstance.getDAuctionAddCarFifthVC()
        auctionAddCarFifthVC.auctionModel = auctionModel
        return auctionAddCarFifthVC
    }
    
    func initializeDAuctionAddCarSixthViewC(auctionModel: DAuctionModel)-> DAuctionAddCarSixthViewC {
        let auctionAddCarSixthVC = DIConfigurator.sharedInstance.getDAuctionAddCarSixthVC()
        auctionAddCarSixthVC.auctionModel = auctionModel
        return auctionAddCarSixthVC
    }
    
    func initializeDAuctionUploadCarImageViewC(auctionModel: DAuctionModel)-> DAuctionUploadCarImageViewC {
        let auctionUploadCarImageVC = DIConfigurator.sharedInstance.getDAuctionUploadCarImageVC()
        auctionUploadCarImageVC.auctionModel = auctionModel
        return auctionUploadCarImageVC
    }
    
    func initializeDAuctionConfirmationViewC(auctionModel: DAuctionModel)-> DChooseAuctionTypeViewC {
        let auctionConfirmationVC = DIConfigurator.sharedInstance.getDChooseAuctionTypeVC()
        auctionConfirmationVC.auctionModel = auctionModel
        return auctionConfirmationVC
    }
    
    //DAuctionAddCarFirstViewC
    func goToNextPage(auctionModel: DAuctionModel,index: Int) {
        var viewController: [UIViewController] = []
        switch auctionModel.auctionPageId {
        case 1:
            viewController.append(self.initializeDAuctionAddCarFirstViewC(auctionModel: auctionModel))
            self.navigationController?.viewControllers.append(contentsOf: viewController)
            self.navigationController?.pushViewController(self.initializeDAuctionAddCarSecondViewC(auctionModel: auctionModel), animated:false)
            break
        case 2:
            viewController.append(self.initializeDAuctionAddCarFirstViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarSecondViewC(auctionModel: auctionModel))
            self.navigationController?.viewControllers.append(contentsOf: viewController)
            self.navigationController?.pushViewController(self.initializeDAuctionAddCarThirdViewC(auctionModel: auctionModel), animated:false)
            break
        case 3:
            viewController.append(self.initializeDAuctionAddCarFirstViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarSecondViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarThirdViewC(auctionModel: auctionModel))
            self.navigationController?.viewControllers.append(contentsOf: viewController)
            self.navigationController?.pushViewController(self.initializeDAuctionAddCarFourthViewC(auctionModel: auctionModel), animated:false)
            break
        case 4:
            viewController.append(self.initializeDAuctionAddCarFirstViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarSecondViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarThirdViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarFourthViewC(auctionModel: auctionModel))
            self.navigationController?.viewControllers.append(contentsOf: viewController)
            self.navigationController?.pushViewController(self.initializeDAuctionAddCarFifthViewC(auctionModel: auctionModel), animated:false)
            break
        case 5:
            viewController.append(self.initializeDAuctionAddCarFirstViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarSecondViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarThirdViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarFourthViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarFifthViewC(auctionModel: auctionModel))
            self.navigationController?.viewControllers.append(contentsOf: viewController)
            self.navigationController?.pushViewController(self.initializeDAuctionAddCarSixthViewC(auctionModel: auctionModel), animated:false)
        case 6:
            viewController.append(self.initializeDAuctionAddCarFirstViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarSecondViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarThirdViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarFourthViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarFifthViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarSixthViewC(auctionModel: auctionModel))
            self.navigationController?.viewControllers.append(contentsOf: viewController)
            self.navigationController?.pushViewController(self.initializeDAuctionUploadCarImageViewC(auctionModel: auctionModel), animated:false)
            break
        case 7:
            viewController.append(self.initializeDAuctionAddCarFirstViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarSecondViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarThirdViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarFourthViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarFifthViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionAddCarSixthViewC(auctionModel: auctionModel))
            viewController.append(self.initializeDAuctionUploadCarImageViewC(auctionModel: auctionModel))
            
            self.navigationController?.viewControllers.append(contentsOf: viewController)
            self.navigationController?.pushViewController(self.initializeDAuctionConfirmationViewC(auctionModel: auctionModel), animated:false)
            break
        case 8:
            self.requestPostAuctionAPI(auctionModel: auctionModel, index: index)
            break
        default:
            break
        }
    }
    
    //MARK: - IBActions
    @IBAction func tapPostNewAuctionButton(_ sender: UIButton) {
           let auctionAddCarFirstVC = DIConfigurator.sharedInstance.getDAuctionAddCarFirstVC()
           self.navigationController?.pushViewController(auctionAddCarFirstVC, animated: true)
       }
    
    //MARK: - API Methods
    func requestSavedAuctionAPI() {
        self.viewModel?.requestSavedAuctionAPI(page: self.nextPageNumber) { (responseData, nextPageNum, perPage) in
            if let data = responseData as? [DAuctionModel]{
                self.savedAuctionDataSource = self.savedAuctionDataSource + data
                self.nextPageNumber = nextPageNum
                self.perPage = perPage
                Threads.performTaskInMainQueue {
                    self.savedCarTableView.reloadData()
                }
            }
        }
    }
    
    //MARK: - API Methods
    func requestCancelAPI(auctionModel: DAuctionModel,index: Int) {
        self.congratulationsViewModel?.requestCancelAuctionAPI(auctionId: Helper.toInt(auctionModel.basicDetails?.id)) { (success) in
            if success {
                self.savedAuctionDataSource.remove(at: index)
                Threads.performTaskInMainQueue {
                    self.savedCarTableView.reloadData()
                    Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Post cancel successfully".localizedString())
                }
            }
        }
    }
    
    //MARK: - API Methods
    func requestPostAuctionAPI(auctionModel: DAuctionModel,index: Int) {
        self.viewModel?.postSavedAuctionAPI(auctionId: Helper.toInt(auctionModel.basicDetails?.id)) { (success) in
            if success {
                
                self.savedAuctionDataSource.remove(at: index)
                Threads.performTaskInMainQueue {
                    self.savedCarTableView.reloadData()
                    let auctionCongratulationsVC = DIConfigurator.sharedInstance.getDAuctionCongratulationsVC()
                    auctionCongratulationsVC.auctionModel = auctionModel
                    self.navigationController?.pushViewController(auctionCongratulationsVC, animated: true)
                }
            }
        }
    }
    
}
