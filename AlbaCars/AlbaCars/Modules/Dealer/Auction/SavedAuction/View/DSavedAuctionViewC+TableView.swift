//
//  DSavedAuctionViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/28/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension DSavedAuctionViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.savedAuctionDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let auctionModel = self.savedAuctionDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath,auctionModel:auctionModel)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let lastVisibleRow = self.savedCarTableView.indexPathsForVisibleRows?.last?.row else { return }
        if (self.savedAuctionDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
            self.requestSavedAuctionAPI()
        }
    }
    
}

extension DSavedAuctionViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath,auctionModel:DAuctionModel) -> UITableViewCell {
        let cell: DSavedAuctionCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
         cell.configureView(auctionInfo: auctionModel)
        return cell
    }
}

//MARK: - DSavedAuctionCellDelegate
extension DSavedAuctionViewC: DSavedAuctionCellDelegate {
    func didTapDeleteSavedCar(cell: DSavedAuctionCell) {
      guard let indexPath = self.savedCarTableView.indexPath(for: cell) else { return }
        let auctionModel = self.savedAuctionDataSource[indexPath.row]
        
        Alert.showAlertWithActionWithCancel(title: StringConstants.Text.AppName.localizedString(), message: StringConstants.Message.cancelPostRequest.localizedString(), actionTitle: StringConstants.Text.Yes.localizedString(), cancelTitle: StringConstants.Text.No.localizedString(), action: { (_) in
            self.requestCancelAPI(auctionModel: auctionModel, index: indexPath.row)
        }, cancelAction: { (_) in

        })
    }
    
    func didTapPostCar(cell: DSavedAuctionCell) {
    guard let indexPath = self.savedCarTableView.indexPath(for: cell) else { return }
        self.goToNextPage(auctionModel: self.savedAuctionDataSource[indexPath.row], index: indexPath.row)
    }
      
}
