//
//  DSavedAuctionCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/28/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol DSavedAuctionCellDelegate: class {
    func didTapDeleteSavedCar(cell: DSavedAuctionCell)
    func didTapPostCar(cell: DSavedAuctionCell)
}

class DSavedAuctionCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var containerView: UIView!
    //CarInfo
    @IBOutlet weak var carDetailsImageView: UIImageView!
    @IBOutlet weak var carHistoryImageView: UIImageView!
    @IBOutlet weak var inspectionImageView: UIImageView!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var optionImageView: UIImageView!
    @IBOutlet weak var auctionDetailImageView: UIImageView!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var progressLabel: UILabel!
    
    //MARK: - Variables
    weak var delegate: DSavedAuctionCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.shadowView.addShadow(Constants.UIConstants.sizeRadius_3half, shadowOpacity: Constants.UIConstants.shadowOpacity_15)
        self.containerView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
    }
    
    //MARK: - Public Methods
    func configureView(auctionInfo: DAuctionModel) {
        self.carNameLabel.text = auctionInfo.basicDetails?.brandName ?? "" + (auctionInfo.basicDetails?.modelName ?? "")
       // progressView.progress = auctionInfo.getProgress()
       // progressLabel.text = auctionInfo.getPercentage()
        var percentage = ""
        var progress:Float = 0.0
        switch auctionInfo.auctionPageId {
        case 1:
            self.carDetailsImageView.image = #imageLiteral(resourceName: "green_tick")
            self.carHistoryImageView.image = #imageLiteral(resourceName: "red_minus")
            self.optionImageView.image = #imageLiteral(resourceName: "red_minus")
            self.inspectionImageView.image = #imageLiteral(resourceName: "red_minus")
            self.photoImageView.image = #imageLiteral(resourceName: "red_minus")
            self.auctionDetailImageView.image = #imageLiteral(resourceName: "red_minus")
            percentage = "16%"
            progress = 0.16
            break
        case 2:
            self.carDetailsImageView.image = #imageLiteral(resourceName: "green_tick")
            self.carHistoryImageView.image = #imageLiteral(resourceName: "green_tick")
            self.optionImageView.image = #imageLiteral(resourceName: "red_minus")
            self.inspectionImageView.image = #imageLiteral(resourceName: "red_minus")
            self.photoImageView.image = #imageLiteral(resourceName: "red_minus")
            self.auctionDetailImageView.image = #imageLiteral(resourceName: "red_minus")
            percentage = "33%"
            progress = 0.33
            break
        case 3,4,5:
            self.carDetailsImageView.image = #imageLiteral(resourceName: "green_tick")
            self.carHistoryImageView.image = #imageLiteral(resourceName: "green_tick")
            self.optionImageView.image = #imageLiteral(resourceName: "green_tick")
            self.inspectionImageView.image = #imageLiteral(resourceName: "red_minus")
            self.photoImageView.image = #imageLiteral(resourceName: "red_minus")
            self.auctionDetailImageView.image = #imageLiteral(resourceName: "red_minus")
            percentage = "50%"
            progress = 0.50
            break
        case 6:
            self.carDetailsImageView.image = #imageLiteral(resourceName: "green_tick")
            self.carHistoryImageView.image = #imageLiteral(resourceName: "green_tick")
            self.optionImageView.image = #imageLiteral(resourceName: "green_tick")
            self.inspectionImageView.image = #imageLiteral(resourceName: "green_tick")
            self.photoImageView.image = #imageLiteral(resourceName: "red_minus")
            self.auctionDetailImageView.image = #imageLiteral(resourceName: "red_minus")
            percentage = "66%"
            progress = 0.66
            break
        case 7:
            self.carDetailsImageView.image = #imageLiteral(resourceName: "green_tick")
            self.carHistoryImageView.image = #imageLiteral(resourceName: "green_tick")
            self.optionImageView.image = #imageLiteral(resourceName: "green_tick")
            self.inspectionImageView.image = #imageLiteral(resourceName: "green_tick")
            self.photoImageView.image = #imageLiteral(resourceName: "green_tick")
            self.auctionDetailImageView.image = #imageLiteral(resourceName: "red_minus")
             percentage = "83%"
             progress = 0.83
        case 8:
            self.carDetailsImageView.image = #imageLiteral(resourceName: "green_tick")
            self.carHistoryImageView.image = #imageLiteral(resourceName: "green_tick")
            self.optionImageView.image = #imageLiteral(resourceName: "green_tick")
            self.inspectionImageView.image = #imageLiteral(resourceName: "green_tick")
            self.photoImageView.image = #imageLiteral(resourceName: "green_tick")
            self.auctionDetailImageView.image = #imageLiteral(resourceName: "green_tick")
            percentage = "100%"
            progress = 1.0
            break
        default:
            break
        }
        
        self.progressLabel.text = percentage
        self.progressView.progress = progress
        
    }
    
    //MARK: - IBAction
    
    @IBAction func tapDeleteSavedCar(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapDeleteSavedCar(cell: self)
        }
    }
    
    @IBAction func tapPostCar(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapPostCar(cell: self)
        }
    }
}
