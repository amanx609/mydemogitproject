//
//  EnterAuctionRoomModel.swift
//  AlbaCars
//
//  Created by Narendra on 2/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class EnterAuctionRoomModel: Codable {
    
    //Variables
    var end_status: Int?
    var id: Int?
    var user_id: Int?
    var vehicle_id: Int?
    var status: Int?
}
