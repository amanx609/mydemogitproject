//
//  LiveAuctionListViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/6/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DLiveAuctionListViewC: BaseViewC {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var liveAuctionListTableView: UITableView!
    
    //MARK: - Variables
    var viewModel: DAuctionListVModeling?
    var params: APIParams = APIParams()
    var isMyAuction: Bool = false
    var auctionCarsDataSource: [Vehicle] = []
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func navigationButtonTapped(_ sender: AnyObject) {
        guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
        switch buttonType {
        case .filterWhite: self.presentFilterViewC()
        case .backWhite: self.navigationController?.popViewController(animated: true)
        default: break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.nextPageNumber = 1
        self.requestAuctionCarListAPI(page: self.nextPageNumber)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Live Auctions".localizedString(), barColor: UIColor.redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [.filterWhite])
        self.recheckVM()
        self.setupTableView()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = DAuctionListViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.liveAuctionListTableView.delegate = self
        self.liveAuctionListTableView.dataSource = self
        self.liveAuctionListTableView.separatorStyle = .none
    }
    
    private func registerNibs() {
        self.liveAuctionListTableView.register(AuctionListTableCell.self)
    }
    
    private func presentFilterViewC() {
        let filterVC = DIConfigurator.sharedInstance.getDAuctionFilterVC()
        filterVC.selectedParams = self.params
        filterVC.selectParams = { [weak self] (params) in
            guard let sSelf = self else {
                return
            }
            if let filterParam = params {
                sSelf.params = filterParam
            }
            sSelf.nextPageNumber = 1
            sSelf.requestAuctionCarListAPI(page: sSelf.nextPageNumber)
        }
        let navC = UINavigationController(rootViewController: filterVC)
        self.present(navC, animated: true, completion: nil)
    }
    
    
    //MARK: - APIMethods
    func requestAuctionCarListAPI(page: Int) {
        //AuctionType
        self.params[ConstantAPIKeys.type] = 1 as AnyObject
        //AuctionListType
        self.params[ConstantAPIKeys.auctionList] = AuctionListType.live.rawValue as AnyObject
        //RoomId
        if self.isMyAuction {
            self.params[ConstantAPIKeys.roomId] = UserSession.shared.getUserId() as AnyObject
        } else {
            self.params[ConstantAPIKeys.roomId] = 0 as AnyObject
        }
        
        //Page
        self.params[ConstantAPIKeys.page] = page as AnyObject
        self.viewModel?.requestAuctionCarListAPI(params: self.params, completion: { [weak self] (nextPage, perPage, vehicleList) in
            guard let sSelf = self,
                let vehicles = vehicleList.vehicles else { return }
            sSelf.nextPageNumber = nextPage
            sSelf.perPage = perPage
            if page == 1 {
                sSelf.auctionCarsDataSource.removeAll()
            }
            sSelf.auctionCarsDataSource = sSelf.auctionCarsDataSource + vehicles
            Threads.performTaskInMainQueue {
                sSelf.liveAuctionListTableView.reloadData()
            }
        })
    }
    
    //MARK: - APIMethods
    func requestVehicleDetailsAPI(vehicleId: Int,notificationType: [Int]) {
        let auctionListViewM:DAuctionListViewM = DAuctionListViewM()
        auctionListViewM.requestNotifyAPI(vehicleId: vehicleId, notificationType: notificationType, completion: { [weak self] (status) in
            guard self != nil else { return }
        })
    }
    
    //MARK: - API Methods
    func requestAuctionAPI(vehicleId: Int,vehicle:Vehicle) {
        
        let auctionListViewM:DAuctionListViewM = DAuctionListViewM()
           auctionListViewM.requestEnterAuctionDetailsAPI(vehicleId: vehicleId) { (responseData) in
               Threads.performTaskInMainQueue {
                let liveAuctionRoomVC = DIConfigurator.sharedInstance.getDLiveAuctionRoomVC()
                liveAuctionRoomVC.enterAuctionRoomModel = responseData
                liveAuctionRoomVC.vehicle = vehicle
                self.navigationController?.pushViewController(liveAuctionRoomVC, animated: true)
           }
       }
    }
}
