//
//  DAllAuctionRoomViewC.swift
//  AlbaCars
//
//  Created by Narendra on 1/9/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DAllAuctionRoomViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var auctionRoomsCollectionView: UICollectionView!

    //MARK: - Variables
    var viewModel: DAllAuctionRoomViewModeling?
    var auctionRoomsDataSource: [AuctionRoom] = []
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "All Auction Rooms".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupCollectionView()
        self.requestAuctionRoomListAPI(page: 1)
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = DAllAuctionRoomViewM()
        }
    }
    
    private func setupCollectionView() {
        self.registerNibs()
        // Auction Rooms
        self.auctionRoomsCollectionView.delegate = self
        self.auctionRoomsCollectionView.dataSource = self
        let auctionRoomLayout = UICollectionViewFlowLayout()
        auctionRoomLayout.scrollDirection = .vertical //depending upon direction of collection view
        self.auctionRoomsCollectionView?.setCollectionViewLayout(auctionRoomLayout, animated: true)
    }
    
    private func registerNibs() {
        self.auctionRoomsCollectionView.register(AuctionRoomCollectionCell.self)
    }
    
    //MARK: - APIMethods
    func requestAuctionRoomListAPI(page: Int) {
        self.viewModel?.requestAuctionRoomListAPI(page: page, completion: { [weak self] (nextPage, perPage, auctionRoomList) in
            guard let sSelf = self else { return }
            sSelf.nextPageNumber = nextPage
            sSelf.perPage = perPage
            if let auctionRooms = auctionRoomList.auctionRooms {
                sSelf.auctionRoomsDataSource = sSelf.auctionRoomsDataSource + auctionRooms
                Threads.performTaskInMainQueue {
                    sSelf.auctionRoomsCollectionView.reloadData()
                }
            }
        })
    }
}
