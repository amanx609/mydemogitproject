//
//  DAllAuctionRoomViewC+CollectionView.swift
//  AlbaCars
//
//  Created by Narendra on 1/9/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension DAllAuctionRoomViewC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.auctionRoomsDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getCell(collectionView: collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/2-5
        return CGSize(width: width, height: Constants.CellHeightConstants.height_265)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // Pagination
        let count = self.auctionRoomsDataSource.count
        if (count - 4 == indexPath.row) && self.nextPageNumber != 0 {
            self.requestAuctionRoomListAPI(page: self.nextPageNumber)
        }
    }
    
}

extension DAllAuctionRoomViewC {
    func getCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell: AuctionRoomCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        let auctionRoom = self.auctionRoomsDataSource[indexPath.row]
        cell.delegate = self
        cell.configureView(auctionRoom: auctionRoom)
        return cell
    }
}

//MARK: - AuctionRoomCollectionCellDelegate
extension DAllAuctionRoomViewC: AuctionRoomCollectionCellDelegate {
    func didTapViewAuctionRoom(cell: AuctionRoomCollectionCell) {
        guard let indexPath = self.auctionRoomsCollectionView.indexPath(for: cell) else { return }
        let auctionRoom = self.auctionRoomsDataSource[indexPath.row]
        let auctionRoomViewC = DIConfigurator.sharedInstance.getDAuctionRoomVC()
        auctionRoomViewC.auctionRoom = auctionRoom
        self.navigationController?.pushViewController(auctionRoomViewC, animated: true)
    }
}
