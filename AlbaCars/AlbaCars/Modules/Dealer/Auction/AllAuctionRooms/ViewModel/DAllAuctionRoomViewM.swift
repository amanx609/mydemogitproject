//
//  DAllAuctionRoomViewM.swift
//  
//
//  Created by Narendra on 1/9/20.
//

protocol DAllAuctionRoomViewModeling: BaseVModeling {
    func requestAuctionRoomListAPI(page: Int, completion: @escaping (Int, Int, AuctionRoomList) -> Void)
}

class DAllAuctionRoomViewM: BaseViewM, DAllAuctionRoomViewModeling {
    
    func requestAuctionRoomListAPI(page: Int, completion: @escaping (Int, Int, AuctionRoomList) -> Void) {
        let auctionRoomListParam = getAPIParams(page: page)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .auctionRoomList(param: auctionRoomListParam))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let auctionRoomData = result.toJSONData(),
                    let auctionRoomList = AuctionRoomList(jsonData: auctionRoomData) {
                        completion(nextPageNumber, perPage, auctionRoomList)
                    }
            }
        }
    }
    
    private func getAPIParams(page: Int) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.page] = page as AnyObject
        return param
    }
}
