//
//  AuctionRoom.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 17/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation


class AuctionRoom: BaseCodable {
    
    //Variables
    var id: Int?
    var description: String?
    var image: String?
    var name: String?
    var rating: Double?
    var liveCars: Int?
    var soldCars: Int?
    var totalCars: Int?
}

class AuctionRoomList: BaseCodable {
    var auctionRooms: [AuctionRoom]?
    
    private enum CodingKeys: String, CodingKey {
      case auctionRooms = "data"
    }
}
