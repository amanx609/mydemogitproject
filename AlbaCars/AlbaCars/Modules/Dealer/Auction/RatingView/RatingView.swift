//
//  RatingView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/7/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class RatingView: UIView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var firstRatingStarButton: UIButton!
    @IBOutlet weak var secondRatingStarButton: UIButton!
    @IBOutlet weak var thirdRatingStarButton: UIButton!
    @IBOutlet weak var fourthRatingStarButton: UIButton!
    @IBOutlet weak var fifthRatingStarButton: UIButton!
    
    //MARK: - Variables
    var ratingButtonArray = [UIButton]()
    var rating = 0
    
    var contentView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadNib()
    }
    
    private func loadNib() {
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        self.ratingButtonArray = [self.firstRatingStarButton, self.secondRatingStarButton, self.thirdRatingStarButton, self.fourthRatingStarButton, self.fifthRatingStarButton]
        self.configureView(rating: self.rating)

        addSubview(contentView)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of:self))
        let nib = UINib(nibName: "RatingView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
     func configureView(rating: Int) {
        
       for i in 0..<rating {
           ratingButtonArray[i].setImage(UIImage(named: "starYellow"), for: .normal)
       }
        self.layoutIfNeeded()
    }
    
    @IBAction func tapRatingButton(_ sender: UIButton) {
        self.selectedRatingButton(sender: sender)
    }
    
    func selectedRatingButton(sender: UIButton) {
        
        
        self.firstRatingStarButton.isSelected = false
        self.secondRatingStarButton.isSelected = false
        self.thirdRatingStarButton.isSelected = false
        self.fourthRatingStarButton.isSelected = false
        self.fifthRatingStarButton.isSelected = false
        sender.isSelected = true
        rating = sender.tag
        
        switch rating {
        case 1:
            self.firstRatingStarButton.isSelected = true
        case 2:
            self.firstRatingStarButton.isSelected = true
            self.secondRatingStarButton.isSelected = true
        case 3:
            self.firstRatingStarButton.isSelected = true
            self.secondRatingStarButton.isSelected = true
            self.thirdRatingStarButton.isSelected = true
        case 4:
            self.firstRatingStarButton.isSelected = true
            self.secondRatingStarButton.isSelected = true
            self.thirdRatingStarButton.isSelected = true
            self.fourthRatingStarButton.isSelected = true
        case 5:
            self.firstRatingStarButton.isSelected = true
            self.secondRatingStarButton.isSelected = true
            self.thirdRatingStarButton.isSelected = true
            self.fourthRatingStarButton.isSelected = true
            self.fifthRatingStarButton.isSelected = true
        default:
            break
        }
        
        //configureView(rating: rating)
        self.layoutIfNeeded()
    }
    
    
}
