//
//  AuctionListTableCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/6/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol AuctionListTableCellDelegate: class {
    func didTapNotifyMe(cell: AuctionListTableCell)
    func didTapEnterAuction(cell: AuctionListTableCell)
}

class AuctionListTableCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
    
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var specsLabel: UILabel!
    @IBOutlet weak var odometerLabel: UILabel!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var remainingTimeLabel: UILabel!
    @IBOutlet weak var timeLeftLabel: UILabel!
    @IBOutlet weak var remainingTimeprogressView: UIProgressView!
    @IBOutlet weak var notifyMeButton: UIButton!
    @IBOutlet weak var auctioneerLogoImageView: UIImageView!
    @IBOutlet weak var auctioneerNameLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var liveStatusBarView: UIView!
    @IBOutlet weak var liveBottomView: UIStackView!
    @IBOutlet weak var currentBidAmountLabel: UILabel!
    @IBOutlet weak var enterAuctionButton: UIButton!
    @IBOutlet weak var ratingView: RatingView!
    @IBOutlet weak var topView: UIView!
    
    //MARK: - Variables
    var ratingButtonArray = [UIButton]()
    weak var delegate: AuctionListTableCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK: - Private Methods
    private func setup() {
        self.remainingTimeprogressView.progressTintColor = UIColor.greenTopHeaderColor
        self.remainingTimeprogressView.transform = CGAffineTransform(rotationAngle: .pi)
        self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.shadowView.addShadow(ofColor:  UIColor.shadowColor, radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        self.containerView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
    }
    
    private func setupForLiveAuctions(cellInfo: CellInfo) {
        self.notifyMeButton.isHidden = true
        self.liveBottomView.isHidden = false
        self.timeLeftLabel.text = "Time Left".localizedString()
    }
    
    private func setupForAuctionRoomLiveAuctions() {
        self.notifyMeButton.isHidden = true
        self.liveBottomView.isHidden = false
        self.liveStatusBarView.isHidden = true
        self.timeLeftLabel.text = "Time Left".localizedString()
    }
    
    private func setupForAuctionOfDayLiveAuction() {
        self.notifyMeButton.isHidden = true
        self.liveBottomView.isHidden = false
        self.liveStatusBarView.isHidden = true
        self.timeLeftLabel.text = "Time Left".localizedString()
        Threads.performTaskInMainQueue {
          self.liveStatusBarView.isHidden = false
          self.liveStatusBarView.drawGradient(startColor: .orangeGradientStartColor, endColor: .orangeGradientEndColor)
        }
    }
    
    private func setupForUpcomingAuctions() {
        self.notifyMeButton.isHidden = false
        self.liveStatusBarView.isHidden = true
        self.liveBottomView.isHidden = true
        self.timeLeftLabel.text = "Auction Starts In".localizedString()
        
    }
    
    //MARK: - Public Methods
    //AuctionRoomLiveAuction
    func configureViewForAuctionRoomLiveAuction(auctionCar: Vehicle) {
        self.setupForAuctionRoomLiveAuctions()
        if let vehicleImage = auctionCar.vehicleImages?.first {
            self.carImageView.setImage(urlStr: vehicleImage, placeHolderImage: nil)
        }
        var carName = ""
        if let brandName = auctionCar.brandName {
            carName = brandName + " "
        }
        if let vehileName = auctionCar.modelName {
            carName = carName + vehileName
        }
        self.brandNameLabel.text = carName
        if let modelYear = auctionCar.year {
            self.carModelLabel.text = "\(modelYear)"
        }
        if let mileage = auctionCar.mileage {
            let odoMeter = Int(mileage).formattedWithSeparator
            self.odometerLabel.text = "\(odoMeter) " + "Km".localizedString()
        }
        self.specsLabel.text = auctionCar.specs
        
        self.ratingView.configureView(rating: Helper.toInt(auctionCar.rating))
        
        if let currentTime = auctionCar.currentDateTime,
            let startTime = auctionCar.startTime,
            let endTime = auctionCar.endTime {
            let timeLeft = Date.timeLeftBetween(currentDateStr: currentTime, futureDateStr: endTime)
            
            let time = Int((auctionCar.timerDuration ?? 0) / 60)
            self.remainingTimeLabel.text = Helper.toString(object: time) + " min".localizedString()
            print(timeLeft)
            print("timeLeft")
            //ProgressView
            if let dtCurrentTime = currentTime.toDateInstaceFromStandardFormat(),
                let dtStartTime = startTime.toDateInstaceFromStandardFormat(),
                let dtEndTime = endTime.toDateInstaceFromStandardFormat() {
                let auctionDuration = dtEndTime.timeIntervalSince(dtStartTime)
                let timeLeft = dtEndTime.timeIntervalSince(dtCurrentTime)
                let timeLeftProgress = Float(timeLeft/auctionDuration)
                self.remainingTimeprogressView.progress = timeLeftProgress
            }
        }
    }
    
    //AuctionOfDayLiveAuction
    func configureViewForAuctionOfDayLiveAuction(auctionCar: Vehicle) {
        self.setupForAuctionOfDayLiveAuction()
        //DealerInfo
        if let dealerImage = auctionCar.image {
            self.auctioneerLogoImageView.setImage(urlStr: dealerImage, placeHolderImage: nil)
        }
        self.auctioneerNameLabel.text = auctionCar.name
        //VehicleInfo
        if let vehicleImage = auctionCar.vehicleImages?.first {
            self.carImageView.setImage(urlStr: vehicleImage, placeHolderImage: nil)
        }
        var carName = ""
        if let brandName = auctionCar.brandName {
            carName = brandName + " "
        }
        if let vehileName = auctionCar.modelName {
            carName = carName + vehileName
        }
        self.brandNameLabel.text = carName
        if let modelYear = auctionCar.year {
            self.carModelLabel.text = "\(modelYear)"
        }
        if let mileage = auctionCar.mileage {
            let odoMeter = Int(mileage).formattedWithSeparator
            self.odometerLabel.text = "\(odoMeter) " + "Km".localizedString()
        }
        self.specsLabel.text = auctionCar.specs
        self.ratingView.configureView(rating: Helper.toInt(auctionCar.rating))
        
        if let currentTime = auctionCar.currentDateTime,
            let startTime = auctionCar.startTime,
            let endTime = auctionCar.endTime {
            let timeLeft = Date.timeLeftBetween(currentDateStr: currentTime, futureDateStr: endTime)
            
            let time = Int((auctionCar.timerDuration ?? 0) / 60)
            self.remainingTimeLabel.text = Helper.toString(object: time) + " min".localizedString()
            print(timeLeft)
            print("timeLeft")
            //ProgressView
            if let dtCurrentTime = currentTime.toDateInstaceFromStandardFormat(),
                let dtStartTime = startTime.toDateInstaceFromStandardFormat(),
                let dtEndTime = endTime.toDateInstaceFromStandardFormat() {
                let auctionDuration = dtEndTime.timeIntervalSince(dtStartTime)
                let timeLeft = dtEndTime.timeIntervalSince(dtCurrentTime)
                let timeLeftProgress = Float(timeLeft/auctionDuration)
                self.remainingTimeprogressView.progress = timeLeftProgress
            }
        }

    }
    
    //AuctionOfDayLiveAuction
    func configureViewForLiveAuction(auctionCar: Vehicle) {
        self.setupForAuctionRoomLiveAuctions()
        //DealerInfo
        if let dealerImage = auctionCar.image {
            self.auctioneerLogoImageView.setImage(urlStr: dealerImage, placeHolderImage: nil)
        }
        self.auctioneerNameLabel.text = auctionCar.name
        //VehicleInfo
        if let vehicleImage = auctionCar.vehicleImages?.first {
            self.carImageView.setImage(urlStr: vehicleImage, placeHolderImage: nil)
        }
        
        var carName = ""
        if let brandName = auctionCar.brandName {
            carName = brandName + " "
        }
        if let vehileName = auctionCar.modelName {
            carName = carName + vehileName
        }
        
        self.brandNameLabel.text = carName
        
        if let modelYear = auctionCar.year {
            self.carModelLabel.text = "\(modelYear)"
        }
        if let mileage = auctionCar.mileage {
            let odoMeter = Int(mileage).formattedWithSeparator
            self.odometerLabel.text = "\(odoMeter) " + "Km".localizedString()
        }
        self.specsLabel.text = auctionCar.specs
        //AuctionInfo
        if let currentBid = auctionCar.currentBid {
            let intCurrentBid = Int(currentBid).formattedWithSeparator
            self.currentBidAmountLabel.text = "AED".localizedString() + " \(intCurrentBid)"
        }
        
        if let currentTime = auctionCar.currentDateTime,
            let startTime = auctionCar.startTime,
            let endTime = auctionCar.endTime {
            let timeLeft = Date.timeLeftBetween(currentDateStr: currentTime, futureDateStr: endTime)
            
            let time = Int((auctionCar.timerDuration ?? 0) / 60)
            self.remainingTimeLabel.text = Helper.toString(object: time) + " min".localizedString()
            print(timeLeft)
            print("timeLeft")
            //ProgressView
            if let dtCurrentTime = currentTime.toDateInstaceFromStandardFormat(),
                let dtStartTime = startTime.toDateInstaceFromStandardFormat(),
                let dtEndTime = endTime.toDateInstaceFromStandardFormat() {
                let auctionDuration = dtEndTime.timeIntervalSince(dtStartTime)
                let timeLeft = dtEndTime.timeIntervalSince(dtCurrentTime)
                let timeLeftProgress = Float(timeLeft/auctionDuration)
                self.remainingTimeprogressView.progress = timeLeftProgress
            }
        }
        
        self.ratingView.configureView(rating: Helper.toInt(auctionCar.rating))

    }
    
    
    //AuctionRoomUpcomingAuction
    func configureViewForAuctionRoomUpcomingAuction(auctionCar: Vehicle) {
        self.setupForUpcomingAuctions()
        if let vehicleImage = auctionCar.vehicleImages?.first {
            self.carImageView.setImage(urlStr: vehicleImage, placeHolderImage: nil)
        }
        var carName = ""
        if let brandName = auctionCar.brandName {
            carName = brandName + " "
        }
        if let vehileName = auctionCar.modelName {
            carName = carName + vehileName
        }
        self.brandNameLabel.text = carName
        if let modelYear = auctionCar.year {
            self.carModelLabel.text = "\(modelYear)"
        }
        if let mileage = auctionCar.mileage {
            let odoMeter = Int(mileage).formattedWithSeparator
            self.odometerLabel.text = "\(odoMeter) " + "Km".localizedString()
        }
        self.specsLabel.text = auctionCar.specs
        
        //AuctionInfo
        if let currentTime = auctionCar.currentDateTime,
            let startTime = auctionCar.startTime,
            let endTime = auctionCar.endTime {
            let timeLeft = Date.timeLeftBetween(currentDateStr: currentTime, futureDateStr: startTime)
            self.remainingTimeLabel.text = timeLeft
            self.remainingTimeprogressView.progress = 1
        }
        
        self.ratingView.configureView(rating: Helper.toInt(auctionCar.rating))
    }
    
    //AuctionOfDayUpcomingAuction
    func configureViewForAuctionOfDayUpcomingAuction(auctionCar: Vehicle) {
        self.setupForUpcomingAuctions()
        //DealerInfo
        if let dealerImage = auctionCar.image {
            self.auctioneerLogoImageView.setImage(urlStr: dealerImage, placeHolderImage: nil)
        }
        self.auctioneerNameLabel.text = auctionCar.name
        //VehicleInfo
        if let vehicleImage = auctionCar.vehicleImages?.first {
            self.carImageView.setImage(urlStr: vehicleImage, placeHolderImage: nil)
        }
        var carName = ""
        if let brandName = auctionCar.brandName {
            carName = brandName + " "
        }
        if let vehileName = auctionCar.modelName {
            carName = carName + vehileName
        }
        self.brandNameLabel.text = carName
        if let modelYear = auctionCar.year {
            self.carModelLabel.text = "\(modelYear)"
        }
        if let mileage = auctionCar.mileage {
            let odoMeter = Int(mileage).formattedWithSeparator
            self.odometerLabel.text = "\(odoMeter) " + "Km".localizedString()
        }
        self.specsLabel.text = auctionCar.specs
        
        //AuctionInfo
        if let currentTime = auctionCar.currentDateTime,
            let startTime = auctionCar.startTime,
            let endTime = auctionCar.endTime {
            let timeLeft = Date.timeLeftBetween(currentDateStr: currentTime, futureDateStr: startTime)
            self.remainingTimeLabel.text = timeLeft
            self.remainingTimeprogressView.progress = 1
        }
        //self.remainingTimeLabel.text = "timeLeft"
        self.ratingView.configureView(rating: Helper.toInt(auctionCar.rating))

    }
    
    //MARK: IBActions
    @IBAction func tapNotifyMeButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapNotifyMe(cell: self)
        }
    }
    
    
    @IBAction func tapAuctionButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapEnterAuction(cell: self)
        }
    }
}
