//
//  AuctionListViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/6/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

extension DAuctionListViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       self.auctionCarsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.CellHeightConstants.height_220
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getCell(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      guard let lastVisibleRow = self.auctionListTableView.indexPathsForVisibleRows?.last?.row else { return }
        if (self.auctionCarsDataSource.count - lastVisibleRow == Constants.minCountBeforeAPICall) && self.nextPageNumber != 0 {
        self.requestAuctionCarListAPI(page: self.nextPageNumber)
      }
    }
}

extension DAuctionListViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: AuctionListTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let auctionCar = self.auctionCarsDataSource[indexPath.row]
        cell.delegate = self
        self.auctionType == .live ? cell.configureViewForAuctionOfDayLiveAuction(auctionCar: auctionCar) :
        cell.configureViewForAuctionOfDayUpcomingAuction(auctionCar: auctionCar)
        return cell
    }
}

//MARK: - AuctionListTableCell delegate
extension DAuctionListViewC: AuctionListTableCellDelegate {

    func didTapEnterAuction(cell: AuctionListTableCell) {
        
        guard let indexPath = self.auctionListTableView.indexPath(for: cell) else { return }
        let auctionCar = self.auctionCarsDataSource[indexPath.row]
        self.requestAuctionAPI(vehicleId: Helper.toInt(auctionCar.id),vehicle:
            self.auctionCarsDataSource[indexPath.row])
    }
    
    func didTapNotifyMe(cell: AuctionListTableCell) {
        
        guard let indexPath = self.auctionListTableView.indexPath(for: cell) else { return }
        let auctionCar = self.auctionCarsDataSource[indexPath.row]
        guard let notificationPopup = NotificationPopup.inistancefromNib() else { return }
        let notificationPopupType = NotificationPopupType.auctionCarNotify
        notificationPopup.initializeViewWith(title: notificationPopupType.headerTitle, arrayList: notificationPopupType.dataList) { [weak self] (notificationType) in
            guard let sSelf = self else { return }
            sSelf.requestVehicleDetailsAPI(vehicleId: Helper.toInt(auctionCar.id), notificationType: notificationType)
        }
        notificationPopup.showWithAnimated(animated: true)
    }
}
