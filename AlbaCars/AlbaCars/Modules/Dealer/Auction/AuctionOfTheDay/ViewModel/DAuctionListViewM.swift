//
//  DAuctionListViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/6/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DAuctionListVModeling: BaseVModeling {
    func requestAuctionCarListAPI(params: APIParams, completion: @escaping (Int, Int, VehicleList) -> Void)
    func requestNotifyAPI(vehicleId: Int, notificationType: [Int], completion: @escaping (Bool) -> Void)
    func requestEnterAuctionDetailsAPI(vehicleId:Int, completion: @escaping (EnterAuctionRoomModel)-> Void)
    
}

class DAuctionListViewM: DAuctionListVModeling {
 
    func requestAuctionCarListAPI(params: APIParams, completion: @escaping (Int, Int, VehicleList) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .auctionCarList(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let vehicleData = result.toJSONData(),
                    let vehicleList = VehicleList(jsonData: vehicleData) {
                    completion(nextPageNumber, perPage, vehicleList)
                }
            }
        }
    }
    
    func requestNotifyAPI(vehicleId: Int, notificationType: [Int], completion: @escaping (Bool) -> Void) {
        let param = getNotifyAPIParams(vehicleId: vehicleId, notificationType: notificationType)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .auctionNotifyMe(param: param))) { (response, success) in
            if success {
                if response is [String: AnyObject]
                   // let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                {
                    Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Notification added Successfully".localizedString())
                }
            }
        }
    }
    
    private func getNotifyAPIParams(vehicleId: Int, notificationType: [Int]) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        var notificationTypeStr = ""
        for type in notificationType {
            if notificationTypeStr == "" {
                notificationTypeStr.append("\(type)")
            } else {
                notificationTypeStr.append(",\(type)")
            }
        }
        param[ConstantAPIKeys.notificationType] = notificationTypeStr as AnyObject
        return param
    }
    
    func requestEnterAuctionDetailsAPI(vehicleId:Int, completion: @escaping (EnterAuctionRoomModel)-> Void) {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .enterAuctionRoom(param: param))) { (response, success) in
          if success {
              if let safeResponse =  response as? [String: AnyObject],
                let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],result.count > 0,
                  let auctionData = result.toJSONData() {
                  do {
                      let decoder = JSONDecoder()
                      let auctionModelData = try decoder.decode(EnterAuctionRoomModel.self, from: auctionData)
                      completion(auctionModelData)
                  } catch let error {
                      print(error)
                  }
              }else {
                  if let safeResponse =  response as? [String: AnyObject],
                      let message = safeResponse[kMessage] as? String {
                      Alert.showOkAlert(title: StringConstants.Text.AppName, message: message)
                  }
              }
        }
      }
    }
}


