//
//  DAuctionAddCarSecondViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DAuctionAddCarSecondViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var addCarTableView: UITableView!
    
    //MARK: - Properties
    var viewModel: DAuctionAddCarSecondViewModeling?
    var addCarDataSource: [CellInfo] = []
    var auctionModel = DAuctionModel()
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Add Car Details".localizedString(),barColor: .redButtonColor,titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.loadDataSource()
    }
    
    //MARK: - Private Methods
    private func setup() {
        setupViewModel()
        registerNibs()
        setupTableView()
    }
    
    private func setupTableView() {
        self.addCarTableView.separatorStyle = .none
        self.addCarTableView.backgroundColor = UIColor.white
        self.addCarTableView.delegate = self
        self.addCarTableView.dataSource = self
        self.addCarTableView.allowsSelection =  false
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = DAuctionAddCarSecondViewM()
        }
    }
    
    private func registerNibs() {
        self.addCarTableView.register(DAuctionAddCarInputCell.self)
        self.addCarTableView.register(DAuctionAddCarDropDownCell.self)
        self.addCarTableView.register(TextViewCell.self)
        self.addCarTableView.register(BottomButtonCell.self)
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getAddCarFormDataSource(historyDetail: self.auctionModel.historyDetails) {
            self.addCarDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.addCarTableView.reloadData()
            }
        }
    }
    
    func showDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        switch indexPath.row {
        case 0:
            listPopupView.initializeViewWith(title: DropDownType.service.title, arrayList: DropDownType.service.dataList, key: DropDownType.service.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let service = response[DropDownType.service.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = service
                sSelf.addCarTableView.reloadData()
            }
        case 1:
            listPopupView.initializeViewWith(title: DropDownType.accident.title, arrayList: DropDownType.accident.dataList, key: DropDownType.accident.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let accident = response[DropDownType.accident.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = accident
                sSelf.addCarTableView.reloadData()
            }
        case 2:
            listPopupView.initializeViewWith(title: DropDownType.chasisDamage.title, arrayList: DropDownType.chasisDamage.dataList, key: DropDownType.chasisDamage.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let chasisDamage = response[DropDownType.chasisDamage.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = chasisDamage
                sSelf.addCarTableView.reloadData()
            }
            
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    //MARK: - API Methods
    func requestAuctionAPI() {
        self.viewModel?.requestAuctionAPI(arrData: self.addCarDataSource, auctionId: Helper.toInt(self.auctionModel.historyDetails?.id)) { (responseData) in
            self.auctionModel = responseData
            Threads.performTaskInMainQueue {
                let auctionAddCarThirdVC = DIConfigurator.sharedInstance.getDAuctionAddCarThirdVC()
                auctionAddCarThirdVC.auctionModel = self.auctionModel
                self.navigationController?.pushViewController(auctionAddCarThirdVC, animated: true)
            }
        }
    }
}
