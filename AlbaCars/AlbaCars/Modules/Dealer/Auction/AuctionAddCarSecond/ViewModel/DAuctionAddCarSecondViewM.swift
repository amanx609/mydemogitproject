//
//  DAuctionAddCarSecondViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DAuctionAddCarSecondViewModeling {
    func getAddCarFormDataSource(historyDetail: HistoryDetails?) -> [CellInfo]
    func validatePostCarData(arrData: [CellInfo]) -> Bool
    func requestAuctionAPI(arrData: [CellInfo],auctionId:Int, completion: @escaping (DAuctionModel)-> Void)
}

class DAuctionAddCarSecondViewM: DAuctionAddCarSecondViewModeling {
    
    func getAddCarFormDataSource(historyDetail: HistoryDetails?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        // Service Cell
        var serviceInfo = [String: AnyObject]()
        serviceInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "service")
        let serviceCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Service".localizedString(), value:Helper.getDefaultString(object: historyDetail?.service, defaultString: "Unverified".localizedString()),info: serviceInfo, height: Constants.CellHeightConstants.height_80)
        array.append(serviceCell)
        
        // Accident Cell
        var accidentInfo = [String: AnyObject]()
        accidentInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "accident")
        let accidentCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Accident".localizedString(), value:Helper.getDefaultString(object: historyDetail?.accident, defaultString: "Unknown".localizedString()) ,info: accidentInfo, height: Constants.CellHeightConstants.height_80)
        array.append(accidentCell)
        
        // Chasis/Frame Damage Cell
        var chasisInfo = [String: AnyObject]()
        chasisInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "chasis")
        let chasisCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Chasis/Frame Damage".localizedString(), value:Helper.toString(object: historyDetail?.chasisDamage) ,info: chasisInfo, height: Constants.CellHeightConstants.height_80)
        array.append(chasisCell)
        
        // Other Information
        var otherInfo = [String: AnyObject]()
        otherInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "other_info")
        otherInfo[Constants.UIKeys.isHide] = true as AnyObject
        let otherCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Other Information".localizedString(), value:"" ,info: otherInfo, height: Constants.CellHeightConstants.height_80)
        array.append(otherCell)
        
        //TextView Cell
        let textViewCell = CellInfo(cellType: .TextViewCell, placeHolder: "Write Something...".localizedString(), value: Helper.toString(object: historyDetail?.carHistoryInfo), info: nil, height: UITableView.automaticDimension)
        array.append(textViewCell)
        
        //Next Cell
        let submitCell = CellInfo(cellType: .BottomButtonCell, placeHolder: "Next".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_170)
        array.append(submitCell)
        
        return array
    }
    
    //MARK: - API Methods
    func requestAuctionAPI(arrData: [CellInfo],auctionId:Int, completion: @escaping (DAuctionModel)-> Void) {
      if self.validatePostCarData(arrData: arrData) {
        let params = self.getPostAddCarParams(arrData: arrData, auctionId: auctionId)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .postAuctionCar(param: params))) { (response, success) in
          if success {
              if let safeResponse =  response as? [String: AnyObject],
                  let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                  let auctionData = result.toJSONData() {
                  do {
                      let decoder = JSONDecoder()
                      let auctionModelData = try decoder.decode(DAuctionModel.self, from: auctionData)
                      completion(auctionModelData)
                  } catch let error {
                      print(error)
                  }
              }
          }
        }
      }
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
     func validatePostCarData(arrData: [CellInfo]) -> Bool {
        var isValid = true
        var message = ""
        if let service = arrData[0].value, service.isEmpty {
            message = "Please select service."
            isValid = false
        } else if let accident = arrData[1].value, accident.isEmpty  {
            message = "Please select accident."
            isValid = false
        } else if let chasis = arrData[2].value, chasis.isEmpty {
            message = "Please select chasis/frame damage."
            isValid = false
        }
       
        if !isValid {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: message.localizedString())
        }
        return isValid
    }
    
    func getPostAddCarParams(arrData: [CellInfo],auctionId:Int)-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.auctionPage] = ConstantAPIKeys.historyDetails as AnyObject
        params[ConstantAPIKeys.service] = arrData[0].value as AnyObject
        params[ConstantAPIKeys.accident] = arrData[1].value as AnyObject
        params[ConstantAPIKeys.chasisDamage] = arrData[2].value as AnyObject
        params[ConstantAPIKeys.carHistoryInfo] = arrData[4].value as AnyObject
        params[ConstantAPIKeys.id] = auctionId as AnyObject
        return params
    }
}
