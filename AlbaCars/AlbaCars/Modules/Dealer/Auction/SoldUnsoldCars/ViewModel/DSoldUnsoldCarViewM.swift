//
//  DSoldUnsoldCarViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 16/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

protocol DSoldUnsoldCarVModeling: BaseVModeling {
    func requestAuctionCarListAPI(page: Int, auctionType: AuctionType, completion: @escaping (Int, Int, VehicleList) -> Void)
}

class DSoldUnsoldCarViewM: BaseViewM, DSoldUnsoldCarVModeling {
    func requestAuctionCarListAPI(page: Int, auctionType: AuctionType, completion: @escaping (Int, Int, VehicleList) -> Void) {
        let auctionCarParams = self.getAPIParams(page: page, auctionType: auctionType)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .auctionCarList(param: auctionCarParams))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let vehicleData = result.toJSONData(),
                    let vehicleList = VehicleList(jsonData: vehicleData) {
                    completion(nextPageNumber, perPage, vehicleList)
                }
            }
        }
    }
    
    private func getAPIParams(page: Int, auctionType: AuctionType) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.roomId] = UserSession.shared.getUserId() as AnyObject
        param[ConstantAPIKeys.type] = auctionType.rawValue as AnyObject
        param[ConstantAPIKeys.auctionList] = AuctionListType.myAuction.rawValue as AnyObject
        param[ConstantAPIKeys.page] = page as AnyObject
        return param
    }
}
