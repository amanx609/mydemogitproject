//
//  DSoldUnsoldCarViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 16/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DSoldUnsoldCarViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var soldUnsoldTableView: UITableView!
    
    //MARK: - Variables
    var viewModel: DSoldUnsoldCarVModeling?
    var auctionType: AuctionType = .unsoldCars
    var auctionCarsDataSource: [Vehicle] = []
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: self.auctionType.headerTitle, barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.registerNibs()
        self.setupTableView()
        self.requestAuctionCarListAPI(page: self.nextPageNumber)
    }
        
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = DSoldUnsoldCarViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.soldUnsoldTableView.delegate = self
        self.soldUnsoldTableView.dataSource = self
        self.soldUnsoldTableView.separatorStyle = .none
    }
    
    private func registerNibs() {
        self.soldUnsoldTableView.register(SoldUnsoldCarCell.self)
    }
    
    //MARK: - APIMethods
    func requestAuctionCarListAPI(page: Int) {
        self.viewModel?.requestAuctionCarListAPI(page: page, auctionType: self.auctionType, completion: { [weak self] (nextPage, perPage, vehicleList) in
            guard let sSelf = self,
                  let vehicles = vehicleList.vehicles else { return }
            sSelf.nextPageNumber = nextPage
            sSelf.perPage = perPage
            if page == 1 {
                sSelf.auctionCarsDataSource.removeAll()
            }
            sSelf.auctionCarsDataSource = sSelf.auctionCarsDataSource + vehicles
            Threads.performTaskInMainQueue {
                sSelf.soldUnsoldTableView.reloadData()
            }
        })
    }
}
