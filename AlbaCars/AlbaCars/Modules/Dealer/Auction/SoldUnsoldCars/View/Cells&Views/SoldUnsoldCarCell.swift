//
//  SoldUnsoldCarCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 16/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SoldUnsoldCarCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var auctionByPlaceholderLabel: UILabel!
    @IBOutlet weak var dealerImageView: UIImageView!
    @IBOutlet weak var dealerNameLabel: UILabel!
    @IBOutlet weak var vehicleImageView: UIImageView!
    @IBOutlet weak var vehicleNameLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var odometerLabel: UILabel!
    @IBOutlet weak var specsLabel: UILabel!
    @IBOutlet weak var reservePriceLabel: UILabel!
    @IBOutlet weak var soldUnsoldButton: UIButton!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.shadowView.addShadow(ofColor:  UIColor.shadowColor, radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        self.containerView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
    }
    
    //MARK: - Public Methods
    //SoldCarsAuction
    func configureViewForSoldCarsAuction(auctionCar: Vehicle, auctionType: AuctionType) {
        //DealerInfo
        if let dealerImage = auctionCar.image {
            self.dealerImageView.setImage(urlStr: dealerImage, placeHolderImage: nil)
        }
        self.dealerNameLabel.text = auctionCar.name
        //VehicleInfo
        if let vehicleImage = auctionCar.vehicleImages?.first {
            self.vehicleImageView.setImage(urlStr: vehicleImage, placeHolderImage: nil)
        }
        var carName = ""
        if let brandName = auctionCar.brandName {
            carName = brandName + " "
        }
        if let vehileName = auctionCar.modelName {
            carName = carName + vehileName
        }
        self.vehicleNameLabel.text = carName
        if let modelYear = auctionCar.year {
            let modelYearText = String.getAttributedText(firstText: "Model :".localizedString(), secondText: " \(modelYear)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.modelLabel.attributedText = modelYearText
            
        }
        if let mileage = auctionCar.mileage {
            let odoMeter = Int(mileage).formattedWithSeparator
            let odoMeterValue = "\(odoMeter) " + "Km".localizedString()
            let odometerText = String.getAttributedText(firstText: "Odometer :".localizedString(), secondText: " \(odoMeterValue)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.odometerLabel.attributedText = odometerText
        }
        if let specs = auctionCar.specs {
            let specsText = String.getAttributedText(firstText: "Specs :".localizedString(), secondText: " \(specs)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.specsLabel.attributedText = specsText
        }
        
        if let price = auctionCar.price {
            let priceText = String.getAttributedText(firstText: "Reserve Price :".localizedString(), secondText: " \(price)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.reservePriceLabel.attributedText = priceText
        }
        
        if auctionType == .soldCars {
            self.soldUnsoldButton.backgroundColor = .greenColor
            self.soldUnsoldButton.setTitle("Sold".localizedString(), for: .normal)
        } else {
            self.soldUnsoldButton.backgroundColor = .redButtonColor
            self.soldUnsoldButton.setTitle("Unsold".localizedString(), for: .normal)
        }
    }
}
