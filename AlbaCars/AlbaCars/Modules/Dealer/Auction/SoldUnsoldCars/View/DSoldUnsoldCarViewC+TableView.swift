//
//  DSoldUnsoldCarViewC+TableView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 16/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

extension DSoldUnsoldCarViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.auctionCarsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.CellHeightConstants.height_210
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getCell(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      guard let lastVisibleRow = self.soldUnsoldTableView.indexPathsForVisibleRows?.last?.row else { return }
        if (self.auctionCarsDataSource.count - lastVisibleRow == Constants.minCountBeforeAPICall) && self.nextPageNumber != 0 {
        self.requestAuctionCarListAPI(page: self.nextPageNumber)
      }
    }
    
}

extension DSoldUnsoldCarViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: SoldUnsoldCarCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let auctionCar = self.auctionCarsDataSource[indexPath.row]
        cell.configureViewForSoldCarsAuction(auctionCar: auctionCar, auctionType: self.auctionType)
        return cell
    }
}
