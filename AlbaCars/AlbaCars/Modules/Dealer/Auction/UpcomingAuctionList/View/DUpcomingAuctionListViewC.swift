//
//  UpcomingAuctionListViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/6/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DUpcomingAuctionListViewC: BaseViewC {
    
    //MARK: -IBOutlets
    
    @IBOutlet weak var upcomingAuctionListTableView: UITableView!
    
    //MARK: - Variables
    var viewModel: DAuctionListVModeling?
    var params: APIParams = APIParams()
    var isMyAuction: Bool = false
    var auctionCarsDataSource: [Vehicle] = []
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func navigationButtonTapped(_ sender: AnyObject) {
        guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
        switch buttonType {
        case .filterWhite: self.presentFilterViewC()
        case .backWhite: self.navigationController?.popViewController(animated: true)
        default: break
        }
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Upcoming Auctions".localizedString(), barColor: UIColor.redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [.filterWhite])
        self.recheckVM()
        self.setupTableView()
        self.requestAuctionCarListAPI(page: self.nextPageNumber)
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = DAuctionListViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.upcomingAuctionListTableView.delegate = self
        self.upcomingAuctionListTableView.dataSource = self
        self.upcomingAuctionListTableView.separatorStyle = .none
    }
    
    private func registerNibs() {
        self.upcomingAuctionListTableView.register(AuctionListTableCell.self)
    }
    
    private func presentFilterViewC() {
        let filterVC = DIConfigurator.sharedInstance.getDAuctionFilterVC()
        filterVC.selectedParams = self.params
        filterVC.selectParams = { [weak self] (params) in
            guard let sSelf = self else {
                return
            }
            if let filterParam = params {
                sSelf.params = filterParam
            }
            sSelf.nextPageNumber = 1
            sSelf.requestAuctionCarListAPI(page: sSelf.nextPageNumber)
        }
        let navC = UINavigationController(rootViewController: filterVC)
        self.present(navC, animated: true, completion: nil)
    }
    
    //MARK: - APIMethods
    func requestAuctionCarListAPI(page: Int) {
        //AuctionType
        self.params[ConstantAPIKeys.type] = 2 as AnyObject
        //AuctionListType
        self.params[ConstantAPIKeys.auctionList] = AuctionListType.upcoming.rawValue as AnyObject
        //RoomId
        if self.isMyAuction {
            self.params[ConstantAPIKeys.roomId] = UserSession.shared.getUserId() as AnyObject
        } else {
            self.params[ConstantAPIKeys.roomId] = 0 as AnyObject
        }
        //Page
        self.params[ConstantAPIKeys.page] = page as AnyObject
        self.viewModel?.requestAuctionCarListAPI(params: self.params, completion: { [weak self] (nextPage, perPage, vehicleList) in
            guard let sSelf = self,
                let vehicles = vehicleList.vehicles else { return }
            sSelf.nextPageNumber = nextPage
            sSelf.perPage = perPage
            if page == 1 {
                sSelf.auctionCarsDataSource.removeAll()
            }
            sSelf.auctionCarsDataSource = sSelf.auctionCarsDataSource + vehicles
            Threads.performTaskInMainQueue {
                sSelf.upcomingAuctionListTableView.reloadData()
            }
        })

    }
    
    //MARK: - APIMethods
    func requestVehicleDetailsAPI(vehicleId: Int,notificationType: [Int]) {
        self.viewModel?.requestNotifyAPI(vehicleId: vehicleId, notificationType: notificationType, completion: { [weak self] (status) in
            guard self != nil else { return }
        })
    }
    
    func requestAuctionAPI(vehicleId: Int,vehicle:Vehicle) {
           
           let auctionListViewM:DAuctionListViewM = DAuctionListViewM()
              auctionListViewM.requestEnterAuctionDetailsAPI(vehicleId: vehicleId) { (responseData) in
                  Threads.performTaskInMainQueue {
                   let liveAuctionRoomVC = DIConfigurator.sharedInstance.getDLiveAuctionRoomVC()
                   liveAuctionRoomVC.enterAuctionRoomModel = responseData
                   liveAuctionRoomVC.vehicle = vehicle
                   self.navigationController?.pushViewController(liveAuctionRoomVC, animated: true)
              }
          }
    }
}

