//
//  DAuctionAddCarDropDownCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol DAuctionAddCarDropDownCellDelegate: class {
    func didTapDAuctionAddCarDropDownCell(cell: DAuctionAddCarDropDownCell)
}

class DAuctionAddCarDropDownCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var placeholderImageView: UIImageView!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - Variables
    weak var delegate: DAuctionAddCarDropDownCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.dropDownView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.titleLabel.text = cellInfo.placeHolder
        self.placeholderLabel.text = cellInfo.value
        
        if let info = cellInfo.info {
            //Image
            if let placeholderImage = info[Constants.UIKeys.placeholderImage] as? UIImage {
                self.placeholderImageView.image = placeholderImage
            }
            
            if let isHide = info[Constants.UIKeys.isHide] as? Bool {
                self.dropDownView.isHidden = isHide
            } else {
                self.dropDownView.isHidden = false
            }
        } else {
            self.dropDownView.isHidden = false
        }
        
    }
    
    //MARK: - IBActions
    
    @IBAction func dropDownButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapDAuctionAddCarDropDownCell(cell: self)
        }
    }
    
}
