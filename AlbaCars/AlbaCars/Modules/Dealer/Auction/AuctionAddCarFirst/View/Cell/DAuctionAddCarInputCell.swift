//
//  DAuctionAddCarInputCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol DAuctionAddCarInputCellDelegate: class {
    func tapNextKeyboard(cell: DAuctionAddCarInputCell)
    func didChangeText(cell: DAuctionAddCarInputCell, text: String)
}

class DAuctionAddCarInputCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var placeholderImageView: UIImageView!
    @IBOutlet weak var inputTextFieldView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var textFielLeftLabel: UILabel!
    
    //MARK: - Variables
    weak var delegate: DAuctionAddCarInputCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.inputTextFieldView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.inputTextField.delegate = self
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.titleLabel.text = cellInfo.placeHolder
        self.inputTextField.text = cellInfo.value
        self.inputTextField.keyboardType = cellInfo.keyboardType
        if let info = cellInfo.info {
            //Image
            if let placeholderImage = info[Constants.UIKeys.placeholderImage] as? UIImage {
                self.placeholderImageView.image = placeholderImage
            }
            
            if let isHide = info[Constants.UIKeys.isHide] as? Bool {
                self.textFielLeftLabel.isHidden = isHide
                self.inputTextField.isEnabled = !isHide
            } else {
                self.textFielLeftLabel.isHidden = true
            }
            
            if let placeholderText = info[Constants.UIKeys.placeholderText] as? String {
                self.inputTextField.setPlaceHolderColor(text: placeholderText, color: UIColor.black)
                
                
            }
        } else {
            self.textFielLeftLabel.isHidden = true
        }
    }
    
}

extension DAuctionAddCarInputCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let delegate = self.delegate {
            delegate.tapNextKeyboard(cell: self)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let delegate = delegate, let text = textField.text {
            delegate.didChangeText(cell: self, text: text)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text), let delegate = self.delegate {
            let finalText = text.replacingCharacters(in: textRange, with: string)
            delegate.didChangeText(cell: self, text: finalText)
        }
        return true
    }
}
