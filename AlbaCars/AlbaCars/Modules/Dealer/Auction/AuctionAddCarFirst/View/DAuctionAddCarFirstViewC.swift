//
//  DAuctionAddCarFirstViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DAuctionAddCarFirstViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var addCarTableView: UITableView!
    
    //MARK: - Properties
    var viewModel: DAuctionAddCarFirstViewModeling?
    var addCarViewModel: CAddNewCarViewModeling?
    
    var addCarDataSource: [CellInfo] = []
    var vehicleBrandDataSource: [[String: AnyObject]] = []
    var vehicleModelDataSource: [[String: AnyObject]] = []
    var vehicleSubModelDataSource: [[String: AnyObject]] = []
    var auctionModel = DAuctionModel()
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Add Car Details".localizedString(),barColor: .redButtonColor,titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.loadDataSource()
       
    }
    
    //MARK: - Private Methods
    private func setup() {
        setupViewModel()
        registerNibs()
        setupTableView()
        self.requestVehicleBrandAPI()
    }
    
    private func setupTableView() {
        self.addCarTableView.separatorStyle = .none
        self.addCarTableView.backgroundColor = UIColor.white
        self.addCarTableView.delegate = self
        self.addCarTableView.dataSource = self
        self.addCarTableView.allowsSelection =  false
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = DAuctionAddCarFirstViewM()
        }
        
        if self.addCarViewModel == nil {
            self.addCarViewModel = CAddNewCarVM()
        }
    }
    
    private func registerNibs() {
        self.addCarTableView.register(DAuctionAddCarInputCell.self)
        self.addCarTableView.register(DAuctionAddCarDropDownCell.self)
        self.addCarTableView.register(BottomButtonCell.self)
        self.addCarTableView.register(TyresConditionCell.self)
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getAddCarFormDataSource(basicDetail: auctionModel.basicDetails) {
            self.addCarDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.addCarTableView.reloadData()
            }
        }
    }
    
    //MARK: - Public Methods
    func setupToolBar(_ textField: UITextField) {
        textField.inputAccessoryView = Helper.getSingleButtonToolBar(buttonTitle: StringConstants.Text.done.localizedString(), selector: #selector(self.doneButtonTapped), target: self)
    }
    
    func showDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        switch indexPath.row {
        case 0:
            if self.vehicleBrandDataSource.count == 0 {
                self.requestVehicleBrandAPI()
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Brand".localizedString(), arrayList: self.vehicleBrandDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let brandId = response[ConstantAPIKeys.id] as? Int,
                    let brandName = response[ConstantAPIKeys.title] as? String else { return }
                sSelf.requestVehicleModelAPI(brandId: brandId)
                sSelf.addCarDataSource[indexPath.row].value = brandName
                sSelf.addCarDataSource[indexPath.row].info?[Constants.UIKeys.firstValue] = "\(brandId)" as AnyObject
                sSelf.addCarTableView.reloadData()
            }
        case 1:
            if self.vehicleModelDataSource.count == 0 {
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Model".localizedString(), arrayList: self.vehicleModelDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let modelName = response[ConstantAPIKeys.title] as? String,
                    let modelId = response[ConstantAPIKeys.id] as? Int else { return }
                
                sSelf.addCarDataSource[indexPath.row].value = modelName
                sSelf.addCarDataSource[indexPath.row].info?[Constants.UIKeys.firstValue] = "\(modelId)" as AnyObject
                sSelf.addCarTableView.reloadData()
            }
        case 2:
            if self.vehicleModelDataSource.count == 0 {
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Sub - Model".localizedString(), arrayList: self.vehicleModelDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let modelName = response[ConstantAPIKeys.title] as? String,
                    let modelId = response[ConstantAPIKeys.id] as? Int else { return }
                sSelf.addCarDataSource[indexPath.row].value = modelName
                sSelf.addCarDataSource[indexPath.row].info?[Constants.UIKeys.firstValue] = "\(modelId)" as AnyObject
                sSelf.addCarTableView.reloadData()
            }
        case 4:
            let yearsList = Helper.getListOfYearsFrom(2010)
            listPopupView.initializeViewWith(title: "Year".localizedString(), arrayList: yearsList, key: Constants.UIKeys.year) { [weak self] (response) in
                guard let sSelf = self,
                    let carType = response[Constants.UIKeys.year] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = carType
                sSelf.addCarTableView.reloadData()
            }
        case 6:
            listPopupView.initializeViewWith(title: DropDownType.cylinders.title, arrayList: DropDownType.cylinders.dataList, key: DropDownType.cylinders.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let cylinders = response[DropDownType.cylinders.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = cylinders
                sSelf.addCarTableView.reloadData()
            }
        case 7:
            listPopupView.initializeViewWith(title: DropDownType.engineSize.title, arrayList: DropDownType.engineSize.dataList, key: DropDownType.engineSize.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let engineSize = response[DropDownType.engineSize.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = engineSize
                sSelf.addCarTableView.reloadData()
            }
        case 8:
            listPopupView.initializeViewWith(title: DropDownType.specs.title, arrayList: DropDownType.specs.dataList, key: DropDownType.specs.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let specs = response[DropDownType.specs.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = specs
                sSelf.addCarTableView.reloadData()
            }
        case 11:
            listPopupView.initializeViewWith(title: DropDownType.transmission.title, arrayList: DropDownType.transmission.dataList, key: DropDownType.transmission.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let transmission = response[DropDownType.transmission.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = transmission
                sSelf.addCarTableView.reloadData()
            }
        case 12:
            listPopupView.initializeViewWith(title: DropDownType.fuelType.title, arrayList: DropDownType.fuelType.dataList, key: DropDownType.fuelType.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let fuelType = response[DropDownType.fuelType.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = fuelType
                sSelf.addCarTableView.reloadData()
            }
        case 13:
            listPopupView.initializeViewWith(title: DropDownType.noOfDoors.title, arrayList: DropDownType.noOfDoors.dataList, key: DropDownType.noOfDoors.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let noOfDoors = response[DropDownType.noOfDoors.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = noOfDoors
                sSelf.addCarTableView.reloadData()
            }
        case 14:
            listPopupView.initializeViewWith(title: DropDownType.drive.title, arrayList: DropDownType.drive.dataList, key: DropDownType.drive.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let drive = response[DropDownType.drive.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = drive
                sSelf.addCarTableView.reloadData()
            }
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    
    //MARK: - API Methods
    func requestVehicleBrandAPI() {
        self.addCarViewModel?.requestVehicleBrandAPI(completion: { (success, vehicleBrands) in
            if success {
                self.vehicleBrandDataSource = vehicleBrands
            }
        })
    }
    
    func requestVehicleSubModelAPI() {
        self.addCarViewModel?.requestVehicleTypeAPI(completion: { (success, vehicleTypes) in
            if success {
                self.vehicleSubModelDataSource = vehicleTypes
            }
        })
    }
    
    func requestVehicleModelAPI(brandId: Int) {
        self.addCarViewModel?.requestVehicleModelAPI(brandId: brandId, completion: { (success, vehicleModels) in
            if success {
                self.vehicleModelDataSource = vehicleModels
            }
        })
    }
    
    //MARK: - API Methods
    func requestAuctionAPI() {
        self.viewModel?.requestAuctionAPI(arrData: self.addCarDataSource, auctionId: Helper.toInt(self.auctionModel.basicDetails?.id)) { (responseData) in
            self.auctionModel = responseData
            Threads.performTaskInMainQueue {
                let auctionAddCarSecondVC = DIConfigurator.sharedInstance.getDAuctionAddCarSecondVC()
                auctionAddCarSecondVC.auctionModel = self.auctionModel
                self.navigationController?.pushViewController(auctionAddCarSecondVC, animated: true)
            }
        }
    }
    
    //MARK: - Selectors
     @objc func doneButtonTapped() {
       self.view.endEditing(true)
     }
}
