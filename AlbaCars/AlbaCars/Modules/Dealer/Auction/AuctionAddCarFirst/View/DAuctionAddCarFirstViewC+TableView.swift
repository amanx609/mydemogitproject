//
//  DAuctionAddCarFirstViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import Foundation
import UIKit

extension DAuctionAddCarFirstViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addCarDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.addCarDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.addCarDataSource[indexPath.row].height
    }
}

extension DAuctionAddCarFirstViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .DAuctionAddCarInputCell:
            let cell: DAuctionAddCarInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            if indexPath.row == 5 {
                self.setupToolBar(cell.inputTextField)
            }
            return cell
        case .DAuctionAddCarDropDownCell:
            let cell: DAuctionAddCarDropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .BottomButtonCell:
            let cell: BottomButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .TyresConditionCell:
            let cell: TyresConditionCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate =  self
            cell.configureEngineSizeView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - DAuctionAddCarDropDownCellDelegate
extension DAuctionAddCarFirstViewC: DAuctionAddCarDropDownCellDelegate {
    func didTapDAuctionAddCarDropDownCell(cell: DAuctionAddCarDropDownCell) {
        guard let indexPath = self.addCarTableView.indexPath(for: cell) else { return }
        self.showDropDownAt(indexPath: indexPath)
    }
}

//MARK: - DAuctionAddCarInputCellDelegate
extension DAuctionAddCarFirstViewC: DAuctionAddCarInputCellDelegate {
    func tapNextKeyboard(cell: DAuctionAddCarInputCell) {
        self.view.endEditing(true)
    }
    
    func didChangeText(cell: DAuctionAddCarInputCell, text: String) {
        guard let indexPath = self.addCarTableView.indexPath(for: cell) else { return }
        self.addCarDataSource[indexPath.row].value = text
    }
}

//MARK: - BottomButtonCellDelegate
extension DAuctionAddCarFirstViewC: BottomButtonCellDelegate {
    func didTapBottomButton(cell: BottomButtonCell) {
        self.view.endEditing(true)
        self.requestAuctionAPI()
        //        if let isValid = self.viewModel?.validatePostCarData(arrData: self.addCarDataSource),isValid {
        //            if let param = self.viewModel?.getPostAddCarParams(arrData: self.addCarDataSource) {
        //               let auctionAddCarSecondVC = DIConfigurator.sharedInstance.getDAuctionAddCarSecondVC()
        //                self.navigationController?.pushViewController(auctionAddCarSecondVC, animated: true)
        //            }
        //        }
    }
}


//MARK: - TyresConditionCellDelegate
extension DAuctionAddCarFirstViewC: TyresConditionCellDelegate {
    
    func didTapFirstDropDownCell(cell: TyresConditionCell) {
        guard let indexPath = self.addCarTableView.indexPath(for: cell) else { return }
        let cellInfo = self.addCarDataSource[indexPath.row]
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: cellInfo.placeHolder, arrayList: DropDownType.engineSize.dataList, key: DropDownType.engineSize.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let engineSize = response[DropDownType.engineSize.rawValue] as? String else { return }
            sSelf.addCarDataSource[indexPath.row].info?[Constants.UIKeys.firstValue] = engineSize as AnyObject
            sSelf.addCarTableView.reloadData()
        }
        listPopupView.showWithAnimated(animated: true)
        
    }
    
    func didTapSecondDropDownCell(cell: TyresConditionCell) {
        guard let indexPath = self.addCarTableView.indexPath(for: cell) else { return }
        let cellInfo = self.addCarDataSource[indexPath.row]
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: cellInfo.placeHolder, arrayList: DropDownType.engineSize.dataList, key: DropDownType.engineSize.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let engineSize = response[DropDownType.engineSize.rawValue] as? String else { return }
            sSelf.addCarDataSource[indexPath.row].info?[Constants.UIKeys.secondValue] = engineSize as AnyObject
            sSelf.addCarTableView.reloadData()
        }
        listPopupView.showWithAnimated(animated: true)
        
    }
    
}
