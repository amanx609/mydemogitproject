//
//  DAuctionAddCarFirstViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DAuctionAddCarFirstViewModeling {
    func getAddCarFormDataSource(basicDetail: BasicDetails?) -> [CellInfo]
    func validatePostCarData(arrData: [CellInfo]) -> Bool
    func requestAuctionAPI(arrData: [CellInfo],auctionId:Int, completion: @escaping (DAuctionModel)-> Void)
}

class DAuctionAddCarFirstViewM: DAuctionAddCarFirstViewModeling {
    
    func getAddCarFormDataSource(basicDetail: BasicDetails?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        // Brand Cell
        var brandInfo = [String: AnyObject]()
        brandInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "brand")
        brandInfo[Constants.UIKeys.firstValue] = Helper.toString(object: basicDetail?.brand) as AnyObject
        let brandCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Brand".localizedString(), value:Helper.toString(object: basicDetail?.brandName) ,info: brandInfo, height: Constants.CellHeightConstants.height_80)
        array.append(brandCell)
        
        // Model Cell
        var modelInfo = [String: AnyObject]()
        modelInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "ext_color")
        modelInfo[Constants.UIKeys.firstValue] = Helper.toString(object: basicDetail?.model) as AnyObject
        let modelCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Model".localizedString(), value:Helper.toString(object: basicDetail?.modelName) ,info: modelInfo, height: Constants.CellHeightConstants.height_80)
        array.append(modelCell)
        
        // Sub - Model Cell
        var subModelInfo = [String: AnyObject]()
        subModelInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "sub_model")
        subModelInfo[Constants.UIKeys.firstValue] = Helper.toString(object: basicDetail?.submodel) as AnyObject
        let subModelCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Sub - Model".localizedString(), value:Helper.toString(object: basicDetail?.subModelName) ,info: subModelInfo, height: Constants.CellHeightConstants.height_80)
        array.append(subModelCell)
        
        // Trim Cell
        var trimInfo = [String: AnyObject]()
        trimInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "trim")
        trimInfo[Constants.UIKeys.placeholderText] = "Input".localizedString() as AnyObject
        let trimCell = CellInfo(cellType: .DAuctionAddCarInputCell, placeHolder: "Trim".localizedString(), value:Helper.toString(object: basicDetail?.trim) ,info: trimInfo, height: Constants.CellHeightConstants.height_80)
        array.append(trimCell)
        
        // Year Cell
        var yearInfo = [String: AnyObject]()
        yearInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "year")
        let yearCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Year".localizedString(), value:Helper.toString(object: basicDetail?.year) ,info: yearInfo, height: Constants.CellHeightConstants.height_80)
        array.append(yearCell)
        
        //Mileage Cell
        var mileageInfo = [String: AnyObject]()
        mileageInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "mileage")
        mileageInfo[Constants.UIKeys.placeholderText] = "Enter Mileage".localizedString() as AnyObject
        let mileageCell = CellInfo(cellType: .DAuctionAddCarInputCell, placeHolder: "Mileage".localizedString(), value:Helper.toString(object: basicDetail?.mileage) ,info: mileageInfo, height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.numberPad)
        array.append(mileageCell)
        
        // Cylinders Cell
        var cylindersInfo = [String: AnyObject]()
        cylindersInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "cylinders")
        let cylindersCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Cylinders".localizedString(), value:Helper.toString(object: basicDetail?.cylinders) ,info: cylindersInfo, height: Constants.CellHeightConstants.height_80)
        array.append(cylindersCell)
        
        // Engine Size Cell
        var engineSizeInfo = [String: AnyObject]()
        engineSizeInfo[Constants.UIKeys.firstValue] = "" as AnyObject
        engineSizeInfo[Constants.UIKeys.secondValue] = "" as AnyObject
        engineSizeInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "malfunction_indicador")
        let engine = Helper.toString(object: basicDetail?.engine)
        if !engine.isEmpty {
            let formattedArray = engine.components(separatedBy: ".")
            if formattedArray.count > 1 {
                engineSizeInfo[Constants.UIKeys.firstValue] = formattedArray[0] as AnyObject
                engineSizeInfo[Constants.UIKeys.secondValue] = formattedArray[1] as AnyObject
            }
        }
        let engineSizeCell = CellInfo(cellType: .TyresConditionCell, placeHolder: "Engine Size".localizedString(), value:Helper.toString(object: basicDetail?.engine) ,info: engineSizeInfo, height: Constants.CellHeightConstants.height_80)
        array.append(engineSizeCell)
        
        // Specs Cell
        var specsInfo = [String: AnyObject]()
        specsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "specs")
        let specsCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Specs".localizedString(), value:Helper.getDefaultString(object: basicDetail?.specs, defaultString: "GCC".localizedString()) ,info: specsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(specsCell)
        
        //Ext. Color Cell
        var extColorInfo = [String: AnyObject]()
        extColorInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "ext_color")
        extColorInfo[Constants.UIKeys.placeholderText] = "".localizedString() as AnyObject
        let extColorCell = CellInfo(cellType: .DAuctionAddCarInputCell, placeHolder: "Ext. Color".localizedString(), value:Helper.toString(object: basicDetail?.extColor) ,info: extColorInfo, height: Constants.CellHeightConstants.height_80)
        array.append(extColorCell)
        
        //Int. Color Cell
        var intColorInfo = [String: AnyObject]()
        intColorInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "int_color")
        intColorInfo[Constants.UIKeys.placeholderText] = "".localizedString() as AnyObject
        let intColorCell = CellInfo(cellType: .DAuctionAddCarInputCell, placeHolder: "Int. Color".localizedString(), value:Helper.toString(object: basicDetail?.innerColor) ,info: intColorInfo, height: Constants.CellHeightConstants.height_80)
        array.append(intColorCell)
        
        // Transmission Cell
        var transmissionInfo = [String: AnyObject]()
        transmissionInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "transmission")
        let transmissionCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Transmission".localizedString(), value:Helper.getDefaultString(object: basicDetail?.transmission, defaultString: "Automatic".localizedString()) ,info: transmissionInfo, height: Constants.CellHeightConstants.height_80)
        array.append(transmissionCell)
        
        // Fuel Type Cell
        var fuelTypeInfo = [String: AnyObject]()
        fuelTypeInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "fuel_type")
        let fuelTypeCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Fuel Type".localizedString(), value:Helper.getDefaultString(object: basicDetail?.fuelType, defaultString: "Petrol".localizedString()) ,info: fuelTypeInfo, height: Constants.CellHeightConstants.height_80)
        array.append(fuelTypeCell)
        
        // No. Of Doors Cell
        var noOfDoorInfo = [String: AnyObject]()
        noOfDoorInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "num_of_door")
        let noOfDoorCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "No. Of Doors".localizedString(), value:Helper.getDefaultString(object: basicDetail?.noOfDoors, defaultString: "4".localizedString()) ,info: noOfDoorInfo, height: Constants.CellHeightConstants.height_80)
        array.append(noOfDoorCell)
        
        // DriveCell
        var driveInfo = [String: AnyObject]()
        driveInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "drive")
        let driveCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Drive".localizedString(), value:Helper.getDefaultString(object: basicDetail?.drive, defaultString: "2WD".localizedString()) ,info: driveInfo, height: Constants.CellHeightConstants.height_80)
        array.append(driveCell)
        
        //Next Cell
        let submitCell = CellInfo(cellType: .BottomButtonCell, placeHolder: "Next".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_100)
        array.append(submitCell)
        
        return array
    }
    
    //MARK: - API Methods
    func requestAuctionAPI(arrData: [CellInfo],auctionId: Int, completion: @escaping (DAuctionModel)-> Void) {
        if self.validatePostCarData(arrData: arrData) {
            let params = self.getPostAddCarParams(arrData: arrData, auctionId: auctionId)
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .postAuctionCar(param: params))) { (response, success) in
                if success {
                    if let safeResponse =  response as? [String: AnyObject],
                        let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                        let auctionData = result.toJSONData() {
                        do {
                            let decoder = JSONDecoder()
                            let auctionModelData = try decoder.decode(DAuctionModel.self, from: auctionData)
                            completion(auctionModelData)
                        } catch let error {
                            print(error)
                        }
                    }
                }
            }
        }
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
    func validatePostCarData(arrData: [CellInfo]) -> Bool {
        var isValid = true
        var message = ""
        if let carBrand = arrData[0].value, carBrand.isEmpty {
            message = "Please select car brand."
            isValid = false
        } else if let carModel = arrData[1].value, carModel.isEmpty  {
            message = "Please select car model."
            isValid = false
        } else if let carType = arrData[2].value, carType.isEmpty {
            message = "Please select car sub - model."
            isValid = false
        }  else if let trim = arrData[3].value, trim.isEmpty {
            message = "Please enter trim."
            isValid = false
        } else if let carYear = arrData[4].value,carYear.isEmpty {
            message = "Please select car year."
            isValid = false
        } else if let carMileage = arrData[5].value, carMileage.isEmpty {
            message = "Please enter car mileage."
            isValid = false
        } else if let noOfCylinders = arrData[6].value, noOfCylinders.isEmpty {
            message = "Please select no. of cylinders."
            isValid = false
        }else if let firstValue = arrData[7].info?[Constants.UIKeys.firstValue] as? String, firstValue.isEmpty {
            message = "Please select engine size."
            isValid = false
        } else if let secondValue = arrData[7].info?[Constants.UIKeys.secondValue] as? String, secondValue.isEmpty {
            message = "Please select engine size."
            isValid = false
        }
            //        else if let engineSize = arrData[7].value, engineSize.isEmpty {
            //            message = "Please select engine size."
            //            isValid = false
            //        }
        else if let specs = arrData[8].value, specs.isEmpty {
            message = "Please select specs."
            isValid = false
        } else if let color = arrData[9].value, color.isEmpty {
            message = "Please enter ext. color."
            isValid = false
        } else if let color = arrData[10].value, color.isEmpty {
            message = "Please enter int. color."
            isValid = false
        } else if let transmission = arrData[11].value, transmission.isEmpty {
            message = "Please select transmission."
            isValid = false
        }  else if let doors = arrData[12].value, doors.isEmpty {
            message = "Please select fuel type"
            isValid = false
        } else if let doors = arrData[13].value, doors.isEmpty {
            message = "Please select no. of doors."
            isValid = false
        } else if let drive = arrData[14].value, drive.isEmpty {
            message = "Please select drive."
            isValid = false
        }
        
        if !isValid {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: message.localizedString())
        }
        return isValid
    }
    
    func getPostAddCarParams(arrData: [CellInfo],auctionId:Int)-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.auctionPage] = ConstantAPIKeys.basicDetails as AnyObject
        params[ConstantAPIKeys.brand] = arrData[0].info?[Constants.UIKeys.firstValue] as AnyObject
        params[ConstantAPIKeys.carModel] = arrData[1].info?[Constants.UIKeys.firstValue] as AnyObject
        params[ConstantAPIKeys.submodel] = arrData[2].info?[Constants.UIKeys.firstValue] as AnyObject
        params[ConstantAPIKeys.trim] = arrData[3].value as AnyObject
        params[ConstantAPIKeys.carYear] = arrData[4].value as AnyObject
        params[ConstantAPIKeys.mileage] = arrData[5].value as AnyObject
        params[ConstantAPIKeys.cylinders] = arrData[6].value as AnyObject
        
        var engine = ""
        if let firstValue = arrData[7].info?[Constants.UIKeys.firstValue] as? String,let secondValue = arrData[7].info?[Constants.UIKeys.secondValue]  as? String {
            engine = firstValue + "." + secondValue
        }
         params[ConstantAPIKeys.engine] = engine as AnyObject
      //  params[ConstantAPIKeys.engine] = arrData[7].value as AnyObject
        
        params[ConstantAPIKeys.specs] = arrData[8].value as AnyObject
        params[ConstantAPIKeys.extColor] = arrData[9].value as AnyObject
        params[ConstantAPIKeys.innerColor] = arrData[10].value as AnyObject
        params[ConstantAPIKeys.transmission] = arrData[11].value as AnyObject
        params[ConstantAPIKeys.fuelType] = arrData[12].value as AnyObject
        params[ConstantAPIKeys.noOfDoors] = arrData[13].value as AnyObject
        params[ConstantAPIKeys.drive] = arrData[14].value as AnyObject
        params[ConstantAPIKeys.id] = auctionId as AnyObject
        return params
    }
}
