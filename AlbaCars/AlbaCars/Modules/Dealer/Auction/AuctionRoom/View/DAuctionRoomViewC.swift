//
//  AuctionRoomViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/9/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DAuctionRoomViewC: BaseViewC {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var auctionListTableView: UITableView!
    @IBOutlet weak var upcomingButton: UIButton!
    @IBOutlet weak var liveButton: UIButton!
    @IBOutlet weak var bottomMarkerViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var ratingView: RatingView!
    @IBOutlet weak var infoValue2Label: UILabel!
    @IBOutlet weak var infoTitle2Label: UILabel!
    @IBOutlet weak var infoValue1Label: UILabel!
    @IBOutlet weak var infoTitle1Label: UILabel!
    @IBOutlet weak var companyLogoImageView: UIImageView!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var sellingRatioLabel: UILabel!
    @IBOutlet weak var ratioProgressView: UIProgressView!
    @IBOutlet weak var ratioProgressValue: UILabel!
    @IBOutlet weak var auctionStatusButton: UIButton!
    @IBOutlet weak var liveAuctionCountLabel: UILabel!
    
    
    //MARK: - Variables
    var viewModel: DAuctionRoomVModeling?
    var auctionRoom: AuctionRoom?
    var params: APIParams = APIParams()
    
    var auctionCarsDataSource: [Vehicle] = []
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    
    var auctionType: AuctionType = .live
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func navigationButtonTapped(_ sender: AnyObject) {
      guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
      switch buttonType {
      case .filterWhite: self.presentFilterViewC()
      case .backWhite: self.navigationController?.popViewController(animated: true)
      default: break
      }
    }
    
    private func setup() {
        var headerTitle = "Auction Room".localizedString()
        if let companyName = self.auctionRoom?.name {
            headerTitle = "\(companyName) " + "Auction Room".localizedString()
        }
        self.setupNavigationBarTitle(title: headerTitle, barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [.filterWhite])
        self.setupView()
        self.recheckVM()
        self.registerNibs()
        self.setupTableView()
        self.didTapButton(self.liveButton)
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = DAuctionRoomViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.auctionListTableView.delegate = self
        self.auctionListTableView.dataSource = self
        self.auctionListTableView.separatorStyle = .none
    }
    
    private func registerNibs() {
        self.auctionListTableView.register(AuctionListTableCell.self)
    }
    
    private func setupView() {
        self.auctionStatusButton.setTitle("LIVE".localizedString(), for: .normal)
        self.auctionStatusButton.roundCorners(Constants.UIConstants.sizeRadius_10)
        self.infoTitle1Label.text = "Cars Added (Till Date)".localizedString()
        self.infoTitle2Label.text = "Cars Sold (Till Date)".localizedString()
        self.sellingRatioLabel.text = "Selling Ratio".localizedString()
        self.setAuctionRoomDataOnView()
    }
    
    private func setAuctionRoomDataOnView() {
        //CompanyInfo
        if let companyImage = self.auctionRoom?.image {
            self.companyLogoImageView.setImage(urlStr: companyImage, placeHolderImage: #imageLiteral(resourceName: "logo"))
        }
        if let companyName = self.auctionRoom?.name {
            self.companyNameLabel.text = companyName
        }
        if let description = self.auctionRoom?.description {
            self.descriptionLabel.text = description
        }
        //CarsInfo
        if let totalCars = self.auctionRoom?.totalCars,
            let soldCars = self.auctionRoom?.soldCars {
            self.infoValue1Label.text = "\(totalCars)"
            self.infoValue2Label.text = "\(soldCars)"
            if totalCars > 0, soldCars > 0 {
                let sellingRatio = soldCars/totalCars
                self.ratioProgressView.progress = Float(sellingRatio)
                self.ratioProgressValue.text = "\(sellingRatio)%"
            } else {
                self.ratioProgressView.progress = 0
                self.ratioProgressValue.text = "\(0)%"
            }
        }
        if let liveCars = self.auctionRoom?.liveCars {
            self.liveAuctionCountLabel.text = "\(liveCars)"
        }
    }
    
    private func loadDataSource() {
        self.nextPageNumber = 1
        self.requestAuctionCarListAPI(page: self.nextPageNumber)
    }
    
    private func didTapButton(_ sender: UIButton) {
        var leadingConstraint: CGFloat = 0.0
        switch sender {
        case self.liveButton:
            //ChangeBottomMarkerConstraint
            leadingConstraint = 0.0
            //ChangeButtonTitleColor
            self.liveButton.setTitleColor(.white, for: .normal)
            self.upcomingButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            self.auctionType = .live
            self.loadDataSource()
            
        case self.upcomingButton:
            //ChangeBottomMarkerConstraint
            leadingConstraint = Constants.Devices.ScreenWidth/2
            //ChangeButtonTitleColor
            self.liveButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            self.upcomingButton.setTitleColor(.white, for: .normal)
            self.auctionType = .upcoming
            self.loadDataSource()
            
        default: break
        }
        //AnimateTheBottomViewMarker
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseOut], animations: {
            self.bottomMarkerViewLeadingConstraint.constant = leadingConstraint
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    private func presentFilterViewC() {
        let filterVC = DIConfigurator.sharedInstance.getDAuctionFilterVC()
        filterVC.selectedParams = self.params
        filterVC.selectParams = { [weak self] (params) in
        guard let sSelf = self else {
            return
         }
            if let filterParam = params {
                sSelf.params = filterParam
            }
            sSelf.nextPageNumber = 1
            sSelf.requestAuctionCarListAPI(page: sSelf.nextPageNumber)
        }
        let navC = UINavigationController(rootViewController: filterVC)
        self.present(navC, animated: true, completion: nil)
    }
    
    //MARK: - APIMethods
    func requestAuctionCarListAPI(page: Int) {
        //AuctionType
        self.params[ConstantAPIKeys.type] = self.auctionType.rawValue as AnyObject
        //AuctionListType
        self.params[ConstantAPIKeys.auctionList] = AuctionListType.none.rawValue as AnyObject
        //RoomId
        if let auctionRoomId = self.auctionRoom?.id {
            self.params[ConstantAPIKeys.roomId] = auctionRoomId as AnyObject
        }
        //Page
        self.params[ConstantAPIKeys.page] = page as AnyObject
        self.viewModel?.requestAuctionCarListAPI(params: self.params, completion: { [weak self] (nextPage, perPage, vehicleList) in
            guard let sSelf = self,
                  let vehicles = vehicleList.vehicles else { return }
            sSelf.nextPageNumber = nextPage
            sSelf.perPage = perPage
            if page == 1 {
                sSelf.auctionCarsDataSource.removeAll()
            }
            sSelf.auctionCarsDataSource = sSelf.auctionCarsDataSource + vehicles
            Threads.performTaskInMainQueue {
                sSelf.auctionListTableView.reloadData()
            }
        })
    }
    
    //MARK: - APIMethods
    func requestVehicleDetailsAPI(vehicleId: Int,notificationType: [Int]) {
        let auctionListViewM:DAuctionListViewM = DAuctionListViewM()
        auctionListViewM.requestNotifyAPI(vehicleId: vehicleId, notificationType: notificationType, completion: { [weak self] (status) in
            guard self != nil else { return }
        })
    }
    
    func requestAuctionAPI(vehicleId: Int,vehicle:Vehicle) {
        
        let auctionListViewM:DAuctionListViewM = DAuctionListViewM()
        auctionListViewM.requestEnterAuctionDetailsAPI(vehicleId: vehicleId) { (responseData) in
            Threads.performTaskInMainQueue {
                let liveAuctionRoomVC = DIConfigurator.sharedInstance.getDLiveAuctionRoomVC()
                liveAuctionRoomVC.enterAuctionRoomModel = responseData
                liveAuctionRoomVC.vehicle = vehicle
                self.navigationController?.pushViewController(liveAuctionRoomVC, animated: true)
            }
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func tapLiveButton(_ sender: UIButton) {
        self.didTapButton(sender)
    }
    
    @IBAction func tapUpcomingButton(_ sender: UIButton) {
        self.didTapButton(sender)
    }
}
