//
//  AuctionRoomViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/9/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DAuctionRoomVModeling: BaseVModeling {
    func requestAuctionCarListAPI(params: APIParams, completion: @escaping (Int, Int, VehicleList) -> Void)
}

class DAuctionRoomViewM: BaseViewM, DAuctionRoomVModeling {
    
    func requestAuctionCarListAPI(params: APIParams, completion: @escaping (Int, Int, VehicleList) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .auctionCarList(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let vehicleData = result.toJSONData(),
                    let vehicleList = VehicleList(jsonData: vehicleData) {
                    completion(nextPageNumber, perPage, vehicleList)
                }
            }
        }
    }
}
