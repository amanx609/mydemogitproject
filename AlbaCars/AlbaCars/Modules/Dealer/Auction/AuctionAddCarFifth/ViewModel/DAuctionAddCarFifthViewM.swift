//
//  DAuctionAddCarFifthViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/28/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DAuctionAddCarFifthViewModeling {
    func getAddCarFormDataSource(interiorInspection: InteriorInspection?) -> [CellInfo]
    func validatePostCarData(arrData: [CellInfo]) -> Bool
    func requestAuctionAPI(arrData: [CellInfo],auctionId:Int, completion: @escaping (DAuctionModel)-> Void)
}

class DAuctionAddCarFifthViewM: DAuctionAddCarFifthViewModeling {
    
    func getAddCarFormDataSource(interiorInspection: InteriorInspection?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        // Seats Cell
        var seatsInfo = [String: AnyObject]()
        seatsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "seats")
        let seatsCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Seats".localizedString(), value:Helper.getDefaultString(object: interiorInspection?.seatsInspection, defaultString: "Perfect".localizedString()) ,info: seatsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(seatsCell)
        
        // Dashboard Cell
        var dashboardInfo = [String: AnyObject]()
        dashboardInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "dashboard")
        let dashboardCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Dashboard".localizedString(), value:Helper.getDefaultString(object: interiorInspection?.dashboard, defaultString: "Perfect".localizedString()) ,info: dashboardInfo, height: Constants.CellHeightConstants.height_80)
        array.append(dashboardCell)
        
        // Roof Cell
        var sunroofInfo = [String: AnyObject]()
        sunroofInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "sunroof")
        let sunroofCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Roof".localizedString(), value:Helper.getDefaultString(object: interiorInspection?.roof, defaultString: "Perfect".localizedString()) ,info: sunroofInfo, height: Constants.CellHeightConstants.height_80)
        array.append(sunroofCell)
        
        // Buttons Cell
        var buttonsInfo = [String: AnyObject]()
        buttonsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "buttons.png")
        let buttonsCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Buttons".localizedString(), value:Helper.getDefaultString(object: interiorInspection?.buttons, defaultString: "Perfect".localizedString()) ,info: buttonsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(buttonsCell)
        
        // Curtains Cell
        var curtainsInfo = [String: AnyObject]()
        curtainsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "curtains")
        let curtainsCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Curtains".localizedString(), value:Helper.getDefaultString(object: interiorInspection?.curtains, defaultString: "Perfect".localizedString()) ,info: curtainsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(curtainsCell)
        
        // Mirrors Cell
        var mirrorsInfo = [String: AnyObject]()
        mirrorsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "mirrors")
        let mirrorsCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Mirrors".localizedString(), value:Helper.getDefaultString(object: interiorInspection?.mirrors, defaultString: "Perfect".localizedString()) ,info: mirrorsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(mirrorsCell)
        
        // Lights Cell
        var lightsInfo = [String: AnyObject]()
        lightsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "lights")
        let lightsCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Lights".localizedString(), value:Helper.getDefaultString(object: interiorInspection?.lights, defaultString: "Perfect".localizedString()) ,info: lightsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(lightsCell)
                
        // Other Information
        var otherInfo = [String: AnyObject]()
        otherInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "other_info")
        otherInfo[Constants.UIKeys.isHide] = true as AnyObject
        let otherCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Other Information".localizedString(), value:"" ,info: otherInfo, height: Constants.CellHeightConstants.height_80)
        array.append(otherCell)
        
        //TextView Cell
        let textViewCell = CellInfo(cellType: .TextViewCell, placeHolder: "Write Something...".localizedString(), value: Helper.toString(object: interiorInspection?.interiorInspectionInfo), info: nil, height: UITableView.automaticDimension)
        array.append(textViewCell)
        
        //Next Cell
        let submitCell = CellInfo(cellType: .BottomButtonCell, placeHolder: "Next".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_100)
        array.append(submitCell)
        
        return array
    }
    
    //MARK: - API Methods
    func requestAuctionAPI(arrData: [CellInfo],auctionId:Int, completion: @escaping (DAuctionModel)-> Void) {
      if self.validatePostCarData(arrData: arrData) {
        let params = self.getPostAddCarParams(arrData: arrData,auctionId:auctionId)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .postAuctionCar(param: params))) { (response, success) in
          if success {
              if let safeResponse =  response as? [String: AnyObject],
                  let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                  let auctionData = result.toJSONData() {
                  do {
                      let decoder = JSONDecoder()
                      let auctionModelData = try decoder.decode(DAuctionModel.self, from: auctionData)
                      completion(auctionModelData)
                  } catch let error {
                      print(error)
                  }
              }
          }
        }
      }
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
     func validatePostCarData(arrData: [CellInfo]) -> Bool {
        var isValid = true
        var message = ""
        if let seats = arrData[0].value, seats.isEmpty {
            message = "Please select seats."
            isValid = false
        } else if let dashboard = arrData[1].value, dashboard.isEmpty  {
            message = "Please select dashboard."
            isValid = false
        } else if let roof = arrData[2].value, roof.isEmpty {
            message = "Please select roof."
            isValid = false
        }  else if let buttons = arrData[3].value, buttons.isEmpty {
            message = "Please enter buttons."
            isValid = false
        } else if let curtains = arrData[4].value,curtains.isEmpty {
            message = "Please select curtains"
            isValid = false
        } else if let mirrors = arrData[5].value, mirrors.isEmpty {
            message = "Please enter mirrors."
            isValid = false
        } else if let lights = arrData[6].value, lights.isEmpty {
            message = "Please select lights"
            isValid = false
        }
       
        if !isValid {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: message.localizedString())
        }
        return isValid
    }
    
    func getPostAddCarParams(arrData: [CellInfo],auctionId:Int)-> APIParams {
           var params: APIParams = APIParams()
           params[ConstantAPIKeys.id] = auctionId as AnyObject
           params[ConstantAPIKeys.auctionPage] = ConstantAPIKeys.interiorInspection as AnyObject
           params[ConstantAPIKeys.seatsInspection] = arrData[0].value as AnyObject
           params[ConstantAPIKeys.dashboard] = arrData[1].value as AnyObject
           params[ConstantAPIKeys.roof] = arrData[2].value as AnyObject
           params[ConstantAPIKeys.buttons] = arrData[3].value as AnyObject
           params[ConstantAPIKeys.curtains] = arrData[4].value as AnyObject
           params[ConstantAPIKeys.mirrors] = arrData[5].value as AnyObject
           params[ConstantAPIKeys.lights] = arrData[6].value as AnyObject
           params[ConstantAPIKeys.interiorInspectionInfo] = arrData[8].value as AnyObject
           return params
       }
}


