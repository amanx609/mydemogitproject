//
//  DAuctionConfirmationViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/28/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DAuctionConfirmationViewModeling: BaseVModeling {
    func getAuctionConfirmationDataSource(auctionDetails: AuctionDetails?) -> [CellInfo]
    func validateAuctionConfirmationData(arrData: [CellInfo]) -> Bool
    func requestAuctionAPI(arrData: [CellInfo],auctionId:Int,isDraft: Int,chooseAuctionType: ChooseAuctionType, completion: @escaping (DAuctionModel)-> Void)
}

class DAuctionConfirmationViewM: DAuctionConfirmationViewModeling {
    
    func getAuctionConfirmationDataSource(auctionDetails: AuctionDetails?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //DateRange Cell
        var dateRangeInfo = [String: AnyObject]()
        dateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date:".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time:".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let dateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: dateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(dateRangeCell)
        
        // Auction Duration Cell
        var durationInfo = [String: AnyObject]()
        durationInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "time-expire")
        let durationCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Auction Duration".localizedString(), value:Helper.toString(object: auctionDetails?.duration == 0 ? "" :auctionDetails?.duration) ,info: durationInfo, height: Constants.CellHeightConstants.height_80)
        array.append(durationCell)
        
        // NoReserve Cell
        var noReserveInfo = [String: AnyObject]()
        noReserveInfo[Constants.UIKeys.isSelected] = Helper.toBool(auctionDetails?.isReserved) as AnyObject
        let reserveCell = CellInfo(cellType: .NoReserveCell, placeHolder: "No Reserve".localizedString(), value:"" ,info: noReserveInfo
            , height: Constants.CellHeightConstants.height_50)
        array.append(reserveCell)
        
        // No Reserve Price Cell
        var noReservePriceInfo = [String: AnyObject]()
        noReservePriceInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "no_reserve_price")
        noReservePriceInfo[Constants.UIKeys.isHide] = true as AnyObject
        let noReservePriceCell = CellInfo(cellType: .DAuctionAddCarInputCell, placeHolder: "No Reserve Price".localizedString(), value:Helper.toString(object: auctionDetails?.price == 0 ? "" : auctionDetails?.price) ,info: noReservePriceInfo, height: Constants.CellHeightConstants.height_80, keyboardType: .numberPad)
        array.append(noReservePriceCell)
        
        //Terms Conditions Cell
        let termsConditionsCell = CellInfo(cellType: .TermsConditionsCell, placeHolder: "I Accept Terms & Conditions".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(termsConditionsCell)
        return array
    }
    
    //MARK: - API Methods
    func requestAuctionAPI(arrData: [CellInfo],auctionId:Int,isDraft: Int,chooseAuctionType: ChooseAuctionType, completion: @escaping (DAuctionModel)-> Void) {
      if self.validateAuctionConfirmationData(arrData: arrData) {
        let params = self.getAuctionConfirmationParams(arrData: arrData,auctionId:auctionId, isDraft: isDraft, chooseAuctionType: chooseAuctionType)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .postAuctionCar(param: params))) { (response, success) in
          if success {
              if let safeResponse =  response as? [String: AnyObject],
                  let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                  let auctionData = result.toJSONData() {
                  do {
                      let decoder = JSONDecoder()
                      let auctionModelData = try decoder.decode(DAuctionModel.self, from: auctionData)
                      completion(auctionModelData)
                  } catch let error {
                      print(error)
                  }
              }
          }
        }
      }
    }
    
    func validateAuctionConfirmationData(arrData: [CellInfo]) -> Bool {
      var isValid = true
      if let date = arrData[0].placeHolder, date.isEmpty {
        Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date for auction.".localizedString())
        isValid = false
      } else if let time = arrData[0].value, time.isEmpty {
        Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time for auction.".localizedString())
        isValid = false
      } else if let duration = arrData[1].value, duration.isEmpty {
        Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select duration for auction.".localizedString())
        isValid = false
      } else if let isSelected = arrData[2].info?[Constants.UIKeys.isSelected] as? Bool, isSelected == true, let noReservePrice = arrData[3].value, noReservePrice.isEmpty {
        Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter price for auction.".localizedString())
        isValid = false
      } else if let isChecked = arrData[4].value, isChecked == "False"  {
           Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please accept terms and conditions.".localizedString())
           isValid = false
         }

      return isValid
    }
    
    func getAuctionConfirmationParams(arrData: [CellInfo],auctionId:Int,isDraft: Int,chooseAuctionType: ChooseAuctionType) -> APIParams {
        var params: APIParams = APIParams()
        
        switch chooseAuctionType {
        case .auctionDay:
            params[ConstantAPIKeys.auctionDay] = "1" as AnyObject
            params[ConstantAPIKeys.isFeature] = "0" as AnyObject
        case .featured:
            params[ConstantAPIKeys.auctionDay] = "0" as AnyObject
            params[ConstantAPIKeys.isFeature] = "1" as AnyObject
        case .normal:
            params[ConstantAPIKeys.auctionDay] = "0" as AnyObject
            params[ConstantAPIKeys.isFeature] = "0" as AnyObject
        default:
            print("")
        }
        
        let dateStr = arrData[0].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.apiDateFormat)
        params[ConstantAPIKeys.date] = dateStr as AnyObject // for date
        params[ConstantAPIKeys.time] = arrData[0].info?[Constants.UIKeys.secondValue] as AnyObject // for time
        params[ConstantAPIKeys.duration] = Helper.parse(from: arrData[1].value) as AnyObject
        params[ConstantAPIKeys.isReserved] = Helper.toInt(arrData[2].info?[Constants.UIKeys.isSelected]) as AnyObject // for no reserve
        params[ConstantAPIKeys.price] = Helper.toInt(arrData[3].value) as AnyObject // no reserve price
        params[ConstantAPIKeys.isDraft] = isDraft as AnyObject // no reserve price
        params[ConstantAPIKeys.auctionPage] = ConstantAPIKeys.auctionDetails as AnyObject
        params[ConstantAPIKeys.id] = auctionId as AnyObject
        params[ConstantAPIKeys.isAuction] = isDraft == 0 ? "1" as AnyObject : "0" as AnyObject

        return params
    }
    
}

