//
//  DAuctionConfirmationViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/28/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension DAuctionConfirmationViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.auctionConfirmationDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.auctionConfirmationDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.auctionConfirmationDataSource[indexPath.row].height
    }
}

extension DAuctionConfirmationViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .DateRangeCell:
            let cell: DateRangeCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureAuctionConfirmationViewCell(cellInfo: cellInfo)
            
            switch chooseAuctionType {
            case .auctionDay,.featured:
                cell.isUserInteractionEnabled = false
            default:
                print("")
            }
            
            return cell
        case .DAuctionAddCarInputCell:
            let cell: DAuctionAddCarInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            let font = FontFamily.AirbnbCerealApp.fontName(wieight: .Book)
            cell.titleLabel.font = UIFont(name: font, size: 14.0)!
            self.setupToolBar(cell.inputTextField)
            return cell
        case .DAuctionAddCarDropDownCell:
            let cell: DAuctionAddCarDropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            cell.placeholderImageView.contentMode = .center
            let font = FontFamily.AirbnbCerealApp.fontName(wieight: .Book)
            cell.titleLabel.font = UIFont(name: font, size: 14.0)!
            return cell
        case .NoReserveCell:
            let cell: NoReserveCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            //cell.configureView(cellInfo: cellInfo)
            return cell
        case .TermsConditionsCell:
            let cell: TermsConditionsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            // cell.delegate = self
            cell.configureViewAuctionCell(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - DropDownCellDelegate
extension DAuctionConfirmationViewC: DateRangeCellDelegate {
    func didTapFromDate(cell: DateRangeCell) {
        self.isDate = true
        self.setupDateTimePicker(cell.fromTextField)
    }
    
    func didTapToDate(cell: DateRangeCell) {
        self.isDate = false
        self.setupDateTimePicker(cell.toTextField)
    }
    
}

//MARK: - DAuctionAddCarDropDownCellDelegate
extension DAuctionConfirmationViewC: DAuctionAddCarDropDownCellDelegate {
    func didTapDAuctionAddCarDropDownCell(cell: DAuctionAddCarDropDownCell) {
        self.view.endEditing(true)
        guard let indexPath = self.auctionConfirmationTableView.indexPath(for: cell) else { return }
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: "Select Time Duration", arrayList: Constants.timeDurationList, key: Constants.UIKeys.duration) { [weak self] (response) in
            guard let sSelf = self,
                let timeDuration = response[ConstantAPIKeys.duration] as? String else { return }
            
            sSelf.auctionConfirmationDataSource[indexPath.row].value = timeDuration
            sSelf.auctionConfirmationTableView.reloadData()
            
        }
        listPopupView.showWithAnimated(animated: true)
    }
}

//MARK: - NoReserveCellCellDelegate
extension DAuctionConfirmationViewC: NoReserveCellCellDelegate {
    func didTapNoReserve(cell: NoReserveCell, isReserved: Bool) {
        self.view.endEditing(true)
        guard let indexPath = self.auctionConfirmationTableView.indexPath(for: cell) else { return }
        self.auctionConfirmationDataSource[indexPath.row].info?[Constants.UIKeys.isSelected] = isReserved as AnyObject
        self.auctionConfirmationDataSource[indexPath.row + 1].info?[Constants.UIKeys.isHide] = !isReserved as AnyObject
        self.auctionConfirmationTableView.reloadData()
    }
}

//MARK: - DAuctionAddCarInputCellDelegate
extension DAuctionConfirmationViewC: DAuctionAddCarInputCellDelegate {
    func tapNextKeyboard(cell: DAuctionAddCarInputCell) {
        self.view.endEditing(true)
    }
    
    func didChangeText(cell: DAuctionAddCarInputCell, text: String) {
        guard let indexPath = self.auctionConfirmationTableView.indexPath(for: cell) else { return }
        self.auctionConfirmationDataSource[indexPath.row].value = text
    }
}

//MARK: - TermsConditionCell
extension DAuctionConfirmationViewC: TermsConditionsCellDelegate {
    
    func didTapTermsConditions(cell: TermsConditionsCell) {
        
    }
    
    func didTapCheckBox(cell: TermsConditionsCell, isChecked: Bool) {
        guard let indexPath = self.auctionConfirmationTableView.indexPath(for: cell) else { return }
        self.auctionConfirmationDataSource[indexPath.row].value = "\(isChecked)"
        
        
    }
    
}
