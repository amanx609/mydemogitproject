//
//  DAuctionConfirmationViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/28/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

enum ChooseAuctionType {
    case normal
    case auctionDay
    case featured
}

class DAuctionConfirmationViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var auctionConfirmationTableView: UITableView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var addCartoAuctionButton: UIButton!
    @IBOutlet var headerView: UIView!
    @IBOutlet var dateTimePicker: UIDatePicker!
    
    //MARK: - Properties
    var viewModel: DAuctionConfirmationViewModeling?
    var auctionConfirmationDataSource: [CellInfo] = []
    var timeDurationDataSource = [[String: AnyObject]]()
    var isDate = true
    var isTermsConditionAccepted = false
    var auctionModel = DAuctionModel()
    var chooseAuctionType: ChooseAuctionType = .normal
    var auctionOfday:AuctionOfday?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Add Auction Details".localizedString(),barColor: .greenTopHeaderColor,titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.loadDataSource()
    }
    
    //MARK: - Private Methods
    private func setup() {
        setupViewModel()
        registerNibs()
        setupTableView()
        self.setupView()
    }
    
    private func setupTableView() {
        self.auctionConfirmationTableView.separatorStyle = .none
        self.auctionConfirmationTableView.backgroundColor = UIColor.white
        self.auctionConfirmationTableView.delegate = self
        self.auctionConfirmationTableView.dataSource = self
        self.auctionConfirmationTableView.tableHeaderView = self.headerView
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.addCartoAuctionButton.setTitle("Add Car to Auction".localizedString(), for: .normal)
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = DAuctionConfirmationViewM()
        }
    }
    
    private func registerNibs() {
        self.auctionConfirmationTableView.register(DateRangeCell.self)
        self.auctionConfirmationTableView.register(DAuctionAddCarInputCell.self)
        self.auctionConfirmationTableView.register(DAuctionAddCarDropDownCell.self)
        self.auctionConfirmationTableView.register(NoReserveCell.self)
        self.auctionConfirmationTableView.register(TermsConditionsCell.self)        
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getAuctionConfirmationDataSource(auctionDetails: self.auctionModel.auctionDetails) {
            self.auctionConfirmationDataSource = dataSource
            
            switch chooseAuctionType {
            case .auctionDay,.featured:
                let currentDate = Helper.getCurrentDate(inFormat: Constants.Format.dateFormatWithoutSpace)
                self.auctionConfirmationDataSource[0].placeHolder = currentDate
                
                let timeStr = Helper.toString(object: auctionOfday?.time).getDateStringFrom(inputFormat: Constants.Format.timeFormat24hhmmss, outputFormat: Constants.Format.timeFormathmma)
                self.auctionConfirmationDataSource[0].value = timeStr
                
                let apiTimeFormat = Helper.toString(object: auctionOfday?.time).getDateStringFrom(inputFormat: Constants.Format.timeFormat24hhmmss, outputFormat: Constants.Format.apiTimeFormat)
                self.auctionConfirmationDataSource[0].info?[Constants.UIKeys.secondValue] = apiTimeFormat as AnyObject
                
            default:
                print("")
            }
            
            Threads.performTaskInMainQueue {
                self.auctionConfirmationTableView.reloadData()
            }
        }
    }
    
    private func updateDateTime(_ pickerValue: String) {
        if isDate {
            self.auctionConfirmationDataSource[0].placeHolder = pickerValue
        } else {
            self.auctionConfirmationDataSource[0].value = pickerValue
        }
        self.auctionConfirmationTableView.reloadData()
    }
    
    //MARK: - Public Methods
    func setupDateTimePicker(_ textField: UITextField) {
        textField.inputView = self.dateTimePicker
        textField.inputAccessoryView = Helper.getDoubleButtonToolBar(leftButtonTitle: StringConstants.Text.Cancel.localizedString(), rightButtonTitle: StringConstants.Text.done.localizedString(), leftButtonselector: #selector(self.cancelButtonTapped), rightButtonSelector: #selector(self.doneButtonTapped), target: self)
        if isDate {
            self.dateTimePicker.datePickerMode = .date
        } else {
            self.dateTimePicker.datePickerMode = .time
        }
        self.dateTimePicker.minimumDate = Date()
    }
    
    func setupToolBar(_ textField: UITextField) {
        textField.inputAccessoryView = Helper.getSingleButtonToolBar(buttonTitle: StringConstants.Text.done.localizedString(), selector: #selector(self.doneButtonTapped), target: self)
    }
    
    //MARK: - Selectors
    @objc func cancelButtonTapped() {
        self.view.endEditing(true)
    }
    
    @objc func doneButtonTapped() {
        self.view.endEditing(true)
        let selectedDate = self.dateTimePicker.date
        var convertedValue = ""
        if isDate {
            convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatWithoutSpace)
            // self.auctionConfirmationDataSource[0].info?[Constants.UIKeys.firstValue] = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatdMMMyyyy) as AnyObject
        } else {
            convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.timeFormathmma)
            self.auctionConfirmationDataSource[0].info?[Constants.UIKeys.secondValue] = selectedDate.dateStringWith(strFormat: Constants.Format.apiTimeFormat) as AnyObject
        }
        updateDateTime(convertedValue)
    }
    
    //MARK: - API Methods
    func requestAuctionAPI(isDraft: Int) {

        self.viewModel?.requestAuctionAPI(arrData: self.auctionConfirmationDataSource, auctionId: Helper.toInt(self.auctionModel.basicDetails?.id), isDraft: isDraft, chooseAuctionType: self.chooseAuctionType) { (responseData) in
            self.auctionModel = responseData
            if isDraft == 1 {
                if let viewController = self.navigationController?.viewControllers {
                    for controller in viewController {
                        if controller.isKind(of: DAuctionViewC.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }
                Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Saved successfully".localizedString())
                
            } else {
                Threads.performTaskInMainQueue {
                    let auctionCongratulationsVC = DIConfigurator.sharedInstance.getDAuctionCongratulationsVC()
                    auctionCongratulationsVC.auctionModel = self.auctionModel
                    self.navigationController?.pushViewController(auctionCongratulationsVC, animated: true)
                }
            }
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func didTapSaveDraftButton(_ sender: Any) {
        self.requestAuctionAPI(isDraft: 1)
    }
    
    @IBAction func didTapAddCartToAuctionButton(_ sender: Any) {
        self.view.endEditing(true)
        self.requestAuctionAPI(isDraft: 0)
    }
}
