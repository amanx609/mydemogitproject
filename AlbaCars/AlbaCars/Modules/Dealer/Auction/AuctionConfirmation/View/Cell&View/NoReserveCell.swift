//
//  NoReserveCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/30/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol NoReserveCellCellDelegate: class {
    func didTapNoReserve(cell: NoReserveCell, isReserved:Bool)
}

class NoReserveCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var noReserveButton: UIButton!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subHeadingView: UIView!
    
    
    //MARK: - Variables
    weak var delegate: NoReserveCellCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            if let isSelected = info[Constants.UIKeys.isSelected] as? Bool {
                self.noReserveButton.isSelected = isSelected
            } else {
                self.noReserveButton.isSelected = false
            }
            
            if let shouldHide = info[Constants.UIKeys.shouldHide] as? Bool {
                self.subHeadingView.isHidden = shouldHide
            } else {
                self.subHeadingView.isHidden = false
            }
        }
        
        if cellInfo.placeHolder != "" {
            self.headingLabel.text = cellInfo.placeHolder
        }
        
        
    }
    
    //MARK: - IBAction
    
    @IBAction func tapNoReserve(_ sender: Any) {
        self.noReserveButton.isSelected = !self.noReserveButton.isSelected
        
        if let delegate = self.delegate {
            delegate.didTapNoReserve(cell: self, isReserved: self.noReserveButton.isSelected)
        }
        
    }
}
