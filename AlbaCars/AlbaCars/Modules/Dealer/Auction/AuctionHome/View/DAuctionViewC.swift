//
//  DAuctionViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DAuctionViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var auctionButton: UIButton!
    @IBOutlet weak var myAuctionButton: UIButton!
    @IBOutlet weak var joinView: UIView!
    @IBOutlet weak var gradientJoinView: UIView!
    @IBOutlet weak var joinNowButton: UIButton!
    @IBOutlet weak var myAuctionCollectionView: UICollectionView!
    @IBOutlet weak var toRegisterLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    //Auction
    @IBOutlet weak var auctionCollectionView: UICollectionView!
    @IBOutlet weak var auctionView: UIView!
    @IBOutlet weak var auctionCollectionVHeightConstraint: NSLayoutConstraint!
    
    //MyAuction View
    @IBOutlet weak var myAuctionView: UIView!
    @IBOutlet weak var postNewAuctionButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    
    //Auction Rooms
    @IBOutlet weak var auctionRoomsCollectionView: UICollectionView!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var auctionRoomsPlaceholderLabel: UILabel!
    @IBOutlet weak var viewAllButton: UIButton!
    
    //ConstraintOutlets
    @IBOutlet weak var bottomMarkerViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomMarkerViewLeadingConstraint: NSLayoutConstraint!
    
    //MARK: - Variables
    var myAuctionDataSource: [CellInfo] = []
    var auctionRoomsDataSource: [AuctionRoom] = []
    var viewModel: DAuctionVModeling?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Auction".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupView()
        self.recheckVM()
        self.setupCollectionView()
        self.requestGetAuctioneerRequestStatus()
        self.didTapButton(self.auctionButton, isCalledOnViewDidLoad: true)
        self.requestAuctionRoomListAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = DAuctionViewM()
        }
    }
    
    private func setupView() {
        self.gradientJoinView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.viewAllButton.roundCorners(Constants.UIConstants.sizeRadius_3half)
        
        Threads.performTaskInMainQueue {
            self.gradientJoinView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.postNewAuctionButton.setTitle("Post New Auction".localizedString(), for: .normal)
        self.joinNowButton.setTitle("Join Now".localizedString(), for: .normal)
        self.bottomMarkerViewWidthConstraint.constant = Constants.Devices.ScreenWidth/2
        self.auctionButton.setTitle("Auctions".localizedString(), for: .normal)
        self.myAuctionButton.setTitle("My Auction".localizedString(), for: .normal)
        self.viewAllButton.setTitle("View All".localizedString(), for: .normal)

        Threads.performTaskAfterDealy(0.1) {
            let auctionCollectionViewHeight = self.auctionCollectionView.contentSize.height
            self.auctionCollectionVHeightConstraint.constant = auctionCollectionViewHeight
        }
    }
    
    private func setupCollectionView() {
        self.registerNibs()
        
        // My Auction
        self.myAuctionCollectionView.delegate = self
        self.myAuctionCollectionView.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //depending upon direction of collection view
        self.myAuctionCollectionView?.setCollectionViewLayout(layout, animated: true)
        
        // Auction
        self.auctionCollectionView.delegate = self
        self.auctionCollectionView.dataSource = self
        let auctionLayout = UICollectionViewFlowLayout()
        auctionLayout.scrollDirection = .vertical //depending upon direction of collection view
        self.auctionCollectionView?.setCollectionViewLayout(auctionLayout, animated: true)

        // Auction Rooms
        self.auctionRoomsCollectionView.delegate = self
        self.auctionRoomsCollectionView.dataSource = self
        let auctionRoomLayout = UICollectionViewFlowLayout()
        auctionRoomLayout.scrollDirection = .horizontal //depending upon direction of collection view
        self.auctionRoomsCollectionView?.setCollectionViewLayout(auctionRoomLayout, animated: true)
    }
    
    private func registerNibs() {
        self.myAuctionCollectionView.register(HomeCollectionCell.self)
        self.auctionCollectionView.register(HomeCollectionCell.self)
        self.auctionRoomsCollectionView.register(AuctionRoomCollectionCell.self)
        
    }
    
    private func loadAuctionDataSource() {
        
        if let dataSource = self.viewModel?.getAuctionDataSource() {
            self.myAuctionDataSource = dataSource
        }
        
        Threads.performTaskInMainQueue {
            self.auctionCollectionView.reloadData()
        }
    }
    
    private func loadMyAuctionDataSource() {
        
        if let dataSource = self.viewModel?.getMyAuctionDataSource() {
            self.myAuctionDataSource = dataSource
        }
        
        Threads.performTaskInMainQueue {
            self.myAuctionCollectionView.reloadData()
        }
    }
    
    private func didTapButton(_ sender: UIButton, isCalledOnViewDidLoad: Bool = false) {
        var leadingConstraint: CGFloat = 0.0
        switch sender {
        case self.auctionButton:
            //ChangeBottomMarkerConstraint
            leadingConstraint = 0.0
            //ChangeButtonTitleColor
            self.auctionButton.setTitleColor(.white, for: .normal)
            self.myAuctionButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            //ChangeViewToBeShown
            self.myAuctionView.isHidden = true
            self.auctionView.isHidden = false
            self.loadAuctionDataSource()
            //self.auctionButton.isSelected = true
        //self.myAuctionButton.isSelected = false
        case self.myAuctionButton:
            //ChangeBottomMarkerConstraint
            leadingConstraint = Constants.Devices.ScreenWidth/2
            //ChangeButtonTitleColor
            self.auctionButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            self.myAuctionButton.setTitleColor(.white, for: .normal)
            //ChangeViewToBeShown
            self.myAuctionView.isHidden = false
            self.auctionView.isHidden = true
            self.loadMyAuctionDataSource()
            //  self.auctionButton.isSelected = false
        //  self.myAuctionButton.isSelected = true
        default: break
        }
        //self.auctionView.isHidden = true

        //AnimateTheBottomViewMarker
        if isCalledOnViewDidLoad {
            self.bottomMarkerViewLeadingConstraint.constant = leadingConstraint
            self.view.layoutIfNeeded()
        } else {
            //AnimateTheBottomViewMarker
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseOut], animations: {
              self.bottomMarkerViewLeadingConstraint.constant = leadingConstraint
              self.view.layoutIfNeeded()
            }, completion: nil)
        }

        
    }
    
    private func requestPostAuctioneerRequestStatus() {
        
        self.viewModel?.requestPostAuctioneerRequestStatus(completion: { (requestStatus) in
            switch requestStatus {
            case .pending:
                self.hideJoinButton()
            case .accepted:
                self.joinView.isHidden = true
            case .rejected, .requestNotSent:
                self.displayJoinButton()
            }
        })
    }
    
    private func requestGetAuctioneerRequestStatus() {
        
        self.viewModel?.requestGetAuctioneerRequestStatus(completion: { (requestStatus) in
            switch requestStatus {
                
            case .pending:
                self.hideJoinButton()
            case .accepted:
                self.joinView.isHidden = true
            case .rejected, .requestNotSent:
                self.displayJoinButton()
            }
        })
        
    }
    
    private func hideJoinButton() {
        self.joinNowButton.isHidden = true
        self.gradientJoinView.isHidden = true
        self.messageLabel.text = "Request awaiting".localizedString()
        self.toRegisterLabel.isHidden = true
    }
    
    private func displayJoinButton() {
        self.joinNowButton.isHidden = false
        self.gradientJoinView.isHidden = false
        self.messageLabel.text = "You’re not registered as an Auctioneer,\nPlease Visit ALBA CARS or Call at +971 4035610".localizedString()
        self.toRegisterLabel.isHidden = false
    }
    
    private func goToAuctionsOfTheDayScreen() {
        let auctionsListVC = DIConfigurator.sharedInstance.getDAuctionListVC()
        self.navigationController?.pushViewController(auctionsListVC, animated: true)
    }
    
    private func goToFeatureAuctionsScreen() {
        let featuredAuctionsVC = DIConfigurator.sharedInstance.getDFeaturedAuctionsVC()
        self.navigationController?.pushViewController(featuredAuctionsVC, animated: true)
    }
    
    private func goToLiveAuctionScreen(isMyAuction: Bool) {
        let liveAuctionListVC = DIConfigurator.sharedInstance.getDLiveAuctionListVC()
        liveAuctionListVC.isMyAuction = isMyAuction
        self.navigationController?.pushViewController(liveAuctionListVC, animated: true)
    }
    
    private func goToUpcomingAuctionScreen(isMyAuction: Bool) {
        let upcomingAuctionListVC = DIConfigurator.sharedInstance.getDUpcomingAuctionListVC()
        upcomingAuctionListVC.isMyAuction = isMyAuction
        self.navigationController?.pushViewController(upcomingAuctionListVC, animated: true)
    }
    
    private func goToSoldUnsoldCarsScreen(auctionType: AuctionType) {
        let soldUnsoldCarsVC = DIConfigurator.sharedInstance.getDSoldUnsoldCarViewC()
        soldUnsoldCarsVC.auctionType = auctionType
        self.navigationController?.pushViewController(soldUnsoldCarsVC, animated: true)
    }
    
    func goToSelectedScreen(indx: Int) {
        
        switch indx {
        case 0:
            //Live Auctions
            self.goToLiveAuctionScreen(isMyAuction: true)
            break
        case 1:
            //Upcoming Auctions
            self.goToUpcomingAuctionScreen(isMyAuction: true)
            break
        case 2:
            //Unsold Cars
            self.goToSoldUnsoldCarsScreen(auctionType: .unsoldCars)
            break
        case 3:
            //Sold Cars
            self.goToSoldUnsoldCarsScreen(auctionType: .soldCars)
            break
        case 4:
            // Saved AuctionV
            let savedAuctionVC = DIConfigurator.sharedInstance.getDSavedAuctionVC()
            self.navigationController?.pushViewController(savedAuctionVC, animated: true)
            break
        default:
            break
        }
    }
    
    func goToSelectedAuctionScreen(indx: Int) {
        
        switch indx {
        case 0:
            self.goToAuctionsOfTheDayScreen()
            break
        case 1:
            self.goToFeatureAuctionsScreen()
            break
        case 2:
            //Live Auctions
            self.goToLiveAuctionScreen(isMyAuction: false)
            break
        case 3:
            //Upcoming Auctions
            self.goToUpcomingAuctionScreen(isMyAuction: false)
            break
        default:
            break
        }
    }
    
    //MARK: - APIMethods
    func requestAuctionRoomListAPI() {
        self.viewModel?.requestAuctionRoomListAPI(completion: { [weak self] (auctionRoomList) in
            guard let sSelf = self else { return }
            if let auctionRooms = auctionRoomList.auctionRooms {
                sSelf.auctionRoomsDataSource = auctionRooms
                Threads.performTaskInMainQueue {
                    sSelf.auctionRoomsCollectionView.reloadData()
                    if sSelf.auctionRoomsDataSource.count > 0 {
                        sSelf.seperatorView.isHidden = false
                        sSelf.auctionRoomsPlaceholderLabel.isHidden = false
                        sSelf.viewAllButton.isHidden = false
                    }
                }
            }
        })
    }
    
    //MARK: - IBActions
    @IBAction func tapJoinNowButton(_ sender: UIButton) {
        self.requestPostAuctioneerRequestStatus()
    }
    
    @IBAction func tapAuctionButton(_ sender: UIButton) {
        self.didTapButton(sender)
    }
    
    @IBAction func tapMyAuctionButton(_ sender: UIButton) {
        self.didTapButton(sender)
        if let requestStatus = (UserDefaultsManager.sharedInstance.getIntValueFor(key: .auctioneerRequestStatus)), requestStatus != AuctioneerRequestStatus.accepted.rawValue  {
            self.requestGetAuctioneerRequestStatus()
        } else {
            self.displayJoinButton()
        }
    }
    
    @IBAction func tapPostNewAuctionButton(_ sender: UIButton) {
        let auctionAddCarFirstVC = DIConfigurator.sharedInstance.getDAuctionAddCarFirstVC()
        self.navigationController?.pushViewController(auctionAddCarFirstVC, animated: true)
    }
    
    @IBAction func tapViewAllAuctionRooms(_ sender: UIButton) {
        let allAuctionRoomVC = DIConfigurator.sharedInstance.getDAllAuctionRoomVC()
        self.navigationController?.pushViewController(allAuctionRoomVC, animated: true)
    }
}
