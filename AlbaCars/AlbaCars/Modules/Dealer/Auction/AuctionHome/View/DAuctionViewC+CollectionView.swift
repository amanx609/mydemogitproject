//
//  DAuctionViewC+CollectionView.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension DAuctionViewC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.auctionRoomsCollectionView {
            return self.auctionRoomsDataSource.count
        }
        return self.myAuctionDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.auctionRoomsCollectionView {
            return self.getAuctionRoomCell(collectionView: collectionView, indexPath: indexPath)
        }
        let cellInfo = self.myAuctionDataSource[indexPath.row]
        return self.getCell(collectionView: collectionView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.auctionRoomsCollectionView {
            return CGSize(width: Constants.CellHeightConstants.height_150, height:collectionView.bounds.height)
        }
        
        let width = collectionView.bounds.width/2-5
        return CGSize(width: width, height: self.myAuctionDataSource[indexPath.row].height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events        
        if collectionView == self.myAuctionCollectionView {
            self.goToSelectedScreen(indx: indexPath.row)
        } else if collectionView == self.auctionCollectionView {
            self.goToSelectedAuctionScreen(indx: indexPath.row)
        }
    }
}

extension DAuctionViewC {
    func getCell(collectionView: UICollectionView, indexPath: IndexPath, cellInfo: CellInfo) -> UICollectionViewCell {
        guard let cellType = cellInfo.cellType else { return UICollectionViewCell() }
        switch cellType {
        case .HomeCollectionCell:
            let cell: HomeCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            // cell.homeInfoView.placeholderImageViewWidth.constant = Constants.CellHeightConstants.height_56
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func getAuctionRoomCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell: AuctionRoomCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        let auctionRoom = self.auctionRoomsDataSource[indexPath.row]
        cell.delegate = self
        cell.configureView(auctionRoom: auctionRoom)
        return cell
    }
}

//MARK: - AuctionRoomCollectionCellDelegate
extension DAuctionViewC: AuctionRoomCollectionCellDelegate {
    func didTapViewAuctionRoom(cell: AuctionRoomCollectionCell) {
        guard let indexPath = self.auctionRoomsCollectionView.indexPath(for: cell) else { return }
        let auctionRoom = self.auctionRoomsDataSource[indexPath.row]
        let auctionRoomsVC = DIConfigurator.sharedInstance.getDAuctionRoomVC()
        auctionRoomsVC.auctionRoom = auctionRoom
        self.navigationController?.pushViewController(auctionRoomsVC, animated: true)
    }
}
