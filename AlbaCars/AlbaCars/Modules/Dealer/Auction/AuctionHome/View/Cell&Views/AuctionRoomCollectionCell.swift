//
//  AuctionRoomCollectionCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/7/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol AuctionRoomCollectionCellDelegate: class, BaseProtocol {
    func didTapViewAuctionRoom(cell: AuctionRoomCollectionCell)
}

class AuctionRoomCollectionCell:  BaseCollectionViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var sellingRatioProgressView: UIProgressView!
    @IBOutlet weak var placeholderImageView: UIImageView!
    @IBOutlet weak var liveLabel: UILabel!
    @IBOutlet weak var carsAddedLabel: UILabel!
    @IBOutlet weak var carsSoldLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var viewAuctionRoomButton: UIButton!
    @IBOutlet weak var firstRatingButton: UIButton!
    @IBOutlet weak var secondRatingButton: UIButton!
    @IBOutlet weak var thirdRatingButton: UIButton!
    @IBOutlet weak var fourthRatingButton: UIButton!
    @IBOutlet weak var fifthRatingButton: UIButton!
    
    //MARK: - Variables
    weak var delegate: AuctionRoomCollectionCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    //MARK: - Private Methods
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.bgView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
            self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
            self.liveLabel.roundCorners(Constants.UIConstants.sizeRadius_6)
            self.gradientView.roundCorners(Constants.UIConstants.sizeRadius_3half)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
    }
    
    //MARK: - Public Methods
    func configureView(auctionRoom: AuctionRoom) {
        //LiveCars
        if let liveCars = auctionRoom.liveCars {
            self.countLabel.text = "\(liveCars)"
        }
        //DealerInfo
        if let image = auctionRoom.image {
            self.placeholderImageView.setImage(urlStr: image, placeHolderImage: nil)
        }
        self.titleLabel.text = auctionRoom.name
        //CarsInfo
        if let totalCars = auctionRoom.totalCars,
           let soldCars = auctionRoom.soldCars {
            self.carsAddedLabel.text = "\(totalCars)"
            self.carsSoldLabel.text = "\(soldCars)"
            if totalCars > 0, soldCars > 0 {
                let sellingRatio = soldCars/totalCars
                self.sellingRatioProgressView.progress = Float(sellingRatio)
                self.percentageLabel.text = "\(sellingRatio)%"
            } else {
                self.sellingRatioProgressView.progress = 0
                self.percentageLabel.text = "\(0)%"
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func tapViewAuctionRoom(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapViewAuctionRoom(cell: self)
        }
    }
}
