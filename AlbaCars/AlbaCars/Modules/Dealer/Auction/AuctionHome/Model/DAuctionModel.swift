//
//  DAuctionModel.swift
//  AlbaCars
//
//  Created by Narendra on 1/2/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//
import Foundation
import UIKit

class DAuctionModel: BaseCodable {
    
    //Variables
    var basicDetails: BasicDetails?
    var historyDetails: HistoryDetails?
    var options: Options?
    var mechincalInspection: MechincalInspection?
    var interiorInspection: InteriorInspection?
    var bodyInspection: BodyInspection?
    var carImages: CarImages?
    var auctionDetails: AuctionDetails?
    var auctionPageId: Int?
    
//    func getProgress() -> Float {
//        let number = Float(Helper.toInt(auctionPageId))
//        let progress:Float = (number)/8.0
//        return progress
//    }
//    
//    func getPercentage() -> String {
//        let progress = Helper.toInt(auctionPageId)*100/8
//        return Helper.toString(object:progress) + "%"
//    }
}

//basicDetails
class BasicDetails: BaseCodable {
    
    //Variables
    var id: Int?
    var brand: Int?
    var model: Int?
    var year: Int?
    var submodel: Int?
    var trim: String?
    var mileage: Int?
    var cylinders: String?
    var engine: Double?
    var specs: String?
    var extColor: String?
    var innerColor: String?
    var transmission: String?
    var fuelType: String?
    var noOfDoors: String?
    var drive: String?
    var brandName: String?
    var modelName: String?
    var subModelName: String?
    
    func carTitle() -> String {
        return Helper.toString(object: brandName) + " " + Helper.toString(object: modelName) + " " + Helper.toString(object: subModelName)
    }
    
    //  brand, model, year, submodel, trim, plateNumber, mileage, cylinders, engine, specs, extColor, innerColor, transmission, fuelType, noOfDoors, drive
}

//historyDetails
class HistoryDetails: BaseCodable {
    
    //Variables
    var id: Int?
    var service: String?
    var accident: String?
    var chasisDamage: String?
    var carHistoryInfo: String?
    // service, accident, chasisDamage, carHistoryInfo
}

//options
class Options: BaseCodable {
    
    //Variables
    var id: Int?
    var seats: String?
    var sunroof: String?
    var navigation: String?
    var camera: String?
    var wheels: String?
    var cruiseControl: String?
    var windows: String?
//  seats, sunroof, navigation, camera, wheels, cruiseControl, windows

}

//mechincalInspection
class MechincalInspection: BaseCodable {
    
    //Variables
    var id: Int?
    var engineInspection: String?
    var engineInspectionInfo: String?
    var gear: String?
    var suspension: String?
    var ac: String?
    var doors: String?
    var windowsInspection: String?
    var audioSystem: String?
    var features: String?
    var tyreCondition: String?
    var mechincalInspectionInfo: String?
    
    func getAllTyreConditionData() -> [String:AnyObject]? {
        return tyreCondition?.toDictionary()
    }
    
    func getTyreCondition(key:String) -> TyreCondition? {
        if let tyreCondition =  self.getAllTyreConditionData(),
            let result = tyreCondition[key] as? [String: AnyObject],
            let auctionData = result.toJSONData() {
            do {
                let decoder = JSONDecoder()
                let condition = try decoder.decode(TyreCondition.self, from: auctionData)
                return condition
            } catch let error {
                print(error)
            }
        }
        return nil
    }
    
   // engineInspection, engineInspectionInfo, gear, suspension, ac, doors, windowsInspection, audioSystem, features, tyreCondition, mechincalInspectionInfo
}

//TyreCondition
class TyreCondition: BaseCodable {
    var week: String?
    var year: String?
}

//interiorInspection
class InteriorInspection: BaseCodable {
    
    //Variables
    var id: Int?
    var seatsInspection: String?
    var dashboard: String?
    var roof: String?
    var buttons: String?
    var curtains: String?
    var mirrors: String?
    var lights: String?
    var interiorInspectionInfo: String?
    //  seatsInspection, dashboard, roof, buttons, curtains, mirrors, lights, interiorInspectionInfo

}

//CarImages
class CarImages: BaseCodable {
    
    //Variables
    var id: Int?
    var images: [Images]?
}

//Images
class Images: BaseCodable {
    
    //Variables
   var image: String?
}

//AuctionDetails
class AuctionDetails: BaseCodable {
    
    //Variables
    var id: Int?
    var date: String?
    var duration: Int?
    var isDraft: Int?
    var isReserved: Int?
    var price: Double?
    var time: String?
    var currentDate: String?
    var endTime: String?
    var startTime: String?
    
    func reservedPrice() -> String {
        return "AED".localizedString() + " " + Helper.toString(object: price)
    }
    
    func getTimerDuration() -> Double {
        
        if let strCurrentDate = currentDate,
            let strEndDate = self.endTime,
            let currentDate = strCurrentDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss"),
            let endDate = strEndDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss") {
            return endDate.timeIntervalSince(currentDate)
        }
        
        return 0
    }
    
    func getTotalTime() -> Double {
        
        if let strStartTimeDate = self.startTime,
            let strEndDate = self.endTime,
            let currentDate = strStartTimeDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss"),
            let endDate = strEndDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss") {
            return endDate.timeIntervalSince(currentDate)
        }
        
        return 0
    }
   // 

}

//BodyInspection
class BodyInspection: BaseCodable {
    
    //Variables
    var id: Int?
    var frontBumper: String?
    var rearBumper: String?
    var frontRightDoors: String?
    var frontLeftDoors: String?
    var rearRightDoors: String?
    var rearLeftDoors: String?
    var frontRightFenders: String?
    var frontLeftFenders: String?
    var rearRightFenders: String?
    var rearLeftFenders: String?
    var rightSideskirt: String?
    var leftSideskirt: String?
    var bonnet: String?
    var boot: String?
    var roofInspection: String?
    var frontRightWheels: String?
    var frontLeftWheels: String?
    var rearRightWheels: String?
    var rearLeftWheels: String?
    var rearGlass: String?
    var frontGlass: String?
    var frontRightGlass: String?
    var frontLeftGlass: String?
    var rearRightGlass: String?
    var rearLeftGlass: String?
    var rearRightCornerGlass: String?
    var rearLeftCornerGlass: String?
    var sidemirrors: String?
    
}
