//
//  DAuctionCongratulationsViewM.swift
//  AlbaCars
//
//  Created by Narendra on 1/6/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DAuctionCongratulationsViewModeling {
    func requestCancelAuctionAPI(auctionId:Int, completion: @escaping (Bool)-> Void)
}

class DAuctionCongratulationsViewM: DAuctionCongratulationsViewModeling {
   
    //MARK: - API Methods
    func requestCancelAuctionAPI(auctionId: Int, completion: @escaping (Bool) -> Void) {
        let params = self.getPostAddCarParams(auctionId:auctionId)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .cancelPostRequest(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]{
                    completion(success)
                }
            }
        }
    }
    
    func getPostAddCarParams(auctionId:Int)-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.id] = auctionId as AnyObject
        return params
    }
}


