//
//  DAuctionViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/27/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import Foundation
import UIKit

protocol DAuctionVModeling: BaseVModeling {
    func getMyAuctionDataSource() -> [CellInfo]
    func getAuctionDataSource() -> [CellInfo]
    func getAuctionRoomsDataSource() -> [CellInfo]
    func requestPostAuctioneerRequestStatus(completion: @escaping (AuctioneerRequestStatus)-> Void)
    func requestGetAuctioneerRequestStatus(completion: @escaping (AuctioneerRequestStatus)-> Void)
    func requestAuctionRoomListAPI(completion: @escaping (AuctionRoomList) -> Void)
}

class DAuctionViewM: DAuctionVModeling {
    
    func getMyAuctionDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Live Auctions Cell
        var auctionsInfo = [String: AnyObject]()
        auctionsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "live_auctions")
        let auctionsCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Live Auctions".localizedString(), value: "", info: auctionsInfo, height: Constants.CellHeightConstants.height_120)
        array.append(auctionsCell)
        
        //Upcoming Acutions Cell
        var upcomingInfo = [String: AnyObject]()
        upcomingInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "ongoingBids")
        let upcomingCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Upcoming Auctions".localizedString(), value: "", info: upcomingInfo, height: Constants.CellHeightConstants.height_120)
        array.append(upcomingCell)
        
        //Unsold Cars Cell
        var unsoldCarInfo = [String: AnyObject]()
        unsoldCarInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "rejectedBids")
        let unsoldCarCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Unsold Cars".localizedString(), value: "", info: unsoldCarInfo, height: Constants.CellHeightConstants.height_120)
        array.append(unsoldCarCell)
        
        //Sold Cars Cell
        var soldCarInfo = [String: AnyObject]()
        soldCarInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "sold_car")
        let soldCarCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Sold Cars".localizedString(), value: "", info: soldCarInfo, height: Constants.CellHeightConstants.height_120)
        array.append(soldCarCell)
        
        //Saved for Later Cell
        var savedInfo = [String: AnyObject]()
        savedInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "saved_later")
        let savedCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Saved for Later".localizedString(), value: "", info: savedInfo, height: Constants.CellHeightConstants.height_120)
        array.append(savedCell)
        
        return array
    }
    
    func getAuctionDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Auction of the Day
        var auctionsInfo = [String: AnyObject]()
        auctionsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "auction_day")
        let auctionsCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Auction of the Day".localizedString(), value: "", info: auctionsInfo, height: Constants.CellHeightConstants.height_120)
        array.append(auctionsCell)
        
        //Featured
        var featuredInfo = [String: AnyObject]()
        featuredInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "featured")
        let featuredCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Featured".localizedString(), value: "", info: featuredInfo, height: Constants.CellHeightConstants.height_120)
        array.append(featuredCell)
        
        //Live Auctions
        var liveInfo = [String: AnyObject]()
        liveInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "live_auctions")
        let liveCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Live Auctions".localizedString(), value: "", info: liveInfo, height: Constants.CellHeightConstants.height_120)
        array.append(liveCell)
        
        //Upcoming Auctions
        var upcomingInfo = [String: AnyObject]()
        upcomingInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "ongoingBids")
        let upcomingCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Upcoming Auctions".localizedString(), value: "", info: upcomingInfo, height: Constants.CellHeightConstants.height_120)
        array.append(upcomingCell)
        
        return array
    }
    
    func getAuctionRoomsDataSource() -> [CellInfo] {
        var array = [CellInfo]()
        
        // Auctions
        var liveInfo = [String: AnyObject]()
        liveInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "logo")
        let liveCell = CellInfo(cellType: .AuctionRoomCollectionCell, placeHolder: "Alba Cars".localizedString(), value: "", info: liveInfo, height: Constants.CellHeightConstants.height_120)
        array.append(liveCell)
        array.append(liveCell)
        array.append(liveCell)
        
        return array
    }
    
    //MARK: - API Methods
    func requestGetAuctioneerRequestStatus(completion: @escaping (AuctioneerRequestStatus) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getAuctioneerRequestStatus)) { (response, success) in
            if let safeResponse = response as? [String: AnyObject],
                let result = safeResponse[kResult] as? [String: AnyObject],
                let requestStatus = result[kSuccess] as? Int, let reqStatusValue = AuctioneerRequestStatus(rawValue: requestStatus) {
                UserDefaultsManager.sharedInstance.saveIntValueFor(key: .auctioneerRequestStatus, value: requestStatus)
                completion(reqStatusValue)
            }
        }
    }
    
    func requestPostAuctioneerRequestStatus(completion: @escaping (AuctioneerRequestStatus) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .postAuctioneerRequestStatus)) { (response, success) in
            if let safeResponse = response as? [String: AnyObject],
                let result = safeResponse[kResult] as? [String: AnyObject],
                let requestStatus = result[kSuccess] as? Int, let reqStatusValue = AuctioneerRequestStatus(rawValue: requestStatus) {
                UserDefaultsManager.sharedInstance.saveIntValueFor(key: .auctioneerRequestStatus, value: requestStatus)
                completion(reqStatusValue)
            }
        }
    }
    
    
    func requestAuctionRoomListAPI(completion: @escaping (AuctionRoomList) -> Void) {
        let auctionRoomListParam = getAPIParams()
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .auctionRoomList(param: auctionRoomListParam))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let auctionRoomData = result.toJSONData(),
                    let auctionRoomList = AuctionRoomList(jsonData: auctionRoomData) {
                        completion(auctionRoomList)
                }
            }
        }
    }
    
    private func getAPIParams() -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.page] = 1 as AnyObject
        return param
    }
}
