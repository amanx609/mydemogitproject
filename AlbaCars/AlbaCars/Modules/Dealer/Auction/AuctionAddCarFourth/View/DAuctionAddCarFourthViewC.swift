//
//  DAuctionAddCarFourthViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/28/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DAuctionAddCarFourthViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var addCarTableView: UITableView!
    
    //MARK: - Properties
    var viewModel: DAuctionAddCarFourthViewModeling?
    var addCarDataSource: [CellInfo] = []
    var auctionModel = DAuctionModel()

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Add Car Details".localizedString(),barColor: .redButtonColor,titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.loadDataSource()
    }
    
    //MARK: - Private Methods
    private func setup() {
        setupViewModel()
        registerNibs()
        setupTableView()
    }
    
    private func setupTableView() {
        self.addCarTableView.separatorStyle = .none
        self.addCarTableView.backgroundColor = UIColor.white
        self.addCarTableView.delegate = self
        self.addCarTableView.dataSource = self
        self.addCarTableView.allowsSelection =  false
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = DAuctionAddCarFourthViewM()
        }
    }
    
    private func registerNibs() {
        self.addCarTableView.register(DAuctionAddCarInputCell.self)
        self.addCarTableView.register(DAuctionAddCarDropDownCell.self)
        self.addCarTableView.register(TextViewCell.self)
        self.addCarTableView.register(BottomButtonCell.self)
        self.addCarTableView.register(TyresConditionCell.self)
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getAddCarFormDataSource(mechincalInspection: self.auctionModel.mechincalInspection) {
            self.addCarDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.addCarTableView.reloadData()
            }
        }
    }
    
    func showDropDownAt(indexPath: IndexPath, title: String) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: title, arrayList: DropDownType.perfectMinorMajorDamaged.dataList, key: DropDownType.perfectMinorMajorDamaged.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let conditionStatus = response[DropDownType.perfectMinorMajorDamaged.rawValue] as? String else { return }
            sSelf.addCarDataSource[indexPath.row].value = conditionStatus
            sSelf.addCarTableView.reloadData()
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    //MARK: - API Methods
    func requestAuctionAPI() {
        self.viewModel?.requestAuctionAPI(arrData: self.addCarDataSource, auctionId: Helper.toInt(self.auctionModel.mechincalInspection?.id)) { (responseData) in
            self.auctionModel = responseData
            Threads.performTaskInMainQueue {
                let auctionAddCarFifthVC = DIConfigurator.sharedInstance.getDAuctionAddCarFifthVC()
                auctionAddCarFifthVC.auctionModel = self.auctionModel
                self.navigationController?.pushViewController(auctionAddCarFifthVC, animated: true)
            }
        }
    }
}
