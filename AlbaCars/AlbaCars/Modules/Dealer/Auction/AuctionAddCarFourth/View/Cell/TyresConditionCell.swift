//
//  TyresConditionCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/28/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol TyresConditionCellDelegate: class {
    func didTapFirstDropDownCell(cell: TyresConditionCell)
    func didTapSecondDropDownCell(cell: TyresConditionCell)
}

class TyresConditionCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var placeholderFirstLabel: UILabel!
    @IBOutlet weak var placeholderSecondLabel: UILabel!
    @IBOutlet weak var dropDownViewFirst: UIView!
    @IBOutlet weak var dropDownViewSecond: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bgImageView: UIView!
    @IBOutlet weak var placeholderImageView: UIImageView!
    @IBOutlet weak var dotLabel: UILabel!


    //MARK: - Variables
    weak var delegate: TyresConditionCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.dropDownViewFirst.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.dropDownViewSecond.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.titleLabel.text = cellInfo.placeHolder
        self.bgImageView.isHidden = true
        if let info = cellInfo.info {
            
            if let firstValue = info[Constants.UIKeys.firstValue] as? String, !firstValue.isEmpty {
                self.placeholderFirstLabel.text = firstValue
            }
            
            if let secondValue = info[Constants.UIKeys.secondValue] as? String,!secondValue.isEmpty {
                self.placeholderSecondLabel.text = secondValue
            }
        }
        
        self.dotLabel.isHidden = true
    }
    
    //MARK: - Public Methods
       func configureEngineSizeView(cellInfo: CellInfo) {
           self.titleLabel.text = cellInfo.placeHolder
           self.placeholderFirstLabel.text = ""
           self.placeholderSecondLabel.text = ""
           self.dotLabel.isHidden = false

           if let info = cellInfo.info {
               
               if let firstValue = info[Constants.UIKeys.firstValue] as? String{
                   self.placeholderFirstLabel.text = firstValue
               }
               
               if let secondValue = info[Constants.UIKeys.secondValue] as? String {
                   self.placeholderSecondLabel.text = secondValue
               }
               
               if let placeholderImage = info[Constants.UIKeys.placeholderImage] as? UIImage {
                   self.placeholderImageView.image = placeholderImage
                   self.bgImageView.isHidden = false
               } else {
                   self.bgImageView.isHidden = true
               }
           } else {
               self.bgImageView.isHidden = true
           }
       }
    
    //MARK: - IBActions
    
    @IBAction func dropDownFirstButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapFirstDropDownCell(cell: self)
        }
    }
    
    @IBAction func dropDownSecondButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapSecondDropDownCell(cell: self)
        }
    }
}
