//
//  DAuctionAddCarFourthViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/28/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import Foundation
import UIKit

extension DAuctionAddCarFourthViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addCarDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.addCarDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.addCarDataSource[indexPath.row].height
    }
}

extension DAuctionAddCarFourthViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .DAuctionAddCarDropDownCell:
            let cell: DAuctionAddCarDropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .BottomButtonCell:
            let cell: BottomButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .TextViewCell:
            let cell: TextViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate =  self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .TyresConditionCell:
            let cell: TyresConditionCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate =  self
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - DAuctionAddCarDropDownCellDelegate
extension DAuctionAddCarFourthViewC: DAuctionAddCarDropDownCellDelegate {
    func didTapDAuctionAddCarDropDownCell(cell: DAuctionAddCarDropDownCell) {
        guard let indexPath = self.addCarTableView.indexPath(for: cell) else { return }
        let cellInfo = self.addCarDataSource[indexPath.row]
        self.showDropDownAt(indexPath: indexPath, title: cellInfo.placeHolder)
    }
}

//MARK: - BottomButtonCellDelegate
extension DAuctionAddCarFourthViewC: BottomButtonCellDelegate {
    func didTapBottomButton(cell: BottomButtonCell) {
        self.view.endEditing(true)
        requestAuctionAPI()
//        if let isValid = self.viewModel?.validatePostCarData(arrData: self.addCarDataSource),isValid {
//            if let param = self.viewModel?.getPostAddCarParams(arrData: self.addCarDataSource) {
//               let auctionAddCarFifthVC = DIConfigurator.sharedInstance.getDAuctionAddCarFifthVC()
//                self.navigationController?.pushViewController(auctionAddCarFifthVC, animated: true)
//            }
//        }
    }
}

//MARK: - TextViewCellDelegate
extension DAuctionAddCarFourthViewC: TextViewCellDelegate {
    func didChangeText(cell: TextViewCell, text: String) {
        guard let indexPath = self.addCarTableView.indexPath(for: cell) else { return }
        self.addCarDataSource[indexPath.row].value = text
    }
}

//MARK: - TyresConditionCellDelegate
extension DAuctionAddCarFourthViewC: TyresConditionCellDelegate {
    
    func didTapFirstDropDownCell(cell: TyresConditionCell) {
        guard let indexPath = self.addCarTableView.indexPath(for: cell) else { return }
        let cellInfo = self.addCarDataSource[indexPath.row]
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: cellInfo.placeHolder, arrayList: DropDownType.tyresConditionWeek.dataList, key: DropDownType.tyresConditionWeek.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let tyresCondition = response[DropDownType.tyresConditionWeek.rawValue] as? String else { return }
            sSelf.addCarDataSource[indexPath.row].info?[Constants.UIKeys.firstValue] = tyresCondition as AnyObject
            sSelf.addCarTableView.reloadData()
        }
        listPopupView.showWithAnimated(animated: true)
        
    }
    
    func didTapSecondDropDownCell(cell: TyresConditionCell) {
        guard let indexPath = self.addCarTableView.indexPath(for: cell) else { return }
        let cellInfo = self.addCarDataSource[indexPath.row]
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: cellInfo.placeHolder, arrayList: DropDownType.tyresConditionYear.dataList, key: DropDownType.tyresConditionYear.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let tyresCondition = response[DropDownType.tyresConditionYear.rawValue] as? String else { return }
            sSelf.addCarDataSource[indexPath.row].info?[Constants.UIKeys.secondValue] = tyresCondition as AnyObject
            sSelf.addCarTableView.reloadData()
        }
        listPopupView.showWithAnimated(animated: true)
        
    }
    
}

