//
//  DAuctionAddCarFourthViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/28/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DAuctionAddCarFourthViewModeling {
    func getAddCarFormDataSource(mechincalInspection: MechincalInspection?) -> [CellInfo]
    func validatePostCarData(arrData: [CellInfo]) -> Bool
    func requestAuctionAPI(arrData: [CellInfo],auctionId:Int, completion: @escaping (DAuctionModel)-> Void)
}

class DAuctionAddCarFourthViewM: DAuctionAddCarFourthViewModeling {
    
    func getAddCarFormDataSource(mechincalInspection: MechincalInspection?) -> [CellInfo] {
        
        var array = [CellInfo]()
        // Engine Size Cell
        var engineInfo = [String: AnyObject]()
        engineInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "malfunction_indicador")
        let engineCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Engine".localizedString(), value:Helper.getDefaultString(object: mechincalInspection?.engineInspection, defaultString: "Perfect".localizedString()) ,info: engineInfo, height: Constants.CellHeightConstants.height_80)
        array.append(engineCell)
        //Perfect
        //TextView Cell
        let textViewEngineCell = CellInfo(cellType: .TextViewCell, placeHolder: "Please specify damages...".localizedString(), value: Helper.toString(object: mechincalInspection?.engineInspectionInfo), info: nil, height: UITableView.automaticDimension)
        array.append(textViewEngineCell)
        
        // Gear Cell
        var gearInfo = [String: AnyObject]()
        gearInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "transmission")
        let gearCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Gear".localizedString(), value:Helper.getDefaultString(object: mechincalInspection?.gear, defaultString: "Perfect".localizedString()) ,info: gearInfo, height: Constants.CellHeightConstants.height_80)
        array.append(gearCell)
        
        // Suspension Cell
        var suspensionInfo = [String: AnyObject]()
        suspensionInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "chasis")
        let suspensionCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Suspension".localizedString(), value:Helper.getDefaultString(object: mechincalInspection?.suspension, defaultString: "Perfect".localizedString()) ,info: suspensionInfo, height: Constants.CellHeightConstants.height_80)
        array.append(suspensionCell)
        
        // AC Cell
        var acInfo = [String: AnyObject]()
        acInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "ac")
        let acCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "AC".localizedString(), value:Helper.getDefaultString(object: mechincalInspection?.ac, defaultString: "Perfect".localizedString()) ,info: acInfo, height: Constants.CellHeightConstants.height_80)
        array.append(acCell)
        
        // Doors Cell
        var doorsInfo = [String: AnyObject]()
        doorsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "num_of_door")
        let doorCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Doors".localizedString(), value:Helper.getDefaultString(object: mechincalInspection?.doors, defaultString: "Perfect".localizedString()) ,info: doorsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(doorCell)
        
        // Windows Cell
        var windowsInfo = [String: AnyObject]()
        windowsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "windows")
        let windowsCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Windows".localizedString(), value:Helper.getDefaultString(object: mechincalInspection?.windowsInspection, defaultString: "Perfect".localizedString()) ,info: windowsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(windowsCell)
        
        // Audio System Cell
        var audioSystemInfo = [String: AnyObject]()
        audioSystemInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "audio_system")
        let audioSystemCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Audio System".localizedString(), value:Helper.getDefaultString(object: mechincalInspection?.audioSystem, defaultString: "Perfect".localizedString()) ,info: audioSystemInfo, height: Constants.CellHeightConstants.height_80)
        array.append(audioSystemCell)
        
        // Features Cell
        var featuresInfo = [String: AnyObject]()
        featuresInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "features")
        let featuresCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Features".localizedString(), value:Helper.getDefaultString(object: mechincalInspection?.features, defaultString: "Perfect".localizedString()) ,info: featuresInfo, height: Constants.CellHeightConstants.height_80)
        array.append(featuresCell)
        
        // Tyres Condition Cell
        var tyresConditionInfo = [String: AnyObject]()
        tyresConditionInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "wheels-1")
        tyresConditionInfo[Constants.UIKeys.isHide] = true as AnyObject
        let tyresConditionCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Tires Condition".localizedString(), value:"" ,info: tyresConditionInfo, height: Constants.CellHeightConstants.height_80)
        array.append(tyresConditionCell)
        
        var frontRightInfo = [String: AnyObject]()
        let fr = mechincalInspection?.getTyreCondition(key: ConstantAPIKeys.fr)
        frontRightInfo[Constants.UIKeys.firstValue] = Helper.toString(object: fr?.week) as AnyObject
        frontRightInfo[Constants.UIKeys.secondValue] = Helper.toString(object: fr?.year) as AnyObject
        let frontRightCell = CellInfo(cellType: .TyresConditionCell, placeHolder: "Front Right (FR)".localizedString(), value:"" ,info: frontRightInfo, height: Constants.CellHeightConstants.height_80)
        array.append(frontRightCell)
        
        var frontLeftInfo = [String: AnyObject]()
        let fl = mechincalInspection?.getTyreCondition(key: ConstantAPIKeys.fl)
        frontLeftInfo[Constants.UIKeys.firstValue] = Helper.toString(object: fl?.week) as AnyObject
        frontLeftInfo[Constants.UIKeys.secondValue] = Helper.toString(object: fl?.year) as AnyObject
        let fronLeftCell = CellInfo(cellType: .TyresConditionCell, placeHolder: "Front Left (FL)".localizedString(), value:"" ,info: frontLeftInfo, height: Constants.CellHeightConstants.height_80)
        array.append(fronLeftCell)
        
        var rearRightInfo = [String: AnyObject]()
        let rr = mechincalInspection?.getTyreCondition(key: ConstantAPIKeys.rr)
        rearRightInfo[Constants.UIKeys.firstValue] = Helper.toString(object: rr?.week) as AnyObject
        rearRightInfo[Constants.UIKeys.secondValue] = Helper.toString(object: rr?.year) as AnyObject
        let rearRightCell = CellInfo(cellType: .TyresConditionCell, placeHolder: "Rear Right (RR)".localizedString(), value:"" ,info: rearRightInfo, height: Constants.CellHeightConstants.height_80)
        array.append(rearRightCell)
        
        var rearLeftInfo = [String: AnyObject]()
        let rl = mechincalInspection?.getTyreCondition(key: ConstantAPIKeys.rl)
        rearLeftInfo[Constants.UIKeys.firstValue] = Helper.toString(object: rl?.week) as AnyObject
        rearLeftInfo[Constants.UIKeys.secondValue] = Helper.toString(object: rl?.year) as AnyObject
        let rearLeftCell = CellInfo(cellType: .TyresConditionCell, placeHolder: "Rear Left (RL)".localizedString(), value:"" ,info: rearLeftInfo, height: Constants.CellHeightConstants.height_80)
        array.append(rearLeftCell)
        
        // Other Information
        var otherInfo = [String: AnyObject]()
        otherInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "other_info")
        otherInfo[Constants.UIKeys.isHide] = true as AnyObject
        let otherCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Other Information".localizedString(), value:"" ,info: otherInfo, height: Constants.CellHeightConstants.height_80)
        array.append(otherCell)
        
        //TextView Cell
        let textViewCell = CellInfo(cellType: .TextViewCell, placeHolder: "Write Something...".localizedString(), value: Helper.toString(object: mechincalInspection?.mechincalInspectionInfo), info: nil, height: UITableView.automaticDimension)
        array.append(textViewCell)
        
        //Next Cell
        let submitCell = CellInfo(cellType: .BottomButtonCell, placeHolder: "Next".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_100)
        array.append(submitCell)
        
        return array
    }
    
    //MARK: - API Methods
     func requestAuctionAPI(arrData: [CellInfo],auctionId:Int, completion: @escaping (DAuctionModel)-> Void) {
       if self.validatePostCarData(arrData: arrData) {
         let params = self.getPostAddCarParams(arrData: arrData,auctionId:auctionId)
         APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .postAuctionCar(param: params))) { (response, success) in
           if success {
               if let safeResponse =  response as? [String: AnyObject],
                   let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                   let auctionData = result.toJSONData() {
                   do {
                       let decoder = JSONDecoder()
                       let auctionModelData = try decoder.decode(DAuctionModel.self, from: auctionData)
                       completion(auctionModelData)
                   } catch let error {
                       print(error)
                   }
               }
           }
         }
       }
     }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
    func validatePostCarData(arrData: [CellInfo]) -> Bool {
        var isValid = true
        var message = ""
        if let engine = arrData[0].value, engine.isEmpty {
            message = "Please select engine."
            isValid = false
        } else if let gear = arrData[2].value, gear.isEmpty  {
            message = "Please select gear."
            isValid = false
        } else if let suspension = arrData[3].value, suspension.isEmpty {
            message = "Please select suspension."
            isValid = false
        }  else if let ac = arrData[4].value, ac.isEmpty {
            message = "Please enter ac."
            isValid = false
        } else if let doors = arrData[5].value,doors.isEmpty {
            message = "Please select doors"
            isValid = false
        } else if let windows = arrData[6].value, windows.isEmpty {
            message = "Please enter windows."
            isValid = false
        } else if let audioSystem = arrData[7].value, audioSystem.isEmpty {
            message = "Please select audio system"
            isValid = false
        } else if let features = arrData[8].value, features.isEmpty {
            message = "Please select features"
            isValid = false
        } else if let firstValue = arrData[10].info?[Constants.UIKeys.firstValue] as? String, firstValue.isEmpty {
            message = "Please select Front Right (FR)"
            isValid = false
        } else if let secondValue = arrData[10].info?[Constants.UIKeys.secondValue] as? String, secondValue.isEmpty {
            message = "Please select Front Right (FR)"
            isValid = false
        } else if let firstValue = arrData[11].info?[Constants.UIKeys.firstValue] as? String, firstValue.isEmpty {
            message = "Please select Front Left (FL)"
            isValid = false
        } else if let secondValue = arrData[11].info?[Constants.UIKeys.secondValue] as? String, secondValue.isEmpty {
            message = "Please select Front Left (FL)"
            isValid = false
        }   else if let firstValue = arrData[12].info?[Constants.UIKeys.firstValue] as? String, firstValue.isEmpty {
            message = "Please select Rear Right (RR)"
            isValid = false
        } else if let secondValue = arrData[12].info?[Constants.UIKeys.secondValue] as? String, secondValue.isEmpty {
            message = "Please select Rear Right (RR)"
            isValid = false
        } else if let firstValue = arrData[13].info?[Constants.UIKeys.firstValue] as? String, firstValue.isEmpty {
            message = "Please select Rear Left (RL)"
            isValid = false
        } else if let secondValue = arrData[13].info?[Constants.UIKeys.secondValue] as? String, secondValue.isEmpty {
            message = "Please select Rear Left (RL)"
            isValid = false
        }
        
        if !isValid {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: message.localizedString())
        }
        return isValid
    }
    
    func getPostAddCarParams(arrData: [CellInfo],auctionId:Int)-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.id] = auctionId as AnyObject
        params[ConstantAPIKeys.auctionPage] = ConstantAPIKeys.mechincalInspection as AnyObject
        params[ConstantAPIKeys.engineInspection] = arrData[0].value as AnyObject
        params[ConstantAPIKeys.engineInspectionInfo] = arrData[1].value as AnyObject
        params[ConstantAPIKeys.gear] = arrData[2].value as AnyObject
        params[ConstantAPIKeys.suspension] = arrData[3].value as AnyObject
        params[ConstantAPIKeys.ac] = arrData[4].value as AnyObject
        params[ConstantAPIKeys.doors] = arrData[5].value as AnyObject
        params[ConstantAPIKeys.windowsInspection] = arrData[6].value as AnyObject
        params[ConstantAPIKeys.audioSystem] = arrData[7].value as AnyObject
        params[ConstantAPIKeys.features] = arrData[8].value as AnyObject
        params[ConstantAPIKeys.mechincalInspectionInfo] = arrData[15].value as AnyObject
        
        var fr = [String: AnyObject]()
        if let firstValue = arrData[10].info?[Constants.UIKeys.firstValue] as? String {
           fr[ConstantAPIKeys.week] = firstValue as AnyObject
        }
        
        if let secondValue = arrData[10].info?[Constants.UIKeys.secondValue] as? String {
           fr[ConstantAPIKeys.year] = secondValue as AnyObject
        }
        
        var fl = [String: AnyObject]()
        if let firstValue = arrData[11].info?[Constants.UIKeys.firstValue] as? String {
           fl[ConstantAPIKeys.week] = firstValue as AnyObject
        }
        
        if let secondValue = arrData[11].info?[Constants.UIKeys.secondValue] as? String {
           fl[ConstantAPIKeys.year] = secondValue as AnyObject
        }
        
        var rr = [String: AnyObject]()
        if let firstValue = arrData[12].info?[Constants.UIKeys.firstValue]  as? String{
           rr[ConstantAPIKeys.week] = firstValue as AnyObject
        }
        
        if let secondValue = arrData[12].info?[Constants.UIKeys.secondValue]  as? String{
           rr[ConstantAPIKeys.year] = secondValue as AnyObject
        }
        
        var rl = [String: AnyObject]()
        if let firstValue = arrData[13].info?[Constants.UIKeys.firstValue] as? String {
           rl[ConstantAPIKeys.week] = firstValue as AnyObject
        }
        
        if let secondValue = arrData[13].info?[Constants.UIKeys.secondValue]  as? String{
           rl[ConstantAPIKeys.year] = secondValue as AnyObject
        }
        
        var tyresCondition = [String: AnyObject]()
        tyresCondition[ConstantAPIKeys.fr] = fr as AnyObject
        tyresCondition[ConstantAPIKeys.fl] = fl as AnyObject
        tyresCondition[ConstantAPIKeys.rr] = rr as AnyObject
        tyresCondition[ConstantAPIKeys.rl] = rl as AnyObject

//        var tyresCondition = [[String: AnyObject]]()
//        tyresCondition.append([ConstantAPIKeys.fr : fr as AnyObject])
//        tyresCondition.append([ConstantAPIKeys.fl : fl as AnyObject])
//        tyresCondition.append([ConstantAPIKeys.rr : rr as AnyObject])
//        tyresCondition.append([ConstantAPIKeys.rl : rl as AnyObject])
        
        params[ConstantAPIKeys.tyreCondition] = tyresCondition as AnyObject
        return params
    }
    
}

