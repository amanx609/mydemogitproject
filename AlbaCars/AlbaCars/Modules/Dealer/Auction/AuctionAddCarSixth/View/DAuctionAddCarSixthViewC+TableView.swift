//
//  DAuctionAddCarSixthViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 1/2/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension DAuctionAddCarSixthViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addCarDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.addCarDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.addCarDataSource[indexPath.row].height
    }
}

extension DAuctionAddCarSixthViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .DAuctionAddCarDropDownCell:
            let cell: DAuctionAddCarDropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .BottomButtonCell:
            let cell: BottomButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .DAddAuctionImageCell:
            let cell: DAddAuctionImageCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - DAuctionAddCarDropDownCellDelegate
extension DAuctionAddCarSixthViewC: DAuctionAddCarDropDownCellDelegate {
    func didTapDAuctionAddCarDropDownCell(cell: DAuctionAddCarDropDownCell) {
        guard let indexPath = self.addCarTableView.indexPath(for: cell) else { return }
        let cellInfo = self.addCarDataSource[indexPath.row]
        self.showDropDownAt(indexPath: indexPath, title: cellInfo.placeHolder)
    }
}

//MARK: - BottomButtonCellDelegate
extension DAuctionAddCarSixthViewC: BottomButtonCellDelegate {
    func didTapBottomButton(cell: BottomButtonCell) {
        self.view.endEditing(true)
        self.requestAuctionAPI()
        
//        if let isValid = self.viewModel?.validatePostCarData(arrData: self.addCarDataSource),isValid {
//                        if let param = self.viewModel?.getPostAddCarParams(arrData: self.addCarDataSource) {
//                            let auctionUploadCarImageVC = DIConfigurator.sharedInstance.getDAuctionUploadCarImageVC()
//                            auctionUploadCarImageVC.params = param
//                            self.navigationController?.pushViewController(auctionUploadCarImageVC, animated: true)
//                        }
//        }
    }
}
