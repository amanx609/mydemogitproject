//
//  DAddAuctionImageCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/2/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol DAddAuctionImageCellDelegate: class {
    func didTapDentDescription(cell: DAddAuctionImageCell,text: String, count: Int)
}

class DAddAuctionImageCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var carImageView: UIImageView!
    
    //MARK: - Variables
    weak var delegate: DAddAuctionImageCellDelegate?
    var count = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        
        if let image = cellInfo.info?[Constants.UIKeys.image] as? UIImage {
            self.carImageView.image = image
        }
    }
    
    //MARK: - Public Methods
    func configureTenderDentView(cellInfo: CellInfo) {
        
        if let image = cellInfo.info?[Constants.UIKeys.image] as? UIImage {
            self.carImageView.image = image
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.carImageView.isUserInteractionEnabled = true
        self.carImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let  touchLocation = tapGestureRecognizer.location(in: self.carImageView)

        guard let dentDescriptionPopup = DentDescriptionPopup.inistancefromNib() else { return }
        dentDescriptionPopup.initializeViewWith() { [weak self] (response) in
            guard let sSelf = self else { return }
            sSelf.count = sSelf.count + 1
            let customView = UIView(frame: CGRect(x: touchLocation.x - 15, y: touchLocation.y - 15, width: 30, height: 30))
            let label = UILabel(frame: customView.bounds)
            label.textColor = .white
            label.textAlignment = .center
            label.text = Helper.toString(object: sSelf.count)
            customView.addSubview(label)
            customView.backgroundColor = .redButtonColor
            customView.layer.cornerRadius = 15
            sSelf.carImageView.addSubview(customView)
            
            if let delegate = sSelf.delegate {
                delegate.didTapDentDescription(cell: sSelf, text: response, count: sSelf.count)
            }
        }
        
        dentDescriptionPopup.showWithAnimated(animated: true)
    }
}
