//
//  DAuctionAddCarSixthViewC.swift
//  AlbaCars
//
//  Created by Narendra on 1/2/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DAuctionAddCarSixthViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var addCarTableView: UITableView!
    
    //MARK: - Properties
    var viewModel: DAuctionAddCarSixthViewModeling?
    var addCarDataSource: [CellInfo] = []
    var auctionModel = DAuctionModel()
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Add Car Details".localizedString(),barColor: .redButtonColor,titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.loadDataSource()
    }
    
    //MARK: - Private Methods
    private func setup() {
        setupViewModel()
        registerNibs()
        setupTableView()
    }
    
    private func setupTableView() {
        self.addCarTableView.separatorStyle = .none
        self.addCarTableView.backgroundColor = UIColor.white
        self.addCarTableView.delegate = self
        self.addCarTableView.dataSource = self
        self.addCarTableView.allowsSelection =  false
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = DAuctionAddCarSixthViewM()
        }
    }
    
    private func registerNibs() {
        self.addCarTableView.register(DAuctionAddCarInputCell.self)
        self.addCarTableView.register(DAuctionAddCarDropDownCell.self)
        self.addCarTableView.register(BottomButtonCell.self)
        self.addCarTableView.register(DAddAuctionImageCell.self)        
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getAddCarFormDataSource(bodyInspection: self.auctionModel.bodyInspection) {
            self.addCarDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.addCarTableView.reloadData()
            }
        }
    }
    
    func showDropDownAt(indexPath: IndexPath, title: String) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        switch indexPath.row {
        case 1,2,11,12,16,17,18,19,20,21,22,23,24,25,26,27,28:
            listPopupView.initializeViewWith(title: title, arrayList: DropDownType.perfectDamaged.dataList, key: DropDownType.perfectDamaged.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let conditionStatus = response[DropDownType.perfectDamaged.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = conditionStatus
                sSelf.addCarTableView.reloadData()
            }
        case 3,4,5,6,7,8,9,10,13,14,15:
            listPopupView.initializeViewWith(title: title, arrayList: DropDownType.originalDamaged.dataList, key: DropDownType.originalDamaged.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let originalDamaged = response[DropDownType.originalDamaged.rawValue] as? String else { return }
                sSelf.addCarDataSource[indexPath.row].value = originalDamaged
                sSelf.addCarTableView.reloadData()
            }
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
        
    }
    
    //MARK: - API Methods
    func requestAuctionAPI() {
        self.viewModel?.requestAuctionAPI(arrData: self.addCarDataSource, auctionId: Helper.toInt(self.auctionModel.mechincalInspection?.id)) { (responseData) in
            self.auctionModel = responseData
            Threads.performTaskInMainQueue {
                let auctionUploadCarImageVC = DIConfigurator.sharedInstance.getDAuctionUploadCarImageVC()
                auctionUploadCarImageVC.auctionModel = self.auctionModel
                self.navigationController?.pushViewController(auctionUploadCarImageVC, animated: true)
            }
        }
    }
    
}
