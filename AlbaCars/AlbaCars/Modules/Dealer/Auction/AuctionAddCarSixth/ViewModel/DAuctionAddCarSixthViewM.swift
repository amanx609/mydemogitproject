//
//  DAuctionAddCarSixthViewM.swift
//  AlbaCars
//
//  Created by Narendra on 1/2/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//


import Foundation
import UIKit

protocol DAuctionAddCarSixthViewModeling {
    func getAddCarFormDataSource(bodyInspection: BodyInspection?) -> [CellInfo]
    func validatePostCarData(arrData: [CellInfo]) -> Bool
    func requestAuctionAPI(arrData: [CellInfo],auctionId:Int, completion: @escaping (DAuctionModel)-> Void)
}

class DAuctionAddCarSixthViewM: DAuctionAddCarSixthViewModeling {
    
    func getAddCarFormDataSource(bodyInspection: BodyInspection?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        // Image
        let addAuctionImageCellCell = CellInfo(cellType: .DAddAuctionImageCell, placeHolder: "", value:"" ,info: nil, height: Constants.CellHeightConstants.height_450)
        array.append(addAuctionImageCellCell)
        
        // Bumper (Front)
        var bumperInfo = [String: AnyObject]()
        bumperInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "bumper.png")
        let bumperCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Bumper (Front)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.frontBumper, defaultString: "Perfect".localizedString()) ,info: bumperInfo, height: Constants.CellHeightConstants.height_80)
        array.append(bumperCell)
        
        // Bumper (Rear)
        var bumperRearInfo = [String: AnyObject]()
        bumperRearInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "bumper.png")
        let bumperRearCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Bumper (Rear)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.rearBumper, defaultString: "Perfect".localizedString()) ,info: bumperRearInfo, height: Constants.CellHeightConstants.height_80)
        array.append(bumperRearCell)
        
        // Doors (Front Right)
        var doorsFrontRightInfo = [String: AnyObject]()
        doorsFrontRightInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "num_of_door")
        let doorsFrontRightCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Doors (Front Right)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.frontRightDoors, defaultString: "Original".localizedString()) ,info: doorsFrontRightInfo, height: Constants.CellHeightConstants.height_80)
        array.append(doorsFrontRightCell)
        
        // Doors (Front Left)
        var doorsFrontLeftInfo = [String: AnyObject]()
        doorsFrontLeftInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "num_of_door")
        let doorsFrontLeftCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Doors (Front Left)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.frontLeftDoors, defaultString: "Original".localizedString()) ,info: doorsFrontLeftInfo, height: Constants.CellHeightConstants.height_80)
        array.append(doorsFrontLeftCell)
        
        // Doors (Rear Right)
        var doorsRearRightInfo = [String: AnyObject]()
        doorsRearRightInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "num_of_door")
        let doorsRearRightCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Doors (Rear Right)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.rearRightDoors, defaultString: "Original".localizedString()) ,info: doorsRearRightInfo, height: Constants.CellHeightConstants.height_80)
        array.append(doorsRearRightCell)
        
        // Doors (Rear Left)
        var doorsRearLeftInfo = [String: AnyObject]()
        doorsRearLeftInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "num_of_door")
        let doorsRearLeftCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Doors (Rear Left)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.rearLeftDoors, defaultString: "Original".localizedString()) ,info: doorsRearLeftInfo, height: Constants.CellHeightConstants.height_80)
        array.append(doorsRearLeftCell)
        
        //  Fenders (Front Right)
        var fendersFrontRightInfo = [String: AnyObject]()
        fendersFrontRightInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "fenders.png")
        let fendersFrontRightCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Fenders (Front Right)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.frontRightFenders, defaultString: "Original".localizedString()) ,info: fendersFrontRightInfo, height: Constants.CellHeightConstants.height_80)
        array.append(fendersFrontRightCell)
        
        // Fenders (Front Left)
        var fendersFrontLeftInfo = [String: AnyObject]()
        fendersFrontLeftInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "fenders.png")
        let fendersFrontLeftCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Fenders (Front Left)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.frontLeftFenders, defaultString: "Original".localizedString()) ,info: fendersFrontLeftInfo, height: Constants.CellHeightConstants.height_80)
        array.append(fendersFrontLeftCell)
        
        // Fenders (Rear Right)
        var fendersRearRightInfo = [String: AnyObject]()
        fendersRearRightInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "fenders.png")
        let fendersRearRightCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Fenders (Rear Right)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.rearRightFenders, defaultString: "Original".localizedString()) ,info:  fendersRearRightInfo, height: Constants.CellHeightConstants.height_80)
        array.append( fendersRearRightCell)
        
        //  Fenders (Rear Left)
        var fendersRearLeftInfo = [String: AnyObject]()
        fendersRearLeftInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "fenders.png")
        let fendersRearLeftCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Fenders (Rear Left)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.rearLeftFenders, defaultString: "Original".localizedString()) ,info: fendersRearLeftInfo, height: Constants.CellHeightConstants.height_80)
        array.append(fendersRearLeftCell)
        
        // Side Skirt (Right)
        var sideSkirtRightInfo = [String: AnyObject]()
        sideSkirtRightInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "side.png")
        let sideSkirtRightCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Side Skirt (Right)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.rightSideskirt, defaultString: "Perfect".localizedString()) ,info: sideSkirtRightInfo, height: Constants.CellHeightConstants.height_80)
        array.append(sideSkirtRightCell)
        
        //  Side Skirt (Left)
        var sideSkirtRearInfo = [String: AnyObject]()
        sideSkirtRearInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "side.png")
        let sideSkirtRearCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Side Skirt (Left)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.leftSideskirt, defaultString: "Perfect".localizedString()) ,info: sideSkirtRearInfo, height: Constants.CellHeightConstants.height_80)
        array.append(sideSkirtRearCell)
        
        // Bonnet Cell
        var bonnetInfo = [String: AnyObject]()
        bonnetInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "bonnet.png")
        let bonnetCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Bonnet".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.bonnet, defaultString: "Original".localizedString()) ,info: bonnetInfo, height: Constants.CellHeightConstants.height_80)
        array.append(bonnetCell)
        
        // Boot Cell
        var bootInfo = [String: AnyObject]()
        bootInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "boot.png")
        let bootCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Boot".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.boot, defaultString: "Original".localizedString()) ,info: bootInfo, height: Constants.CellHeightConstants.height_80)
        array.append(bootCell)
        
        // Roof Cell
        var sunroofInfo = [String: AnyObject]()
        sunroofInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "sunroof")
        let sunroofCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Roof".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.roofInspection, defaultString: "Original".localizedString()) ,info: sunroofInfo, height: Constants.CellHeightConstants.height_80)
        array.append(sunroofCell)
        
        // Wheels (Front Right)
        var wheelsFrontRightInfo = [String: AnyObject]()
        wheelsFrontRightInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "wheels-1")
        let wheelsFrontRightCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Wheels (Front Right)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.frontRightWheels, defaultString: "Perfect".localizedString()) ,info: wheelsFrontRightInfo, height: Constants.CellHeightConstants.height_80)
        array.append(wheelsFrontRightCell)
        
        // Wheels (Front Left)
        var wheelsFrontLeftInfo = [String: AnyObject]()
        wheelsFrontLeftInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "wheels-1")
        let wheelsFrontLeftCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Wheels (Front Left)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.frontLeftWheels, defaultString: "Perfect".localizedString()) ,info: wheelsFrontLeftInfo, height: Constants.CellHeightConstants.height_80)
        array.append(wheelsFrontLeftCell)
        
        //  Wheels (Rear Right)
        var wheelsRearRightInfo = [String: AnyObject]()
        wheelsRearRightInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "wheels-1")
        let wheelsRearRightCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Wheels (Rear Right)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.rearRightWheels, defaultString: "Perfect".localizedString()) ,info: wheelsRearRightInfo, height: Constants.CellHeightConstants.height_80)
        array.append(wheelsRearRightCell)
        
        // Wheels (Rear Left)
        var wheelsRearLeftInfo = [String: AnyObject]()
        wheelsRearLeftInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "wheels-1")
        let wheelsRearLeftCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Wheels (Rear Left)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.rearLeftWheels, defaultString: "Perfect".localizedString()) ,info: wheelsRearLeftInfo, height: Constants.CellHeightConstants.height_80)
        array.append(wheelsRearLeftCell)
        
        //  Glass (Front)
        var  glassFrontInfo = [String: AnyObject]()
        glassFrontInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "glass.png")
        let glassFrontCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Glass (Front)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.frontGlass, defaultString: "Perfect".localizedString()) ,info: glassFrontInfo, height: Constants.CellHeightConstants.height_80)
        array.append(glassFrontCell)
        
        //  Glass (Rear)
        var  glassRearInfo = [String: AnyObject]()
        glassRearInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "glass.png")
        let glassRearCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Glass (Rear)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.rearGlass, defaultString: "Perfect".localizedString()) ,info: glassRearInfo, height: Constants.CellHeightConstants.height_80)
        array.append(glassRearCell)
        
        //  Glass (Front Right)
        var  glassFrontRightInfo = [String: AnyObject]()
        glassFrontRightInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "glass.png")
        let glassFrontRightCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Glass (Front Right)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.frontRightGlass, defaultString: "Perfect".localizedString()) ,info: glassFrontRightInfo, height: Constants.CellHeightConstants.height_80)
        array.append(glassFrontRightCell)
        
        //  Glass (Front Left)
        var  glassFrontLeftInfo = [String: AnyObject]()
        glassFrontLeftInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "glass.png")
        let glassFrontLeftCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: " Glass (Front Left)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.frontLeftGlass, defaultString: "Perfect".localizedString()) ,info: glassFrontLeftInfo, height: Constants.CellHeightConstants.height_80)
        array.append(glassFrontLeftCell)
        
        // Glass (Rear Right)
        var glassRearRightInfo = [String: AnyObject]()
        glassRearRightInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "glass.png")
        let glassRearRightCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Glass (Rear Right)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.rearRightGlass, defaultString: "Perfect".localizedString()) ,info: glassRearRightInfo, height: Constants.CellHeightConstants.height_80)
        array.append(glassRearRightCell)
        
        // Glass (Rear Left)
        var  glassRearLeftInfo = [String: AnyObject]()
        glassRearLeftInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "glass.png")
        let glassRearLeftCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: " Glass (Rear Left)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.rearLeftGlass, defaultString: "Perfect".localizedString()) ,info: glassRearLeftInfo, height: Constants.CellHeightConstants.height_80)
        array.append(glassRearLeftCell)
        
        // Glass (Rear Right Corner)
        var glassRearRightCornerInfo = [String: AnyObject]()
        glassRearRightCornerInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "glass.png")
        let glassRearRightCornerCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Glass (Rear Right Corner)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.rearRightCornerGlass, defaultString: "Perfect".localizedString()) ,info: glassRearRightCornerInfo, height: Constants.CellHeightConstants.height_80)
        array.append(glassRearRightCornerCell)
        
        // Glass (Rear Left Corner)
        var glassRearLeftCornerInfo = [String: AnyObject]()
        glassRearLeftCornerInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "glass.png")
        let glassRearLeftCornerCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Glass (Rear Left Corner)".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.rearLeftCornerGlass, defaultString: "Perfect".localizedString()) ,info: glassRearLeftCornerInfo, height: Constants.CellHeightConstants.height_80)
        array.append(glassRearLeftCornerCell)
        
        // Side Mirrors Cell
        var mirrorsInfo = [String: AnyObject]()
        mirrorsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "mirrors")
        let mirrorsCell = CellInfo(cellType: .DAuctionAddCarDropDownCell, placeHolder: "Side Mirrors".localizedString(), value:Helper.getDefaultString(object: bodyInspection?.sidemirrors, defaultString: "Perfect".localizedString()) ,info: mirrorsInfo, height: Constants.CellHeightConstants.height_80)
        array.append(mirrorsCell)
        
        //Next Cell
        let submitCell = CellInfo(cellType: .BottomButtonCell, placeHolder: "Next".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_100)
        array.append(submitCell)
        
        return array
    }
    
    //MARK: - API Methods
    func requestAuctionAPI(arrData: [CellInfo],auctionId:Int, completion: @escaping (DAuctionModel)-> Void) {
      if self.validatePostCarData(arrData: arrData) {
        let params = self.getPostAddCarParams(arrData: arrData,auctionId:auctionId)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .postAuctionCar(param: params))) { (response, success) in
          if success {
              if let safeResponse =  response as? [String: AnyObject],
                  let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                  let auctionData = result.toJSONData() {
                  do {
                      let decoder = JSONDecoder()
                      let auctionModelData = try decoder.decode(DAuctionModel.self, from: auctionData)
                      completion(auctionModelData)
                  } catch let error {
                      print(error)
                  }
              }
          }
        }
      }
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
    func validatePostCarData(arrData: [CellInfo]) -> Bool {
        var isValid = true
        var message = ""
        if let bumper = arrData[1].value, bumper.isEmpty {
            message = "Please select Bumper (Front)."
            isValid = false
        } else if let bumper = arrData[2].value, bumper.isEmpty  {
            message = "Please select Bumper (Rear)."
            isValid = false
        } else if let doors = arrData[3].value, doors.isEmpty {
            message = "Please select Doors (Front Right)."
            isValid = false
        }  else if let doors = arrData[4].value, doors.isEmpty {
            message = "Please select Doors (Front Left)."
            isValid = false
        } else if let doors = arrData[5].value,doors.isEmpty {
            message = "Please select Doors (Rear Right)"
            isValid = false
        } else if let doors = arrData[6].value, doors.isEmpty {
            message = "Please select Doors (Rear Left)"
            isValid = false
        } else if let fenders = arrData[7].value, fenders.isEmpty {
            message = "Please select Fenders (Front Right)"
            isValid = false
        } else if let fenders = arrData[8].value, fenders.isEmpty {
            message = "Please select Fenders (Front Left)"
            isValid = false
        } else if let fenders = arrData[9].value, fenders.isEmpty {
            message = "Please select Fenders (Rear Right)"
            isValid = false
        } else if let fenders = arrData[10].value, fenders.isEmpty {
            message = "Please select Fenders (Rear Left)"
            isValid = false
        } else if let skirt = arrData[11].value, skirt.isEmpty {
            message = "Please select Side Skirt (Right)"
            isValid = false
        } else if let skirt = arrData[12].value, skirt.isEmpty {
            message = "Please select Side Skirt (Left)"
            isValid = false
        } else if let bonnet = arrData[13].value, bonnet.isEmpty {
            message = "Please select Bonnet"
            isValid = false
        } else if let boot = arrData[14].value, boot.isEmpty {
            message = "Please select Boot"
            isValid = false
        } else if let roof = arrData[15].value, roof.isEmpty {
            message = "Please select Roof"
            isValid = false
        } else if let wheels = arrData[16].value, wheels.isEmpty {
            message = "Please select Wheels (Front Right)"
            isValid = false
        } else if let wheels = arrData[17].value, wheels.isEmpty {
            message = "Please select Wheels (Front Left)"
            isValid = false
        } else if let wheels = arrData[18].value, wheels.isEmpty {
            message = "Please select Wheels (Rear Right)"
            isValid = false
        } else if let wheels = arrData[19].value, wheels.isEmpty {
            message = "Please select Wheels (Rear Left)"
            isValid = false
        } else if let glass = arrData[20].value, glass.isEmpty {
            message = "Please select Glass (Front)"
            isValid = false
        } else if let glass = arrData[21].value, glass.isEmpty {
            message = "Please select Glass (Rear)"
            isValid = false
        } else if let glass = arrData[22].value, glass.isEmpty {
            message = "Please select Glass (Front Right)"
            isValid = false
        } else if let glass = arrData[23].value, glass.isEmpty {
            message = "Please select Glass (Front Left)"
            isValid = false
        } else if let glass = arrData[24].value, glass.isEmpty {
            message = "Please select Glass (Rear Right)"
            isValid = false
        } else if let glass = arrData[25].value, glass.isEmpty {
            message = "Please select Glass (Rear Left)"
            isValid = false
        } else if let glass = arrData[26].value, glass.isEmpty {
            message = "Please select Glass (Rear Right Corner)"
            isValid = false
        } else if let glass = arrData[27].value, glass.isEmpty {
            message = "Please select Glass (Rear Left Corner)"
            isValid = false
        } else if let mirrors = arrData[28].value, mirrors.isEmpty {
            message = "Please select Side Mirrors"
            isValid = false
        }
        
        if !isValid {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: message.localizedString())
        }
        return isValid
    }
    
    func getPostAddCarParams(arrData: [CellInfo],auctionId:Int)-> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.id] = auctionId as AnyObject
        params[ConstantAPIKeys.auctionPage] = ConstantAPIKeys.bodyInspection as AnyObject
        params[ConstantAPIKeys.frontBumper] = arrData[1].value as AnyObject
        params[ConstantAPIKeys.rearBumper] = arrData[2].value as AnyObject
        params[ConstantAPIKeys.frontRightDoors] = arrData[3].value as AnyObject
        params[ConstantAPIKeys.frontLeftDoors] = arrData[4].value as AnyObject
        params[ConstantAPIKeys.rearRightDoors] = arrData[5].value as AnyObject
        params[ConstantAPIKeys.rearLeftDoors] = arrData[6].value as AnyObject
        params[ConstantAPIKeys.frontRightFenders] = arrData[7].value as AnyObject
        params[ConstantAPIKeys.frontLeftFenders] = arrData[8].value as AnyObject
        params[ConstantAPIKeys.rearRightFenders] = arrData[9].value as AnyObject
        params[ConstantAPIKeys.rearLeftFenders] = arrData[10].value as AnyObject
        params[ConstantAPIKeys.rightSideskirt] = arrData[11].value as AnyObject
        params[ConstantAPIKeys.leftSideskirt] = arrData[12].value as AnyObject
        params[ConstantAPIKeys.bonnet] = arrData[13].value as AnyObject
        params[ConstantAPIKeys.boot] = arrData[14].value as AnyObject
        params[ConstantAPIKeys.roofInspection] = arrData[15].value as AnyObject
        params[ConstantAPIKeys.frontRightWheels] = arrData[16].value as AnyObject
        params[ConstantAPIKeys.frontLeftWheels] = arrData[17].value as AnyObject
        params[ConstantAPIKeys.rearRightWheels] = arrData[18].value as AnyObject
        params[ConstantAPIKeys.rearLeftWheels] = arrData[19].value as AnyObject
        params[ConstantAPIKeys.frontGlass] = arrData[20].value as AnyObject
        params[ConstantAPIKeys.rearGlass] = arrData[21].value as AnyObject
        params[ConstantAPIKeys.frontRightGlass] = arrData[22].value as AnyObject
        params[ConstantAPIKeys.frontLeftGlass] = arrData[23].value as AnyObject
        params[ConstantAPIKeys.rearLeftGlass] = arrData[24].value as AnyObject
        params[ConstantAPIKeys.rearRightGlass] = arrData[25].value as AnyObject
        params[ConstantAPIKeys.rearRightCornerGlass] = arrData[26].value as AnyObject
        params[ConstantAPIKeys.rearLeftCornerGlass] = arrData[27].value as AnyObject
        params[ConstantAPIKeys.sidemirrors] = arrData[28].value as AnyObject
        return params
    }
}
