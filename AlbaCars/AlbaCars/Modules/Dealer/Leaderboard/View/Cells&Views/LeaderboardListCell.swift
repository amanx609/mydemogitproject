//
//  LeaderboardListCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 23/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class LeaderboardListCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var brandLogoView: UIView!
    @IBOutlet weak var brandImageView: UIImageView!
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var leadsClosedCountLabel: UILabel!
    @IBOutlet weak var leadsPurchasedCountLabel: UILabel!
    @IBOutlet weak var conversionRatioLabel: UILabel!
    @IBOutlet weak var leadsClosedLabel: UILabel!
    @IBOutlet weak var leadsPurchasedLabel: UILabel!
    @IBOutlet weak var ratioLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.containerView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: 1, round: Constants.UIConstants.sizeRadius_3half)
        self.brandLogoView.roundCorners(self.brandLogoView.frame.width/2)
        self.ratioLabel.text = "Conversion Ratio ".localizedString()
    }
    
    //MARK: - Public Methods
    func configureView(cellData: DLeaderBoardModel, leaderboardType: LeaderBoardType, rank: Int) {
        self.brandImageView.setImage(urlStr: Helper.toString(object: cellData.logo), placeHolderImage: #imageLiteral(resourceName: "recovery"))
        self.rankLabel.text = Helper.toString(object: rank)
        switch leaderboardType {
        case .auctions:
            self.leadsClosedLabel.text = "Cars Sold ".localizedString()
            self.leadsPurchasedLabel.text = "Cars Added ".localizedString()
            self.brandNameLabel.text = Helper.toString(object: cellData.name)
            self.leadsClosedCountLabel.text = Helper.toString(object: cellData.carSold)
            self.leadsPurchasedCountLabel.text = Helper.toString(object: cellData.carAdded)
            self.conversionRatioLabel.text = Helper.toString(object: cellData.conversionPercentage())
            
            break
        case .fleetBidding:
            self.leadsClosedLabel.text = "Cars Won ".localizedString()
            self.leadsPurchasedLabel.text = "Cars Bid ".localizedString()
            self.brandNameLabel.text = Helper.toString(object: cellData.name)
            self.leadsClosedCountLabel.text = Helper.toString(object: cellData.wonBids)
            self.leadsPurchasedCountLabel.text = Helper.toString(object: cellData.totalBids)
            self.conversionRatioLabel.text = Helper.toString(object: cellData.conversionPercentage())
            break
        case .leads:
            self.leadsClosedLabel.text = "Leads Closed ".localizedString()
            self.leadsPurchasedLabel.text = "Leads Purchased ".localizedString()
            self.brandNameLabel.text = Helper.toString(object: cellData.name)
            self.leadsClosedCountLabel.text = Helper.toString(object: cellData.leadsClosed)
            self.leadsPurchasedCountLabel.text = Helper.toString(object: cellData.leadPurchased)
            self.conversionRatioLabel.text = Helper.toString(object: cellData.conversionPercentage())
            
            break
        }
        
        
    }
    
}
