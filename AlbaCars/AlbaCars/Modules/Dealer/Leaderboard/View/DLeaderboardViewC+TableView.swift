//
//  DLeaderboardViewC+TableView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 23/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension DLeaderboardViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.leaderBoardDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellData = self.leaderBoardDataSource[indexPath.row]
        return getCell(tableView, indexPath: indexPath, cellInfo: cellData)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return Constants.CellHeightConstants.height_130
        default:
            return Constants.CellHeightConstants.height_70
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        guard let lastVisibleRow = self.fleetsTableView.indexPathsForVisibleRows?.last?.row else { return }
//        if (self.perPage - lastVisibleRow == 3) && self.nextPageNumber != 0 {
//            self.requestFleetCompanyAPI()
//        }
    }
}

extension DLeaderboardViewC {
    func getCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: DLeaderBoardModel) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell: LeaderboardFirstRankCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellData: cellInfo, leaderboardType: self.leaderBoardType)
            return cell
        default:
            let cell: LeaderboardListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellData: cellInfo, leaderboardType: self.leaderBoardType, rank: indexPath.row + 1)
            return cell
        }
    }
}
