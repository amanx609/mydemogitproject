//
//  DLeaderboardViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 23/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DLeaderboardViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var leaderboardTableView: UITableView!
    @IBOutlet weak var auctionsButton: UIButton!
    @IBOutlet weak var fleetBiddingButton: UIButton!
    @IBOutlet weak var leadsButton: UIButton!
    
    //ConstraintOutlets
    @IBOutlet weak var bottomBarWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomBarLeadingConstraint: NSLayoutConstraint!
    
    //MARK: - Variables
    var viewModel: DLeaderboardVModeling?
    var leaderBoardDataSource: [DLeaderBoardModel] = []
    var leaderBoardType: LeaderBoardType = .auctions
     
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
     private func setup() {
       self.setupNavigationBarTitle(title: "Leaderboard".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
       self.recheckVM()
       self.setupView()
       self.setUpTableView()
       self.requestAuctionLeaderBoardAPI()
     }
     
     private func recheckVM() {
       if self.viewModel == nil {
         self.viewModel = DLeaderboardViewM()
       }
     }
    
    private func setupView() {
      self.bottomBarWidthConstraint.constant = Constants.Devices.ScreenWidth/3
      self.auctionsButton.setTitle("Auctions".localizedString(), for: .normal)
      self.fleetBiddingButton.setTitle("Fleet Bidding".localizedString(), for: .normal)
      self.leadsButton.setTitle("Leads".localizedString(), for: .normal)
      self.didTapButton(self.auctionsButton, isCalledOnViewDidLoad: true)
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.leaderboardTableView.delegate = self
        self.leaderboardTableView.dataSource = self
        self.leaderboardTableView.separatorStyle = .none
    }

    private func registerNibs() {
      self.leaderboardTableView.register(LeaderboardFirstRankCell.self)
      self.leaderboardTableView.register(LeaderboardListCell.self)
    }
    
    private func didTapButton(_ sender: UIButton, isCalledOnViewDidLoad: Bool = false) {
        var leadingConstraint: CGFloat = 0.0
        switch sender {
        case self.auctionsButton:
            //ChangeBottomMarkerConstraint
            leadingConstraint = 0.0
            //ChangeButtonTitleColor
            self.auctionsButton.setTitleColor(.white, for: .normal)
            self.fleetBiddingButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            self.leadsButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            leaderBoardType = .auctions
            self.requestAuctionLeaderBoardAPI()
            
        case self.fleetBiddingButton:
            //ChangeBottomMarkerConstraint
            leadingConstraint = Constants.Devices.ScreenWidth/3
            //ChangeButtonTitleColor
            self.auctionsButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            self.fleetBiddingButton.setTitleColor(.white, for: .normal)
            self.leadsButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            leaderBoardType = .fleetBidding
            self.requestFleetLeaderBoardAPI()
        case self.leadsButton:
            //ChangeBottomMarkerConstraint
            leadingConstraint = (Constants.Devices.ScreenWidth/3)*2
            //ChangeButtonTitleColor
            self.auctionsButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            self.fleetBiddingButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            self.leadsButton.setTitleColor(.white, for: .normal)
            leaderBoardType = .leads
            self.requestLeadsLeaderBoardAPI()
        default: break
        }
        if isCalledOnViewDidLoad {
            self.bottomBarLeadingConstraint.constant = leadingConstraint
            self.view.layoutIfNeeded()
        } else {
            //AnimateTheBottomViewMarker
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseOut], animations: {
                self.bottomBarLeadingConstraint.constant = leadingConstraint
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    //MARK: - API Methods
    private func requestAuctionLeaderBoardAPI() {
        self.viewModel?.requestAuctionLeaderboardAPI(completion: { [weak self] (nextPageNum, perPage, leaderBoardData) in
            guard let sSelf = self else { return }
            
            sSelf.leaderBoardDataSource = leaderBoardData
            sSelf.leaderboardTableView.reloadData()
        })
    }
    
    private func requestFleetLeaderBoardAPI() {
        self.viewModel?.requestFleetLeaderboardAPI(completion: { [weak self] (nextPageNum, perPage, leaderBoardData) in
            guard let sSelf = self else { return }
            sSelf.leaderBoardDataSource = leaderBoardData
            sSelf.leaderboardTableView.reloadData()
        })
    }
    
    private func requestLeadsLeaderBoardAPI() {
        self.viewModel?.requestLeadsLeaderboardAPI(completion: { [weak self] (nextPageNum, perPage, leaderBoardData) in
            guard let sSelf = self else { return }
            sSelf.leaderBoardDataSource = leaderBoardData
            sSelf.leaderboardTableView.reloadData()
        })
    }
    
    //MARK: - IBActions
    @IBAction func tapAuctions(_ sender: UIButton) {
        self.didTapButton(sender)
    }
    
    @IBAction func tapFleetBiding(_ sender: UIButton) {
        self.didTapButton(sender)
    }
    
    @IBAction func tapLeadsButton(_ sender: UIButton) {
        self.didTapButton(sender)
    }
}
