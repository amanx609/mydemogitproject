//
//  DLeaderBoardModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/31/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class DLeaderBoardModel: BaseCodable {
    var logo: String?
    var carAdded: Int?
    var carSold: Int?
    var carsBid: Int?
    var carsWon: Int?
    var leadPurchased: Int?
    var leadsClosed: Int?
    var image: String?
    var name: String?
    var user_id: Int?
    var conversionRatio: String?
    var totalBids: Int?
    var wonBids: Int?
    
    func conversionPercentage() -> String {
        let percentage = Helper.toDouble(conversionRatio)
        return "\(String(format: "%.2f", percentage))%"
    }
    
    
}

class DLeaderBoardList: BaseCodable {
    var leaderBoard: [DLeaderBoardModel]?
    
    private enum CodingKeys: String, CodingKey {
      case leaderBoard = "data"
    }

    
}

