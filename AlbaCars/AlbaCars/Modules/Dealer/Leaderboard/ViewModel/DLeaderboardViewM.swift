//
//  DLeaderboardViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 23/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DLeaderboardVModeling: BaseVModeling {
    func requestAuctionLeaderboardAPI(completion: @escaping (Int, Int, [DLeaderBoardModel]) -> Void)
    func requestFleetLeaderboardAPI(completion: @escaping (Int, Int, [DLeaderBoardModel]) -> Void)
    func requestLeadsLeaderboardAPI(completion: @escaping (Int, Int, [DLeaderBoardModel]) -> Void)
}

class DLeaderboardViewM: BaseViewM, DLeaderboardVModeling {
    
    func getUSTendersTableDataSource(tenderList: [AllTendersModel]) -> [CellInfo] {
        var array = [CellInfo]()
        //pending Tender Cell
        for i in 0..<tenderList.count {
            let tender = tenderList[i]
            var pendingTenderCellInfo = [String: AnyObject]()
            pendingTenderCellInfo[Constants.UIKeys.pendingStatus] = TenderStatus(rawValue: Helper.toInt(tender.status)) as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.locationDateInfo1] = tender.requestedDate as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.locationDateInfo2] = tender.requestedTime as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.timerInfo1] = tender.timeLeft() as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.id] = tender.id as AnyObject
            
            var cellHeight = CGFloat()
            switch TenderStatus(rawValue: Helper.toInt(tender.status)) {
            case .inProgress, .bidAccepted:
                cellHeight = Constants.CellHeightConstants.height_240
            case .noBidAccepted, .allBidsRejected:
                cellHeight = Constants.CellHeightConstants.height_260
            case .completedTenders:
                cellHeight = Constants.CellHeightConstants.height_121
            default:
                break
            }
            let pendingTenderCell = CellInfo(cellType: .TenderStatusDetailCell, placeHolder: "", value: "", info: pendingTenderCellInfo, height: cellHeight)
            array.append(pendingTenderCell)
        }
        
        return array
    }
    
    func requestAuctionLeaderboardAPI(completion: @escaping (Int, Int, [DLeaderBoardModel]) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getAuctionLeaderBoard)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    /*let nextPageNumber = result[kNextPageNum] as? Int,*/
                    /*let perPage = result[kPerPage] as? Int,*/
                    let resultData = result.toJSONData(),
                    let tenderList = DLeaderBoardList(jsonData: resultData) {
                    completion(1,10,tenderList.leaderBoard ?? [])
                }
            }
        }
    }
    
    func requestFleetLeaderboardAPI(completion: @escaping (Int, Int, [DLeaderBoardModel]) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getFleetLeaderBoard)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    /*let nextPageNumber = result[kNextPageNum] as? Int,*/
                    /*let perPage = result[kPerPage] as? Int,*/
                    let resultData = result.toJSONData(),
                    let tenderList = DLeaderBoardList(jsonData: resultData) {
                    completion(1,10,tenderList.leaderBoard ?? [])
                }
            }
        }
    }
    
    func requestLeadsLeaderboardAPI(completion: @escaping (Int, Int, [DLeaderBoardModel]) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getLeadsLeaderBoard)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    /*let nextPageNumber = result[kNextPageNum] as? Int,*/
                    /*let perPage = result[kPerPage] as? Int,*/
                    let resultData = result.toJSONData(),
                    let tenderList = DLeaderBoardList(jsonData: resultData) {
                    completion(1,10,tenderList.leaderBoard ?? [])
                }
            }
        }
    }
}
