//
//  DBrandFleetListViewC+TableView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 26/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension DBrandFleetListViewC: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.fleetsDataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return getCell(tableView, indexPath: indexPath)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return Constants.CellHeightConstants.height_115
  }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      guard let lastVisibleRow = self.fleetBrandTableView.indexPathsForVisibleRows?.last?.row else { return }
      if (self.fleetsDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
          self.requestFleetAPI()
      }
    }
}

extension DBrandFleetListViewC {
  func getCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
    let cell: FleetBrandListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
    let fleet = self.fleetsDataSource[indexPath.row]
    cell.delegate = self
    cell.configureView(data: fleet)
    return cell
  }
}

//MARK: - FleetBrandListCellDelegate
extension DBrandFleetListViewC: FleetBrandListCellDelegate {
  func didTapStartBidding(cell: FleetBrandListCell) {
    guard let indexPath = self.fleetBrandTableView.indexPath(for: cell) else { return }
    let fleet = self.fleetsDataSource[indexPath.row]
    let fleetBidingListVC = DIConfigurator.sharedInstance.getDFleetBidingListVC()
    fleetBidingListVC.fleet = fleet
    self.navigationController?.pushViewController(fleetBidingListVC, animated: true)
  }
}
