//
//  DBrandFleetListViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 26/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DBrandFleetListViewC: BaseViewC {
  
  //MARK: - IBOutlets
  @IBOutlet weak var fleetBrandTableView: UITableView!
    @IBOutlet weak var noDataView: UIView!
    
  //MARK: - Variables
  var viewModel: DBrandFleetListVModeling?
  var fleetsDataSource: [Fleet] = []
  var fleetCompany: FleetCompany?
  var nextPageNumber = 1
  var perPage = Constants.Validations.perPage
  
  //MARK: - LifeCycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setup()
  }
  
  //MARK: - Private Methods
  private func setup() {
    let headerTitle = self.fleetCompany?.name ?? "Fleets"
    self.setupNavigationBarTitle(title: headerTitle, barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
    self.recheckVM()
    self.setUpTableView()
    self.requestFleetAPI()
  }
  
  private func recheckVM() {
    if self.viewModel == nil {
      self.viewModel = DBrandFleetListViewM()
    }
  }
  
  private func setUpTableView() {
    self.registerNibs()
    self.fleetBrandTableView.delegate = self
    self.fleetBrandTableView.dataSource = self
    self.fleetBrandTableView.separatorStyle = .none
  }
  
  private func registerNibs() {
    self.fleetBrandTableView.register(FleetBrandListCell.self)
  }
  
  //MARK: - APIMethods
    func requestFleetAPI() {
        guard let companyId = self.fleetCompany?.id else { return }
        self.viewModel?.requestFleetAPI(companyId: companyId, page: self.nextPageNumber, completion: { (fleets, nextPageNum, perPage) in
            self.nextPageNumber = nextPageNum
            self.perPage = perPage
            self.fleetsDataSource = self.fleetsDataSource + fleets
            if self.fleetsDataSource.count == 0 {
                self.noDataView.isHidden = false
                self.fleetBrandTableView.isHidden = true
            } else {
                self.noDataView.isHidden = true
                self.fleetBrandTableView.isHidden = false
            }
            Threads.performTaskInMainQueue {
                self.fleetBrandTableView.reloadData()
            }
        })
    }
}
