//
//  FleetBrandListCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 26/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol FleetBrandListCellDelegate: class, BaseProtocol {
    func didTapStartBidding(cell: FleetBrandListCell)
}

class FleetBrandListCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var listNameLabel: UILabel!
    @IBOutlet weak var carsImageView: UIImageView!
    @IBOutlet weak var numberOfCarsLabel: UILabel!
    @IBOutlet weak var calendarImageView: UIImageView!
    @IBOutlet weak var listAddedLabel: UILabel!
    @IBOutlet weak var biddingLastDateLabel: UILabel!
    @IBOutlet weak var daysLeftLabel: UILabel!
    @IBOutlet weak var startBiddingButton: UIButton!
    
    //MARK: - Variables
    weak var delegate: FleetBrandListCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.shadowView.addShadow(Constants.UIConstants.sizeRadius_3half, shadowOpacity: Constants.UIConstants.shadowOpacity_15)
        self.containerView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.carsImageView.roundCorners(self.carsImageView.frame.size.width/2)
        self.calendarImageView.roundCorners(self.carsImageView.frame.size.width/2)
        self.startBiddingButton.setTitle("Start Bidding".localizedString(), for: .normal)
    }
    
    //MARK: - Public Methods
    func configureView(data: Fleet) {
        if let fleetName = data.name {
            let fleetNameText = String.getAttributedText(firstText: "List# :".localizedString(), secondText: " \(fleetName)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_12, secondTextSize: .size_12)
            self.listNameLabel.attributedText = fleetNameText
        }
        if let numberOfCars = data.numberOfCars {
            let numberOfCarsText = String.getAttributedText(firstText: "Numbers of Cars in List :".localizedString(), secondText: " \(numberOfCars)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.numberOfCarsLabel.attributedText = numberOfCarsText
        }
        if let createdAt = data.createdAt {
            let listAddedDate = createdAt.UTCToLocal(toFormat: Constants.Format.dateFormatdMMMyyyy)
            let listAddedDateText = String.getAttributedText(firstText: "List Added on :".localizedString(), secondText: " \(listAddedDate)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.listAddedLabel.attributedText = listAddedDateText
        }
        if let lastBiddingDate = data.lastBiddingDate {
            let listBidDate = lastBiddingDate.UTCToLocal(toFormat: Constants.Format.dateFormatdMMMyyyy)
            let lastBiddingDateText = String.getAttributedText(firstText: "Last Date of Bidding :".localizedString(), secondText: " \(listBidDate)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.biddingLastDateLabel.attributedText = lastBiddingDateText
        }
        if let daysLeft = data.daysLeft {
            var daysLeftText: String = ""
            switch daysLeft {
            case 0:
                daysLeftText = ""
            case 1:
                daysLeftText = "\(daysLeft) " + "Day Left"
            default:
                daysLeftText = "\(daysLeft) " + "Days Left"
            }
            self.daysLeftLabel.text = daysLeftText
        }
    }
    
    //MARK: - IBActions
    @IBAction func tapStartBidding(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapStartBidding(cell: self)
        }
    }
}
