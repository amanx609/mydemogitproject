//
//  Fleet.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 28/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

class Fleet: Codable {
  
  //Variables
  var companyId: Int?
  var createdAt: String?
  var daysLeft: Int?
  var id: Int?
  var lastBiddingDate: String?
  var name: String?
  var numberOfCars: Int?
}
