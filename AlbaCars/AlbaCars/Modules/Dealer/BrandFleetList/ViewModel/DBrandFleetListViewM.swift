//
//  DFleetBrandViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 27/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

protocol DBrandFleetListVModeling: BaseVModeling {
    func requestFleetAPI(companyId: Int, page: Int, completion: @escaping ([Fleet], Int, Int) -> Void)
}

class DBrandFleetListViewM: BaseViewM, DBrandFleetListVModeling {
    func requestFleetAPI(companyId: Int, page: Int, completion: @escaping ([Fleet], Int, Int) -> Void) {
        let sellCarListParam = getAPIParams(page: page, companyId: companyId)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .fleet(param: sellCarListParam))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject], let data = result[kData] as? [[String: AnyObject]], let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let fleetData = data.toJSONData() {
                    print(result)
                    do {
                        let decoder = JSONDecoder()
                        let fleets = try decoder.decode([Fleet].self, from: fleetData)
                        completion(fleets, nextPageNumber, perPage)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
    private func getAPIParams(page: Int, companyId: Int) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.page] = page as AnyObject
        param[ConstantAPIKeys.companyId] = companyId as AnyObject
        return param
    }
}
