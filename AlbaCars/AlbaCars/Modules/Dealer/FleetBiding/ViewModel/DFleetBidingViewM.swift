//
//  FleetBidingViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 26/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

protocol DFleetBidingVModeling: BaseVModeling {
    func requestFleetCompanyAPI(page: Int, completion: @escaping (Int, Int, [FleetCompany]) -> Void)
}

class DFleetBidingViewM: BaseViewM, DFleetBidingVModeling {
  func requestFleetCompanyAPI(page: Int, completion: @escaping (Int, Int, [FleetCompany]) -> Void) {
    let getFleetCompanyParam = getAPIParams(page: page)
    APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .fleetCompany(param: getFleetCompanyParam))) { (response, success) in
      if success {
        if let safeResponse =  response as? [String: AnyObject],
          let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject], let data = result[kData] as? [[String: AnyObject]], let nextPageNumber = result[kNextPageNum] as? Int, let perPage = result[kPerPage] as? Int,  let fleetCompanyData = data.toJSONData() {
          print(result)
          do {
            let decoder = JSONDecoder()
            let fleetCompanies = try decoder.decode([FleetCompany].self, from: fleetCompanyData)
            completion(nextPageNumber, perPage, fleetCompanies)
          } catch let error {
            print(error)
          }
        }
      }
    }
  }
    
    private func getAPIParams(page: Int) -> APIParams {
           var param: APIParams = APIParams()
           param[ConstantAPIKeys.page] = page as AnyObject
           return param
       }

}
