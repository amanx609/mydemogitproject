//
//  FleetBidingViewC+TableView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 26/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension DFleetBidingViewC: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.fleetCompaniesDataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return getCell(tableView, indexPath: indexPath)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return Constants.CellHeightConstants.height_150
  }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fleetCompany = self.fleetCompaniesDataSource[indexPath.row]
        let brandFleetListVC = DIConfigurator.sharedInstance.getDBrandFleetListVC()
        brandFleetListVC.fleetCompany = fleetCompany
        self.navigationController?.pushViewController(brandFleetListVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      guard let lastVisibleRow = self.fleetsTableView.indexPathsForVisibleRows?.last?.row else { return }
      if (self.fleetCompaniesDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
          self.requestFleetCompanyAPI()
      }
    }
}

extension DFleetBidingViewC {
  func getCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
    let cell: FleetListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
    let fleetCompany = self.fleetCompaniesDataSource[indexPath.row]
    cell.delegate = self
    cell.configureView(data: fleetCompany)
    return cell
  }
}

//MARK: - FleetListCellDelegate
extension DFleetBidingViewC: FleetListCellDelegate {
  func didTapViewFleets(cell: FleetListCell) {
    //AlsoAddedTheFlowOn - didSelectRow
    guard let indexPath = self.fleetsTableView.indexPath(for: cell) else { return }
    let fleetCompany = self.fleetCompaniesDataSource[indexPath.row]
    let brandFleetListVC = DIConfigurator.sharedInstance.getDBrandFleetListVC()
    brandFleetListVC.fleetCompany = fleetCompany
    self.navigationController?.pushViewController(brandFleetListVC, animated: true)
  }
}
