//
//  FleetsListCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 26/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol FleetListCellDelegate: class, BaseProtocol {
  func didTapViewFleets(cell: FleetListCell)
}

class FleetListCell: BaseTableViewCell, NibLoadableView, ReusableView {
  
  //MARK: - IBOutlets
  @IBOutlet weak var shadowView: UIView!
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var brandImageView: UIImageView!
  @IBOutlet weak var brandNameLabel: UILabel!
  @IBOutlet weak var liveFleetsLabel: UILabel!
  @IBOutlet weak var totalCarsLabel: UILabel!
  @IBOutlet weak var viewFleetsButton: UIButton!
  
  //MARK: - Variables
  weak var delegate: FleetListCellDelegate?
  
  //MARK: - LifeCycle Methods
  override func awakeFromNib() {
    super.awakeFromNib()
    self.setup()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  //MARK: - Private Methods
  private func setup() {
    self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_3half)
    self.shadowView.addShadow(Constants.UIConstants.sizeRadius_3half, shadowOpacity: Constants.UIConstants.shadowOpacity_15)
    self.containerView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
    self.viewFleetsButton.setTitle("View Fleets".localizedString(), for: .normal)
  }
  
  //MARK: - Public Methods
  func configureView(data: FleetCompany) {
    if let companyLogo = data.logo {
      self.brandImageView.setImage(urlStr: companyLogo, placeHolderImage: nil)
    }
    if let companyName = data.name {
      let companyNameText = String.getAttributedText(firstText: "Fleets By :".localizedString(), secondText: " \(companyName)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_12, secondTextSize: .size_12)
      self.brandNameLabel.attributedText = companyNameText
    }
    if let liveFleetCount = data.liveFleetCount {
      let liveFleetText = String.getAttributedText(firstText: "Fleets Live for Bidding :".localizedString(), secondText: " \(liveFleetCount)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
      self.liveFleetsLabel.attributedText = liveFleetText
    }
    if let totalCars = data.totalCars {
      let totalCarsText = String.getAttributedText(firstText: "Total Cars :".localizedString(), secondText: " \(totalCars)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
      self.totalCarsLabel.attributedText = totalCarsText
    }
  }
  
  //MARK: - IBActions
  @IBAction func tapViewFleets(_ sender: UIButton) {
    if let delegate = self.delegate {
      delegate.didTapViewFleets(cell: self)
    }
  }
}
