//
//  FleetBidingViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 26/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DFleetBidingViewC: BaseViewC {
  
  //MARK: - IBOutlets
  @IBOutlet weak var fleetsButton: UIButton!
  @IBOutlet weak var myBidsButton: UIButton!
  @IBOutlet weak var bottomMarkerView: UIView!
  @IBOutlet weak var fleetsTableView: UITableView!
  //MyBidsView
  @IBOutlet weak var myBidsView: UIView!
  @IBOutlet weak var bidsWonShadowView: UIView!
  @IBOutlet weak var bidsWonView: UIView!
  @IBOutlet weak var bidsWonLabel: UILabel!
  @IBOutlet weak var ongoingBidsShadowView: UIView!
  @IBOutlet weak var ongoingBidsView: UIView!
  @IBOutlet weak var ongoingBidsLabel: UILabel!
  @IBOutlet weak var rejectedBidsShadowView: UIView!
  @IBOutlet weak var rejectedBidsView: UIView!
  @IBOutlet weak var rejectedBidsLabel: UILabel!
  @IBOutlet weak var carsBoughtShadowView: UIView!
  @IBOutlet weak var carsBoughtView: UIView!
  @IBOutlet weak var carsBoughtLabel: UILabel!
  
  //ConstraintOutlets
  @IBOutlet weak var bottomMarkerViewWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var bottomMarkerViewLeadingConstraint: NSLayoutConstraint!
  
  
    //MARK: - Variables
    var viewModel: DFleetBidingVModeling?
    var fleetCompaniesDataSource: [FleetCompany] = []
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage

  
  //MARK: - LifeCycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setup()
  }
  
  //MARK: - Private Methods
  private func setup() {
    self.setupNavigationBarTitle(title: "Fleet Biding".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
    self.recheckVM()
    self.setupView()
    self.setUpTableView()
    self.requestFleetCompanyAPI()
  }
  
  private func recheckVM() {
    if self.viewModel == nil {
      self.viewModel = DFleetBidingViewM()
    }
  }
  
  private func setupView() {
    self.bottomMarkerViewWidthConstraint.constant = Constants.Devices.ScreenWidth/2
    self.setupMyBidsView()
    self.fleetsButton.setTitle("Fleets".localizedString(), for: .normal)
    self.myBidsButton.setTitle("My Bids".localizedString(), for: .normal)
    self.didTapButton(self.fleetsButton, isCalledOnViewDidLoad: true)
  }
  
  private func setupMyBidsView() {
    self.bidsWonLabel.text = "Bids Won".localizedString()
    self.ongoingBidsLabel.text = "Ongoing Bids".localizedString()
    self.rejectedBidsLabel.text = "Rejected Bids".localizedString()
    self.carsBoughtLabel.text = "Cars Bought".localizedString()
    self.bidsWonShadowView.roundCorners(Constants.UIConstants.sizeRadius_7)
    self.bidsWonShadowView.addShadow(Constants.UIConstants.sizeRadius_7, shadowOpacity: Constants.UIConstants.shadowOpacity_15)
    self.bidsWonView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    self.ongoingBidsShadowView.roundCorners(Constants.UIConstants.sizeRadius_7)
    self.ongoingBidsShadowView.addShadow(Constants.UIConstants.sizeRadius_7, shadowOpacity: Constants.UIConstants.shadowOpacity_15)
    self.ongoingBidsView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    self.rejectedBidsShadowView.roundCorners(Constants.UIConstants.sizeRadius_7)
    self.rejectedBidsShadowView.addShadow(Constants.UIConstants.sizeRadius_7, shadowOpacity: Constants.UIConstants.shadowOpacity_15)
    self.rejectedBidsView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    self.carsBoughtShadowView.roundCorners(Constants.UIConstants.sizeRadius_7)
    self.carsBoughtShadowView.addShadow(Constants.UIConstants.sizeRadius_7, shadowOpacity: Constants.UIConstants.shadowOpacity_15)
    self.carsBoughtView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
  }
  
  private func setUpTableView() {
      self.registerNibs()
      self.fleetsTableView.delegate = self
      self.fleetsTableView.dataSource = self
      self.fleetsTableView.separatorStyle = .none
  }
  
  private func registerNibs() {
    self.fleetsTableView.register(FleetListCell.self)
  }
  
    private func didTapButton(_ sender: UIButton, isCalledOnViewDidLoad: Bool = false) {
    var leadingConstraint: CGFloat = 0.0
    switch sender {
    case self.fleetsButton:
      //ChangeBottomMarkerConstraint
      leadingConstraint = 0.0
      //ChangeButtonTitleColor
      self.fleetsButton.setTitleColor(.white, for: .normal)
      self.myBidsButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
      //ChangeViewToBeShown
      self.fleetsTableView.isHidden = false
      self.myBidsView.isHidden = true
    case self.myBidsButton:
      //ChangeBottomMarkerConstraint
      leadingConstraint = Constants.Devices.ScreenWidth/2
      //ChangeButtonTitleColor
      self.fleetsButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
      self.myBidsButton.setTitleColor(.white, for: .normal)
      //ChangeViewToBeShown
      self.fleetsTableView.isHidden = true
      self.myBidsView.isHidden = false
    default: break
    }
        if isCalledOnViewDidLoad {
            self.bottomMarkerViewLeadingConstraint.constant = leadingConstraint
            self.view.layoutIfNeeded()
        } else {
            //AnimateTheBottomViewMarker
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseOut], animations: {
              self.bottomMarkerViewLeadingConstraint.constant = leadingConstraint
              self.view.layoutIfNeeded()
            }, completion: nil)
        }
    
  }
  
  private func moveToMyBidsScreen(bidStatus: BidStatus) {
    let myBidsVC = DIConfigurator.sharedInstance.getDMyBidsVC()
    myBidsVC.bidStatus = bidStatus
    self.navigationController?.pushViewController(myBidsVC, animated: true)
  }
  
  //MARK: - APIMethods
  func requestFleetCompanyAPI() {
    self.viewModel?.requestFleetCompanyAPI(page: self.nextPageNumber, completion: { (nextPageNum, perPage, fleetCompanies) in
      self.nextPageNumber = nextPageNum
      self.perPage = perPage
      self.fleetCompaniesDataSource = self.fleetCompaniesDataSource + fleetCompanies
      Threads.performTaskInMainQueue {
        self.fleetsTableView.reloadData()
      }
    })
  }
  
  //MARK: - IBActions
  @IBAction func tapFleetsButton(_ sender: UIButton) {
    self.didTapButton(sender)
  }
  
  @IBAction func tapMyBidsButton(_ sender: UIButton) {
    self.didTapButton(sender)
  }
  
  @IBAction func tapBidsWon(_ sender: UIButton) {
    self.moveToMyBidsScreen(bidStatus: .won)
  }
  
  @IBAction func tapOngoingBids(_ sender: UIButton) {
    self.moveToMyBidsScreen(bidStatus: .ongoing)
  }
  
  @IBAction func tapRejectedBids(_ sender: UIButton) {
    self.moveToMyBidsScreen(bidStatus: .rejected)
  }
  
  @IBAction func tapCarsBought(_ sender: UIButton) {
    self.moveToMyBidsScreen(bidStatus: .carBought)
  }
  
  @IBAction func tapViewAllBids(_ sender: UIButton) {
    self.moveToMyBidsScreen(bidStatus: .pending)
  }
}
