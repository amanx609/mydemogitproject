//
//  Fleet.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 28/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

class FleetCompany: Codable {
  
  //Variables
  var id: Int?
  var liveFleetCount: Int?
  var logo: String?
  var name: String?
  var totalCars: Int?
}
