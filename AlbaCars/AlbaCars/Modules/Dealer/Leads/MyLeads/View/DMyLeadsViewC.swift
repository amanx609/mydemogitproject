//
//  DMyLeadsViewC.swift
//  AlbaCars
//
//  Created by Narendra on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DMyLeadsViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var sellButton: UIButton!
    @IBOutlet weak var bottomMarkerView: UIView!
    @IBOutlet weak var leadsTableView: UITableView!
    
    @IBOutlet weak var contactedView: UIView!
    @IBOutlet weak var dealClosedView: UIView!
    @IBOutlet weak var notContactedView: UIView!
    @IBOutlet weak var notDealView: UIView!
    
    @IBOutlet weak var contactedLabel: UILabel!
    @IBOutlet weak var dealClosedLabel: UILabel!
    @IBOutlet weak var notContactedLabel: UILabel!
    @IBOutlet weak var noDealLabel: UILabel!
    
    //ConstraintOutlets
    //@IBOutlet weak var bottomMarkerViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomMarkerViewLeadingConstraint: NSLayoutConstraint!
    
    //MARK: - Variables
    var viewModel: DMyLeadsViewModeling?
    var filterDataSource: [CellInfo] = []
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    var leads: [Leads] = []
    var params: APIParams = APIParams()
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "My Leads".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setupView()
        self.setUpTableView()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = DMyLeadsViewM()
        }
    }
    
    private func setupView() {
        //self.bottomMarkerViewWidthConstraint.constant = Constants.Devices.ScreenWidth/2
        self.buyButton.setTitle("Looking to Buy".localizedString(), for: .normal)
        self.sellButton.setTitle("Looking to Sell".localizedString(), for: .normal)
        self.didTapButton(self.buyButton, isCalledOnViewDidLoad: true)
        
        self.contactedView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.contactedView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.contactedView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        
        self.dealClosedView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.dealClosedView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.dealClosedView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        
        self.notContactedView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.notContactedView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.notContactedView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        
        self.notDealView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.notDealView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.notDealView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.leadsTableView.delegate = self
        self.leadsTableView.dataSource = self
        self.leadsTableView.separatorStyle = .none
    }
    
    private func registerNibs() {
        self.leadsTableView.register(MyLeadsSellCell.self)
        self.leadsTableView.register(MyLeadsBuyCell.self)
        //self.leadsTableView.register(BuyLeadCell.self)
    }
    
    private func loadLeadsBuyDataSource() {
        self.leads.removeAll()
        self.nextPageNumber = 1
        Threads.performTaskInMainQueue {
            self.leadsTableView.reloadData()
        }
        self.requestLeadsStatus()
        self.requestLeadsBuyAPI()
    }
    
    private func loadLeadsSellDataSource() {
        self.leads.removeAll()
        self.nextPageNumber = 1
        Threads.performTaskInMainQueue {
            self.leadsTableView.reloadData()
        }
        
        self.requestLeadsStatus()
        self.requestLeadsSellAPI()
    }
    
    private func didTapButton(_ sender: UIButton, isCalledOnViewDidLoad: Bool = false) {
        var leadingConstraint: CGFloat = 0.0
        switch sender {
        case self.buyButton:
            //ChangeBottomMarkerConstraint
            leadingConstraint = 0.0
            //ChangeButtonTitleColor
            self.buyButton.setTitleColor(.white, for: .normal)
            self.sellButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            self.buyButton.isSelected = true
            self.sellButton.isSelected = false
            self.loadLeadsBuyDataSource()
        case self.sellButton:
            //ChangeBottomMarkerConstraint
            leadingConstraint = Constants.Devices.ScreenWidth/2
            //ChangeButtonTitleColor
            self.buyButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            self.sellButton.setTitleColor(.white, for: .normal)
            self.buyButton.isSelected = false
            self.sellButton.isSelected = true
            self.loadLeadsSellDataSource()
        default: break
        }
        if isCalledOnViewDidLoad {
            self.bottomMarkerViewLeadingConstraint.constant = leadingConstraint
            self.view.layoutIfNeeded()
        } else {
            //AnimateTheBottomViewMarker
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseOut], animations: {
                self.bottomMarkerViewLeadingConstraint.constant = leadingConstraint
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
        
        
    }
    
    func selectedLeadStatus(indexPath: IndexPath) {
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: "Leads Status".localizedString(), arrayList: LeadsStatusType.leadsStatus.dataList, key: LeadsStatusType.leadsStatus.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                // let brandId = response[ConstantAPIKeys.id] as? Int,
                let leadsStatus = response[LeadsStatusType.leadsStatus.rawValue] as? String else { return }
            sSelf.requestUpdateLeadsStatus(indexPath: indexPath, status: Helper.toString(object: response[ConstantAPIKeys.id]))
        }
        
        listPopupView.showWithAnimated(animated: true)
    }
    
    //MARK: - APIMethods
    func requestLeadsBuyAPI() {
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        
        self.viewModel?.requestGetLeadsBuyAPI(param: params, completion: { [weak self] (nextPage, perPage, leadsList) in
            guard let sSelf = self,
                let leads = leadsList.leads else { return }
            sSelf.nextPageNumber = nextPage
            sSelf.perPage = perPage
            sSelf.leads = sSelf.leads + leads
            Threads.performTaskInMainQueue {
                sSelf.leadsTableView.reloadData()
            }
        })
    }
    
    func makeExclusiveLeads(indexPath: IndexPath) {
        
        let paymentDetails = PaymentDetailsData(viewController: self, totalAmmount: Helper.toDouble(self.leads[indexPath.row].leadPrice), referenceId: Helper.toInt(self.leads[indexPath.row].leadId), paymentServiceCategoryId: PaymentServiceCategoryId.leads,vatAmmount: 0)
        
        PaymentManager.sharedInstance.initiatePayment(paymentDetailsData: paymentDetails) { (success, paymentID) in
                   
                   if success {
                       
                       var params: APIParams = APIParams()
                               
                               params[ConstantAPIKeys.leadType] = self.buyButton.isSelected == true ? 0 as AnyObject : 1  as AnyObject
                               params[ConstantAPIKeys.leadId] = self.leads[indexPath.row].leadId as AnyObject
                               params[ConstantAPIKeys.paymentId] = paymentID as AnyObject

                               self.viewModel?.requestMakeExclusiveAPI(param: params, completion: { [weak self] (status) in
                                   guard let sSelf = self else { return }
                                Threads.performTaskInMainQueue {
                                 if sSelf.buyButton.isSelected {
                                     sSelf.requestLeadsBuyAPI()
                                 } else {
                                     sSelf.requestLeadsSellAPI()
                                 }
                                    
                                }
                                 Alert.showOkAlert(title: StringConstants.Text.AppName, message: StringConstants.Text.purchasedSuccessfully.localizedString())


                               })
                   }
               }
        
    }
    
    func requestLeadsSellAPI() {
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        
        self.viewModel?.requestGetLeadsSellAPI(param: params, completion: { [weak self] (nextPage, perPage, leadsList) in
            guard let sSelf = self,
                let leads = leadsList.leads else { return }
            sSelf.nextPageNumber = nextPage
            sSelf.perPage = perPage
            sSelf.leads = sSelf.leads + leads
            Threads.performTaskInMainQueue {
                sSelf.leadsTableView.reloadData()
            }
        })
    }
    
    func requestUpdateLeadsStatus(indexPath: IndexPath, status:String) {
        
        var param = [String:AnyObject]()
        param[ConstantAPIKeys.leadType] = self.buyButton.isSelected == true ? 0 as AnyObject : 1  as AnyObject
        param[ConstantAPIKeys.vehicleId] = self.leads[indexPath.row].vehicleId as AnyObject
        param[ConstantAPIKeys.status] = status as AnyObject
        
        self.viewModel?.requestUpdateLeadsStatusAPI(param:param , completion: { [weak self] (nextPage) in
            guard let sSelf = self else { return }
            // sSelf.leads.remove(at: selectedRow)
            sSelf.requestLeadsStatus()
            sSelf.leads[indexPath.row].status = status
            Threads.performTaskInMainQueue {
                sSelf.leadsTableView.reloadData()
            }
        })
        // print(satatus)
    }
    
    func requestLeadsStatus() {
        self.contactedLabel.text = "0"
        self.notContactedLabel.text = "0"
        self.noDealLabel.text = "0"
        self.dealClosedLabel.text = "0"
        
        var param = [String:AnyObject]()
        param[ConstantAPIKeys.leadType] = self.buyButton.isSelected == true ? 0 as AnyObject : 1  as AnyObject
        self.viewModel?.requestLeadsStatusAPI(param:param , completion: { [weak self] (leadsStatus) in
            guard let sSelf = self else { return }
            sSelf.contactedLabel.text = Helper.toString(object: leadsStatus[ConstantAPIKeys.contactedCount])
            sSelf.notContactedLabel.text = Helper.toString(object: leadsStatus[ConstantAPIKeys.notContactedCount])
            sSelf.noDealLabel.text = Helper.toString(object: leadsStatus[ConstantAPIKeys.noDealCount])
            sSelf.dealClosedLabel.text = Helper.toString(object: leadsStatus[ConstantAPIKeys.dealClosedCount])
        })
    }
    
    //MARK: - IBActions
    @IBAction func tapBuyButton(_ sender: UIButton) {
        self.didTapButton(sender)
    }
    
    @IBAction func tapSellButton(_ sender: UIButton) {
        self.didTapButton(sender)
    }
    
    @IBAction func tapFilterButton(_ sender: UIButton) {
        let filterVC = DIConfigurator.sharedInstance.getDLeadsFilterVC()
        filterVC.filterType = .myLeads
        filterVC.filterDataSource = self.filterDataSource
        filterVC.selectDataSource = { [weak self] (dataSource) in
            guard let sSelf = self else {
                return
            }
            
            if let filterDataSource = dataSource {
                sSelf.filterDataSource = filterDataSource
                if let param = sSelf.viewModel?.getLeadsFilterParams(filterData: filterDataSource) {
                    sSelf.params = param
                }
            }
            
            if sSelf.buyButton.isSelected {
                sSelf.loadLeadsBuyDataSource()
            } else {
                sSelf.loadLeadsSellDataSource()
            }
        }
        
        let navC = UINavigationController(rootViewController: filterVC)
        self.present(navC, animated: true, completion: nil)
    }
    
}
