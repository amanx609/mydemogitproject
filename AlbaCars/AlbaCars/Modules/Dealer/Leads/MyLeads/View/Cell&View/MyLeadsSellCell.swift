//
//  MyLeadsSellCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol MyLeadsSellCellDelegate: class {
    func didTapLeadBuyStatusCell(cell: MyLeadsSellCell)
    func didTapMakeExclusiveLeadSellCell(cell: MyLeadsSellCell)
}

class MyLeadsSellCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var carModelNameLabel: UILabel!
    @IBOutlet weak var specsLabel: UILabel!
    @IBOutlet weak var odometerLabel: UILabel!
    @IBOutlet weak var refNoLabel: UILabel!
    @IBOutlet weak var leadAddedOnLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var mileageLabel: UILabel!
    @IBOutlet weak var engineSizeLabel: UILabel!
    @IBOutlet weak var bodyTypeLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var placeholderImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var makeExclusiveLabel: UILabel!
    @IBOutlet weak var makeExclusiveButton: UIButton!
    
    //MARK: - Variables
    weak var delegate: MyLeadsSellCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.topView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.placeholderImageView.roundCorners(Constants.UIConstants.sizeRadius_20)
        
        self.topView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.topView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        
        self.bottomView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.bottomView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.bottomView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
    }
    
    //MARK: - Public Methods
    func configureView(leads: Leads) {
        
        self.carNameLabel.text = Helper.toString(object: leads.brandName)
        self.carModelNameLabel.attributedText = String.getAttributedText(firstText: "Model : ".localizedString(), secondText:Helper.toString(object: leads.modelName), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.specsLabel.attributedText = String.getAttributedText(firstText: "Specs : ".localizedString(), secondText:Helper.toString(object: leads.specs), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.odometerLabel.attributedText = String.getAttributedText(firstText: "Odometer : ".localizedString(), secondText:leads.formattedSellMileage(), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.refNoLabel.attributedText = String.getAttributedText(firstText: "Ref. No : ".localizedString(), secondText:Helper.toString(object: leads.refNo), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.leadAddedOnLabel.attributedText = String.getAttributedText(firstText: "Lead Added on : ".localizedString(), secondText:Helper.toString(object: leads.leadsAddedOn), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.nameLabel.text = Helper.toString(object: leads.name)
        self.mobileLabel.text = Helper.toString(object: leads.mobile)
        if let image = leads.image {
            self.placeholderImageView.setImage(urlStr: Helper.toString(object:image), placeHolderImage: #imageLiteral(resourceName: "user_placeholder"))
        }
        
        self.priceLabel.text = "AED".localizedString()+" "+Helper.toString(object: leads.leadPrice)
        
        self.mileageLabel.attributedText = String.getAttributedText(firstText: "Mileage : ".localizedString(), secondText:leads.formattedSellMileage(), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        
        self.bodyTypeLabel.attributedText = String.getAttributedText(firstText: "Body Type : ".localizedString(), secondText:Helper.toString(object: leads.typeName), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.engineSizeLabel.attributedText = String.getAttributedText(firstText: "Engine Size : ".localizedString(), secondText:Helper.toString(object: leads.engineSize), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.colorLabel.attributedText = String.getAttributedText(firstText: "Color : ".localizedString(), secondText:Helper.toString(object: leads.color), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.statusLabel.text = Helper.getLeadsStatus(statusCode: Helper.toString(object: leads.status))
        if Helper.toInt(leads.isExclusive) == 0 {
            self.makeExclusiveLabel.text = "Make Exclusive".localizedString()
            self.priceLabel.isHidden = false
            self.makeExclusiveButton.isEnabled = true
            
        } else if Helper.toInt(leads.isExclusive) == 1 {
            self.makeExclusiveLabel.text = "Made Exclusive".localizedString()
            self.priceLabel.isHidden = true
            self.makeExclusiveButton.isEnabled = false
        }
        

    }
    
    //MARK: - IBActions
    
    @IBAction func statusButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapLeadBuyStatusCell(cell: self)
        }
    }
    
    @IBAction func makeExclusiveButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapMakeExclusiveLeadSellCell(cell: self)
        }
    }
    
}
