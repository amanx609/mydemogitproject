//
//  DMyLeadsViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension DMyLeadsViewC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.leads.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getCell(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let lastVisibleRow = self.leadsTableView.indexPathsForVisibleRows?.last?.row else { return }
        if (self.leads.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
            
            if self.buyButton.isSelected {
                self.requestLeadsBuyAPI()
            } else {
                self.requestLeadsSellAPI()
            }
        }
    }
}

extension DMyLeadsViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        if self.buyButton.isSelected {
            let cell: MyLeadsBuyCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(leads:self.leads[indexPath.row])
            return cell
        }
        
        let cell: MyLeadsSellCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.configureView(leads:self.leads[indexPath.row])
        return cell
    }
}


//MARK: - MyLeadsSellCellDelegate
extension DMyLeadsViewC: MyLeadsSellCellDelegate {
    
    func didTapMakeExclusiveLeadSellCell(cell: MyLeadsSellCell) {
        guard let indexPath = self.leadsTableView.indexPath(for: cell) else { return }
        self.makeExclusiveLeads(indexPath: indexPath)
    }
    
    func didTapLeadBuyStatusCell(cell: MyLeadsSellCell) {
        guard let indexPath = self.leadsTableView.indexPath(for: cell) else { return }
        self.selectedLeadStatus(indexPath: indexPath)
    }
}

//MARK: - LeadsBuyCellDelegate
extension DMyLeadsViewC: MyLeadsBuyCellDelegate {
    func didTapMakeExclusiveLeadBuyCell(cell: MyLeadsBuyCell) {
        guard let indexPath = self.leadsTableView.indexPath(for: cell) else { return }
        self.makeExclusiveLeads(indexPath: indexPath)
    }
    
    func didTapLeadSellStatusCell(cell: MyLeadsBuyCell) {
        guard let indexPath = self.leadsTableView.indexPath(for: cell) else { return }
        self.selectedLeadStatus(indexPath: indexPath)
    }
}
