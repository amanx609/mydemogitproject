//
//  DLeadsViewC.swift
//  AlbaCars
//
//  Created by Narendra on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DLeadsViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var sellButton: UIButton!
    @IBOutlet weak var bottomMarkerView: UIView!
    @IBOutlet weak var leadsTableView: UITableView!
    @IBOutlet weak var myLeadsButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    
    //ConstraintOutlets
    //@IBOutlet weak var bottomMarkerViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomMarkerViewLeadingConstraint: NSLayoutConstraint!
    
    //MARK: - Variables
    var viewModel: DLeadsViewViewModeling?
    var filterDataSource: [CellInfo] = []
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    var leads: [Leads] = []
    var params: APIParams = APIParams()
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Leads".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.recheckVM()
        self.setupView()
        self.setUpTableView()
        
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = DLeadsViewM()
        }
    }
    
    private func setupView() {
        //self.bottomMarkerViewWidthConstraint.constant = Constants.Devices.ScreenWidth/2
        self.buyButton.setTitle("Looking to Buy".localizedString(), for: .normal)
        self.sellButton.setTitle("Looking to Sell".localizedString(), for: .normal)
        self.didTapButton(self.buyButton, isCalledOnViewDidLoad: true)
        
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
            
        }
        self.myLeadsButton.setTitle("My Leads".localizedString(), for: .normal)
    }
    
    
    private func setUpTableView() {
        self.registerNibs()
        self.leadsTableView.delegate = self
        self.leadsTableView.dataSource = self
        self.leadsTableView.separatorStyle = .none
    }
    
    private func registerNibs() {
        self.leadsTableView.register(LeadsBuyCell.self)
        self.leadsTableView.register(LeadsSellCell.self)
        // self.leadsTableView.register(BuyLeadCell.self)
    }
    
    private func loadLeadsBuyDataSource() {
        self.leads.removeAll()
        self.nextPageNumber = 1
        Threads.performTaskInMainQueue {
            self.leadsTableView.reloadData()
        }
        self.requestLeadsBuyAPI()
    }
    
    private func loadLeadsSellDataSource() {
        self.leads.removeAll()
        self.nextPageNumber = 1
        Threads.performTaskInMainQueue {
            self.leadsTableView.reloadData()
        }
        self.requestLeadsSellAPI()
    }
    
    private func didTapButton(_ sender: UIButton, isCalledOnViewDidLoad: Bool = false) {
        var leadingConstraint: CGFloat = 0.0
        switch sender {
        case self.buyButton:
            //ChangeBottomMarkerConstraint
            leadingConstraint = 0.0
            //ChangeButtonTitleColor
            self.buyButton.setTitleColor(.white, for: .normal)
            self.sellButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            self.buyButton.isSelected = true
            self.sellButton.isSelected = false
            self.loadLeadsBuyDataSource()
        case self.sellButton:
            //ChangeBottomMarkerConstraint
            leadingConstraint = Constants.Devices.ScreenWidth/2
            //ChangeButtonTitleColor
            self.buyButton.setTitleColor(.unselectedButtonTitleColor, for: .normal)
            self.sellButton.setTitleColor(.white, for: .normal)
            self.buyButton.isSelected = false
            self.sellButton.isSelected = true
            self.loadLeadsSellDataSource()
        default: break
        }
        if isCalledOnViewDidLoad {
            self.bottomMarkerViewLeadingConstraint.constant = leadingConstraint
            self.view.layoutIfNeeded()
        } else {
            //AnimateTheBottomViewMarker
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseOut], animations: {
                self.bottomMarkerViewLeadingConstraint.constant = leadingConstraint
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
        
        
    }
    
    //MARK: - APIMethods
    func requestLeadsBuyAPI() {
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        
        self.viewModel?.requestGetLeadsBuyAPI(param: params, completion: { [weak self] (nextPage, perPage, leadsList) in
            guard let sSelf = self,
                let leads = leadsList.leads else { return }
            sSelf.nextPageNumber = nextPage
            sSelf.perPage = perPage
            sSelf.leads = sSelf.leads + leads
            Threads.performTaskInMainQueue {
                sSelf.leadsTableView.reloadData()
            }
        })
    }
    
    func requestLeadsSellAPI() {
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        
        self.viewModel?.requestGetLeadsSellAPI(param: params, completion: { [weak self] (nextPage, perPage, leadsList) in
            guard let sSelf = self,
                let leads = leadsList.leads else { return }
            sSelf.nextPageNumber = nextPage
            sSelf.perPage = perPage
            sSelf.leads = sSelf.leads + leads
            Threads.performTaskInMainQueue {
                sSelf.leadsTableView.reloadData()
            }
        })
    }
    
    func requestBuyAPI(selectedRow:Int) {
        
        let paymentDetails = PaymentDetailsData(viewController: self, totalAmmount: Helper.toDouble(self.leads[selectedRow].leadPrice), referenceId: Helper.toInt(self.leads[selectedRow].vehicleId), paymentServiceCategoryId: PaymentServiceCategoryId.leads,vatAmmount: 0)

        PaymentManager.sharedInstance.initiatePayment(paymentDetailsData: paymentDetails)  { (success, paymentID) in
            
            if success {
                
                var param = [String:AnyObject]()
                param[ConstantAPIKeys.leadType] = self.buyButton.isSelected == true ? 0 as AnyObject : 1  as AnyObject
                param[ConstantAPIKeys.vehicleId] = self.leads[selectedRow].vehicleId as AnyObject
                param[ConstantAPIKeys.paymentId] = paymentID as AnyObject

                self.viewModel?.requestLeadBuyAPI(param:param , completion: { [weak self] (nextPage) in
                    guard let sSelf = self else { return }
                    // sSelf.leads.remove(at: selectedRow)
                    Threads.performTaskInMainQueue {
                        sSelf.leadsTableView.reloadData()
                    }
                })
            }
        }
        
    }
    
    //MARK: - IBActions
    @IBAction func tapBuyButton(_ sender: UIButton) {
        self.didTapButton(sender)
    }
    
    @IBAction func tapSellButton(_ sender: UIButton) {
        self.didTapButton(sender)
    }
    
    @IBAction func tapFilterButton(_ sender: UIButton) {
        let filterVC = DIConfigurator.sharedInstance.getDLeadsFilterVC()
        filterVC.filterType = .leads
        filterVC.filterDataSource = self.filterDataSource
        filterVC.selectDataSource = { [weak self] (dataSource) in
            guard let sSelf = self else {
                return
            }
            
            if let filterDataSource = dataSource {
                sSelf.filterDataSource = filterDataSource
                if let param = sSelf.viewModel?.getLeadsFilterParams(filterData: filterDataSource) {
                    sSelf.params = param
                }
            }
            
            if sSelf.buyButton.isSelected {
                sSelf.loadLeadsBuyDataSource()
            } else {
                sSelf.loadLeadsSellDataSource()
            }
            
        }
        
        let navC = UINavigationController(rootViewController: filterVC)
        self.present(navC, animated: true, completion: nil)
    }
    
    @IBAction func tapMyLeadsButton(_ sender: UIButton) {
        let leadsVC = DIConfigurator.sharedInstance.getDMyLeadsVC()
        self.navigationController?.pushViewController(leadsVC, animated: true)
    }
}
