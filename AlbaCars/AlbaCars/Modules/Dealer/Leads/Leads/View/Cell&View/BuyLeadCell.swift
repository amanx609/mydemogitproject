//
//  BuyLeadCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol BuyLeadCellCellDelegate: class {
    func didTapBuyCell(cell: BuyLeadCell)
}

class BuyLeadCell: BaseTableViewCell, ReusableView, NibLoadableView  {

    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var buyButton: UIButton!

    //MARK: - Variables
    weak var delegate: BuyLeadCellCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        
    }
    
    //MARK: - IBActions
    
    @IBAction func buyButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapBuyCell(cell: self)
        }
    }
}
