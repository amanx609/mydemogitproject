//
//  LeadsSellCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit


protocol LeadsSellCellDelegate: class {
    func didTapLeadBuyCell(cell: LeadsSellCell)
}

class LeadsSellCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var carModelNameLabel: UILabel!
    @IBOutlet weak var specsLabel: UILabel!
    @IBOutlet weak var odometerLabel: UILabel!
    @IBOutlet weak var refNoLabel: UILabel!
    @IBOutlet weak var leadAddedOnLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var leadPriceLabel: UILabel!
    @IBOutlet weak var mileageLabel: UILabel!
    @IBOutlet weak var engineSizeLabel: UILabel!
    @IBOutlet weak var bodyTypeLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var placeholderImageView: UIImageView!
    
    //MARK: - Variables
    weak var delegate: LeadsSellCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.topView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.placeholderImageView.roundCorners(Constants.UIConstants.sizeRadius_20)
        
        self.topView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.topView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        
        self.bottomView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.bottomView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.bottomView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
    }
    
    //MARK: - Public Methods
    func configureView(leads: Leads) {
        self.carNameLabel.text = Helper.toString(object: leads.brandName)
        self.carModelNameLabel.attributedText = String.getAttributedText(firstText: "Model : ".localizedString(), secondText:Helper.toString(object: leads.modelName), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.specsLabel.attributedText = String.getAttributedText(firstText: "Specs : ".localizedString(), secondText:Helper.toString(object: leads.specs), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.odometerLabel.attributedText = String.getAttributedText(firstText: "Odometer : ".localizedString(), secondText:leads.formattedSellMileage(), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.refNoLabel.attributedText = String.getAttributedText(firstText: "Ref. No : ".localizedString(), secondText:Helper.toString(object: leads.refNo), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.leadAddedOnLabel.attributedText = String.getAttributedText(firstText: "Lead Added on : ".localizedString(), secondText:Helper.toString(object: leads.leadAddedOn), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)

        self.nameLabel.attributedText = String.getAttributedText(firstText: "Name : ".localizedString(), secondText:Helper.toString(object: leads.name), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.mobileLabel.attributedText = String.getAttributedText(firstText: "Mobile : ".localizedString(), secondText:Helper.toString(object: leads.mobile), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        if let image = leads.image {
            self.placeholderImageView.setImage(urlStr: Helper.toString(object:image), placeHolderImage: #imageLiteral(resourceName: "user_placeholder"))
        }
        self.leadPriceLabel.attributedText = String.getAttributedText(firstText: "Lead Price".localizedString(), secondText:leads.formattedLeadPrice(), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        
        self.mileageLabel.attributedText = String.getAttributedText(firstText: "Mileage : ".localizedString(), secondText:leads.formattedSellMileage(), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)

        self.bodyTypeLabel.attributedText = String.getAttributedText(firstText: "Body Type : ".localizedString(), secondText:Helper.toString(object: leads.typeName), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.engineSizeLabel.attributedText = String.getAttributedText(firstText: "Engine Size : ".localizedString(), secondText:Helper.toString(object: leads.engineSize), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        self.colorLabel.attributedText = String.getAttributedText(firstText: "Color : ".localizedString(), secondText:Helper.toString(object: leads.color), firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_09, secondTextSize: .size_09, fontWeight: .Medium)
        
    }
    
    //MARK: - IBActions
    
    @IBAction func buyButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapLeadBuyCell(cell: self)
        }
    }
}
