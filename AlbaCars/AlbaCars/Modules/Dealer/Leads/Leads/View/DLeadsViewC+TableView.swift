//
//  DLeadsViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension DLeadsViewC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.leads.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.getCell(tableView: tableView, indexPath: indexPath)
    }
}

extension DLeadsViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        if self.buyButton.isSelected {
            let cell: LeadsBuyCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(leads:self.leads[indexPath.row])
            return cell
        }
        
        let cell: LeadsSellCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.configureView(leads:self.leads[indexPath.row])

        return cell
        
//        let cellInfo = self.leadsDataSource[indexPath.row]
//
//        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
//        switch cellType {
//        case .LeadsBuyCell:
//            let cell: LeadsBuyCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
//            cell.delegate = self
//            return cell
//        case .LeadsSellCell:
//            let cell: LeadsSellCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
//            cell.delegate = self
//            return cell
//        default:
//            return UITableViewCell()
//        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      guard let lastVisibleRow = self.leadsTableView.indexPathsForVisibleRows?.last?.row else { return }
      if (self.leads.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
        
        if self.buyButton.isSelected {
            self.requestLeadsBuyAPI()
        } else {
            self.requestLeadsSellAPI()
        }
      }
    }
}

//MARK: - LeadsSellCellDelegate
extension DLeadsViewC: LeadsSellCellDelegate {
    func didTapLeadBuyCell(cell: LeadsSellCell) {
        guard let indexPath = self.leadsTableView.indexPath(for: cell) else { return }
        self.requestBuyAPI(selectedRow: indexPath.row)
    }    
}

//MARK: - LeadsBuyCellDelegate
extension DLeadsViewC: LeadsBuyCellDelegate {
    func didTapLeadBuyCell(cell: LeadsBuyCell) {
        guard let indexPath = self.leadsTableView.indexPath(for: cell) else { return }
        self.requestBuyAPI(selectedRow: indexPath.row)
    }
}

