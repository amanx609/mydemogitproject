//
//  LeadsModel.swift
//  AlbaCars
//
//  Created by Narendra on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

//historyDetails
class Leads: BaseCodable {
    
    //Variables
    var vehicleId: Int?
    var brand: Int?
    var model: Int?
    var type: Int?
    var maxBudget: Int?
    var minimumYear: Int?
    var mileageTo: Int?
    var mileageFrom: Int?
    var leadsAddedOn: String?
    var color: String?
    var leadPrice: Int?
    var engineSize: Double?
    var specs: String?
    var brandName: String?
    var modelName: String?
    var typeName: String?
    var name: String?
    var mobile: String?
    var engine: Double?
    var year: Int?
    var mileage: Int?
    var leadAddedOn: String?
    var refNo: Int?
    var image: String?
    
    // My leads
    var status: String?
    var brandId: Int?
    var modelId: Int?
    var typeId: Int?
    var leadId: Int?
    var isExclusive: Int?

    func formattedLeadPrice() -> String {
        return "\n"+"AED".localizedString()+" "+Helper.toString(object: leadPrice)
    }
    
    func formattedMileage() -> String {
        return Helper.toString(object: mileageFrom) + " - " + Helper.toString(object: mileageTo) + " KM".localizedString()
    }
    
    func formattedSellMileage() -> String {
        return Helper.toString(object: mileage) + " KM".localizedString()
    }
    
}

class LeadsList: BaseCodable {
    var leads: [Leads]?
    
    private enum CodingKeys: String, CodingKey {
      case leads = "data"
    }
}
