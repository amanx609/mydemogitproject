//
//  LeadsViewM.swift
//  AlbaCars
//
//  Created by Narendra on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//


import Foundation
import UIKit

protocol DLeadsViewViewModeling: BaseVModeling {
    func requestGetLeadsBuyAPI(param: APIParams, completion: @escaping (Int, Int, LeadsList) -> Void)
    func requestGetLeadsSellAPI(param: APIParams, completion: @escaping (Int, Int, LeadsList) -> Void)
    func requestLeadBuyAPI(param: APIParams, completion: @escaping (Bool) -> Void)
    func getLeadsFilterParams(filterData: [CellInfo]) -> APIParams
}

class DLeadsViewM: DLeadsViewViewModeling {
    
    func requestGetLeadsBuyAPI(param: APIParams, completion: @escaping (Int, Int, LeadsList) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getBuyLeads(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let resultData = result.toJSONData(),
                    let leadsList = LeadsList(jsonData: resultData) {
                    completion(nextPageNumber, perPage, leadsList)
                }
            }
        }
    }
    
    func requestGetLeadsSellAPI(param: APIParams, completion: @escaping (Int, Int, LeadsList) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getSellLeads(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let resultData = result.toJSONData(),
                    let leadsList = LeadsList(jsonData: resultData) {
                    completion(nextPageNumber, perPage, leadsList)
                }
            }
        }
    }
    
    func requestLeadBuyAPI(param: APIParams, completion: @escaping (Bool) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .buyLead(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                {
                    Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Lead Bought".localizedString())
                    completion(true)
                }
            }
        }
    }
    
    func getLeadsFilterParams(filterData: [CellInfo]) -> APIParams {
        var params: APIParams = APIParams()
        
        let leadStartDate = Helper.toString(object: filterData[1].info?[Constants.UIKeys.firstValue])
        if !leadStartDate.isEmpty {
            params[ConstantAPIKeys.leadStartDate] = leadStartDate as AnyObject
        }
        
        let leadEndDate = Helper.toString(object: filterData[1].info?[Constants.UIKeys.secondValue])
        if !leadEndDate.isEmpty {
            params[ConstantAPIKeys.leadEndDate] = leadEndDate as AnyObject
        }
        
        let brandIDs = Helper.toString(object: filterData[3].info?[Constants.UIKeys.id])
        if !brandIDs.isEmpty {
            params[ConstantAPIKeys.brand] = brandIDs as AnyObject
        }
        
        let modelIDs = Helper.toString(object: filterData[5].info?[Constants.UIKeys.id])
        if !modelIDs.isEmpty {
            params[ConstantAPIKeys.model] = modelIDs as AnyObject
        }
        
        let minYear = Helper.toString(object: filterData[7].value)
        if !minYear.isEmpty {
            params[ConstantAPIKeys.minYear] = minYear as AnyObject
        }
        
        let minMileage = Helper.toString(object: filterData[9].info?[Constants.UIKeys.firstValue])
        if !minMileage.isEmpty {
            params[ConstantAPIKeys.minMileage] = minMileage as AnyObject
        }
        
        let maxMileage = Helper.toString(object: filterData[9].info?[Constants.UIKeys.secondValue])
        if !maxMileage.isEmpty {
            params[ConstantAPIKeys.maxMileage] = maxMileage as AnyObject
        }
        
        let type = Helper.toString(object: filterData[11].info?[Constants.UIKeys.id])
        if !type.isEmpty {
            params[ConstantAPIKeys.type] = type as AnyObject
        }
        
        let maxBudget = Helper.toString(object: filterData[13].value)
        if !maxBudget.isEmpty {
            params[ConstantAPIKeys.type] = Helper.toDouble(maxBudget) as AnyObject
        }
        
        let cylinder = Helper.toString(object: filterData[15].value)
        if !cylinder.isEmpty {
            params[ConstantAPIKeys.cylinder] = Helper.toInt(cylinder) as AnyObject
        }        
        
        let color = Helper.toString(object: filterData[17].value)
        if !color.isEmpty {
            params[ConstantAPIKeys.cylinder] = color as AnyObject
        }
        
        return params
    }
}
