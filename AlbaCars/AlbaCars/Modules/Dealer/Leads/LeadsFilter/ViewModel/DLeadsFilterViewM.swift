//
//  DLeadsFilterViewM.swift
//  AlbaCars
//
//  Created by Narendra on 1/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DLeadsFilterViewModeling {
    func getLeadsFilterDataSource() -> [CellInfo]
    func getMyLeadsFilterDataSource() -> [CellInfo]
}

class DLeadsFilterViewM: DLeadsFilterViewModeling {
    
    func getLeadsFilterDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Lead added on
        let leadAddedTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Lead added on".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(leadAddedTitleCell)
        
        let leadAddedCell = CellInfo(cellType: .LeadAddedCell, placeHolder: "", value: "", info: [:], height: Constants.CellHeightConstants.height_80)
        array.append(leadAddedCell)
        
        //Choose Brand
        let chooseBrandTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Choose Brand".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(chooseBrandTitleCell)
        
        let chooseBrandCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "", info: [:], height: Constants.CellHeightConstants.height_80)
        array.append(chooseBrandCell)
        
        //Choose Model
        let chooseModelTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Choose Model".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(chooseModelTitleCell)
        
        let chooseModelCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "", info: [:], height: Constants.CellHeightConstants.height_80)
        array.append(chooseModelCell)
        
        //Select Minimum Year
        let selectMinimumYearTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Select Minimum Year".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(selectMinimumYearTitleCell)
        
        let selectMinimumYearCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "", info: [:], height: Constants.CellHeightConstants.height_80)
        array.append(selectMinimumYearCell)
        
        //Select Mileage
        
        let selectMileageCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Select Mileage".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(selectMileageCell)
        
        var rangeCellInfo = [String: AnyObject]()
        rangeCellInfo[Constants.UIKeys.minimum] = StringConstants.Text.minimum.localizedString() as AnyObject
        rangeCellInfo[Constants.UIKeys.maximum] = StringConstants.Text.maximum.localizedString() as AnyObject
        let mileageRangeCell = CellInfo(cellType: .MileageTextFieldCell, placeHolder: StringConstants.Text.carSelectionQuestion.localizedString(), value: "", info: rangeCellInfo, height: Constants.CellHeightConstants.height_110)
        array.append(mileageRangeCell)
        
        //Select Body Type
        let selectBodyTypeTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Select Body Type".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(selectBodyTypeTitleCell)
        
        let selectBodyTypeCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "", info: [:], height: Constants.CellHeightConstants.height_80)
        array.append(selectBodyTypeCell)
        
        //Max Budget
        let maxBudgetTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Max Budget".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(maxBudgetTitleCell)
        
        let maxBudgetCell = CellInfo(cellType: .MaxBudgetCell, placeHolder: "Monthly".localizedString(), value: "", info: [:], height: Constants.CellHeightConstants.height_80)
        array.append(maxBudgetCell)
        
        // No. Of Cylinders
        let noOfCylindersTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "No. Of Cylinders".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(noOfCylindersTitleCell)
        
        let noOfCylindersCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "", info: [:], height: Constants.CellHeightConstants.height_80)
        array.append(noOfCylindersCell)
        
        // Color
        let colorTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Color".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(colorTitleCell)
        
        let colorCell = CellInfo(cellType: .TextInputCell, placeHolder: "", value: "", info: nil, height: Constants.CellHeightConstants.height_80)
        array.append(colorCell)
        
        //Apply
        let applyCell = CellInfo(cellType: .BottomButtonCell, placeHolder: "Apply".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_130)
        array.append(applyCell)
        
        return array
    }
    
    func getMyLeadsFilterDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Leads Status
        let leadsStatusTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Leads Status".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(leadsStatusTitleCell)
        
        let leadsStatusCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "", info: [:], height: Constants.CellHeightConstants.height_80)
        array.append(leadsStatusCell)
        
        //Lead added on
        let leadAddedTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Lead added on".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(leadAddedTitleCell)
        
        let leadAddedCell = CellInfo(cellType: .LeadAddedCell, placeHolder: "", value: "", info: [:], height: Constants.CellHeightConstants.height_80)
        array.append(leadAddedCell)
        
        //Choose Brand
        let chooseBrandTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Choose Brand".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(chooseBrandTitleCell)
        
        let chooseBrandCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "", info: [:], height: Constants.CellHeightConstants.height_80)
        array.append(chooseBrandCell)
        
        //Choose Model
        let chooseModelTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Choose Model".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(chooseModelTitleCell)
        
        let chooseModelCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "", info: [:], height: Constants.CellHeightConstants.height_80)
        array.append(chooseModelCell)
        
        //Select Minimum Year
        let selectMinimumYearTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Select Minimum Year".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(selectMinimumYearTitleCell)
        
        let selectMinimumYearCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "", info: [:], height: Constants.CellHeightConstants.height_80)
        array.append(selectMinimumYearCell)
        
        //Select Mileage
        
        let selectMileageCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Select Mileage".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(selectMileageCell)
        
        var rangeCellInfo = [String: AnyObject]()
        rangeCellInfo[Constants.UIKeys.minimum] = StringConstants.Text.minimum.localizedString() as AnyObject
        rangeCellInfo[Constants.UIKeys.maximum] = StringConstants.Text.maximum.localizedString() as AnyObject
        let mileageRangeCell = CellInfo(cellType: .MileageTextFieldCell, placeHolder: StringConstants.Text.carSelectionQuestion.localizedString(), value: "", info: rangeCellInfo, height: Constants.CellHeightConstants.height_110)
        array.append(mileageRangeCell)
        
        //Select Body Type
        let selectBodyTypeTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Select Body Type".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(selectBodyTypeTitleCell)
        
        let selectBodyTypeCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "", info: [:], height: Constants.CellHeightConstants.height_80)
        array.append(selectBodyTypeCell)
        
        // No. Of Cylinders
        let noOfCylindersTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "No. Of Cylinders".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(noOfCylindersTitleCell)
        
        let noOfCylindersCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: "", info: [:], height: Constants.CellHeightConstants.height_80)
        array.append(noOfCylindersCell)
        
        // Color
        let colorTitleCell = CellInfo(cellType: .SideMenuCell, placeHolder: "", value: "Color".localizedString(), info: nil, height: Constants.CellHeightConstants.height_50)
        array.append(colorTitleCell)
        
        let colorCell = CellInfo(cellType: .TextInputCell, placeHolder: "", value: "", info: nil, height: Constants.CellHeightConstants.height_80)
        array.append(colorCell)
        
        //Apply
        let applyCell = CellInfo(cellType: .BottomButtonCell, placeHolder: "Apply".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_130)
        array.append(applyCell)
        return array
    }
    
    func getLeadsFilterParams(filterData: [CellInfo]) -> APIParams {
        var params: APIParams = APIParams()
        
//        if brand.count > 0 {
//            var brandIDs = ""
//            for vehicleBrand in brand {
//
//                if let brandIsSelected = vehicleBrand.isSelected, brandIsSelected == true {
//                    if brandIDs.isEmpty {
//                        brandIDs = Helper.toString(object: vehicleBrand.id)
//                    } else {
//                        brandIDs = brandIDs + "," + Helper.toString(object: vehicleBrand.id)
//                    }
//                }
//            }
//
//            if !brandIDs.isEmpty {
//                params[ConstantAPIKeys.brand] = brandIDs as AnyObject
//            }
//        }
        
        let leadStartDate = Helper.toString(object: filterData[1].placeHolder)
        if !leadStartDate.isEmpty {
            params[ConstantAPIKeys.leadStartDate] = leadStartDate as AnyObject
        }
        
        let leadEndDate = Helper.toString(object: filterData[1].value)
        if !leadEndDate.isEmpty {
            params[ConstantAPIKeys.leadEndDate] = leadEndDate as AnyObject
        }
        
        let minYear = Helper.toString(object: filterData[7].value)
        if !minYear.isEmpty {
            params[ConstantAPIKeys.minYear] = minYear as AnyObject
        }
        
//        if !mileage.isEmpty {
//            params[ConstantAPIKeys.mileage] = mileage as AnyObject
//        }
//
//        if !maxMonthlyInstallment.isEmpty {
//            params[ConstantAPIKeys.maxMonthlyInstallment] = maxMonthlyInstallment as AnyObject
//        }
//
//        if !maximumPrice.isEmpty {
//            params[ConstantAPIKeys.maximumPrice] = maximumPrice as AnyObject
//        }

        return params
    }
}
