//
//  DLeadsFilterViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 1/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension DLeadsFilterViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.filterDataSource[indexPath.row]
        return self.getCell(tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.filterDataSource[indexPath.row].height
    }
}

extension DLeadsFilterViewC {
    
    func getCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .SideMenuCell:
            let cell: SideMenuCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .DropDownCell:
            let cell: DropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .MileageTextFieldCell:
            let cell: MileageTextFieldCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureAuctionFilterView(cellInfo: cellInfo)
            return cell
        case .LeadAddedCell:
            let cell: LeadAddedCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
             cell.delegate = self
             cell.configureView(cellInfo: cellInfo)
            return cell
        case .MaxBudgetCell:
            let cell: MaxBudgetCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
             cell.delegate = self
             cell.configureView(cellInfo: cellInfo)
            return cell
        case .TextInputCell:
            let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .BottomButtonCell:
            let cell: BottomButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - DropDownCellDelegate
extension DLeadsFilterViewC: DropDownCellDelegate {
    func didTapDropDownCell(cell: DropDownCell) {
        guard let indexPath = self.filterTableView.indexPath(for: cell) else { return }
        if filterType == .leads {
            self.showLeadsDropDownAt(indexPath: indexPath)
        } else {
            self.showMyLeadsDropDownAt(indexPath: indexPath)
        }
    }
}
//MARK: - MaxBudgetCellDelegate
extension DLeadsFilterViewC:MaxBudgetCellDelegate {
    
    func didTapFirstDropDownCell(cell: MaxBudgetCell) {
        guard let indexPath = self.filterTableView.indexPath(for: cell) else { return }
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: DropDownType.monthlyInstallment.title, arrayList: DropDownType.monthlyInstallment.dataList, key: DropDownType.monthlyInstallment.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let monthlyInstallment = response[DropDownType.monthlyInstallment.rawValue] as? String else { return }
            var monthly = monthlyInstallment == "All".localizedString() ? "" : monthlyInstallment
            monthly = monthly.replacingOccurrences(of: ",", with: "")
            monthly = monthly.replacingOccurrences(of: " ", with: "")
            monthly = monthly.replacingOccurrences(of: "AED".localizedString(), with: "")
            sSelf.filterDataSource[indexPath.row].value = monthly
            sSelf.filterTableView.reloadData()
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    func didTapSecondDropDownCell(cell: MaxBudgetCell) {
        
        guard let indexPath = self.filterTableView.indexPath(for: cell) else { return }
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: DropDownType.maxBudgetType.title, arrayList: DropDownType.maxBudgetType.dataList, key: DropDownType.maxBudgetType.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let maxBudgetType = response[DropDownType.maxBudgetType.rawValue] as? String else { return }
            sSelf.filterDataSource[indexPath.row].placeHolder = maxBudgetType
            sSelf.filterTableView.reloadData()
        }
        listPopupView.showWithAnimated(animated: true)
    }

}

// MileageTextFieldCellDelegate
extension DLeadsFilterViewC:MileageTextFieldCellDelegate {
    func didChangeMinimumMileage(cell: MileageTextFieldCell, text: String) {
        
    }
    
    func didChangeMaximumMileage(cell: MileageTextFieldCell, text: String) {
        
    }
    
    
    func didTapMinimumMileage(cell: MileageTextFieldCell) {
        guard let indexPath = self.filterTableView.indexPath(for: cell) else { return }
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: DropDownType.minMileageRange.title, arrayList: DropDownType.minMileageRange.dataList, key: DropDownType.minMileageRange.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let minMileageRange = response[DropDownType.minMileageRange.rawValue] as? String else { return }
            // sSelf.auctionFilterDataSource[indexPath.row].value = minMileageRange
            sSelf.filterDataSource[indexPath.row].info?[Constants.UIKeys.firstValue] = minMileageRange as AnyObject
            sSelf.filterTableView.reloadData()
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    func didTapMaximumMileage(cell: MileageTextFieldCell) {
        guard let indexPath = self.filterTableView.indexPath(for: cell) else { return }
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        listPopupView.initializeViewWith(title: DropDownType.maxMileageRange.title, arrayList: DropDownType.maxMileageRange.dataList, key: DropDownType.maxMileageRange.rawValue) { [weak self] (response) in
            guard let sSelf = self,
                let maxMileageRange = response[DropDownType.maxMileageRange.rawValue] as? String else { return }
            sSelf.filterDataSource[indexPath.row].info?[Constants.UIKeys.secondValue] = maxMileageRange as AnyObject
            sSelf.filterTableView.reloadData()
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
}


//MARK: - TextInputCellDelegate
extension DLeadsFilterViewC: TextInputCellDelegate {
    func tapNextKeyboard(cell: TextInputCell) {
        self.view.endEditing(true)
    }
    
    func didChangeText(cell: TextInputCell, text: String) {
        guard let indexPath = self.filterTableView.indexPath(for: cell) else { return }
        self.filterDataSource[indexPath.row].value = text
    }
}

//MARK: - BottomButtonCellDelegate
extension DLeadsFilterViewC: BottomButtonCellDelegate {
    func didTapBottomButton(cell: BottomButtonCell) {
        self.view.endEditing(true)
        self.dismiss(animated: true)
        self.selectDataSource?(self.filterDataSource)
    }
}

//MARK: - DropDownCellDelegate
extension DLeadsFilterViewC: LeadAddedCellDelegate {
    func didTapFromDate(cell: LeadAddedCell) {
        self.fromDate = true
        self.setupDateTimePicker(cell.fromTextField)
    }
    
    func didTapToDate(cell: LeadAddedCell) {
        self.fromDate = false
        self.setupDateTimePicker(cell.toTextField)
    }
    
}
