//
//  LeadAddedCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol LeadAddedCellDelegate: class {
    func didTapFromDate(cell: LeadAddedCell)
    func didTapToDate(cell: LeadAddedCell)
}

class LeadAddedCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets

    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var toTextField: UITextField!
    
    //MARK: - Variables
    weak var delegate: LeadAddedCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        //self.firstView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.firstView.makeLayer(color: UIColor.lightGray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.secondView.makeLayer(color: UIColor.lightGray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
       // self.secondView.roundCorners(Constants.UIConstants.sizeRadius_3half)
    }
    
    //MARK: - Public Methods
     func configureView(cellInfo: CellInfo) {
         self.fromTextField.delegate = self
         self.toTextField.delegate = self
         self.fromTextField.text = cellInfo.placeHolder
         self.toTextField.text = cellInfo.value
     }
}

extension LeadAddedCell: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.fromTextField {
            if let delegate = self.delegate {
                delegate.didTapFromDate(cell: self)
            }
        } else if textField == self.toTextField {
            if let delegate = self.delegate {
                delegate.didTapToDate(cell: self)
            }
        }
    }
    
}
