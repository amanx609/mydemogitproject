//
//  MaxBudgetCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol MaxBudgetCellDelegate: class {
    func didTapFirstDropDownCell(cell: MaxBudgetCell)
    func didTapSecondDropDownCell(cell: MaxBudgetCell)
}

class MaxBudgetCell: BaseTableViewCell, ReusableView, NibLoadableView  {
    
    //MARK: - IBOutlets
    @IBOutlet weak var placeholderFirstLabel: UILabel!
    @IBOutlet weak var placeholderSecondLabel: UILabel!
    @IBOutlet weak var dropDownViewFirst: UIView!
    @IBOutlet weak var dropDownViewSecond: UIView!
    
    //MARK: - Variables
    weak var delegate: MaxBudgetCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        //self.firstView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.dropDownViewFirst.makeLayer(color: UIColor.lightGray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.dropDownViewSecond.makeLayer(color: UIColor.lightGray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
       // self.secondView.roundCorners(Constants.UIConstants.sizeRadius_3half)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.placeholderSecondLabel.text = cellInfo.placeHolder
        
        if let value = cellInfo.value, !value.isEmpty {
         self.placeholderFirstLabel.text = "AED".localizedString() + " \(value)"
        }
       //
    }
    
    //MARK: - IBActions
    
    @IBAction func dropDownFirstButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapFirstDropDownCell(cell: self)
        }
    }
    
    @IBAction func dropDownSecondButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapSecondDropDownCell(cell: self)
        }
    }
}
