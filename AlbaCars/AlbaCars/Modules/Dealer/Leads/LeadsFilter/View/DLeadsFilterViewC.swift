//
//  DLeadsFilterViewC.swift
//  AlbaCars
//
//  Created by Narendra on 1/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

enum LeadsFilterType {
    case none
    case leads
    case myLeads
}

class DLeadsFilterViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var filterTableView: UITableView!
    @IBOutlet var dateTimePicker: UIDatePicker!
    
    //MARK: - Variables
    var viewModel: DLeadsFilterViewModeling?
    var filterDataSource: [CellInfo] = []
    var filterType: LeadsFilterType = .none
    var selectDataSource:(([CellInfo]?)-> Void)?
    var fromDate =  true
    var vehicleBrandDataSource: [[String: AnyObject]] = []
    var vehicleModelDataSource: [[String: AnyObject]] = []
    var vehicleTypeDataSource: [[String: AnyObject]] = []
    var addCarViewModel: CAddNewCarViewModeling?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setup()
        
    }
    
    override func navigationButtonTapped(_ sender: AnyObject) {
        guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
        switch buttonType {
        case .cross:
            self.dismiss(animated: true, completion: nil)
        case .reset:
            self.resetFilter()
        default: break
        }
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Filter".localizedString(), leftBarButtonsType: [.cross], rightBarButtonsType: [.reset])
        self.recheckVM()
        self.setUpTableView()
        self.loadDataSource()
        self.requestVehicleBrandAPI()
        self.requestVehicleTypeAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = DLeadsFilterViewM()
        }
        
        if self.addCarViewModel == nil {
            self.addCarViewModel = CAddNewCarVM()
        }
    }
    
    private func setUpTableView() {
        self.registerNibs()
        self.filterTableView.delegate = self
        self.filterTableView.dataSource = self
        self.filterTableView.separatorStyle = .none
        self.filterTableView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.filterTableView.register(LeadAddedCell.self)
        self.filterTableView.register(MileageTextFieldCell.self)
        self.filterTableView.register(SideMenuCell.self)
        self.filterTableView.register(DropDownCell.self)
        self.filterTableView.register(MaxBudgetCell.self)
        self.filterTableView.register(TextInputCell.self)
        self.filterTableView.register(BottomButtonCell.self)
    }
    
    private func resetFilter() {
        
        if filterType == .leads {
            if let dataSource = self.viewModel?.getLeadsFilterDataSource() {
                self.filterDataSource = dataSource
                Threads.performTaskInMainQueue {
                    self.filterTableView.reloadData()
                }
            }
        } else {
            if let dataSource = self.viewModel?.getMyLeadsFilterDataSource() {
                self.filterDataSource = dataSource
                Threads.performTaskInMainQueue {
                    self.filterTableView.reloadData()
                }
            }
        }
        
        
    }
    
    func loadDataSource() {
        
        if filterType == .leads {
            if let dataSource = self.viewModel?.getLeadsFilterDataSource() {
                if self.filterDataSource.count == 0 {
                    self.filterDataSource = dataSource
                }
                Threads.performTaskInMainQueue {
                    self.filterTableView.reloadData()
                }
            }
        } else {
            if let dataSource = self.viewModel?.getMyLeadsFilterDataSource() {
                if self.filterDataSource.count == 0 {
                    self.filterDataSource = dataSource
                }
                Threads.performTaskInMainQueue {
                    self.filterTableView.reloadData()
                }
            }
        }
        
    }
    
    //MARK: - Public Methods
    func setupDateTimePicker(_ textField: UITextField) {
        textField.inputView = self.dateTimePicker
        textField.inputAccessoryView = Helper.getDoubleButtonToolBar(leftButtonTitle: StringConstants.Text.Cancel.localizedString(), rightButtonTitle: StringConstants.Text.done.localizedString(), leftButtonselector: #selector(self.cancelButtonTapped), rightButtonSelector: #selector(self.doneButtonTapped), target: self)
        self.dateTimePicker.datePickerMode = .date
        // self.dateTimePicker.minimumDate = Date()
    }
    
    //MARK: - Selectors
    @objc func cancelButtonTapped() {
        self.view.endEditing(true)
    }
    
    @objc func doneButtonTapped() {
        self.view.endEditing(true)
        let showDate = self.dateTimePicker.date.dateStringWith(strFormat: Constants.Format.dateFormatdMMMyyyy)
        let selectedDate = self.dateTimePicker.date.dateStringWith(strFormat: Constants.Format.dateFormatyyyyMMdd)

        //dateFormatyyyyMMdd
        if filterType == .leads {
            if self.fromDate {
                self.filterDataSource[1].placeHolder = showDate
                self.filterDataSource[1].info?[Constants.UIKeys.firstValue] = selectedDate as AnyObject
            } else {
                self.filterDataSource[1].value = showDate
                self.filterDataSource[1].info?[Constants.UIKeys.secondValue] = selectedDate as AnyObject
            }
        } else {
            if self.fromDate {
                self.filterDataSource[3].placeHolder = showDate
                self.filterDataSource[3].info?[Constants.UIKeys.firstValue] = selectedDate as AnyObject
            } else {
                self.filterDataSource[3].value = showDate
                self.filterDataSource[3].info?[Constants.UIKeys.secondValue] = selectedDate as AnyObject
            }
        }
        
        Threads.performTaskInMainQueue {
            self.filterTableView.reloadData()
        }
    }
    
    func showLeadsDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        
        switch indexPath.row {
        case 3:
            if self.vehicleBrandDataSource.count == 0 {
                self.requestVehicleBrandAPI()
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Brand".localizedString(), arrayList: self.vehicleBrandDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let brandId = response[ConstantAPIKeys.id] as? Int,
                    let brandName = response[ConstantAPIKeys.title] as? String else { return }
                sSelf.requestVehicleModelAPI(brandId: brandId)
                sSelf.filterDataSource[indexPath.row].value = brandName
                sSelf.filterDataSource[indexPath.row].info?[Constants.UIKeys.id] = "\(brandId)" as AnyObject
                sSelf.filterTableView.reloadData()
            }
        case 5:
            if self.vehicleModelDataSource.count == 0 {
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Model".localizedString(), arrayList: self.vehicleModelDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let modelName = response[ConstantAPIKeys.title] as? String,
                    let modelId = response[ConstantAPIKeys.id] as? Int else { return }
                
                sSelf.filterDataSource[indexPath.row].value = modelName
                sSelf.filterDataSource[indexPath.row].info?[Constants.UIKeys.id] = "\(modelId)" as AnyObject
                sSelf.filterTableView.reloadData()
            }
        case 7:
            let yearsList = Helper.getListOfYearsFrom(2010)
            listPopupView.initializeViewWith(title: "Year".localizedString(), arrayList: yearsList, key: Constants.UIKeys.year) { [weak self] (response) in
                guard let sSelf = self,
                    let carType = response[Constants.UIKeys.year] as? String else { return }
                sSelf.filterDataSource[indexPath.row].value = carType
                sSelf.filterTableView.reloadData()
            }
        case 11:
            if self.vehicleTypeDataSource.count == 0 {
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Type".localizedString(), arrayList: self.vehicleTypeDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let carType = response[ConstantAPIKeys.title] as? String,
                    let carId = response[ConstantAPIKeys.id] as? Int else { return }
                sSelf.filterDataSource[indexPath.row].value = carType
                sSelf.filterDataSource[indexPath.row].info?[Constants.UIKeys.id] = "\(carId)" as AnyObject
                sSelf.filterTableView.reloadData()
            }
        case 15:
            listPopupView.initializeViewWith(title: DropDownType.cylinders.title, arrayList: DropDownType.cylinders.dataList, key: DropDownType.cylinders.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let cylinders = response[DropDownType.cylinders.rawValue] as? String else { return }
                sSelf.filterDataSource[indexPath.row].value = cylinders
                sSelf.filterTableView.reloadData()
            }
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    
    func showMyLeadsDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
                
        switch indexPath.row {
        case 1:
            listPopupView.initializeViewWith(title: "Leads Status".localizedString(), arrayList: LeadsStatusType.leadsStatus.dataList, key: LeadsStatusType.leadsStatus.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    // let brandId = response[ConstantAPIKeys.id] as? Int,
                let leadsStatus = response[LeadsStatusType.leadsStatus.rawValue] as? String else { return }
                sSelf.filterDataSource[indexPath.row].value = leadsStatus
                sSelf.filterDataSource[indexPath.row].info?[Constants.UIKeys.id] = Helper.toString(object: response[ConstantAPIKeys.id]) as AnyObject
                sSelf.filterTableView.reloadData()
            }
        case 5:
            if self.vehicleBrandDataSource.count == 0 {
                self.requestVehicleBrandAPI()
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Brand".localizedString(), arrayList: self.vehicleBrandDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let brandId = response[ConstantAPIKeys.id] as? Int,
                    let brandName = response[ConstantAPIKeys.title] as? String else { return }
                sSelf.requestVehicleModelAPI(brandId: brandId)
                sSelf.filterDataSource[indexPath.row].value = brandName
                sSelf.filterDataSource[indexPath.row].info?[Constants.UIKeys.id] = "\(brandId)" as AnyObject
                sSelf.filterTableView.reloadData()
            }
        case 7:
            if self.vehicleModelDataSource.count == 0 {
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Model".localizedString(), arrayList: self.vehicleModelDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let modelName = response[ConstantAPIKeys.title] as? String,
                    let modelId = response[ConstantAPIKeys.id] as? Int else { return }
                
                sSelf.filterDataSource[indexPath.row].value = modelName
                sSelf.filterDataSource[indexPath.row].info?[Constants.UIKeys.id] = "\(modelId)" as AnyObject
                sSelf.filterTableView.reloadData()
            }
        case 9:
            let yearsList = Helper.getListOfYearsFrom(2010)
            listPopupView.initializeViewWith(title: "Year".localizedString(), arrayList: yearsList, key: Constants.UIKeys.year) { [weak self] (response) in
                guard let sSelf = self,
                    let carType = response[Constants.UIKeys.year] as? String else { return }
                sSelf.filterDataSource[indexPath.row].value = carType
                sSelf.filterTableView.reloadData()
            }
        case 13:
            if self.vehicleTypeDataSource.count == 0 {
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Type".localizedString(), arrayList: self.vehicleTypeDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let carType = response[ConstantAPIKeys.title] as? String,
                    let carId = response[ConstantAPIKeys.id] as? Int else { return }
                sSelf.filterDataSource[indexPath.row].value = carType
                sSelf.filterDataSource[indexPath.row].info?[Constants.UIKeys.id] = "\(carId)" as AnyObject
                sSelf.filterTableView.reloadData()
            }
        case 15:
            listPopupView.initializeViewWith(title: DropDownType.cylinders.title, arrayList: DropDownType.cylinders.dataList, key: DropDownType.cylinders.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let cylinders = response[DropDownType.cylinders.rawValue] as? String else { return }
                sSelf.filterDataSource[indexPath.row].value = cylinders
                sSelf.filterTableView.reloadData()
            }
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    //MARK: - API Methods
    func requestVehicleBrandAPI() {
        self.addCarViewModel?.requestVehicleBrandAPI(completion: { (success, vehicleBrands) in
            if success {
                self.vehicleBrandDataSource = vehicleBrands
            }
        })
    }
    
    func requestVehicleModelAPI(brandId: Int) {
        self.addCarViewModel?.requestVehicleModelAPI(brandId: brandId, completion: { (success, vehicleModels) in
            if success {
                self.vehicleModelDataSource = vehicleModels
            }
        })
    }
    
    func requestVehicleTypeAPI() {
        self.addCarViewModel?.requestVehicleTypeAPI(completion: { (success, vehicleTypes) in
            if success {
                self.vehicleTypeDataSource = vehicleTypes
            }
        })
    }
}
