//
//  BidingSheetListCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 27/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol BidingSheetListCellDelegate: class, BaseProtocol {
  func didEnterBidAmount(cell: BidingSheetListCell, amount: String)
  func didTapSubmitBid(cell: BidingSheetListCell)
}

class BidingSheetListCell: BaseTableViewCell, NibLoadableView, ReusableView {
  
  //MARK: - IBOutlets
  @IBOutlet weak var shadowView: UIView!
  @IBOutlet weak var containerView: UIView!
  //CarInfo
  @IBOutlet weak var fleetCarNameLabel: UILabel!
  @IBOutlet weak var vinLabel: UILabel!
  @IBOutlet weak var brandLabel: UILabel!
  @IBOutlet weak var odometerLabel: UILabel!
  @IBOutlet weak var modelLabel: UILabel!
  @IBOutlet weak var specsLabel: UILabel!
  @IBOutlet weak var yearLabel: UILabel!
  @IBOutlet weak var carTypeLabel: UILabel!
  //BidNotSubmitted
  @IBOutlet weak var bidNotSubmittedView: UIView!
  @IBOutlet weak var bidAmountTextField: UITextField!
  @IBOutlet weak var submitBidButton: UIButton!
  //BidSubmitted
  @IBOutlet weak var bidSubmittedView: UIView!
  @IBOutlet weak var bidAmountLabel: UILabel!
  @IBOutlet weak var submittedButton: UIButton!
  
  //MARK: - Variables
  weak var delegate: BidingSheetListCellDelegate?
  
  //MARK: - LifeCycle Methods
  override func awakeFromNib() {
    super.awakeFromNib()
    self.setup()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  //MARK: - Private Methods
  private func setup() {
    self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_3half)
    self.shadowView.addShadow(Constants.UIConstants.sizeRadius_3half, shadowOpacity: Constants.UIConstants.shadowOpacity_15)
    self.containerView.makeLayer(color: .lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
    self.bidAmountTextField.delegate = self
    self.bidAmountTextField.inputAccessoryView = Helper.getSingleButtonToolBar(buttonTitle: StringConstants.Text.done.localizedString(), selector: #selector(tapDone), target: self)
  }
  
  private func setupViewForBidStatus(data: FleetCar, bidStatus: BidStatus) {
    switch bidStatus {
    case .pending:
      self.bidNotSubmittedView.isHidden = false
      self.bidSubmittedView.isHidden = true
      if let bidAmount = data.bidAmount, bidAmount > 0.0 {
        self.bidAmountTextField.text = "\(bidAmount)"
      }
    case .ongoing:
      self.bidNotSubmittedView.isHidden = true
      self.bidSubmittedView.isHidden = false
      if let bidAmount = data.bidAmount {
        self.bidAmountLabel.text = "AED  \(bidAmount.formattedWithSeparator)"
      }
    default:
      break
    }
  }
  
  //MARK: - Selector Methods
  @objc func tapDone() {
    self.bidAmountTextField.resignFirstResponder()
  }
  
  //MARK: - Public Methods
  func configureView(data: FleetCar) {
    if let carName = data.title {
      self.fleetCarNameLabel.text = carName
    }
    if let plateNumber = data.plateNumber {
      let plateNumberText = String.getAttributedText(firstText: "VIN :".localizedString(), secondText: " \(plateNumber)", firstTextColor: .black, secondTextColor: .greenTopHeaderColor, firstTextSize: .size_12, secondTextSize: .size_12)
      self.vinLabel.attributedText = plateNumberText
    }
    if let brandName = data.brandName {
      let brandNameText = String.getAttributedText(firstText: "Brand :".localizedString(), secondText: " \(brandName)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
      self.brandLabel.attributedText = brandNameText
    }
    if let model = data.modelName {
      let modelText = String.getAttributedText(firstText: "Model :".localizedString(), secondText: " \(model)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
      self.modelLabel.attributedText = modelText
    }
    if let modelYear = data.year {
      let modelYearText = String.getAttributedText(firstText: "Model Year :".localizedString(), secondText: " \(modelYear)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
      self.yearLabel.attributedText = modelYearText
    }
    if let odometer = data.mileage {
      let odometerText = String.getAttributedText(firstText: "Odometer :".localizedString(), secondText: " \(odometer.formattedWithSeparator) Km", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
      self.odometerLabel.attributedText = odometerText
    }
    if let specs = data.specs {
      let specsText = String.getAttributedText(firstText: "Specs :".localizedString(), secondText: " \(specs)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
      self.specsLabel.attributedText = specsText
    }
    if let carType = data.typeName {
      let carTypeText = String.getAttributedText(firstText: "Car Type :".localizedString(), secondText: " \(carType)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
      self.carTypeLabel.attributedText = carTypeText
    }
    if let bidStatusValue = data.bidStatus, let bidStatus = BidStatus(rawValue: bidStatusValue) {
      self.setupViewForBidStatus(data: data, bidStatus: bidStatus)
    }
  }
  
  //MARK: - IBActions
  @IBAction func tapSubmitBid(_ sender: UIButton) {
    if let delegate = self.delegate {
      delegate.didTapSubmitBid(cell: self)
    }
  }
}

extension BidingSheetListCell: UITextFieldDelegate {
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if let text = textField.text, let textRange = Range(range, in: text), let delegate = self.delegate {
      let finalText = text.replacingCharacters(in: textRange, with: string)
        if finalText.count > 5 {
            return false
        }
      delegate.didEnterBidAmount(cell: self, amount: finalText)
    }
    return true
  }
}
