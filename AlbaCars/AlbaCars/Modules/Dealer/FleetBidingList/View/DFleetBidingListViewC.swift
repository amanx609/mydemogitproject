//
//  DFleetBidingListViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 27/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DFleetBidingListViewC: BaseViewC {
  
  //MARK: - IBOutlets
  @IBOutlet var headerView: UIView!
  @IBOutlet weak var informationLabel: UILabel!
  @IBOutlet weak var bidingListTableView: UITableView!
    
  //MARK: - Variables
  var viewModel: DFleetBidingListVModeling?
  var fleetCarsDataSource: [FleetCar] = []
  var fleet: Fleet?
  var nextPageNumber = 1
  var perPage = Constants.Validations.perPage
  
  //MARK: - LifeCycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setup()
  }
  
  //MARK: - Private Methods
  private func setup() {
    self.setupNavigationBarTitle(title: "Bidding Sheet".localizedString(), barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
    self.recheckVM()
    self.setUpTableView()
    self.informationLabel.text = "Please enter your bids with considerations. Once Submitted you won’t be able to edit your bids.".localizedString()
    self.requestFleetCarAPI()
  }
  
  private func recheckVM() {
    if self.viewModel == nil {
      self.viewModel = DFleetBidingListViewM()
    }
  }
  
  private func setUpTableView() {
      self.registerNibs()
      self.bidingListTableView.delegate = self
      self.bidingListTableView.dataSource = self
      self.bidingListTableView.separatorStyle = .none
      self.bidingListTableView.tableHeaderView = self.headerView
  }
  
  private func registerNibs() {
    self.bidingListTableView.register(BidingSheetListCell.self)
  }
  
  //MARK: - APIMethods
  func requestFleetCarAPI() {
    guard let fleetId = self.fleet?.id else { return }
    self.viewModel?.requestFleetCarAPI(fleetId: fleetId, page: self.nextPageNumber, completion: { (fleetCars, nextPageNum, perPage) in
      self.nextPageNumber = nextPageNum
      self.perPage = perPage
      self.fleetCarsDataSource = self.fleetCarsDataSource + fleetCars
      Threads.performTaskInMainQueue {
        self.bidingListTableView.reloadData()
      }
    })
  }
  
    func requestSubmitFleetBidApi(forDataAt index: Int) {
        
        let fleetCar = self.fleetCarsDataSource[index]
                self.viewModel?.requestSubmitFleetBidAPI(fleetCar: fleetCar, completion: { (success) in
                    if success {
                        self.fleetCarsDataSource[index].bidStatus = BidStatus.ongoing.rawValue
                        Threads.performTaskInMainQueue {
                            self.bidingListTableView.reloadData()
                        }
                    }
                })

//        if let valid = self.viewModel?.validateSubmitFleetBidData(fleetCar: fleetCar), valid == true {
//
//            PaymentManager.sharedInstance.initiatePayment(viewController: self, totalAmmount: Helper.toInt(fleetCar.bidAmount), referenceId:Helper.toInt(fleetCar.fleetId), paymentServiceCategoryId: PaymentServiceCategoryId.fleetBidding) { (success, paymentID) in
//
//                if success {
//
//                }
//            }
//        }
        
    }
}
