//
//  DFleetBidingListViewC+TableView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 27/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension DFleetBidingListViewC: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.fleetCarsDataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return getCell(tableView, indexPath: indexPath)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return Constants.CellHeightConstants.height_135
  }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      guard let lastVisibleRow = self.bidingListTableView.indexPathsForVisibleRows?.last?.row else { return }
      if (self.fleetCarsDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
          self.requestFleetCarAPI()
      }
    }
}

extension DFleetBidingListViewC {
  func getCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
    let cell: BidingSheetListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
    let fleetCar = self.fleetCarsDataSource[indexPath.row]
    cell.delegate = self
    cell.configureView(data: fleetCar)
    return cell
  }
}

//MARK: - BidingSheetListCellDelegate
extension DFleetBidingListViewC: BidingSheetListCellDelegate {
  func didEnterBidAmount(cell: BidingSheetListCell, amount: String) {
    guard let indexPath = self.bidingListTableView.indexPath(for: cell),
    let bidAmount = Double(amount) else { return }
    self.fleetCarsDataSource[indexPath.row].bidAmount = bidAmount
  }
  
  func didTapSubmitBid(cell: BidingSheetListCell) {
    guard let indexPath = self.bidingListTableView.indexPath(for: cell) else { return }
    self.requestSubmitFleetBidApi(forDataAt: indexPath.row)
  }
}
