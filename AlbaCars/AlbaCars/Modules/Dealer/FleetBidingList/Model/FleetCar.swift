//
//  FleetCar.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 28/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

class FleetCar: Codable {
    
    //Variables
    var companyLogo: String?
    var companyName: String?
    var fleetName: String?
    var id: Int?
    var title: String?
    var makeId: Int?
    var modelId: Int?
    var typeId: Int?
    var plateNumber: String?
    var specs: String?
    var mileage: Int?
    var year: Int?
    var vehicleMake: String?
    var vehicleModel: String?
    var vehicleType: String?
    var bidStatus: Int?
    var bidAmount: Double?
    var winningAmount: Double?
    var fleetId: Int?
    
    //NewKeys
    var brand: Int?
    var brandName: String?
    var model: Int?
    var modelName: String?
    var type: Int?
    var typeName: String?
}
