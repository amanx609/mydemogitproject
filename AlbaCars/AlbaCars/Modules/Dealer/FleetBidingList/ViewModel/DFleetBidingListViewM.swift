//
//  DFleetBidingListViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 27/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

protocol DFleetBidingListVModeling: BaseVModeling {
  func requestFleetCarAPI(fleetId: Int, page: Int, completion: @escaping ([FleetCar], Int, Int) -> Void)
  func requestSubmitFleetBidAPI(fleetCar: FleetCar, completion: @escaping (Bool) -> Void)
  func validateSubmitFleetBidData(fleetCar: FleetCar) -> Bool
}

class DFleetBidingListViewM: BaseViewM, DFleetBidingListVModeling {
  func requestFleetCarAPI(fleetId: Int, page: Int, completion: @escaping ([FleetCar], Int, Int) -> Void) {
    let fleetCarParams = getFleetCarAPIParams(page: page, fleetId: fleetId)
   
    APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .fleetCar(param: fleetCarParams))) { (response, success) in
      if success {
        if let safeResponse =  response as? [String: AnyObject],
          let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
          let data = result[kData] as? [[String: AnyObject]],
          let nextPageNumber = result[kNextPageNum] as? Int,
          let perPage = result[kPerPage] as? Int,
          let fleetCarData = data.toJSONData() {
          print(result)
          do {
            let decoder = JSONDecoder()
            let fleetCars = try decoder.decode([FleetCar].self, from: fleetCarData)
            completion(fleetCars, nextPageNumber, perPage)
          } catch let error {
            print(error)
          }
        }
      }
    }
  }
  
  func requestSubmitFleetBidAPI(fleetCar: FleetCar, completion: @escaping (Bool) -> Void) {
    if self.validateSubmitFleetBidData(fleetCar: fleetCar) {
      let submitFleetBidParams = self.getSubmitFleetBidParams(fleetCar: fleetCar)
      APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .submitFleetBid(param: submitFleetBidParams))) { (response, success) in
        if success {
          if let safeResponse =  response as? [String: AnyObject],
            let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject] {
            completion(success)
          }
        }
      }
    }
  }
  
  //MARK: - Private Methods
  //DataValidationsBeforeAPICall
   func validateSubmitFleetBidData(fleetCar: FleetCar) -> Bool {
    var isValid = true
    if let bidAmount = fleetCar.bidAmount, bidAmount == 0.0 {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter bid amount.".localizedString())
      isValid = false
    }
    return isValid
  }
  
  private func getSubmitFleetBidParams(fleetCar: FleetCar)-> APIParams {
    var params: APIParams = APIParams()
    params[ConstantAPIKeys.fleetId] = fleetCar.fleetId as AnyObject
    params[ConstantAPIKeys.fleetCarId] = fleetCar.id as AnyObject
    params[ConstantAPIKeys.amount] = fleetCar.bidAmount as AnyObject
    return params
  }
    
    private func getFleetCarAPIParams(page: Int, fleetId: Int) -> APIParams {
        var param: APIParams = APIParams()
        param[ConstantAPIKeys.page] = page as AnyObject
        param[ConstantAPIKeys.fleetId] = fleetId as AnyObject
        return param
    }
}
