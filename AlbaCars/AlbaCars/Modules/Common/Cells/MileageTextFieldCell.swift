//
//  MileageTextFieldCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol MileageTextFieldCellDelegate: class, BaseProtocol {
    func didTapMinimumMileage(cell: MileageTextFieldCell)
    func didTapMaximumMileage(cell: MileageTextFieldCell)
    func didChangeMinimumMileage(cell:MileageTextFieldCell, text: String)
    func didChangeMaximumMileage(cell:MileageTextFieldCell, text: String)
}

class MileageTextFieldCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var minimumLabel: UILabel!
    @IBOutlet weak var minimumTextField: UITextField!
    @IBOutlet weak var maximumTextField: UITextField!
    @IBOutlet weak var maximumLabel: UILabel!
    @IBOutlet weak var minimumButton: UIButton!
    @IBOutlet weak var maximumButton: UIButton!
    
    //MARK: - Variables
    weak var delegate: MileageTextFieldCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupView() {
        minimumTextField.delegate = self
        maximumTextField.delegate = self
        minimumTextField.roundCorners(Constants.UIConstants.sizeRadius_7)
        maximumTextField.roundCorners(Constants.UIConstants.sizeRadius_7)
        minimumTextField.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        maximumTextField.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo:CellInfo) {
        self.minimumButton.isHidden = true
        self.maximumButton.isHidden = true
        if let info = cellInfo.info {
            self.minimumTextField.text = Helper.toString(object: info[Constants.UIKeys.minimumValue])
            
            self.maximumTextField.text = Helper.toString(object: info[Constants.UIKeys.maximumValue])
            
        }
    }
    
    //MARK: - Public Methods
    func configureAuctionFilterView(cellInfo: CellInfo) {
        self.minimumButton.isHidden = false
        self.maximumButton.isHidden = false
        //  self.placeholderLabel.text = cellInfo.value
        
        if let info = cellInfo.info {
            if let firstValue = info[Constants.UIKeys.firstValue] as? String {
                self.minimumTextField.text = firstValue
            }
            if let secondValue = info[Constants.UIKeys.secondValue] as? String {
                self.maximumTextField.text = secondValue
            }
        }
    }
    
    //MARK: - IBACtions
    @IBAction func tapMinimumMileage(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapMinimumMileage(cell: self)
        }
    }
    
    @IBAction func tapMaximumMileage(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapMaximumMileage(cell: self)
        }
    }
}

extension MileageTextFieldCell: UITextFieldDelegate {
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case self.minimumTextField:
            if let delegate = delegate, let text = textField.text {
                delegate.didChangeMinimumMileage(cell: self, text: text)
            }
            
        case self.maximumTextField:
            if let delegate = delegate, let text = textField.text {
                delegate.didChangeMinimumMileage(cell: self, text: text)
            }
        default:
            break
        }
        
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text), let delegate = self.delegate {
            let finalText = text.replacingCharacters(in: textRange, with: string)
            switch textField {
            case self.minimumTextField:
                delegate.didChangeMinimumMileage(cell: self, text: finalText)
            case self.maximumTextField:
                delegate.didChangeMaximumMileage(cell: self, text: finalText)
            default:
                break
                
            }
        }
        return true
    }
}
