//
//  headerCollectionCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class headerCollectionCell: BaseCollectionViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBoutlets
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var bottomMarkerLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK: - Public Methods
    
    func configureView(agencyList: [String: AnyObject], isSelected: Bool) {
        self.headerTitleLabel.text = Helper.toString(object: agencyList[ConstantAPIKeys.agency])
        
        if isSelected {
            self.headerTitleLabel.font = UIFont.font(name: .AirbnbCerealApp, weight: .Medium, size: .size_14)
            self.bottomMarkerLabel.isHidden = false
        } else {
            self.headerTitleLabel.font = UIFont.font(name: .AirbnbCerealApp, weight: .Book, size: .size_14)
            self.bottomMarkerLabel.isHidden = true
        }
        
        
    }

}
