//
//  BottomButtonCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol BottomButtonCellDelegate: class {
  func didTapBottomButton(cell: BottomButtonCell)
}

class BottomButtonCell: BaseTableViewCell, ReusableView, NibLoadableView {
  
  //MARK: - IBOutlets
  @IBOutlet weak var gradientView: UIView!
  @IBOutlet weak var buttomButton: UIButton!
  
  //MARK: - Variables
  weak var delegate: BottomButtonCellDelegate?
  
  //MARK: - LifeCycle Methods
  override func awakeFromNib() {
    super.awakeFromNib()
    self.setupView()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  //MARK: - Private Methods
  private func setupView() {
    self.buttomButton.roundCorners(Constants.UIConstants.sizeRadius_7)
    self.gradientView.roundCorners(Constants.UIConstants.sizeRadius_7)
    Threads.performTaskInMainQueue {
      self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
    }
  }
  
  //MARK: - Public Methods
  func configureView(cellInfo: CellInfo) {
    self.buttomButton.setTitle(cellInfo.placeHolder, for: .normal)
  }
  
  //MARK: - IBActions
  @IBAction func tapBottomButton(_ sender: UIButton) {
    if let delegate = self.delegate {
      delegate.didTapBottomButton(cell: self)
    }
  }
}
