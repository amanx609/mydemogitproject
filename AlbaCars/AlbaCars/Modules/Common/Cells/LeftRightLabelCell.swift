//
//  LeftRightLabelCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class LeftRightLabelCell: BaseTableViewCell, NibLoadableView, ReusableView {

    
    //MARK: - IBOutlets
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    @IBOutlet weak var bgViewleadding: NSLayoutConstraint!
    @IBOutlet weak var bgViewtrailing: NSLayoutConstraint!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo:CellInfo) {
        self.leftLabel.text = cellInfo.placeHolder
        self.rightLabel.text = cellInfo.value
    }
    
    func configureServiceHistoryView(cellInfo:CellInfo) {
        self.leftLabel.text = cellInfo.placeHolder
        self.bgViewleadding.constant = Constants.CellHeightConstants.height_30
        self.bgViewtrailing.constant = Constants.CellHeightConstants.height_30
        self.rightLabel.isHidden = true
    }
    
}
