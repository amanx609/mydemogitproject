//
//  SmartTenderCarDetailCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 13/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol STCarDetailCellDelegate: class {
    func didTapCancelButton(cell: STCarDetailCell)
}

class STCarDetailCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var timeLeftLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var carTypeLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var recoveryTypeLabel: UILabel!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var carYearLabel: UILabel!
    @IBOutlet weak var pickupLocationLabel: UILabel!
    @IBOutlet weak var pickupLocNameLabel: UILabel!
    @IBOutlet weak var dropLocationLabel: UILabel!
    @IBOutlet weak var dropLocNameLabel: UILabel!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var truckNumberLabel: UILabel!
    
    //MARK: - Variables
    weak var delegate: STCarDetailCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.cancelButton.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.pickupLocationLabel.text = "Pick Up Location".localizedString()
        self.dropLocationLabel.text = "Drop off Location".localizedString()
        self.cancelButton.setTitle("Cancel".localizedString(), for: .normal)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo, type: ChooseCarForServiceType, tenderStatus: TenderStatus) {
        
        if let info = cellInfo.info {
            
            let isSecondDetailPage = Helper.toBool(info[Constants.UIKeys.isSecondDetailPage])
            if isSecondDetailPage || tenderStatus == .completedTenders || tenderStatus == .allBidsRejected {
                self.cancelButton.isHidden = true
                self.timeLabel.isHidden = true
                self.timeLeftLabel.isHidden = true
            } else {
                self.cancelButton.isHidden = false
                self.timeLabel.isHidden = false
                self.timeLeftLabel.isHidden = false
            }
        }
        
        if let info = cellInfo.info, let apiDetails = info[Constants.UIKeys.cellInfo] as? RDBidListModel {
            self.carNameLabel.text = Helper.toString(object: apiDetails.brandName) + " \(Helper.toString(object: apiDetails.modelName))" + " \(Helper.toString(object: apiDetails.year))"
            self.brandLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.brand, secondText: Helper.toString(object: apiDetails.brandName), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
            self.carTypeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.carType, secondText: Helper.toString(object: apiDetails.typeName), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
            if type == .recoveryRequest {
                self.dateTimeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.dateText, secondText: apiDetails.getDateTimeString(), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
                self.recoveryTypeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.recoveryType, secondText: Helper.toString(object: apiDetails.recoveryType), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            } else {
                self.dateTimeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.dateText, secondText: apiDetails.getDate(), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
                self.recoveryTypeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.timeText, secondText: Helper.toString(object: apiDetails.getTime()), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            }
            
            self.truckNumberLabel.isHidden = true
            
//            if let truckNumber = apiDetails.Bids?.truckNumber as? String, !truckNumber.isEmpty {
//                self.truckNumberLabel.isHidden = false
//                self.truckNumberLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.truckNumber, secondText: Helper.toString(object:truckNumber), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
//            } else {
//                self.truckNumberLabel.isHidden = true
//            }
            
            //Setting up pickup dropOff Location Labels
            switch type {
            case .spareParts:
                self.dropLocationLabel.isHidden = true
                self.dropLocNameLabel.isHidden = true
                self.pickupLocationLabel.text = "Delivery Location".localizedString()
                self.pickupLocNameLabel.text = Helper.toString(object: apiDetails.deliveryLocation)
                break
            case .windowTinting:
                self.dropLocationLabel.isHidden = false
                self.dropLocNameLabel.isHidden = false
               // self.pickupLocationLabel.text = "Delivery Location".localizedString()
                self.pickupLocNameLabel.text = Helper.toString(object: apiDetails.pickupLocation)
                self.dropLocNameLabel.text = Helper.toString(object: apiDetails.dropoffLocation)
                break
                
            case .carDetailing:
                self.dropLocationLabel.isHidden = false
                self.dropLocNameLabel.isHidden = false
                self.pickupLocationLabel.text = "Pickup location".localizedString()
                self.pickupLocNameLabel.text = Helper.toString(object: apiDetails.pickupLocation)
                self.dropLocNameLabel.text = Helper.toString(object: apiDetails.dropoffLocation)
                break
            case .insurance, .bankValuation:
                self.locationView.isHidden = true
            default:
                self.pickupLocNameLabel.text = Helper.toString(object: apiDetails.pickupLocation)
                self.dropLocNameLabel.text = Helper.toString(object: apiDetails.dropoffLocation)
                
            }
            
            self.carModelLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.modelText, secondText: Helper.toString(object: apiDetails.modelName), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
            self.carYearLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.modelYearText, secondText: Helper.toString(object: apiDetails.year), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
            //Timer
            switch tenderStatus {
            case .upcomingTenders:
                if let timerDuration = apiDetails.startTimerDuration {
                    self.timeLabel.text = timerDuration.convertToTimerFormat()
                }
            case .inProgress, .noBidAccepted:
                if let timerDuration = apiDetails.endTimerDuration {
                    self.timeLabel.text = timerDuration.convertToTimerFormat()
                }
            default:
                break
            }
        }        
    }
    
    
    @IBAction func tapCancelButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapCancelButton(cell: self)
        }
    }
    
}
