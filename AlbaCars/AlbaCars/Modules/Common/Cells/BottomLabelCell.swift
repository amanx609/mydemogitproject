//
//  BottomTextButtonCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 10/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol BottomLabelCellDelegate: class {
  func didTapLabel(cell: BottomLabelCell)
}

class BottomLabelCell: BaseTableViewCell, ReusableView, NibLoadableView {
  
  //MARK: - IBOutlets
  @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var labelLeadingConstraint: NSLayoutConstraint!
    
  //MARK: - Variables
  weak var delegate: BottomLabelCellDelegate?
  var placeholderText = ""
  var tappableText = ""
  
  //MARK: - LifeCycle Methods
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  
  //MARK: - Public Methods
  func configureView(cellInfo: CellInfo) {
    if let info = cellInfo.info {
      if let placeholderText = info[Constants.UIKeys.placeholderText] as? String {
        self.placeholderText = placeholderText
      }
      if let tappableText = info[Constants.UIKeys.tappableText] as? String {
        self.tappableText = tappableText
      }
        //for third party warranty screen
        if let alignment = info[Constants.UIKeys.labelAlignment] as? NSTextAlignment {
            self.placeholderLabel.textAlignment = alignment
        }
        
      //SetupTextAttributes
      self.placeholderLabel.text = self.placeholderText
      let attriString = NSMutableAttributedString(string: self.placeholderText)
      let tapTextRange = (self.placeholderText as NSString).range(of: self.tappableText)
      attriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.redButtonColor, range: tapTextRange)
      self.placeholderLabel.attributedText = attriString
      self.placeholderLabel.isUserInteractionEnabled = true
      self.placeholderLabel.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))
    }
  }
    
    //MARK: - Public Methods
    func configureViewWithLightText(cellInfo: CellInfo) {
        self.placeholderLabel.text = cellInfo.placeHolder
    }
  
    func configureViewWithAttributedText(cellInfo: CellInfo) {
        let font = FontFamily.AirbnbCerealApp.fontName(wieight: .Book)
        self.placeholderLabel.text = cellInfo.placeHolder
        self.placeholderLabel.textAlignment = .left
        self.placeholderLabel.font = UIFont(name: font, size: 12.0)
    }
    
    func configureViewWithBoldText(cellInfo: CellInfo) {
        self.placeholderLabel.text = cellInfo.placeHolder
        self.placeholderLabel.textAlignment = .left
    }
    
    func configureViewForRequestForm(cellInfo: CellInfo) {
        self.labelLeadingConstraint.constant = 30
        if let info = cellInfo.info {
             if let placeholderText = info[Constants.UIKeys.placeholderText] as? String {
                self.placeholderLabel.text = placeholderText
                self.placeholderLabel.textAlignment = .left
             }
        }
    }
    
  //MARK: - Selector Methods
  @objc func tapLabel(gesture: UITapGestureRecognizer) {
    let tapTextRange = (self.placeholderText as NSString).range(of: self.tappableText)
    if gesture.didTapAttributedTextInLabel(label: self.placeholderLabel, inRange: tapTextRange),
      let delegate = self.delegate {
      delegate.didTapLabel(cell: self)
    }
  }
}
