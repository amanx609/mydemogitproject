//
//  TextViewCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import UITextView_Placeholder

protocol TextViewCellDelegate: class {
    func didChangeText(cell: TextViewCell, text: String)
}

class TextViewCell:  BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var bgView: UIView!
    
    //MARK: - Variables
    weak var delegate: TextViewCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.bgView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.textView.delegate =  self
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo:CellInfo) {
        if let info = cellInfo.info {
            if let titlePlaceholder = info[Constants.UIKeys.titlePlaceholder] as? String,
                let subTitlePlaceholder = info[Constants.UIKeys.subTitlePlaceholder] as? String {
                let placeholderText = String.getAttributedText(firstText: titlePlaceholder, secondText: " \(subTitlePlaceholder)", firstTextColor: .black, secondTextColor: .lightGray, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Book)
                self.textView.attributedPlaceholder = placeholderText
            }
        } else {
            self.textView.placeholder = cellInfo.placeHolder
            self.textView.text = cellInfo.value
        }
    }
    
    func configureViewWithBlackPlaceholder(cellInfo:CellInfo) {
        let placeholderText = NSAttributedString(string: cellInfo.placeHolder,
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.blackColor,
                     NSAttributedString.Key.font: UIFont.font(name: .AirbnbCerealApp, weight: .Book, size: .size_14)])
        self.textView.attributedPlaceholder = placeholderText
        self.textView.text = cellInfo.value
    }
}


extension TextViewCell: UITextViewDelegate {
    
    // Use this if you have a UITextView
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // get the current text, or use an empty string if that failed
        let currentText = textView.text ?? ""

        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }

        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        if let delegate = self.delegate {
            delegate.didChangeText(cell: self, text: updatedText)
        }
        return true
    }
    
}
