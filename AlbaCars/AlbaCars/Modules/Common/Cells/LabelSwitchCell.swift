//
//  LabelSwitchCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol LabelSwitchCellDelegate: class {
    func switchButtonTapped(cell: LabelSwitchCell, isActivated: Bool)
}

class LabelSwitchCell: BaseTableViewCell,ReusableView,NibLoadableView {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var switchButton: UIButton!
    @IBOutlet weak var placeHolderLabel: UILabel!
    
    //MARK: - Properties
    weak var delegate: LabelSwitchCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        placeHolderLabel.text = cellInfo.value
        if let info = cellInfo.info {
            switchButton.isSelected = Helper.toBool(info[Constants.UIKeys.switchStatus])
        }
    }
    
    //MARK: - IBACtions
    
    
    @IBAction func switchButtonTapped(_ sender: Any) {
        
        switchButton.isSelected = !switchButton.isSelected
        if let delegate = self.delegate {
            delegate.switchButtonTapped(cell: self, isActivated: switchButton.isSelected)
        }
    }
}
