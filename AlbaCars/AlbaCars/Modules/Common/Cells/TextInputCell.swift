//
//  TextInputCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import GooglePlaces

protocol TextInputCellDelegate: class {
    func tapNextKeyboard(cell: TextInputCell)
    func didChangeText(cell: TextInputCell, text: String)
}

class TextInputCell:  BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var placeholderView: UIView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var placeholderImageView: UIImageView!
    @IBOutlet weak var showPasswordButton: UIButton!

    //MARK: - Variables
    weak var delegate: TextInputCellDelegate?
    var inputType: TextInputType?
    weak var targetViewController: BaseViewC?
    
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.bgView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        inputTextField.delegate = self
        self.placeholderView.isHidden = true
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        //Placeholder
        inputTextField.setPlaceHolderColor(text: cellInfo.placeHolder, color: UIColor.blackColor)
        
        self.inputTextField.keyboardType = cellInfo.keyboardType
        self.inputTextField.isSecureTextEntry = cellInfo.isSecureText
        self.inputTextField.isUserInteractionEnabled = cellInfo.isUserInteractionEnabled
        self.inputTextField.text = cellInfo.value
        //self.placeholderView.isHidden = true
        if let info = cellInfo.info {
            //Image
            if let placeholderImage = info[Constants.UIKeys.placeholderImage] as? UIImage {
                self.placeholderView.isHidden = false
                self.placeholderImageView.image = placeholderImage
            } else {
                self.placeholderView.isHidden = true
            }
            if let inputType = info[Constants.UIKeys.inputType] as? TextInputType {
                self.inputType = inputType
            } else {
                self.placeholderView.isHidden = true
            }
        } else {
            self.placeholderView.isHidden = true
        }
    }
    
    func configureViewWithGreyPlaceholder(cellInfo: CellInfo) {
        //Placeholder
        self.inputTextField.placeholder = cellInfo.placeHolder
        self.inputTextField.keyboardType = cellInfo.keyboardType
        self.inputTextField.isSecureTextEntry = cellInfo.isSecureText
        self.inputTextField.isUserInteractionEnabled = cellInfo.isUserInteractionEnabled
        self.inputTextField.text = cellInfo.value
        //self.placeholderView.isHidden = true
        if let info = cellInfo.info {
            //Image
            if let placeholderImage = info[Constants.UIKeys.placeholderImage] as? UIImage {
                self.placeholderView.isHidden = false
                self.placeholderImageView.image = placeholderImage
            } else {
                self.placeholderView.isHidden = true
            }
            if let inputType = info[Constants.UIKeys.inputType] as? TextInputType {
                self.inputType = inputType
            }
        } else {
            self.placeholderView.isHidden = true
        }
    }
}

extension TextInputCell: UITextFieldDelegate {
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let delegate = self.delegate {
            delegate.tapNextKeyboard(cell: self)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let delegate = delegate, let text = textField.text {
            delegate.didChangeText(cell: self, text: text)
        }
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if self.inputType == .deliveryLocation {
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            self.targetViewController?.present(acController, animated: true, completion: nil)
            return false
        } else {
            return true
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text), let delegate = self.delegate {
            let finalText = text.replacingCharacters(in: textRange, with: string)
            switch inputType {
            case .name:
                if finalText.count > Constants.Validations.nameMaxLength {
                    return false
                }
                
                if finalText.count == 1,  finalText == " " {
                    return false
                }
                
            case .email:
                if finalText.count > Constants.Validations.emailMaxLength {
                    return false
                }
                
                if string == " " {
                    return false
                }
            case .password:
                
                if finalText.count > Constants.Validations.passwordMaxLength {
                    return false
                }
                
                if string == " " {
                    return false
                }
                
            case .mileage, .odometer:
                if finalText.count > Constants.Validations.mileageMaxLength {
                    return false
                }
            case .plateNumber:
                if finalText.count > Constants.Validations.plateNumMaxLength {
                    return false
                }
                
            case .expectedPrice:
                if finalText.count > Constants.Validations.priceMaxLength {
                    return false
                }
                
            case .mobile:
                if finalText.count > Constants.Validations.mobileNumberMaxLength {
                    return false
                }
                
                if string == " " {
                    return false
                }
            case .engineSize:
                if finalText.count > Constants.Validations.engineSizeMaxLength {
                    return false
                }
                
            case .modelYear:
                if finalText.count > Constants.Validations.yearLenth {
                    return false
                }
                
//                if Helper.toInt(finalText) > Helper.getCurrentYear() {
//                    return false
//                }
                
            default: break
            }
            delegate.didChangeText(cell: self, text: finalText)
        }
        return true
    }
}

extension TextInputCell: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        print(place.name)
        
        
        if let delegate = self.delegate {
            //destinationTextField.text = place.name
            //delegate.didChangeLocationText(cell: self, text: place.name ?? "", latitude: place.coordinate.latitude , longitude: place.coordinate.longitude)
        }
        
        //textField.text = place.name
        // Dismiss the GMSAutocompleteViewController when something is selected
        self.targetViewController?.dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        self.targetViewController?.dismiss(animated: true, completion: nil)
    }
    
}
