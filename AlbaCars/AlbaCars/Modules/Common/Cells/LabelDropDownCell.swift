    //
    //  LabelDropDownCell.swift
    //  AlbaCars
    //
    //  Created by Sakshi Singh on 12/11/19.
    //  Copyright © 2019 Appventurez. All rights reserved.
    //
    
    import UIKit
    
    protocol LabelDropDownCellDelegate: class {
        func didTapLabelDropDownCell(cell: LabelDropDownCell)
    }
    
    class LabelDropDownCell: BaseTableViewCell, NibLoadableView, ReusableView {
        
        //MARK: - IBOutlets
        @IBOutlet weak var placeholderLabel: UILabel!
        @IBOutlet weak var bgView: UIView!
        @IBOutlet weak var selectedOptionLabel: UILabel!
        @IBOutlet weak var placeholderView: UIView!
        
        //MARK: - Properties
        weak var delegate: LabelDropDownCellDelegate?
        
        //MARK: - LifeCycle Methods
        override func awakeFromNib() {
            super.awakeFromNib()
            self.setupView()
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
        }
        
        //MARK: - Private Methods
        private func setupView() {
            self.bgView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
            self.selectedOptionLabel.isHidden = true
        }
        
        //MARK: - Public Methods
        func configureView(cellInfo: CellInfo) {
            self.placeholderLabel.text = cellInfo.placeHolder
            self.selectedOptionLabel.text = cellInfo.value
            self.selectedOptionLabel.isHidden = true
            self.placeholderView.isHidden = true
            
            if let info = cellInfo.info {
                if let labelDropDownType = info[Constants.UIKeys.dropdownType] as? LabelDropDownType {
                    var placeHolderText = ""
                    switch labelDropDownType {
                        
                    case .brands:
                        if let brands = info[Constants.UIKeys.brands] as? [String], brands.count > 0 {
                            self.selectedOptionLabel.isHidden = false
                            for i in 0..<brands.count {
                                if !placeHolderText.isEmpty {
                                    placeHolderText.append(", \(brands[i])")
                                } else {
                                    placeHolderText.append("\(brands[i])")
                                }
                            }
                        }
                        
                        
                    case .year:
                        
                        if let years = info[Constants.UIKeys.years] as? [Int], years.count > 0 {
                            self.selectedOptionLabel.isHidden = false
                            for i in 0..<years.count {
                                if !placeHolderText.isEmpty {
                                    placeHolderText.append(", \(years[i])")
                                } else {
                                    placeHolderText.append("\(years[i])")
                                }
                                
                            }
                            
                        }
                    case .time:
                        if let time = info[Constants.UIKeys.time] as? [String], time.count > 0 {
                            self.selectedOptionLabel.isHidden = false
                            for i in 0..<time.count {
                                if !placeHolderText.isEmpty {
                                    placeHolderText.append(", \(time[i])")
                                } else {
                                    placeHolderText.append("\(time[i])")
                                }
                                
                            }
                            
                        }
                       
                        case .option:
                            if let value = info[Constants.UIKeys.value] as? String, !value.isEmpty {
                            self.selectedOptionLabel.isHidden = false
                            placeHolderText = value
                        }
                    }
                    
                    self.selectedOptionLabel.text = placeHolderText
                }
            }            
        }
        
        //MARK: - IBActions
        
        @IBAction func cellSelectionButtonTapped(_ sender: Any) {
            if let delegate = self.delegate {
                delegate.didTapLabelDropDownCell(cell: self)
            }
        }
    }
