//
//  DropDownCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol DropDownCellDelegate: class {
  func didTapDropDownCell(cell: DropDownCell)
}


class DropDownCell:  BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var cellSelectionButton: UIButton!
    @IBOutlet weak var iconImageView: UIImageView!

    //MARK: - Properties
    weak var delegate: DropDownCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupView() {
      self.bgView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
      self.placeholderLabel.text = cellInfo.value
      self.isUserInteractionEnabled = cellInfo.isUserInteractionEnabled
        
        if cellInfo.isUserInteractionEnabled == false {
            iconImageView.isHidden = true
        } else {
            iconImageView.isHidden = false
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func cellSelectionButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapDropDownCell(cell: self)
        }
    }
}
