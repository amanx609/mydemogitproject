//
//  STBidsCountCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 14/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class STBidsCountCell: BaseTableViewCell, NibLoadableView, ReusableView {

    //MARK: - IBOutlets
    @IBOutlet weak var bidsCountLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var bidAcceptedLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.bidsCountLabel.isHidden = false
        self.bidAcceptedLabel.isHidden = true
        self.priceLabel.isHidden = false
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo, type: ChooseCarForServiceType) {
        if let info = cellInfo.info {
            let isSecondDetailPage = Helper.toBool(info[Constants.UIKeys.isSecondDetailPage])
            if isSecondDetailPage {
                self.bidsCountLabel.isHidden = true
                self.bidAcceptedLabel.isHidden = false
                self.priceLabel.isHidden = true
            } else {
                self.setup()
            }
        }

        if Helper.toInt(cellInfo.value) == 0 || Helper.toInt(cellInfo.value) == 1  {
            self.bidsCountLabel.text = "\(Helper.toString(object: cellInfo.value)) Bid"
        } else {
             self.bidsCountLabel.text = "\(Helper.toString(object: cellInfo.value)) Bids"
        }
        
        
        
    }
}
