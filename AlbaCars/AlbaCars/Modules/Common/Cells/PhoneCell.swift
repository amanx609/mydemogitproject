//
//  PhoneCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/10/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol PhoneCellDelegate: class {
    func didTapCountryCode(cell: PhoneCell)
    func didChangeText(cell: PhoneCell, text: String)
}

class PhoneCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var placeholderImageView: UIView!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Variables
    weak var delegate: PhoneCellDelegate?
    
    //MARK: - Private Methods
    private func setupView() {
        self.phoneTextField.delegate = self
        self.bgView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            if let countryCodePlaceholder = info[Constants.UIKeys.countryCodePlaceholder] as? String {
                self.countryCodeLabel.text = countryCodePlaceholder
            }
        }
        phoneTextField.keyboardType = .numberPad
        phoneTextField.setPlaceHolderColor(text: cellInfo.placeHolder, color: UIColor.blackColor)
        phoneTextField.text = cellInfo.value
    }
    
    //MARK: - IBActions
    @IBAction func tapCountryCode(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapCountryCode(cell: self)
        }
    }
}

extension PhoneCell: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text), let delegate = self.delegate {
            let finalText = text.replacingCharacters(in: textRange, with: string)
            if finalText.count > Constants.Validations.mobileNumberMaxLength {
                return false
            }
            
            if string == " " {
                return false
            }
            delegate.didChangeText(cell: self, text: finalText)
        }
        return true
    }
}
