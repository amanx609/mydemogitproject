//
//  RegistrationPaymentViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/15/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol RegistrationPaymentViewModeling: BaseVModeling {
    func requestRegistrationPaymentAPI(completion: @escaping (Bool)-> Void)
}

class RegistrationPaymentViewM: RegistrationPaymentViewModeling {

//MARK: - API Methods
func requestRegistrationPaymentAPI(completion: @escaping (Bool)-> Void) {
    
       
    APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .skipRegistrationPayment)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? Int
                {
                    completion(success)
                }
            }
        }
    
    }
}


