//
//  RegistrationPaymentViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/15/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

class RegistrationPaymentViewC: BaseViewC {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var registrationPaymentTableView: TPKeyboardAvoidingTableView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var payNowButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var registrationAmountLabel: UILabel!
    //MARK: - Variables
    var viewModel: RegistrationPaymentViewModeling?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    deinit {
        print("deinit RegistrationPaymentViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupTableView()
        self.roundCorners()
        self.setupView()
        self.setupNavigationBarTitle(title: StringConstants.Text.registerationPayment.localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = RegistrationPaymentViewM()
        }
    }
    
    private func setupTableView() {
        
        self.registrationPaymentTableView.tableHeaderView = self.headerView
        self.registrationPaymentTableView.sectionHeaderHeight = 200
        self.registrationPaymentTableView.backgroundColor = UIColor.white
        self.registrationPaymentTableView.separatorStyle = .none
        self.registrationPaymentTableView.allowsSelection = false
        self.registrationPaymentTableView.bounces = false
    }
    
    private func roundCorners() {
        
        Threads.performTaskInMainQueue  {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.payNowButton.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
        }
        
    }
    
    private func setupView() {
        
        if let registrationAmount = UserDefaultsManager.sharedInstance.getIntValueFor(key: .registrationAmount) {
            self.registrationAmountLabel.text = "AED \(registrationAmount)"
        }
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        self.payNowButton.setTitle(StringConstants.Text.payNow.localizedString(), for: .normal)
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
    }
    
    private func saveUserId() {
        if let userTypeVal = UserDefaultsManager.sharedInstance.getIntValueFor(key: .userType), let userType = UserType(rawValue: userTypeVal) {
            if userType == .dealer || userType == .supplier {
                let userId = UserSession.shared.getUserId()
                UserDefaultsManager.sharedInstance.saveIntValueFor(key: .userId, value: userId)
            }
        }
    }
    
    //MARK: - API Methods
    private func callSkipRegistrationPaymentAPI() {
        self.viewModel?.requestRegistrationPaymentAPI(completion:{ (success) in
            if success {
                
                Threads.performTaskInMainQueue {
                    self.saveUserId()
                    AppDelegate.delegate.showHome()
                }
            }
        })
    }
    
    
    
    
    //MARK: - IBActions
    
    @IBAction func skipButtonTapped(_ sender: Any) {
        
        self.callSkipRegistrationPaymentAPI()
    }
    
    @IBAction func payNowButtonTapped(_ sender: Any) {
        
        let paymentDetails = PaymentDetailsData(viewController: self, totalAmmount: Helper.toDouble(UserDefaultsManager.sharedInstance.getIntValueFor(key: .registrationAmount)), referenceId: Helper.toInt(UserSession.shared.getUserId()), paymentServiceCategoryId: PaymentServiceCategoryId.registration,vatAmmount: 0)
        PaymentManager.sharedInstance.initiatePayment(paymentDetailsData: paymentDetails) { (success, paymentID) in
            
            if success {
                Threads.performTaskInMainQueue {
                    self.saveUserId()
                    AppDelegate.delegate.showHome()
                }
            }
        }
        
    }
}

