//
//  EditProfileViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import Foundation
import UIKit

extension EditProfileViewC: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.editProfileDataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellInfo = self.editProfileDataSource[indexPath.row]
    return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return self.editProfileDataSource[indexPath.row].height
  }
}

extension EditProfileViewC {
  func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
    guard let cellType = cellInfo.cellType else { return UITableViewCell() }
    switch cellType {
    case .TextInputCell:
      let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate = self
      if indexPath.row == 3 {
        self.setupDatePicker(cell.inputTextField)
      }
      cell.configureView(cellInfo: cellInfo)
      return cell
    case .PhoneCell:
      let cell: PhoneCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate = self
      cell.configureView(cellInfo: cellInfo)
      return cell
    case .BottomButtonCell:
      let cell: BottomButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
      cell.delegate = self
      cell.configureView(cellInfo: cellInfo)
      return cell
      
    default:
      return UITableViewCell()
    }
  }
}

//MARK: - BottomButtonCellDelegate
extension EditProfileViewC: BottomButtonCellDelegate {
  func didTapBottomButton(cell: BottomButtonCell) {
    self.uploadImageToS3()
  }
}

//MARK: - TextInputCellDelegate
extension EditProfileViewC: TextInputCellDelegate {
  func tapNextKeyboard(cell: TextInputCell) {
    self.view.endEditing(true)
  }
  
  func didChangeText(cell: TextInputCell, text: String) {
    guard let indexPath = self.editProfileTableView.indexPath(for: cell) else { return }
    self.editProfileDataSource[indexPath.row].value = text
  }
}

//MARK: - PhoneCell Delegate
extension EditProfileViewC: PhoneCellDelegate {
  func didTapCountryCode(cell: PhoneCell) {
    
  }
  
  func didChangeText(cell: PhoneCell, text: String) {
    guard let indexPath = self.editProfileTableView.indexPath(for: cell) else { return }
    self.editProfileDataSource[indexPath.row].value = text
  }
}
