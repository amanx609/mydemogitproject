//
//  EditProfileViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class EditProfileViewC: BaseViewC {
  
  //MARK: - IBOutlets
  
  @IBOutlet weak var editProfileTableView: UITableView!
  @IBOutlet var headerView: UIView!
  @IBOutlet var datePicker: UIDatePicker!
  @IBOutlet weak var uploadImageButton: UIButton!
  @IBOutlet weak var profileImageView: UIImageView!
  @IBOutlet weak var profileImageBgView: UIImageView!
  
  //MARK: - Variables
  
  var editProfileDataSource: [CellInfo] = []
  var viewModel: EditProfileVModeling?
  var profileImageUrl = ""
  
  
  //MARK: - LifeCycle Methods
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setup()
  }
  
  deinit {
    print("deinit EditProfileViewC")
  }
  
  //MARK: - Private Methods
  private func setup() {
    self.setupNavigationBarTitle(title: "Edit Profile".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
    self.recheckVM()
    self.setupTableView()
    self.setupImageView()
    if let dataSource = self.viewModel?.getEditProfileDataSource(){
      self.editProfileDataSource = dataSource
    }
  }
  
  private func recheckVM() {
    if self.viewModel == nil {
      self.viewModel = EditProfileViewM()
    }
  }
  
  private func setupTableView() {
    self.registerNibs()
    self.editProfileTableView.delegate = self
    self.editProfileTableView.dataSource = self
    self.editProfileTableView.separatorStyle = .none
    self.editProfileTableView.allowsSelection = false
    self.editProfileTableView.tableHeaderView = self.headerView
  }
  
  private func registerNibs() {
    self.editProfileTableView.register(TextInputCell.self)
    self.editProfileTableView.register(PhoneCell.self)
    self.editProfileTableView.register(BottomLabelCell.self)
    self.editProfileTableView.register(BottomButtonCell.self)
  }
  
  private func setupImageView() {
    self.profileImageView.layer.cornerRadius = profileImageView.frame.width / 2
    self.profileImageBgView.layer.cornerRadius = profileImageBgView.frame.width/2
    self.profileImageView.layer.masksToBounds = true
    if let userImageUrl = UserSession.shared.image {
      self.profileImageUrl = userImageUrl
      self.profileImageView.setImage(urlStr: userImageUrl, placeHolderImage:  #imageLiteral(resourceName: "avatar-placeholder"))
    }
    viewWillLayoutSubviews()
  }
  
  private func updateDateOfBirth(_ dob: String) {
    self.editProfileDataSource[3].value = dob
    self.editProfileTableView.reloadData()
  }
  
  
  
  //MARK: - Public Methods
  func setupDatePicker(_ textField: UITextField) {
    textField.inputView = self.datePicker
    textField.inputAccessoryView = Helper.getDoubleButtonToolBar(leftButtonTitle: StringConstants.Text.Cancel.localizedString(), rightButtonTitle: StringConstants.Text.done.localizedString(), leftButtonselector: #selector(self.cancelButtonTapped), rightButtonSelector: #selector(self.doneButtonTapped), target: self)
    //self.datePicker.maximumDate = Date()
    let date = Calendar.current.date(byAdding: .year, value: -13, to: Date())
    self.datePicker.maximumDate = date
  }
  
  //MARK: - API Methods
    
  func uploadImageToS3() {
     Loader.showLoader()
     guard let profileImage = profileImageView.image else { return }
     self.uploadImageButton.setTitle("", for: .normal)
     self.uploadImageButton.backgroundColor = UIColor.clear
     S3Manager.sharedInstance.uploadImage(image: profileImage, imageQuality: .medium, progress: nil) { (response, imageName, error) in
       guard let imageName = imageName as? String else { return }
       self.profileImageUrl = imageName
       Threads.performTaskInMainQueue {
         Loader.hideLoader()
         self.requestUpdateProfileAPI()
       }
     }
   }
    
  func requestUpdateProfileAPI() {
    self.viewModel?.requestUpdateProfileAPI(arrData: self.editProfileDataSource, image: self.profileImageUrl, completion: {  [weak self] (success, isMobileUpdated, user) in
      guard let sSelf = self else { return }
      Threads.performTaskInMainQueue {
        if isMobileUpdated {
          let otpVC = DIConfigurator.sharedInstance.getOTPVerificationVC()
          otpVC.user = user
          otpVC.editedPhoneNumber = sSelf.editProfileDataSource[2].value
          sSelf.navigationController?.pushViewController(otpVC, animated: true)
        } else {
          sSelf.navigationController?.popViewController(animated: true)
        }
      }
    })
  }
  
  //MARK: - IBActions
  
  @IBAction func didTapUploadImage(_ sender: Any) {
    Threads.performTaskInMainQueue {
      ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
        if let image = image {
            self.profileImageView.image = image
        }
      }
    }
  }
  
  //MARK: - Selectors
  @objc func doneButtonTapped() {
    self.view.endEditing(true)
    let selectedDate = self.datePicker.date
    let convertedDate = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatWithoutSpace)
    self.updateDateOfBirth(convertedDate)
  }
  
  @objc func cancelButtonTapped() {
    self.view.endEditing(true)
  }
}
