//
//  EditProfileViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import Foundation
import UIKit

protocol EditProfileVModeling: BaseVModeling {
  func getEditProfileDataSource() -> [CellInfo]
  func requestUpdateProfileAPI(arrData: [CellInfo], image: String, completion: @escaping (Bool, Bool, User)-> Void)
}

class EditProfileViewM: EditProfileVModeling {
  
  func getEditProfileDataSource() -> [CellInfo] {
    
    var array = [CellInfo]()
    
    //Full Name Cell
    var fullNameInfo = [String: AnyObject]()
    fullNameInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "userName")
    fullNameInfo[Constants.UIKeys.inputType] = TextInputType.name as AnyObject
    let fullNameCell = CellInfo(cellType: .TextInputCell, placeHolder: "Full Name".localizedString(), value: Helper.toString(object: UserSession.shared.name), info: fullNameInfo, height: Constants.CellHeightConstants.height_80)
    array.append(fullNameCell)
    
    //Email Cell
    var emailInfo = [String: AnyObject]()
    emailInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "email")
    emailInfo[Constants.UIKeys.inputType] = TextInputType.email as AnyObject
    let emailCell = CellInfo(cellType: .TextInputCell, placeHolder: "Email".localizedString(), value: Helper.toString(object: UserSession.shared.email), info: emailInfo, height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.emailAddress, isUserInteractionEnabled: false)
    array.append(emailCell)
    
    //Phone Cell
    var phoneInfo = [String: AnyObject]()
    phoneInfo[Constants.UIKeys.countryCodePlaceholder] = Helper.toString(object: UserSession.shared.countryCode) as AnyObject
    let phoneCell = CellInfo(cellType: .PhoneCell, placeHolder: "Phone Number".localizedString(), value: Helper.toString(object: UserSession.shared.mobile), info: phoneInfo,height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.numberPad)
    array.append(phoneCell)
    
    //Date of  Birth
    var dobInfo = [String: AnyObject]()
    dobInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "date")
    dobInfo[Constants.UIKeys.inputType] = TextInputType.dob as AnyObject

    var dateStr = Helper.toString(object:Helper.toString(object: UserSession.shared.dob).getDateStringFrom(inputFormat: Constants.Format.dateFormatyyyyMMdd, outputFormat: Constants.Format.dateFormatWithoutSpace))

    if dateStr.isEmpty {
        dateStr = Helper.toString(object:Helper.toString(object: UserSession.shared.dob).getDateStringFrom(inputFormat: Constants.Format.dateFormatdMMMyyyy, outputFormat: Constants.Format.dateFormatWithoutSpace))
    }
    
    if dateStr.isEmpty {
        dateStr = Helper.toString(object:Helper.toString(object: UserSession.shared.dob).getDateStringFrom(inputFormat: Constants.Format.editProfileDateFormat, outputFormat: Constants.Format.dateFormatWithoutSpace))
    }
    
    let dobCell = CellInfo(cellType: .TextInputCell, placeHolder: "Date of Birth".localizedString(), value: dateStr , info: dobInfo, height: Constants.CellHeightConstants.height_80)
    array.append(dobCell)
    
    //Save Profile Cell
    let saveProfileCell = CellInfo(cellType: .BottomButtonCell, placeHolder: "Save Profile".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_170)
    array.append(saveProfileCell)
    
    return array
  }
  
  //MARK: - API Methods
  func requestUpdateProfileAPI(arrData: [CellInfo], image: String, completion: @escaping (Bool, Bool, User)-> Void) {
    if self.validateUpdateProfileData(arrData: arrData) {
      let updateProfileParams = self.getUpdateProfileParams(arrData: arrData, image: image)
      APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .updateProfile(param: updateProfileParams))) { (response, success) in
        if success {
          if let safeResponse =  response as? [String: AnyObject],
            let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
            let isMobileUpdated = result[ConstantAPIKeys.isMobileUpdated] as? Bool,
            let profileData = result.toJSONData(),
            let user = User(jsonData: profileData),
            let savedUser = UserDefaultsManager.sharedInstance.getUserValueFor(key: .user) {
            let savedMobile = savedUser.mobile
            user.mobile = savedMobile //Don't update phone number until phone is verified
            UserDefaultsManager.sharedInstance.saveUserValueFor(key:.user, value: user)
            UserSession.shared.initializeValuesForUser()
            NotificationCenter.default.post(name: Notification.Name("profileImageData"), object: nil)
            completion(success, isMobileUpdated, user)
          }
        }
      }
    }
  }
  
  //MARK: - Private Methods
  //DataValidationsBeforeAPICall
  private func validateUpdateProfileData(arrData: [CellInfo]) -> Bool {
    var isValid = true
    if let name = arrData[0].value, name.isEmpty {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter name.".localizedString())
      isValid = false
    } else if let email = arrData[1].value, email.isEmpty {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter email.".localizedString())
      isValid = false
    } else if let email = arrData[1].value, !email.isValidEmail() {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter a valid email.".localizedString())
      isValid = false
    } else if let phone = arrData[2].value, phone.isEmpty {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter your phone number".localizedString())
      isValid = false
    } else if let dob = arrData[3].value, dob.isEmpty {
      Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter date of birth.".localizedString())
      isValid = false
    }
    return isValid
  }
  
  
  private func getUpdateProfileParams(arrData: [CellInfo], image: String)-> APIParams {
    var params: APIParams = APIParams()
    params[ConstantAPIKeys.name] = arrData[0].value as AnyObject
    params[ConstantAPIKeys.dobLowerCase] = arrData[3].value.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd) as AnyObject
    params[ConstantAPIKeys.image] = image as AnyObject
    params[ConstantAPIKeys.mobile] = arrData[2].value as AnyObject
    params[ConstantAPIKeys.countryCode] = UserSession.shared.countryCode as AnyObject
    var isMobileUpdated = 0
    let enteredMobile = arrData[2].value
    if let mobile = UserSession.shared.mobile, enteredMobile != mobile {
      isMobileUpdated = 1
    }
    params[ConstantAPIKeys.isMobileUpdated] = isMobileUpdated as AnyObject
    return params
  }
}
