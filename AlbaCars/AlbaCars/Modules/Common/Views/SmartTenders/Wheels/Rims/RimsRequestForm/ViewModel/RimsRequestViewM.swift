//
//  RimsRequestViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol RimsRequestVModeling: BaseVModeling {
    func getRimsTenderReqDataSource(carDetails: ChooseCarModel?) -> [CellInfo]
    func requestTenderAPI(arrData: [CellInfo], vehicleId: Int, completion: @escaping (Bool)-> Void)
}

class RimsRequestViewM: RimsRequestVModeling {
    
    func getRimsTenderReqDataSource(carDetails: ChooseCarModel?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Plate Number
        let carPlateCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.carPlate.localizedString(), value: Helper.toString(object: carDetails?.plateNumber), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carPlateCell)
        
        //Car brand
        let carBrandCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value:  Helper.toString(object: carDetails?.brandName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carBrandCell)
        
        //Car Model
        let carModelCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value:  Helper.toString(object: carDetails?.modelName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carModelCell)
        
        //Car Year
        let carYearCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.year), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carYearCell)
        
        
        //Car Type
        let carTypeCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.typeName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carTypeCell)
        
        //Preferred brand
        let rimsBrandCell = CellInfo(cellType: .TextInputCell, placeHolder: "Preferred Brand".localizedString(), value:  "", info: nil, height: Constants.CellHeightConstants.height_80)
        array.append(rimsBrandCell)
        
        //Wheel Size
        let wheelSizeCell = CellInfo(cellType: .TextInputCell, placeHolder: "Wheel Size".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_80, keyboardType: .numberPad)
        array.append(wheelSizeCell)
        
        //Wheel Size info
        let wheelSizeInfoCell = CellInfo(cellType: .WheelSizeInfoCell, placeHolder: "", value: "", info: nil, height: Constants.CellHeightConstants.height_40)
        array.append(wheelSizeInfoCell)
        
        //ChooseServiceTypeLabel Cell
        var chooseServiceTypeInfo = [String: AnyObject]()
        chooseServiceTypeInfo[Constants.UIKeys.placeholderText] = "Choose Rim Service Type".localizedString() as AnyObject
        chooseServiceTypeInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseServiceTypeCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Choose Part Type ".localizedString(), value: "", info: chooseServiceTypeInfo, height: Constants.CellHeightConstants.height_30)
        array.append(chooseServiceTypeCell)
        
        //FourRadioButtonCell
        var radioButtonInfo = [String: AnyObject]()
        radioButtonInfo[Constants.UIKeys.firstButtonTitle] = "Rim Restoration".localizedString() as AnyObject
        radioButtonInfo[Constants.UIKeys.secondButtonTitle] = "Rim Replacement".localizedString() as AnyObject
        radioButtonInfo[Constants.UIKeys.shouldHide] = true as AnyObject
        radioButtonInfo[Constants.UIKeys.selectedButtonIndex] = SparePartType.originalNew.rawValue as AnyObject as AnyObject
        let radioButtonInfoCell = CellInfo(cellType: .FourRadioButtonCell, placeHolder: "", value: "", info: radioButtonInfo, height: Constants.CellHeightConstants.height_40)
        array.append(radioButtonInfoCell)
        
        //Number of rims
        let numOfrimsCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value:  "No of Rims".localizedString(), info: nil, height: Constants.CellHeightConstants.height_80)
        array.append(numOfrimsCell)
        
        //ChoosePartTypeLabel Cell
        var choosePartTypeInfo = [String: AnyObject]()
        choosePartTypeInfo[Constants.UIKeys.placeholderText] = "Choose Part Type ".localizedString() as AnyObject
        choosePartTypeInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let choosePartTypeCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Choose Part Type ".localizedString(), value: "", info: choosePartTypeInfo, height: Constants.CellHeightConstants.height_30)
        array.append(choosePartTypeCell)
        
        //FourRadioButtonCell
        var radioButtonInfo2 = [String: AnyObject]()
        radioButtonInfo2[Constants.UIKeys.firstButtonTitle] = "Original New".localizedString() as AnyObject
        radioButtonInfo2[Constants.UIKeys.secondButtonTitle] = "Commercial New".localizedString() as AnyObject
        radioButtonInfo2[Constants.UIKeys.thirdButtonTitle] = "Orginal Used".localizedString() as AnyObject
        radioButtonInfo2[Constants.UIKeys.fourthButtonTitle] = "Commercial  Used".localizedString() as AnyObject
        radioButtonInfo2[Constants.UIKeys.shouldHide] = false as AnyObject
        radioButtonInfo2[Constants.UIKeys.selectedButtonIndex] = SparePartType.originalNew.rawValue as AnyObject
        let radioButtonInfo2Cell = CellInfo(cellType: .FourRadioButtonCell, placeHolder: "", value: "", info: radioButtonInfo2, height: Constants.CellHeightConstants.height_90)
        array.append(radioButtonInfo2Cell)
        
        //Additional Information Cell
        let addInfoCell = CellInfo(cellType: .TextViewCell, placeHolder: "Additional Information".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(addInfoCell)
        
        
        //choose Date time Label Cell
        var chooseLabelInfo = [String: AnyObject]()
        chooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Tender".localizedString() as AnyObject
        chooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Tender".localizedString(), value: "", info: chooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseLabelCell)
        
        //DateRange Cell
        var dateRangeInfo = [String: AnyObject]()
        dateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let dateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: dateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(dateRangeCell)
        
        //Job Date time Label Cell
        var jobVhooseLabelInfo = [String: AnyObject]()
        jobVhooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Job".localizedString() as AnyObject
        jobVhooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let jobChooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Job".localizedString(), value: "", info: jobVhooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(jobChooseLabelCell)
        
        //DateRange Cell
        var jobDateRangeInfo = [String: AnyObject]()
        jobDateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let jobDateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: jobDateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(jobDateRangeCell)
        
        //choose Duration Label Cell
        var chooseDurationLabelInfo = [String: AnyObject]()
        chooseDurationLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Tender Duration".localizedString() as AnyObject
        chooseDurationLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseDurationLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Tender Duration".localizedString(), value: "", info: chooseDurationLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseDurationLabelCell)
        
        //Tender Duration
        let tenderDurationCell = CellInfo(cellType: .ImageDropDownCell, placeHolder: "", value: StringConstants.Text.tenderDuration.localizedString(), info: nil, height: Constants.CellHeightConstants.height_80)
        array.append( tenderDurationCell)
        
        //Upload photo Label Cell
        var uploadPhotoInfo = [String: AnyObject]()
        uploadPhotoInfo[Constants.UIKeys.placeholderText] = "Upload Photos of Your Vehicle".localizedString() as AnyObject
        chooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let uploadPhotoCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Tender".localizedString(), value: "", info: uploadPhotoInfo, height: Constants.CellHeightConstants.height_60)
        array.append(uploadPhotoCell)
        
        //Vehicle Images Cell
        var vehicleImagesCellInfo = [String: AnyObject]()
        vehicleImagesCellInfo[Constants.UIKeys.images] = [""] as AnyObject
        let vehicleImagesCell = CellInfo(cellType: .UploadVehicleImagesCell, placeHolder: "", value: StringConstants.Text.selectCarModel.localizedString(), info: vehicleImagesCellInfo, height: Constants.CellHeightConstants.height_320)
        array.append(vehicleImagesCell)
        
        // Want pickup and drop off cell
        var pickUpInfo = [String: AnyObject]()
        pickUpInfo[Constants.UIKeys.isSelected] = false as AnyObject
        pickUpInfo[Constants.UIKeys.shouldHide] = true as AnyObject
        let pickUpCell = CellInfo(cellType: .NoReserveCell, placeHolder: "I want pick Up and Drop Off".localizedString(), value:"" ,info: pickUpInfo
            , height: Constants.CellHeightConstants.height_50)
        array.append(pickUpCell)
        
        
        //Location cell
        var recoveryRequestInfo = [String: AnyObject]()
        recoveryRequestInfo[Constants.UIKeys.sourcePlaceHolder] = "Pick Up Location".localizedString() as AnyObject
        recoveryRequestInfo[Constants.UIKeys.destinationPlaceHolder] = "Drop Off Location".localizedString() as AnyObject
        recoveryRequestInfo[Constants.UIKeys.sourceValue] = "" as AnyObject
        recoveryRequestInfo[Constants.UIKeys.destinationValue] = "" as AnyObject
        
        let recoveryRequestCell = CellInfo(cellType: .LocationTextFieldsCell, placeHolder: "", value: "", info: recoveryRequestInfo, height: Constants.CellHeightConstants.height_220)
        array.append(recoveryRequestCell)
        return array
    }
    
    
    
    func requestTenderAPI(arrData: [CellInfo], vehicleId: Int, completion: @escaping (Bool)-> Void) {
        if self.validateFormParameters(arrData: arrData) {
            self.uploadImages(arrData: arrData) { (uploadedImages) in
                let tenderRequestParams = self.getAPIParams(arrData: arrData, vehicleId: vehicleId, images: uploadedImages)
                APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderRequestForm(param: tenderRequestParams))) { (response, success) in
                    if success {
                        if let _ =  response as? [String: AnyObject] {
                            completion(success)
                        }
                    }
                }
            }
        }
        
    }
    
    func validateFormParameters(arrData: [CellInfo]) -> Bool {
        var isValid = true
        if let wheelBrand = arrData[5].value, wheelBrand.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select tires brand.".localizedString())
            isValid = false
        } else if let wheelSize = arrData[6].value, wheelSize.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select wheel size.".localizedString())
            isValid = false
        } else if let numOfRims = arrData[10].value, numOfRims == "No of Rims".localizedString() {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select number of rims.".localizedString())
            isValid = false
        }
        if let date = arrData[15].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of tender.".localizedString())
            isValid = false
        } else if let time = arrData[15].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of tender.".localizedString())
            isValid = false
        } else if let date = arrData[17].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of job.".localizedString())
            isValid = false
        } else if let time = arrData[17].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of job.".localizedString())
            isValid = false
        } else if let duration = arrData[19].value, (duration.isEmpty || duration == StringConstants.Text.tenderDuration.localizedString()) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select duration of tender.".localizedString())
            isValid = false
        }   else if let willPickDrop = arrData[22].info?[Constants.UIKeys.isSelected] as? Bool, let pickupLocation = arrData[23].info?[Constants.UIKeys.sourceValue] as? String, (willPickDrop && pickupLocation.isEmpty)  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide your pickup location.".localizedString())
            isValid = false
        } else if let willPickDrop = arrData[22].info?[Constants.UIKeys.isSelected] as? Bool, let dropOffLocation = arrData[23].info?[Constants.UIKeys.destinationValue] as? String, (willPickDrop && dropOffLocation.isEmpty)  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide your drop off location.".localizedString())
            isValid = false
        }
        
        if isValid  {
            
            let dateStr = arrData[15].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let timeStr = arrData[15].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            
            let jobDateStr = arrData[17].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let jobTimeStr = arrData[17].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            
            let tenderDateTime = Helper.toString(object: dateStr) + " " +  Helper.toString(object: timeStr)
            let jobDateTime = Helper.toString(object: jobDateStr) + " " +  Helper.toString(object: jobTimeStr)
            isValid = Helper.compareJobAndTenderDateTime(tenderDateTime: tenderDateTime, jobDateTime: jobDateTime)
        }
        

        
        return isValid
    }
    
    private func uploadImages(arrData: [CellInfo], completion: @escaping(([String]) -> Void)) {
        var imagesToUpload: [UIImage] = []
        if let imagesInfo = arrData[21].info {
            if let firstImage = imagesInfo[Constants.UIKeys.firstImg] as? UIImage {
                imagesToUpload.append(firstImage)
            }
            if let secondImage = imagesInfo[Constants.UIKeys.secondImg] as? UIImage {
                imagesToUpload.append(secondImage)
            }
            if let thirdImage = imagesInfo[Constants.UIKeys.thirdImg] as? UIImage {
                imagesToUpload.append(thirdImage)
            }
            if let fourthImage = imagesInfo[Constants.UIKeys.fourthImg] as? UIImage {
                imagesToUpload.append(fourthImage)
            }
            if imagesToUpload.count > 0 {
                S3Manager.sharedInstance.uploadImages(images: imagesToUpload, imageQuality: .medium, progress: nil) { (imageUrls, imageNames, error) in
                    if let uploadedImages = imageUrls as? [String] {
                        completion(uploadedImages)
                    }
                }
            } else {
                completion([])
            }
        }
    }
    
    func getAPIParams(arrData: [CellInfo], vehicleId: Int, images: [String]) -> APIParams {
        var params: APIParams = APIParams()
        
        params[ConstantAPIKeys.preferredBrand] = arrData[5].value as AnyObject
        params[ConstantAPIKeys.wheelType] = WheelType.rims.rawValue as AnyObject
        params[ConstantAPIKeys.noOfRims] = arrData[10].value as AnyObject
        params[ConstantAPIKeys.additionalInformation] = arrData[13].value as AnyObject
        params[ConstantAPIKeys.images] =  images as AnyObject
        params[ConstantAPIKeys.wheelSize] = arrData[6].value as AnyObject
        params[ConstantAPIKeys.rimServiceType] = arrData[9].info?[Constants.UIKeys.selectedButtonIndex] as AnyObject
        params[ConstantAPIKeys.wheelPartType] = arrData[12].info?[Constants.UIKeys.selectedButtonIndex] as AnyObject
        params[ConstantAPIKeys.type] = RecoveryHomeScreenType.rims.rawValue as AnyObject
        params[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject

        let dateStr = arrData[15].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let timeStr = arrData[15].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.requestedDate] = dateStr as AnyObject
        params[ConstantAPIKeys.requestedTime] = timeStr as AnyObject

        let jobDateStr = arrData[17].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let jobTimeStr = arrData[17].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.jobDate] = jobDateStr as AnyObject
        params[ConstantAPIKeys.jobTime] = jobTimeStr as AnyObject
        
        params[ConstantAPIKeys.duration] = arrData[19].placeHolder as AnyObject

        params[ConstantAPIKeys.pickupLocation] = arrData[23].info?[Constants.UIKeys.sourceValue] as? String == "" ? "<null>" as AnyObject : arrData[23].info?[Constants.UIKeys.sourceValue] as AnyObject
        params[ConstantAPIKeys.pickupLocationLatitude] =  arrData[23].info?[Constants.UIKeys.sourceLatitude] as AnyObject
        params[ConstantAPIKeys.pickupLocationLongitude] =  arrData[23].info?[Constants.UIKeys.sourceLongitude] as AnyObject
        params[ConstantAPIKeys.dropLocation] = arrData[23].info?[Constants.UIKeys.destinationValue] as? String == "" ? "<null>" as AnyObject : arrData[23].info?[Constants.UIKeys.destinationValue] as AnyObject
        params[ConstantAPIKeys.dropLocationLatitude] =  arrData[23].info?[Constants.UIKeys.destinationLatitude] as AnyObject
        params[ConstantAPIKeys.dropLocationLongitude] =  arrData[23].info?[Constants.UIKeys.destinationLongitude] as AnyObject
                
        return params
    }
    
}
