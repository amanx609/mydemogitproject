//
//  WheelSizeInfoCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol WheelSizeInfoCellDelegate: class {
    func didTapInfoButton(cell: WheelSizeInfoCell)
}

class WheelSizeInfoCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var wheelSizeInfoLabel: UILabel!
    @IBOutlet weak var infoButton: UIButton!
    
    //MARK: - Variables
    weak var delegate: WheelSizeInfoCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK: - Private Methods
    private func setup() {
        self.wheelSizeInfoLabel.text = "Find your wheel size here".localizedString()
       
    }
    
    //MARK: - IBActions
    
    @IBAction func tapInfoButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapInfoButton(cell: self)
        }
    }
}
