//
//  WTenderListViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension WTenderListViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tenderStatusDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tenderListData = self.tenderStatusDataSource[indexPath.row]
        return getCell(tableView, indexPath: indexPath, tenderListData: tenderListData)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tender = self.tenderStatusDataSource[indexPath.row]
        switch tender.tenderStatus() {
        case .bidAccepted, .completedTenders:
            let recoveryRequestViewC = DIConfigurator.sharedInstance.getWBidAcceptedVC()
            recoveryRequestViewC.tenderId = Helper.toInt(self.tenderStatusDataSource[indexPath.row].id)
            recoveryRequestViewC.tenderStatus = tender.tenderStatus()
            self.navigationController?.pushViewController(recoveryRequestViewC, animated: true)
        default:
            let recoveryRequestViewC = DIConfigurator.sharedInstance.getWBidListVC()
            recoveryRequestViewC.tenderId = Helper.toInt(self.tenderStatusDataSource[indexPath.row].id)
            recoveryRequestViewC.tenderStatus = tender.tenderStatus()
            self.navigationController?.pushViewController(recoveryRequestViewC, animated: true)
            
        }
       
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      guard let lastVisibleRow = self.tenderStatusTableView.indexPathsForVisibleRows?.last?.row else { return }
      if (self.tenderStatusDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
        self.requestTenderListAPI()
      }
    }
}

extension WTenderListViewC {
    func getCell(_ tableView: UITableView, indexPath: IndexPath, tenderListData: AllTendersModel) -> UITableViewCell {
        let cell: ServicingTenderListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureViewForRims(tenderModel: tenderListData)
        return cell
    }
}
