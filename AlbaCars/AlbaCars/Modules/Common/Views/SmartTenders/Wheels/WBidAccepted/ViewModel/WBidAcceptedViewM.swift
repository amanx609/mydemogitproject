//
//  WBidAcceptedViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol WBidAcceptedVModeling: BaseVModeling {
    func getWBidAcceptedDataSource(tenderDetails: RDBidListModel) -> [CellInfo]
    
}

class WBidAcceptedVM: BaseViewM, WBidAcceptedVModeling {
    
    
    func getWBidAcceptedDataSource(tenderDetails: RDBidListModel) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //CarDetailCell
        var carDetailInfo = [String: AnyObject]()
        carDetailInfo[Constants.UIKeys.cellInfo] = tenderDetails
        carDetailInfo[Constants.UIKeys.isSecondDetailPage] = true as AnyObject
        let carDetailCell = CellInfo(cellType: .STCarDetailCell, placeHolder: "", value: "", info: carDetailInfo, height: Constants.CellHeightConstants.height_240)
        array.append(carDetailCell)
        
        //ServiceDetailCell
        var serviceDetailInfo = [String: AnyObject]()
        serviceDetailInfo[Constants.UIKeys.serviceType] = tenderDetails.getShowServiceTypeDetails() as AnyObject
        serviceDetailInfo[Constants.UIKeys.wheelSize] = tenderDetails.wheelSize as AnyObject
        serviceDetailInfo[Constants.UIKeys.additionalInformation] = tenderDetails.additionalInfo as AnyObject
        let serviceDetailCell = CellInfo(cellType: .WServiceDetailsCell, placeHolder: "", value: "", info: serviceDetailInfo, height: UITableView.automaticDimension)
        array.append(serviceDetailCell)
        
        //Three Images Cell
        if let totalImage = tenderDetails.image, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info:imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }

        //BidsCountCell
        var bidsCountCellInfo = [String: AnyObject]()
        bidsCountCellInfo[Constants.UIKeys.isSecondDetailPage] = true as AnyObject
        let bidsCountCell = CellInfo(cellType: .STBidsCountCell, placeHolder: "", value: "", info: bidsCountCellInfo, height: Constants.CellHeightConstants.height_55)
        array.append(bidsCountCell)
        
        //Bid Detail cell
        var bidDetailCellInfo = [String: AnyObject]()
        if let bids = tenderDetails.Bids {
            bidDetailCellInfo[Constants.UIKeys.serviceType] = bids.getShowServiceTypeDetails() as AnyObject
           // bidDetailCellInfo[Constants.UIKeys.partType] = Helper.toString(object: bids.partType) as AnyObject
            bidDetailCellInfo[Constants.UIKeys.additionalInformation] = Helper.toString(object: bids.additionalInfo) as AnyObject
            bidDetailCellInfo[Constants.UIKeys.servicePrice] = Helper.toString(object: bids.amount) as AnyObject
            bidDetailCellInfo[Constants.UIKeys.paymentType] = Helper.toString(object: bids.paymentType) as AnyObject
            bidDetailCellInfo[Constants.UIKeys.transaction] =  Helper.toString(object: bids.transactionNo) as AnyObject
        }
        
        let bidDetailCell = CellInfo(cellType: .BidDetailCell, placeHolder: "", value: "", info: bidDetailCellInfo, height:UITableView.automaticDimension)
        array.append(bidDetailCell)
        
        //service provider detail
        var firstBidCellInfo = [String: AnyObject]()
        firstBidCellInfo[Constants.UIKeys.agencyName] = Helper.toString(object: tenderDetails.driverName) as AnyObject
        firstBidCellInfo[Constants.UIKeys.rating] = tenderDetails.rating as AnyObject
        firstBidCellInfo[Constants.UIKeys.address] = Helper.toString(object: tenderDetails.driverAddress) as AnyObject
         firstBidCellInfo[Constants.UIKeys.driverImage] = Helper.toString(object: tenderDetails.driverImage) as AnyObject
        
        let firstBidCell = CellInfo(cellType: .SPDetailCell, placeHolder: "", value: "", info: firstBidCellInfo, height: Constants.CellHeightConstants.height_110)
        array.append(firstBidCell)
        
        //Contact Info Cell
       
        let secondBidCell = CellInfo(cellType: .VIContactInfoCell, placeHolder: "+971 \(Helper.toString(object: tenderDetails.driverMobile))", value:Helper.toString(object: tenderDetails.driverEmail), info: nil, height: Constants.CellHeightConstants.height_120)
        array.append(secondBidCell)
        
        
        return array
    }
}
