//
//  WServiceDetailsCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class WServiceDetailsCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var serviceTypeLabel: UILabel!
    @IBOutlet weak var wheelSizeLabel: UILabel!
    @IBOutlet weak var infoDetailLabel: UILabel!
    @IBOutlet weak var additionalInfoLabel: UILabel!
    @IBOutlet weak var wheelSizeBgView: UIView!
    @IBOutlet weak var serviceTypeBgView: UIView!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.serviceTypeBgView.isHidden = false
        self.wheelSizeBgView.isHidden = false
    }
    
    //MARK: - Public Methods
    //Wheels
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            //serviceType
            self.serviceTypeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.serviceType.localizedString()) ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.serviceType]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
            self.wheelSizeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.wheelSizeText.localizedString()) ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.wheelSize]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.infoDetailLabel.text = Helper.toString(object: info[Constants.UIKeys.additionalInformation])
            self.additionalInfoLabel.text = "Additional Information".localizedString()
        }
    }
    
    func configureViewBW(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            self.serviceTypeBgView.isHidden = true
            self.wheelSizeBgView.isHidden = true
            self.infoDetailLabel.text = Helper.toString(object: info[Constants.UIKeys.additionalInformation])
            self.additionalInfoLabel.text = "Additional Information".localizedString()
        }
        
    }
    
    //Car Detailing
    func configureViewCD(cellInfo: CellInfo, carDetailingType: CarDetailingOptions) {
        if let tenderDetails = cellInfo.info?[Constants.UIKeys.cellInfo] as? RDBidListModel {
            
            switch carDetailingType {
            case .quickWash, .bodyPolishing:
                self.serviceTypeBgView.isHidden = true
                self.wheelSizeBgView.isHidden = true
                if let info = cellInfo.info {
                    
                    self.infoDetailLabel.text = Helper.toString(object: info[Constants.UIKeys.additionalInformation])
                    self.additionalInfoLabel.text = "Additional Information".localizedString()
                }
                
            case .detailing:
                self.serviceTypeBgView.isHidden = true
                self.wheelSizeBgView.isHidden = false
                if let info = cellInfo.info {
                    self.wheelSizeLabel.attributedText = String.getAttributedText(firstText: "\("Service type : ".localizedString()) ", secondText: " \(Helper.toString(object: Helper.getDetailingService(statusCode: Helper.toInt(tenderDetails.carDetailServiceType))))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
                    self.infoDetailLabel.text = Helper.toString(object: info[Constants.UIKeys.additionalInformation])
                    self.additionalInfoLabel.text = "Additional Information".localizedString()
                }
                
            }
            
        }
        
    }
}
