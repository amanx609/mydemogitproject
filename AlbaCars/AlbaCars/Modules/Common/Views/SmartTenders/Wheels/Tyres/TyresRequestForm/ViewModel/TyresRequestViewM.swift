//
//  TyresRequestViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol TyresRequestVModeling: BaseVModeling {
    func getTyresTenderReqDataSource(carDetails: ChooseCarModel?) -> [CellInfo]
    func requestTenderAPI(arrData: [CellInfo], vehicleId: Int, completion: @escaping (Bool)-> Void)
}

class TyresRequestViewM: TyresRequestVModeling {
    
    func getTyresTenderReqDataSource(carDetails: ChooseCarModel?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Plate Number
        let carPlateCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.carPlate.localizedString(), value: Helper.toString(object: carDetails?.plateNumber), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carPlateCell)
        
        //Car brand
        let carBrandCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value:  Helper.toString(object: carDetails?.brandName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carBrandCell)
        
        //Car Model
        let carModelCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value:  Helper.toString(object: carDetails?.modelName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carModelCell)
        
        //Car Year
        let carYearCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.year), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carYearCell)
        
        
        //Car Type
        let carTypeCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.typeName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carTypeCell)
        
        //Preferred brand
        let rimsBrandCell = CellInfo(cellType: .TextInputCell, placeHolder: "Preferred Brand".localizedString(), value:  "", info: nil, height: Constants.CellHeightConstants.height_80)
        array.append(rimsBrandCell)
        
        //Wheel Size
        let wheelSizeCell = CellInfo(cellType: .TextInputCell, placeHolder: "Wheel Size".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_80, keyboardType: .numberPad)
        array.append(wheelSizeCell)
        
        
        //Wheel Size info
        let wheelSizeInfoCell = CellInfo(cellType: .WheelSizeInfoCell, placeHolder: "", value: "", info: nil, height: Constants.CellHeightConstants.height_40)
        array.append(wheelSizeInfoCell)
        
        //Additional Information Cell
        let addInfoCell = CellInfo(cellType: .TextViewCell, placeHolder: "Additional Information".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(addInfoCell)
        
        
        //choose Date time Label Cell
        var chooseLabelInfo = [String: AnyObject]()
        chooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Tender".localizedString() as AnyObject
        chooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Tender".localizedString(), value: "", info: chooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseLabelCell)
        
        //DateRange Cell
        var dateRangeInfo = [String: AnyObject]()
        dateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let dateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: dateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(dateRangeCell)
                
        //Job Date time Label Cell
        var jobVhooseLabelInfo = [String: AnyObject]()
        jobVhooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Job".localizedString() as AnyObject
        jobVhooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let jobChooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Job".localizedString(), value: "", info: jobVhooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(jobChooseLabelCell)
        
        //DateRange Cell
        var jobDateRangeInfo = [String: AnyObject]()
        jobDateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let jobDateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: jobDateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(jobDateRangeCell)
        
        //choose Duration Label Cell
        var chooseDurationLabelInfo = [String: AnyObject]()
        chooseDurationLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Tender Duration".localizedString() as AnyObject
        chooseDurationLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseDurationLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Tender Duration".localizedString(), value: "", info: chooseDurationLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseDurationLabelCell)
        
        //Tender Duration
        let tenderDurationCell = CellInfo(cellType: .ImageDropDownCell, placeHolder: "", value: StringConstants.Text.tenderDuration.localizedString(), info: nil, height: Constants.CellHeightConstants.height_80)
        array.append( tenderDurationCell)
        
        // Want pickup and drop off cell
        var pickUpInfo = [String: AnyObject]()
        pickUpInfo[Constants.UIKeys.isSelected] = false as AnyObject
        pickUpInfo[Constants.UIKeys.shouldHide] = true as AnyObject
        let pickUpCell = CellInfo(cellType: .NoReserveCell, placeHolder: "I want pick Up and Drop Off".localizedString(), value:"" ,info: pickUpInfo
            , height: Constants.CellHeightConstants.height_50)
        array.append(pickUpCell)
        
        
        //Location cell
        var recoveryRequestInfo = [String: AnyObject]()
        recoveryRequestInfo[Constants.UIKeys.sourcePlaceHolder] = "Pick Up Location".localizedString() as AnyObject
        recoveryRequestInfo[Constants.UIKeys.destinationPlaceHolder] = "Drop Off Location".localizedString() as AnyObject
        recoveryRequestInfo[Constants.UIKeys.sourceValue] = "" as AnyObject
        recoveryRequestInfo[Constants.UIKeys.destinationValue] = "" as AnyObject
        
        let recoveryRequestCell = CellInfo(cellType: .LocationTextFieldsCell, placeHolder: "", value: "", info: recoveryRequestInfo, height: Constants.CellHeightConstants.height_220)
        array.append(recoveryRequestCell)
        
        return array
    }
    
    
    
    func requestTenderAPI(arrData: [CellInfo], vehicleId: Int, completion: @escaping (Bool)-> Void) {
        if self.validateFormParameters(arrData: arrData) {
            let tenderRequestParams = self.getAPIParams(arrData: arrData, vehicleId: vehicleId)
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderRequestForm(param: tenderRequestParams))) { (response, success) in
                if success {
                    if let safeResponse =  response as? [String: AnyObject]
                    {
                        completion(success)
                    }
                }
            }
        }
    }
    
    func validateFormParameters(arrData: [CellInfo]) -> Bool {
        var isValid = true
        if let wheelBrand = arrData[5].value, wheelBrand.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select tire brand.".localizedString())
            isValid = false
        } else if let wheelSize = arrData[6].value, wheelSize.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select wheel size.".localizedString())
            isValid = false
        } else if let date = arrData[10].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of tender.".localizedString())
            isValid = false
        } else if let time = arrData[10].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of tender.".localizedString())
            isValid = false
        } else if let date = arrData[12].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of job.".localizedString())
            isValid = false
        } else if let time = arrData[12].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of job.".localizedString())
            isValid = false
        } else if let duration = arrData[14].value, (duration.isEmpty || duration == StringConstants.Text.tenderDuration.localizedString()) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select duration of tender.".localizedString())
            isValid = false
        }   else if let willPickDrop = arrData[15].info?[Constants.UIKeys.isSelected] as? Bool, let pickupLocation = arrData[16].info?[Constants.UIKeys.sourceValue] as? String, (willPickDrop && pickupLocation.isEmpty)  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide your pickup location.".localizedString())
            isValid = false
        } else if let willPickDrop = arrData[15].info?[Constants.UIKeys.isSelected] as? Bool, let dropOffLocation = arrData[16].info?[Constants.UIKeys.destinationValue] as? String, (willPickDrop && dropOffLocation.isEmpty)  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide your drop off location.".localizedString())
            isValid = false
        }
        
        if isValid  {
            
            let dateStr = arrData[10].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let timeStr = arrData[10].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            
            let jobDateStr = arrData[12].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let jobTimeStr = arrData[12].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            
            let tenderDateTime = Helper.toString(object: dateStr) + " " +  Helper.toString(object: timeStr)
            let jobDateTime = Helper.toString(object: jobDateStr) + " " +  Helper.toString(object: jobTimeStr)
            isValid = Helper.compareJobAndTenderDateTime(tenderDateTime: tenderDateTime, jobDateTime: jobDateTime)
        }

        return isValid
    }
    
    func getAPIParams(arrData: [CellInfo], vehicleId: Int) -> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.wheelSize] = arrData[6].value as AnyObject
        params[ConstantAPIKeys.preferredBrand] = arrData[5].value as AnyObject
        params[ConstantAPIKeys.additionalInformation] = arrData[8].value as AnyObject
        params[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        let dateStr = arrData[10].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let timeStr = arrData[10].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.requestedDate] = dateStr as AnyObject
        params[ConstantAPIKeys.requestedTime] = timeStr as AnyObject
        
        let jobDateStr = arrData[12].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let jobTimeStr = arrData[12].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.jobDate] = jobDateStr as AnyObject
        params[ConstantAPIKeys.jobTime] = jobTimeStr as AnyObject
        
        params[ConstantAPIKeys.pickupLocation] = arrData[16].info?[Constants.UIKeys.sourceValue] as? String == "" ? "<null>" as AnyObject : arrData[16].info?[Constants.UIKeys.sourceValue] as AnyObject
        params[ConstantAPIKeys.pickupLocationLatitude] =  arrData[16].info?[Constants.UIKeys.sourceLatitude] as AnyObject
        params[ConstantAPIKeys.pickupLocationLongitude] =  arrData[16].info?[Constants.UIKeys.sourceLongitude] as AnyObject
        params[ConstantAPIKeys.dropLocation] = arrData[16].info?[Constants.UIKeys.destinationValue] as? String == "" ? "<null>" as AnyObject : arrData[16].info?[Constants.UIKeys.destinationValue] as AnyObject
        params[ConstantAPIKeys.dropLocationLatitude] =  arrData[16].info?[Constants.UIKeys.destinationLatitude] as AnyObject
        params[ConstantAPIKeys.dropLocationLongitude] =  arrData[16].info?[Constants.UIKeys.destinationLongitude] as AnyObject
        params[ConstantAPIKeys.duration] = arrData[14].placeHolder as AnyObject
        
        params[ConstantAPIKeys.wheelType] = WheelType.tyres.rawValue as AnyObject
        params[ConstantAPIKeys.type] = RecoveryHomeScreenType.tyres.rawValue as AnyObject
        return params
    }
    
}
