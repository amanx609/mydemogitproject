//
//  VIContactInfoCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh  on 1/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol VIContactInfoCellDelegate: class {
    func didTapPhone(cell: VIContactInfoCell, phone: String)
}

class VIContactInfoCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var contactInfoLabel: UILabel!
    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var bgViewLeading: NSLayoutConstraint!
    @IBOutlet weak var bgViewTralling: NSLayoutConstraint!
    
    //MARK: - Variables
    weak var delegate: VIContactInfoCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK: - Public Methods
     func configureView(cellInfo: CellInfo) {
        self.contactNumberLabel.text = cellInfo.placeHolder
        self.emailLabel.text = cellInfo.value
     }
    
    func configureTenderDetailsView(cellInfo: CellInfo) {
        self.bgViewLeading.constant = 20
        self.bgViewTralling.constant = 20
        self.contactNumberLabel.text = cellInfo.placeHolder
        self.emailLabel.text = cellInfo.value
    }
    
    //MARK: - IBAction
    @IBAction func tapContactButton(_ sender: Any) {
        if let delegate = self.delegate, let phone = self.contactNumberLabel.text {
            delegate.didTapPhone(cell: self, phone: phone)
        }
    }
    
}
