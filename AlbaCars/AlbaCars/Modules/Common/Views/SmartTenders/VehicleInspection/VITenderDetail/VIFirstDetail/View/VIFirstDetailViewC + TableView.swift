//
//  VIFirstDetailViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension VIFirstDetailViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.inspectionDetailDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.inspectionDetailDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellInfo = self.inspectionDetailDataSource[indexPath.row]
        guard let cellType = cellInfo.cellType else { return self.inspectionDetailDataSource[indexPath.row].height }
        if (self.tenderStatus == .allBidsRejected) {
            
            switch cellType {
            case .STBidsCountCell:
                return Constants.CellHeightConstants.height_0
                
            default:
                break
            }
        }
        return self.inspectionDetailDataSource[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cellInfo = inspectionDetailDataSource[indexPath.row]
        guard let cellType = cellInfo.cellType else { return }
        switch cellType {
        case .RDBidListCell:
            if let info = cellInfo.info {
                self.requestBidTenderDetails(bidId: Helper.toInt(info[ConstantAPIKeys.id]))
            }
           break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let lastVisibleRow = self.inspectionDetailTableView.indexPathsForVisibleRows?.last?.row else { return }
        if (self.inspectionDetailDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
            if let tenderDetail = self.tenderDetails {
                self.requestBidListAPI(tenderDetails: tenderDetail)
            }
        }
    }

}

extension VIFirstDetailViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .STCarDetailCell:
            let cell: STCarDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo, type: .inspectionRequest, tenderStatus: self.tenderStatus)
            return cell
            
        case .STBidsCountCell:
            let cell: STBidsCountCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .inspectionRequest)
            return cell
            
        case .RDBidListCell:
            let cell: RDBidListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .vehicleInspection)
            cell.delegate = self
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

extension VIFirstDetailViewC: RDBidListCellDelegate {
    func didTapCheck(cell: RDBidListCell) {
       guard let indexPath = self.inspectionDetailTableView.indexPath(for: cell) else { return }
       let bidId = Helper.toInt(self.inspectionDetailDataSource[indexPath.row].info?[Constants.UIKeys.id])
       let bidAmount = Helper.toDouble(self.inspectionDetailDataSource[indexPath.row].info?[Constants.UIKeys.bidAmount])
       self.requestAcceptBid(bidId: bidId, winAmount: bidAmount)

    }
    
    func didTapCross(cell: RDBidListCell) {
        guard let indexPath = self.inspectionDetailTableView.indexPath(for: cell) else { return }
               let bidId = Helper.toInt(self.inspectionDetailDataSource[indexPath.row].info?[Constants.UIKeys.id])
               self.requestRejectBid(bidId: bidId)
    }
    
    
}

extension VIFirstDetailViewC: STCarDetailCellDelegate {
    func didTapCancelButton(cell: STCarDetailCell) {
        self.requestCancelTender()
    }
    
    
}

