//
//  SPDetailCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SPDetailCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var serviceProviderLabel: UILabel!
    @IBOutlet weak var agencyNameLabel: UILabel!
    @IBOutlet weak var ratingView: ShowRating!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var sProviderImageView: UIImageView!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    private func setup() {
     self.sProviderImageView.roundCorners(24)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.ratingView.isUserInteractionEnabled = false
        if let info = cellInfo.info {
            self.serviceProviderLabel.text = "Service Provider Detail".localizedString()
            self.sProviderImageView.setImage(urlStr: Helper.toString(object: info[Constants.UIKeys.driverImage]), placeHolderImage: UIImage(named: "user_placeholder"))
            
            self.agencyNameLabel.text = Helper.toString(object: info[Constants.UIKeys.agencyName])
            self.ratingView.configureView(rating: Helper.toDouble(info[Constants.UIKeys.rating]))
            self.locationLabel.text = Helper.toString(object: info[Constants.UIKeys.address])
        }
    }
    
}
