//
//  VISecondDetailViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

protocol VISecondDetailViewModeling: BaseVModeling {
    func getInspectionDetailDataSource(tenderDetails: RDBidListModel) -> [CellInfo]
}

class VISecondDetailViewM: BaseViewM, VISecondDetailViewModeling {
    
    func getInspectionDetailDataSource(tenderDetails: RDBidListModel) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //CarDetailCell
        var carDetailInfo = [String: AnyObject]()
        carDetailInfo[Constants.UIKeys.cellInfo] = tenderDetails
        carDetailInfo[Constants.UIKeys.isSecondDetailPage] = true as AnyObject
        let carDetailCell = CellInfo(cellType: .STCarDetailCell, placeHolder: "", value: "", info: carDetailInfo, height: Constants.CellHeightConstants.height_240)
        array.append(carDetailCell)
        
        //BidsCountCell
        var bidsCountCellInfo = [String: AnyObject]()
        bidsCountCellInfo[Constants.UIKeys.isSecondDetailPage] = true as AnyObject
        let bidsCountCell = CellInfo(cellType: .STBidsCountCell, placeHolder: "", value: "", info: bidsCountCellInfo, height: Constants.CellHeightConstants.height_55)
        array.append(bidsCountCell)
        
        //Bid Detail cell
        var bidDetailCellInfo = [String: AnyObject]()
        bidDetailCellInfo[Constants.UIKeys.serviceType] = "Vehicle Inspection" as AnyObject
        bidDetailCellInfo[Constants.UIKeys.date] = Helper.toString(object: tenderDetails.Bids?.getJobDate()) as AnyObject
        bidDetailCellInfo[Constants.UIKeys.pickupService] = Helper.toBool( tenderDetails.Bids?.isPickupDropoff)  as AnyObject
        bidDetailCellInfo[Constants.UIKeys.servicePrice] = Helper.toString(object: tenderDetails.Bids?.amount) as AnyObject
        bidDetailCellInfo[Constants.UIKeys.paymentType] = Helper.toString(object: tenderDetails.Bids?.paymentType) as AnyObject
        bidDetailCellInfo[Constants.UIKeys.transaction] = Helper.toString(object: tenderDetails.Bids?.transactionNo) as AnyObject
        let bidDetailCell = CellInfo(cellType: .BidDetailCell, placeHolder: "", value: "", info: bidDetailCellInfo, height: Constants.CellHeightConstants.height_230)
        array.append(bidDetailCell)
        
        //service provider detail
        var firstBidCellInfo = [String: AnyObject]()
        firstBidCellInfo[Constants.UIKeys.agencyName] = tenderDetails.driverName as AnyObject
         
        firstBidCellInfo[Constants.UIKeys.address] = tenderDetails.driverAddress as AnyObject
        firstBidCellInfo[ConstantAPIKeys.rating] = tenderDetails.rating  as AnyObject

        let firstBidCell = CellInfo(cellType: .SPDetailCell, placeHolder: "", value: "", info: firstBidCellInfo, height: Constants.CellHeightConstants.height_110)
        array.append(firstBidCell)
        
        //Contact Info Cell
        let secondBidCell = CellInfo(cellType: .VIContactInfoCell, placeHolder:"+971 \(Helper.toString(object: tenderDetails.driverMobile))", value: Helper.toString(object: tenderDetails.driverEmail), info: nil, height: Constants.CellHeightConstants.height_120)
         array.append(secondBidCell)
        
        
        return array
    }
}
