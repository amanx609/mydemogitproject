//
//  BidDetailCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class BidDetailCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutelts
    @IBOutlet weak var bidDetailLabel: UILabel!
    @IBOutlet weak var serviceTypeLabel: UILabel!
    @IBOutlet weak var inspectionDateLabel: UILabel!
    @IBOutlet weak var pickupServiceLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var paymentTypeLabel: UILabel!
    @IBOutlet weak var transactionNoLabel: UILabel!
    @IBOutlet weak var inspectionDateBgView: UIView!
    @IBOutlet weak var pickupServiceBgView: UIView!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK: - Public Methods
    
    func configureViewForServicing(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            //serviceType
            self.serviceTypeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.serviceType.localizedString()) ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.serviceType]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.inspectionDateBgView.isHidden = true
            self.pickupServiceBgView.isHidden = true
            
            
            //service Price
            let price = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.servicePrice]), text: StringConstants.Text.Currency.localizedString())
            self.priceLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.price.localizedString())", secondText: " \(price)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            // paymentType
            self.paymentTypeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.paymentType.localizedString())", secondText: " \(Helper.toString(object: info[Constants.UIKeys.paymentType]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            //transactionNo
            self.transactionNoLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.transactionNo.localizedString())", secondText: " \(Helper.toString(object: info[Constants.UIKeys.transaction]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        }
    }
    
    func configureViewForSpareParts(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            //serviceType
            self.serviceTypeLabel.attributedText = String.getAttributedText(firstText: "Part Type: ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.serviceType]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.inspectionDateLabel.attributedText = String.getAttributedText(firstText: "Part Name: ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.description]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.pickupServiceBgView.isHidden = true
            
            
            //service Price
            let price = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.servicePrice]), text: StringConstants.Text.Currency.localizedString())
            self.priceLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.price.localizedString())", secondText: " \(price)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            // paymentType
            self.paymentTypeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.paymentType.localizedString())", secondText: " \(Helper.toString(object: info[Constants.UIKeys.paymentType]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            //transactionNo
            self.transactionNoLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.transactionNo.localizedString())", secondText: " \(Helper.toString(object: info[Constants.UIKeys.transaction]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        }
    }
    
    func configureViewForBW(cellInfo: CellInfo, bodyWorkType: BodyWorkOptions) {
        if let info = cellInfo.info {
            //serviceType
            self.serviceTypeLabel.attributedText = String.getAttributedText(firstText: "Service Type: ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.title]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.inspectionDateLabel.attributedText = String.getAttributedText(firstText: "\(Helper.toString(object: info[Constants.UIKeys.serviceType]))", secondText: " \(Helper.toString(object: info[Constants.UIKeys.serviceTypeValue]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
            
            self.pickupServiceBgView.isHidden = true
            
            
            //service Price
            let price = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.servicePrice]), text: StringConstants.Text.Currency.localizedString())
            self.priceLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.price.localizedString())", secondText: " \(price)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            // paymentType
            self.paymentTypeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.paymentType.localizedString())", secondText: " \(Helper.toString(object: info[Constants.UIKeys.paymentType]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            //transactionNo
            self.transactionNoLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.transactionNo.localizedString())", secondText: " \(Helper.toString(object: info[Constants.UIKeys.transaction]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        }
    }
    
    func configureViewForBV(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            //serviceType
            self.serviceTypeLabel.attributedText =  String.getAttributedText(firstText: "Bank Name: ".localizedString(), secondText: "\(Helper.toString(object: info[Constants.UIKeys.bankName]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            // self.inspectionDateLabel.attributedText = String.getAttributedText(firstText: "Bank Name: ".localizedString(), secondText: "\(Helper.toString(object: info[Constants.UIKeys.bankName]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.inspectionDateBgView.isHidden = true
            self.pickupServiceLabel.attributedText = String.getAttributedText(firstText: "Requested Car Value: ".localizedString(), secondText: "AED \(Helper.toString(object: info[Constants.UIKeys.carPrice]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
            //service Price
            let price = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.servicePrice]), text: StringConstants.Text.Currency.localizedString())
            self.priceLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.price.localizedString())", secondText: " \(price)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            // paymentType
            self.paymentTypeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.paymentType.localizedString())", secondText: " \(Helper.toString(object: info[Constants.UIKeys.paymentType]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            //transactionNo
            self.transactionNoLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.transactionNo.localizedString())", secondText: " \(Helper.toString(object: info[Constants.UIKeys.transaction]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        }
    }
    
    func configureView(cellInfo: CellInfo, type: ChooseServiceType) {
        
        if let info = cellInfo.info {
            //serviceType
            self.serviceTypeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.serviceType.localizedString()) ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.serviceType]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.pickupServiceBgView.isHidden = false
            
            //service Price
            let price = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.servicePrice]), text: StringConstants.Text.Currency.localizedString())
            self.priceLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.price.localizedString())", secondText: " \(price)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
            switch type {
            case .vehicleInspection:
                // inspection date
                self.inspectionDateLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.inspectionDate.localizedString())", secondText: " \(Helper.toString(object: info[Constants.UIKeys.date]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
                // pickupService
                let pickupService = Helper.toBool(info[Constants.UIKeys.pickupService]) ? "Yes".localizedString() : "No".localizedString()
                self.pickupServiceLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.pickUpService.localizedString())", secondText: " \(Helper.toString(object: pickupService))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
                break
                
            case .upholstery:
                // description
                self.inspectionDateLabel.attributedText = String.getAttributedText(firstText: "Description: ".localizedString(), secondText: "\n\(Helper.toString(object: info[Constants.UIKeys.description]))", firstTextColor: .black, secondTextColor: .black, firstTextSize: .size_11, secondTextSize: .size_11, firstFontWeight: .Medium, secondFontWeight: .Book)
                // Additional Information
                
                self.pickupServiceLabel.attributedText = String.getAttributedText(firstText: "Additional Information: ".localizedString(), secondText: "\n\(Helper.toString(object: info[Constants.UIKeys.additionalInformation]))", firstTextColor: .black, secondTextColor: .black, firstTextSize: .size_11, secondTextSize: .size_11, firstFontWeight: .Medium, secondFontWeight: .Book)
                break
                
            case .wheels:
                // Part Type
                self.inspectionDateLabel.attributedText = String.getAttributedText(firstText: "Description: ".localizedString(), secondText: "\n\(Helper.toString(object: info[Constants.UIKeys.description]))", firstTextColor: .black, secondTextColor: .black, firstTextSize: .size_11, secondTextSize: .size_11, firstFontWeight: .Medium, secondFontWeight: .Book)
                self.pickupServiceBgView.isHidden = true
                
            case .windowTinting:
                // Tint Color
                self.inspectionDateLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.tintColorTxt.localizedString(), secondText: "\(Helper.toString(object: info[Constants.UIKeys.tintColor]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, firstFontWeight: .Medium, secondFontWeight: .Book)
                
                // Visibility Percentage
                let visibilityPercentage = Helper.toString(object: info[Constants.UIKeys.visibilityPercentage]) == "" ? "N/A" : "\(Helper.toString(object: info[Constants.UIKeys.visibilityPercentage]))%"
                self.pickupServiceLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.tintVisibilityTxt.localizedString(), secondText: visibilityPercentage, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, firstFontWeight: .Medium, secondFontWeight: .Book)
                break
            case .insurance:
                
                if let tenderDetails = cellInfo.info?[Constants.UIKeys.cellInfo] as? RDBidListModel {
                    let bidInsuranceType = Helper.toInt(tenderDetails.typeOfInsurance)
                    self.serviceTypeLabel.attributedText = String.getAttributedText(firstText: "Insurance Type : ".localizedString(), secondText: " \(Helper.getInsuranceType(statusCode: bidInsuranceType))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
                    self.inspectionDateLabel.attributedText = String.getAttributedText(firstText: "\("Bid Price : ".localizedString())", secondText: " \(price)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
                    
                    self.pickupServiceLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.vehicleValueText.localizedString()) ", secondText: "AED \(Helper.toString(object: tenderDetails.vehiclePrice))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
                    
                    self.priceLabel.attributedText = String.getAttributedText(firstText: "\("Insurer Name : ".localizedString()) ", secondText: " \(Helper.toString(object: tenderDetails.driverName))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)

                }
                
                print("")
            default:
                self.pickupServiceBgView.isHidden = false
                break
                
            }
            
            // paymentType
            self.paymentTypeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.paymentType.localizedString())", secondText: " \(Helper.toString(object: info[Constants.UIKeys.paymentType]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            //transactionNo
            self.transactionNoLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.transactionNo.localizedString())", secondText: " \(Helper.toString(object: info[Constants.UIKeys.transaction]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        }
    }
    
    func configureViewForCarDetailing(cellInfo: CellInfo, carDetailingOptions: CarDetailingOptions) {
        
        if let info = cellInfo.info {
            
            if let tenderDetails = cellInfo.info?[Constants.UIKeys.cellInfo] as? RDBidListModel {
                
                switch carDetailingOptions {
                case .quickWash:
                    self.serviceTypeLabel.attributedText = String.getAttributedText(firstText: "Service Type: ", secondText: " \(CarDetailingOptions.quickWash.title)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
                    self.inspectionDateBgView.isHidden = true
                    
                    break
                case .detailing:
                    self.inspectionDateBgView.isHidden = false
                    self.serviceTypeLabel.attributedText = String.getAttributedText(firstText: "Tender Type: ", secondText: " \(Helper.toString(object: Helper.getDetailingService(statusCode: Helper.toInt(tenderDetails.carDetailServiceType))))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
                    
                    self.inspectionDateLabel.attributedText = String.getAttributedText(firstText: "Service Type: ", secondText: " \(CarDetailingOptions.detailing.title)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
                    break
                case .bodyPolishing:
                    //serviceType
                    self.serviceTypeLabel.attributedText = String.getAttributedText(firstText: "Service Type: ", secondText: " \(CarDetailingOptions.bodyPolishing.title)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
                    self.inspectionDateBgView.isHidden = true
                    break
                default:
                    print()
                }
                
            }
            
            
            
            self.pickupServiceBgView.isHidden = true
            
            //service Price
            let price = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.servicePrice]), text: StringConstants.Text.Currency.localizedString())
            self.priceLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.price.localizedString())", secondText: " \(price)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            // paymentType
            self.paymentTypeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.paymentType.localizedString())", secondText: " \(Helper.toString(object: info[Constants.UIKeys.paymentType]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            //transactionNo
            self.transactionNoLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.transactionNo.localizedString())", secondText: " \(Helper.toString(object: info[Constants.UIKeys.transaction]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        }
    }
    
    
}
