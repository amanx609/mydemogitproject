//
//  VISecondDetailViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class VISecondDetailViewC: BaseViewC {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var secondDetailTableView: UITableView!
    @IBOutlet weak var jobCompletedButton: UIButton!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var jobCompletedButtonHeight: NSLayoutConstraint!
    
    //MARK: - Variables
       var viewModel: VISecondDetailViewModeling?
       var inspectionDetailDataSource: [CellInfo] = []
       var acceptedBidId: Int?
       var tenderDetails: RDBidListModel?
       var tenderId: Int?
       var tenderStatus: TenderStatus?
       var recoveryViewModel: RDBidListVModeling?
           
       //MARK: - LifeCycle Methods
       override func viewDidLoad() {
           super.viewDidLoad()
           self.setup()
       }
    
    override func backButtonTapped() {
        if let viewControllers = self.navigationController?.viewControllers {
            //CreateAccountViewCIsPresentInNavigationStack
            for viewController in viewControllers {
                if viewController is RecoveryHomeViewC {
                    self.navigationController?.popToViewController(viewController, animated: true)
                    return
                }
            }
            
        }
    }
    
    deinit {
        print("deinit VISecondDetailViewC")
    }
       
       //MARK: - Private Methods
       private func setup() {
           self.setupNavigationBarTitle(title: "Vehicle Inspection Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
           self.recheckVM()
           self.setupButton()
           self.setupTableView()
           self.requestTenderDetailsAPI()
        
        if let tenderStatus = self.tenderStatus, tenderStatus == .completedTenders {
            self.jobCompletedButton.isHidden = true
            self.jobCompletedButtonHeight.constant = 0
            self.gradientView.isHidden = true
        }
           
       }
     
    private func loadDataSource() {
        if let tndrDetails = self.tenderDetails,
           let dataSource = self.viewModel?.getInspectionDetailDataSource(tenderDetails: tndrDetails) {
            if let tenderStatus = self.tenderStatus, tenderStatus != .completedTenders {
                 self.secondDetailTableView.tableFooterView = self.footerView
            }
           
            self.inspectionDetailDataSource = dataSource
            self.secondDetailTableView.reloadData()
        }
    }
       
       private func recheckVM() {
           if self.viewModel == nil {
               self.viewModel = VISecondDetailViewM()
           }
        if self.recoveryViewModel == nil {
            self.recoveryViewModel = RDBidListViewM()
        }
       }
       
       private func setupTableView() {
           self.registerNibs()
           self.secondDetailTableView.delegate = self
           self.secondDetailTableView.dataSource = self
           self.secondDetailTableView.separatorStyle = .none
           
       }
    
       private func setupButton() {
              Threads.performTaskInMainQueue {
                  self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
                self.gradientView.backgroundColor = .redButtonColor
                  self.jobCompletedButton.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
              }
        if self.tenderStatus == .completedTenders {
            self.gradientView.isHidden = true
            self.jobCompletedButton.isHidden = true
        } else {
            self.gradientView.isHidden = false
            self.jobCompletedButton.isHidden = false
        }
        
            
          }
       
       private func registerNibs() {
           self.secondDetailTableView.register(STCarDetailCell.self)
           self.secondDetailTableView.register(STBidsCountCell.self)
           self.secondDetailTableView.register(BidDetailCell.self)
           self.secondDetailTableView.register(VIContactInfoCell.self)
           self.secondDetailTableView.register(SPDetailCell.self)
       }
    
    //MARK: - APIMethods
       private func requestTenderDetailsAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
        //params[ConstantAPIKeys.type] = 1 as AnyObject
        self.recoveryViewModel?.requestTenderDetailsAPI(params: params, completion: {[weak self] (tenderDetails) in
            guard let sSelf = self else { return }
            sSelf.tenderDetails = tenderDetails
            sSelf.loadDataSource()
        })
    }

    
    //MARK: IBActions
    @IBAction func tapJobCompletedButton(_ sender: Any) {
        let jobCompletedVC = DIConfigurator.sharedInstance.getJobDoneViewC()
        if let tenderDetails = self.tenderDetails {
            jobCompletedVC.serviceProviderId = Helper.toInt(tenderDetails.Bids?.serviceProviderId)
            jobCompletedVC.tenderId = Helper.toInt(tenderDetails.id)
            
        }
        self.navigationController?.pushViewController(jobCompletedVC, animated: true)
    }
}
