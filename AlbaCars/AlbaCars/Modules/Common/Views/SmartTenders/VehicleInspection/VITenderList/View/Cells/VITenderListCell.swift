//
//  VITenderListCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class VITenderListCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bidsView: UIView!
    @IBOutlet weak var bidsLabel: UILabel!
    @IBOutlet weak var bidsCountLabel: UILabel!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var timerInfo1Label: UILabel!
    @IBOutlet weak var timerInfo2Label: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var tenderStatusButton: UIButton!
    @IBOutlet weak var completedImageView: UIImageView!
    @IBOutlet weak var dataView: UIView!
    
    
    //MARK: - Life Cycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.timerInfo1Label.text = ""
        self.timerInfo2Label.text = ""
        Threads.performTaskInMainQueue {
            self.bidsView.roundCorners([.bottomLeft, .bottomRight], radius: Constants.UIConstants.sizeRadius_14)
            self.bgView.roundCorners(Constants.UIConstants.sizeRadius_7)
            self.bgView.addShadow(ofColor: UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
            self.dataView.roundCorners(Constants.UIConstants.sizeRadius_7)
        }
    }
    
    func setupCellData(tenderStatus: TenderStatus, cellInfo: AllTendersModel) {
        
        
    }
    
    //MARK: - Public Methods
    func configureView(tenderModel: AllTendersModel) {
        //setting date timer labels
        self.dateLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.inspectionDate.localizedString(), secondText: " \(Helper.toString(object: tenderModel.getDate()))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Book)
        self.timeLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.inspectionTime.localizedString(), secondText: " \(Helper.toString(object: tenderModel.getTime()))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Book)
        self.carModelLabel.text = Helper.toString(object: tenderModel.carTitle())
        self.bidsCountLabel.text = Helper.toString(object: tenderModel.bidsCount)
        
        switch tenderModel.tenderStatus() {
        case .inProgress:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
            self.timerInfo1Label.isHidden = false
            if let timerDuration = tenderModel.endTimerDuration {
                self.timerInfo1Label.text = timerDuration.convertToTimerFormat()
            }

            self.timerInfo1Label.textColor = .red
            self.timerInfo2Label.isHidden = true
        self.tenderStatusButton.setTitle(StringConstants.Text.tenderInProgress.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .greenColor
            
        case .bidAccepted:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
            self.timerInfo1Label.isHidden = true
            self.timerInfo2Label.isHidden = true
        self.tenderStatusButton.setTitle(StringConstants.Text.tenderBidAccepted.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .yellowColor
            
        case .noBidAccepted:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
            self.timerInfo1Label.isHidden = false
            self.timerInfo2Label.isHidden = false
            self.timerInfo1Label.text = "\(Helper.toString(object: tenderModel.timeLeft())) \(StringConstants.Text.tenderComplete.localizedString())"
            self.timerInfo1Label.textColor = .grayTextColor
            if let timerDuration = tenderModel.endTimerDuration {
                self.timerInfo2Label.attributedText =  String.getAttributedText(firstText: " \(Helper.toString(object: timerDuration.convertToTimerFormat()))", secondText: " \(StringConstants.Text.toAcceptBid.localizedString())", firstTextColor: .redButtonColor, secondTextColor: .greenTopHeaderColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
            }
            
           self.tenderStatusButton.setTitle(StringConstants.Text.tenderNoBidsAccepted.localizedString(), for: .normal)
           self.tenderStatusButton.backgroundColor = .blackColor
            
        case .allBidsRejected:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
            self.timerInfo1Label.isHidden = false
            self.timerInfo2Label.isHidden = false
            self.timerInfo1Label.text = "\(Helper.toString(object: tenderModel.timeLeft())) \(StringConstants.Text.tenderComplete.localizedString())"
            self.timerInfo1Label.textColor = .grayTextColor
            self.timerInfo2Label.text = "\(Helper.toString(object: tenderModel.timeLeft())) \(StringConstants.Text.timeElapsed.localizedString())"
            self.timerInfo1Label.textColor = .grayTextColor
        self.tenderStatusButton.setTitle(StringConstants.Text.tenderAllBidsRejected.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .redButtonColor
            
        case .upcomingTenders:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
            self.timerInfo1Label.isHidden = false
            self.timerInfo2Label.isHidden = false
            if let timerDuration = tenderModel.startTimerDuration {
                self.timerInfo1Label.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.tenderStartsIn.localizedString())", secondText: " \(timerDuration.convertToTimerFormat())", firstTextColor: .grayTextColor, secondTextColor: .greenTopHeaderColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Book)
            }
            
            self.timerInfo2Label.attributedText =  String.getAttributedText(firstText:"\(StringConstants.Text.tenderDateTime.localizedString())", secondText: " \(Helper.toString(object: tenderModel.getDateTime()))", firstTextColor: .black, secondTextColor: .greenTopHeaderColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Book)
        self.tenderStatusButton.setTitle(StringConstants.Text.tenderUpcoming.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .blueTenderButtonColor
            
        case .completedTenders:
            self.timerInfo1Label.isHidden = true
            self.timerInfo2Label.isHidden = true
            self.tenderStatusButton.isHidden = true
            self.completedImageView.isHidden = false
            
        default:
            break
        }
    }
}
