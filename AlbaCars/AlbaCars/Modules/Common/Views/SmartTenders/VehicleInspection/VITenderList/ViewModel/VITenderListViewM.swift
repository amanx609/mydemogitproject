//
//  VITenderListViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit


protocol VITenderListVModeling: BaseVModeling {
    func getVITendersTableDataSource(tenderList: [AllTendersModel]) -> [CellInfo]
}

class VITenderListViewM: BaseViewM, VITenderListVModeling {
    func getVITendersTableDataSource(tenderList: [AllTendersModel]) -> [CellInfo] {
        var array = [CellInfo]()
        for i in 0..<tenderList.count {
            let tender = tenderList[i]
            var pendingTenderCellInfo = [String: AnyObject]()
            pendingTenderCellInfo[Constants.UIKeys.pendingStatus] = tender.tenderStatus() as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.locationDateInfo1] = tender.getDate() as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.locationDateInfo2] = tender.getTime() as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.timerInfo1] = tender.pickupLocation as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.id] = tender.id as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.destinationText] = tender.dropoffLocation as AnyObject
            pendingTenderCellInfo[ConstantAPIKeys.brandName] = tender.brandName as AnyObject
            pendingTenderCellInfo[ConstantAPIKeys.year] = tender.year as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.bidCount] = tender.bidsCount as AnyObject
           
            var cellHeight = CGFloat()
            switch tender.tenderStatus() {
            case .inProgress, .bidAccepted:
                cellHeight = Constants.CellHeightConstants.height_180
            case .noBidAccepted, .allBidsRejected:
                cellHeight = Constants.CellHeightConstants.height_240
            case .completedTenders:
                cellHeight = Constants.CellHeightConstants.height_140
            default:
                break
            }
            let pendingTenderCell = CellInfo(cellType: .VITenderListCell, placeHolder: "", value: "", info: pendingTenderCellInfo, height: cellHeight)
            array.append(pendingTenderCell)
        }

        return array
    }
    
}
