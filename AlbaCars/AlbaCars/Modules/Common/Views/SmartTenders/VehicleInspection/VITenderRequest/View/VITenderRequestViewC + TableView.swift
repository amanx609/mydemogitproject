//
//  VITenderRequestViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces

extension VITenderRequestViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tenderRequestDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.tenderRequestDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let isPickUpDropOff =  self.tenderRequestDataSource[12].info?[Constants.UIKeys.isSelected] as? Bool {
            if isPickUpDropOff {
                return self.tenderRequestDataSource[indexPath.row].height
            } else {
                let cellInfo = self.tenderRequestDataSource[indexPath.row]
                guard let cellType = cellInfo.cellType else { return Constants.CellHeightConstants.height_0 }
                if cellType == .LocationTextFieldsCell {
                    return Constants.CellHeightConstants.height_0
                }
                
            }
        }
        return self.tenderRequestDataSource[indexPath.row].height
    }
}

extension VITenderRequestViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .LocationTextFieldsCell:
            let cell: LocationTextFieldsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.targetViewController = self
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .DateRangeCell:
            let cell: DateRangeCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureAuctionConfirmationViewCell(cellInfo: cellInfo)
            return cell
            
        case .TextInputCell:
            let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .DropDownCell:
            let cell: DropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureViewForRequestForm(cellInfo: cellInfo)
            return cell
            
        case .ImageDropDownCell:
            let cell: ImageDropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .NoReserveCell:
            let cell: NoReserveCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .TextViewCell:
            let cell: TextViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - DateRangeCellDelegate
extension VITenderRequestViewC: DateRangeCellDelegate {
    func didTapFromDate(cell: DateRangeCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.selectedIndexPath = indexPath
        self.isDate = true
        self.setupDateTimePicker(cell.fromTextField)
    }
    
    func didTapToDate(cell: DateRangeCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.selectedIndexPath = indexPath
        self.isDate = false
        self.setupDateTimePicker(cell.toTextField)
    }
}

//MARK: - TextViewCellDelegate
extension VITenderRequestViewC: TextViewCellDelegate {
    func didChangeText(cell: TextViewCell, text: String) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].value = text
    }
}

//MARK: - TextInputCellDelegate
extension VITenderRequestViewC: TextInputCellDelegate {
    func tapNextKeyboard(cell: TextInputCell) {
        self.view.endEditing(true)
    }
    
    func didChangeText(cell: TextInputCell, text: String) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].value = text
    }
}

//MARK: - DropDownCellDelegate
extension VITenderRequestViewC: DropDownCellDelegate {
    func didTapDropDownCell(cell: DropDownCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.showDropDownAt(indexPath: indexPath)
    }
}

//MARK: - ImageDropDownCellDelegate
extension VITenderRequestViewC: ImageDropDownCellDelegate {
    func didTapImageDropDownCell(cell: ImageDropDownCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.showDropDownAt(indexPath: indexPath)
    }
}

//MARK: - NoReserveCellDelegate
extension VITenderRequestViewC: NoReserveCellCellDelegate {
    func didTapNoReserve(cell: NoReserveCell, isReserved: Bool) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.isSelected] = isReserved as AnyObject
        self.view.endEditing(true)
        self.tenderRequestTableView.reloadData()
    }
}

//MARK: - LocationTextFieldCellDelegate
extension VITenderRequestViewC: LocationTextFieldsCellDelegate {
    
    func didChangeSourceText(cell: LocationTextFieldsCell, text: String, latitude: Double, longitude: Double) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.sourceValue] = text as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.sourceLatitude] = latitude as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.sourceLongitude] = longitude as AnyObject
    }
    
    func didChangeDestinationText(cell: LocationTextFieldsCell, text: String, latitude: Double, longitude: Double) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.destinationValue] = text as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.destinationLatitude] = latitude as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.destinationLongitude] = longitude as AnyObject
    }
    
    func tapNextKeyboard(cell: LocationTextFieldsCell) {
        self.view.endEditing(true)
    }
    
}

extension VITenderRequestViewC: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        
        if self.isSource {
            self.tenderRequestDataSource[13].info?[Constants.UIKeys.sourceValue] = place.name as AnyObject
            
        } else {
            self.tenderRequestDataSource[13].info?[Constants.UIKeys.destinationValue] = place.name as AnyObject
        }
        self.tenderRequestTableView.reloadData()
        // textField.text = place.name
        // Dismiss the GMSAutocompleteViewController when something is selected
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}




