//
//  JobDoneViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/15/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class JobDoneViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var ratingView: RatingView!
    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var jobCompletedLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    //MARK: - Variables
    var viewModel: JobDoneViewModeling?
    var serviceProviderId: Int?
    var tenderId: Int?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupView()
    }
    
    private func setupView() {
        self.setupNavigationBarTitle(title: "Job Completed".localizedString(), barColor: .white, titleColor: .black, leftBarButtonsType: [.back], rightBarButtonsType: [])
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.submitButton.setTitle("Submit".localizedString(), for: .normal)
        self.jobCompletedLabel.text = "Job Completed".localizedString()
        self.descriptionLabel.text = "Please provide feedback about, How Satisfied are you with Service Provider".localizedString()
        self.bgView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.reviewTextView.delegate =  self
        self.recheckVM()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = JobDoneViewM()
        }
    }
    
    private func validateParams() {
        if self.ratingView.rating == 0 {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide rating to service provider.")
        }
    }
    
    private func requestJobDoneAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
        params[ConstantAPIKeys.serviceProviderId] = self.serviceProviderId as AnyObject
        params[ConstantAPIKeys.rating] = self.ratingView.rating as AnyObject
        params[ConstantAPIKeys.review] = self.reviewTextView.text as AnyObject
        
        self.viewModel?.requestJobDoneAPI(param: params, completion: {[weak self] (success) in
            guard let sSelf = self else { return }
            if let viewControllers = sSelf.navigationController?.viewControllers {
                //CreateAccountViewCIsPresentInNavigationStack
                for viewController in viewControllers {
                    if viewController is RecoveryHomeViewC {
                        sSelf.navigationController?.popToViewController(viewController, animated: true)
                        return
                    }
                }
                
            }
        })
    }
    
    //MARK: - IBAction
    @IBAction func tapSubmitButton(_ sender: Any) {
        self.requestJobDoneAPI()
    }
    
}

extension JobDoneViewC: UITextViewDelegate {
    
}
