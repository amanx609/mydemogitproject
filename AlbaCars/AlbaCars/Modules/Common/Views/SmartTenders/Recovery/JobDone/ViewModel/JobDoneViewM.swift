//
//  JobDoneViewM.swift
//  AlbaCars
//
//  Created by Narendra on 2/5/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol JobDoneViewModeling: BaseVModeling {
    func requestJobDoneAPI(param: APIParams, completion: @escaping (Bool) -> Void)
}

class JobDoneViewM: BaseViewM, JobDoneViewModeling {
    
    func requestJobDoneAPI(param: APIParams, completion: @escaping (Bool) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .jobCompleted(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                {
                    completion(success)
                }
            }
        }
    }
    
}
