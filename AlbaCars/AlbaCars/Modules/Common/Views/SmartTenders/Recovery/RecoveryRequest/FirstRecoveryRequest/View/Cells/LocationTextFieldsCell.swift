//
//  LocationTextFieldsCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/15/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
import GooglePlaces

protocol LocationTextFieldsCellDelegate: class {
    func didChangeSourceText(cell: LocationTextFieldsCell, text: String, latitude: Double, longitude: Double)
    func didChangeDestinationText(cell: LocationTextFieldsCell, text: String, latitude: Double, longitude: Double)
    func tapNextKeyboard(cell: LocationTextFieldsCell)
}

class LocationTextFieldsCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bg1View: UIView!
    @IBOutlet weak var bg2View: UIView!
    @IBOutlet weak var sourceTextField: UITextField!
    @IBOutlet weak var destinationTextField: UITextField!
    
    //MARK: - Variables
    weak var delegate: LocationTextFieldsCellDelegate?
    weak var targetViewController: BaseViewC?
    var isSourceTextField = false
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK: - Private Methods
    private func setupView() {
       self.bg1View.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.bg2View.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
        self.sourceTextField.delegate = self
        self.destinationTextField.delegate = self
     }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
      //Placeholder
        if let info = cellInfo.info {
            let sourcePlaceHolder = Helper.toString(object: info[Constants.UIKeys.sourcePlaceHolder])
            let destPlaceHolder = Helper.toString(object: info[Constants.UIKeys.destinationPlaceHolder])
            sourceTextField.setPlaceHolderColor(text: sourcePlaceHolder, color: UIColor.blackColor)
            destinationTextField.setPlaceHolderColor(text: destPlaceHolder, color: UIColor.blackColor)
            self.sourceTextField.text = Helper.toString(object: info[Constants.UIKeys.sourceValue])
            self.destinationTextField.text = Helper.toString(object: info[Constants.UIKeys.destinationValue])
           
        }
    }
}

extension LocationTextFieldsCell: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case sourceTextField: self.isSourceTextField = true
        case destinationTextField: self.isSourceTextField = false
        default:
            break
        }
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        self.targetViewController?.present(acController, animated: true, completion: nil)
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       if let delegate = self.delegate {
         delegate.tapNextKeyboard(cell: self)
       }
       return true
     }
       
       func textFieldDidEndEditing(_ textField: UITextField) {
           if let delegate = delegate, let text = textField.text {
               //delegate.didChangeSourceText(cell: self, text: text)
           }
       }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text), let delegate = self.delegate {
            let finalText = text.replacingCharacters(in: textRange, with: string)
            if finalText.count > Constants.Validations.maxaddressLength {
              return false
            }
            
            
//            switch textField {
//            case sourceTextField: delegate.didChangeSourceText(cell: self, text: finalText)
//            case destinationTextField: delegate.didChangeDestinationText(cell: self, text: finalText)
//            default:
//                break
//            }
        }
        
        return true
    }
}

extension LocationTextFieldsCell: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        print(place.name)
        
        if isSourceTextField {
            if let delegate = self.delegate {
                sourceTextField.text = place.name
                delegate.didChangeSourceText(cell: self, text: place.name ?? "", latitude: place.coordinate.latitude , longitude: place.coordinate.longitude)
            }
        } else {
            if let delegate = self.delegate {
                          destinationTextField.text = place.name
                delegate.didChangeDestinationText(cell: self, text: place.name ?? "", latitude: place.coordinate.latitude , longitude: place.coordinate.longitude)
                }
        }
        //textField.text = place.name
    // Dismiss the GMSAutocompleteViewController when something is selected
        self.targetViewController?.dismiss(animated: true, completion: nil)
      }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
      }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        self.targetViewController?.dismiss(animated: true, completion: nil)
      }
    
}
