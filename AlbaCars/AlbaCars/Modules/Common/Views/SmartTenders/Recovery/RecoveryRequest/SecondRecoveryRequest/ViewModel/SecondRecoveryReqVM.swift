//
//  SecondRecoveryReqVM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SecondRecoveryReqVModeling: BaseVModeling {
    func getSecondRecoveryReqDataSource() -> [CellInfo]
    func requestTenderAPI(arrData: [CellInfo],firstFormParams: APIParams,  completion: @escaping (Bool)-> Void)
}

class SecondRecoveryReqVM: SecondRecoveryReqVModeling {
    
    func getSecondRecoveryReqDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //contact Label Cell
        var contactLabelInfo = [String: AnyObject]()
        contactLabelInfo[Constants.UIKeys.placeholderText] = "Pick Up Contact Info".localizedString() as AnyObject
        contactLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let contactLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "", value: "", info: contactLabelInfo, height: Constants.CellHeightConstants.height_80)
        array.append(contactLabelCell)
        
        //Full Name Cell
        var fullNameInfo = [String: AnyObject]()
        fullNameInfo[Constants.UIKeys.inputType] = TextInputType.name as AnyObject
        let fullNameCell = CellInfo(cellType: .TextInputCell, placeHolder: "Contact Person Name".localizedString(), value: "", info: fullNameInfo, height: Constants.CellHeightConstants.height_80)
        array.append(fullNameCell)
        
        //Mobile Cell
        var mobileInfo = [String: AnyObject]()
        mobileInfo[Constants.UIKeys.countryCodePlaceholder] = Helper.toString(object: UserSession.shared.countryCode) as AnyObject
        let mobileCell = CellInfo(cellType: .PhoneCell, placeHolder: "Mobile".localizedString(), value: "", info: mobileInfo, height: Constants.CellHeightConstants.height_80, keyboardType: .numberPad)
        array.append(mobileCell)
        
        //choose Date time Label Cell
        var chooseLabelInfo = [String: AnyObject]()
        chooseLabelInfo[Constants.UIKeys.placeholderText] = "Drop off Contact Info".localizedString() as AnyObject
        chooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "", value: "", info: chooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseLabelCell)
        
        // NoReserve Cell
        var pickUpInfo = [String: AnyObject]()
        pickUpInfo[Constants.UIKeys.isSelected] = false as AnyObject
        pickUpInfo[Constants.UIKeys.shouldHide] = true as AnyObject
        let pickUpCell = CellInfo(cellType: .NoReserveCell, placeHolder: "Same as Pickup".localizedString(), value:"" ,info: pickUpInfo
            , height: Constants.CellHeightConstants.height_50)
        array.append(pickUpCell)
        
        //Full Name Cell
        var fullName2Info = [String: AnyObject]()
        fullName2Info[Constants.UIKeys.inputType] = TextInputType.name as AnyObject
        let fullName2Cell = CellInfo(cellType: .TextInputCell, placeHolder: "Contact Person Name".localizedString(), value:"", info: fullName2Info, height: Constants.CellHeightConstants.height_80)
        array.append(fullName2Cell)
        
       //Mobile Cell
        var mobile2Info = [String: AnyObject]()
        mobile2Info[Constants.UIKeys.countryCodePlaceholder] = Helper.toString(object: UserSession.shared.countryCode) as AnyObject
        let mobile2Cell = CellInfo(cellType: .PhoneCell, placeHolder: "Mobile".localizedString(), value: "", info: mobile2Info, height: Constants.CellHeightConstants.height_80, keyboardType: .numberPad)
        array.append(mobile2Cell)
        
        //TextView Cell
        let textViewCell = CellInfo(cellType: .TextViewCell, placeHolder: "Additional Information".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_150)
        array.append(textViewCell)
        
        return array
    }
    
    func requestTenderAPI(arrData: [CellInfo],firstFormParams: APIParams,  completion: @escaping (Bool)-> Void) {
          if self.validateFormData(arrData: arrData) {
              let tenderRequestParams = self.getAPIParams(arrData: arrData, firstFormParams: firstFormParams)
              APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderRequestForm(param: tenderRequestParams))) { (response, success) in
                  if success {
                      if let safeResponse =  response as? [String: AnyObject]
                         // let result = safeResponse[ConstantAPIKeys.result] as? Int
                      {
                        
                          completion(success)
                      }
                  }
              }
          }
      }
      
    
    func validateFormData(arrData: [CellInfo]) -> Bool {
        var isValid = true
        if let pickupName = arrData[1].value, pickupName.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter contact person name for pickup.".localizedString())
            isValid = false
        } else if let pickupMobile = arrData[2].value, pickupMobile.isEmpty  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter contact number for pickup.".localizedString())
            isValid = false
        } else if let dropName = arrData[5].value, dropName.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message:"Please enter contact person name for dropoff.".localizedString())
            isValid = false
        } else if let dropMobile = arrData[6].value, dropMobile.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter contact number for dropoff.".localizedString())
            isValid = false
        }
        return isValid
    }
    
    func getAPIParams(arrData: [CellInfo], firstFormParams: APIParams) -> APIParams {
        var params: APIParams = APIParams()
        params = firstFormParams
        params[ConstantAPIKeys.pickupPersonName] = arrData[1].value as AnyObject
        params[ConstantAPIKeys.pickupMobile] = arrData[2].value as AnyObject
        params[ConstantAPIKeys.dropOffPersonName] = arrData[5].value as AnyObject
        params[ConstantAPIKeys.dropOffMobile] = arrData[6].value as AnyObject
        params[ConstantAPIKeys.type] = RecoveryHomeScreenType.recovery.rawValue as AnyObject
        params[ConstantAPIKeys.additionalInformation] = arrData[7].value as AnyObject
        return params
       }
}
