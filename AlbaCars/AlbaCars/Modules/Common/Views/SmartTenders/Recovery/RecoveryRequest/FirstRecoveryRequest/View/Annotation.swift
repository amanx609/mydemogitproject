//
//  Annotation.swift
//  AlbaCars
//
//  Created by Narendra on 3/20/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class Annotation: MKPointAnnotation {
    var identifier: Int = 0
}
