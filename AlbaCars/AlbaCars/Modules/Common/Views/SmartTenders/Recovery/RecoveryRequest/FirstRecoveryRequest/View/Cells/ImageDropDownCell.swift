//
//  ImageDropDownCell.swift
//  AlbaCars
//
//  Created by Admin on 1/15/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
protocol ImageDropDownCellDelegate: class {
  func didTapImageDropDownCell(cell: ImageDropDownCell)
}

class ImageDropDownCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var placeholderImageView: UIImageView!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    //MARK: - Properties
    weak var delegate: ImageDropDownCellDelegate?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK: - Private Methods
    private func setupView() {
      self.bgView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.placeholderLabel.text = cellInfo.value
    }
    
    
    //MARK: - IBAction
    
    @IBAction func tapCellSelectionButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapImageDropDownCell(cell: self)
        }
    }
    
}
