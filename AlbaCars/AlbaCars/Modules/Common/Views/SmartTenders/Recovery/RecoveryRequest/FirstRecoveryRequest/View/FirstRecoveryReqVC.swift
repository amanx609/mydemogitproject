//
//  FirstRecoveryReqVC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
//import MapKit
import CoreLocation
import GoogleMaps

class FirstRecoveryReqVC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var recoveryTableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet var dateTimePicker: UIDatePicker!
   // @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var gMapView: GMSMapView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    //Variables
    var viewModel: FirstRecoveryReqVModeling?
    var recoveryDataSource: [CellInfo] = []
    var vehicleId = Int()
    var isDate = true
    var addCarViewModel: CAddNewCarViewModeling?
    var requestCarDataSource: [CellInfo] = []
    var vehicleBrandDataSource: [[String: AnyObject]] = []
    var vehicleTypeDataSource: [[String: AnyObject]] = []
    var vehicleModelDataSource: [[String: AnyObject]] = []
    var carDetails: ChooseCarModel?
    var sourceLocation:CLLocationCoordinate2D?
    var destinationLocation:CLLocationCoordinate2D?
    var selectedIndexPath = IndexPath()
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    //MARK: - Private Methods
    private func setup() {
        
        self.registerNibs()
        self.setupViewModel()
        self.setupView()
        self.setupTableView()
        self.loadDataSource()
        self.requestVehicleBrandAPI()
        self.requestUserLocation()
        //self.mapView.delegate = self
        self.gMapView.delegate = self
    }
    
    private func requestUserLocation() {
        
        DeviceSettings.checkLocationSettings(self) { (success) in
            if success {
                LocationManager.sharedInstance.determineCurrentLocation()
                LocationManager.sharedInstance.currentLocationProvider = {
                    location in
//                    let viewRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 1200, longitudinalMeters: 1200)
//                    self.mapView.setRegion(viewRegion, animated: true)
//                    self.mapView.showsUserLocation = true
                    let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude:location.coordinate.longitude, zoom: 8.0)
                    self.gMapView.camera = camera
                    self.gMapView.isMyLocationEnabled = true
                }
            }
        }
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.nextButton.setTitle("Next".localizedString(), for: .normal)
    }
    
    private func setupTableView() {
        self.recoveryTableView.separatorStyle = .none
        self.recoveryTableView.backgroundColor = UIColor.white
        self.recoveryTableView.delegate = self
        self.recoveryTableView.dataSource = self
    }
    
    private func setupViewModel() {
        
        if self.viewModel == nil {
            self.viewModel = FirstRecoveryReqVM()
        }
        if self.addCarViewModel == nil {
            self.addCarViewModel = CAddNewCarVM()
        }
    }
    
    private func registerNibs() {
        self.recoveryTableView.register(LocationTextFieldsCell.self)
        self.recoveryTableView.register(DropDownCell.self)
        self.recoveryTableView.register(DateRangeCell.self)
        self.recoveryTableView.register(BottomLabelCell.self)
        self.recoveryTableView.register(ImageDropDownCell.self)
        self.recoveryTableView.register(TextInputCell.self)
    }
    
    private func loadDataSource() {
        
        if let dataSource = self.viewModel?.getFirstRecoveryReqDataSource(carDetails: self.carDetails) {
            self.recoveryDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.recoveryTableView.reloadData()
            }
        }
    }
    
    private func updateDateTime(_ pickerValue: String) {
        
        if isDate {
            self.recoveryDataSource[selectedIndexPath.row].placeHolder = pickerValue
        } else {
            self.recoveryDataSource[selectedIndexPath.row].value = pickerValue
        }
        
        self.recoveryTableView.reloadData()
    }
    
    //MARK: - Public Methods
    func setupDateTimePicker(_ textField: UITextField) {
        textField.inputView = self.dateTimePicker
        textField.inputAccessoryView = Helper.getDoubleButtonToolBar(leftButtonTitle: StringConstants.Text.Cancel.localizedString(), rightButtonTitle: StringConstants.Text.done.localizedString(), leftButtonselector: #selector(self.cancelButtonTapped), rightButtonSelector: #selector(self.doneButtonTapped), target: self)
        if isDate {
            self.dateTimePicker.datePickerMode = .date
        } else {
            self.dateTimePicker.datePickerMode = .time
        }
        self.dateTimePicker.minimumDate = Date()
    }
    
    func setupToolBar(_ textField: UITextField) {
        textField.inputAccessoryView = Helper.getSingleButtonToolBar(buttonTitle: StringConstants.Text.done.localizedString(), selector: #selector(self.doneButtonTapped), target: self)
    }
    
    func showDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        switch indexPath.row {
        case 3:
            if self.vehicleBrandDataSource.count == 0 {
                self.requestVehicleBrandAPI()
                return
            }
            listPopupView.initializeViewWith(title: "Select Brand".localizedString(), arrayList: self.vehicleBrandDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let brandId = response[ConstantAPIKeys.id] as? Int,
                    let brandName = response[ConstantAPIKeys.title] as? String else { return }
                sSelf.requestVehicleModelAPI(brandId: brandId)
                sSelf.recoveryDataSource[indexPath.row].value = brandName
                sSelf.recoveryDataSource[indexPath.row].placeHolder = "\(brandId)"
                sSelf.recoveryTableView.reloadRows(at: [indexPath], with: .none)
            }
        case 4:
            if self.vehicleModelDataSource.count == 0 {
                
                return
            }
            listPopupView.initializeViewWith(title: "Model Name".localizedString(), arrayList: self.vehicleModelDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let modelName = response[ConstantAPIKeys.title] as? String,
                    let modelId = response[ConstantAPIKeys.id] as? Int else { return }
                sSelf.requestVehicleTypeAPI()
                sSelf.recoveryDataSource[indexPath.row].value = modelName
                sSelf.recoveryDataSource[indexPath.row].placeHolder = "\(modelId)"
                sSelf.recoveryTableView.reloadRows(at: [indexPath], with: .none)
            }
            
        case 5:
            if self.vehicleTypeDataSource.count == 0 {
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Type".localizedString(), arrayList: self.vehicleTypeDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let carType = response[ConstantAPIKeys.title] as? String,
                    let carId = response[ConstantAPIKeys.id] as? Int else { return }
                sSelf.recoveryDataSource[indexPath.row].value = carType
                sSelf.recoveryDataSource[indexPath.row].placeHolder = "\(carId)"
                sSelf.recoveryTableView.reloadRows(at: [indexPath], with: .none)
            }
            
        case 6:
            listPopupView.initializeViewWith(title: DropDownType.recoveryType.title, arrayList: DropDownType.recoveryType.dataList, key: DropDownType.recoveryType.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let employees = response[DropDownType.recoveryType.rawValue] as? String else { return }
                sSelf.recoveryDataSource[indexPath.row].value = employees
                sSelf.recoveryTableView.reloadRows(at: [indexPath], with: .none)
            }
            
        case 12:
            listPopupView.initializeViewWith(title: "Select Time Duration", arrayList: Constants.tenderDuration, key: Constants.UIKeys.duration) { [weak self] (response) in
                guard let sSelf = self,
                    let timeDuration = response[ConstantAPIKeys.duration] as? String, let apiValue = response[Constants.UIKeys.value] as? String else { return }
                sSelf.recoveryDataSource[indexPath.row].value = timeDuration
                sSelf.recoveryDataSource[indexPath.row].placeHolder = apiValue
                sSelf.recoveryTableView.reloadRows(at: [indexPath], with: .none)
            }
            
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    //MARK: - API Methods
    func requestVehicleBrandAPI() {
        self.addCarViewModel?.requestVehicleBrandAPI(completion: { (success, vehicleBrands) in
            if success {
                self.vehicleBrandDataSource = vehicleBrands
            }
        })
    }
    
    func requestVehicleModelAPI(brandId: Int) {
        self.addCarViewModel?.requestVehicleModelAPI(brandId: brandId, completion: { (success, vehicleModels) in
            if success {
                self.vehicleModelDataSource = vehicleModels
            }
        })
    }
    
    func requestVehicleTypeAPI() {
        self.addCarViewModel?.requestVehicleTypeAPI(completion: { (success, vehicleTypes) in
            if success {
                self.vehicleTypeDataSource = vehicleTypes
            }
        })
    }
    
    
    func drowLineOnMap() {
        self.gMapView.clear()
        
        //        let allAnnotations = self.mapView.annotations
        //        self.mapView.removeAnnotations(allAnnotations)
        var bounds = GMSCoordinateBounds()
        
        if let locationCoordinate = self.sourceLocation {
            //            let annotation = Annotation()
            //            annotation.identifier = 0
            //            annotation.coordinate = locationCoordinate
            //            self.mapView.addAnnotation(annotation)
            //
            //            let region = MKCoordinateRegion(center: locationCoordinate, span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
            //            self.mapView.setRegion(region, animated: true)
                        
            //creating a marker view
            let markerView = UIImageView(image: UIImage(named: "greenMarker"))
            let marker = GMSMarker()
            marker.position = locationCoordinate
            marker.iconView = markerView
            marker.map = self.gMapView
            bounds = bounds.includingCoordinate(marker.position)

        }
        
        if let locationCoordinate = self.destinationLocation {
            //            let annotation = Annotation()
            //            annotation.identifier = 1
            //            annotation.coordinate = locationCoordinate
            //            self.mapView.addAnnotation(annotation)
            //
            //            let region = MKCoordinateRegion(center: locationCoordinate, span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
            //            self.mapView.setRegion(region, animated: true)
            
            // I have taken a pin image which is a custom image
            
            //creating a marker view
            let markerView = UIImageView(image: UIImage(named: "redMarker"))
            let marker = GMSMarker()
            marker.position = locationCoordinate
            marker.iconView = markerView
            marker.map = self.gMapView
            bounds = bounds.includingCoordinate(marker.position)
            
        }
        
        if let destinationLocation = self.destinationLocation, let sourceLocation = self.sourceLocation {
            
//            for poll in self.mapView.overlays {
//                self.mapView.removeOverlay(poll)
//            }
//
//            // Store the coordinates in an array.
//            var coordinates_1 = [sourceLocation,destinationLocation]
//            // Create polyline.
//            let myPolyLine_1: MKPolyline = MKPolyline(coordinates: &coordinates_1, count: coordinates_1.count)
//            // Add circle to mapView.
//            self.mapView.addOverlay(myPolyLine_1)
            
            let path = GMSMutablePath()
            path.add(sourceLocation)
            path.add(destinationLocation)
            let polyline = GMSPolyline(path: path)
//            polyline.strokeColor = .black
//            polyline.strokeWidth = 3.0
//            polyline.map = self.gMapView
            
            let strokeStyles = [GMSStrokeStyle.solidColor(.black), GMSStrokeStyle.solidColor(.clear)]
            let strokeLengths = [NSNumber(value: 10), NSNumber(value: 10)]
            if let path = polyline.path {
              polyline.spans = GMSStyleSpans(path, strokeStyles, strokeLengths, .rhumb)
            }
            polyline.map = self.gMapView
        }
        
       // self.mapView.showAnnotations(self.mapView.annotations, animated: true)
        
        self.gMapView.setMinZoom(1, maxZoom: 10)
        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
        self.gMapView.animate(with: update)
    }
    
    //MARK: - Selectors
    @objc func cancelButtonTapped() {
        self.view.endEditing(true)
    }
    
    @objc func doneButtonTapped() {
        self.view.endEditing(true)
        let selectedDate = self.dateTimePicker.date
        var convertedValue = ""
        if isDate {
            convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatWithoutSpace)
        } else {
            convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.timeFormathmma)
            //self.recoveryDataSource[selectedIndexPath.row].info?[Constants.UIKeys.secondValue] = selectedDate.dateStringWith(strFormat: Constants.Format.apiTimeFormat) as AnyObject
        }
        updateDateTime(convertedValue)
    }
        
    //MARK: - IBActions
    @IBAction func tapNextButton(_ sender: Any) {
        if self.viewModel?.validateFormParameters(arrData: self.recoveryDataSource) ?? false {
            if let params = self.viewModel?.getAPIParams(arrData: self.recoveryDataSource, vehicleId: self.vehicleId) {
                let secondRecoveryReqVC = DIConfigurator.sharedInstance.getSecondRecoveryReqVC()
                secondRecoveryReqVC.firstFormParameters = params
                self.navigationController?.pushViewController(secondRecoveryReqVC, animated: true)
            }
        } else {
            return
        }
    }
        
    @IBAction func tapBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
//
//extension FirstRecoveryReqVC: MKMapViewDelegate {
//
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//
//        if annotation is MKUserLocation {
//            return nil
//        }
//
//        var imageName =  ""
//        if annotation is Annotation {
//            let annot = annotation as! Annotation
//            imageName = annot.identifier == 0 ? "greenMarker" : "redMarker"
//        }
//
//        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "Customannotation")
//        annotationView.image = UIImage(named: imageName)
//        annotationView.canShowCallout = false
//        return annotationView
//    }
//
//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//        guard let polyline = overlay as? MKPolyline else {
//            fatalError("Not a MKPolyline")
//        }
//        let renderer = MKPolylineRenderer(polyline: polyline)
//        renderer.strokeColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
//        renderer.lineWidth = 2
//        renderer.lineDashPattern = [4, 4]
//        return renderer
//    }
//}

extension FirstRecoveryReqVC: GMSMapViewDelegate {


}
