//
//  SecondRecoveryReqVC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SecondRecoveryReqVC: BaseViewC {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var recoveryTableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    
    //Variables
    var viewModel: SecondRecoveryReqVModeling?
    var recoveryDataSource: [CellInfo] = []
    var isDate = true
    var firstFormParameters: APIParams = APIParams()
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "New Recovery Request".localizedString(), barColor: .white, titleColor: .black, leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.registerNibs()
        self.setupViewModel()
        self.setupView()
        self.setupTableView()
        self.loadDataSource()
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.submitButton.setTitle("Submit".localizedString(), for: .normal)
        
    }
    
    private func setupTableView() {
        self.recoveryTableView.separatorStyle = .none
        self.recoveryTableView.backgroundColor = UIColor.white
        self.recoveryTableView.delegate = self
        self.recoveryTableView.dataSource = self
        
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = SecondRecoveryReqVM()
        }
    }
    
    private func registerNibs() {
        self.recoveryTableView.register(TextInputCell.self)
        self.recoveryTableView.register(DropDownCell.self)
        self.recoveryTableView.register(TextViewCell.self)
        self.recoveryTableView.register(BottomLabelCell.self)
        self.recoveryTableView.register(NoReserveCell.self)
        self.recoveryTableView.register(PhoneCell.self)
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getSecondRecoveryReqDataSource() {
            self.recoveryDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.recoveryTableView.reloadData()
            }
        }
    }
    
    //MARK: - Public Methods
    
    //MARK: - IBActions
    @IBAction func tapSubmitButton(_ sender: Any) {
        self.viewModel?.requestTenderAPI(arrData: self.recoveryDataSource, firstFormParams: self.firstFormParameters, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            if let viewControllers = sSelf.navigationController?.viewControllers {
              //CreateAccountViewCIsPresentInNavigationStack
              for viewController in viewControllers {
                if viewController is RecoveryHomeViewC {
                  sSelf.navigationController?.popToViewController(viewController, animated: true)
                  return
                }
              }
              
            }

        })
        
    }
    
}
