//
//  FirstRecoveryReqVM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol FirstRecoveryReqVModeling: BaseVModeling {
    func getFirstRecoveryReqDataSource(carDetails: ChooseCarModel?) -> [CellInfo]
    func validateFormParameters(arrData: [CellInfo]) -> Bool
    func getAPIParams(arrData: [CellInfo], vehicleId: Int) -> APIParams
}

class FirstRecoveryReqVM: FirstRecoveryReqVModeling {
    
    func getFirstRecoveryReqDataSource(carDetails: ChooseCarModel?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Location cell
        var recoveryRequestInfo = [String: AnyObject]()
        recoveryRequestInfo[Constants.UIKeys.sourcePlaceHolder] = "Pick Up Location".localizedString() as AnyObject
        recoveryRequestInfo[Constants.UIKeys.destinationPlaceHolder] = "Drop Off Location".localizedString() as AnyObject
        recoveryRequestInfo[Constants.UIKeys.sourceValue] = "".localizedString() as AnyObject
        recoveryRequestInfo[Constants.UIKeys.destinationValue] = "".localizedString() as AnyObject
        
        let recoveryRequestCell = CellInfo(cellType: .LocationTextFieldsCell, placeHolder: "", value: "", info: recoveryRequestInfo, height: Constants.CellHeightConstants.height_220)
        array.append(recoveryRequestCell)
        
        //Car Plate Number
        var carPlateCellInfo = [String: AnyObject]()
        carPlateCellInfo[Constants.UIKeys.inputType] = TextInputType.plateNumber as AnyObject
        
        let carPlateCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.carPlate.localizedString(), value: Helper.toString(object: carDetails?.plateNumber), info: carPlateCellInfo, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carPlateCell)
        
        //Car brand
        let carBrandCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.brandName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carBrandCell)
        
        //Car Model
        let carModelCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.modelName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carModelCell)
        
        //Car Year
        let carYearCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.year), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carYearCell)
        
        //Car Type
        let carTypeCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.typeName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carTypeCell)
        
        //recovery Type
        let recoveryTypeCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: StringConstants.Text.typeOfRecovery.localizedString(), info: nil, height: Constants.CellHeightConstants.height_80)
        array.append(recoveryTypeCell)
        
        //choose Date time Label Cell
        var chooseLabelInfo = [String: AnyObject]()
        chooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Tender".localizedString() as AnyObject
        chooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Tender".localizedString(), value: "", info: chooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseLabelCell)
        
        //DateRange Cell
        var dateRangeInfo = [String: AnyObject]()
        dateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let dateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: dateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(dateRangeCell)
        
        //Job Date time Label Cell
        var jobChooseLabelInfo = [String: AnyObject]()
        jobChooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Job".localizedString() as AnyObject
        jobChooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let jobChooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Job".localizedString(), value: "", info: jobChooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(jobChooseLabelCell)
        
        //DateRange Cell
        var jobDateRangeInfo = [String: AnyObject]()
        jobDateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let jobDateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: jobDateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(jobDateRangeCell)
        
        //choose Duration Label Cell
        var chooseDurationLabelInfo = [String: AnyObject]()
        chooseDurationLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Tender Duration".localizedString() as AnyObject
        chooseDurationLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseDurationLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Tender Duration".localizedString(), value: "", info: chooseDurationLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseDurationLabelCell)
        
        //Car Model
        let tenderDurationCell = CellInfo(cellType: .ImageDropDownCell, placeHolder: "", value: StringConstants.Text.tenderDuration.localizedString(), info: nil, height: Constants.CellHeightConstants.height_80)
        array.append( tenderDurationCell)
        return array
    }
    
    func validateFormParameters(arrData: [CellInfo]) -> Bool {
        
        var isValid = true
        if let pickupLocation = arrData[0].info?[Constants.UIKeys.sourceValue] as? String, pickupLocation.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide your pickup location.".localizedString())
            isValid = false
        } else if let dropOffLocation = arrData[0].info?[Constants.UIKeys.destinationValue] as? String, dropOffLocation.isEmpty  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide your drop off location.".localizedString())
            isValid = false
        }
        else if let typeOfRecovery = arrData[6].value, typeOfRecovery == StringConstants.Text.typeOfRecovery.localizedString() {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select type of recovery.".localizedString())
            isValid = false
        } else if let date = arrData[8].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of tender.".localizedString())
            isValid = false
        } else if let time = arrData[8].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of tender.".localizedString())
            isValid = false
        } else if let date = arrData[10].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of job.".localizedString())
            isValid = false
        } else if let time = arrData[10].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of job.".localizedString())
            isValid = false
        }   else if let duration = arrData[12].value, (duration.isEmpty || duration == StringConstants.Text.tenderDuration.localizedString()) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select duration of tender.".localizedString())
            isValid = false
        }
        
        if isValid  {
          
            let dateStr = arrData[8].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let timeStr = arrData[8].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            
            let jobDateStr = arrData[10].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let jobTimeStr = arrData[10].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            
            let tenderDateTime = Helper.toString(object: dateStr) + " " +  Helper.toString(object: timeStr)
            let jobDateTime = Helper.toString(object: jobDateStr) + " " +  Helper.toString(object: jobTimeStr)

            isValid = Helper.compareJobAndTenderDateTime(tenderDateTime: tenderDateTime, jobDateTime: jobDateTime)
            
        }

        
        return isValid
    }
    
    /*
     else if let jobDate = arrData[10].placeHolder.getDateInstaceFrom(format: Constants.Format.dateFormatWithoutSpace), let jobTime = arrData[10].value, let tenderDate = arrData[8].placeHolder.getDateInstaceFrom(format: Constants.Format.dateFormatWithoutSpace), let tenderTime = arrData[8].value, tenderDate < jobDate   {
         Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Job date cannot be less than tender date.".localizedString())
         isValid = false
     } else if let jobTime = arrData[10].value.getDateInstaceFrom(format: Constants.Format.timeFormathmma), let tenderTime = arrData[8].value.getDateInstaceFrom(format: Constants.Format.timeFormathmma), tenderTime <= jobTime {
         Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Job Time cannot be less than tender time.".localizedString())
         isValid = false
     }
     */
    
    func getAPIParams(arrData: [CellInfo], vehicleId: Int) -> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        params[ConstantAPIKeys.pickupLocation] = arrData[0].info?[Constants.UIKeys.sourceValue] as AnyObject
        params[ConstantAPIKeys.pickupLocationLatitude] = arrData[0].info?[Constants.UIKeys.sourceLatitude] as AnyObject
        params[ConstantAPIKeys.pickupLocationLongitude] =  arrData[0].info?[Constants.UIKeys.sourceLongitude] as AnyObject
        params[ConstantAPIKeys.dropLocation] = arrData[0].info?[Constants.UIKeys.destinationValue] as AnyObject
        params[ConstantAPIKeys.dropLocationLatitude] = arrData[0].info?[Constants.UIKeys.destinationLatitude] as AnyObject
        params[ConstantAPIKeys.dropLocationLongitude] = arrData[0].info?[Constants.UIKeys.destinationLongitude] as AnyObject
        params[ConstantAPIKeys.recoveryType] = arrData[6].value as AnyObject
        
        let dateStr = arrData[8].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let timeStr = arrData[8].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.requestedDate] = dateStr as AnyObject
        params[ConstantAPIKeys.requestedTime] = timeStr as AnyObject
        
        let jobDateStr = arrData[10].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let jobTimeStr = arrData[10].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.jobDate] = jobDateStr as AnyObject
        params[ConstantAPIKeys.jobTime] = jobTimeStr as AnyObject
        
        params[ConstantAPIKeys.duration] = arrData[12].placeHolder as AnyObject
        
        return params
    }
}
