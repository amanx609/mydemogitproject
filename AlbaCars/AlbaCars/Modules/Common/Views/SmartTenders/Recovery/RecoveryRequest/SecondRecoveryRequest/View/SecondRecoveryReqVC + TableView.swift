//
//  SecondRecoveryReqVC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension SecondRecoveryReqVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recoveryDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.recoveryDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.recoveryDataSource[indexPath.row].height
    }
}

extension SecondRecoveryReqVC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
            
        case .TextInputCell:
            let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .TextViewCell:
            let cell: TextViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate =  self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .NoReserveCell:
            let cell: NoReserveCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .PhoneCell:
            let cell: PhoneCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - DropDownCellDelegate
extension SecondRecoveryReqVC: DateRangeCellDelegate {
    func didTapFromDate(cell: DateRangeCell) {
        self.isDate = true
    }
    
    func didTapToDate(cell: DateRangeCell) {
        self.isDate = false
    }
    
}

//MARK: - TextInputCellDelegate
extension SecondRecoveryReqVC: TextInputCellDelegate {
    func tapNextKeyboard(cell: TextInputCell) {
        self.view.endEditing(true)
    }
    
    func didChangeText(cell: TextInputCell, text: String) {
        guard let indexPath = self.recoveryTableView.indexPath(for: cell) else { return }
        self.recoveryDataSource[indexPath.row].value = text
    }
}


//MARK: - TextViewCellDelegate
extension SecondRecoveryReqVC: TextViewCellDelegate {
    func didChangeText(cell: TextViewCell, text: String) {
        guard let indexPath = self.recoveryTableView.indexPath(for: cell) else { return }
        self.recoveryDataSource[indexPath.row].value = text
    }
}

//MARK: - NoReserveCellCellDelegate
extension SecondRecoveryReqVC: NoReserveCellCellDelegate {
    func didTapNoReserve(cell: NoReserveCell, isReserved: Bool) {
         self.recoveryDataSource[4].info?[Constants.UIKeys.isSelected] = isReserved as AnyObject
        if isReserved {
           
            self.recoveryDataSource[5].value = self.recoveryDataSource[1].value
            self.recoveryDataSource[5].isUserInteractionEnabled = false
            self.recoveryDataSource[6].value = self.recoveryDataSource[2].value
            self.recoveryDataSource[6].isUserInteractionEnabled = false
            self.recoveryTableView.reloadData()
        } else {
            self.recoveryDataSource[5].value = ""
            self.recoveryDataSource[5].isUserInteractionEnabled = true
            self.recoveryDataSource[6].value = ""
            self.recoveryDataSource[6].isUserInteractionEnabled = true
            self.recoveryTableView.reloadData()
        }
        self.view.endEditing(true)
    }
}

//MARK: - PhoneCell Delegate
extension SecondRecoveryReqVC: PhoneCellDelegate {
  func didTapCountryCode(cell: PhoneCell) {
    
  }
  
  func didChangeText(cell: PhoneCell, text: String) {
    guard let indexPath = self.recoveryTableView.indexPath(for: cell) else { return }
    self.recoveryDataSource[indexPath.row].value = text
  }
}

