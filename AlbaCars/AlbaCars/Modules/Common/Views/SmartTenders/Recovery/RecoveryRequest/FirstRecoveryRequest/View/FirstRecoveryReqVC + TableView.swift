//
//  FirstRecoveryReqVC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces

extension FirstRecoveryReqVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recoveryDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.recoveryDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.recoveryDataSource[indexPath.row].height
    }
}

extension FirstRecoveryReqVC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .LocationTextFieldsCell:
            let cell: LocationTextFieldsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.targetViewController = self
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .DateRangeCell:
            let cell: DateRangeCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureAuctionConfirmationViewCell(cellInfo: cellInfo)
            return cell
            
        case .TextInputCell:
            let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        
        case .DropDownCell:
            let cell: DropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureViewForRequestForm(cellInfo: cellInfo)
            return cell
            
        case .ImageDropDownCell:
        let cell: ImageDropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        cell.configureView(cellInfo: cellInfo)
        return cell
            
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - DropDownCellDelegate
extension FirstRecoveryReqVC: DateRangeCellDelegate {
    
    func didTapFromDate(cell: DateRangeCell) {
        guard let indexPath = self.recoveryTableView.indexPath(for: cell) else { return }
        self.selectedIndexPath = indexPath
        self.isDate = true
        self.setupDateTimePicker(cell.fromTextField)
    }
    
    func didTapToDate(cell: DateRangeCell) {
        guard let indexPath = self.recoveryTableView.indexPath(for: cell) else { return }
        self.selectedIndexPath = indexPath
        self.isDate = false
        self.setupDateTimePicker(cell.toTextField)
    }
    
}

//MARK: - TextInputCellDelegate
extension FirstRecoveryReqVC: TextInputCellDelegate {
    func tapNextKeyboard(cell: TextInputCell) {
        self.view.endEditing(true)
    }
    
    func didChangeText(cell: TextInputCell, text: String) {
        guard let indexPath = self.recoveryTableView.indexPath(for: cell) else { return }
        self.recoveryDataSource[indexPath.row].value = text
    }
}

//MARK: - DropDownCellDelegate
extension FirstRecoveryReqVC: DropDownCellDelegate {
    func didTapDropDownCell(cell: DropDownCell) {
        guard let indexPath = self.recoveryTableView.indexPath(for: cell) else { return }
        self.showDropDownAt(indexPath: indexPath)
    }
}

//MARK: - ImageDropDownCellDelegate
extension FirstRecoveryReqVC: ImageDropDownCellDelegate {
    func didTapImageDropDownCell(cell: ImageDropDownCell) {
        guard let indexPath = self.recoveryTableView.indexPath(for: cell) else { return }
        self.showDropDownAt(indexPath: indexPath)
    }
}

//MARK: - LocationTextFieldCellDelegate
extension FirstRecoveryReqVC: LocationTextFieldsCellDelegate {
    
    func didChangeSourceText(cell: LocationTextFieldsCell, text: String, latitude: Double, longitude: Double) {
        
        guard let indexPath = self.recoveryTableView.indexPath(for: cell) else { return }
        self.recoveryDataSource[indexPath.row].info?[Constants.UIKeys.sourceValue] = text as AnyObject
        self.recoveryDataSource[indexPath.row].info?[Constants.UIKeys.sourceLatitude] = latitude as AnyObject
        self.recoveryDataSource[indexPath.row].info?[Constants.UIKeys.sourceLongitude] = longitude as AnyObject
        self.sourceLocation = CLLocationCoordinate2DMake(latitude, longitude)
        self.drowLineOnMap()
    }
    
    func didChangeDestinationText(cell: LocationTextFieldsCell, text: String, latitude: Double, longitude: Double) {
        
        guard let indexPath = self.recoveryTableView.indexPath(for: cell) else { return }
               self.recoveryDataSource[indexPath.row].info?[Constants.UIKeys.destinationValue] = text as AnyObject
               self.recoveryDataSource[indexPath.row].info?[Constants.UIKeys.destinationLatitude] = latitude as AnyObject
               self.recoveryDataSource[indexPath.row].info?[Constants.UIKeys.destinationLongitude] = longitude as AnyObject
        self.destinationLocation = CLLocationCoordinate2DMake(latitude, longitude)
        self.drowLineOnMap()

    }
    
    func tapNextKeyboard(cell: LocationTextFieldsCell) {
        self.view.endEditing(true)
    }
}

extension FirstRecoveryReqVC: GMSAutocompleteViewControllerDelegate {
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    // Get the place name from 'GMSAutocompleteViewController'
    // Then display the name in textField
    
    print(place.name)
   // textField.text = place.name
// Dismiss the GMSAutocompleteViewController when something is selected
    dismiss(animated: true, completion: nil)
  }
func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // Handle the error
    print("Error: ", error.localizedDescription)
  }
func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    // Dismiss when the user canceled the action
    dismiss(animated: true, completion: nil)
  }
}



