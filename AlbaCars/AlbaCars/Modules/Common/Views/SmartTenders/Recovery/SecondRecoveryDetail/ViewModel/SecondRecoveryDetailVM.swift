//
//  SecondRecoveryDetailVM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SecondRecoveryDetailVModeling: BaseVModeling {
    func getRecoveryDetailDataSource(tenderDetails: RDBidListModel) -> [CellInfo]
    func requestVerifyDropOffCodeAPI(tenderId: Int,arrData: [CellInfo], completion: @escaping (Bool) -> Void)
}

class SecondRecoveryDetailVM: BaseViewM, SecondRecoveryDetailVModeling {
    
    
    func getRecoveryDetailDataSource(tenderDetails: RDBidListModel) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //CarDetailCell
        var carDetailInfo = [String: AnyObject]()
        carDetailInfo[Constants.UIKeys.cellInfo] = tenderDetails
        carDetailInfo[Constants.UIKeys.isSecondDetailPage] = true as AnyObject
        let carDetailCell = CellInfo(cellType: .STCarDetailCell, placeHolder: "", value: "", info: carDetailInfo, height: Constants.CellHeightConstants.height_240)
        array.append(carDetailCell)
        
        //ContactInfoCell
        var contactInfo = [String: AnyObject]()
        contactInfo[Constants.UIKeys.isHide] = false as AnyObject
        let contactInfoCell = CellInfo(cellType: .RDContactInfoCell, placeHolder: Helper.toString(object: tenderDetails.name), value: Helper.toString(object: tenderDetails.contact), info: contactInfo, height: Constants.CellHeightConstants.height_100)
        array.append(contactInfoCell)
        
        //MapCell
        var mapInfo = [String: AnyObject]()
        mapInfo[Constants.UIKeys.cellInfo] = tenderDetails as AnyObject
        let mapCell = CellInfo(cellType: .RDMapCell, placeHolder: "", value: "", info: mapInfo, height: Constants.CellHeightConstants.height_165)
        array.append(mapCell)
        
        //Driver Info Cell
        var driverCellInfo = [String: AnyObject]()
        driverCellInfo[Constants.UIKeys.driverName] = tenderDetails.driverName as AnyObject
        driverCellInfo[Constants.UIKeys.driverContact] = tenderDetails.driverMobile as AnyObject
        driverCellInfo[Constants.UIKeys.driverImage] = tenderDetails.driverImage as AnyObject
        driverCellInfo[Constants.UIKeys.truckNumber] = tenderDetails.Bids?.truckNumber as AnyObject
        let driverInfoCell = CellInfo(cellType: .RDDriverInfoCell, placeHolder: "", value: "", info: driverCellInfo, height: Constants.CellHeightConstants.height_100)
        array.append(driverInfoCell)
        
        //Pickup Code Cell
        var codeTitleInfo = [String: AnyObject]()
        codeTitleInfo[Constants.UIKeys.placeholderText] = "Pickup Code".localizedString() as AnyObject
        codeTitleInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.center as AnyObject
        let codeTitleCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "".localizedString(), value: "", info: codeTitleInfo, height: Constants.CellHeightConstants.height_40)
        array.append(codeTitleCell)

        
        //Coupon code cell
        var codeCellInfo = [String: AnyObject]()
        codeCellInfo[Constants.UIKeys.backgroundColor] = UIColor.pinkButtonColor as AnyObject
        codeCellInfo[Constants.UIKeys.boundaryColor] = UIColor.redButtonColor as AnyObject
        codeCellInfo[Constants.UIKeys.inputType] = TextInputType.pickupCode as AnyObject
        let codeCell = CellInfo(cellType: .DashedTextFieldCell, placeHolder: "", value: Helper.toString(object: tenderDetails.pickupCode), info: codeCellInfo, height: Constants.CellHeightConstants.height_60)
        array.append(codeCell)
        
        //Drop Off Cell
        var dropTitleInfo = [String: AnyObject]()
        dropTitleInfo[Constants.UIKeys.placeholderText] = "Drop off Code".localizedString() as AnyObject
        dropTitleInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.center as AnyObject
        let dropTitleCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "".localizedString(), value: "", info: dropTitleInfo, height: Constants.CellHeightConstants.height_40)
        array.append(dropTitleCell)
        
        var dropOffCode = Helper.toString(object: tenderDetails.dropOffCode)
        if dropOffCode == "N/A" || dropOffCode == "n/a" {
            dropOffCode = ""
        }
        var codeCell2Info = [String: AnyObject]()
        codeCell2Info[Constants.UIKeys.backgroundColor] = UIColor.lightGreenColor as AnyObject
        codeCell2Info[Constants.UIKeys.boundaryColor] = UIColor.greenColor as AnyObject
        codeCell2Info[Constants.UIKeys.inputType] = TextInputType.dropOffCode as AnyObject
        let codeCell2 = CellInfo(cellType: .DashedTextFieldCell, placeHolder: "To be provided  by driver after end of recovery".localizedString(), value: Helper.toInt(tenderDetails.isDropoffcodeVerified) == 0 ? "" : dropOffCode, info: codeCell2Info, height: Constants.CellHeightConstants.height_60)
        array.append(codeCell2)

        return array
        //
    }
    
    func requestVerifyDropOffCodeAPI(tenderId: Int, arrData: [CellInfo], completion: @escaping (Bool) -> Void) {
        let params = getAPIParams(tenderId: tenderId, arrData: arrData)
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .verifyDropoffCode(param: params))) { (response, success) in
            if success {
               
                    completion(success)
            }
        }
    }
    
    func getAPIParams(tenderId: Int, arrData: [CellInfo]) -> APIParams {
        var params = APIParams()
        params[ConstantAPIKeys.tenderId] = tenderId as AnyObject
        params[ConstantAPIKeys.code] = arrData[7].value as AnyObject
        return params
    }
}
