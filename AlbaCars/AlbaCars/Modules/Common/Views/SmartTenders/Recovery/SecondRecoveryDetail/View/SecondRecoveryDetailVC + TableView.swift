//
//  SecondRecoveryDetailVC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit
import MapKit

extension SecondRecoveryDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.inspectionDetailDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.inspectionDetailDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.inspectionDetailDataSource[indexPath.row].height
    }
}

extension SecondRecoveryDetailVC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .STCarDetailCell:
            let cell: STCarDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .inspectionRequest, tenderStatus: .all)
            return cell
            
        case .RDContactInfoCell:
            let cell: RDContactInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .RDMapCell:
            let cell: RDMapCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .RDDriverInfoCell:
            let cell: RDDriverInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .DashedTextFieldCell:
            let cell: DashedTextFieldCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            cell.delegate = self
            return cell
            
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

extension SecondRecoveryDetailVC: VIContactInfoCellDelegate {
    
    func didTapPhone(cell: VIContactInfoCell, phone: String) {
        guard let url = URL(string: "tel://" + phone) else { return }
        UIApplication.shared.open(url)
    }
}

extension SecondRecoveryDetailVC: DashedTextFieldCellDelegate {
    func tapNextKeyboard(cell: DashedTextFieldCell) {
        
    }
    
    func didChangeText(cell: DashedTextFieldCell, text: String) {
        guard let indexPath = self.secondDetailTableView.indexPath(for: cell) else { return }
        self.inspectionDetailDataSource[indexPath.row].value  = text
        if text.count == Constants.Validations.dropOffMaxLength {
            self.requestVerifyDropOffCode()
        }
    }
    
    func callVerifyAPI(cell: DashedTextFieldCell, text: String) {
        //self.requestVerifyDropOffCode()
    }
    
    
}




