//
//  DashedTextFieldCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol DashedTextFieldCellDelegate: class {
  func tapNextKeyboard(cell: DashedTextFieldCell)
  func didChangeText(cell: DashedTextFieldCell, text: String)
  func callVerifyAPI(cell: DashedTextFieldCell, text: String)
}

class DashedTextFieldCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var codeTextField: UITextField!
    
    //MARK: - Variables
    var inputType: TextInputType?
    weak var delegate: DashedTextFieldCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.codeTextField.delegate = self
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_3half)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.codeTextField.text = cellInfo.value
        self.codeTextField.placeholder = cellInfo.placeHolder

        if let info = cellInfo.info {
            if let bgColor = info[Constants.UIKeys.backgroundColor] as? UIColor, let boundaryColor = info[Constants.UIKeys.boundaryColor] as? UIColor {
                self.codeTextField.textColor = boundaryColor
                Threads.performTaskInMainQueue {
                    self.bgView.addRoundDashedLine(color: boundaryColor, backgroundColor: bgColor)
                    self.bgView.backgroundColor = bgColor
                    self.codeTextField.textColor = boundaryColor
                    let placeholderText = NSAttributedString(string: cellInfo.placeHolder,
                    attributes: [NSAttributedString.Key.foregroundColor: boundaryColor,
                                 NSAttributedString.Key.font: UIFont.font(name: .AirbnbCerealApp, weight: .Medium, size: .size_14)])
                    self.codeTextField.attributedPlaceholder = placeholderText
                    self.codeTextField.text = cellInfo.value
                }
                self.codeTextField.setPlaceHolderColor(text: cellInfo.placeHolder, color: boundaryColor)
            }
            if let inputType = info[Constants.UIKeys.inputType] as? TextInputType {
                 
                if inputType == .pickupCode {
                    self.isUserInteractionEnabled = false
                } else if inputType == .dropOffCode {
                    self.isUserInteractionEnabled = true
                    
                    if !cellInfo.value.isEmpty {
                        self.isUserInteractionEnabled = false
                    }
                    
                }
                else {
                    self.isUserInteractionEnabled = true
                }
                
                self.inputType = inputType
            }
            self.layoutIfNeeded()
        }
    }
}

extension DashedTextFieldCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text), let delegate = self.delegate {
             let finalText = text.replacingCharacters(in: textRange, with: string)
            switch self.inputType {
             case .dropOffCode:
               if finalText.count > Constants.Validations.dropOffMaxLength {
                 return false
               }
             
             default:
                break
             }
             delegate.didChangeText(cell: self, text: finalText)
            
             
        }
        return true
    }
        
}
