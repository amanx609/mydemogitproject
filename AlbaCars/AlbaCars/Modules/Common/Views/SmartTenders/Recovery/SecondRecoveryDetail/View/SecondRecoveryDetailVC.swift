//
//  SecondRecoveryDetailVC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SecondRecoveryDetailVC: BaseViewC {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var secondDetailTableView: UITableView!
    @IBOutlet weak var jobCompletedButton: UIButton!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var recoveryStatusView: FinanceStatusView!
    @IBOutlet weak var jobCompletedButtonHeight: NSLayoutConstraint!
    
    //MARK: - IBOutlets
    @IBOutlet weak var dashedLineFirstImageView: UIImageView!
    @IBOutlet weak var dashedLineSecondImageView: UIImageView!
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var thirdButton: UIButton!
    @IBOutlet weak var timeFirstLabel: UILabel!
    @IBOutlet weak var timeSecondLabel: UILabel!
    @IBOutlet weak var timeThirdLabel: UILabel!
    
    
    //MARK: - Variables
    var viewModel: SecondRecoveryDetailVModeling?
    var inspectionDetailDataSource: [CellInfo] = []
    var acceptedBidId: Int?
    var tenderDetails: RDBidListModel?
    var tenderId: Int?
    var recoveryViewModel: RDBidListVModeling?
    var tenderStatus: TenderStatus?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func backButtonTapped() {
        if let viewControllers = self.navigationController?.viewControllers {
            //CreateAccountViewCIsPresentInNavigationStack
            for viewController in viewControllers {
                if viewController is RecoveryHomeViewC {
                    self.navigationController?.popToViewController(viewController, animated: true)
                    return
                }
            }
            
        }
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: "Recovery Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.recheckVM()
       // self.setupButton()
        self.setupTableView()
        self.requestTenderDetailsAPI()
        self.jobCompletedButton.isHidden = true
        self.gradientView.isHidden = true
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = SecondRecoveryDetailVM()
        }
        if self.recoveryViewModel == nil {
            self.recoveryViewModel = RDBidListViewM()
        }
    }
    
    private func loadDataSource() {
        
        self.secondDetailTableView.tableFooterView = self.footerView
        if let tndrDetails = self.tenderDetails, let dataSource = self.viewModel?.getRecoveryDetailDataSource(tenderDetails: tndrDetails) {
            self.inspectionDetailDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.secondDetailTableView.reloadData()
                self.setRecoveryStatus()
            }
        }
        
    }
    
    private func setRecoveryStatus() {
   
        if let tenderRecoveryStatuses = self.tenderDetails?.tenderRecoveryStatuses, tenderRecoveryStatuses.count > 0 {
            
            for i in 0..<tenderRecoveryStatuses.count {
                switch i {
                case 0:
                    self.timeFirstLabel.text = tenderRecoveryStatuses[i].startDate?.getDateStringFrom(inputFormat: Constants.Format.apiSpaceDateTimeFormat, outputFormat: Constants.Format.displayDateTimeFormat)
                    self.firstButton.isSelected = true
                case 1:
                    self.timeSecondLabel.text = tenderRecoveryStatuses[i].startDate?.getDateStringFrom(inputFormat: Constants.Format.apiSpaceDateTimeFormat, outputFormat: Constants.Format.displayDateTimeFormat)
                    self.secondButton.isSelected = true
                    self.thirdButton.isSelected = true
                    self.dashedLineFirstImageView.image = #imageLiteral(resourceName: "dashedGreen")
                case 2:
                    self.timeThirdLabel.text = tenderRecoveryStatuses[i].startDate?.getDateStringFrom(inputFormat: Constants.Format.apiSpaceDateTimeFormat, outputFormat: Constants.Format.displayDateTimeFormat)
                    self.thirdButton.isSelected = true
                    self.dashedLineSecondImageView.image = #imageLiteral(resourceName: "dashedGreen")
                    self.jobCompletedButton.isHidden = false
                    self.gradientView.isHidden = false
                    self.setupButton()

                default:
                    break
                }
            }
            
        }
        
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.secondDetailTableView.delegate = self
        self.secondDetailTableView.dataSource = self
        self.secondDetailTableView.separatorStyle = .none
       // self.secondDetailTableView.tableFooterView = self.footerView
        
    }
    
    private func setupButton() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.backgroundColor = .redButtonColor
           // self.jobCompletedButton.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
        }
    }
    
    private func registerNibs() {
        self.secondDetailTableView.register(STCarDetailCell.self)
        self.secondDetailTableView.register(RDContactInfoCell.self)
        self.secondDetailTableView.register(RDMapCell.self)
        self.secondDetailTableView.register(RDDriverInfoCell.self)
        self.secondDetailTableView.register(BottomLabelCell.self)
        self.secondDetailTableView.register(DashedTextFieldCell.self)
    }
    
    private func requestTenderDetailsAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
        //params[ConstantAPIKeys.type] = 1 as AnyObject
        self.recoveryViewModel?.requestTenderDetailsAPI(params: params, completion: {[weak self] (tenderDetails) in
            guard let sSelf = self else { return }
            sSelf.tenderDetails = tenderDetails
            sSelf.loadDataSource()

        })
    }
    
    //MARK: - Public Methods
    func requestVerifyDropOffCode() {
        self.view.endEditing(true)
        self.viewModel?.requestVerifyDropOffCodeAPI(tenderId:Helper.toInt(self.tenderId), arrData: self.inspectionDetailDataSource, completion: {[weak self] (success) in
            guard let sSelf = self else { return }
            Threads.performTaskAfterDealy(0.1, {
                 sSelf.requestTenderDetailsAPI()
            })
        })
    }
    
    //MARK: IBActions
    @IBAction func tapJobCompletedButton(_ sender: Any) {
        
        Threads.performTaskAfterDealy(0.1, {
            let jobCompletedVC = DIConfigurator.sharedInstance.getJobDoneViewC()
            jobCompletedVC.serviceProviderId = Helper.toInt(self.tenderDetails?.Bids?.serviceProviderId)
            jobCompletedVC.tenderId = Helper.toInt(self.tenderDetails?.id)
            self.navigationController?.pushViewController(jobCompletedVC, animated: true)
        })
        
        //        let pickupCode = Helper.toString(object: self.inspectionDetailDataSource[7].value)
        //        if !pickupCode.isEmpty {
        //            self.requestVerifyDropOffCode()
        //        } else {
        //            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter dropoff Code".localizedString())
        //        }
        
    }
}
