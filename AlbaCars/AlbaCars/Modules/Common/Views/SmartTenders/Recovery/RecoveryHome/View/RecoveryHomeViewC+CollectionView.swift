//
//  RecoveryHomeViewC+CollectionView.swift
//  AlbaCars
//
//  Created by Narendra on 1/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension RecoveryHomeViewC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.recoveryDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellInfo = self.recoveryDataSource[indexPath.row]
        return self.getCell(collectionView: collectionView, indexPath: indexPath, cellInfo: cellInfo)
    }
  
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.bounds.width/2-5
        return CGSize(width: width, height: self.recoveryDataSource[indexPath.row].height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        self.goToSelectedScreen(indx: indexPath.row)
    }
    
}

extension RecoveryHomeViewC {
    func getCell(collectionView: UICollectionView, indexPath: IndexPath, cellInfo: CellInfo) -> UICollectionViewCell {
        guard let cellType = cellInfo.cellType else { return UICollectionViewCell() }
        switch cellType {
        case .HomeCollectionCell:
            let cell: HomeCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureForRecovery(cellInfo: cellInfo)
            cell.homeInfoView.placeholderImageViewWidth.constant = Constants.CellHeightConstants.height_56
            return cell
        default:
            return UICollectionViewCell()
        }
    }
}
