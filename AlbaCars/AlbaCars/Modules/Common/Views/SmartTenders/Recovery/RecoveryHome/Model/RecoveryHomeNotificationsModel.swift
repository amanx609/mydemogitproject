//
//  RecoveryHomeNotificationsModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/31/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class RecoveryHomeNotificationsModel: BaseCodable {
    
    var allBidsRejected: Int?
    var bidAccepted: Int?
    var completedTenders: Int?
    var inProgressTenders: Int?
    var noBidsAccepted: Int?
    var upcomingTenders: Int?
}
