//
//  RecoveryHomeViewM.swift
//  AlbaCars
//
//  Created by Narendra on 1/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//


import Foundation
import UIKit

protocol RecoveryHomeViewModeling: BaseVModeling {
    func getRecoveryHomeDataSource(notificationsInfo: RecoveryHomeNotificationsModel) -> [CellInfo]
    func requestNotificationsAPI(params: APIParams, completion: @escaping (RecoveryHomeNotificationsModel)-> Void)
}

class RecoveryHomeViewM: RecoveryHomeViewModeling {
    
    func getRecoveryHomeDataSource(notificationsInfo: RecoveryHomeNotificationsModel) -> [CellInfo] {
                
        var array = [CellInfo]()
        
        //Bid Accepted
        var smartTendersInfo = [String: AnyObject]()
        smartTendersInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "bidAccepted")
        smartTendersInfo[Constants.UIKeys.notifications] = notificationsInfo.bidAccepted as AnyObject
        let smartTendersCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Bid Accepted".localizedString(), value: "", info: smartTendersInfo, height: Constants.CellHeightConstants.height_130)
        array.append(smartTendersCell)
        
        //In-Progress Tenders
        var auctionsInfo = [String: AnyObject]()
        auctionsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "vectorSmatObject")
        auctionsInfo[Constants.UIKeys.notifications] = notificationsInfo.inProgressTenders as AnyObject
        let auctionsCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "In-Progress Tenders".localizedString(), value: "", info: auctionsInfo, height: Constants.CellHeightConstants.height_130)
        array.append(auctionsCell)
        
        //Upcoming Tenders
        var leadsInfo = [String: AnyObject]()
        leadsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "upcomingTender")
        leadsInfo[Constants.UIKeys.notifications] = notificationsInfo.upcomingTenders as AnyObject
        let leadsCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Upcoming Tenders".localizedString(), value: "", info: leadsInfo, height: Constants.CellHeightConstants.height_130)
        array.append(leadsCell)
        
        //No Bids Accepted
        var fleetBiddingInfo = [String: AnyObject]()
        fleetBiddingInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "noBidAccepted")
        fleetBiddingInfo[Constants.UIKeys.notifications] = notificationsInfo.noBidsAccepted as AnyObject
        let fleetBiddingCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "No Bids Accepted".localizedString(), value: "", info: fleetBiddingInfo, height: Constants.CellHeightConstants.height_130)
        array.append(fleetBiddingCell)
        
        //All Bids Rejected
        var leaderboardInfo = [String: AnyObject]()
        leaderboardInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "bidRejected")
        leaderboardInfo[Constants.UIKeys.notifications] = notificationsInfo.allBidsRejected as AnyObject
        let leaderboardCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "All Bids Rejected".localizedString(), value: "", info: leaderboardInfo, height: Constants.CellHeightConstants.height_130)
        array.append(leaderboardCell)
        
        //Completed Tenders
        var warrantyInfo = [String: AnyObject]()
        warrantyInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "compltedTenders")
        warrantyInfo[Constants.UIKeys.notifications] = notificationsInfo.completedTenders as AnyObject
        let warrantyCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Completed Tenders".localizedString(), value: "", info: warrantyInfo, height: Constants.CellHeightConstants.height_130)
        array.append(warrantyCell)
    
        return array
    }
    
    func requestNotificationsAPI(params: APIParams, completion: @escaping (RecoveryHomeNotificationsModel)-> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderHomeNotifications(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let jsonData = result.toJSONData(),
                    let resultData = RecoveryHomeNotificationsModel.init(jsonData: jsonData)
                {
                    completion(resultData)
                }
            }
        }
    }
    
    
}
