//
//  RecoveryHomeViewC.swift
//  AlbaCars
//
//  Created by Narendra on 1/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class RecoveryHomeViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var recoveryCollectionView: UICollectionView!
    @IBOutlet weak var createNewTenderButton: UIButton!
    @IBOutlet weak var  viewAllTendersButton: UIButton!
    
    //MARK: - Variables
    var recoveryDataSource: [CellInfo] = []
    var viewModel: RecoveryHomeViewModeling?
    var recoveryHomeType: RecoveryHomeScreenType = .none
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        switch recoveryHomeType {
        case .recovery:
            self.setupNavigationBarTitle(title: ChooseServiceType.recovery.title, leftBarButtonsType: [.back], rightBarButtonsType: [])
        case .inspection:
            self.setupNavigationBarTitle(title: ChooseServiceType.vehicleInspection.title, leftBarButtonsType: [.back], rightBarButtonsType: [])
        case .upholstery:
            self.setupNavigationBarTitle(title: ChooseServiceType.upholstery.title, leftBarButtonsType: [.back], rightBarButtonsType: [])
        case .servicing:
            self.setupNavigationBarTitle(title: ChooseServiceType.carServicing.title, leftBarButtonsType: [.back], rightBarButtonsType: [])
        case .spareParts:
            self.setupNavigationBarTitle(title: ChooseServiceType.spareParts.title, leftBarButtonsType: [.back], rightBarButtonsType: [])
            
        case .wheels:
            self.setupNavigationBarTitle(title: ChooseServiceType.wheels.title, leftBarButtonsType: [.back], rightBarButtonsType: [])
            
        case .windowTinting:
            self.setupNavigationBarTitle(title: ChooseServiceType.windowTinting.title, leftBarButtonsType: [.back], rightBarButtonsType: [])
            
        case .bankValuation:
            self.setupNavigationBarTitle(title: ChooseServiceType.bankValuation.title, leftBarButtonsType: [.back], rightBarButtonsType: [])
            
        case .carDetailing:
            self.setupNavigationBarTitle(title: ChooseServiceType.carDetailing.title, leftBarButtonsType: [.back], rightBarButtonsType: [])
            
        case .bodyWork:
            self.setupNavigationBarTitle(title: ChooseServiceType.bodyWork.title, leftBarButtonsType: [.back], rightBarButtonsType: [])
            
        case .insurance:
            self.setupNavigationBarTitle(title: ChooseServiceType.insurance.title, leftBarButtonsType: [.back], rightBarButtonsType: [])
            
            
        case .none:
            break
        default:
            break
        }
        self.requestNotificationsAPI()
    }
    
    deinit {
        
        print("deinit RecoveryHomeViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupView()
        self.recheckVM()
        self.setupCollectionView()
        self.loadDataSource(notificationsInfo: RecoveryHomeNotificationsModel())
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.createNewTenderButton.setTitle("Create New Tender".localizedString(), for: .normal)
        self.viewAllTendersButton.setTitle("View All Tenders".localizedString(), for: .normal)
    }
    
    private func loadDataSource(notificationsInfo: RecoveryHomeNotificationsModel) {
        if let dataSource = self.viewModel?.getRecoveryHomeDataSource(notificationsInfo: notificationsInfo) {
            self.recoveryDataSource = dataSource
        }
        self.recoveryCollectionView.reloadData()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = RecoveryHomeViewM()
        }
    }
    
    private func setupCollectionView() {
        self.registerNibs()
        self.recoveryCollectionView.delegate = self
        self.recoveryCollectionView.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //depending upon direction of collection view
        self.recoveryCollectionView?.setCollectionViewLayout(layout, animated: true)
    }
    
    private func registerNibs() {
        self.recoveryCollectionView.register(HomeCollectionCell.self)
    }
    
    func goToSelectedScreen(indx: Int) {
        var tenderStatus: TenderStatus = .all
        switch indx {
        case 0:
            tenderStatus = .bidAccepted
            break
        case 1:
            tenderStatus = .inProgress
            break
        case 2:
            tenderStatus = .upcomingTenders
            break
        case 3:
            tenderStatus = .noBidAccepted
            break
        case 4:
            tenderStatus = .allBidsRejected
            break
        case 5:
            tenderStatus = .completedTenders
            break
            
        default:
            break
        }
        
        switch recoveryHomeType {
        case .recovery:
            let allTendersViewC = DIConfigurator.sharedInstance.getAllTendersVC()
            allTendersViewC.tenderStatus = tenderStatus
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
        case .inspection:
            let allTendersViewC = DIConfigurator.sharedInstance.getVITenderListVC()
            allTendersViewC.tenderStatus = tenderStatus
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
        case .servicing:
            let allTendersViewC = DIConfigurator.sharedInstance.getServicingTenderListViewC()
            allTendersViewC.tenderStatus = tenderStatus
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
        case .upholstery:
            let allTendersViewC = DIConfigurator.sharedInstance.getUSTenderListVC()
            allTendersViewC.tenderStatus = tenderStatus
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
        case .spareParts:
            let pTenderListViewC = DIConfigurator.sharedInstance.getPTenderListVC()
            pTenderListViewC.tenderStatus = tenderStatus
            self.navigationController?.pushViewController(pTenderListViewC, animated: true)
            break
        case .wheels:
            let allTendersViewC = DIConfigurator.sharedInstance.getWTenderListVC()
            allTendersViewC.tenderStatus = tenderStatus
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
        case .windowTinting:
            let allTendersViewC = DIConfigurator.sharedInstance.getWTTenderListVC()
            allTendersViewC.tenderStatus = tenderStatus
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
            
        case .bankValuation:
            let allTendersViewC = DIConfigurator.sharedInstance.getBVTenderListVC()
            allTendersViewC.tenderStatus = tenderStatus
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
            
        case .carDetailing:
            let allTendersViewC = DIConfigurator.sharedInstance.getCDTenderListViewC()
            allTendersViewC.tenderStatus = tenderStatus
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
        case .bodyWork:
            let allTendersViewC = DIConfigurator.sharedInstance.getBWTenderListVC()
            allTendersViewC.tenderStatus = tenderStatus
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            
        case .insurance:
            let allTendersViewC = DIConfigurator.sharedInstance.getITenderListVC()
            allTendersViewC.tenderStatus = tenderStatus
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            
        case .none:
            break
            
        default:
            break
            
        }
    }
    
    func requestNotificationsAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.type] = recoveryHomeType.rawValue as AnyObject
        self.viewModel?.requestNotificationsAPI(params: params, completion: { (response) in
            self.loadDataSource(notificationsInfo: response)
        })
    }
    
    //MARK: - IBActions
    @IBAction func tapCreateNewTenderButton(_ sender: Any) {
        var chooseServiceType: ChooseCarForServiceType = .none
        switch recoveryHomeType {
        case .recovery:
            chooseServiceType = .recoveryRequest
            break
        case .inspection:
            chooseServiceType = .inspectionRequest
            break
        case .upholstery:
            chooseServiceType = .upholsteryRequest
            break
        case .servicing:
            chooseServiceType = .carServicing
        case .spareParts:
            chooseServiceType = .spareParts
        case .wheels:
            chooseServiceType = .wheels
            
        case .windowTinting:
            chooseServiceType = .windowTinting
            
        case .bankValuation:
            chooseServiceType = .bankValuation
            
        case .carDetailing:
            chooseServiceType = .carDetailing
            
        case .bodyWork:
            chooseServiceType = .bodyWork
            
        case .insurance:
            chooseServiceType = .insurance
            
        case .none:
            break
            
        default:
            break
            
        }
        let chooseCarViewC = DIConfigurator.sharedInstance.getChooseCarForServiceVC()
        chooseCarViewC.chooseCarForServiceType = chooseServiceType
        self.navigationController?.pushViewController(chooseCarViewC, animated: true)
    }
    
    @IBAction func tapViewAllTenderButton(_ sender: Any) {
        
        switch recoveryHomeType {
        case .recovery:
            let allTendersViewC = DIConfigurator.sharedInstance.getAllTendersVC()
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
        case .inspection:
            let allTendersViewC = DIConfigurator.sharedInstance.getVITenderListVC()
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
        case .upholstery:
            let allTendersViewC = DIConfigurator.sharedInstance.getUSTenderListVC()
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
        case .servicing:
            let allTendersViewC = DIConfigurator.sharedInstance.getServicingTenderListViewC()
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
        case .spareParts:
            let pTenderListVC = DIConfigurator.sharedInstance.getPTenderListVC()
            self.navigationController?.pushViewController(pTenderListVC, animated: true)
            break
            
        case .wheels:
            let allTendersViewC = DIConfigurator.sharedInstance.getWTenderListVC()
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
            
        case .windowTinting:
            let allTendersViewC = DIConfigurator.sharedInstance.getWTTenderListVC()
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
            
        case .bankValuation:
            let allTendersViewC = DIConfigurator.sharedInstance.getBVTenderListVC()
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            break
        case .bodyWork:
            let allTendersViewC = DIConfigurator.sharedInstance.getBWTenderListVC()
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            
        case .insurance:
            let allTendersViewC = DIConfigurator.sharedInstance.getITenderListVC()
            self.navigationController?.pushViewController(allTendersViewC, animated: true)
            
        case .none:
            break
            
        default:
            break
            
        }
        
    }
    
}
