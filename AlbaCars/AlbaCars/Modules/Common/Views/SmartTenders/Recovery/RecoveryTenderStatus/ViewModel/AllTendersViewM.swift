//
//  AllTendersViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol AllTendersVModeling: BaseVModeling {
//    func getAllTendersTableDataSource(tenders: [AllTendersModel]) -> [CellInfo]
    func requestTenderListAPI(params: APIParams, completion: @escaping (Int, Int, [AllTendersModel])-> Void)
}

class AllTendersViewM: BaseViewM, AllTendersVModeling {
    
    func getAllTendersTableDataSource(tenders: [AllTendersModel]) -> [CellInfo] {
        var array = [CellInfo]()
        //pending Tender Cell
        for i in 0..<tenders.count {
            let tender = tenders[i]
            var pendingTenderCellInfo = [String: AnyObject]()
            pendingTenderCellInfo[Constants.UIKeys.pendingStatus] = tender.tenderStatus() as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.timerInfo1] = tender.timeLeft() as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.timerInfo2] = tender.timeLeft() as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.sourceText] = tender.pickupLocation as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.id] = tender.id as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.destinationText] = tender.dropoffLocation as AnyObject
            pendingTenderCellInfo[ConstantAPIKeys.brandName] = tender.brandName as AnyObject
            pendingTenderCellInfo[ConstantAPIKeys.year] = tender.year as AnyObject
            pendingTenderCellInfo[Constants.UIKeys.bidCount] = tender.bidsCount as AnyObject
            //pendingTenderCellInfo[ConstantAPIKeys.bran]
            //pendingTenderCellInfo[]
            var cellHeight = CGFloat()
            switch TenderStatus(rawValue: Helper.toInt(tender.status)) {
            case .inProgress:
                cellHeight = Constants.CellHeightConstants.height_240
            case .noBidAccepted, .allBidsRejected:
                cellHeight = Constants.CellHeightConstants.height_260
            case .completedTenders:
                cellHeight = Constants.CellHeightConstants.height_121
                
            case .bidAccepted:
                cellHeight = Constants.CellHeightConstants.height_200
            default:
                break
            }
            let pendingTenderCell = CellInfo(cellType: .TenderStatusDetailCell, placeHolder: "", value: "", info: pendingTenderCellInfo, height: cellHeight)
            array.append(pendingTenderCell)
        }
        return array
    }
    
    func requestTenderListAPI(params: APIParams, completion: @escaping (Int, Int, [AllTendersModel]) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderList(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let resultData = result.toJSONData(),
                    let tenderList = TenderList(jsonData: resultData) {
                    completion(nextPageNumber, perPage,tenderList.tenders ?? [])
                }
            }
        }
    }
    
   
    
}
