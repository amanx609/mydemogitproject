//
//  ChooseCarFormViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

protocol ChooseCarFormViewModeling: BaseVModeling {
  func requestChooseCarAPI(completion: @escaping (Bool, [[String: AnyObject]])-> Void)
}

class ChooseCarFormViewM: BaseViewM, ChooseCarFormViewModeling {

    func requestChooseCarAPI(completion: @escaping (Bool, [[String : AnyObject]]) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .chooseCar)) { (response, success) in
          if success {
            if let safeResponse =  response as? [String: AnyObject],
              let vehicleBrands = safeResponse[ConstantAPIKeys.result] as? [[String: AnyObject]]
            {
              completion(success, vehicleBrands)
            }
          }
        }

    }
    
    
}



