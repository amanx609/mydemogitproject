//
//  BidListModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/31/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class BidListModel: BaseCodable {
    var amount: Int?
    var bidId: Int?
    var bidderComments: String?
    var daysForService: Int?
    var dropoffLocation: String?
    var image: String?
    var jobDate: String?
    var jobTime: String?
    var name: String?
    var pickupLocation: String?
    var rating: Double?
    var recoveryType: String?
    var serviceProviderId: Int?
    var isPickupDropoff: Int?
    var tintColor: String?
    var tintVisibility: Int?
    var visibilityPercentage: Int?
    
    var typeOfService: String?
    
    var partName: String?
    var partType: Int?
    
    var bankId: String?
    var bankName: String?
    var carPrice: Double?
    var rimService: Int?
    
    var bodyServiceType: Int?
    var paintColor: String?
    var paintSiteBody: Int?
    var paintSiteGlass: Int?
    var paintSiteInterior: Int?
    var paintSitePackage: Int?
    var paintSiteRims: Int?
    var typeOfInsurance: String?
    var vehiclePrice: Double?
    var noOfDents: Int?
    var paintSiteArr: [Int]?
    var wheelPartType: Int?
    var wheelSize: String?
    
    required init(from decoder: Decoder) throws {
        let values = try! decoder.container(keyedBy: CodingKeys.self)
        self.amount = try values.decodeIfPresent(Int.self, forKey: .amount)
        self.bidId = try values.decodeIfPresent(Int.self, forKey: .bidId)
        self.bidderComments = try values.decodeIfPresent(String.self, forKey: .bidderComments)
        self.daysForService = try values.decodeIfPresent(Int.self, forKey: .daysForService)
        self.dropoffLocation = try values.decodeIfPresent(String.self, forKey: .dropoffLocation)
        self.image = try values.decodeIfPresent(String.self, forKey: .image)
        self.jobDate = try values.decodeIfPresent(String.self, forKey: .jobDate)
        self.jobTime = try values.decodeIfPresent(String.self, forKey: .jobTime)
        self.name = try values.decodeIfPresent(String.self, forKey: .name)
        self.pickupLocation = try values.decodeIfPresent(String.self, forKey: .pickupLocation)
        self.rating = try values.decodeIfPresent(Double.self, forKey: .rating)
        self.recoveryType = try values.decodeIfPresent(String.self, forKey: .recoveryType)
        self.serviceProviderId = try values.decodeIfPresent(Int.self, forKey: .serviceProviderId)
        self.isPickupDropoff = try values.decodeIfPresent(Int.self, forKey: .isPickupDropoff)
        self.tintColor = try values.decodeIfPresent(String.self, forKey: .tintColor)
        self.tintVisibility = try values.decodeIfPresent(Int.self, forKey: .tintVisibility)
        self.visibilityPercentage = try values.decodeIfPresent(Int.self, forKey: .visibilityPercentage)
        self.typeOfService = try values.decodeIfPresent(String.self, forKey: .typeOfService)
        self.partName = try values.decodeIfPresent(String.self, forKey: .partName)
        self.partType = try values.decodeIfPresent(Int.self, forKey: .partType)
        self.bankId = try values.decodeIfPresent(String.self, forKey: .bankId)
        self.bankName = try values.decodeIfPresent(String.self, forKey: .bankName)
        self.carPrice = try values.decodeIfPresent(Double.self, forKey: .carPrice)
        self.rimService = try values.decodeIfPresent(Int.self, forKey: .rimService)
        self.bodyServiceType = try values.decodeIfPresent(Int.self, forKey: .bodyServiceType)
        self.paintColor = try values.decodeIfPresent(String.self, forKey: .paintColor)
        self.paintSiteBody = try values.decodeIfPresent(Int.self, forKey: .paintSiteBody)
        self.paintSiteGlass = try values.decodeIfPresent(Int.self, forKey: .paintSiteGlass)
        self.paintSiteInterior = try values.decodeIfPresent(Int.self, forKey: .paintSiteInterior)
        self.paintSitePackage = try values.decodeIfPresent(Int.self, forKey: .paintSitePackage)
        self.paintSiteRims = try values.decodeIfPresent(Int.self, forKey: .paintSiteRims)
        self.typeOfInsurance = try values.decodeIfPresent(String.self, forKey: .typeOfInsurance)
        self.vehiclePrice = try values.decodeIfPresent(Double.self, forKey: .vehiclePrice)
        self.noOfDents = try values.decodeIfPresent(Int.self, forKey: .noOfDents)
        self.paintSiteArr = try values.decodeIfPresent([Int].self, forKey: .paintSiteArr)
        self.wheelPartType = try values.decodeIfPresent(Int.self, forKey: .wheelPartType)
        self.wheelSize = try values.decodeIfPresent(String.self, forKey: .wheelSize)
        
    }
    
    func getJobDate() -> String {
        return Helper.toString(object: jobDate?.getDateStringFrom(inputFormat: Constants.Format.dateFormatyyyyMMdd, outputFormat: Constants.Format.dateFormatdMMMyyyy))
    }
    
    func getJobTime() -> String {
        return Helper.toString(object: jobTime?.getDateStringFrom(inputFormat: Constants.Format.timeFormat24hhmmss, outputFormat: Constants.Format.timeFormathmma))
    }
    
    func isPickupDropoffStr() -> String {
        if Helper.toInt(isPickupDropoff) == 1 {
            return "Yes".localizedString()
        } else {
            return "No".localizedString()
        }
    }
    
    func getDaysForService() -> String {
        
        if let daysRequired = self.daysForService {
            let daysText = daysRequired > 1 ? "\(daysRequired) Days" : "\(daysRequired) Day"
            return daysText
        }
        return ""
    }
    
    func getServiceType() -> String {
        
        if let service = self.rimService, service != 0 {
            return RimServiceType(rawValue: service)?.title ?? ""
        } else {
            return "Tire".localizedString()
        }
        
    }
    
    func getBidServicingType() -> String {
        if let type = self.typeOfService {
            if type == "major" {
                return "Major Service"
            } else if type == "minor" {
                return "Minor Service"
            }
        }
        return self.typeOfService ?? ""
    }
    
    func getBodyWorkType() -> (String,String) {
        
        if let typeValue = self.bodyServiceType , let bwType = BodyWorkType(rawValue: typeValue) {
            switch bwType {
            case .panelPainting:
                return ("Paint Color: ", self.paintColor ?? "")
            case .paintlessDentRemoval:
                return ("No. of Dents: ", Helper.toString(object: noOfDents))
            case .ceramicPaintProtection:
                var paintSite = Helper.getPaitSite(paintSiteArray:paintSiteArr)
                
                //                if Helper.toInt(self.paintSiteBody) == 1 {
                //                    paintSite == "" ? paintSite.append("Body") : paintSite.append(", Body")
                //                } else if Helper.toInt(self.paintSiteGlass) == 1 {
                //                    paintSite == "" ? paintSite.append("Glass") : paintSite.append(", Glass")
                //                } else if Helper.toInt(self.paintSiteInterior) == 1 {
                //                    paintSite == "" ? paintSite.append("Interior") : paintSite.append(", Interior")
                //                }
                //                    //                else if Helper.toBool(self.paintSitePackage) {
                //                    //                    paintSite == "" ? paintSite.append("Package") : paintSite.append(", Package")
                //                    //                }
                if Helper.toInt(self.paintSiteRims) == 1 {
                    paintSite == "" ? paintSite.append("Rims") : paintSite.append(", Rims")
                    
                }
                return ("Paint Site : ", paintSite)
                
            }
            
        }
        return ("","")
    }
    
    func getPartType() -> String {
        if let part = self.partType, let prtType = SparePartType(rawValue: part ) {
            return prtType.title
        }
        return ""
    }
    
    func getWheelPartType() -> String {
        if let part = self.wheelPartType, let prtType = SparePartType(rawValue: part ) {
            return prtType.title
        }
        return ""
    }
    
    func getInsuranceType() -> String {
        let type = Helper.toInt(self.typeOfInsurance) 
        if let insuranceType = InsuranceType(rawValue: type) {
            return insuranceType.title
        }
        return ""
    }
    
    
}

class Bids: BaseCodable {
    var bids: [BidListModel]?
    
    private enum CodingKeys: String, CodingKey {
        case bids = "data"
    }
}
