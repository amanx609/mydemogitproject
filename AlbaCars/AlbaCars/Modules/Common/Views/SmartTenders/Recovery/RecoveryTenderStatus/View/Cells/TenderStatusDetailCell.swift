//
//  TenderStatusDetailCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class TenderStatusDetailCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var bidsView: UIView!
    @IBOutlet weak var bidsLabel: UILabel!
    @IBOutlet weak var bidsCountLabel: UILabel!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var timerInfo1Label: UILabel!
    @IBOutlet weak var timerInfo2Label: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var location1Label: UILabel!
    @IBOutlet weak var location2Label: UILabel!
    @IBOutlet weak var tenderStatusButton: UIButton!
    @IBOutlet weak var completedImageView: UIImageView!
    @IBOutlet weak var timerLabelsBgView: UIView!
    @IBOutlet weak var tenderStatusBgView: UIView!
    
    
    //MARK: - Life Cycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.timerInfo1Label.text = ""
        self.timerInfo2Label.text = ""
        Threads.performTaskInMainQueue {
            self.bidsView.roundCorners([.bottomLeft, .bottomRight], radius: Constants.UIConstants.sizeRadius_14)
            self.bgView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
            self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_7))
            self.dataView.roundCorners(Constants.UIConstants.sizeRadius_7)
        }
    }
    
   
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            guard let tenderStatus = info[Constants.UIKeys.pendingStatus] as? TenderStatus else  { return }
            //self.setupCellData(tenderStatus: tenderStatus, info: info)
        }
    }
    
    func configureView(tenderModel: AllTendersModel) {
        self.location1Label.text = tenderModel.pickupLocation
        self.location2Label.text = tenderModel.dropoffLocation
        self.carModelLabel.text = Helper.toString(object: tenderModel.carTitle())
        self.bidsCountLabel.text = Helper.toString(object: tenderModel.bidsCount)
        
        switch tenderModel.tenderStatus() {
        case .inProgress:
            self.completedImageView.isHidden = true
            self.tenderStatusBgView.isHidden = false
            self.timerInfo1Label.textColor = .red
            self.timerInfo2Label.isHidden = true
            self.timerLabelsBgView.isHidden = false
        self.tenderStatusButton.setTitle(StringConstants.Text.tenderInProgress.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .greenColor
            //Timer
            if let timerDuration = tenderModel.endTimerDuration {
                self.timerInfo1Label.text = timerDuration.convertToTimerFormat()
            }
        case .bidAccepted:
            self.completedImageView.isHidden = true
            self.tenderStatusBgView.isHidden = false
            self.timerLabelsBgView.isHidden = true
           self.timerInfo1Label.isHidden = false
            self.tenderStatusButton.setTitle(StringConstants.Text.tenderBidAccepted.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .yellowColor
        case .noBidAccepted:
            self.completedImageView.isHidden = true
            self.tenderStatusBgView.isHidden = false
            self.timerInfo1Label.text = "\(Helper.toString(object: tenderModel.timeLeft())) \(StringConstants.Text.tenderComplete.localizedString())"
            self.timerInfo1Label.textColor = .grayTextColor
            //Timer
            if let timerDuration = tenderModel.endTimerDuration {
                self.timerInfo2Label.attributedText =  String.getAttributedText(firstText: " \(Helper.toString(object: timerDuration.convertToTimerFormat()))", secondText: " \(StringConstants.Text.toAcceptBid.localizedString())", firstTextColor: .redButtonColor, secondTextColor: .greenTopHeaderColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
            }
            self.tenderStatusButton.setTitle(StringConstants.Text.tenderNoBidsAccepted.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .blackColor
        case .allBidsRejected:
            self.completedImageView.isHidden = true
            self.tenderStatusBgView.isHidden = false
            self.timerInfo2Label.isHidden = false
            self.timerInfo1Label.text = "\(Helper.toString(object: tenderModel.timeLeft())) \(StringConstants.Text.tenderComplete.localizedString())"
            self.timerInfo1Label.textColor = .grayTextColor
            self.timerInfo2Label.text = "\(Helper.toString(object: tenderModel.timeLeft())) \(StringConstants.Text.timeElapsed.localizedString())"
            self.timerInfo1Label.textColor = .grayTextColor
            self.tenderStatusButton.setTitle(StringConstants.Text.tenderAllBidsRejected.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .redButtonColor
        case .upcomingTenders:
            self.completedImageView.isHidden = true
            self.tenderStatusBgView.isHidden = false
            self.timerInfo2Label.isHidden = false
            self.timerInfo2Label.attributedText =  String.getAttributedText(firstText:"\(StringConstants.Text.tenderDateTime.localizedString())", secondText: " \(Helper.toString(object: tenderModel.getDateTime()))", firstTextColor: .black, secondTextColor: .greenTopHeaderColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Book)
            self.tenderStatusButton.setTitle(StringConstants.Text.tenderUpcoming.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .blueTenderButtonColor
            
            //Timer
            if let timerDuration = tenderModel.startTimerDuration {
                self.timerInfo1Label.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.tenderStartsIn.localizedString()) ", secondText: timerDuration.convertToTimerFormat(), firstTextColor: .grayTextColor, secondTextColor: .greenTopHeaderColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Book)
            }
        case .completedTenders:
            self.timerLabelsBgView.isHidden = true
            self.tenderStatusBgView.isHidden = true
            self.completedImageView.isHidden = false
        
        default:
            break
        }
    }
}
