//
//  TenderStatusDetailsViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension AllTendersViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tenderStatusDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getCell(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let tender = self.tenderStatusDataSource[indexPath.row]
        if tender.tenderStatus() == .all {
            return 0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      guard let lastVisibleRow = self.tenderStatusTableView.indexPathsForVisibleRows?.last?.row else { return }
        if (self.tenderStatusDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
        self.requestTenderListAPI()
      }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tender = self.tenderStatusDataSource[indexPath.row]
        
        if (tender.tenderStatus() == .bidAccepted) {
            let recoveryRequestViewC = DIConfigurator.sharedInstance.getSecondRecoveryDetailVC()
            recoveryRequestViewC.tenderId = Helper.toInt(self.tenderStatusDataSource[indexPath.row].id)
            self.navigationController?.pushViewController(recoveryRequestViewC, animated: true)
        } else {
            let recoveryRequestViewC = DIConfigurator.sharedInstance.getRDBidListVC()
            recoveryRequestViewC.tenderId = Helper.toInt(self.tenderStatusDataSource[indexPath.row].id)
            recoveryRequestViewC.tenderStatus = self.tenderStatusDataSource[indexPath.row].tenderStatus()
            self.navigationController?.pushViewController(recoveryRequestViewC, animated: true)
        }
    }
}
extension AllTendersViewC {
    func getCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: TenderStatusDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let tender = self.tenderStatusDataSource[indexPath.row]
        cell.configureView(tenderModel: tender)
        return cell
    }
}
