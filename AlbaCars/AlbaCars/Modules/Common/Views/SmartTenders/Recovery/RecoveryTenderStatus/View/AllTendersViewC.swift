//
//  TenderStatusDetailsViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class AllTendersViewC: BaseViewC {
    //MARK: - IBOutlets
    @IBOutlet weak var tenderStatusTableView: UITableView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var createTenderButton: UIButton!
    
    //MARK: - Variables
    var tenderStatusDataSource: [AllTendersModel] = []
    var viewModel: AllTendersVModeling?
    var selectedIndex = 0
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    var tenderStatus: TenderStatus = .all
    
    var durationTimer : Timer?
    
    //MARK: - LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
  
    deinit {
        print("deinit AllTendersViewC")
        invalidateTimer()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: self.tenderStatus.title,barColor: .white,titleColor: .blackTextColor, leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.setupView()
        self.recheckVM()
        self.registerNibs()
        self.setupTableView()
        self.requestTenderListAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = AllTendersViewM()
        }
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
        }
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        self.createTenderButton.setTitle(StringConstants.Text.createTender.localizedString(), for: .normal)
    }
    
    private func setupTableView() {
        self.tenderStatusTableView.delegate = self
        self.tenderStatusTableView.dataSource = self
        self.tenderStatusTableView.separatorStyle = .none
    }
    
    private func registerNibs() {
        self.tenderStatusTableView.register(TenderStatusDetailCell.self)
    }
    
    private func setupTimer() {
        self.invalidateTimer()
        self.durationTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimeValueInDataSource), userInfo: nil, repeats: true)
    }
    
    private func invalidateTimer() {
        self.durationTimer?.invalidate()
        self.durationTimer = nil
    }
    
    //MARK: - Selector Methods
    @objc func updateTimeValueInDataSource() {
        var index = 0
        for tenderData in self.tenderStatusDataSource {
            switch self.tenderStatus {
                //UpcomingTender
            case .upcomingTenders:
                if let timerDuration = tenderData.startTimerDuration {
                    tenderData.startTimerDuration = timerDuration - 1
                    //RemoveDataIfDurationIsFinished
                    if timerDuration == 0 {
                        self.tenderStatusDataSource.remove(at: index)
                        continue
                    }
                    self.tenderStatusDataSource[index] = tenderData
                    index += 1
                }
                break
                //InProgress
            case .inProgress, .noBidAccepted:
                if let timerDuration = tenderData.endTimerDuration {
                    tenderData.endTimerDuration = timerDuration - 1
                    //RemoveDataIfDurationIsFinished
                    if timerDuration == 0 {
                        self.tenderStatusDataSource.remove(at: index)
                        continue
                    }
                    self.tenderStatusDataSource[index] = tenderData
                    index += 1
                }
                break
            default:
                break
            }
        }
        self.tenderStatusTableView.reloadData()
    }
    
    func requestTenderListAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        params[ConstantAPIKeys.type] = RecoveryHomeScreenType.recovery.rawValue as AnyObject
        params[ConstantAPIKeys.status] = self.tenderStatus.rawValue as AnyObject
        self.viewModel?.requestTenderListAPI(params: params, completion: { [weak self] (nextPageNumber, perPage, tenderList) in
            guard let sSelf = self else { return }
            sSelf.tenderStatusDataSource = sSelf.tenderStatusDataSource + tenderList
            if sSelf.nextPageNumber == 1 {
                sSelf.setupTimer()
            }
            sSelf.nextPageNumber = nextPageNumber
            sSelf.perPage = perPage
            Threads.performTaskInMainQueue {
                sSelf.tenderStatusTableView.reloadData()
            }
        })
    }
    
    
    
    //MARK: - IBActions
    @IBAction func tapCreateTenderButton(_ sender: Any) {
        let chooseCarForServiceVC = DIConfigurator.sharedInstance.getChooseCarForServiceVC()
        chooseCarForServiceVC.chooseCarForServiceType = .recoveryRequest
        self.navigationController?.pushViewController(chooseCarForServiceVC, animated: true)
    }
    
}
