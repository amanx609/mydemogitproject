//
//  AllTendersModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/30/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class AllTendersModel: BaseCodable {
    var bidsCount: Int?
    var brandName: String?
    var dropLocationLongitude: Double?
    var dropOffLocation: String?
    var dropoffLocation: String?
    var pickupLocationLongitude: Double?
    var pickupLocationLatitude: Double?
    var dropLocationLatitude: Double?
    var duration: String?
    var id: Int?
    var modelName: String?
    var pickupLocation: String?
    var requestedDate: String?
    var requestedTime: String?
    var startDate: String?
    var currentDate: String?
    var endDate: String?
    var status: Int?
    var typeName: String?
    var vehicleId: Int?
    var year: Int?
    var description: String?
    var name: String?
    var contact: String?
    var acceptanceDate: String?
    var typeOfService: String?
    
    
    //Wheels
    var wheelSize: String?
    var rimServiceType:Int?
    var showServiceType: String?
    
    //Window Tinting
    var tintColor: String?
    var visibilityPercentage: String?
    //SpareParts
    var partNumber: String?
    var partType: Int?
    // Body Work
    var bodyServiceType: Int?
    var tenderBodyWorkType: Int?

    //Bank Valuation
    var bankName: String?
    var carPrice: Double?
    var bankId: String?
    
    //Car Detailing
    var carDetailingType: Int?
    
    //Insurance
    var typeOfInsurance: Int?
    var vehiclePrice: Int?
    
    var cylinders: String?
     
    
    //Timer
    var startTimerDuration: Double?
    var endTimerDuration: Double?
    
    required init(from decoder: Decoder) throws {
        let values = try! decoder.container(keyedBy: CodingKeys.self)
        self.bidsCount = try values.decodeIfPresent(Int.self, forKey: .bidsCount)
        self.brandName = try values.decodeIfPresent(String.self, forKey: .brandName)
        self.dropLocationLongitude = try values.decodeIfPresent(Double.self, forKey: .dropLocationLongitude)
        self.dropOffLocation = try values.decodeIfPresent(String.self, forKey: .dropOffLocation)
        self.dropoffLocation = try values.decodeIfPresent(String.self, forKey: .dropoffLocation)
        self.pickupLocationLongitude = try values.decodeIfPresent(Double.self, forKey: .pickupLocationLongitude)
        self.dropLocationLatitude = try values.decodeIfPresent(Double.self, forKey: .dropLocationLatitude)
        self.duration = try values.decodeIfPresent(String.self, forKey: .duration)
        self.id = try values.decodeIfPresent(Int.self, forKey: .id)
        self.modelName = try values.decodeIfPresent(String.self, forKey: .modelName)
        self.pickupLocation = try values.decodeIfPresent(String.self, forKey: .pickupLocation)
        self.pickupLocationLatitude = try values.decodeIfPresent(Double.self, forKey: .pickupLocationLatitude)
        self.requestedDate = try values.decodeIfPresent(String.self, forKey: .requestedDate)
        self.requestedTime = try values.decodeIfPresent(String.self, forKey: .requestedTime)
        self.startDate = try values.decodeIfPresent(String.self, forKey: .startDate)
        self.currentDate = try values.decodeIfPresent(String.self, forKey: .currentDate)
        self.endDate = try values.decodeIfPresent(String.self, forKey: .endDate)
        self.status = try values.decodeIfPresent(Int.self, forKey: .status)
        self.typeName = try values.decodeIfPresent(String.self, forKey: .typeName)
        self.vehicleId = try values.decodeIfPresent(Int.self, forKey: .vehicleId)
        self.year = try values.decodeIfPresent(Int.self, forKey: .year)
        self.description = try values.decodeIfPresent(String.self, forKey: .description)
        self.name = try values.decodeIfPresent(String.self, forKey: .name)
        self.contact = try values.decodeIfPresent(String.self, forKey: .contact)
        self.typeOfService = try values.decodeIfPresent(String.self, forKey: .typeOfService)
        self.acceptanceDate = try values.decodeIfPresent(String.self, forKey: .acceptanceDate)
        self.wheelSize = try values.decodeIfPresent(String.self, forKey: .wheelSize)
        
        self.partNumber = try values.decodeIfPresent(String.self, forKey: .partNumber)
        self.partType = try values.decodeIfPresent(Int.self, forKey: .partType)
        self.visibilityPercentage = try values.decodeIfPresent(String.self, forKey: .visibilityPercentage)
        self.tintColor = try values.decodeIfPresent(String.self, forKey: .tintColor)
        self.bankName = try values.decodeIfPresent(String.self, forKey: .bankName)
        self.carPrice = try values.decodeIfPresent(Double.self, forKey: .carPrice)
        self.bodyServiceType = try values.decodeIfPresent(Int.self, forKey: .bodyServiceType)
        self.rimServiceType = try values.decodeIfPresent(Int.self, forKey: .rimServiceType)
        self.showServiceType = try values.decodeIfPresent(String.self, forKey: .showServiceType)
        self.tenderBodyWorkType = try values.decodeIfPresent(Int.self, forKey: .tenderBodyWorkType)

        self.typeOfInsurance = try values.decodeIfPresent(Int.self, forKey: .typeOfInsurance)
        self.vehiclePrice = try values.decodeIfPresent(Int.self, forKey: .vehiclePrice)
        self.bankId = try values.decodeIfPresent(String.self, forKey: .bankId)
        self.cylinders = try values.decodeIfPresent(String.self, forKey: .cylinders)
        self.carDetailingType = try values.decodeIfPresent(Int.self, forKey: .carDetailingType)
        if let strCurrentDate = self.currentDate,
            let strEndDate = self.endDate,
            let currentDate = strCurrentDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss"),
            let endDate = strEndDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss") {
            self.endTimerDuration = endDate.timeIntervalSince(currentDate)
        }
        
        if let strCurrentDate = self.currentDate,
            let strStartDate = self.startDate,
            let currentDate = strCurrentDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss"),
            let startDate = strStartDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss") {
            self.startTimerDuration = startDate.timeIntervalSince(currentDate)
        }
        
        if self.tenderStatus() == .noBidAccepted,
            let strCurrentDate = self.currentDate,
            let strAcceptanceDate = self.acceptanceDate,
            let currentDate = strCurrentDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss"),
            let accptncDate = strAcceptanceDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss") {
            self.endTimerDuration = accptncDate.timeIntervalSince(currentDate)
        }
    }
    
    
    
    func timeLeft() -> String {
        return "00:00:00"
    }
    
    func getBodyWorkType() -> BodyWorkOptions {
        guard let bodyworkType = BodyWorkOptions(rawValue: Helper.toInt(self.tenderBodyWorkType)) else { return .none }
        return bodyworkType
    }
    
    func tenderStatus() -> TenderStatus {
        if let startDate = self.startDate?.getDateInstaceFrom(format: Constants.Format.apiSpaceDateTimeFormat),
        let currentDate = self.currentDate?.getDateInstaceFrom(format: Constants.Format.apiSpaceDateTimeFormat),
        let endDate = self.endDate?.getDateInstaceFrom(format: Constants.Format.apiSpaceDateTimeFormat),
        let acceptanceDate = self.acceptanceDate?.getDateInstaceFrom(format: Constants.Format.apiSpaceDateTimeFormat)
         {
            switch status {
            case 1:
                    if startDate <= currentDate && currentDate <= endDate {
                        return .inProgress
                    } else if currentDate < startDate {
                        return .upcomingTenders
                    } else if currentDate > endDate && currentDate < acceptanceDate {
                        return .noBidAccepted
                    } else if currentDate > acceptanceDate {
                        return .allBidsRejected
                    }
                break
            case 2:
                return .bidAccepted
            case 3:
                return .completedTenders
                
            case 4:
                return .allBidsRejected
  
            default:
                return .all
            }
        }
        return .all
        
}
    
    func carTitle() -> String {
        return Helper.toString(object: brandName) + " " + Helper.toString(object: modelName) + " " + Helper.toString(object: year)
    }
    
    func getDate() -> String {
        return Helper.toString(object: requestedDate?.getDateStringFrom(inputFormat: Constants.Format.dateFormatyyyyMMdd, outputFormat: Constants.Format.dateFormatdMMMyyyy))
    }
    
    func getTime() -> String {
        return Helper.toString(object: requestedTime?.getDateStringFrom(inputFormat: Constants.Format.apiTimeFormat, outputFormat: Constants.Format.timeFormathmma))
    }
    
    func getDateTime() -> String {
        return "\(Helper.toString(object: getDate())) \(Helper.toString(object: getTime()))"
    }
    
    func getInsuranceType() -> String {
        if let type =  self.typeOfInsurance, let insuranceType = InsuranceType(rawValue: type) {
            return insuranceType.title
        }
        return ""
    }
    
    func getServicingType() -> String {
        if let type = self.typeOfService {
            if type == "major" {
                return "Major"
            } else if type == "minor" {
                return "Minor"
            }
        }
        return self.typeOfService ?? ""
    }
    
}

class TenderList: BaseCodable {
    var tenders: [AllTendersModel]?
    
    private enum CodingKeys: String, CodingKey {
        case tenders = "data"
    }
}
