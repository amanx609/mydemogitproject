//
//  RDBidListModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/30/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class RDBidListModel: BaseCodable {
    var bidsCount: Int?
    var brandName: String?
    var contact: String?
    var dropoffLocationLongitude: Double?
    var dropOffLocation: String?
    var dropoffLocation: String?
    var dropoffLocationLatitude: Double?
    var duration: String?
    var id: Int?
    var modelName: String?
    var dropOffCode: String?
    var dropOffMobile: String?
    var dropOffPersonName: String?
    var endDate: String?
    var image: [String]?
    var name: String?
    var pickupCode: String?
    var pickupLocation: String?
    var pickupLocationLatitude: Double?
    var pickupLocationLongitude: Double?
    var requestedDate: String?
    var requestedTime: String?
    var typeName: String?
    var vehicleId: Int?
    var pickupMobile: String?
    var year: Int?
    var description: String?
    var additionalInfo: String?
    var Bids: BidDetails?
    var currentDate: String?
    var pickupPersonName: String?
    var recoveryType: String?
    var startDate: String?
    var status: Int?
    var tenderRecoveryStatuses: [RecoveryStatus]?
    var type: Int?
    var userImage: String?
    var driverName: String?
    var driverMobile: String?
    var driverImage: String?
    var driverEmail: String?
    var rating: Double?
    var driverAddress: String?
    var acceptanceDate: String?
    var typeOfService: String?
    var serviceOption: String?
    var wheelSize: String?
    var tintColor: String?
    var visibilityPercentage: Double?
    var carDetailServiceType: String?
    var carDetailingType: Int?
    
    //Wheels
    var rimService: String?
    var showServiceType: String?
    var tyreOrRim: Bool?
    var serviceType: Int?
    var wheelType: Int?
    
    //SpareParts
    var partNumber: String?
    var partType: Int?
    var partName: String?
    var partDescription: String?
    var deliveryLocation: String?
    var images: [String]?
    
    //BankValuation
    var bankId: String?
    var bankName: String?
    var carPrice: Double?
    
    var cylinders: String?
    var engine: Double?
    var vinNumber: String?
    var vehiclePrice: Int?
    var typeOfInsurance: Int?
    
    //BodyWork
    var dentDescription: [String]?
    var paintColor: String?
    var noOfDents: Int?
    var bodyServiceType: Int?
    var paintSiteBody: Int?
    var paintSiteGlass: Int?
    var paintSiteInterior: Int?
    var paintSitePackage: Int?
    var paintSiteRims: Int?
    
    //Timer
    var startTimerDuration: Double?
    var endTimerDuration: Double?
    var mileage: Int?
    var paintSiteArr: [Int]?
    var vatPercentage: String?
    var isDropoffcodeVerified: Int?

    required init(from decoder: Decoder) throws {
        let values = try! decoder.container(keyedBy: CodingKeys.self)
        self.bidsCount = try values.decodeIfPresent(Int.self, forKey: .bidsCount)
        self.brandName = try values.decodeIfPresent(String.self, forKey: .brandName)
        self.contact = try values.decodeIfPresent(String.self, forKey: .contact)
        self.dropoffLocationLongitude = try values.decodeIfPresent(Double.self, forKey: .dropoffLocationLongitude)
        self.dropOffLocation = try values.decodeIfPresent(String.self, forKey: .dropOffLocation)
        self.dropoffLocation = try values.decodeIfPresent(String.self, forKey: .dropoffLocation)
        self.dropoffLocationLatitude = try values.decodeIfPresent(Double.self, forKey: .dropoffLocationLatitude)
        self.duration = try values.decodeIfPresent(String.self, forKey: .duration)
        self.id = try values.decodeIfPresent(Int.self, forKey: .id)
        self.modelName = try values.decodeIfPresent(String.self, forKey: .modelName)
        self.dropOffCode = try values.decodeIfPresent(String.self, forKey: .dropOffCode)
        self.dropOffMobile = try values.decodeIfPresent(String.self, forKey: .dropOffMobile)
        self.dropOffPersonName = try values.decodeIfPresent(String.self, forKey: .dropOffPersonName)
        self.endDate = try values.decodeIfPresent(String.self, forKey: .endDate)
        self.image = try values.decodeIfPresent([String].self, forKey: .image)
        self.name = try values.decodeIfPresent(String.self, forKey: .name)
        self.pickupCode = try values.decodeIfPresent(String.self, forKey: .pickupCode)
        self.pickupLocation = try values.decodeIfPresent(String.self, forKey: .pickupLocation)
        self.pickupLocationLatitude = try values.decodeIfPresent(Double.self, forKey: .pickupLocationLatitude)
        self.pickupLocationLongitude = try values.decodeIfPresent(Double.self, forKey: .pickupLocationLongitude)
        self.requestedDate = try values.decodeIfPresent(String.self, forKey: .requestedDate)
        self.requestedTime = try values.decodeIfPresent(String.self, forKey: .requestedTime)
        self.typeName = try values.decodeIfPresent(String.self, forKey: .typeName)
        self.vehicleId = try values.decodeIfPresent(Int.self, forKey: .vehicleId)
        self.pickupMobile = try values.decodeIfPresent(String.self, forKey: .pickupMobile)
        self.year = try values.decodeIfPresent(Int.self, forKey: .year)
        self.description = try values.decodeIfPresent(String.self, forKey: .description)
        self.Bids = try values.decodeIfPresent(BidDetails.self, forKey: .Bids)
        self.currentDate = try values.decodeIfPresent(String.self, forKey: .currentDate)
        self.pickupPersonName = try values.decodeIfPresent(String.self, forKey: .pickupPersonName)
        self.recoveryType = try values.decodeIfPresent(String.self, forKey: .recoveryType)
        self.startDate = try values.decodeIfPresent(String.self, forKey: .startDate)
        self.status = try values.decodeIfPresent(Int.self, forKey: .status)
        self.rating = try values.decodeIfPresent(Double.self, forKey: .rating)
   
        self.typeOfService = try values.decodeIfPresent(String.self, forKey: .typeOfService)
        self.tenderRecoveryStatuses = try values.decodeIfPresent([RecoveryStatus].self, forKey: .tenderRecoveryStatuses)
        self.type = try values.decodeIfPresent(Int.self, forKey: .type)
        self.userImage = try values.decodeIfPresent(String.self, forKey: .userImage)
        self.driverName = try values.decodeIfPresent(String.self, forKey: .driverName)
        self.driverEmail = try values.decodeIfPresent(String.self, forKey: .driverEmail)
        self.driverAddress = try values.decodeIfPresent(String.self, forKey: .driverAddress)
        self.driverMobile = try values.decodeIfPresent(String.self, forKey: .driverMobile)
        self.driverImage = try values.decodeIfPresent(String.self, forKey: .driverImage)
        self.acceptanceDate = try values.decodeIfPresent(String.self, forKey: .acceptanceDate)
        self.image = try values.decodeIfPresent([String].self, forKey: .image)
        self.serviceOption = try values.decodeIfPresent(String.self, forKey: .serviceOption)
        self.wheelSize = try values.decodeIfPresent(String.self, forKey: .wheelSize)
        self.additionalInfo = try values.decodeIfPresent(String.self, forKey: .additionalInfo)
        
        self.partNumber = try values.decodeIfPresent(String.self, forKey: .partNumber)
        self.partType = try values.decodeIfPresent(Int.self, forKey: .partType)
        self.partName = try values.decodeIfPresent(String.self, forKey: .partName)
        self.partDescription = try values.decodeIfPresent(String.self, forKey: .partDescription)
        self.deliveryLocation = try values.decodeIfPresent(String.self, forKey: .deliveryLocation)
        self.images = try values.decodeIfPresent([String].self, forKey: .images)
        self.tintColor = try values.decodeIfPresent(String.self, forKey: .tintColor)
        self.visibilityPercentage = try values.decodeIfPresent(Double.self, forKey: .visibilityPercentage)
        self.vehiclePrice = try values.decodeIfPresent(Int.self, forKey: .vehiclePrice)
        self.typeOfInsurance = try values.decodeIfPresent(Int.self, forKey: .typeOfInsurance)
        self.mileage = try values.decodeIfPresent(Int.self, forKey: .mileage)
        
        //Wheels
        self.showServiceType = try values.decodeIfPresent(String.self, forKey: .showServiceType)
        self.rimService = try values.decodeIfPresent(String.self, forKey: .rimService)
        self.tyreOrRim = try values.decodeIfPresent(Bool.self, forKey: .tyreOrRim)
        self.wheelType = try values.decodeIfPresent(Int.self, forKey: .wheelType)
        //self.serviceType = try values.decodeIfPresent(String.self, forKey: .serviceType)
        self.serviceType = try values.decodeIfPresent(Int.self, forKey: .serviceType)
        //Bank Valuation
        self.carPrice = try values.decodeIfPresent(Double.self, forKey: .carPrice)
        self.bankId = try values.decodeIfPresent(String.self, forKey: .bankId)
        self.bankName = try values.decodeIfPresent(String.self, forKey: .bankName)
        self.cylinders = try values.decodeIfPresent(String.self, forKey: .cylinders)
        self.engine = try values.decodeIfPresent(Double.self, forKey: .engine)
        self.vinNumber = try values.decodeIfPresent(String.self, forKey: .vinNumber)
        self.carDetailServiceType = try values.decodeIfPresent(String.self, forKey: .carDetailServiceType)
        self.carDetailingType = try values.decodeIfPresent(Int.self, forKey: .carDetailingType)
        
        //BodyWork
        self.dentDescription = try values.decodeIfPresent([String].self, forKey: .dentDescription)
        self.paintColor = try values.decodeIfPresent(String.self, forKey: .paintColor)
        self.noOfDents = try values.decodeIfPresent(Int.self, forKey: .noOfDents)
        self.bodyServiceType = try values.decodeIfPresent(Int.self, forKey: .bodyServiceType)
        self.paintSiteBody = try values.decodeIfPresent(Int.self, forKey: .paintSiteBody)
        self.paintSiteGlass = try values.decodeIfPresent(Int.self, forKey: .paintSiteGlass)
        self.paintSiteInterior = try values.decodeIfPresent(Int.self, forKey: .paintSiteInterior)
        self.paintSitePackage = try values.decodeIfPresent(Int.self, forKey: .paintSitePackage)
        self.paintSiteRims = try values.decodeIfPresent(Int.self, forKey: .paintSiteRims)
        self.paintSiteArr = try values.decodeIfPresent([Int].self, forKey: .paintSiteArr)
        self.vatPercentage = try values.decodeIfPresent(String.self, forKey: .vatPercentage)
        self.isDropoffcodeVerified = try values.decodeIfPresent(Int.self, forKey: .isDropoffcodeVerified)

        if let strCurrentDate = self.currentDate,
            let strEndDate = self.endDate,
            let currentDate = strCurrentDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss"),
            let endDate = strEndDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss") {
            self.endTimerDuration = endDate.timeIntervalSince(currentDate)
        }
        
        if let strCurrentDate = self.currentDate,
            let strStartDate = self.startDate,
            let currentDate = strCurrentDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss"),
            let startDate = strStartDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss") {
            self.startTimerDuration = startDate.timeIntervalSince(currentDate)
        }
        
        if self.tenderStatus() == .noBidAccepted,
            let strCurrentDate = self.currentDate,
            let strAcceptanceDate = self.acceptanceDate,
            let currentDate = strCurrentDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss"),
            let accptncDate = strAcceptanceDate.getDateInstaceFrom(format: "yyyy-MM-dd HH:mm:ss") {
            self.endTimerDuration = accptncDate.timeIntervalSince(currentDate)
        }
    }
    
    func getServicingType() -> String {
        if let type = self.typeOfService {
            if type == "major" {
                return "Major"
            } else if type == "minor" {
                return "Minor"
            }
        }
        return self.typeOfService ?? ""
    }
    
    func getCarDetalingServiceType() -> String {
        
        let type = CarDetailServiceType(rawValue: Helper.toInt(self.carDetailServiceType))
        return type?.title ?? ""
    }
    
    func getInsuranceType() -> String {
        let type = Helper.toInt(self.typeOfInsurance)
        if let insuranceType = InsuranceType(rawValue: type) {
            return insuranceType.title
        }
        return ""
    }
    
    func getShowServiceType() -> String {
        if let type = self.rimService, let rimType = RimServiceType(rawValue: Helper.toInt(type)) {
            return rimType.title
        } else if self.tyreOrRim ?? false {
            return "Tire".localizedString()
        }
        return ""
    }
    
    func getShowServiceTypeDetails() -> String {
        
        if let type = self.rimService, let rimType = RimServiceType(rawValue: Helper.toInt(type)) {
            return rimType.title
        } else if self.wheelType == 0 {
            return "Tire".localizedString()
        }
        
        if let type = self.serviceType, let rimType = RimServiceType(rawValue: Helper.toInt(type)) {
            return rimType.title
        }
        
        return ""
    }
    
    func getDateTimeString() -> String {
        let dateStr = self.requestedDate?.getDateStringFrom(inputFormat: Constants.Format.dateFormatyyyyMMdd, outputFormat: Constants.Format.dateFormatdMMMyyyy)
        let timeStr = self.requestedTime?.getDateStringFrom(inputFormat: Constants.Format.apiTimeFormat, outputFormat: Constants.Format.timeFormathmma)
        return "\(Helper.toString(object: dateStr)), \(Helper.toString(object: timeStr))"
    }
    
    func getDate() -> String {
        let dateStr = self.requestedDate?.getDateStringFrom(inputFormat: Constants.Format.dateFormatyyyyMMdd, outputFormat: Constants.Format.dateFormatdMMMyyyy)
        return Helper.toString(object: dateStr)
        
    }
    
    func getTime() -> String {
        let timeStr = self.requestedTime?.getDateStringFrom(inputFormat: Constants.Format.apiTimeFormat, outputFormat: Constants.Format.timeFormathmma)
        return Helper.toString(object: timeStr)
    }
    
    func tenderStatus() -> TenderStatus {
        if let startDate = self.startDate?.getDateInstaceFrom(format: Constants.Format.apiSpaceDateTimeFormat),
            let currentDate = self.currentDate?.getDateInstaceFrom(format: Constants.Format.apiSpaceDateTimeFormat),
            let endDate = self.endDate?.getDateInstaceFrom(format: Constants.Format.apiSpaceDateTimeFormat),
            let accptnceDate = self.acceptanceDate?.getDateInstaceFrom(format: Constants.Format.apiSpaceDateTimeFormat)
        {
            switch status {
            case 1:
                if startDate <= currentDate && currentDate <= endDate {
                    return .inProgress
                } else if currentDate < startDate {
                    return .upcomingTenders
                } else if currentDate > endDate && currentDate < accptnceDate {
                    return .noBidAccepted
                } else if currentDate > accptnceDate {
                    return .allBidsRejected
                }
                break
            case 2:
                return .bidAccepted
            case 3:
                return .completedTenders
                
            default:
                return .all
                
            }
            
        }
        return .all
    }
    
    func getServiceStatus(value: String) -> Bool {
        if let serviceOption = self.serviceOption, serviceOption.contains(value) {
            return true
        }
        return false
    }
    
    
    func getBodyWorkType() -> (String,String) {
        
        if let typeValue = self.bodyServiceType , let bwType = BodyWorkType(rawValue: typeValue) {
                    switch bwType {
                    case .panelPainting:
                        return ("Paint Color: ", self.paintColor ?? "")
                    case .paintlessDentRemoval:
                        return ("No. of Dents: ", Helper.toString(object: noOfDents))
                    case .ceramicPaintProtection:
                        var paintSite = Helper.getPaitSite(paintSiteArray:paintSiteArr)
                        
        //                if Helper.toInt(self.paintSiteBody) == 1 {
        //                    paintSite == "" ? paintSite.append("Body") : paintSite.append(", Body")
        //                } else if Helper.toInt(self.paintSiteGlass) == 1 {
        //                    paintSite == "" ? paintSite.append("Glass") : paintSite.append(", Glass")
        //                } else if Helper.toInt(self.paintSiteInterior) == 1 {
        //                    paintSite == "" ? paintSite.append("Interior") : paintSite.append(", Interior")
        //                }
        //                    //                else if Helper.toBool(self.paintSitePackage) {
        //                    //                    paintSite == "" ? paintSite.append("Package") : paintSite.append(", Package")
        //                    //                }
                         if Helper.toInt(self.paintSiteRims) == 1 {
                            paintSite == "" ? paintSite.append("Rims") : paintSite.append(", Rims")
                            
                        }
                        return ("Paint Site : ", paintSite)
                        
                    }
                    
                }
        return ("","")
    }
    
    func getEngineSize() -> String {
        
        let engineSize = Helper.toString(object: self.engine).trim()
        
        if engineSize.count == 1 {
            return engineSize + ".0"
        }
        return engineSize
    }
    
    func getVatPercentage() -> String {
        let vatPercentage = Helper.toDouble(self.vatPercentage)
        return "+\(String(format:"%.1f",vatPercentage))% VAT"
    }
            
}

class BidDetails: BaseCodable {
    var paymentType: String?
    var transactionNo: String?
    var additionalInfo: String?
    var amount: Double?
    var id: Int?
    var isPickupDropoff: Int?
    var jobDate: String?
    var jobDescription: String?
    var jobTime: String?
    var recoveryType: String?
    var serviceDays: Int?
    var serviceProviderId: Int?
    var tintColor: String?
    var visibilityPercentage: Int?
    var showServiceType: String?
    var bankId: String?
    var bankName: String?
    var carPrice: Double?
    
    var wheelType: Int?
    
    var rimService: Int?
    var tyreOrRim: Bool?
    var serviceType: String?
    
    //SpareParts
    var partNumber: String?
    var partType: Int?
    var partName: String?
    var partDescription: String?
    var deliveryLocation: String?
    var truckNumber: String?
        
    func getJobDate() -> String {
        let dateStr = self.jobDate?.getDateStringFrom(inputFormat: Constants.Format.dateFormatyyyyMMdd, outputFormat: Constants.Format.dateFormatdMMMyyyy)
        return Helper.toString(object: dateStr)
    }
    
    func getJobTime() -> String {
        let time = self.jobTime?.getDateStringFrom(inputFormat: Constants.Format.apiTimeFormat, outputFormat: Constants.Format.timeFormathmma)
        return Helper.toString(object: time)
    }
    
    func getShowServiceTypeDetails() -> String {
        if let type = self.rimService, let rimType = RimServiceType(rawValue: Helper.toInt(type)) {
            return rimType.title
        } else if self.tyreOrRim ?? false {
            return "Tire".localizedString()
        }
        return ""
    }
    
}

class RecoveryStatus: BaseCodable {
    var startDate: String?
}

class RecoveryStatusArray: BaseCodable {
    var recoveryStatus: [RecoveryStatus]?
    
    private enum CodingKeys: String, CodingKey {
        case recoveryStatus = "tenderRecoveryStatuses"
    }
    
}
