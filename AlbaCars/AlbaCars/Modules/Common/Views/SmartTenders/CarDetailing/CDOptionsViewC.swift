//
//  CDOptionsViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class CDOptionsViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var optionsTableView: UITableView!
    
    //MARK: - Variables
    var screenType: ChooseServiceType?
    var optionsDataSource = [Any]()
    var vehicleId: Int?
    var carDetails: ChooseCarModel?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupNavigationBarTitle(title: self.screenType?.title ?? "",barColor: .white,titleColor: .blackTextColor, leftBarButtonsType: [.back], rightBarButtonsType: [])
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.registerNibs()
        self.setupTableView()
    }
    
    
    private func setupTableView() {
        self.optionsTableView.delegate = self
        self.optionsTableView.dataSource = self
        self.optionsTableView.separatorStyle = .none
        if self.screenType == .bodyWork {
            optionsDataSource = BodyWorkOptions.allValues
        } else {
            optionsDataSource = CarDetailingOptions.allValues
        }
    }
    
    private func registerNibs() {
        self.optionsTableView.register(CDOptionsCell.self)
    }
    
    
    
    //MARK: - Public Methods
    func goToNextPageForCD(rowNumber: Int) {
        let optionType = CarDetailingOptions(rawValue: rowNumber)
        switch optionType {
        case .quickWash, .bodyPolishing:
            let quickWashVC = DIConfigurator.sharedInstance.getQWRequestViewC()
            quickWashVC.vehicleId = self.vehicleId
            quickWashVC.carDetails = self.carDetails
            quickWashVC.screenType = optionType
            self.navigationController?.pushViewController(quickWashVC, animated: true)
            break
        case .detailing:
            let quickWashVC = DIConfigurator.sharedInstance.getCDDRequestViewC()
            quickWashVC.vehicleId = self.vehicleId
            quickWashVC.carDetails = self.carDetails
            self.navigationController?.pushViewController(quickWashVC, animated: true)
            break
        default:
            break
        }
    }
    
    func goToNextPageForBW(rowNumber: Int) {
        let optionType = BodyWorkOptions(rawValue: rowNumber)
               switch optionType {
               case .panelPainting,.paintlessDentRemoval:
                let ppTenderRequestVC = DIConfigurator.sharedInstance.getPPTenderRequestVC()
                ppTenderRequestVC.vehicleId = self.vehicleId
                ppTenderRequestVC.carDetails = self.carDetails
                if let optionType = optionType {
                    ppTenderRequestVC.bodyWorkOptions = optionType
                }
                
                self.navigationController?.pushViewController(ppTenderRequestVC, animated: true)
                   break
//               case .paintlessDentRemoval:
//                   break
               case .ceramicPaint:
                let ppTenderRequestVC = DIConfigurator.sharedInstance.getCPPTenderRequestVC()
                ppTenderRequestVC.vehicleId = self.vehicleId
                ppTenderRequestVC.carDetails = self.carDetails
                self.navigationController?.pushViewController(ppTenderRequestVC, animated: true)
                   break
               default:
                   break
               }
    }
    
    
}
