//
//  CDTenderListViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/20/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension CDTenderListViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tenderStatusDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        getCell(tableView, indexPath: indexPath, tenderListData: self.tenderStatusDataSource[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.CellHeightConstants.height_230
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tender = self.tenderStatusDataSource[indexPath.row]
        let type = Helper.toInt(tenderStatusDataSource[indexPath.row].carDetailingType)
        let carDetailType = CarDetailingOptions(rawValue: type)
        switch tender.tenderStatus() {
        case .bidAccepted, .completedTenders:
            let recoveryRequestViewC = DIConfigurator.sharedInstance.getCDBidAcceptedViewC()
            recoveryRequestViewC.tenderId = Helper.toInt(self.tenderStatusDataSource[indexPath.row].id)
           
            //if let carDetailingType = self.carDetailingType {
                 recoveryRequestViewC.carDetailingType = carDetailType
            //}
            recoveryRequestViewC.tenderStatus = tender.tenderStatus()
            self.navigationController?.pushViewController(recoveryRequestViewC, animated: true)
            
        default:
            let recoveryRequestViewC = DIConfigurator.sharedInstance.getCDBidListViewC()
            recoveryRequestViewC.tenderId = Helper.toInt(self.tenderStatusDataSource[indexPath.row].id)
            recoveryRequestViewC.tenderStatus = tender.tenderStatus()
            //if let carDetailingType = self.carDetailingType {
                recoveryRequestViewC.carDetailingType = carDetailType
            //}
            self.navigationController?.pushViewController(recoveryRequestViewC, animated: true)
            
        }
       
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      guard let lastVisibleRow = self.tenderStatusTableView.indexPathsForVisibleRows?.last?.row else { return }
      if (self.tenderStatusDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
        self.requestTenderListAPI()
      }
    }
}
extension CDTenderListViewC {
    func getCell(_ tableView: UITableView, indexPath: IndexPath, tenderListData: AllTendersModel) -> UITableViewCell {
        let cell: ServicingTenderListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureViewForServicing(tenderModel: tenderListData, smartTenderType: .carDetailing)
        return cell
    }
}
