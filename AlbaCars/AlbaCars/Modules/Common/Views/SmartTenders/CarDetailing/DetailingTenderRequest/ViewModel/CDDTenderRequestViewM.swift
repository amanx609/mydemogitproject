//
//  CDDTenderRequestViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/20/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol CDDTenderRequestVModeling: BaseVModeling {
    func getCDDTenderReqDataSource(carDetails: ChooseCarModel?) -> [CellInfo]
    func requestTenderAPI(vehicleId: Int, arrData: [CellInfo], completion: @escaping (Bool)-> Void)
}

class CDDTenderRequestViewM: CDDTenderRequestVModeling {
    
    func getCDDTenderReqDataSource(carDetails: ChooseCarModel?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Plate Number
        var carPlateCellInfo = [String: AnyObject]()
        carPlateCellInfo[Constants.UIKeys.inputType] = TextInputType.plateNumber as AnyObject
        let carPlateCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.carPlate.localizedString(), value: Helper.toString(object: carDetails?.plateNumber), info: carPlateCellInfo, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carPlateCell)
        
        //Car brand
        let carBrandCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.brandName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carBrandCell)
        
        //Car Model
        let carModelCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.modelName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carModelCell)
        
        //Car Year
        let carYearCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.year), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carYearCell)
        
        //Car Type
        let carTypeCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.typeName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carTypeCell)
        
        //TextView Cell
        let textViewCell = CellInfo(cellType: .TextViewCell, placeHolder: "Write Something...".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(textViewCell)
        
        //choose Service Type Label Cell
        var chooseServiceInfo = [String: AnyObject]()
        chooseServiceInfo[Constants.UIKeys.placeholderText] = "Choose Service Type".localizedString() as AnyObject
        chooseServiceInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseServiceCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "".localizedString(), value: "", info: chooseServiceInfo, height: Constants.CellHeightConstants.height_40)
        array.append(chooseServiceCell)
        
        //Select Service type label
        var radioButtonInfo = [String: AnyObject]()
        radioButtonInfo[Constants.UIKeys.firstButtonTitle] = CarDetailServiceType.interior.title as AnyObject
        radioButtonInfo[Constants.UIKeys.secondButtonTitle] = CarDetailServiceType.exterior.title as AnyObject
        radioButtonInfo[Constants.UIKeys.thirdButtonTitle] = CarDetailServiceType.both.title as AnyObject
        radioButtonInfo[Constants.UIKeys.selectedButtonIndex] = CarDetailServiceType.interior.rawValue as AnyObject
        let radioButtonInfoCell = CellInfo(cellType: .CarDetailServiceTypeCell, placeHolder: "", value: "", info: radioButtonInfo, height: Constants.CellHeightConstants.height_140)
        array.append(radioButtonInfoCell)
        
        //choose Date time Label Cell
        var chooseLabelInfo = [String: AnyObject]()
        chooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Tender".localizedString() as AnyObject
        chooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Tender".localizedString(), value: "", info: chooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseLabelCell)
        
        //DateRange Cell
        var dateRangeInfo = [String: AnyObject]()
        dateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let dateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: dateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(dateRangeCell)
        
        //Job Date time Label Cell
        var jobVhooseLabelInfo = [String: AnyObject]()
        jobVhooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Job".localizedString() as AnyObject
        jobVhooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let jobChooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Job".localizedString(), value: "", info: jobVhooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(jobChooseLabelCell)
        
        //DateRange Cell
        var jobDateRangeInfo = [String: AnyObject]()
        jobDateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let jobDateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: jobDateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(jobDateRangeCell)
        
        //choose Duration Label Cell
        var chooseDurationLabelInfo = [String: AnyObject]()
        chooseDurationLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Tender Duration".localizedString() as AnyObject
        chooseDurationLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseDurationLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Tender Duration".localizedString(), value: "", info: chooseDurationLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseDurationLabelCell)
        
        //Tender Duration
        let tenderDurationCell = CellInfo(cellType: .ImageDropDownCell, placeHolder: "", value: StringConstants.Text.tenderDuration.localizedString(), info: nil, height: Constants.CellHeightConstants.height_80)
        array.append( tenderDurationCell)
        
        // Want pickup and drop off cell
        var pickUpInfo = [String: AnyObject]()
        pickUpInfo[Constants.UIKeys.isSelected] = false as AnyObject
        pickUpInfo[Constants.UIKeys.shouldHide] = true as AnyObject
        let pickUpCell = CellInfo(cellType: .NoReserveCell, placeHolder: "I want pick Up and Drop Off".localizedString(), value:"" ,info: pickUpInfo
            , height: Constants.CellHeightConstants.height_50)
        array.append(pickUpCell)
        
        //Location cell
        var recoveryRequestInfo = [String: AnyObject]()
        recoveryRequestInfo[Constants.UIKeys.sourcePlaceHolder] = "Pick Up Location".localizedString() as AnyObject
        recoveryRequestInfo[Constants.UIKeys.destinationPlaceHolder] = "Drop Off Location".localizedString() as AnyObject
        recoveryRequestInfo[Constants.UIKeys.sourceValue] = "" as AnyObject
        recoveryRequestInfo[Constants.UIKeys.destinationValue] = "" as AnyObject
        
        let recoveryRequestCell = CellInfo(cellType: .LocationTextFieldsCell, placeHolder: "", value: "", info: recoveryRequestInfo, height: Constants.CellHeightConstants.height_220)
        array.append(recoveryRequestCell)
        
        var uploadImagesLabelInfo = [String: AnyObject]()
        uploadImagesLabelInfo[Constants.UIKeys.placeholderText] = StringConstants.Text.uploadCarImages.localizedString() as AnyObject
        uploadImagesLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let uploadImagesCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "", value: "", info: uploadImagesLabelInfo, height: Constants.CellHeightConstants.height_40)
        array.append(uploadImagesCell)
        
        //PartImages Cell
        let partImagesCell = CellInfo(cellType: .UploadVehicleImagesCell, placeHolder: "Upload Part Photos (Optional)", value: "", info: [:], height: Constants.CellHeightConstants.height_360)
        array.append(partImagesCell)
        
        return array
    }
    
    func requestTenderAPI(vehicleId: Int, arrData: [CellInfo], completion: @escaping (Bool)-> Void) {
        if self.validateFormParameters(arrData: arrData) {
            self.uploadImages(arrData: arrData) { (uploadedImages) in
                let tenderRequestParams = self.getAPIParams(arrData: arrData, vehicleId: vehicleId, images: uploadedImages)
                APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderRequestForm(param: tenderRequestParams))) { (response, success) in
                    if success {
                        if let _ =  response as? [String: AnyObject] {
                            completion(success)
                        }
                    }
                }
            }
        }
    }
    
    func validateFormParameters(arrData: [CellInfo]) -> Bool {
        var isValid = true
        
        if let date = arrData[9].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of tender.".localizedString())
            isValid = false
        } else if let time = arrData[9].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of tender.".localizedString())
            isValid = false
        } else if let date = arrData[11].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of job.".localizedString())
            isValid = false
        } else if let time = arrData[11].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of job.".localizedString())
            isValid = false
        } else if let duration = arrData[13].value, (duration.isEmpty || duration == StringConstants.Text.tenderDuration.localizedString()) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select duration of tender.".localizedString())
            isValid = false
        }  else if let willPickDrop = arrData[14].info?[Constants.UIKeys.isSelected] as? Bool, let pickupLocation = arrData[15].info?[Constants.UIKeys.sourceValue] as? String, (willPickDrop && (pickupLocation == ""))  {
            
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide your pickup location.".localizedString())
            isValid = false
        } else if let willPickDrop = arrData[14].info?[Constants.UIKeys.isSelected] as? Bool, let dropOffLocation = arrData[15].info?[Constants.UIKeys.destinationValue] as? String, (willPickDrop && dropOffLocation.isEmpty)  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide your drop off location.".localizedString())
            isValid = false
        }
        
        if isValid  {
            
            let dateStr = arrData[9].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let timeStr = arrData[9].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            
            let jobDateStr = arrData[11].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let jobTimeStr = arrData[11].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            
            let tenderDateTime = Helper.toString(object: dateStr) + " " +  Helper.toString(object: timeStr)
            let jobDateTime = Helper.toString(object: jobDateStr) + " " +  Helper.toString(object: jobTimeStr)
            isValid = Helper.compareJobAndTenderDateTime(tenderDateTime: tenderDateTime, jobDateTime: jobDateTime)
            
        }
        

        
        return isValid
    }
    
    private func uploadImages(arrData: [CellInfo], completion: @escaping(([String]) -> Void)) {
        
        var imagesToUpload: [UIImage] = []
        
        if let imagesInfo = arrData[16].info {
            if let firstImage = imagesInfo[Constants.UIKeys.firstImg] as? UIImage {
                imagesToUpload.append(firstImage)
            }
            if let secondImage = imagesInfo[Constants.UIKeys.secondImg] as? UIImage {
                imagesToUpload.append(secondImage)
            }
            if let thirdImage = imagesInfo[Constants.UIKeys.thirdImg] as? UIImage {
                imagesToUpload.append(thirdImage)
            }
            if let fourthImage = imagesInfo[Constants.UIKeys.fourthImg] as? UIImage {
                imagesToUpload.append(fourthImage)
            }
            if imagesToUpload.count > 0 {
                S3Manager.sharedInstance.uploadImages(images: imagesToUpload, imageQuality: .medium, progress: nil) { (imageUrls, imageNames, error) in
                    if let uploadedImages = imageUrls as? [String] {
                        completion(uploadedImages)
                    }
                }
            } else {
                completion([])
            }
        }
    }
    
    func getAPIParams(arrData: [CellInfo], vehicleId: Int, images: [String]) -> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.images] = images as AnyObject
        params[ConstantAPIKeys.additionalInformation] = arrData[5].value as AnyObject
        params[ConstantAPIKeys.type] = RecoveryHomeScreenType.detailing.rawValue as AnyObject
        let dateStr = arrData[9].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let timeStr = arrData[9].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        params[ConstantAPIKeys.requestedDate] = dateStr as AnyObject
        params[ConstantAPIKeys.requestedTime] = timeStr as AnyObject
        
        let jobDateStr = arrData[11].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let jobTimeStr = arrData[11].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.jobDate] = jobDateStr as AnyObject
        params[ConstantAPIKeys.jobTime] = jobTimeStr as AnyObject
        params[ConstantAPIKeys.duration] = arrData[13].placeHolder as AnyObject
        
        params[ConstantAPIKeys.pickupLocation] = arrData[15].info?[Constants.UIKeys.sourceValue] as? String == "" ? "<null>" as AnyObject: arrData[15].info?[Constants.UIKeys.sourceValue] as AnyObject
        params[ConstantAPIKeys.pickupLocationLatitude] =  arrData[15].info?[Constants.UIKeys.sourceLatitude] as AnyObject
        params[ConstantAPIKeys.pickupLocationLongitude] = arrData[15].info?[Constants.UIKeys.sourceLongitude] as AnyObject
        params[ConstantAPIKeys.dropLocation] = arrData[15].info?[Constants.UIKeys.destinationValue] as? String == "" ? "<null>" as AnyObject : arrData[15].info?[Constants.UIKeys.destinationValue] as AnyObject
        params[ConstantAPIKeys.dropLocationLatitude] =  arrData[15].info?[Constants.UIKeys.destinationLatitude] as AnyObject
        params[ConstantAPIKeys.dropLocationLongitude] = arrData[15].info?[Constants.UIKeys.destinationLongitude] as AnyObject
        params[ConstantAPIKeys.carDetailServiceType] = arrData[7].info?[Constants.UIKeys.selectedButtonIndex] as AnyObject
        
        return params
    }
    
    
}
