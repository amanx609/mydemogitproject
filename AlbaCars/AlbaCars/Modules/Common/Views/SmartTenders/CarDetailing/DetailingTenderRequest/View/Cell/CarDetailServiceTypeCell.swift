//
//  CarDetailServiceTypeCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
protocol CarDetailServiceTypeCellDelegate: class {
    func didTapCheckBoxButton(cell: CarDetailServiceTypeCell, buttonIndex: Int)
}

class CarDetailServiceTypeCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var interiorButton: UIButton!
    @IBOutlet weak var exteriorButton: UIButton!
    @IBOutlet weak var bothButton: UIButton!
    @IBOutlet weak var interiorTitleLabel: UILabel!
    @IBOutlet weak var exteriorTitleLabel: UILabel!
    @IBOutlet weak var bothTitleLabel: UILabel!
    
    //MARK: - Variables
    weak var delegate: CarDetailServiceTypeCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
   
    //MARK: - Private Methods
    private func selectButton(at index: Int) {
           switch index {
           case CarDetailServiceType.interior.rawValue:
               self.interiorButton.isSelected = true
               self.exteriorButton.isSelected = false
               self.bothButton.isSelected = false
           case CarDetailServiceType.exterior.rawValue:
               self.interiorButton.isSelected = false
               self.exteriorButton.isSelected = true
               self.bothButton.isSelected = false
           case CarDetailServiceType.both.rawValue:
               self.interiorButton.isSelected = false
               self.exteriorButton.isSelected = false
               self.bothButton.isSelected = true
           
           default:
               break
           }
       }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            if let firstButtonTitle = info[Constants.UIKeys.firstButtonTitle] as? String {
                self.interiorTitleLabel.text = firstButtonTitle
            }
            if let secondButtonTitle = info[Constants.UIKeys.secondButtonTitle] as? String {
                self.exteriorTitleLabel.text = secondButtonTitle
            }
            if let thirdButtonTitle = info[Constants.UIKeys.thirdButtonTitle] as? String {
                self.bothTitleLabel.text = thirdButtonTitle
            }
            if let selectedButtonIndex = info[Constants.UIKeys.selectedButtonIndex] as? Int {
                self.selectButton(at: selectedButtonIndex)
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func tapInteriorButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapCheckBoxButton(cell: self, buttonIndex: CarDetailServiceType.interior.rawValue)
        }
    }
    
    @IBAction func tapExteriorButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapCheckBoxButton(cell: self, buttonIndex: CarDetailServiceType.exterior.rawValue)
        }
    }
    
    @IBAction func tapBothButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapCheckBoxButton(cell: self, buttonIndex: CarDetailServiceType.both.rawValue)
        }
    }
    
    
}
