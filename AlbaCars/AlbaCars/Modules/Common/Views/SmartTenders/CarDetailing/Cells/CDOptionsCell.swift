//
//  CDOptionsCell.swift
//  AlbaCars
//
//  Created by Admin on 2/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
protocol CDOptionsCellDelegate: class {
  func didTapOptionButton(cell: CDOptionsCell)
}

class CDOptionsCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var homeInfoView: UIView!
    @IBOutlet weak var optionImageView: UIImageView!
    @IBOutlet weak var optionTitleLabel: UILabel!
    @IBOutlet weak var optionButton: UIButton!
    
    //MARK: - Variables
    weak var delegate: CDOptionsCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK: - Private Methods
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.bgView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
            self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_16, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
            self.homeInfoView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        }
    }
    
    
    private func configureCDView(rowNumber: Int) {
        let optionType = CarDetailingOptions(rawValue: rowNumber + 1)
        switch optionType {
        case .quickWash:
            self.optionImageView.image = CarDetailingOptions.quickWash.optionImage
            self.optionTitleLabel.text = CarDetailingOptions.quickWash.title
            break
        case .detailing:
            self.optionImageView.image = CarDetailingOptions.detailing.optionImage
            self.optionTitleLabel.text = CarDetailingOptions.detailing.title
            break
        case .bodyPolishing:
            self.optionImageView.image = CarDetailingOptions.bodyPolishing.optionImage
            self.optionTitleLabel.text = CarDetailingOptions.bodyPolishing.title
            break
        default:
            break
        }
    }
    
    private func configureBWView(rowNumber: Int) {
           let optionType = BodyWorkOptions(rawValue: rowNumber)
           switch optionType {
           case .panelPainting:
               self.optionImageView.image = BodyWorkOptions.panelPainting.optionImage
               self.optionTitleLabel.text = BodyWorkOptions.panelPainting.title
               break
           case .paintlessDentRemoval:
               self.optionImageView.image = BodyWorkOptions.paintlessDentRemoval.optionImage
               self.optionTitleLabel.text = BodyWorkOptions.paintlessDentRemoval.title
               break
           case .ceramicPaint:
               self.optionImageView.image = BodyWorkOptions.ceramicPaint.optionImage
               self.optionTitleLabel.text = BodyWorkOptions.ceramicPaint.title
               break
           default:
               break
           }
       }
    
    //MARK: - Public Methods
    func configureView(screenType: ChooseServiceType, rowNumber: Int) {
        switch screenType {
        case .carDetailing:
            configureCDView(rowNumber: rowNumber)
            break
        case .bodyWork:
            configureBWView(rowNumber: rowNumber)
            break
        default:
            break
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func tapOptionButton(_ sender: Any) {
        if let delegate = self.delegate {
          delegate.didTapOptionButton(cell: self)
        }
    }
    
}
