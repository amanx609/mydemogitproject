//
//  CDOptionsViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/23/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension CDOptionsViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.optionsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getCell(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return Constants.CellHeightConstants.height_240
    }
    
}

extension CDOptionsViewC {
    func getCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: CDOptionsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        
        if let screenType = self.screenType {
            cell.delegate = self
            cell.configureView(screenType: screenType, rowNumber: indexPath.row)
        }
        return cell
    }
}

extension CDOptionsViewC: CDOptionsCellDelegate {
    func didTapOptionButton(cell: CDOptionsCell) {
        guard let indexPath = self.optionsTableView.indexPath(for: cell) else { return }
        if let screenType = self.screenType {
            switch screenType {
            case .carDetailing:
                self.goToNextPageForCD(rowNumber: indexPath.row + 1)
            case .bodyWork:
                self.goToNextPageForBW(rowNumber: indexPath.row)
            default:
                break
            }
        }
    }
    
    
}
