//
//  CDBidAcceptedViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/20/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class CDBidAcceptedViewC: BaseViewC {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var secondDetailTableView: UITableView!
    @IBOutlet weak var jobCompletedButton: UIButton!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var jobCompletedButtonHeight: NSLayoutConstraint!
    
    //MARK: - Variables
    var viewModel: CDBidAcceptedVModeling?
    var inspectionDetailDataSource: [CellInfo] = []
    var acceptedBidId: Int?
    var tenderDetails: RDBidListModel?
    var tenderId: Int?
    var recoveryViewModel: RDBidListVModeling?
    var tenderStatus: TenderStatus?
    var carDetailingType: CarDetailingOptions?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func backButtonTapped() {
        if let viewControllers = self.navigationController?.viewControllers {
            //CreateAccountViewCIsPresentInNavigationStack
            for viewController in viewControllers {
                if viewController is RecoveryHomeViewC {
                    self.navigationController?.popToViewController(viewController, animated: true)
                    return
                }
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupTitle()
    }
    
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupButton()
        self.setupTableView()
        self.requestTenderDetailsAPI()
        if let tenderStatus = self.tenderStatus, tenderStatus == .completedTenders {
            self.jobCompletedButton.isHidden = true
            self.jobCompletedButtonHeight.constant = 0
            self.gradientView.isHidden = true
        }
        
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = CDBidAcceptedViewM()
        }
        if self.recoveryViewModel == nil {
            self.recoveryViewModel = RDBidListViewM()
        }
    }
    
    private func loadDataSource() {
        if let tndrDetails = self.tenderDetails,
            let dataSource = self.viewModel?.getCDBidAcceptedDataSource(tenderDetails: tndrDetails, carDetailType: self.carDetailingType ?? .quickWash) {
            if let tenderStatus = self.tenderStatus, tenderStatus != .completedTenders {
                self.secondDetailTableView.tableFooterView = self.footerView
            }
            self.inspectionDetailDataSource = dataSource
            self.secondDetailTableView.reloadData()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.secondDetailTableView.delegate = self
        self.secondDetailTableView.dataSource = self
        self.secondDetailTableView.separatorStyle = .none
        // self.secondDetailTableView.tableFooterView = self.footerView
        
    }
    
    private func setupButton() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.backgroundColor = .redButtonColor
            self.jobCompletedButton.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
        }
    }
    
    private func setupTitle() {
        if let carDetailingType = self.carDetailingType {
            switch carDetailingType {
            case .quickWash:
                self.setupNavigationBarTitle(title: "Quick Wash Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
            case .detailing:
                self.setupNavigationBarTitle(title: "Detailing Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
            case .bodyPolishing:
                self.setupNavigationBarTitle(title: "Body Polishing Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
            }
        }
        
    }
    
    private func registerNibs() {
        self.secondDetailTableView.register(STCarDetailCell.self)
        self.secondDetailTableView.register(RDContactInfoCell.self)
        self.secondDetailTableView.register(RDMapCell.self)
        self.secondDetailTableView.register(RDDriverInfoCell.self)
        self.secondDetailTableView.register(BottomLabelCell.self)
        self.secondDetailTableView.register(WServiceDetailsCell.self)
        self.secondDetailTableView.register(ThreeImageCell.self)
        self.secondDetailTableView.register(BidDetailCell.self)
        self.secondDetailTableView.register(STBidsCountCell.self)
        self.secondDetailTableView.register(SPDetailCell.self)
        self.secondDetailTableView.register(VIContactInfoCell.self)
        
    }
    
    private func requestTenderDetailsAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
        //params[ConstantAPIKeys.type] = 1 as AnyObject
        self.recoveryViewModel?.requestTenderDetailsAPI(params: params, completion: {[weak self] (tenderDetails) in
            guard let sSelf = self else { return }
            sSelf.tenderDetails = tenderDetails
            sSelf.loadDataSource()
            
        })
    }
    
    //MARK: - Public Methods
    
    
    //MARK: IBActions
    @IBAction func tapJobCompletedButton(_ sender: Any) {
        
        let jobCompletedVC = DIConfigurator.sharedInstance.getJobDoneViewC()
        jobCompletedVC.serviceProviderId = Helper.toInt(self.tenderDetails?.Bids?.serviceProviderId)
        jobCompletedVC.tenderId = Helper.toInt(self.tenderDetails?.id)
        self.navigationController?.pushViewController(jobCompletedVC, animated: true)
    }
}
