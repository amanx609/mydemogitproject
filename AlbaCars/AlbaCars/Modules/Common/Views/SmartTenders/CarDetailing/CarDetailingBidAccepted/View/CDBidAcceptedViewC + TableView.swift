//
//  CDBidAcceptedViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/20/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension CDBidAcceptedViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.inspectionDetailDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.inspectionDetailDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellInfo = self.inspectionDetailDataSource[indexPath.row]
        guard let cellType = cellInfo.cellType else { return self.inspectionDetailDataSource[indexPath.row].height }
        if (self.tenderStatus == .completedTenders) || (self.tenderStatus == .allBidsRejected) {
            
            switch cellType {
            case .VIContactInfoCell:
                return Constants.CellHeightConstants.height_0
           
            default:
                break
            }
        }
        return self.inspectionDetailDataSource[indexPath.row].height
    }
}

extension CDBidAcceptedViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .STCarDetailCell:
            let cell: STCarDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .carDetailing, tenderStatus: .all)
            return cell
            
        case .RDContactInfoCell:
            let cell: RDContactInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .RDDriverInfoCell:
            let cell: RDDriverInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .ThreeImageCell:
            let cell: ThreeImageCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .WServiceDetailsCell:
            let cell: WServiceDetailsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            if let carDetailingType = self.carDetailingType {
                cell.configureViewCD(cellInfo: cellInfo, carDetailingType: carDetailingType )
            }
            
            return cell
            
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .BidDetailCell:
            let cell: BidDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            if let carDetailingType = self.carDetailingType {
                cell.configureViewForCarDetailing(cellInfo: cellInfo, carDetailingOptions: carDetailingType)
            }
            return cell
            
        case .SPDetailCell:
            let cell: SPDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .STBidsCountCell:
            let cell: STBidsCountCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .carDetailing)
            return cell
            
        case .VIContactInfoCell:
            let cell: VIContactInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

extension CDBidAcceptedViewC: VIContactInfoCellDelegate {
    
    func didTapPhone(cell: VIContactInfoCell, phone: String) {
        guard let url = URL(string: "tel://" + phone) else { return }
        UIApplication.shared.open(url)
    }
}


//Mark: ThreeImageCellDelegate
extension CDBidAcceptedViewC: ThreeImageCellDelegate {
    func didTapViewAllCell(cell: ThreeImageCell) {
        let imageViewerVC = DIConfigurator.sharedInstance.getImageViewerVC()
        imageViewerVC.imageDataSource = self.tenderDetails?.image ?? []
        self.navigationController?.pushViewController(imageViewerVC, animated: true)
    }
    
}






