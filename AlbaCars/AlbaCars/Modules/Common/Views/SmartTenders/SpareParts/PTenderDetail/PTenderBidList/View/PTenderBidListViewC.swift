//
//  PTenderDetailViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 18/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class PTenderBidListViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tenderDetailTableView: UITableView!
    
    //MARK: - Variables
    var viewModel: PTenderBidListVModeling?
    var recoveryViewModel: RDBidListVModeling?
    var tenderDetailDataSource: [CellInfo] = []
    var nextPageNumber: Int = 1
    var tenderId: Int?
    var tenderStatus: TenderStatus = .all
    //Timer
    var durationTimer: Timer?
    var allBidList: [BidListModel] = []
    var tenderDetails: RDBidListModel?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func setup() {
        self.setupNavigationBarTitle(title: "Spare Parts Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.recheckVM()
        self.setupTableView()
        self.requestTenderDetailsAPI()
    }
    
    private func loadDataSource(tenderDetails: RDBidListModel, bidList: [BidListModel]) {
        if let dataSource = self.viewModel?.getSparePartsDetailDataSource(tenderDetails: tenderDetails, bidList: bidList) {
            self.tenderDetailDataSource = dataSource
            self.tenderDetailTableView.reloadData()
        }
    }
    
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = PTenderBidListViewM()
        }
        if self.recoveryViewModel == nil {
            self.recoveryViewModel = RDBidListViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.tenderDetailTableView.delegate = self
        self.tenderDetailTableView.dataSource = self
        self.tenderDetailTableView.separatorStyle = .none
    }
    
    private func registerNibs() {
        self.tenderDetailTableView.register(STCarDetailCell.self)
        self.tenderDetailTableView.register(SparePartInfoCell.self)
        self.tenderDetailTableView.register(ThreeImageCell.self)
        self.tenderDetailTableView.register(STBidsCountCell.self)
        self.tenderDetailTableView.register(RDBidListCell.self)
    }
    
    private func setupTimer() {
        self.invalidateTimer()
        self.durationTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimeValueInDataSource), userInfo: nil, repeats: true)
    }
    
    private func invalidateTimer() {
        self.durationTimer?.invalidate()
        self.durationTimer = nil
    }
    
    //MARK: - Selector Methods
    @objc func updateTimeValueInDataSource() {
        if var stCarDetailCellInfo = self.tenderDetailDataSource.first?.info,
            let tenderDetail = stCarDetailCellInfo[Constants.UIKeys.cellInfo] as? RDBidListModel {
            switch self.tenderStatus {
            case .upcomingTenders:
                if let timerDuration = tenderDetail.startTimerDuration {
                    tenderDetail.startTimerDuration = timerDuration - 1
                    
                    //RemoveDataIfDurationIsFinished
                    if timerDuration == 0 {
                        //ActionWhenTimerIsZero
                    }
                }
            case .inProgress, .noBidAccepted:
                if let timerDuration = tenderDetail.endTimerDuration {
                    tenderDetail.endTimerDuration = timerDuration - 1
                    
                    //RemoveDataIfDurationIsFinished
                    if timerDuration == 0 {
                        //ActionWhenTimerIsZero
                    }
                }
            default:
                break
            }
            stCarDetailCellInfo[Constants.UIKeys.cellInfo] = tenderDetail
            self.tenderDetailDataSource[0].info = stCarDetailCellInfo
            let stCarDetailCellIndexPath = IndexPath(row: 0, section: 0)
            self.tenderDetailTableView.reloadRows(at: [stCarDetailCellIndexPath], with: .none)
        }
    }
    
    //MARK: - APIMethods
    private func requestTenderDetailsAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
        
        self.recoveryViewModel?.requestTenderDetailsAPI(params: params, completion: {[weak self] (tenderDetails) in
            guard let sSelf = self else { return }
            sSelf.tenderDetails = tenderDetails
            Threads.performTaskInMainQueue {
                sSelf.loadDataSource(tenderDetails: tenderDetails, bidList: [])
                sSelf.setupTimer()
            }
            sSelf.requestBidListAPI(tenderDetails: tenderDetails)
        })
    }
    
    func requestBidListAPI(tenderDetails: RDBidListModel) {
        var params = APIParams()
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
        params[ConstantAPIKeys.orderBy] = 1 as AnyObject
        self.recoveryViewModel?.requestBidListAPI(params: params, completion: { [weak self] (nextPageNumber, perPage, bidList) in
            guard let sSelf = self else { return }
            sSelf.nextPageNumber = nextPageNumber
            sSelf.allBidList = sSelf.allBidList + bidList
            sSelf.loadDataSource(tenderDetails: tenderDetails, bidList: sSelf.allBidList)
        })
    }
    
    
    //MARK: - Public Methods
    func requestAcceptBid(bidId: Int, winAmount: Double) {
        
//      let vatAmmount = Helper.calculateVat(vatPercentage: self.tenderDetails?.vatPercentage, bidAmount: winAmount)
//      let totalAmmount = winAmount + vatAmmount

      let paymentDetails = PaymentDetailsData(viewController: self, totalAmmount: winAmount, referenceId: Helper.toInt(self.tenderId), paymentServiceCategoryId: PaymentServiceCategoryId.smartTenders,vatAmmount: 0)
        
        PaymentManager.sharedInstance.initiatePayment(paymentDetailsData: paymentDetails) { (success, paymentID) in
            
            if success {
                
                var params = APIParams()
                params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
                params[ConstantAPIKeys.bidId] = bidId as AnyObject
                params[ConstantAPIKeys.winAmount] = winAmount as AnyObject
                params[ConstantAPIKeys.paymentId] = paymentID as AnyObject
                
                self.recoveryViewModel?.requestAcceptBidAPI(params: params, completion: { [weak self] (success) in
                    guard let sSelf = self else { return }
                    let pTenderBidAcceptedVC = DIConfigurator.sharedInstance.getPTenderBidAcceptedVC()
                    pTenderBidAcceptedVC.acceptedBidId = bidId
                    pTenderBidAcceptedVC.tenderId = sSelf.tenderId
                    sSelf.navigationController?.pushViewController(pTenderBidAcceptedVC, animated: true)
                })
                
            }
        }
        
    }
    
    func requestRejectBid(bidId: Int) {
        var params = APIParams()
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        params[ConstantAPIKeys.bidId] = bidId as AnyObject
        
        self.recoveryViewModel?.requestRejectBidAPI(params: params, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            sSelf.requestTenderDetailsAPI()
            
        })
    }
    
    func requestCancelTender() {
        var params = APIParams()
        params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
        self.recoveryViewModel?.requestCancelTenderAPI(params: params, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            if let viewControllers = sSelf.navigationController?.viewControllers {
                //RecoveryHomeViewCIsPresentInNavigationStack
                for viewController in viewControllers {
                    if viewController is RecoveryHomeViewC {
                        sSelf.navigationController?.popToViewController(viewController, animated: true)
                        return
                    }
                }
            }
        })
    }
    
    func requestBidTenderDetails(bidId: Int) {
          
          let recoveryTenderViewModel:SRecoveryTenderViewM = SRecoveryTenderViewM()
          var params = APIParams()
          params[ConstantAPIKeys.tenderId] = bidId as AnyObject
          recoveryTenderViewModel.requestMyTenderDetailsAPI(params: params, completion: { [weak self] (tender) in
              guard let sSelf = self else { return }
              Threads.performTaskInMainQueue {
                  let submittedVC = DIConfigurator.sharedInstance.getSSparePartsBidSubmittedVC()
                  submittedVC.tenderDetails = tender
                  sSelf.navigationController?.pushViewController(submittedVC, animated: true)
              }
          })
      }
}
