//
//  PTenderBidAcceptedViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 18/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class PTenderBidAcceptedViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tenderDetailTableView: UITableView!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var jobCompletedButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    
    //MARK: - Variables
    var viewModel: PTenderBidAcceptedVModeling?
    var recoveryViewModel: RDBidListVModeling?
    var tenderDetailDataSource: [CellInfo] = []
    var acceptedBidId: Int?
    var tenderDetails: RDBidListModel?
    var tenderId: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func backButtonTapped() {
        if let viewControllers = self.navigationController?.viewControllers {
            //CreateAccountViewCIsPresentInNavigationStack
            for viewController in viewControllers {
                if viewController is RecoveryHomeViewC {
                    self.navigationController?.popToViewController(viewController, animated: true)
                    return
                }
            }
        }
    }
    
    //MARK: - Private Methods
       private func setup() {
           self.setupNavigationBarTitle(title: "Spare Parts Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
           self.recheckVM()
           self.setupButton()
           self.setupTableView()
           self.requestTenderDetailsAPI()
       }
       
       private func recheckVM() {
           if self.viewModel == nil {
               self.viewModel = PTenderBidAcceptedViewM()
           }
           if self.recoveryViewModel == nil {
               self.recoveryViewModel = RDBidListViewM()
           }
       }
       
       private func loadDataSource() {
          if let tndrDetails = self.tenderDetails,
           let dataSource = self.viewModel?.getSparePartsDetailDataSource(tenderDetails: tndrDetails) {
               self.tenderDetailTableView.tableFooterView = self.footerView
               self.tenderDetailDataSource = dataSource
               self.tenderDetailTableView.reloadData()
           }
       }
       
       private func setupTableView() {
           self.registerNibs()
           self.tenderDetailTableView.delegate = self
           self.tenderDetailTableView.dataSource = self
           self.tenderDetailTableView.separatorStyle = .none           
       }
       
       private func setupButton() {
           Threads.performTaskInMainQueue {
               self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
               self.gradientView.backgroundColor = .redButtonColor
               self.jobCompletedButton.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
           }
       }
       
       private func registerNibs() {
           self.tenderDetailTableView.register(STCarDetailCell.self)
           self.tenderDetailTableView.register(SparePartInfoCell.self)
           self.tenderDetailTableView.register(ThreeImageCell.self)
           self.tenderDetailTableView.register(STBidsCountCell.self)
           self.tenderDetailTableView.register(BidDetailCell.self)
           self.tenderDetailTableView.register(SPDetailCell.self)
           self.tenderDetailTableView.register(VIContactInfoCell.self)
       }
       
       private func requestTenderDetailsAPI() {
           var params = APIParams()
           params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
           self.recoveryViewModel?.requestTenderDetailsAPI(params: params, completion: {[weak self] (tenderDetails) in
               guard let sSelf = self else { return }
               sSelf.tenderDetails = tenderDetails
               sSelf.loadDataSource()
               
           })
       }
    
    //MARK: - IBActions
    @IBAction func tapJobCompleted(_ sender: UIButton) {
        let jobCompletedVC = DIConfigurator.sharedInstance.getJobDoneViewC()
        if let tenderDetails = self.tenderDetails {
            jobCompletedVC.serviceProviderId = Helper.toInt(tenderDetails.Bids?.serviceProviderId)
            jobCompletedVC.tenderId = Helper.toInt(tenderDetails.id)
        }
        self.navigationController?.pushViewController(jobCompletedVC, animated: true)
    }
}
