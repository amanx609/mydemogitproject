//
//  PTenderDetailViewC+TableView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 18/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
extension PTenderBidListViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tenderDetailDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.tenderDetailDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellInfo = self.tenderDetailDataSource[indexPath.row]
        guard let cellType = cellInfo.cellType else { return self.tenderDetailDataSource[indexPath.row].height }
        if (self.tenderStatus == .allBidsRejected) {
            
            switch cellType {
            case .STBidsCountCell:
                return Constants.CellHeightConstants.height_0
            case .RDBidListCell:
                return Constants.CellHeightConstants.height_0
                
            default:
                break
            }
        }
        return self.tenderDetailDataSource[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cellInfo = tenderDetailDataSource[indexPath.row]
        guard let cellType = cellInfo.cellType else { return }
        switch cellType {
        case .RDBidListCell:
            if let info = cellInfo.info {
                self.requestBidTenderDetails(bidId: Helper.toInt(info[ConstantAPIKeys.id]))
            }
           break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let lastVisibleRow = self.tenderDetailTableView.indexPathsForVisibleRows?.last?.row else { return }
        if (self.tenderDetailDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
            if let tenderDetail = self.tenderDetails {
                self.requestBidListAPI(tenderDetails: tenderDetail)
            }
        }
    }
}

extension PTenderBidListViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .STCarDetailCell:
            let cell: STCarDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo, type: .spareParts, tenderStatus: self.tenderStatus)
            return cell
            
        case .SparePartInfoCell:
            let cell: SparePartInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .ThreeImageCell:
            let cell: ThreeImageCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .STBidsCountCell:
            let cell: STBidsCountCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .inspectionRequest)
            return cell
            
        case .RDBidListCell:
            let cell: RDBidListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .spareParts)
            cell.delegate = self
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

extension PTenderBidListViewC: RDBidListCellDelegate {
    func didTapCheck(cell: RDBidListCell) {
        guard let indexPath = self.tenderDetailTableView.indexPath(for: cell) else { return }
        let bidId = Helper.toInt(self.tenderDetailDataSource[indexPath.row].info?[Constants.UIKeys.id])
        let bidAmount = Helper.toDouble(self.tenderDetailDataSource[indexPath.row].info?[Constants.UIKeys.bidAmount])
        self.requestAcceptBid(bidId: bidId, winAmount: bidAmount)
        
    }
    
    func didTapCross(cell: RDBidListCell) {
        guard let indexPath = self.tenderDetailTableView.indexPath(for: cell) else { return }
        let bidId = Helper.toInt(self.tenderDetailDataSource[indexPath.row].info?[Constants.UIKeys.id])
        self.requestRejectBid(bidId: bidId)
    }
}

extension PTenderBidListViewC: STCarDetailCellDelegate {
    func didTapCancelButton(cell: STCarDetailCell) {
        self.requestCancelTender()
    }
}

