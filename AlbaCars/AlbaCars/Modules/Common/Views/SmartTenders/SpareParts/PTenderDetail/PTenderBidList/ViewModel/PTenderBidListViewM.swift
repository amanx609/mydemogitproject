//
//  PTenderDetailViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 18/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol PTenderBidListVModeling: BaseVModeling {
    func getSparePartsDetailDataSource(tenderDetails: RDBidListModel, bidList: [BidListModel]) -> [CellInfo]
}

class PTenderBidListViewM: BaseViewM, PTenderBidListVModeling {
    
    func getSparePartsDetailDataSource(tenderDetails: RDBidListModel, bidList: [BidListModel]) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //CarDetailCell
        var carDetailInfo = [String: AnyObject]()
        carDetailInfo[Constants.UIKeys.cellInfo] = tenderDetails
        let carDetailCell = CellInfo(cellType: .STCarDetailCell, placeHolder: "", value: "", info: carDetailInfo, height: Constants.CellHeightConstants.height_190)
        array.append(carDetailCell)
        
        //PartDetailCell
        var partDetailInfo = [String: AnyObject]()
        partDetailInfo[Constants.UIKeys.cellInfo] = tenderDetails
        let partDetailCell = CellInfo(cellType: .SparePartInfoCell, placeHolder: "", value: "", info: partDetailInfo, height: UITableView.automaticDimension)
        array.append(partDetailCell)
        
        //Three Images Cell
        if let totalImage = tenderDetails.image, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info: imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }
        
        //BidsCountCell
        var bidsCountCellInfo = [String: AnyObject]()
        bidsCountCellInfo[Constants.UIKeys.isSecondDetailPage] = false as AnyObject
        let bidsCountCell = CellInfo(cellType: .STBidsCountCell, placeHolder: "", value: Helper.toString(object: tenderDetails.bidsCount), info: bidsCountCellInfo, height: Constants.CellHeightConstants.height_55)
        array.append(bidsCountCell)
        
        //BidCell
        for i in 0..<bidList.count {
            var bidCellInfo = [String: AnyObject]()
            bidCellInfo[Constants.UIKeys.daysForService] = bidList[i].getDaysForService() as AnyObject
            bidCellInfo[Constants.UIKeys.partType] = bidList[i].getPartType() as AnyObject
            bidCellInfo[Constants.UIKeys.partName] = bidList[i].partName as AnyObject
            bidCellInfo[Constants.UIKeys.id] = bidList[i].bidId as AnyObject
            bidCellInfo[ConstantAPIKeys.name] = bidList[i].name as AnyObject
            bidCellInfo[Constants.UIKeys.bidAmount] = bidList[i].amount as AnyObject
            bidCellInfo[ConstantAPIKeys.rating] = bidList[i].rating as AnyObject
            bidCellInfo[ConstantAPIKeys.vatAmmount] = tenderDetails.getVatPercentage() as AnyObject
            bidCellInfo[ConstantAPIKeys.image] = bidList[i].image as AnyObject

            let firstBidCell = CellInfo(cellType: .RDBidListCell, placeHolder: "", value: "", info: bidCellInfo, height: UITableView.automaticDimension)
            array.append(firstBidCell)
        }
        
        return array
    }
}
