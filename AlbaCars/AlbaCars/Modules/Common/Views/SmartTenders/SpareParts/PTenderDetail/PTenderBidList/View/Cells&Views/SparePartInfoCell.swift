//
//  SparePartInfoCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 18/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SparePartInfoCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var partTypeLabel: UILabel!
    @IBOutlet weak var partNumberLabel: UILabel!
    @IBOutlet weak var partNameLabel: UILabel!
    @IBOutlet weak var partDescriptionLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        if let partDetail = cellInfo.info?[Constants.UIKeys.cellInfo] as? RDBidListModel {
            //PartType
            if let partType = partDetail.partType, let partTypeTitle = SparePartType(rawValue: partType)?.title {
                self.partTypeLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.partTypeText.localizedString())", secondText: partTypeTitle, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            }
            //PartNumber
            self.partNumberLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.partNumberText.localizedString())", secondText: Helper.toString(object: partDetail.partNumber), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            //PartName
            self.partNameLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.partNameText.localizedString())", secondText: Helper.toString(object: partDetail.partName), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            //PartDescription
            self.partDescriptionLabel.text = partDetail.partDescription
        }
    }
}
