//
//  PTenderBidAcceptedViewC+TableView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 18/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

extension PTenderBidAcceptedViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tenderDetailDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.tenderDetailDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tenderDetailDataSource[indexPath.row].height
    }
}

extension PTenderBidAcceptedViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .STCarDetailCell:
            let cell: STCarDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .inspectionRequest, tenderStatus: .all)
            return cell
            
        case .SparePartInfoCell:
            let cell: SparePartInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .ThreeImageCell:
            let cell: ThreeImageCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .STBidsCountCell:
            let cell: STBidsCountCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .upholsteryRequest)
            return cell
            
        case .BidDetailCell:
            let cell: BidDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            //cell.configureView(cellInfo: cellInfo, type: .upholstery)
            cell.configureViewForSpareParts(cellInfo: cellInfo)
            return cell
            
        case .SPDetailCell:
            let cell: SPDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .VIContactInfoCell:
            let cell: VIContactInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

extension PTenderBidAcceptedViewC: VIContactInfoCellDelegate {
    
    func didTapPhone(cell: VIContactInfoCell, phone: String) {
        guard let url = URL(string: "tel://" + phone) else { return }
        UIApplication.shared.open(url)
    }
}


//Mark: ThreeImageCellDelegate
extension PTenderBidAcceptedViewC: ThreeImageCellDelegate {
    func didTapViewAllCell(cell: ThreeImageCell) {
        let imageViewerVC = DIConfigurator.sharedInstance.getImageViewerVC()
        imageViewerVC.imageDataSource = self.tenderDetails?.image ?? []
        self.navigationController?.pushViewController(imageViewerVC, animated: true)
    }
    
}
