//
//  PTenderBidAcceptedViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 18/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol PTenderBidAcceptedVModeling: BaseVModeling {
    func getSparePartsDetailDataSource(tenderDetails: RDBidListModel) -> [CellInfo]
}

class PTenderBidAcceptedViewM: BaseViewM, PTenderBidAcceptedVModeling {
    func getSparePartsDetailDataSource(tenderDetails: RDBidListModel) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //CarDetailCell
        var carDetailInfo = [String: AnyObject]()
        carDetailInfo[Constants.UIKeys.cellInfo] = tenderDetails
        carDetailInfo[Constants.UIKeys.isSecondDetailPage] = true as AnyObject
        let carDetailCell = CellInfo(cellType: .STCarDetailCell, placeHolder: "", value: "", info: carDetailInfo, height: Constants.CellHeightConstants.height_190)
        array.append(carDetailCell)
        
        //PartDetailCell
        var partDetailInfo = [String: AnyObject]()
        partDetailInfo[Constants.UIKeys.cellInfo] = tenderDetails
        let partDetailCell = CellInfo(cellType: .SparePartInfoCell, placeHolder: "", value: "", info: partDetailInfo, height: UITableView.automaticDimension)
        array.append(partDetailCell)
        
        //Three Images Cell
        if let totalImage = tenderDetails.image, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info: imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }
        
        //BidsCountCell
        var bidsCountCellInfo = [String: AnyObject]()
        bidsCountCellInfo[Constants.UIKeys.isSecondDetailPage] = true as AnyObject
        let bidsCountCell = CellInfo(cellType: .STBidsCountCell, placeHolder: "", value: Helper.toString(object: tenderDetails.bidsCount), info: bidsCountCellInfo, height: Constants.CellHeightConstants.height_55)
        array.append(bidsCountCell)
                
         //BidDetail Cell
         var bidDetailCellInfo = [String: AnyObject]()
         if let bids = tenderDetails.Bids {
            if let partType = bids.partType, let partTypeTitle = SparePartType(rawValue: partType)?.title {
                 bidDetailCellInfo[Constants.UIKeys.serviceType] = partTypeTitle as AnyObject
            }
             bidDetailCellInfo[Constants.UIKeys.description] = Helper.toString(object: bids.partName) as AnyObject
             bidDetailCellInfo[Constants.UIKeys.servicePrice] = Helper.toString(object: bids.amount) as AnyObject
             bidDetailCellInfo[Constants.UIKeys.paymentType] = Helper.toString(object: bids.paymentType) as AnyObject
             bidDetailCellInfo[Constants.UIKeys.transaction] =  Helper.toString(object: bids.transactionNo) as AnyObject
         }
         let bidDetailCell = CellInfo(cellType: .BidDetailCell, placeHolder: "", value: "", info: bidDetailCellInfo, height:UITableView.automaticDimension)
         array.append(bidDetailCell)
         
         //ServiceProviderDetail Cell
         var spDetailCellInfo = [String: AnyObject]()
         spDetailCellInfo[Constants.UIKeys.agencyName] = Helper.toString(object: tenderDetails.driverName) as AnyObject
        spDetailCellInfo[Constants.UIKeys.rating] = tenderDetails.rating as AnyObject
         spDetailCellInfo[Constants.UIKeys.address] = Helper.toString(object: tenderDetails.driverAddress) as AnyObject
          spDetailCellInfo[Constants.UIKeys.driverImage] = Helper.toString(object: tenderDetails.driverImage) as AnyObject
         
        let spDetailCell = CellInfo(cellType: .SPDetailCell, placeHolder: "", value: "", info: spDetailCellInfo, height: UITableView.automaticDimension)
         array.append(spDetailCell)
         
         //ContactInfo Cell
         let contactInfoCell = CellInfo(cellType: .VIContactInfoCell, placeHolder: "+971 \(Helper.toString(object: tenderDetails.contact))", value:Helper.toString(object: tenderDetails.driverEmail), info: nil, height: Constants.CellHeightConstants.height_80)
         array.append(contactInfoCell)
        return array
    }
}
