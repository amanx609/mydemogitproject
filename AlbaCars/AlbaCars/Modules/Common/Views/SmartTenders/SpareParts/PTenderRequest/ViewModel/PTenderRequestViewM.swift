
//
//  PTenderRequestViewM.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 14/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol PTenderRequestVModeling: BaseVModeling {
    func getPTenderRequestDataSource(carDetails: ChooseCarModel?) -> [CellInfo]
    func requestTenderAPI(vehicleId: Int, arrData: [CellInfo], completion: @escaping (Bool)-> Void)
}

class PTenderRequestViewM: BaseViewM, PTenderRequestVModeling {
    
    func getPTenderRequestDataSource(carDetails: ChooseCarModel?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //CarPlateNumber
        var carPlateCellInfo = [String: AnyObject]()
        carPlateCellInfo[Constants.UIKeys.inputType] = TextInputType.plateNumber as AnyObject
        let carPlateCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.carPlate.localizedString(), value: Helper.toString(object: carDetails?.plateNumber), info: carPlateCellInfo, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carPlateCell)
        
        //CarBrand
        let carBrandCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.brandName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carBrandCell)
        
        //CarModel
        let carModelCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.modelName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carModelCell)
        
        //CarType
        let carTypeCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.typeName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carTypeCell)
        
        //ChooseDateTimeLabel Cell
        var chooseLabelInfo = [String: AnyObject]()
        chooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Tender".localizedString() as AnyObject
        chooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Tender".localizedString(), value: "", info: chooseLabelInfo, height: Constants.CellHeightConstants.height_30)
        array.append(chooseLabelCell)
        
        //DateRange Cell
        var dateRangeInfo = [String: AnyObject]()
        dateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let dateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: dateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(dateRangeCell)
        
        //Job Date time Label Cell
        var jobVhooseLabelInfo = [String: AnyObject]()
        jobVhooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Job".localizedString() as AnyObject
        jobVhooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let jobChooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Job".localizedString(), value: "", info: jobVhooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(jobChooseLabelCell)
        
        //DateRange Cell
        var jobDateRangeInfo = [String: AnyObject]()
        jobDateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let jobDateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: jobDateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(jobDateRangeCell)
        
        //ChooseDurationLabel Cell
        var chooseDurationLabelInfo = [String: AnyObject]()
        chooseDurationLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Tender Duration".localizedString() as AnyObject
        chooseDurationLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseDurationLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Tender Duration".localizedString(), value: "", info: chooseDurationLabelInfo, height: Constants.CellHeightConstants.height_40)
        array.append(chooseDurationLabelCell)
        
        //TenderDuration
        let tenderDurationCell = CellInfo(cellType: .ImageDropDownCell, placeHolder: "", value: StringConstants.Text.tenderDuration.localizedString(), info: nil, height: Constants.CellHeightConstants.height_80)
        array.append( tenderDurationCell)
        
        //ChoosePartTypeLabel Cell
        var choosePartTypeInfo = [String: AnyObject]()
        choosePartTypeInfo[Constants.UIKeys.placeholderText] = "Choose Part Type ".localizedString() as AnyObject
        choosePartTypeInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let choosePartTypeCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Choose Part Type ".localizedString(), value: "", info: choosePartTypeInfo, height: Constants.CellHeightConstants.height_30)
        array.append(choosePartTypeCell)
        
        //FourRadioButtonCell
        var radioButtonInfo = [String: AnyObject]()
        radioButtonInfo[Constants.UIKeys.firstButtonTitle] = "Original New".localizedString() as AnyObject
        radioButtonInfo[Constants.UIKeys.secondButtonTitle] = "Commercial New".localizedString() as AnyObject
        radioButtonInfo[Constants.UIKeys.thirdButtonTitle] = "Orginal Used".localizedString() as AnyObject
        radioButtonInfo[Constants.UIKeys.fourthButtonTitle] = "Commercial  Used".localizedString() as AnyObject
        radioButtonInfo[Constants.UIKeys.selectedButtonIndex] = SparePartType.originalNew.rawValue as AnyObject
        let radioButtonInfoCell = CellInfo(cellType: .FourRadioButtonCell, placeHolder: "", value: "", info: radioButtonInfo, height: Constants.CellHeightConstants.height_90)
        array.append(radioButtonInfoCell)
        
        //PartNumber Cell
        //        var plateNumberCellInfo = [String: AnyObject]()
        //        plateNumberCellInfo[Constants.UIKeys.inputType] = TextInputType.plateNumber as AnyObject
        let partNumberCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.partNumber.localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_80)
        array.append(partNumberCell)
        
        //PartName Cell
        //        var carPlateCellInfo = [String: AnyObject]()
        //        carPlateCellInfo[Constants.UIKeys.inputType] = TextInputType.plateNumber as AnyObject
        let partNameCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.partName.localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_80)
        array.append(partNameCell)
        
        //PartDescription Cell
        let partDescCell = CellInfo(cellType: .TextViewCell, placeHolder: StringConstants.Text.partDescriptiom.localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(partDescCell)
        
        //PartImages Cell
        let partImagesCell = CellInfo(cellType: .UploadVehicleImagesCell, placeHolder: "Upload Part Photos (Optional)", value: "", info: [:], height: Constants.CellHeightConstants.height_360)
        array.append(partImagesCell)
        
        //WantDelivery Cell
        var wantDeliveryInfo = [String: AnyObject]()
        wantDeliveryInfo[Constants.UIKeys.isSelected] = false as AnyObject
        wantDeliveryInfo[Constants.UIKeys.shouldHide] = true as AnyObject
        let wantDeliveryCell = CellInfo(cellType: .NoReserveCell, placeHolder: "I Want Delivery".localizedString(), value:"" ,info: wantDeliveryInfo
            , height: Constants.CellHeightConstants.height_50)
        array.append(wantDeliveryCell)
        
        //DeliveryLocation Cell
        var deliveryLocationInfo = [String: AnyObject]()
        deliveryLocationInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "locationRed")
        deliveryLocationInfo[Constants.UIKeys.inputType] = TextInputType.deliveryLocation as AnyObject
        let deliveryLocationCell = CellInfo(cellType: .DeliveryLocationCell, placeHolder: "Delivery Location".localizedString(), value: "", info: deliveryLocationInfo, height: Constants.CellHeightConstants.height_80)
        array.append(deliveryLocationCell)
        
        return array
    }
    
    func requestTenderAPI(vehicleId: Int, arrData: [CellInfo], completion: @escaping (Bool)-> Void) {
        if self.validateFormParameters(arrData: arrData) {
            self.uploadImages(arrData: arrData) { (uploadedImages) in
                let tenderRequestParams = self.getAPIParams(arrData: arrData, vehicleId: vehicleId, images: uploadedImages)
                APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderRequestForm(param: tenderRequestParams))) { (response, success) in
                    if success {
                        if let _ =  response as? [String: AnyObject] {
                            completion(success)
                        }
                    }
                }
            }
        }
    }
    
    //MARK: - Private Methods
    private func validateFormParameters(arrData: [CellInfo]) -> Bool {
        var isValid = true
        if let date = arrData[5].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of tender.".localizedString())
            isValid = false
        } else if let time = arrData[5].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of tender.".localizedString())
            isValid = false
        } else if let date = arrData[7].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of job.".localizedString())
            isValid = false
        } else if let time = arrData[7].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of job.".localizedString())
            isValid = false
        } else if let duration = arrData[9].value, (duration.isEmpty || duration == StringConstants.Text.tenderDuration.localizedString()) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select duration of tender.".localizedString())
            isValid = false
        } else if let partName = arrData[13].value, partName.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide part name.".localizedString())
            isValid = false
        } else if let partDescription = arrData[14].value, partDescription.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide part description.".localizedString())
            isValid = false
        } else if let isWantDelivery = arrData[16].info?[Constants.UIKeys.isSelected] as? Bool,
            let deliveryLocation = arrData[17].value, (isWantDelivery && deliveryLocation.isEmpty)  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide delivery location.".localizedString())
            isValid = false
        }
        
        if isValid  {
            
            let dateStr = arrData[5].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let timeStr = arrData[5].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            
            let jobDateStr = arrData[7].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let jobTimeStr = arrData[7].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            
            let tenderDateTime = Helper.toString(object: dateStr) + " " +  Helper.toString(object: timeStr)
            let jobDateTime = Helper.toString(object: jobDateStr) + " " +  Helper.toString(object: jobTimeStr)
            isValid = Helper.compareJobAndTenderDateTime(tenderDateTime: tenderDateTime, jobDateTime: jobDateTime)
        }

        return isValid
    }
    
    private func uploadImages(arrData: [CellInfo], completion: @escaping(([String]) -> Void)) {
        
        var imagesToUpload: [UIImage] = []
        
        if let imagesInfo = arrData[15].info {
            if let firstImage = imagesInfo[Constants.UIKeys.firstImg] as? UIImage {
                imagesToUpload.append(firstImage)
            }
            if let secondImage = imagesInfo[Constants.UIKeys.secondImg] as? UIImage {
                imagesToUpload.append(secondImage)
            }
            if let thirdImage = imagesInfo[Constants.UIKeys.thirdImg] as? UIImage {
                imagesToUpload.append(thirdImage)
            }
            if let fourthImage = imagesInfo[Constants.UIKeys.fourthImg] as? UIImage {
                imagesToUpload.append(fourthImage)
            }
            if imagesToUpload.count > 0 {
                S3Manager.sharedInstance.uploadImages(images: imagesToUpload, imageQuality: .medium, progress: nil) { (imageUrls, imageNames, error) in
                    if let uploadedImages = imageUrls as? [String] {
                        completion(uploadedImages)
                    }
                }
            } else {
                completion([])
            }
        }
    }
    
    private func getAPIParams(arrData: [CellInfo], vehicleId: Int, images: [String]) -> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.type] = RecoveryHomeScreenType.spareParts.rawValue as AnyObject
        params[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        let dateStr = arrData[5].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let timeStr = arrData[5].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.requestedDate] = dateStr as AnyObject
        params[ConstantAPIKeys.requestedTime] = timeStr as AnyObject
        
        let jobDateStr = arrData[7].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let jobTimeStr = arrData[7].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.jobDate] = jobDateStr as AnyObject
        params[ConstantAPIKeys.jobTime] = jobTimeStr as AnyObject
        
        params[ConstantAPIKeys.duration] = arrData[9].placeHolder as AnyObject
        params[ConstantAPIKeys.partType] = arrData[11].info?[Constants.UIKeys.selectedButtonIndex] as AnyObject
        
        if let partNumber = arrData[12].value, !partNumber.isEmpty {
            params[ConstantAPIKeys.partNumber] = partNumber as AnyObject
        }
        params[ConstantAPIKeys.partName] = arrData[13].value as AnyObject
        params[ConstantAPIKeys.partDescription] = arrData[14].value as AnyObject
        params[ConstantAPIKeys.images] = images as AnyObject
        if let isDelivery = arrData[16].info?[Constants.UIKeys.isSelected] as? Bool, isDelivery {
            params[ConstantAPIKeys.isDelivery] = 1 as AnyObject
            // params[ConstantAPIKeys.deliveryLocationLatitude] = arrData[15].info?[Constants.UIKeys.deliveryLatitude] as AnyObject
            // params[ConstantAPIKeys.deliveryLocationLongitude] =  arrData[15].info?[Constants.UIKeys.deliveryLongitude] as AnyObject
            params[ConstantAPIKeys.deliveryLocation] = arrData[17].value as AnyObject
        } else {
            params[ConstantAPIKeys.isDelivery] = 0 as AnyObject
        }
        return params
    }
}


