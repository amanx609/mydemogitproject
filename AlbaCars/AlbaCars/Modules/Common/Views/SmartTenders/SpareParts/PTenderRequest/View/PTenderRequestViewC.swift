//
//  SPTenderRequestViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 14/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class PTenderRequestViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tenderRequestTableView: UITableView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet var datePicker: UIDatePicker!
    
    //MARK: - Variables
    var viewModel: PTenderRequestVModeling?
    var tenderRequestDataSource: [CellInfo] = []
    var vehicleId: Int?
    var carDetails: ChooseCarModel?
    var isDate: Bool = true
    var selectedIndexPath = IndexPath()
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: ChooseCarForServiceType.spareParts.title ?? "",barColor: .white,titleColor: .black, leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.setupViewModel()
        self.setupTableView()
        self.setupView()
        self.loadDataSource()
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = PTenderRequestViewM()
        }
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.submitButton.setTitle("Submit".localizedString(), for: .normal)
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.tenderRequestTableView.separatorStyle = .none
        self.tenderRequestTableView.backgroundColor = UIColor.white
        self.tenderRequestTableView.delegate = self
        self.tenderRequestTableView.dataSource = self
    }
    
    private func registerNibs() {
        self.tenderRequestTableView.register(TextInputCell.self)
        self.tenderRequestTableView.register(DropDownCell.self)
        self.tenderRequestTableView.register(BottomLabelCell.self)
        self.tenderRequestTableView.register(DateRangeCell.self)
        self.tenderRequestTableView.register(ImageDropDownCell.self)
        self.tenderRequestTableView.register(TextViewCell.self)
        self.tenderRequestTableView.register(UploadVehicleImagesCell.self)
        self.tenderRequestTableView.register(NoReserveCell.self)
        self.tenderRequestTableView.register(FourRadioButtonCell.self)
        self.tenderRequestTableView.register(DeliveryLocationCell.self)
        
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getPTenderRequestDataSource(carDetails: carDetails) {
            self.tenderRequestDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.tenderRequestTableView.reloadData()
            }
        }
    }
    
    private func updateDateTime(_ pickerValue: String) {
        if self.isDate {
            self.tenderRequestDataSource[self.selectedIndexPath.row].placeHolder = pickerValue
        } else {
            self.tenderRequestDataSource[self.selectedIndexPath.row].value = pickerValue
        }
        self.tenderRequestTableView.reloadData()
    }
    
    //MARK: - Public Methods
    func showDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        switch indexPath.row {
        case 9:
            listPopupView.initializeViewWith(title: "Select Time Duration", arrayList: Constants.tenderDuration, key: Constants.UIKeys.duration) { [weak self] (response) in
                guard let sSelf = self,
                    let timeDuration = response[ConstantAPIKeys.duration] as? String, let apiValue = response[Constants.UIKeys.value] as? String else { return }
                
                sSelf.tenderRequestDataSource[indexPath.row].value = timeDuration
                sSelf.tenderRequestDataSource[indexPath.row].placeHolder = apiValue
                sSelf.tenderRequestTableView.reloadRows(at: [indexPath], with: .none)
            }
            
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
    }
    func setupDateTimePicker(_ textField: UITextField) {
        textField.inputView = self.datePicker
        textField.inputAccessoryView = Helper.getDoubleButtonToolBar(leftButtonTitle: StringConstants.Text.Cancel.localizedString(), rightButtonTitle: StringConstants.Text.done.localizedString(), leftButtonselector: #selector(self.cancelButtonTapped), rightButtonSelector: #selector(self.doneButtonTapped), target: self)
        if self.isDate {
            self.datePicker.datePickerMode = .date
        } else {
            self.datePicker.datePickerMode = .time
        }
        self.datePicker.minimumDate = Date()
    }
    
    //MARK: - Selectors
    @objc func cancelButtonTapped() {
        self.view.endEditing(true)
    }
    
    @objc func doneButtonTapped() {
        self.view.endEditing(true)
        let selectedDate = self.datePicker.date
        var convertedValue = ""
        if isDate {
            convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatWithoutSpace)
            // self.auctionConfirmationDataSource[0].info?[Constants.UIKeys.firstValue] = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatdMMMyyyy) as AnyObject
        } else {
            convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.timeFormathmma)
            // self.tenderRequestDataSource[self.selectedIndexPath.row].info?[Constants.UIKeys.secondValue] = selectedDate.dateStringWith(strFormat: Constants.Format.timeFormathhmmss) as AnyObject
        }
        updateDateTime(convertedValue)
    }
    
    //MARK: - APIMethods
    private func requestTenderAPI() {
        self.viewModel?.requestTenderAPI(vehicleId: self.vehicleId ?? 0, arrData: self.tenderRequestDataSource, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            if let viewControllers = sSelf.navigationController?.viewControllers {
                //RecoveryHomeViewCIsPresentInNavigationStack
                for viewController in viewControllers {
                    if viewController is RecoveryHomeViewC {
                        sSelf.navigationController?.popToViewController(viewController, animated: true)
                        return
                    }
                }
            }
        })
    }
    
    //MARK: - IBActions
    @IBAction func tapSubmit(_ sender: UIButton) {
        self.requestTenderAPI()
    }
}
