//
//  DeliveryLocationCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 3/6/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
import GooglePlaces

protocol DeliveryLocationCellDelegate: class {
    func didChangeLocationText(cell: DeliveryLocationCell, text: String, latitude: Double, longitude: Double)
    func tapNextKeyboard(cell: DeliveryLocationCell)
}


class DeliveryLocationCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var deliveryTextField: UITextField!
    
    //MARK: - Variables
    weak var delegate: DeliveryLocationCellDelegate?
    weak var targetViewController: BaseViewC?
    var isSourceTextField = false
  
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
      super.awakeFromNib()
      self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
      super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setupView() {
      self.bgView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
      deliveryTextField.delegate = self
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
      //Placeholder
      deliveryTextField.setPlaceHolderColor(text: cellInfo.placeHolder, color: UIColor.blackColor)
      self.deliveryTextField.text = cellInfo.value
   }
}

extension DeliveryLocationCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let delegate = self.delegate {
            delegate.tapNextKeyboard(cell: self)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        self.targetViewController?.present(acController, animated: true, completion: nil)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}

extension DeliveryLocationCell: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        print(place.name)
        
        
        if let delegate = self.delegate {
            deliveryTextField.text = place.name
            delegate.didChangeLocationText(cell: self, text: place.name ?? "", latitude: place.coordinate.latitude , longitude: place.coordinate.longitude)
        }
        
        //textField.text = place.name
        // Dismiss the GMSAutocompleteViewController when something is selected
        self.targetViewController?.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        self.targetViewController?.dismiss(animated: true, completion: nil)
    }
    
}
