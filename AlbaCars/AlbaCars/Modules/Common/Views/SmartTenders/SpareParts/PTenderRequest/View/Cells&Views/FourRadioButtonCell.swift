//
//  FourRadioButtonCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 14/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol FourRadioButtonCellDelegate: class, BaseProtocol {
    func didTapRadioButton(cell: FourRadioButtonCell, buttonIndex: Int)
}

class FourRadioButtonCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirdButton: UIButton!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var fourthButton: UIButton!
    @IBOutlet weak var fourthLabel: UILabel!
    @IBOutlet weak var firstStackView: UIStackView!
    @IBOutlet weak var secondStackView: UIStackView!
    
    //MARK: - Variables
    weak var delegate: FourRadioButtonCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setup() {
        self.firstStackView.isHidden = false
        self.secondStackView.isHidden = false
    }
    
    private func selectButton(at index: Int) {
        switch index {
        case SparePartType.originalNew.rawValue:
            self.firstButton.isSelected = true
            self.secondButton.isSelected = false
            self.thirdButton.isSelected = false
            self.fourthButton.isSelected = false
        case SparePartType.commercialNew.rawValue:
            self.firstButton.isSelected = false
            self.secondButton.isSelected = true
            self.thirdButton.isSelected = false
            self.fourthButton.isSelected = false
        case SparePartType.originalUsed.rawValue:
            self.firstButton.isSelected = false
            self.secondButton.isSelected = false
            self.thirdButton.isSelected = true
            self.fourthButton.isSelected = false
        case SparePartType.commercialUsed.rawValue:
            self.firstButton.isSelected = false
            self.secondButton.isSelected = false
            self.thirdButton.isSelected = false
            self.fourthButton.isSelected = true
        default:
            break
        }
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            if let firstButtonTitle = info[Constants.UIKeys.firstButtonTitle] as? String {
                self.firstLabel.text = firstButtonTitle
            }
            if let secondButtonTitle = info[Constants.UIKeys.secondButtonTitle] as? String {
                self.secondLabel.text = secondButtonTitle
            }
            if let thirdButtonTitle = info[Constants.UIKeys.thirdButtonTitle] as? String {
                self.thirdLabel.text = thirdButtonTitle
            }
            if let fourthButtonTitle = info[Constants.UIKeys.fourthButtonTitle] as? String {
                self.fourthLabel.text = fourthButtonTitle
            }
            if let selectedButtonIndex = info[Constants.UIKeys.selectedButtonIndex] as? Int {
                self.selectButton(at: selectedButtonIndex)
            }
        }
    }
    
    func configureViewForRims(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            if let shouldHide = info[Constants.UIKeys.shouldHide] as? Bool {
                if shouldHide {
                    self.secondStackView.isHidden = true
                    self.configureView(cellInfo: cellInfo)
                } else {
                    self.secondStackView.isHidden = false
                    self.configureView(cellInfo: cellInfo)
                }
                
            }
        }
    }
    
    func configureViewForInsurance(cellInfo: CellInfo) {
       
            self.secondStackView.isHidden = true
            configureView(cellInfo: cellInfo)
        
    }
    
    //MARK: - IBActions
    @IBAction func tapFirstButton(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapRadioButton(cell: self, buttonIndex: SparePartType.originalNew.rawValue)
        }
    }
    
    @IBAction func tapSecondButton(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapRadioButton(cell: self, buttonIndex: SparePartType.commercialNew.rawValue)
        }
    }
    
    @IBAction func tapThirdButton(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapRadioButton(cell: self, buttonIndex: SparePartType.originalUsed.rawValue)
        }
    }
    
    @IBAction func tapFourthButton(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapRadioButton(cell: self, buttonIndex: SparePartType.commercialUsed.rawValue)
        }
    }
}
