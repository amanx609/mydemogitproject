//
//  PTenderRequestViewC+TableView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 14/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
import GooglePlaces

extension PTenderRequestViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tenderRequestDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.tenderRequestDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let isPickUpDropOff =  self.tenderRequestDataSource[16].info?[Constants.UIKeys.isSelected] as? Bool {
            if isPickUpDropOff {
                return self.tenderRequestDataSource[indexPath.row].height
            } else {
                let cellInfo = self.tenderRequestDataSource[indexPath.row]
                guard let cellType = cellInfo.cellType else { return Constants.CellHeightConstants.height_0 }
                if cellType == .DeliveryLocationCell {
                    return Constants.CellHeightConstants.height_0
                }
                
            }
        }
        return self.tenderRequestDataSource[indexPath.row].height
    }
}

extension PTenderRequestViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .TextInputCell:
            let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.targetViewController = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .DeliveryLocationCell:
            let cell: DeliveryLocationCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.targetViewController = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .DropDownCell:
            let cell: DropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureViewForRequestForm(cellInfo: cellInfo)
            return cell
            
        case .DateRangeCell:
            let cell: DateRangeCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureAuctionConfirmationViewCell(cellInfo: cellInfo)
            return cell
            
        case .ImageDropDownCell:
            let cell: ImageDropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .TextViewCell:
            let cell: TextViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .UploadVehicleImagesCell:
            let cell: UploadVehicleImagesCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configurePTenderRequestView(cellInfo: cellInfo)
            return cell
            
        case .NoReserveCell:
            let cell: NoReserveCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .FourRadioButtonCell:
            let cell: FourRadioButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - TextInputCellDelegate
extension PTenderRequestViewC: TextInputCellDelegate {
    func tapNextKeyboard(cell: TextInputCell) {
        
    }
    
    func didChangeText(cell: TextInputCell, text: String) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].value = text
        
        
    }
}

//MARK: - DeliveryLocationCellDelegate

extension PTenderRequestViewC: DeliveryLocationCellDelegate {
    
    func didChangeLocationText(cell: DeliveryLocationCell, text: String, latitude: Double, longitude: Double) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].value = text
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.deliveryLatitude] = latitude as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.deliveryLongitude] = longitude as AnyObject
    }
    
    func tapNextKeyboard(cell: DeliveryLocationCell) {
        
    }
    
}


//MARK: - DateRangeCellDelegate
extension PTenderRequestViewC: DateRangeCellDelegate {
    
    func didTapFromDate(cell: DateRangeCell) {
        
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.selectedIndexPath = indexPath
        self.isDate = true
        self.setupDateTimePicker(cell.fromTextField)
    }
    
    func didTapToDate(cell: DateRangeCell) {
        
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.selectedIndexPath = indexPath
        self.isDate = false
        self.setupDateTimePicker(cell.toTextField)
    }
}

//MARK: - ImageDropDownCellDelegate
extension PTenderRequestViewC: ImageDropDownCellDelegate {
    func didTapImageDropDownCell(cell: ImageDropDownCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.showDropDownAt(indexPath: indexPath)
    }
}

//MARK: - TextViewCellDelegate
extension PTenderRequestViewC: TextViewCellDelegate {
    func didChangeText(cell: TextViewCell, text: String) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].value = text
    }
}

//MARK: - UploadVehicleImagesCellDelegate
extension PTenderRequestViewC: UploadVehicleImagesCellDelegate {
    func didTapFirstImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { [weak self] (image, url) in
                if let sSelf = self, let image = image {
                    sSelf.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.firstImg] = image as AnyObject
                    sSelf.tenderRequestTableView.reloadData()
                }
            }
        }
        
    }
    
    func didTapSecondImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { [weak self] (image, url) in
                if let sSelf = self, let image = image {
                    sSelf.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.secondImg] = image as AnyObject
                    sSelf.tenderRequestTableView.reloadData()
                }
            }
        }
        
    }
    
    func didTapThirdImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { [weak self] (image, url) in
                if let sSelf = self, let image = image {
                    sSelf.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.thirdImg] = image as AnyObject
                    sSelf.tenderRequestTableView.reloadData()
                }
            }
        }
    }
    
    func didTapFourthImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { [weak self] (image, url) in
                if let sSelf = self, let image = image {
                    sSelf.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.fourthImg] = image as AnyObject
                    sSelf.tenderRequestTableView.reloadData()
                }
            }
        }
    }
}

//MARK: - NoReserveCellCellDelegate
extension PTenderRequestViewC: NoReserveCellCellDelegate {
    func didTapNoReserve(cell: NoReserveCell, isReserved: Bool) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.isSelected] = isReserved as AnyObject
        self.view.endEditing(true)
        self.tenderRequestTableView.reloadData()
    }
}

//MARK: - FourRadioButtonCellDelegate
extension PTenderRequestViewC: FourRadioButtonCellDelegate {
    func didTapRadioButton(cell: FourRadioButtonCell, buttonIndex: Int) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        if var info = self.tenderRequestDataSource[indexPath.row].info {
            info[Constants.UIKeys.selectedButtonIndex] = buttonIndex as AnyObject
            self.tenderRequestDataSource[indexPath.row].info = info
            self.tenderRequestTableView.reloadData()
        }
    }
}

extension PTenderRequestViewC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print(place.name)
        //guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[17].value = place.name
        self.tenderRequestDataSource[17].info?[Constants.UIKeys.deliveryLatitude] = place.coordinate.latitude as AnyObject
        self.tenderRequestDataSource[17].info?[Constants.UIKeys.deliveryLongitude] = place.coordinate.longitude as AnyObject
        
        self.tenderRequestTableView.reloadData()
        
        //}
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
    
}
