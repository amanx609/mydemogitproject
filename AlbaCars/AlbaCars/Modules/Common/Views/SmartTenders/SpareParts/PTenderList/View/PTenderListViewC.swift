//
//  SPTenderListViewC.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 14/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class PTenderListViewC: BaseViewC {
    
    //MARK: - IBOutelts
    @IBOutlet weak var tenderListTableView: UITableView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var createTenderButton: UIButton!
    
    //MARK: - Variables
    var viewModel: AllTendersVModeling?
    var tendersDataSource: [AllTendersModel] = []
    var tenderStatus: TenderStatus = .all
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    
    var durationTimer : Timer?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        var title = ""
        if tenderStatus == .all {
            title = ChooseServiceType.spareParts.title
        } else {
            title = self.tenderStatus.title
        }
        self.setupNavigationBarTitle(title: title, barColor: .white,titleColor: .blackTextColor, leftBarButtonsType: [.back], rightBarButtonsType: [])
    }
    
    deinit {
        print("deinit SPTenderListViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupView()
        self.recheckVM()
        self.registerNibs()
        self.setupTableView()
        self.requestTenderListAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = AllTendersViewM()
        }
    }
    
    private func setupTableView() {
        self.tenderListTableView.delegate = self
        self.tenderListTableView.dataSource = self
        self.tenderListTableView.separatorStyle = .none
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
        }
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        self.createTenderButton.setTitle(StringConstants.Text.createTender.localizedString(), for: .normal)
    }
    
    private func registerNibs() {
        self.tenderListTableView.register(ServicingTenderListCell.self)
    }
    
    private func setupTimer() {
        self.invalidateTimer()
        self.durationTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimeValueInDataSource), userInfo: nil, repeats: true)
    }
    
    private func invalidateTimer() {
        self.durationTimer?.invalidate()
        self.durationTimer = nil
    }
    
    //MARK: - Selector Methods
    @objc func updateTimeValueInDataSource() {
        var index = 0
        for tenderData in self.tendersDataSource {
            switch self.tenderStatus {
            //UpcomingTender
            case .upcomingTenders:
                if let timerDuration = tenderData.startTimerDuration {
                    tenderData.startTimerDuration = timerDuration - 1
                    //RemoveDataIfDurationIsFinished
                    if timerDuration == 0 {
                        self.tendersDataSource.remove(at: index)
                        continue
                    }
                    self.tendersDataSource[index] = tenderData
                    index += 1
                }
            //InProgress
            case .inProgress, .noBidAccepted:
                if let timerDuration = tenderData.endTimerDuration {
                    tenderData.endTimerDuration = timerDuration - 1
                    //RemoveDataIfDurationIsFinished
                    if timerDuration == 0 {
                        self.tendersDataSource.remove(at: index)
                        continue
                    }
                    self.tendersDataSource[index] = tenderData
                    index += 1
                }
            default:
                break
            }
        }
        self.tenderListTableView.reloadData()
    }
    
    //MARK: - APIMethods
    func requestTenderListAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        params[ConstantAPIKeys.type] = RecoveryHomeScreenType.spareParts.rawValue as AnyObject
        params[ConstantAPIKeys.status] = self.tenderStatus.rawValue as AnyObject
        self.viewModel?.requestTenderListAPI(params: params, completion: { [weak self] (nextPageNumber, perPage, tenderList) in
            guard let sSelf = self else { return }
            sSelf.tendersDataSource = sSelf.tendersDataSource + tenderList
            if sSelf.nextPageNumber == 1 {
                sSelf.setupTimer()
            }
            sSelf.nextPageNumber = nextPageNumber
            sSelf.perPage = perPage
            Threads.performTaskInMainQueue {
                sSelf.tenderListTableView.reloadData()
            }
        })
    }
    
    //MARK: - IBActions
    @IBAction func tapCreateTender(_ sender: UIButton) {
        let chooseCarForServiceVC = DIConfigurator.sharedInstance.getChooseCarForServiceVC()
        chooseCarForServiceVC.chooseCarForServiceType = .spareParts
        self.navigationController?.pushViewController(chooseCarForServiceVC, animated: true)
    }
    
}
