//
//  SPTenderListViewC+TableView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 14/02/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension PTenderListViewC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tendersDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tenderListData = self.tendersDataSource[indexPath.row]
        return getCell(tableView, indexPath: indexPath, tenderListData: tenderListData)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tender = self.tendersDataSource[indexPath.row]
        switch tender.tenderStatus() {
        case .bidAccepted, .completedTenders:
            let pTenderBidAcceptedVC = DIConfigurator.sharedInstance.getPTenderBidAcceptedVC()
            pTenderBidAcceptedVC.tenderId = Helper.toInt(self.tendersDataSource[indexPath.row].id)
            self.navigationController?.pushViewController(pTenderBidAcceptedVC, animated: true)
        default:
            let pTenderBidListVC = DIConfigurator.sharedInstance.getPTenderBidListVC()
            pTenderBidListVC.tenderId = Helper.toInt(self.tendersDataSource[indexPath.row].id)
            pTenderBidListVC.tenderStatus = tender.tenderStatus()
            self.navigationController?.pushViewController(pTenderBidListVC, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      guard let lastVisibleRow = self.tenderListTableView.indexPathsForVisibleRows?.last?.row else { return }
      if (self.tendersDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
        self.requestTenderListAPI()
      }
    }
}
extension PTenderListViewC {
    func getCell(_ tableView: UITableView, indexPath: IndexPath, tenderListData: AllTendersModel) -> UITableViewCell {
        let cell: ServicingTenderListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureViewForSpareParts(tenderModel: tenderListData)
        return cell
    }
}
