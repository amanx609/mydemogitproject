//
//  SmartTendersViewC.swift
//  AlbaCars
//
//  Created by Narendra on 1/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SmartTendersViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var smartTendersCollectionView: UICollectionView!
    
    //MARK: - Variables
    var smartTendersDataSource: [CellInfo] = []
    var viewModel: SmartTendersViewModeling?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Smart Tenders".localizedString(),barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
    }
    
    deinit {
        print("deinit SmartTendersViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupView()
        self.recheckVM()
        self.setupCollectionView()
        self.loadDataSource()
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getSmartTendersDataSource() {
            self.smartTendersDataSource = dataSource
        }
    }
    
    //MARK: - Private Methods
    private func setupView() {
        
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = SmartTendersViewM()
        }
    }
    
    private func setupCollectionView() {
        self.registerNibs()
        self.smartTendersCollectionView.delegate = self
        self.smartTendersCollectionView.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //depending upon direction of collection view
        self.smartTendersCollectionView?.setCollectionViewLayout(layout, animated: true)
        //self.homeCollectionView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.smartTendersCollectionView.register(HomeCollectionCell.self)
    }
    
    func goToSelectedScreen(indx: Int) {
        
        switch indx {
        case 0:
            // RecoveryHome
            let recoveryHomeVC = DIConfigurator.sharedInstance.getRecoveryHomeVC()
            recoveryHomeVC.recoveryHomeType = .recovery
            self.navigationController?.pushViewController(recoveryHomeVC, animated: true)
            break
        case 1:
            //Vehicle Inspection
            let recoveryHomeVC = DIConfigurator.sharedInstance.getRecoveryHomeVC()
            recoveryHomeVC.recoveryHomeType = .inspection
            self.navigationController?.pushViewController(recoveryHomeVC, animated: true)

            break
        case 2:
            //Car Servicing
            let recoveryHomeVC = DIConfigurator.sharedInstance.getRecoveryHomeVC()
            recoveryHomeVC.recoveryHomeType = .servicing
            self.navigationController?.pushViewController(recoveryHomeVC, animated: true)
            break
        case 3:
            //Upholstery
            let recoveryHomeVC = DIConfigurator.sharedInstance.getRecoveryHomeVC()
            recoveryHomeVC.recoveryHomeType = .upholstery
            self.navigationController?.pushViewController(recoveryHomeVC, animated: true)
            break
        case 4:
            //SpareParts
            let recoveryHomeVC = DIConfigurator.sharedInstance.getRecoveryHomeVC()
            recoveryHomeVC.recoveryHomeType = .spareParts
            self.navigationController?.pushViewController(recoveryHomeVC, animated: true)
            break
        case 5:
            //Wheels
            let recoveryHomeVC = DIConfigurator.sharedInstance.getRecoveryHomeVC()
            recoveryHomeVC.recoveryHomeType = .wheels
            self.navigationController?.pushViewController(recoveryHomeVC, animated: true)
            break
        case 6:
            //Body Work
            let recoveryHomeVC = DIConfigurator.sharedInstance.getRecoveryHomeVC()
            recoveryHomeVC.recoveryHomeType = .bodyWork
            self.navigationController?.pushViewController(recoveryHomeVC, animated: true)
            break
        case 7:
            //Car Detailing
            let recoveryHomeVC = DIConfigurator.sharedInstance.getRecoveryHomeVC()
            recoveryHomeVC.recoveryHomeType = .carDetailing
            self.navigationController?.pushViewController(recoveryHomeVC, animated: true)
            break
        case 8:
            //Window Tinting
            let recoveryHomeVC = DIConfigurator.sharedInstance.getRecoveryHomeVC()
            recoveryHomeVC.recoveryHomeType = .windowTinting
            self.navigationController?.pushViewController(recoveryHomeVC, animated: true)
            break
        case 9:
           // Bank Valuation
            let recoveryHomeVC = DIConfigurator.sharedInstance.getRecoveryHomeVC()
            recoveryHomeVC.recoveryHomeType = .bankValuation
            self.navigationController?.pushViewController(recoveryHomeVC, animated: true)
            break
        case 10:
            //Insurance
            let recoveryHomeVC = DIConfigurator.sharedInstance.getRecoveryHomeVC()
            recoveryHomeVC.recoveryHomeType = .insurance
            self.navigationController?.pushViewController(recoveryHomeVC, animated: true)
            break
        default:
            break
        }
        
    }
}
