//
//  SmartTendersViewC+CollectionView.swift
//  AlbaCars
//
//  Created by Narendra on 1/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension SmartTendersViewC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.smartTendersDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellInfo = self.smartTendersDataSource[indexPath.row]
        return self.getCell(collectionView: collectionView, indexPath: indexPath, cellInfo: cellInfo)
    }
  
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width = collectionView.bounds.width/2-5
        if let id = self.smartTendersDataSource[indexPath.row].info?[Constants.UIKeys.id] as? Int, id == ChooseServiceType.insurance.rawValue, self.smartTendersDataSource.count % 2 == 1 {
           width = collectionView.bounds.width
        }
        
        return CGSize(width: width, height: self.smartTendersDataSource[indexPath.row].height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        self.goToSelectedScreen(indx: indexPath.row)

    }
    
}

extension SmartTendersViewC {
    func getCell(collectionView: UICollectionView, indexPath: IndexPath, cellInfo: CellInfo) -> UICollectionViewCell {
        guard let cellType = cellInfo.cellType else { return UICollectionViewCell() }
        switch cellType {
        case .HomeCollectionCell:
            let cell: HomeCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureForRecovery(cellInfo: cellInfo)
            return cell
        default:
            return UICollectionViewCell()
        }
    }
}
