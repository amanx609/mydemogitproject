//
//  SmartTendersViewM.swift
//  AlbaCars
//
//  Created by Narendra on 1/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol SmartTendersViewModeling: BaseVModeling {
    func getSmartTendersDataSource() -> [CellInfo]
}

class SmartTendersViewM: SmartTendersViewModeling {
    
    func getSmartTendersDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        for chooseServiceType in ChooseServiceType.allValues{
            var info = [String: AnyObject]()
            info[Constants.UIKeys.placeholderImage] = chooseServiceType.homeImage
            info[Constants.UIKeys.id] = chooseServiceType.rawValue as AnyObject
            let cell = CellInfo(cellType: .HomeCollectionCell, placeHolder: chooseServiceType.title, value: "", info: info, height: Constants.CellHeightConstants.height_128)
            array.append(cell)
        }
        
        return array
    }
}


