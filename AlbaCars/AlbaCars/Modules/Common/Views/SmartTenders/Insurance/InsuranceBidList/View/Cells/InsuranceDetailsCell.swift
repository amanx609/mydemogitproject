//
//  InsuranceDetailsCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class InsuranceDetailsCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var carMileageLabel: UILabel!
    @IBOutlet weak var vehicleValueLabel: UILabel!
    @IBOutlet weak var insuranceTypeLabel: UILabel!
    @IBOutlet weak var infoDetailLabel: UILabel!
    @IBOutlet weak var additionalInfoLabel: UILabel!
    @IBOutlet weak var insuranceBgView: UIView!
    @IBOutlet weak var carMileageBgView: UIView!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.insuranceBgView.isHidden = false
        self.carMileageBgView.isHidden = false
    }
    
    //MARK: - Public Methods
    //Wheels
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            //serviceType
            self.carMileageLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.carMileageText.localizedString()) ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.carMileage])) Km", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
           
            self.insuranceTypeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.insuranceTypeText.localizedString()) ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.insuranceType]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.vehicleValueLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.vehicleValueText.localizedString()) ", secondText: "AED \(Helper.toString(object: info[Constants.UIKeys.vehicleValue]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.infoDetailLabel.text = Helper.toString(object: info[Constants.UIKeys.additionalInformation])
            self.additionalInfoLabel.text = "Additional Information".localizedString()
        }
    }
    
   
    
    
}
