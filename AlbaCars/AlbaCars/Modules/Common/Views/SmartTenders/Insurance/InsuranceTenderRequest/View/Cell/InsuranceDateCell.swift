//
//  InsuranceDateCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol InsuranceDateCellDelegate: class {
    func didTapDate(cell: InsuranceDateCell)
    
}

class InsuranceDateCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var bgView: UIView!
    
    //MARK: - Variables
    weak var delegate: InsuranceDateCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK: - Private Methods
    private func setupView() {
      self.bgView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
      //dateTextField.delegate = self
      
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        let placeholderText = NSAttributedString(string: cellInfo.placeHolder,
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.blackColor,
                     NSAttributedString.Key.font: UIFont.font(name: .AirbnbCerealApp, weight: .Book, size: .size_14)])
        
        self.dateTextField.attributedPlaceholder = placeholderText
        self.dateTextField.delegate = self
        self.dateTextField.text = cellInfo.value
    }
    
  
}

extension InsuranceDateCell: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.dateTextField {
            if let delegate = self.delegate {
                delegate.didTapDate(cell: self)
            }
        }
    }
    
}
