//
//  ITenderRequestViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension ITenderRequestViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tenderRequestDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.tenderRequestDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tenderRequestDataSource[indexPath.row].height
    }
}

extension ITenderRequestViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .LocationTextFieldsCell:
            let cell: LocationTextFieldsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.targetViewController = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .DateRangeCell:
            let cell: DateRangeCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureAuctionConfirmationViewCell(cellInfo: cellInfo)
            return cell
            
        case .TextInputCell:
            let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .InsuranceDateCell:
        let cell: InsuranceDateCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .DropDownCell:
            let cell: DropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureViewForRequestForm(cellInfo: cellInfo)
            return cell
            
        case .TextViewCell:
            let cell: TextViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureViewWithBlackPlaceholder(cellInfo: cellInfo)
            return cell
            
        case .ImageDropDownCell:
            let cell: ImageDropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .InsuranceTypeCell:
            let cell: InsuranceTypeCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .FourRadioButtonCell:
            let cell: FourRadioButtonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureViewForInsurance(cellInfo: cellInfo)
            return cell
            
        case .NoReserveCell:
            let cell: NoReserveCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .ChooseServiceOptionCell:
            let cell: ChooseServiceOptionCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            //cell.configureView(cellInfo: cellInfo)
            return cell
            
            
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - DropDownCellDelegate
extension ITenderRequestViewC: DateRangeCellDelegate {
    func didTapFromDate(cell: DateRangeCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.selectedIndexPath = indexPath
        self.isDate = true
        self.setupDateTimePicker(cell.fromTextField, indexPath: indexPath)
    }
    
    func didTapToDate(cell: DateRangeCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.selectedIndexPath = indexPath
        self.isDate = false
        self.setupDateTimePicker(cell.toTextField, indexPath: indexPath)
        
    }
    
}

//MARK: - TextInputCellDelegate
extension ITenderRequestViewC: TextInputCellDelegate {
    func tapNextKeyboard(cell: TextInputCell) {
        self.view.endEditing(true)
    }
    
    func didChangeText(cell: TextInputCell, text: String) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].value = text
    }
}

//MARK: - DropDownCellDelegate
extension ITenderRequestViewC: DropDownCellDelegate {
    func didTapDropDownCell(cell: DropDownCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.showDropDownAt(indexPath: indexPath)
    }
}

//MARK: - ImageDropDownCellDelegate
extension ITenderRequestViewC: ImageDropDownCellDelegate {
    func didTapImageDropDownCell(cell: ImageDropDownCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.showDropDownAt(indexPath: indexPath)
    }
}

//MARK: - NoReserveCellCellDelegate
extension ITenderRequestViewC: NoReserveCellCellDelegate {
    func didTapNoReserve(cell: NoReserveCell, isReserved: Bool) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.isSelected] = isReserved as AnyObject
        self.view.endEditing(true)
        
    }
}

//MARK: - LocationTextFieldCellDelegate
extension ITenderRequestViewC: LocationTextFieldsCellDelegate {
    func didChangeSourceText(cell: LocationTextFieldsCell, text: String, latitude: Double, longitude: Double) {
         guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.sourceValue] = text as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.sourceLatitude] = latitude as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.sourceLongitude] = longitude as AnyObject
    }
    
    func didChangeDestinationText(cell: LocationTextFieldsCell, text: String, latitude: Double, longitude: Double) {
         guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
               self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.destinationValue] = text as AnyObject
               self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.destinationLatitude] = latitude as AnyObject
               self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.destinationLongitude] = longitude as AnyObject
    }
    
    func tapNextKeyboard(cell: LocationTextFieldsCell) {
        self.view.endEditing(true)
    }
}



//MARK: - TextViewCellDelegate
extension ITenderRequestViewC: TextViewCellDelegate {
    func didChangeText(cell: TextViewCell, text: String) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].value = text
    }
}

//MARK: - ChooseServiceOptions Cell Delegate
extension ITenderRequestViewC: ChooseServiceOptionCellDelegate {
    func didSelectServiceType(cell: ChooseServiceOptionCell, value: String) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.serviceType] = value as AnyObject
    }
    
    func didUnSelectSubService(cell: ChooseServiceOptionCell, value: String) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell), var servicesArray =  self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.subServices] as? [String] else { return }
        servicesArray.removeObject(object: value)
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.subServices] = servicesArray as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.subServicesString] = servicesArray.joined(separator:",") as AnyObject
    }
    
    func didSelectSubService(cell: ChooseServiceOptionCell, value: String) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell), var servicesArray =  self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.subServices] as? [String] else { return }
        servicesArray.append(value)
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.subServices] = servicesArray as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.subServicesString] = servicesArray.joined(separator:",") as AnyObject
    }
}

//MARK: - FourRadioButtonCellDelegate
extension ITenderRequestViewC: FourRadioButtonCellDelegate {
    func didTapRadioButton(cell: FourRadioButtonCell, buttonIndex: Int) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        if var info = self.tenderRequestDataSource[indexPath.row].info {
            info[Constants.UIKeys.selectedButtonIndex] = buttonIndex as AnyObject
            self.tenderRequestDataSource[indexPath.row].info = info
            self.tenderRequestTableView.reloadData()
        }
    }
}

//MARK: - InsuranceTypeCellDelegate
extension ITenderRequestViewC: InsuranceTypeCellDelegate {
    func didTapRadioButton(cell: InsuranceTypeCell, buttonIndex: Int) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        if var info = self.tenderRequestDataSource[indexPath.row].info {
            info[Constants.UIKeys.selectedButtonIndex] = buttonIndex as AnyObject
            self.tenderRequestDataSource[indexPath.row].info = info
            self.tenderRequestTableView.reloadData()
        }
    }
}

extension ITenderRequestViewC: InsuranceDateCellDelegate {
    func didTapDate(cell: InsuranceDateCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.selectedIndexPath = indexPath
        self.setupDateTimePicker(cell.dateTextField, indexPath: indexPath)
    }
    
}


