//
//  ITenderRequestViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol ITenderRequestVModeling: BaseVModeling {
    func getIReqDataSource(carDetails: ChooseCarModel?) -> [CellInfo]
    func getAPIParams(arrData: [CellInfo], vehicleId: Int) -> APIParams
    func validateFormParameters(arrData: [CellInfo]) -> Bool
}

class ITenderRequestViewM: BaseViewM, ITenderRequestVModeling {
    func getIReqDataSource(carDetails: ChooseCarModel?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Plate Number
        let carPlateCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.carPlate.localizedString(), value: Helper.toString(object: carDetails?.plateNumber), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carPlateCell)
        
        //Car brand
        let carBrandCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value:  Helper.toString(object: carDetails?.brandName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carBrandCell)
        
        //Car Model
        let carModelCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value:  Helper.toString(object: carDetails?.modelName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carModelCell)
        
        //Car Type
        let carTypeCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.typeName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carTypeCell)
        
        //Car Year
        let carYearCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.year), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carYearCell)
        
        //Mileage
        let carCylinderCell = CellInfo(cellType: .TextInputCell, placeHolder: "", value: Helper.toString(object: carDetails?.mileage), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carCylinderCell)
        
        //Is Your car gcc label Cell
        var isCarGccLabelInfo = [String: AnyObject]()
        isCarGccLabelInfo[Constants.UIKeys.placeholderText] = "Is Your Car GCC?".localizedString() as AnyObject
        isCarGccLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let isCarGccLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "".localizedString(), value: "", info: isCarGccLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(isCarGccLabelCell)
        
        //TwoRadioButtonCell
        var radioButtonInfo = [String: AnyObject]()
        radioButtonInfo[Constants.UIKeys.firstButtonTitle] = "Yes".localizedString() as AnyObject
        radioButtonInfo[Constants.UIKeys.secondButtonTitle] = "No".localizedString() as AnyObject
        radioButtonInfo[Constants.UIKeys.selectedButtonIndex] = 1 as AnyObject
        let radioButtonInfoCell = CellInfo(cellType: .FourRadioButtonCell, placeHolder: "", value: "", info: radioButtonInfo, height: Constants.CellHeightConstants.height_40)
        array.append(radioButtonInfoCell)
        
        
        //Is Your car gcc label Cell
        var insuranceTypeLabelInfo = [String: AnyObject]()
        insuranceTypeLabelInfo[Constants.UIKeys.placeholderText] = "Choose the type of Insurance you wish to have".localizedString() as AnyObject
        insuranceTypeLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let insuranceTypeLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "".localizedString(), value: "", info: insuranceTypeLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(insuranceTypeLabelCell)
        
        
        //Insurancetype Cell
        var insuranceTypeCellInfo = [String: AnyObject]()
        insuranceTypeCellInfo[Constants.UIKeys.selectedButtonIndex] = InsuranceCellDataType.AgencyRepair.rawValue as AnyObject
        let insuranceTypeCell = CellInfo(cellType: .InsuranceTypeCell, placeHolder: "".localizedString(), value: "", info: insuranceTypeCellInfo, height: Constants.CellHeightConstants.height_192)
        array.append(insuranceTypeCell)
        
        //Is Your car gcc label Cell
        var vehiclePurchaseLabelInfo = [String: AnyObject]()
        vehiclePurchaseLabelInfo[Constants.UIKeys.placeholderText] = "Means of Vehicle Purchase".localizedString() as AnyObject
        vehiclePurchaseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let vehiclePurchaseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "".localizedString(), value: "", info: vehiclePurchaseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(vehiclePurchaseLabelCell)
        
        //TwoRadioButtonCell
        var radioButton1Info = [String: AnyObject]()
        radioButton1Info[Constants.UIKeys.firstButtonTitle] = "Cash".localizedString() as AnyObject
        radioButton1Info[Constants.UIKeys.secondButtonTitle] = "Bank Finance".localizedString() as AnyObject
        radioButton1Info[Constants.UIKeys.selectedButtonIndex] = 1 as AnyObject
        let radioButton1InfoCell = CellInfo(cellType: .FourRadioButtonCell, placeHolder: "", value: "", info: radioButton1Info, height: Constants.CellHeightConstants.height_40)
        array.append(radioButton1InfoCell)
        
        //Additional Information Cell
        let addInfoCell = CellInfo(cellType: .TextViewCell, placeHolder: "Additional Information".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(addInfoCell)
        
        //price input
        var priceLabelInfo = [String: AnyObject]()
        priceLabelInfo[Constants.UIKeys.inputType] = TextInputType.expectedPrice as AnyObject
        let priceInputCell = CellInfo(cellType: .TextInputCell, placeHolder: "Value of Vehicle(Price)", value:"", info: priceLabelInfo, height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.numberPad)
        array.append(priceInputCell)
        
        //insuranceDate input
        var insuranceDateLabelInfo = [String: AnyObject]()
        insuranceDateLabelInfo[Constants.UIKeys.inputType] = TextInputType.expectedPrice as AnyObject
        let insuranceDateCell = CellInfo(cellType: .InsuranceDateCell, placeHolder: "Insurance Activation Date", value:"", info: insuranceDateLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(insuranceDateCell)
        
        //choose Date time Label Cell
        var chooseLabelInfo = [String: AnyObject]()
        chooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Tender".localizedString() as AnyObject
        chooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Tender".localizedString(), value: "", info: chooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseLabelCell)
        
        //DateRange Cell
        var dateRangeInfo = [String: AnyObject]()
        dateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let dateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: dateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(dateRangeCell)
        
        //Job Date time Label Cell
        var jobVhooseLabelInfo = [String: AnyObject]()
        jobVhooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Job".localizedString() as AnyObject
        jobVhooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let jobChooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Job".localizedString(), value: "", info: jobVhooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(jobChooseLabelCell)
        
        //DateRange Cell
        var jobDateRangeInfo = [String: AnyObject]()
        jobDateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let jobDateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: jobDateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(jobDateRangeCell)
        
        //choose Duration Label Cell
        var chooseDurationLabelInfo = [String: AnyObject]()
        chooseDurationLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Tender Duration".localizedString() as AnyObject
        chooseDurationLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseDurationLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Tender Duration".localizedString(), value: "", info: chooseDurationLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseDurationLabelCell)
        
        //Tender Duration
        let tenderDurationCell = CellInfo(cellType: .ImageDropDownCell, placeHolder: "", value: StringConstants.Text.tenderDuration.localizedString(), info: nil, height: Constants.CellHeightConstants.height_80)
        array.append( tenderDurationCell)
        
        return array
    }
    
    func requestTenderAPI(arrData: [CellInfo], vehicleId: Int, completion: @escaping (Bool)-> Void) {
        if self.validateFormParameters(arrData: arrData) {
            let tenderRequestParams = self.getAPIParams(arrData: arrData, vehicleId: vehicleId)
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderRequestForm(param: tenderRequestParams))) { (response, success) in
                if success {
                    if let safeResponse =  response as? [String: AnyObject]
                    {
                        completion(success)
                    }
                }
            }
        }
    }
    
    func validateFormParameters(arrData: [CellInfo]) -> Bool {
        
        var isValid = true
        if let price = arrData[13].value, price.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please Enter the Car Price".localizedString())
            isValid = false
        } else if let duration = arrData[14].value, (duration.isEmpty || duration == StringConstants.Text.tenderDuration.localizedString()) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select activation date of insurance.".localizedString())
            isValid = false
        } else if let date = arrData[16].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of tender.".localizedString())
            isValid = false
        } else if let time = arrData[16].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of tender.".localizedString())
            isValid = false
        } else if let date = arrData[18].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of job.".localizedString())
            isValid = false
        } else if let time = arrData[18].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of job.".localizedString())
            isValid = false
        } else if let duration = arrData[20].value, (duration.isEmpty || duration == StringConstants.Text.tenderDuration.localizedString()) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select duration of tender.".localizedString())
            isValid = false
        }
        
        if isValid  {
            
            let dateStr = arrData[16].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let timeStr = arrData[16].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            
            let jobDateStr = arrData[18].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let jobTimeStr = arrData[18].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            
            let tenderDateTime = Helper.toString(object: dateStr) + " " +  Helper.toString(object: timeStr)
            let jobDateTime = Helper.toString(object: jobDateStr) + " " +  Helper.toString(object: jobTimeStr)
            
            isValid = Helper.compareJobAndTenderDateTime(tenderDateTime: tenderDateTime, jobDateTime: jobDateTime)
            
        }

        
        return isValid
    }
    
    func getImagesFromImagesCell(arrData: [CellInfo]) {
        //let images = []
    }
    
    func getAPIParams(arrData: [CellInfo], vehicleId: Int) -> APIParams {
        
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        params[ConstantAPIKeys.gccCar] = arrData[7].info?[Constants.UIKeys.selectedButtonIndex] as AnyObject
        params[ConstantAPIKeys.typeOfInsurance] = arrData[9].info?[Constants.UIKeys.selectedButtonIndex] as AnyObject
        params[ConstantAPIKeys.vehiclePurchase] = arrData[11].info?[Constants.UIKeys.selectedButtonIndex] as AnyObject
        params[ConstantAPIKeys.type] = RecoveryHomeScreenType.insurance.rawValue as AnyObject
        params[ConstantAPIKeys.vehiclePrice] = arrData[13].value as AnyObject
        let insuranceDateStr = arrData[14].value.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        params[ConstantAPIKeys.insuranceActivationDate] = insuranceDateStr as AnyObject

        let dateStr = arrData[16].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let timeStr = arrData[16].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.requestedDate] = dateStr as AnyObject
        params[ConstantAPIKeys.requestedTime] = timeStr as AnyObject
        
        let jobDateStr = arrData[18].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let jobTimeStr = arrData[18].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.jobDate] = jobDateStr as AnyObject
        params[ConstantAPIKeys.jobTime] = jobTimeStr as AnyObject
        params[ConstantAPIKeys.duration] = arrData[20].placeHolder as AnyObject
        return params
    }
}
