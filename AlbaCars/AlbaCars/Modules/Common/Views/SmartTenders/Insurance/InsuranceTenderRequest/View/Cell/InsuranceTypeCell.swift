//
//  InsuranceTypeCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/26/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol InsuranceTypeCellDelegate: class, BaseProtocol {
    func didTapRadioButton(cell: InsuranceTypeCell, buttonIndex: Int)
}


class InsuranceTypeCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var comprehensiveButton: UIButton!
    @IBOutlet weak var thirdPartyButton: UIButton!
    @IBOutlet weak var agencyPairButton: UIButton!
    @IBOutlet weak var nonAgencyPairButton: UIButton!
    
    //MARK: - Variables
    weak var delegate: InsuranceTypeCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK: - Private Methods
    private func selectButton(at index: Int) {
        switch index {
        case InsuranceCellDataType.comprehensive.rawValue:
            self.comprehensiveButton.isSelected = true
            if !self.agencyPairButton.isSelected && !self.agencyPairButton.isSelected {
                self.agencyPairButton.isSelected = false
                self.nonAgencyPairButton.isSelected = false
            }
            
            self.thirdPartyButton.isSelected = false
        case InsuranceCellDataType.AgencyRepair.rawValue:
            self.comprehensiveButton.isSelected = true
            self.agencyPairButton.isSelected = true
            self.nonAgencyPairButton.isSelected = false
            self.thirdPartyButton.isSelected = false
        case InsuranceCellDataType.NonAgencyRepair.rawValue:
            self.comprehensiveButton.isSelected = true
            self.agencyPairButton.isSelected = false
            self.nonAgencyPairButton.isSelected = true
            self.thirdPartyButton.isSelected = false
        case InsuranceCellDataType.ThirdParty.rawValue:
            self.comprehensiveButton.isSelected = false
            self.agencyPairButton.isSelected = false
            self.nonAgencyPairButton.isSelected = false
            self.thirdPartyButton.isSelected = true
        default:
            break
        }
    }
    
    //MARK: Public Methods
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            if let selectedButtonIndex = info[Constants.UIKeys.selectedButtonIndex] as? Int {
                self.selectButton(at: selectedButtonIndex)
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func tapComprehensiveButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapRadioButton(cell: self, buttonIndex: InsuranceCellDataType.comprehensive.rawValue)
        }
    }
    
    @IBAction func tapAgencyButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapRadioButton(cell: self, buttonIndex: InsuranceCellDataType.AgencyRepair.rawValue)
        }
    }
    
    @IBAction func tapNonAgencyButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapRadioButton(cell: self, buttonIndex: InsuranceCellDataType.NonAgencyRepair.rawValue)
        }
    }
    
    @IBAction func tapThirdPartyButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapRadioButton(cell: self, buttonIndex: InsuranceCellDataType.ThirdParty.rawValue)
        }
    }
}
