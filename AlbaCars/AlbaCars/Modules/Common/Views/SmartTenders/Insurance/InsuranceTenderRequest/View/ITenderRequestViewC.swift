//
//  ITenderRequestViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class ITenderRequestViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tenderRequestTableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet var dateTimePicker: UIDatePicker!
    
    //Variables
    var viewModel: ITenderRequestVModeling?
    var bankViewModel: SBankValuationTenderDetailsViewModeling?
    var tenderRequestDataSource: [CellInfo] = []
    var isDate = true
    var selectedIndexPath = IndexPath()
    var vehicleId: Int?
    var addCarViewModel: CAddNewCarViewModeling?
    var requestCarDataSource: [CellInfo] = []
    var carDetails: ChooseCarModel?
    var bankDataSource: [[String: AnyObject]] = []
    
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupNavigationBarTitle(title: ChooseCarForServiceType.insurance.title ?? "",barColor: .white,titleColor: .black, leftBarButtonsType: [.back], rightBarButtonsType: [])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    
    //MARK: - Private Methods
    private func setup() {
        
        self.registerNibs()
        self.setupViewModel()
        self.setupView()
        self.setupTableView()
        self.loadDataSource()
        self.requestBankAPI()
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.submitButton.setTitle("Submit".localizedString(), for: .normal)
    }
    
    private func setupTableView() {
        self.tenderRequestTableView.separatorStyle = .none
        self.tenderRequestTableView.backgroundColor = UIColor.white
        self.tenderRequestTableView.delegate = self
        self.tenderRequestTableView.dataSource = self
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = ITenderRequestViewM()
        }
        if self.addCarViewModel == nil {
            self.addCarViewModel = CAddNewCarVM()
        }
        
        if self.bankViewModel == nil {
            self.bankViewModel = SBankValuationTenderDetailsViewM()
        }
    }
    
    private func registerNibs() {
        self.tenderRequestTableView.register(LocationTextFieldsCell.self)
        self.tenderRequestTableView.register(DropDownCell.self)
        self.tenderRequestTableView.register(DateRangeCell.self)
        self.tenderRequestTableView.register(BottomLabelCell.self)
        self.tenderRequestTableView.register(ImageDropDownCell.self)
        self.tenderRequestTableView.register(NoReserveCell.self)
        self.tenderRequestTableView.register(TextViewCell.self)
        self.tenderRequestTableView.register(UploadVehicleImagesCell.self)
        self.tenderRequestTableView.register(TextInputCell.self)
        self.tenderRequestTableView.register(ChooseServiceOptionCell.self)
        self.tenderRequestTableView.register(FourRadioButtonCell.self)
        self.tenderRequestTableView.register(InsuranceTypeCell.self)
        self.tenderRequestTableView.register(InsuranceDateCell.self)
        
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getIReqDataSource(carDetails: self.carDetails) {
            self.tenderRequestDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.tenderRequestTableView.reloadData()
            }
        }
    }
    
    private func updateDateTime(_ pickerValue: String) {
        guard let cellType = self.tenderRequestDataSource[self.selectedIndexPath.row].cellType else { return }
        switch cellType {
        case .DateRangeCell:
            if isDate {
                self.tenderRequestDataSource[self.selectedIndexPath.row].placeHolder = pickerValue
            } else {
                self.tenderRequestDataSource[self.selectedIndexPath.row].value = pickerValue
            }
            break
        case .InsuranceDateCell:
            self.tenderRequestDataSource[self.selectedIndexPath.row].value = pickerValue
            break
        default:
            break
        }
        self.tenderRequestTableView.reloadData()
    }
    
    //MARK: - API Methods
    func requestBankAPI() {
        self.bankViewModel?.requestBankAPI(completion: { (success, bankList) in
            if success {
                self.bankDataSource = bankList
            }
        })
    }
    
    //MARK: - Public Methods
    func setupDateTimePicker(_ textField: UITextField, indexPath: IndexPath) {
        textField.inputView = self.dateTimePicker
        textField.inputAccessoryView = Helper.getDoubleButtonToolBar(leftButtonTitle: StringConstants.Text.Cancel.localizedString(), rightButtonTitle: StringConstants.Text.done.localizedString(), leftButtonselector: #selector(self.cancelButtonTapped), rightButtonSelector: #selector(self.doneButtonTapped), target: self)
        guard let cellType = self.tenderRequestDataSource[self.selectedIndexPath.row].cellType else { return }
        switch cellType {
        case .DateRangeCell :
            if isDate {
                self.dateTimePicker.datePickerMode = .date
            } else {
                self.dateTimePicker.datePickerMode = .time
            }
            self.dateTimePicker.minimumDate = Date()

            break
        case .InsuranceDateCell:
            self.dateTimePicker.datePickerMode = .date
            self.dateTimePicker.minimumDate = nil
            break
            
        default:
            
            break
        }
    }
    
    func setupToolBar(_ textField: UITextField) {
        textField.inputAccessoryView = Helper.getSingleButtonToolBar(buttonTitle: StringConstants.Text.done.localizedString(), selector: #selector(self.doneButtonTapped), target: self)
    }
    
    func showDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        switch indexPath.row {
            
        case 20:
            listPopupView.initializeViewWith(title: "Select Time Duration", arrayList: Constants.tenderDuration, key: Constants.UIKeys.duration) { [weak self] (response) in
                guard let sSelf = self,
                    let timeDuration = response[ConstantAPIKeys.duration] as? String, let apiValue = response[Constants.UIKeys.value] as? String else { return }
                sSelf.tenderRequestDataSource[indexPath.row].value = timeDuration
                sSelf.tenderRequestDataSource[indexPath.row].placeHolder = apiValue
                sSelf.tenderRequestTableView.reloadData()
            }
            
        case 15:
            if self.bankDataSource.count == 0 {
                self.requestBankAPI()
                return
            }
            listPopupView.initializeViewWith(title: "Select Bank".localizedString(), arrayList: self.bankDataSource, key: ConstantAPIKeys.name) { [weak self] (response) in
                guard let sSelf = self,
                    let bankName = response[ConstantAPIKeys.name] as? String,
                    let bankId = response[ConstantAPIKeys.bankId] as? String else { return }
                
                sSelf.tenderRequestDataSource[indexPath.row].value = bankName
                sSelf.tenderRequestDataSource[indexPath.row].placeHolder = bankId
                sSelf.tenderRequestTableView.reloadRows(at: [indexPath], with: .none)
            }
            
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    
    
    //MARK: - Selectors
    @objc func cancelButtonTapped() {
        self.view.endEditing(true)
    }
    
    @objc func doneButtonTapped() {
        self.view.endEditing(true)
        let selectedDate = self.dateTimePicker.date
        var convertedValue = ""
        
        guard let cellType = self.tenderRequestDataSource[self.selectedIndexPath.row].cellType else { return }
        switch cellType {
        case .DateRangeCell:
            if isDate {
                convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatWithoutSpace)
            } else {
                convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.timeFormathmma)
               // self.tenderRequestDataSource[self.selectedIndexPath.row].info?[Constants.UIKeys.secondValue] = selectedDate.dateStringWith(strFormat: Constants.Format.timeFormathhmmss) as AnyObject
            }
            break
        case .InsuranceDateCell:
            convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatWithoutSpace)
            break
        default:
            break
        }
        
        updateDateTime(convertedValue)
    }
    
    
    
    //MARK: - API Methods
    //    func requestTenderAPI() {
    //        self.viewModel?.requestTenderAPI(arrData: self.tenderRequestDataSource, vehicleId: vehicleId ?? 0, completion: { [weak self] (success) in
    //            guard let sSelf = self else { return }
    //            if let viewControllers = sSelf.navigationController?.viewControllers {
    //                //CreateAccountViewCIsPresentInNavigationStack
    //                for viewController in viewControllers {
    //                    if viewController is RecoveryHomeViewC {
    //                        sSelf.navigationController?.popToViewController(viewController, animated: true)
    //                        return
    //                    }
    //                }
    //
    //            }
    //        })
    //    }
    //
    
    
    //MARK: - IBActions
    @IBAction func tapSubmitButton(_ sender: Any) {
        
        let uploadCarImagesVC = DIConfigurator.sharedInstance.getIUploadCarImagesVC()
        if self.viewModel?.validateFormParameters(arrData: self.tenderRequestDataSource) ?? false {
            if  let apiParams = self.viewModel?.getAPIParams(arrData:self.tenderRequestDataSource, vehicleId: self.vehicleId ?? 0) {
                uploadCarImagesVC.firstScreenParams = apiParams
            }
            self.navigationController?.pushViewController(uploadCarImagesVC, animated: true)
        }
        
        
        
    }
}
