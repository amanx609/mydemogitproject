//
//  IUploadImagesViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension IUploadImagesViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.uploadDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        getCell(tableView, indexPath: indexPath, cellInfo: self.uploadDataSource[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let cellHeight = self.uploadDataSource[indexPath.row].height else { return 0.0}
        return cellHeight
    }
    
}

extension IUploadImagesViewC {
    func getCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = self.uploadDataSource[indexPath.row].cellType else { return UITableViewCell()}
        switch cellType {
        case .UploadVehicleImagesCell:
            let cell: UploadVehicleImagesCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            cell.delegate = self
            return cell
            
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        
        default:
            break
        }
        return UITableViewCell()
    }
}

extension IUploadImagesViewC: UploadVehicleImagesCellDelegate {
    func didTapFirstImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.uploadCarImagesTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.uploadDataSource[indexPath.row].info?[Constants.UIKeys.firstImg] = image as AnyObject
                    self.uploadCarImagesTableView.reloadData()
                }
            }
        }
        
    }
    
    func didTapSecondImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.uploadCarImagesTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.uploadDataSource[indexPath.row].info?[Constants.UIKeys.secondImg] = image as AnyObject
                    self.uploadCarImagesTableView.reloadData()
                }
            }
        }
    }
    
    func didTapThirdImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.uploadCarImagesTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.uploadDataSource[indexPath.row].info?[Constants.UIKeys.thirdImg] = image as AnyObject
                    self.uploadCarImagesTableView.reloadData()
                }
            }
        }
    }
    
    func didTapFourthImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.uploadCarImagesTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.uploadDataSource[indexPath.row].info?[Constants.UIKeys.fourthImg] = image as AnyObject
                    self.uploadCarImagesTableView.reloadData()
                }
            }
        }
    }
}


