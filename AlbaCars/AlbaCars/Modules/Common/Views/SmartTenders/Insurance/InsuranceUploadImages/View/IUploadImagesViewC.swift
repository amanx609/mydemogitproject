//
//  IUploadImagesViewC.swift
//  AlbaCars
//
//  Created by Admin on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

class IUploadImagesViewC: BaseViewC {
    
    @IBOutlet weak var uploadCarImagesTableView: TPKeyboardAvoidingTableView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var drivingImageView: UIImageView!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var certificateImageView: UIImageView!
    
    //MARK: - Variables
    var uploadDataSource: [CellInfo] = []
    var viewModel: IUploadImgReqVModeling?
    var firstScreenParams = APIParams()
    var docParams = [String: AnyObject]()

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupNavigationBarTitle(title: ChooseCarForServiceType.insurance.title ?? "",barColor: .white,titleColor: .black, leftBarButtonsType: [.back], rightBarButtonsType: [])
        
    }
    
    //MARK: - Private Methods
    
    private func setup() {
        self.recheckVM()
        self.setupView()
        self.setupTableView()
        self.loadDataSource()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = IUploadImagesViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.uploadCarImagesTableView.delegate = self
        self.uploadCarImagesTableView.dataSource = self
        self.uploadCarImagesTableView.separatorStyle = .none
        self.uploadCarImagesTableView.allowsSelection = false
        self.uploadCarImagesTableView.tableHeaderView = self.headerView
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.submitButton.setTitle("Submit".localizedString(), for: .normal)
    }
    
    private func registerNibs() {
        self.uploadCarImagesTableView.register(UploadVehicleImagesCell.self)
        self.uploadCarImagesTableView.register(BottomLabelCell.self)
    }
    
    private func loadDataSource() {
        
        if let dataSource = self.viewModel?.getITenderReqDataSource() {
            self.uploadDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.uploadCarImagesTableView.reloadData()
            }
        }
    }
        
    //MARK: - IBActions
    
    @IBAction func tapSubmitButton(_ sender: Any) {
        self.viewModel?.requestTenderAPI(arrData: self.uploadDataSource, documentData: self.docParams, firstScreenAPIParams: self.firstScreenParams, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            if let viewControllers = sSelf.navigationController?.viewControllers {
                //CreateAccountViewCIsPresentInNavigationStack
                for viewController in viewControllers {
                    if viewController is RecoveryHomeViewC {
                        sSelf.navigationController?.popToViewController(viewController, animated: true)
                        return
                    }
                }
            }
        })
    }
    
    @IBAction func tapDrivingLicenceImgButton(_ sender: Any) {
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.drivingImageView.image = image
                    self.uploadCarImagesTableView.reloadData()
                    Loader.showLoader()
                    S3Manager.sharedInstance.uploadImage(image: image, imageQuality: .medium, progress: nil) { (imageUrl, imageName, error) in
                        Loader.hideLoader()
                        self.docParams[Constants.UIKeys.docFrontImage] = Helper.toString(object: imageName) as AnyObject
                    }
                    
                }
            }
        }
    }
    
    @IBAction func tapBackImgButton(_ sender: Any) {
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.backImageView.image = image
                    self.uploadCarImagesTableView.reloadData()
                    Loader.showLoader()
                    S3Manager.sharedInstance.uploadImage(image: image, imageQuality: .medium, progress: nil) { (imageUrl, imageName, error) in
                        Loader.hideLoader()
                        self.docParams[Constants.UIKeys.docBackImage] = Helper.toString(object: imageName) as AnyObject
                    }
                    
                }
            }
        }
    }
    
    @IBAction func tapCertificateImgButton(_ sender: Any) {
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.certificateImageView.image = image
                    self.uploadCarImagesTableView.reloadData()
                    Loader.showLoader()
                    S3Manager.sharedInstance.uploadImage(image: image, imageQuality: .medium, progress: nil) { (imageUrl, imageName, error) in
                        Loader.hideLoader()
                        self.docParams[Constants.UIKeys.docCertificateImage] = Helper.toString(object: imageName) as AnyObject
                    }
                }
            }
        }
    }
}
