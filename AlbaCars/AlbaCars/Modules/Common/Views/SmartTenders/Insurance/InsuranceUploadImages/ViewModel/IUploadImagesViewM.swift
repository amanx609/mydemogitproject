//
//  IUploadImagesViewM.swift
//  AlbaCars
//
//  Created by Admin on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol IUploadImgReqVModeling: BaseVModeling {
    func getITenderReqDataSource() -> [CellInfo]
    func requestTenderAPI(arrData: [CellInfo], documentData: [String: AnyObject], firstScreenAPIParams: APIParams,  completion: @escaping (Bool)-> Void)
    
}

class IUploadImagesViewM: IUploadImgReqVModeling {
    
    func getITenderReqDataSource() -> [CellInfo] {
        var array = [CellInfo]()
        
        //ChooseDateTimeLabel Cell
        var chooseLabelInfo = [String: AnyObject]()
        chooseLabelInfo[Constants.UIKeys.placeholderText] = "Upload Car Pictures".localizedString() as AnyObject
        chooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Tender".localizedString(), value: "", info: chooseLabelInfo, height: Constants.CellHeightConstants.height_30)
        array.append(chooseLabelCell)
        
        //Vehicle Images Cell
        
        let vehicleImagesCellInfo = [String: AnyObject]()
        
        let vehicleImagesCell = CellInfo(cellType: .UploadVehicleImagesCell, placeHolder: "", value: StringConstants.Text.selectCarModel.localizedString(), info: vehicleImagesCellInfo, height: CGFloat(400) /*Constants.CellHeightConstants.height_400*/)
        array.append(vehicleImagesCell)
        
        return array
    }
    
    private func requestUploadCarImages(arrData: [CellInfo], completion: @escaping(([String]) -> Void)) {
        var imagesToUpload: [UIImage] = []
     //   var documentImages: [String: String] = [:]
        Loader.showLoader()
        if let imagesInfo = arrData[1].info {
            if let firstImage = imagesInfo[Constants.UIKeys.firstImg] as? UIImage {
                imagesToUpload.append(firstImage)
            }
            if let secondImage = imagesInfo[Constants.UIKeys.secondImg] as? UIImage {
                imagesToUpload.append(secondImage)
            }
            if let thirdImage = imagesInfo[Constants.UIKeys.thirdImg] as? UIImage {
                imagesToUpload.append(thirdImage)
            }
            if let fourthImage = imagesInfo[Constants.UIKeys.fourthImg] as? UIImage {
                imagesToUpload.append(fourthImage)
            }
        }
        
//        if let firstImage = documentData[Constants.UIKeys.docCertificateImage] as? UIImage {
//            S3Manager.sharedInstance.uploadImage(image: firstImage, imageQuality: .medium, progress: nil) { (imageUrl, imageName, error) in
//                //documentData[Constants.UIKeys.docFrontImage] = imageName as AnyObject
//                documentImages[Constants.UIKeys.docCertificateImage] = Helper.toString(object: imageName)
//
//            }
//        }
//
//        if let firstImage = documentData[Constants.UIKeys.docBackImage] as? UIImage {
//            S3Manager.sharedInstance.uploadImage(image: firstImage, imageQuality: .medium, progress: nil) { (imageUrl, imageName, error) in
//                //documentData[Constants.UIKeys.docFrontImage] = imageName as AnyObject
//                documentImages[Constants.UIKeys.docBackImage] = Helper.toString(object: imageName)
//
//            }
//        }
        
        if imagesToUpload.count > 0 {
            S3Manager.sharedInstance.uploadImages(images: imagesToUpload, imageQuality: .medium, progress: nil) { (imageUrls, imageNames, error) in
                if let uploadedImages = imageUrls as? [String] {
                    completion(uploadedImages)
                }
            }
        } else {
            completion([])
        }
        
        
//        if let firstImage = documentData[Constants.UIKeys.docFrontImage] as? UIImage {
//            S3Manager.sharedInstance.uploadImage(image: firstImage, imageQuality: .medium, progress: nil) { (imageUrl, imageName, error) in
//                //documentData[Constants.UIKeys.docFrontImage] = imageName as AnyObject
//                documentImages[Constants.UIKeys.docFrontImage] = Helper.toString(object: imageName)
//                if imagesToUpload.count > 0 {
//                    S3Manager.sharedInstance.uploadImages(images: imagesToUpload, imageQuality: .medium, progress: nil) { (imageUrls, imageNames, error) in
//                        if let uploadedImages = imageUrls as? [String] {
//                            completion(uploadedImages, documentImages)
//                        }
//                    }
//                } else {
//                    completion([], [:])
//                }
//
//            }
//        }
    }
    
    
    func requestTenderAPI(arrData: [CellInfo], documentData: [String: AnyObject], firstScreenAPIParams: APIParams,  completion: @escaping (Bool)-> Void) {
        if self.validateFormParameters(arrData: arrData, documentData: documentData) {
            self.requestUploadCarImages(arrData: arrData) { (uploadedImages)  in
                let tenderRequestParams = self.getAPIParams(imagesURL: uploadedImages,firstScreenAPIParams: firstScreenAPIParams, documentData: documentData)
                APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderRequestForm(param: tenderRequestParams))) { (response, success) in
                    if success {
                        if let safeResponse =  response as? [String: AnyObject]
                        {
                            completion(success)
                        }
                    }
                }
            }
            
        }
    }
    
    func validateFormParameters(arrData: [CellInfo], documentData: [String: AnyObject]) -> Bool {
        var isValid = true
//        let frontImage = documentData[Constants.UIKeys.docFrontImage] as? UIImage
//        let backImage = documentData[Constants.UIKeys.docBackImage] as? UIImage
//        let certificateImg = documentData[Constants.UIKeys.docCertificateImage] as? UIImage
        
//        if certificateImg == nil  {
//            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for registered Card or Ownership Certificate".localizedString())
//            isValid = false
//        } else if frontImage == nil {
//            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image front of driving license".localizedString())
//            isValid = false
//        } else if backImage == nil {
//            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for back of driving license".localizedString())
//            isValid = false
//        } else
            
        if let firstImg = arrData[1].info, firstImg[Constants.UIKeys.firstImg] == nil {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select first car image.".localizedString())
            isValid = false
        } else if let secondImg = arrData[1].info, secondImg[Constants.UIKeys.secondImg] == nil {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select second car image.".localizedString())
            isValid = false
        } else if let thirdImg = arrData[1].info, thirdImg[Constants.UIKeys.thirdImg] == nil {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select third car image.".localizedString())
            isValid = false
        } else if let fourthImg = arrData[1].info, fourthImg[Constants.UIKeys.fourthImg] == nil {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select fourth car image.".localizedString())
            isValid = false
        }
        return isValid
    }
    
    func getAPIParams(imagesURL: [String],firstScreenAPIParams: APIParams, documentData: [String: AnyObject]) -> APIParams {
        var params: APIParams = APIParams()
        params = firstScreenAPIParams
        params[ConstantAPIKeys.images] = imagesURL as AnyObject

        var documents = [[String: AnyObject]]()
        
        if let imageUrl = documentData[Constants.UIKeys.docFrontImage] as? String, !imageUrl.isEmpty {
            documents.append([ConstantAPIKeys.name: documentData[Constants.UIKeys.docFrontImage] as AnyObject, ConstantAPIKeys.type: "3" as AnyObject])
        }
        
        if let imageUrl = documentData[Constants.UIKeys.docBackImage] as? String, !imageUrl.isEmpty {
            documents.append([ConstantAPIKeys.name: documentData[Constants.UIKeys.docBackImage] as AnyObject, ConstantAPIKeys.type: "4" as AnyObject])
        }
        
        if let imageUrl = documentData[Constants.UIKeys.docCertificateImage] as? String, !imageUrl.isEmpty {
            documents.append([ConstantAPIKeys.name: documentData[Constants.UIKeys.docCertificateImage] as AnyObject, ConstantAPIKeys.type: "5" as AnyObject])
        }
        
        params[ConstantAPIKeys.documents] = documents as AnyObject
        
        return params
    }
    
    
}
