//
//  USSecondDetailViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/6/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
import Foundation

protocol USSecondDetailViewModeling: BaseVModeling {
    func getUpholsteryDetailDataSource(screenDetails: RDBidListModel) -> [CellInfo]
}

class USSecondDetailViewM: BaseViewM, USSecondDetailViewModeling {
    
    func getUpholsteryDetailDataSource(screenDetails: RDBidListModel) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //CarDetailCell
        var carDetailInfo = [String: AnyObject]()
        carDetailInfo[Constants.UIKeys.cellInfo] = screenDetails
        carDetailInfo[Constants.UIKeys.isSecondDetailPage] = true as AnyObject
        let carDetailCell = CellInfo(cellType: .STCarDetailCell, placeHolder: "", value: "", info: carDetailInfo, height: Constants.CellHeightConstants.height_240)
        array.append(carDetailCell)
        
        //Three Images Cell
        if let totalImage = screenDetails.image, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info:imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }

        
        //BidsCountCell
        var bidsCountCellInfo = [String: AnyObject]()
        bidsCountCellInfo[Constants.UIKeys.isSecondDetailPage] = true as AnyObject
        let bidsCountCell = CellInfo(cellType: .STBidsCountCell, placeHolder: "", value: "", info: bidsCountCellInfo, height: Constants.CellHeightConstants.height_55)
        array.append(bidsCountCell)
        
        //Bid Detail cell
        var bidDetailCellInfo = [String: AnyObject]()
        if let bids = screenDetails.Bids {
            bidDetailCellInfo[Constants.UIKeys.serviceType] = "Upholstery".localizedString() as AnyObject
            bidDetailCellInfo[Constants.UIKeys.description] = Helper.toString(object: bids.jobDescription) as AnyObject
            bidDetailCellInfo[Constants.UIKeys.additionalInformation] = Helper.toString(object: bids.additionalInfo) as AnyObject
            bidDetailCellInfo[Constants.UIKeys.servicePrice] = Helper.toString(object: bids.amount) as AnyObject
            bidDetailCellInfo[Constants.UIKeys.paymentType] = Helper.toString(object: bids.paymentType) as AnyObject
            bidDetailCellInfo[Constants.UIKeys.transaction] =  Helper.toString(object: bids.transactionNo) as AnyObject
        }
        
        let bidDetailCell = CellInfo(cellType: .BidDetailCell, placeHolder: "", value: "", info: bidDetailCellInfo, height:UITableView.automaticDimension)
        array.append(bidDetailCell)
        
        //service provider detail
        var firstBidCellInfo = [String: AnyObject]()
        firstBidCellInfo[Constants.UIKeys.agencyName] = Helper.toString(object: screenDetails.driverName) as AnyObject
        firstBidCellInfo[Constants.UIKeys.rating] = Helper.toInt(screenDetails.rating?.rounded()) as AnyObject
        firstBidCellInfo[Constants.UIKeys.address] = Helper.toString(object: screenDetails.driverAddress) as AnyObject
         firstBidCellInfo[Constants.UIKeys.driverImage] = Helper.toString(object: screenDetails.driverImage) as AnyObject
        
        let firstBidCell = CellInfo(cellType: .SPDetailCell, placeHolder: "", value: "", info: firstBidCellInfo, height: Constants.CellHeightConstants.height_110)
        array.append(firstBidCell)
        
        //Contact Info Cell
       
        let secondBidCell = CellInfo(cellType: .VIContactInfoCell, placeHolder: "+971 \(Helper.toString(object: screenDetails.driverMobile))", value:Helper.toString(object: screenDetails.driverEmail), info: nil, height: Constants.CellHeightConstants.height_120)
        array.append(secondBidCell)
        
        
        return array
    }
}

