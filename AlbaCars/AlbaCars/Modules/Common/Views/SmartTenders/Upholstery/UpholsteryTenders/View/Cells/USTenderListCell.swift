//
//  USTenderListCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/24/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class USTenderListCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bidsView: UIView!
    @IBOutlet weak var bidsLabel: UILabel!
    @IBOutlet weak var bidsCountLabel: UILabel!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var timerInfo1Label: UILabel!
    @IBOutlet weak var timerInfo2Label: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descDetailLabel: UILabel!
    @IBOutlet weak var tenderStatusButton: UIButton!
    @IBOutlet weak var completedImageView: UIImageView!
    @IBOutlet weak var timerLabelsBgView: UIView!
    @IBOutlet weak var dataView: UIView!


    //MARK: - Life Cycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.timerInfo1Label.text = ""
        self.timerInfo2Label.text = ""
        Threads.performTaskInMainQueue {
            self.bidsView.roundCorners([.bottomLeft, .bottomRight], radius: Constants.UIConstants.sizeRadius_14)
            self.bgView.roundCorners(Constants.UIConstants.sizeRadius_7)
            self.bgView.addShadow(ofColor: UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
            self.dataView.roundCorners(Constants.UIConstants.sizeRadius_7)
        }
    }
    
    //MARK: - Public Methods
    func configureView(tenderModel: AllTendersModel) {
        //setting description labels
        self.descriptionLabel.text = StringConstants.Text.description.localizedString()
        self.descDetailLabel.text = Helper.toString(object: tenderModel.description)
        self.bidsCountLabel.text = Helper.toString(object: tenderModel.bidsCount)
        self.timerInfo2Label.isHidden = false
        self.carModelLabel.text = Helper.toString(object: tenderModel.carTitle())

        switch tenderModel.tenderStatus() {
        case .inProgress:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
            self.timerInfo1Label.isHidden = false
            if let timerDuration = tenderModel.endTimerDuration {
                self.timerInfo1Label.text = timerDuration.convertToTimerFormat()
            }
            self.timerInfo1Label.textColor = .red
            self.timerInfo2Label.isHidden = true
            self.timerInfo2Label.text = ""
            self.tenderStatusButton.setTitle(StringConstants.Text.tenderInProgress.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .greenColor
            
        case .bidAccepted:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
//            self.timerInfo1Label.isHidden = true
//            self.timerInfo2Label.isHidden = true
            self.timerLabelsBgView.isHidden = true
            self.tenderStatusButton.setTitle(StringConstants.Text.tenderBidAccepted.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .yellowColor
            
        case .noBidAccepted:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
            self.timerLabelsBgView.isHidden = false
            self.timerInfo1Label.isHidden = false
            self.timerInfo1Label.text = "00:00:00 \(StringConstants.Text.tenderComplete.localizedString())"
            self.timerInfo1Label.textColor = .grayTextColor
            if let timerDuration = tenderModel.endTimerDuration {
                self.timerInfo2Label.attributedText =  String.getAttributedText(firstText: timerDuration.convertToTimerFormat(), secondText: " \(StringConstants.Text.toAcceptBid.localizedString())", firstTextColor: .redButtonColor, secondTextColor: .greenTopHeaderColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
            }
            self.tenderStatusButton.setTitle(StringConstants.Text.tenderNoBidsAccepted.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .blackColor
            
        case .allBidsRejected:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
            self.timerLabelsBgView.isHidden = false
            self.timerInfo1Label.isHidden = false
            self.timerInfo1Label.text = "00:00:00 \(StringConstants.Text.tenderComplete.localizedString())"
            self.timerInfo1Label.textColor = .grayTextColor
            self.timerInfo2Label.text = "00:00:00 \(StringConstants.Text.timeElapsed.localizedString())"
            self.timerInfo1Label.textColor = .grayTextColor
            self.tenderStatusButton.setTitle(StringConstants.Text.tenderAllBidsRejected.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .redButtonColor
            
        case .upcomingTenders:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
            self.timerLabelsBgView.isHidden = false

            if let timerDuration = tenderModel.startTimerDuration {
                self.timerInfo1Label.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.tenderStartsIn.localizedString())", secondText: " \(timerDuration.convertToTimerFormat())", firstTextColor: .grayTextColor, secondTextColor: .greenTopHeaderColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Book)
            }
            self.timerInfo2Label.attributedText =  String.getAttributedText(firstText:"\(StringConstants.Text.tenderDateTime.localizedString())", secondText: " \(Helper.toString(object:tenderModel.getDateTime()))", firstTextColor: .black, secondTextColor: .greenTopHeaderColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Book)
            self.tenderStatusButton.setTitle(StringConstants.Text.tenderUpcoming.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .blueTenderButtonColor
            
        case .completedTenders:
            self.timerLabelsBgView.isHidden = true
            self.tenderStatusButton.isHidden = true
            self.completedImageView.isHidden = false
            
        default:
            break
        }
    }
}
