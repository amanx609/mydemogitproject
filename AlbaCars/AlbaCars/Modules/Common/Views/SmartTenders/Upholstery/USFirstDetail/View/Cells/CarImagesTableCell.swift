//
//  CarImagesTableCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class CarImagesTableCell: BaseTableViewCell, ReusableView, NibLoadableView {

    //MARK: - IBOutlets
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var viewAllButton: UIButton!
    
    //MARK: - Variables
    var collectionDataSource: [[String: AnyObject]] = []
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK: - Private Methods
    private func setup() {
        self.imagesCollectionView.delegate = self
        self.imagesCollectionView.dataSource = self
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        
    }
    
    //MARK: - IBActions
    @IBAction func tapViewAllButton(_ sender: Any) {
        
    }
    
    
}

extension CarImagesTableCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
      return -30
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           return self.collectionDataSource.count
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
           let cellInfo = self.collectionDataSource[indexPath.row]
        return self.getCell(collectionView, indexPath: indexPath)
       }
       
       func collectionView(_ collectionView: UICollectionView,
                           layout collectionViewLayout: UICollectionViewLayout,
                           sizeForItemAt indexPath: IndexPath) -> CGSize {
           
           return CGSize(width: Constants.CellHeightConstants.height_60, height: Constants.CellHeightConstants.height_60)
       }
}

extension CarImagesTableCell {
    
    func getCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
               
               guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CarImageCollectionCell.className, for: indexPath) as? CarImageCollectionCell else {
                   return UICollectionViewCell()
               }
               cell.configureForUSDetailCell(cellData: self.collectionDataSource[indexPath.row])
               return cell

       }
}
