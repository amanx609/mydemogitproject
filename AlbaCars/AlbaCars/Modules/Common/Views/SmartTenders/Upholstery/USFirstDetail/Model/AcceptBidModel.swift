//
//  AcceptBidModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/6/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class AcceptBidModel: BaseCodable {
    var bidsCount: Int?
    var brandName: String?
    var contact: String?
    var currentDate: String?
    var description: String?
    var dropLocationLongitude: String?
    var dropOffCode: String?
    var dropoffLocation: String?
    var dropoffLocationLatitude: String?
    var duration: String?
    var endDate: String?
    var id: Int?
    var name: String?
    var pickupCode: String?
    var pickupLocation: String?
    var pickupLocationLatitude: String?
    var pickupLocationLongitude: String?
    var requestedDate: String?
    var requestedTime: String?
    var startDate: String?
    var status:Int?
    var type: Int?
    var typeName: String?
    var userImage: String?
    var vehicleId: Int?
    var year: Int?
}
