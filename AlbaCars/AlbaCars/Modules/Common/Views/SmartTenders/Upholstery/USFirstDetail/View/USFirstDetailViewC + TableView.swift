//
//  USFirstDetailViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension USFirstDetailViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.upholsteryDetailDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.upholsteryDetailDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellInfo = self.upholsteryDetailDataSource[indexPath.row]
        guard let cellType = cellInfo.cellType else { return self.upholsteryDetailDataSource[indexPath.row].height }
        if (self.tenderStatus == .allBidsRejected) {
            switch cellType {
            case .STBidsCountCell:
                return Constants.CellHeightConstants.height_0
                
            default:
                break
            }
        }
        return self.upholsteryDetailDataSource[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
           let cellInfo = upholsteryDetailDataSource[indexPath.row]
           guard let cellType = cellInfo.cellType else { return }
           switch cellType {
           case .RDBidListCell:
               if let info = cellInfo.info {
                   self.requestBidTenderDetails(bidId: Helper.toInt(info[ConstantAPIKeys.id]))
               }
              break
           default:
               break
           }
       }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let lastVisibleRow = self.upholsteryDetailTableView.indexPathsForVisibleRows?.last?.row else { return }
        if (self.upholsteryDetailDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
            if let tenderDetail = self.tenderDetails {
                self.requestBidListAPI(tenderDetails: tenderDetail)
            }
        }
    }

}

extension USFirstDetailViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .STCarDetailCell:
            let cell: STCarDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .upholsteryRequest, tenderStatus: self.tenderStatus)
            cell.delegate = self
            return cell
            
        case .STBidsCountCell:
            let cell: STBidsCountCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .upholsteryRequest)
            return cell
            
        case .RDBidListCell:
            let cell: RDBidListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .upholstery)
            cell.delegate = self
            return cell
            
        case .CarImagesTableCell:
            let cell: CarImagesTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        
        case .USDescriptonCell:
            let cell: USDescriptonCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .ThreeImageCell:
            let cell: ThreeImageCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            cell.delegate = self
            return cell
                
        default:
            return UITableViewCell()
        }
    }
}

extension USFirstDetailViewC: RDBidListCellDelegate {
    func didTapCheck(cell: RDBidListCell) {
        guard let indexPath = self.upholsteryDetailTableView.indexPath(for: cell) else { return }
        let bidId = Helper.toInt(self.upholsteryDetailDataSource[indexPath.row].info?[Constants.UIKeys.id])
        let amount = Helper.toDouble(self.upholsteryDetailDataSource[indexPath.row].info?[Constants.UIKeys.bidAmount])
        self.requestAcceptBid(bidId: bidId, winAmount: amount)
        
    }
    
    func didTapCross(cell: RDBidListCell) {
        guard let indexPath = self.upholsteryDetailTableView.indexPath(for: cell) else { return }
        let bidId = Helper.toInt(self.upholsteryDetailDataSource[indexPath.row].info?[Constants.UIKeys.id])
        self.requestRejectBid(bidId: bidId)
    }
    
    
}

extension USFirstDetailViewC: STCarDetailCellDelegate {
    func didTapCancelButton(cell: STCarDetailCell) {
        self.requestCancelTender()
    }
    
    
}

extension USFirstDetailViewC: ThreeImageCellDelegate {
    func didTapViewAllCell(cell: ThreeImageCell) {
        let imageViewerVC = DIConfigurator.sharedInstance.getImageViewerVC()
        if let imgArray = self.tenderDetails?.image, imgArray.count > 0 {
            imageViewerVC.imageDataSource = imgArray
        }
        self.navigationController?.pushViewController(imageViewerVC, animated: true)
        
    }
}
