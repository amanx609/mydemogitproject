//
//  USDescriptonCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class USDescriptonCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: Private methods
    
    @IBOutlet weak var descTitleLabel: UILabel!
    @IBOutlet weak var descDetailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //Public Methods
    func configureView(cellInfo: CellInfo) {
        self.descTitleLabel.text = cellInfo.placeHolder
        self.descDetailLabel.text = cellInfo.value
    }
    
}
