//
//  USFirstDetailViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol USFirstDetailViewModeling: BaseVModeling {
    func getUpholsteryDetailDataSource(tenderDetails: RDBidListModel, bidList: [BidListModel]) -> [CellInfo]
}

class USFirstDetailViewM: BaseViewM, USFirstDetailViewModeling {
    
    func getUpholsteryDetailDataSource(tenderDetails: RDBidListModel, bidList: [BidListModel]) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //CarDetailCell
        var carDetailInfo = [String: AnyObject]()
        carDetailInfo[Constants.UIKeys.cellInfo] = tenderDetails
        let carDetailCell = CellInfo(cellType: .STCarDetailCell, placeHolder: "", value: "", info: carDetailInfo, height: Constants.CellHeightConstants.height_240)
        array.append(carDetailCell)
        
        //Description
        let descriptionCell = CellInfo(cellType: .USDescriptonCell, placeHolder: StringConstants.Text.jobDescription.localizedString(), value: Helper.toString(object: tenderDetails.description), info: nil, height: UITableView.automaticDimension)
        array.append(descriptionCell)
        
        //Additional Information Cell
        let additionalInfoCell = CellInfo(cellType: .USDescriptonCell, placeHolder: StringConstants.Text.additionalInfo.localizedString(), value: Helper.toString(object: tenderDetails.additionalInfo), info: nil, height: UITableView.automaticDimension)
        array.append(additionalInfoCell)
        
        //Additional Information Cell
        //var carImagesCellInfo = [String: AnyObject]()
        var carArray = [UIImage]()
        for _ in 0..<4 {
            carArray.append(#imageLiteral(resourceName: "home-insurance"))
        }
        //Three Images Cell
        let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info:nil, height: Constants.CellHeightConstants.height_80)
        array.append(threeImageCell)
        
        //BidsCountCell
        var bidsCountCellInfo = [String: AnyObject]()
        bidsCountCellInfo[Constants.UIKeys.isSecondDetailPage] = false as AnyObject
        let bidsCountCell = CellInfo(cellType: .STBidsCountCell, placeHolder: "", value: Helper.toString(object: tenderDetails.bidsCount), info: bidsCountCellInfo, height: Constants.CellHeightConstants.height_55)
        array.append(bidsCountCell)
        
        //BidCell
        for i in 0..<bidList.count {
            var bidCellInfo = [String: AnyObject]()
            bidCellInfo[Constants.UIKeys.date] = bidList[i].getJobDate() as AnyObject
            bidCellInfo[Constants.UIKeys.bidAmount] = bidList[i].amount as AnyObject
            bidCellInfo[Constants.UIKeys.time] = bidList[i].daysForService as AnyObject
            bidCellInfo[Constants.UIKeys.id] = bidList[i].bidId as AnyObject
            bidCellInfo[Constants.UIKeys.status] = Helper.toString(object: bidList[i].isPickupDropoffStr())  as AnyObject
            bidCellInfo[ConstantAPIKeys.name] = bidList[i].name as AnyObject
            bidCellInfo[Constants.UIKeys.bidComments] = bidList[i].bidderComments as AnyObject
            bidCellInfo[ConstantAPIKeys.rating] = bidList[i].rating as AnyObject
            bidCellInfo[ConstantAPIKeys.vatAmmount] = tenderDetails.getVatPercentage() as AnyObject
            bidCellInfo[ConstantAPIKeys.image] = bidList[i].image as AnyObject

            let firstBidCell = CellInfo(cellType: .RDBidListCell, placeHolder: "", value: "", info: bidCellInfo, height: UITableView.automaticDimension)
            array.append(firstBidCell)
        }
        
        return array
    }
}
