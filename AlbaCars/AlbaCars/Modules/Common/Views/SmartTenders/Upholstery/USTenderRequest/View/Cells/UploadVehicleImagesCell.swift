//
//  UploadVehicleImagesCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol UploadVehicleImagesCellDelegate: class {
    func didTapFirstImgCell(cell: UploadVehicleImagesCell)
    func didTapSecondImgCell(cell: UploadVehicleImagesCell)
    func didTapThirdImgCell(cell: UploadVehicleImagesCell)
    func didTapFourthImgCell(cell: UploadVehicleImagesCell)
}

class UploadVehicleImagesCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var firstImageView: UIImageView!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var thirdImageView: UIImageView!
    @IBOutlet weak var fourthImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bgViewLeading: NSLayoutConstraint!
    @IBOutlet weak var bgViewTrailing: NSLayoutConstraint!
    
    //MARK: - Variables
    weak var delegate: UploadVehicleImagesCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.titleLabel.isHidden = true
        
        if let info = cellInfo.info {
            //Image
            if let placeholderImage = info[Constants.UIKeys.firstImg] as? UIImage {
            
                self.firstImageView.image = placeholderImage
            }
            
            if let placeholderImage = info[Constants.UIKeys.secondImg] as? UIImage {
            
                self.secondImageView.image = placeholderImage
            }
            if let placeholderImage = info[Constants.UIKeys.thirdImg] as? UIImage {
            
                self.thirdImageView.image = placeholderImage
            }
            if let placeholderImage = info[Constants.UIKeys.fourthImg] as? UIImage {
            
                self.fourthImageView.image = placeholderImage
            }
        }
    }
    
    func configureBidDetailsView(cellInfo: CellInfo) {
        self.titleLabel.isHidden = false
        self.bgViewLeading.constant = 20
        self.bgViewTrailing.constant = 20
        
        if let info = cellInfo.info {
            //Image
            if let placeholderImage = info[Constants.UIKeys.firstImg] as? UIImage {
            
                self.firstImageView.image = placeholderImage
            }
            
            if let placeholderImage = info[Constants.UIKeys.secondImg] as? UIImage {
            
                self.secondImageView.image = placeholderImage
            }
            
            if let placeholderImage = info[Constants.UIKeys.thirdImg] as? UIImage {
            
                self.thirdImageView.image = placeholderImage
            }
            
            if let placeholderImage = info[Constants.UIKeys.fourthImg] as? UIImage {
            
                self.fourthImageView.image = placeholderImage
            }
        }
        
    }
    
    func configurePTenderRequestView(cellInfo: CellInfo) {
        self.titleLabel.isHidden = false
        self.titleLabel.text = cellInfo.placeHolder
        if let info = cellInfo.info {
            if let placeholderImage = info[Constants.UIKeys.firstImg] as? UIImage {
                self.firstImageView.image = placeholderImage
            }
            
            if let placeholderImage = info[Constants.UIKeys.secondImg] as? UIImage {
            
                self.secondImageView.image = placeholderImage
            }
            
            if let placeholderImage = info[Constants.UIKeys.thirdImg] as? UIImage {
            
                self.thirdImageView.image = placeholderImage
            }
            
            if let placeholderImage = info[Constants.UIKeys.fourthImg] as? UIImage {
            
                self.fourthImageView.image = placeholderImage
            }
        }
        
    }
    
    //MARK: - IBActions
    @IBAction func tapFirstImg(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapFirstImgCell(cell: self)
        }
    }
    
    @IBAction func tapSecondImg(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapSecondImgCell(cell: self)
        }
    }
    
    @IBAction func tapThirdImg(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapThirdImgCell(cell: self)
            
        }
    }
    
    @IBAction func tapFourthImg(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapFourthImgCell(cell: self)
        }
    }
    
}
