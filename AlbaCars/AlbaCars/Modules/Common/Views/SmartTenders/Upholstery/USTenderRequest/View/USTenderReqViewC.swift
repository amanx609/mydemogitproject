//
//  USTenderReqViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class USTenderReqViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tenderRequestTableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet var dateTimePicker: UIDatePicker!
    
    //Variables
    var viewModel: USTenderReqVModeling?
    var tenderRequestDataSource: [CellInfo] = []
    var isDate = true
    var vehicleId: Int?
    var addCarViewModel: CAddNewCarViewModeling?
    var requestCarDataSource: [CellInfo] = []
    var vehicleBrandDataSource: [[String: AnyObject]] = []
    var vehicleTypeDataSource: [[String: AnyObject]] = []
    var vehicleModelDataSource: [[String: AnyObject]] = []
    var carDetails: ChooseCarModel?
    var selectedIndexPath = IndexPath()
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: ChooseCarForServiceType.upholsteryRequest.title ?? "",barColor: .white,titleColor: .black, leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.registerNibs()
        self.setupViewModel()
        self.setupView()
        self.setupTableView()
        self.requestVehicleBrandAPI()
        self.loadDataSource()
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.submitButton.setTitle("Submit".localizedString(), for: .normal)
        
    }
    
    private func setupTableView() {
        self.tenderRequestTableView.separatorStyle = .none
        self.tenderRequestTableView.backgroundColor = UIColor.white
        self.tenderRequestTableView.delegate = self
        self.tenderRequestTableView.dataSource = self
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = USTenderReqVM()
        }
        if self.addCarViewModel == nil {
            self.addCarViewModel = CAddNewCarVM()
        }
    }
    
    private func registerNibs() {
        self.tenderRequestTableView.register(LocationTextFieldsCell.self)
        self.tenderRequestTableView.register(DropDownCell.self)
        self.tenderRequestTableView.register(DateRangeCell.self)
        self.tenderRequestTableView.register(BottomLabelCell.self)
        self.tenderRequestTableView.register(ImageDropDownCell.self)
        self.tenderRequestTableView.register(NoReserveCell.self)
        self.tenderRequestTableView.register(TextViewCell.self)
        self.tenderRequestTableView.register(UploadVehicleImagesCell.self)
        self.tenderRequestTableView.register(TextInputCell.self)
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getUSTenderReqDataSource(carDetails: self.carDetails) {
            self.tenderRequestDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.tenderRequestTableView.reloadData()
            }
        }
    }
    
    private func updateDateTime(_ pickerValue: String) {
        if isDate {
            self.tenderRequestDataSource[self.selectedIndexPath.row].placeHolder = pickerValue
        } else {
            self.tenderRequestDataSource[self.selectedIndexPath.row].value = pickerValue
        }
        
        self.tenderRequestTableView.reloadData()
    }
    
    //MARK: - Public Methods
    func setupDateTimePicker(_ textField: UITextField) {
        textField.inputView = self.dateTimePicker
        textField.inputAccessoryView = Helper.getDoubleButtonToolBar(leftButtonTitle: StringConstants.Text.Cancel.localizedString(), rightButtonTitle: StringConstants.Text.done.localizedString(), leftButtonselector: #selector(self.cancelButtonTapped), rightButtonSelector: #selector(self.doneButtonTapped), target: self)
        if isDate {
            self.dateTimePicker.datePickerMode = .date
        } else {
            self.dateTimePicker.datePickerMode = .time
        }
        self.dateTimePicker.minimumDate = Date()
    }
    
    func setupToolBar(_ textField: UITextField) {
        textField.inputAccessoryView = Helper.getSingleButtonToolBar(buttonTitle: StringConstants.Text.done.localizedString(), selector: #selector(self.doneButtonTapped), target: self)
    }
    
    func showDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        switch indexPath.row {
        case 1:
            if self.vehicleBrandDataSource.count == 0 {
                self.requestVehicleBrandAPI()
                return
            }
            listPopupView.initializeViewWith(title: "Select Brand".localizedString(), arrayList: self.vehicleBrandDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let brandId = response[ConstantAPIKeys.id] as? Int,
                    let brandName = response[ConstantAPIKeys.title] as? String else { return }
                sSelf.requestVehicleModelAPI(brandId: brandId)
                sSelf.tenderRequestDataSource[indexPath.row].value = brandName
                sSelf.tenderRequestDataSource[indexPath.row].placeHolder = "\(brandId)"
                sSelf.tenderRequestTableView.reloadRows(at: [indexPath], with: .none)
            }
        case 2:
            if self.vehicleModelDataSource.count == 0 {
                
                return
            }
            listPopupView.initializeViewWith(title: "Model Name".localizedString(), arrayList: self.vehicleModelDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let modelName = response[ConstantAPIKeys.title] as? String,
                    let modelId = response[ConstantAPIKeys.id] as? Int else { return }
                sSelf.requestVehicleTypeAPI()
                sSelf.tenderRequestDataSource[indexPath.row].value = modelName
                sSelf.tenderRequestDataSource[indexPath.row].placeHolder = "\(modelId)"
                sSelf.tenderRequestTableView.reloadRows(at: [indexPath], with: .none)
            }
            
        case 3:
            if self.vehicleTypeDataSource.count == 0 {
                return
            }
            listPopupView.initializeViewWith(title: "Select Car Type".localizedString(), arrayList: self.vehicleTypeDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
                guard let sSelf = self,
                    let carType = response[ConstantAPIKeys.title] as? String,
                    let carId = response[ConstantAPIKeys.id] as? Int else { return }
                sSelf.tenderRequestDataSource[indexPath.row].value = carType
                sSelf.tenderRequestDataSource[indexPath.row].placeHolder = "\(carId)"
                sSelf.tenderRequestTableView.reloadRows(at: [indexPath], with: .none)
            }
            
        case 10:
            listPopupView.initializeViewWith(title: "Select Time Duration", arrayList: Constants.tenderDuration, key: Constants.UIKeys.duration) { [weak self] (response) in
                guard let sSelf = self,
                    let timeDuration = response[ConstantAPIKeys.duration] as? String, let apiValue = response[Constants.UIKeys.value] as? String else { return }
                sSelf.tenderRequestDataSource[indexPath.row].value = timeDuration
                sSelf.tenderRequestDataSource[indexPath.row].placeHolder = apiValue
                sSelf.tenderRequestTableView.reloadRows(at: [indexPath], with: .none)
            }
            
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    //MARK: - API Methods
    func requestVehicleBrandAPI() {
        self.addCarViewModel?.requestVehicleBrandAPI(completion: { (success, vehicleBrands) in
            if success {
                self.vehicleBrandDataSource = vehicleBrands
            }
        })
    }
    
    func requestVehicleModelAPI(brandId: Int) {
        self.addCarViewModel?.requestVehicleModelAPI(brandId: brandId, completion: { (success, vehicleModels) in
            if success {
                self.vehicleModelDataSource = vehicleModels
            }
        })
    }
    
    func requestVehicleTypeAPI() {
        self.addCarViewModel?.requestVehicleTypeAPI(completion: { (success, vehicleTypes) in
            if success {
                self.vehicleTypeDataSource = vehicleTypes
            }
        })
    }
    
    //MARK: - Selectors
    @objc func cancelButtonTapped() {
        self.view.endEditing(true)
    }
    
    @objc func doneButtonTapped() {
        self.view.endEditing(true)
        let selectedDate = self.dateTimePicker.date
        var convertedValue = ""
        if isDate {
            convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatWithoutSpace)
            // self.auctionConfirmationDataSource[0].info?[Constants.UIKeys.firstValue] = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatdMMMyyyy) as AnyObject
        } else {
            convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.timeFormathmma)
          //  self.tenderRequestDataSource[self.selectedIndexPath.row].info?[Constants.UIKeys.secondValue] = selectedDate.dateStringWith(strFormat: Constants.Format.timeFormathhmmss) as AnyObject
        }
        updateDateTime(convertedValue)
    }
    
    func uploadVehicleImages() {
        var images = [UIImage]()
        if let firstImg = self.tenderRequestDataSource[13].info?[Constants.UIKeys.firstImg] as? UIImage {
            images.append(firstImg)
        }
        if  let secondImg = self.tenderRequestDataSource[13].info?[Constants.UIKeys.secondImg]  as? UIImage {
            images.append(secondImg)
        }
        if let thirdImg = self.tenderRequestDataSource[13].info?[Constants.UIKeys.thirdImg] as? UIImage {
            images.append(thirdImg)
        }
        if let fourthImg = self.tenderRequestDataSource[13].info?[Constants.UIKeys.fourthImg] as? UIImage {
            images.append(fourthImg)
            
        }
        if images.count > 0 {
            S3Manager.sharedInstance.uploadImages(images: images, imageQuality: .medium, progress: { (percentage, success) in
                print(percentage)
                print(success)
            }) { (imageNames, images, error) in
                self.tenderRequestDataSource[13].info?[Constants.UIKeys.images] = imageNames as AnyObject
                self.requestTenderAPI()
            }
        } else {
            self.requestTenderAPI()
        }
        
    }
    
    func requestTenderAPI() {
        self.viewModel?.requestTenderAPI(arrData: self.tenderRequestDataSource, vehicleId: vehicleId ?? 0, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            if let viewControllers = sSelf.navigationController?.viewControllers {
                //CreateAccountViewCIsPresentInNavigationStack
                for viewController in viewControllers {
                    if viewController is RecoveryHomeViewC {
                        sSelf.navigationController?.popToViewController(viewController, animated: true)
                        return
                    }
                }
                
            }
        })
    }
    
    //MARK: - IBActions
    @IBAction func tapSubmitButton(_ sender: Any) {
        uploadVehicleImages()
    }
}
