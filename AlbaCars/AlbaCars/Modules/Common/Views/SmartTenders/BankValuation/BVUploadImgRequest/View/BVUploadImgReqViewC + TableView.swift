//
//  BVUploadImgReqViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/20/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension BVUploadImgReqViewC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.uploadDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        getCell(tableView, indexPath: indexPath, cellInfo: self.uploadDataSource[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let cellHeight = self.uploadDataSource[indexPath.row].height else { return 0.0}
        return cellHeight
    }
    
}

extension BVUploadImgReqViewC {
    func getCell(_ tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        let cell: BVCarImagesCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.delegate = self
        switch indexPath.row {
        case 0:
            cell.configureView(cellInfo: cellInfo, firstimageType: .carFront, secondImageType: .carRear)
            break
        case 1:
            cell.configureView(cellInfo: cellInfo, firstimageType: .carLeftSide, secondImageType: .carRightSide)
            break
        case 2:
            cell.configureView(cellInfo: cellInfo, firstimageType: .carMileage, secondImageType: .gccSticker)
            break
        default:
            break
        }
        
        return cell
    }
}

extension BVUploadImgReqViewC: BVCarImagesCellDelegate {
    func didTapFirstImageButton(cell: BVCarImagesCell) {
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    guard let indexPath = self.uploadCarImagesTableView.indexPath(for: cell) else { return }
                    switch indexPath.row {
                    case 0:
                        self.uploadDataSource[indexPath.row].info?[Constants.UIKeys.carFront] = image as AnyObject
                        break
                        
                    case 1:
                        self.uploadDataSource[indexPath.row].info?[Constants.UIKeys.carLeftSide] = image as AnyObject
                        break
                        
                    case 2:
                        self.uploadDataSource[indexPath.row].info?[Constants.UIKeys.carMileage] = image as AnyObject
                        break
                        
                    default:
                        break
                    }
                    
                    self.uploadCarImagesTableView.reloadData()
                }
            }
        }
        
    }
    
    func didTapSecondImageButton(cell: BVCarImagesCell) {
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    guard let indexPath = self.uploadCarImagesTableView.indexPath(for: cell) else { return }
                    switch indexPath.row {
                    case 0:
                        self.uploadDataSource[indexPath.row].info?[Constants.UIKeys.carRear] = image as AnyObject
                        break
                        
                    case 1:
                        self.uploadDataSource[indexPath.row].info?[Constants.UIKeys.carRightSide] = image as AnyObject
                        break
                        
                    case 2:
                        self.uploadDataSource[indexPath.row].info?[Constants.UIKeys.gccSticker] = image as AnyObject
                        break
                        
                    default:
                        break
                    }
                    self.uploadCarImagesTableView.reloadData()
                }
            }
        }
    }
}
