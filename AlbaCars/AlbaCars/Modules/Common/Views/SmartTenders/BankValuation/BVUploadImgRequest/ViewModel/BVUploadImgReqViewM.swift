//
//  BVUploadImgReqViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/20/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol BVUploadImgReqVModeling: BaseVModeling {
    func getBVTenderReqDataSource(firstScreenAPIParams: APIParams
    ) -> [CellInfo]
    func requestTenderAPI(arrData: [CellInfo], documentData: [String: AnyObject], firstScreenAPIParams: APIParams,  completion: @escaping (Bool)-> Void)
    
}

class BVUploadImgReqViewM: BVUploadImgReqVModeling {
    func getBVTenderReqDataSource(firstScreenAPIParams: [String : AnyObject]) -> [CellInfo] {
        var array = [CellInfo]()
        
        //Vehicle Images Cell
        var vehicleImagesCellInfo = [String: AnyObject]()
        vehicleImagesCellInfo[Constants.UIKeys.firstImg] = CarParts.carFront as AnyObject
        vehicleImagesCellInfo[Constants.UIKeys.secondImg] = CarParts.carRear as AnyObject
        let vehicleImagesCell = CellInfo(cellType: .BVCarImagesCell, placeHolder: "", value: "", info: vehicleImagesCellInfo, height: Constants.CellHeightConstants.height_198)
        array.append(vehicleImagesCell)
        
        //Vehicle Images Cell
        var vehicleImagesCell2Info = [String: AnyObject]()
        vehicleImagesCell2Info[Constants.UIKeys.firstImg] = CarParts.carLeftSide as AnyObject
        vehicleImagesCell2Info[Constants.UIKeys.secondImg] = CarParts.carRightSide as AnyObject
        let vehicleImagesCell2 = CellInfo(cellType: .BVCarImagesCell, placeHolder: "", value: StringConstants.Text.selectCarModel.localizedString(), info: vehicleImagesCell2Info, height: Constants.CellHeightConstants.height_198)
        array.append(vehicleImagesCell2)
        
        //Vehicle Images Cell
        var vehicleImagesCell3Info = [String: AnyObject]()
        vehicleImagesCell3Info[Constants.UIKeys.firstImg] = CarParts.carMileage as AnyObject
        vehicleImagesCell3Info[Constants.UIKeys.secondImg] = CarParts.gccSticker as AnyObject
        let vehicleImagesCell3 = CellInfo(cellType: .BVCarImagesCell, placeHolder: "", value: StringConstants.Text.selectCarModel.localizedString(), info: vehicleImagesCell3Info, height: Constants.CellHeightConstants.height_198)
        array.append(vehicleImagesCell3)
        return array
    }
    
    private func requestUploadCarImages(arrData: [CellInfo], documentData: [String: AnyObject], completion: @escaping(([String],[String: String]) -> Void)) {
        var imagesToUpload: [UIImage] = []
        var documentImages: [String: String] = [:]
        
        if let firstImage = documentData[Constants.UIKeys.docBackImage] as? UIImage {
            S3Manager.sharedInstance.uploadImage(image: firstImage, imageQuality: .medium, progress: nil) { (imageUrl, imageName, error) in
                //documentData[Constants.UIKeys.docFrontImage] = imageName as AnyObject
                documentImages[Constants.UIKeys.docBackImage] = Helper.toString(object: imageName)
                
            }
        }
        
        if let firstImage = documentData[Constants.UIKeys.docFrontImage] as? UIImage {
            S3Manager.sharedInstance.uploadImage(image: firstImage, imageQuality: .medium, progress: nil) { (imageUrl, imageName, error) in
                //documentData[Constants.UIKeys.docFrontImage] = imageName as AnyObject
                documentImages[Constants.UIKeys.docFrontImage] = Helper.toString(object: imageName)
                
            }
        }
        
        if documentImages[Constants.UIKeys.docFrontImage] == nil {
            if let firstImage = documentData[Constants.UIKeys.docCertificateImage] as? UIImage {
                S3Manager.sharedInstance.uploadImage(image: firstImage, imageQuality: .medium, progress: nil) { (imageUrl, imageName, error) in
                        documentImages[Constants.UIKeys.docCertificateImage] = Helper.toString(object: imageName)
                }
            }
        }
        
        
        if let imagesInfo = arrData[0].info {
            if let firstImage = imagesInfo[Constants.UIKeys.carFront] as? UIImage {
                imagesToUpload.append(firstImage)
            }
            if let secondImage = imagesInfo[Constants.UIKeys.carRear] as? UIImage {
                imagesToUpload.append(secondImage)
            }
        }
        
        if let imagesInfo = arrData[1].info {
            if let firstImage = imagesInfo[Constants.UIKeys.carLeftSide] as? UIImage {
                imagesToUpload.append(firstImage)
            }
            if let secondImage = imagesInfo[Constants.UIKeys.carRightSide] as? UIImage {
                imagesToUpload.append(secondImage)
            }
        }
        
        if let imagesInfo = arrData[2].info {
            if let firstImage = imagesInfo[Constants.UIKeys.carMileage] as? UIImage {
                imagesToUpload.append(firstImage)
            }
            if let secondImage = imagesInfo[Constants.UIKeys.gccSticker] as? UIImage {
                imagesToUpload.append(secondImage)
            }
        }
                
        
        if imagesToUpload.count > 0 {
            S3Manager.sharedInstance.uploadImages(images: imagesToUpload, imageQuality: .medium, progress: nil) { (imageUrls, imageNames, error) in
                if let uploadedImages = imageUrls as? [String] {
                    completion(uploadedImages, documentImages)
                }
            }
        } else {
            completion([], [:])
        }
    }
    
    func requestTenderAPI(arrData: [CellInfo], documentData: [String: AnyObject], firstScreenAPIParams: APIParams,  completion: @escaping (Bool)-> Void) {
        if self.validateFormParameters(arrData: arrData, documentData: documentData) {
            self.requestUploadCarImages(arrData: arrData, documentData: documentData) { (uploadedImages, documentImagesDict)  in
                let tenderRequestParams = self.getAPIParams(imagesURL: uploadedImages,firstScreenAPIParams: firstScreenAPIParams, documentData: documentImagesDict)
                APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderRequestForm(param: tenderRequestParams))) { (response, success) in
                    if success {
                        if let safeResponse =  response as? [String: AnyObject]
                        {
                            completion(success)
                        }
                    }
                }
            }
            
        }
    }
    
    func validateFormParameters(arrData: [CellInfo], documentData: [String: AnyObject]) -> Bool {
        var isValid = true
        let frontImage = documentData[Constants.UIKeys.docFrontImage] as? UIImage
        let backImage = documentData[Constants.UIKeys.docBackImage] as? UIImage
        let certificateImg = documentData[Constants.UIKeys.docCertificateImage] as? UIImage
        if let carFront = arrData[0].info, carFront[Constants.UIKeys.carFront] == nil {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for car front.".localizedString())
            isValid = false
        } else if let carRear = arrData[0].info, carRear[Constants.UIKeys.carRear] == nil {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for Car rear.".localizedString())
            isValid = false
        } else if let carLeftSide = arrData[1].info, carLeftSide[Constants.UIKeys.carLeftSide] == nil {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for Car Left Side.".localizedString())
            isValid = false
        } else if let carRightSide = arrData[1].info, carRightSide[Constants.UIKeys.carRightSide] == nil {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for Car Right Side.".localizedString())
            isValid = false
        } else if let carMileage = arrData[2].info, carMileage[Constants.UIKeys.carMileage] == nil {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for Car Mileage (Odometer).".localizedString())
            isValid = false
        } else if let gccSticker = arrData[2].info, gccSticker[Constants.UIKeys.gccSticker] == nil {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for GCC Sticker (If GCC)".localizedString())
            isValid = false
        } else if let gccSticker = arrData[2].info, gccSticker[Constants.UIKeys.gccSticker] == nil {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for GCC Sticker (If GCC)".localizedString())
            isValid = false
        } else if frontImage == nil, backImage == nil, certificateImg == nil  {            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for registered Card or Ownership Certificate".localizedString())
            isValid = false
        } else if frontImage != nil, backImage == nil,certificateImg == nil {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image back of registered card".localizedString())
            isValid = false
        } else if frontImage == nil, backImage != nil, certificateImg == nil {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select image for front of registered card".localizedString())
            isValid = false
        }
        return isValid
    }
    
    func getAPIParams(imagesURL: [String],firstScreenAPIParams: APIParams, documentData: [String: String]) -> APIParams {
        var params: APIParams = APIParams()
        params = firstScreenAPIParams
        params[ConstantAPIKeys.images] = imagesURL as AnyObject
        
        if documentData[Constants.UIKeys.docFrontImage] == nil, documentData[Constants.UIKeys.docBackImage] == nil {
            if let imageUrl = documentData[Constants.UIKeys.docCertificateImage], !imageUrl.isEmpty {
                params[ConstantAPIKeys.documents] = [[ConstantAPIKeys.name: documentData[Constants.UIKeys.docCertificateImage] as AnyObject, ConstantAPIKeys.type: "2" as AnyObject]] as AnyObject
            }
            
        } else {
            
           // params[ConstantAPIKeys.documents] = [[ConstantAPIKeys.name: Helper.toString(object: documentData[Constants.UIKeys.docFrontImage]) as AnyObject, ConstantAPIKeys.type: "1" as AnyObject], [ConstantAPIKeys.name: Helper.toString(object: documentData[Constants.UIKeys.docBackImage]) as AnyObject, ConstantAPIKeys.type: "1" as AnyObject]] as AnyObject
            
            var documents = [[String: AnyObject]]()
            
            if let imageUrl = documentData[Constants.UIKeys.docFrontImage], !imageUrl.isEmpty {
                documents.append([ConstantAPIKeys.name: Helper.toString(object: documentData[Constants.UIKeys.docFrontImage]) as AnyObject, ConstantAPIKeys.type: "1" as AnyObject])
            }
            
            if let imageUrl = documentData[Constants.UIKeys.docBackImage], !imageUrl.isEmpty {
                documents.append([ConstantAPIKeys.name: Helper.toString(object: documentData[Constants.UIKeys.docBackImage]) as AnyObject, ConstantAPIKeys.type: "1" as AnyObject])
            }
            
            params[ConstantAPIKeys.documents] = documents as AnyObject
            
        }
        
        return params
    }
    
    
}
