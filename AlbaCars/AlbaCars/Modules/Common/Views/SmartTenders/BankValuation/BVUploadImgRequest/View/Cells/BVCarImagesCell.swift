//
//  BVCarImagesCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol BVCarImagesCellDelegate: class {
    func didTapFirstImageButton(cell: BVCarImagesCell)
    func didTapSecondImageButton(cell: BVCarImagesCell)
}

class BVCarImagesCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var firstImageView: UIImageView!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var firstImageButton: UIButton!
    @IBOutlet weak var secondImageButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var firstImageLabel: UILabel!
    @IBOutlet weak var secondImageLabel: UILabel!
    
    //MARK: - Variables
    weak var delegate: BVCarImagesCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.infoButton.isHidden = true
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo, firstimageType: CarParts, secondImageType: CarParts) {
        if let info = cellInfo.info {
            switch firstimageType {
            case .carFront:
                self.infoButton.isHidden = true
                if Helper.toString(object: info[Constants.UIKeys.carFront]) != "" {
                    self.firstImageView.setImage(urlStr: Helper.toString(object: info[Constants.UIKeys.carFront]), placeHolderImage: #imageLiteral(resourceName: "placeholder"))
                } else if let image = info[Constants.UIKeys.carFront] as? UIImage {
                    self.firstImageView.image = image
                }
                self.firstImageLabel.text = "Car Front".localizedString()
                break
            case .carLeftSide:
                self.infoButton.isHidden = true
                    if Helper.toString(object: info[Constants.UIKeys.carLeftSide]) != "" {
                    self.firstImageView.setImage(urlStr: Helper.toString(object: info[Constants.UIKeys.carLeftSide]), placeHolderImage: #imageLiteral(resourceName: "placeholder"))
                    } else if let image = info[Constants.UIKeys.carLeftSide] as? UIImage {
                        self.firstImageView.image = image
                    }
                    self.firstImageLabel.text = "Car Left Side".localizedString()
                
                break
            case .carMileage:
                self.infoButton.isHidden = true
                if Helper.toString(object: info[Constants.UIKeys.carMileage]) != "" {
                    self.firstImageView.setImage(urlStr: Helper.toString(object: info[Constants.UIKeys.carMileage]), placeHolderImage: #imageLiteral(resourceName: "placeholder"))
                } else if let image = info[Constants.UIKeys.carMileage] as? UIImage {
                    self.firstImageView.image = image
                }
                self.firstImageLabel.text = "Car Mileage (Odometer)".localizedString()
                
                break
            default:
                break
            }
            
            switch secondImageType {
            case .carRear:
                self.infoButton.isHidden = true
                 if Helper.toString(object: info[Constants.UIKeys.carRear]) != "" {
                     self.secondImageView.setImage(urlStr: Helper.toString(object: info[Constants.UIKeys.carRear]), placeHolderImage: #imageLiteral(resourceName: "placeholder"))
                 } else if let image = info[Constants.UIKeys.carRear] as? UIImage {
                     self.secondImageView.image = image
                 }
                 self.secondImageLabel.text = "Car Rear".localizedString()
                break
            case .carRightSide:
                self.infoButton.isHidden = true
                if Helper.toString(object: info[Constants.UIKeys.carRightSide]) != "" {
                    self.secondImageView.setImage(urlStr: Helper.toString(object: info[Constants.UIKeys.carRightSide]), placeHolderImage: #imageLiteral(resourceName: "placeholder"))
                } else if let image = info[Constants.UIKeys.carRightSide] as? UIImage {
                    self.secondImageView.image = image
                }
                self.secondImageLabel.text = "Car Right Side".localizedString()
                
                break
            case .gccSticker:
                self.infoButton.isHidden = false
                if Helper.toString(object: info[Constants.UIKeys.gccSticker]) != "" {
                self.secondImageView.setImage(urlStr: Helper.toString(object: info[Constants.UIKeys.gccSticker]), placeHolderImage: #imageLiteral(resourceName: "placeholder"))
                } else if let image = info[Constants.UIKeys.gccSticker] as? UIImage {
                    self.secondImageView.image = image
                }
                self.secondImageLabel.text = "GCC Sticker (If GCC)".localizedString()
                break
            default:
                break
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func tapFirstImageButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapFirstImageButton(cell: self)
        }
    }
    
    
    @IBAction func tapsecondImageButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapSecondImageButton(cell: self)
        }    }
}
