//
//  BVUploadImgReqViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/20/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

class BVUploadImgReqViewC: BaseViewC {
    
    @IBOutlet weak var uploadCarImagesTableView: TPKeyboardAvoidingTableView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var footerBgView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var frontImageView: UIImageView!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var certificateImageView: UIImageView!
    
    //MARK: - Variables
    var uploadDataSource: [CellInfo] = []
    var viewModel: BVUploadImgReqVModeling?
    var firstScreenParams = APIParams()
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupNavigationBarTitle(title: ChooseCarForServiceType.bankValuation.title ?? "",barColor: .white,titleColor: .black, leftBarButtonsType: [.back], rightBarButtonsType: [])
        
    }
    
    //MARK: - Private Methods
    
    private func setup() {
        self.recheckVM()
        self.setupView()
        self.setupTableView()
        self.loadDataSource()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = BVUploadImgReqViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.uploadCarImagesTableView.delegate = self
        self.uploadCarImagesTableView.dataSource = self
        self.uploadCarImagesTableView.separatorStyle = .none
        self.uploadCarImagesTableView.allowsSelection = false
        self.setupFooterView()
        self.uploadCarImagesTableView.tableFooterView = self.footerView
    }
    
    private func setupFooterView() {
        self.footerBgView.makeLayer(color: .blackColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_4)
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.submitButton.setTitle("Submit".localizedString(), for: .normal)
    }
    
    private func registerNibs() {
        
        self.uploadCarImagesTableView.register(BVCarImagesCell.self)
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getBVTenderReqDataSource(firstScreenAPIParams: self.firstScreenParams
            ) {
            self.uploadDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.uploadCarImagesTableView.reloadData()
            }
        }
    }
    
    private func getDocumentDict() -> [String: AnyObject] {
        var docParams = [String: AnyObject]()
        docParams[Constants.UIKeys.docFrontImage] = self.frontImageView.image as AnyObject
        docParams[Constants.UIKeys.docBackImage] = self.backImageView.image as AnyObject
        docParams[Constants.UIKeys.docCertificateImage] = self.certificateImageView.image as AnyObject
        return docParams
    }
    
    //MARK: - IBActions
    
    @IBAction func tapSubmitButton(_ sender: Any) {
        let params = getDocumentDict()
        self.viewModel?.requestTenderAPI(arrData: self.uploadDataSource, documentData: params, firstScreenAPIParams: self.firstScreenParams, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            if let viewControllers = sSelf.navigationController?.viewControllers {
                //CreateAccountViewCIsPresentInNavigationStack
                for viewController in viewControllers {
                    if viewController is RecoveryHomeViewC {
                        sSelf.navigationController?.popToViewController(viewController, animated: true)
                        return
                    }
                }
                
            }
        })
    }
    
    @IBAction func tapFrontImgButton(_ sender: Any) {
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.frontImageView.image = image
                    self.uploadCarImagesTableView.reloadData()
                }
            }
        }
    }
    
    @IBAction func tapBackImgButton(_ sender: Any) {
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.backImageView.image = image
                    self.uploadCarImagesTableView.reloadData()
                }
            }
        }
    }
    
    @IBAction func tapCertificateImgButton(_ sender: Any) {
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.certificateImageView.image = image
                    self.uploadCarImagesTableView.reloadData()
                }
            }
        }
    }
}
