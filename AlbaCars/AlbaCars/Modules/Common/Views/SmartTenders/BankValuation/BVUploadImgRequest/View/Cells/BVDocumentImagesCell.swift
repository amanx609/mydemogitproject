//
//  BVDocumentImagesCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/20/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
protocol BVDocumentImagesCellDelegate: class {
    func didTapImgCell(cell: BVDocumentImagesCell, imageType: DocumentType)
}

class BVDocumentImagesCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var frontImgButton: UIButton!
    @IBOutlet weak var frontImageView: UIImageView!
    @IBOutlet weak var backImgButton: UIButton!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var certificateImgButton: UIButton!
    @IBOutlet weak var certificateImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    //MARK: - Variables
    weak var delegate: BVDocumentImagesCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setup() {
        self.bgView.makeLayer(color: .blackColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_4)
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        
    }
    
    //MARK: - IBActions
    
    @IBAction func tapFrontImgButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapImgCell(cell: self, imageType: .front)
        }
    }
    
    @IBAction func tapBackImgButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapImgCell(cell: self, imageType: .back)
        }
    }
    
    @IBAction func tapCertificateImgButton(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapImgCell(cell: self, imageType: .ownerCertificate)
        }
    }
}
