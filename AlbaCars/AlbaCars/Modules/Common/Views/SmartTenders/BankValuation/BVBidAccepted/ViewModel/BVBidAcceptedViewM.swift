//
//  BVBidAcceptedViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/20/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol BVBidAcceptedVModeling: BaseVModeling {
    func getBVBidAcceptedDataSource(tenderDetails: RDBidListModel) -> [CellInfo]
    
}

class BVBidAcceptedVM: BaseViewM, BVBidAcceptedVModeling {
    
    
    func getBVBidAcceptedDataSource(tenderDetails: RDBidListModel) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //CarDetailCell
        var carDetailInfo = [String: AnyObject]()
        carDetailInfo[Constants.UIKeys.cellInfo] = tenderDetails
        carDetailInfo[Constants.UIKeys.isSecondDetailPage] = true as AnyObject
        let carDetailCell = CellInfo(cellType: .STCarDetailCell, placeHolder: "", value: "", info: carDetailInfo, height: Constants.CellHeightConstants.height_160)
        array.append(carDetailCell)
        
        //CarDetailCell
        var serviceDetailInfo = [String: AnyObject]()
         serviceDetailInfo[Constants.UIKeys.additionalInformation] = tenderDetails.additionalInfo as AnyObject
        serviceDetailInfo[Constants.UIKeys.cellInfo] = tenderDetails
        let serviceDetailCell = CellInfo(cellType: .WTServiceInfoCell, placeHolder: "", value: "", info: serviceDetailInfo, height: UITableView.automaticDimension)
        array.append(serviceDetailCell)
        
        
        //BidsCountCell
        var bidsCountCellInfo = [String: AnyObject]()
        bidsCountCellInfo[Constants.UIKeys.isSecondDetailPage] = true as AnyObject
        let bidsCountCell = CellInfo(cellType: .STBidsCountCell, placeHolder: "", value: "", info: bidsCountCellInfo, height: Constants.CellHeightConstants.height_55)
        array.append(bidsCountCell)
        
        //Bid Detail cell
        var bidDetailCellInfo = [String: AnyObject]()
        if let bids = tenderDetails.Bids {
            bidDetailCellInfo[Constants.UIKeys.serviceType] = "Bank Valuation".localizedString() as AnyObject
            bidDetailCellInfo[Constants.UIKeys.bankName] = Helper.toString(object: tenderDetails.bankName) as AnyObject
            bidDetailCellInfo[Constants.UIKeys.carPrice] = Helper.toString(object: tenderDetails.carPrice) as AnyObject
            bidDetailCellInfo[Constants.UIKeys.servicePrice] = Helper.toString(object: bids.amount) as AnyObject
            bidDetailCellInfo[Constants.UIKeys.paymentType] = Helper.toString(object: bids.paymentType) as AnyObject
            bidDetailCellInfo[Constants.UIKeys.transaction] =  Helper.toString(object: bids.transactionNo) as AnyObject
        }
        
        let bidDetailCell = CellInfo(cellType: .BidDetailCell, placeHolder: "", value: "", info: bidDetailCellInfo, height:UITableView.automaticDimension)
        array.append(bidDetailCell)
        
        //service provider detail
        var firstBidCellInfo = [String: AnyObject]()
        firstBidCellInfo[Constants.UIKeys.agencyName] = Helper.toString(object: tenderDetails.driverName) as AnyObject
        firstBidCellInfo[Constants.UIKeys.rating] = tenderDetails.rating as AnyObject
        firstBidCellInfo[Constants.UIKeys.address] = Helper.toString(object: tenderDetails.driverAddress) as AnyObject
        firstBidCellInfo[Constants.UIKeys.driverImage] = Helper.toString(object: tenderDetails.driverImage) as AnyObject
        
        let firstBidCell = CellInfo(cellType: .SPDetailCell, placeHolder: "", value: "", info: firstBidCellInfo, height: Constants.CellHeightConstants.height_110)
        array.append(firstBidCell)
        
        //Contact Info Cell
        
        let secondBidCell = CellInfo(cellType: .VIContactInfoCell, placeHolder: "+971 \(Helper.toString(object: tenderDetails.driverMobile))", value:Helper.toString(object: tenderDetails.driverEmail), info: nil, height: Constants.CellHeightConstants.height_120)
        array.append(secondBidCell)
        
        
        return array    }
}
