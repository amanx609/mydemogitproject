//
//  BVTenderRequestViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/20/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol BVTenderRequestVModeling: BaseVModeling {
    func getBVReqDataSource(carDetails: ChooseCarModel?) -> [CellInfo]
    func getAPIParams(arrData: [CellInfo], vehicleId: Int) -> APIParams
    func requestTenderAPI(arrData: [CellInfo], vehicleId: Int, completion: @escaping (Bool)-> Void)
    func validateFormParameters(arrData: [CellInfo]) -> Bool
}

class BVTenderRequestViewM: BaseViewM, BVTenderRequestVModeling {
    func getBVReqDataSource(carDetails: ChooseCarModel?) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Plate Number
        let carPlateCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.carPlate.localizedString(), value: Helper.toString(object: carDetails?.plateNumber), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carPlateCell)
        
        //Car brand
        let carBrandCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value:  Helper.toString(object: carDetails?.brandName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carBrandCell)
        
        //Car Model
        let carModelCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value:  Helper.toString(object: carDetails?.modelName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carModelCell)
        
        //Car Type
        let carTypeCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.typeName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carTypeCell)
        
        //Car Year
        let carYearCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.year), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carYearCell)
        
        //Mileage
        let carCylinderCell = CellInfo(cellType: .TextInputCell, placeHolder: "", value: Helper.toString(object: carDetails?.mileage), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carCylinderCell)
        
        //Additional Information Cell
        let addInfoCell = CellInfo(cellType: .TextViewCell, placeHolder: "Additional Information".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(addInfoCell)
        
        //Price Needed Label Cell
        var priceNeedLabelInfo = [String: AnyObject]()
        priceNeedLabelInfo[Constants.UIKeys.placeholderText] = "Please Enter the Car Price Needed for Valuation".localizedString() as AnyObject
        priceNeedLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let priceNeedLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "".localizedString(), value: "", info: priceNeedLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(priceNeedLabelCell)
        
        //price input
        var priceLabelInfo = [String: AnyObject]()
        priceLabelInfo[Constants.UIKeys.inputType] = TextInputType.expectedPrice as AnyObject
        let priceInputCell = CellInfo(cellType: .TextInputCell, placeHolder: "", value:"", info: priceLabelInfo, height: Constants.CellHeightConstants.height_80, keyboardType: UIKeyboardType.numberPad)
        array.append(priceInputCell)
        
        //Bank Need Label Cell
        var bankNeedLabelInfo = [String: AnyObject]()
        bankNeedLabelInfo[Constants.UIKeys.placeholderText] = "Bank Needed for Valuation".localizedString() as AnyObject
        bankNeedLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let bankNeedLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "".localizedString(), value: "", info: bankNeedLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(bankNeedLabelCell)
        
        //bank name input
        
        let bankInputCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value:"Select Bank".localizedString(), info: nil, height: Constants.CellHeightConstants.height_80)
        array.append(bankInputCell)
        
        
        //choose Date time Label Cell
        var chooseLabelInfo = [String: AnyObject]()
        chooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Tender".localizedString() as AnyObject
        chooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Tender".localizedString(), value: "", info: chooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseLabelCell)
        
        //DateRange Cell
        var dateRangeInfo = [String: AnyObject]()
        dateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let dateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: dateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(dateRangeCell)
        
        
        //Job Date time Label Cell
        var jobVhooseLabelInfo = [String: AnyObject]()
        jobVhooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Job".localizedString() as AnyObject
        jobVhooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let jobChooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Job".localizedString(), value: "", info: jobVhooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(jobChooseLabelCell)
        
        //DateRange Cell
        var jobDateRangeInfo = [String: AnyObject]()
        jobDateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let jobDateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: jobDateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(jobDateRangeCell)
        
        
        //choose Duration Label Cell
        var chooseDurationLabelInfo = [String: AnyObject]()
        chooseDurationLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Tender Duration".localizedString() as AnyObject
        chooseDurationLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseDurationLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Tender Duration".localizedString(), value: "", info: chooseDurationLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseDurationLabelCell)
        
        //Tender Duration
        let tenderDurationCell = CellInfo(cellType: .ImageDropDownCell, placeHolder: "", value: StringConstants.Text.tenderDuration.localizedString(), info: nil, height: Constants.CellHeightConstants.height_80)
        array.append( tenderDurationCell)
        
        
        return array
    }
    
    func requestTenderAPI(arrData: [CellInfo], vehicleId: Int, completion: @escaping (Bool)-> Void) {
        if self.validateFormParameters(arrData: arrData) {
            let tenderRequestParams = self.getAPIParams(arrData: arrData, vehicleId: vehicleId)
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderRequestForm(param: tenderRequestParams))) { (response, success) in
                if success {
                    if let safeResponse =  response as? [String: AnyObject]
                    {
                        completion(success)
                    }
                }
            }
        }
    }
    
    func validateFormParameters(arrData: [CellInfo]) -> Bool {
        
        var isValid = true
        if let price = arrData[8].value, price.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please Enter the Car Price Needed for Valuation".localizedString())
            isValid = false
        }  else if let bank = arrData[10].value, bank == "Select Bank".localizedString() {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select bank for valuation.".localizedString())
            isValid = false
        }  else if let date = arrData[12].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of tender.".localizedString())
            isValid = false
        } else if let time = arrData[12].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of tender.".localizedString())
            isValid = false
        }  else if let date = arrData[14].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of job.".localizedString())
            isValid = false
        } else if let time = arrData[14].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of job.".localizedString())
            isValid = false
        }  else if let duration = arrData[16].value, (duration.isEmpty || duration == StringConstants.Text.tenderDuration.localizedString()) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select duration of tender.".localizedString())
            isValid = false
        }
        
        if isValid  {
            
            let dateStr = arrData[12].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let timeStr = arrData[12].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            let jobDateStr = arrData[14].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let jobTimeStr = arrData[14].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            
            let tenderDateTime = Helper.toString(object: dateStr) + " " +  Helper.toString(object: timeStr)
            let jobDateTime = Helper.toString(object: jobDateStr) + " " +  Helper.toString(object: jobTimeStr)
            isValid = Helper.compareJobAndTenderDateTime(tenderDateTime: tenderDateTime, jobDateTime: jobDateTime)
        }

        
        return isValid
    }
    
    func getImagesFromImagesCell(arrData: [CellInfo]) {
        //let images = []
    }
    
    func getAPIParams(arrData: [CellInfo], vehicleId: Int) -> APIParams {
        var params: APIParams = APIParams()
        params[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        params[ConstantAPIKeys.carPrice] = arrData[8].value as AnyObject
        params[ConstantAPIKeys.bankId] = arrData[10].placeHolder as AnyObject
        params[ConstantAPIKeys.type] = RecoveryHomeScreenType.bankValuation.rawValue as AnyObject
        
        let dateStr = arrData[12].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let timeStr = arrData[12].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.requestedDate] = dateStr as AnyObject
        params[ConstantAPIKeys.requestedTime] = timeStr as AnyObject
        let jobDateStr = arrData[14].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let jobTimeStr = arrData[14].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.jobDate] = jobDateStr as AnyObject
        params[ConstantAPIKeys.jobTime] = jobTimeStr as AnyObject
        params[ConstantAPIKeys.duration] = arrData[16].placeHolder as AnyObject
        
        return params
    }
}
