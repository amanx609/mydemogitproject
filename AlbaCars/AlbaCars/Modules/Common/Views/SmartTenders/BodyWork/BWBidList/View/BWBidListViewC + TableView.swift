//
//  BWBidListViewC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension BWBidListViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bidListDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.bidListDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellInfo = self.bidListDataSource[indexPath.row]
        guard let cellType = cellInfo.cellType else { return self.bidListDataSource[indexPath.row].height }
        if self.tenderStatus == .completedTenders || self.tenderStatus == .allBidsRejected {
            
            switch cellType {
            case .STBidsCountCell:
                return Constants.CellHeightConstants.height_0
            case .RDBidListCell:
                return Constants.CellHeightConstants.height_0
            default:
                break
            }
        }
        return self.bidListDataSource[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
           let cellInfo = self.bidListDataSource[indexPath.row]
           guard let cellType = cellInfo.cellType else { return }
           switch cellType {
           case .RDBidListCell:
               if let info = cellInfo.info {
                   self.requestBidTenderDetails(bidId: Helper.toInt(info[ConstantAPIKeys.id]))
               }
              break
           default:
               break
           }
       }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let lastVisibleRow = self.bidListTableView.indexPathsForVisibleRows?.last?.row else { return }
        if (self.bidListDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
            if let tenderDetail = self.tenderDetails {
                self.requestBidListAPI(tenderDetails: tenderDetail)
            }
        }
    }
}

extension BWBidListViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .STCarDetailCell:
            let cell: STCarDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo, type: .bodyWork, tenderStatus: self.tenderStatus)
            return cell
            
        case .RDContactInfoCell:
            let cell: RDContactInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .WServiceDetailsCell:
            let cell: WServiceDetailsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureViewBW(cellInfo: cellInfo)
            return cell
            
        case .STBidsCountCell:
            let cell: STBidsCountCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .bodyWork)
            return cell
            
        case .DentDescriptionCell:
            let cell: DentDescriptionCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .RDBidListCell:
            let cell: RDBidListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo, type: .bodyWork)
            return cell
            
        case .DAddAuctionImageCell:
            let cell: DAddAuctionImageCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .ThreeImageCell:
            let cell: ThreeImageCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

extension BWBidListViewC: RDBidListCellDelegate {
    func didTapCheck(cell: RDBidListCell) {
        guard let indexPath = self.bidListTableView.indexPath(for: cell) else { return }
        let bidId = Helper.toInt(self.bidListDataSource[indexPath.row].info?[Constants.UIKeys.id])
        let bidAmount = Helper.toDouble(self.bidListDataSource[indexPath.row].info?[Constants.UIKeys.bidAmount])
        self.requestAcceptBid(bidId: bidId, winAmount: bidAmount)
    }
    
    func didTapCross(cell: RDBidListCell) {
        guard let indexPath = self.bidListTableView.indexPath(for: cell) else { return }
        let bidId = Helper.toInt(self.bidListDataSource[indexPath.row].info?[Constants.UIKeys.id])
        
        self.requestRejectBid(bidId: bidId)
    }
}

extension BWBidListViewC: STCarDetailCellDelegate {
    func didTapCancelButton(cell: STCarDetailCell) {
        self.requestCancelTender()
    }
    
    
}

//Mark: ThreeImageCellDelegate
extension BWBidListViewC: ThreeImageCellDelegate {
    func didTapViewAllCell(cell: ThreeImageCell) {
        let imageViewerVC = DIConfigurator.sharedInstance.getImageViewerVC()
        imageViewerVC.imageDataSource = self.tenderDetails?.image ?? []
        self.navigationController?.pushViewController(imageViewerVC, animated: true)
    }
    
}

