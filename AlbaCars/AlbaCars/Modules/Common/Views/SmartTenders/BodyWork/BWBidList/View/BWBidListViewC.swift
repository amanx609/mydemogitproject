//
//  BWBidListViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class BWBidListViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bidListTableView: UITableView!
    
    //MARK: - Variables
    var viewModel: BWBidListVModeling?
    var bidListDataSource: [CellInfo] = []
    var tenderId: Int?
    var nextPageNumber = Int(1)
    var tenderDetails: RDBidListModel?
    var tenderStatus: TenderStatus = .all
    var screenType = BodyWorkOptions(rawValue: 0)
    var durationTimer : Timer?
    var allBidList: [BidListModel] = []

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        switch screenType {
        case .panelPainting:
            self.setupNavigationBarTitle(title: "Panel Painting Tender Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
            break
        case .paintlessDentRemoval:
            self.setupNavigationBarTitle(title: "Paintless Dent Removal Tender Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
            break
        case .ceramicPaint:
            self.setupNavigationBarTitle(title: "Ceramic Paint Protection Tender Details".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
            break
        default:
            break
        }
        
    }
    
    deinit {
        print("deinit BWBidListViewC")
        self.invalidateTimer()
    }
    
    //MARK: - Private Methods
    private func setup() {
        
        self.recheckVM()
        self.setupTableView()
        self.requestTenderDetailsAPI()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = BWBidListViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.bidListTableView.delegate = self
        self.bidListTableView.dataSource = self
        self.bidListTableView.separatorStyle = .none
    }
    
    private func registerNibs() {
        self.bidListTableView.register(STCarDetailCell.self)
        self.bidListTableView.register(RDContactInfoCell.self)
        self.bidListTableView.register(STBidsCountCell.self)
        self.bidListTableView.register(RDBidListCell.self)
        self.bidListTableView.register(WServiceDetailsCell.self)
        self.bidListTableView.register(BottomLabelCell.self)
        self.bidListTableView.register(ThreeImageCell.self)
        self.bidListTableView.register(DentDescriptionCell.self)
        self.bidListTableView.register(DAddAuctionImageCell.self)
        self.bidListTableView.register(RDBidListCell.self)
        
    }
    
    private func loadDataSource(tenderDetails: RDBidListModel, bidList: [BidListModel]) {
        if let screenType = self.screenType {
            if let dataSource = self.viewModel?.getBidDetailDataSource(tenderDetails: tenderDetails, bidList: bidList, screenType: screenType) {
                self.bidListDataSource = dataSource
                self.bidListTableView.reloadData()
            }
        }
        
    }
    
    private func setupTimer() {
        self.invalidateTimer()
        self.durationTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimeValueInDataSource), userInfo: nil, repeats: true)
    }
    
    private func invalidateTimer() {
        self.durationTimer?.invalidate()
        self.durationTimer = nil
    }
    
    //MARK: - Selector Methods
    @objc func updateTimeValueInDataSource() {
        if var stCarDetailCellInfo = self.bidListDataSource.first?.info,
            let tenderDetail = stCarDetailCellInfo[Constants.UIKeys.cellInfo] as? RDBidListModel {
            switch self.tenderStatus {
            case .upcomingTenders:
                if let timerDuration = tenderDetail.startTimerDuration {
                    tenderDetail.startTimerDuration = timerDuration - 1
                    
                    //RemoveDataIfDurationIsFinished
                    if timerDuration == 0 {
                        //ActionWhenTimerIsZero
                    }
                }
            case .inProgress, .noBidAccepted:
                if let timerDuration = tenderDetail.endTimerDuration {
                    tenderDetail.endTimerDuration = timerDuration - 1
                    
                    //RemoveDataIfDurationIsFinished
                    if timerDuration == 0 {
                        //ActionWhenTimerIsZero
                    }
                }
            default:
                break
            }
            stCarDetailCellInfo[Constants.UIKeys.cellInfo] = tenderDetail
            self.bidListDataSource[0].info = stCarDetailCellInfo
            let stCarDetailCellIndexPath = IndexPath(row: 0, section: 0)
            self.bidListTableView.reloadRows(at: [stCarDetailCellIndexPath], with: .none)
        }
    }
    
    //MARK: - APIMethods
    private func requestTenderDetailsAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
        //params[ConstantAPIKeys.type] = 1 as AnyObject
        self.viewModel?.requestTenderDetailsAPI(params: params, completion: {[weak self] (tenderDetails) in
            guard let sSelf = self else { return }
            sSelf.tenderDetails = tenderDetails
            sSelf.loadDataSource(tenderDetails: tenderDetails, bidList: [])
            sSelf.setupTimer()
            sSelf.requestBidListAPI(tenderDetails: tenderDetails)
        })
    }
    
    func requestBidListAPI(tenderDetails: RDBidListModel) {
        var params = APIParams()
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
        params[ConstantAPIKeys.orderBy] = 1 as AnyObject
        self.viewModel?.requestBidListAPI(params: params, completion: { [weak self] (nextPageNumber, perPage, bidList) in
            guard let sSelf = self else { return }
            sSelf.nextPageNumber = nextPageNumber
            sSelf.allBidList = sSelf.allBidList + bidList
            sSelf.loadDataSource(tenderDetails: tenderDetails, bidList: sSelf.allBidList)
        })
    }
    
    //MARK: - Public Methods
    func requestAcceptBid(bidId: Int, winAmount: Double) {
        
//       let vatAmmount = Helper.calculateVat(vatPercentage: self.tenderDetails?.vatPercentage, bidAmount: winAmount)
//        let totalAmmount = winAmount + vatAmmount

        let paymentDetails = PaymentDetailsData(viewController: self, totalAmmount: winAmount, referenceId: Helper.toInt(self.tenderId), paymentServiceCategoryId: PaymentServiceCategoryId.smartTenders,vatAmmount: 0)
        
        PaymentManager.sharedInstance.initiatePayment(paymentDetailsData: paymentDetails) { (success, paymentID) in
            
            if success {
                
                var params = APIParams()
                params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
                params[ConstantAPIKeys.bidId] = bidId as AnyObject
                params[ConstantAPIKeys.winAmount] = winAmount as AnyObject
                params[ConstantAPIKeys.paymentId] = paymentID as AnyObject
                
                self.viewModel?.requestAcceptBidAPI(params: params, completion: { [weak self] (success)  in
                    guard let sSelf = self else { return }
                    
                    let secondDetailVC = DIConfigurator.sharedInstance.getBWBidAcceptedVC()
                    secondDetailVC.tenderId = sSelf.tenderId
                    secondDetailVC.acceptedBidId = bidId
                    secondDetailVC.tenderStatus = sSelf.tenderStatus
                    sSelf.navigationController?.pushViewController(secondDetailVC, animated: true)
                })
                
            }
        }
        
    }
    
    func requestRejectBid(bidId: Int) {
        var params = APIParams()
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        params[ConstantAPIKeys.bidId] = bidId as AnyObject
        
        self.viewModel?.requestRejectBidAPI(params: params, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            sSelf.requestTenderDetailsAPI()
            
        })
    }
    
    func requestCancelTender() {
        var params = APIParams()
        params[ConstantAPIKeys.tenderId] = self.tenderId as AnyObject
        self.viewModel?.requestCancelTenderAPI(params: params, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            if let viewControllers = sSelf.navigationController?.viewControllers {
                //CreateAccountViewCIsPresentInNavigationStack
                for viewController in viewControllers {
                    if viewController is RecoveryHomeViewC {
                        sSelf.navigationController?.popToViewController(viewController, animated: true)
                        return
                    }
                }
                
            }
            
        })
    }
    
    func requestBidTenderDetails(bidId: Int) {
          
        let recoveryTenderViewModel:SRecoveryTenderViewM = SRecoveryTenderViewM()
          var params = APIParams()
          params[ConstantAPIKeys.tenderId] = bidId as AnyObject
          recoveryTenderViewModel.requestMyTenderDetailsAPI(params: params, completion: { [weak self] (tender) in
              guard let sSelf = self else { return }
              Threads.performTaskInMainQueue {
                  let submittedVC = DIConfigurator.sharedInstance.getSBodyWorkBidSubmittedVC()
                  submittedVC.tenderDetails = tender
                  sSelf.navigationController?.pushViewController(submittedVC, animated: true)
              }
          })
      }
}
