//
//  BWBidListViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol BWBidListVModeling: BaseVModeling {
    func getBidDetailDataSource(tenderDetails: RDBidListModel, bidList: [BidListModel], screenType: BodyWorkOptions) -> [CellInfo]
    func requestTenderDetailsAPI(params: APIParams, completion: @escaping (RDBidListModel) -> Void)
    func requestBidListAPI(params: APIParams, completion: @escaping (Int, Int, [BidListModel]) -> Void)
    func requestAcceptBidAPI(params: APIParams, completion: @escaping (Bool) -> Void)
    func requestRejectBidAPI(params: APIParams, completion: @escaping (Bool) -> Void)
    func requestCancelTenderAPI(params: APIParams, completion: @escaping (Bool) -> Void)
}

class BWBidListViewM: BaseViewM, BWBidListVModeling {
    
    
    func getBidDetailDataSource(tenderDetails: RDBidListModel, bidList: [BidListModel], screenType: BodyWorkOptions) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //CarDetailCell
        var carDetailInfo = [String: AnyObject]()
        carDetailInfo[Constants.UIKeys.cellInfo] = tenderDetails
        let carDetailCell = CellInfo(cellType: .STCarDetailCell, placeHolder: "", value: "", info: carDetailInfo, height: Constants.CellHeightConstants.height_240)
        array.append(carDetailCell)
        
        // Image
        var imageInfo = [String: AnyObject]()
        imageInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "bodywork.png") as AnyObject
        let imageCell = CellInfo(cellType: .DAddAuctionImageCell, placeHolder: "", value:"" ,info: imageInfo, height: Constants.CellHeightConstants.height_230)
        array.append(imageCell)
        
        //Dent Description Cell
        //var dentDescriptionInfo = [String: AnyObject]()
        if let dentDescArray = tenderDetails.dentDescription {
            for i in 0..<dentDescArray.count {
                let dentDescriptionCell = CellInfo(cellType: .DentDescriptionCell, placeHolder: Helper.toString(object: i+1), value: dentDescArray[i], info: nil, height: UITableView.automaticDimension)
                array.append(dentDescriptionCell)
            }
        }
        
        //Three Images Cell
        if let totalImage = tenderDetails.image, totalImage.count > 0 {
            var imagesInfo = [String: AnyObject]()
            imagesInfo[Constants.UIKeys.images] = totalImage as AnyObject
            let threeImageCell = CellInfo(cellType: .ThreeImageCell, placeHolder:"", value:"" ,info:imagesInfo, height: Constants.CellHeightConstants.height_80)
            array.append(threeImageCell)
        }
        
        //CarDetailCell
        var serviceDetailInfo = [String: AnyObject]()
        serviceDetailInfo[Constants.UIKeys.serviceType] = tenderDetails.typeOfService as AnyObject
        serviceDetailInfo[Constants.UIKeys.wheelSize] = tenderDetails.wheelSize as AnyObject
        serviceDetailInfo[Constants.UIKeys.additionalInformation] = tenderDetails.additionalInfo as AnyObject
        let serviceDetailCell = CellInfo(cellType: .WServiceDetailsCell, placeHolder: "", value: "", info: serviceDetailInfo, height: UITableView.automaticDimension)
        array.append(serviceDetailCell)
        
//        //ContactInfoCell
//        let contactInfoCell = CellInfo(cellType: .RDContactInfoCell, placeHolder: Helper.toString(object: tenderDetails.name), value: Helper.toString(object: tenderDetails.contact), info: nil, height: Constants.CellHeightConstants.height_100)
//        array.append(contactInfoCell)
        
        //BidsCountCell
        var bidsCountCellInfo = [String: AnyObject]()
        bidsCountCellInfo[Constants.UIKeys.isSecondDetailPage] = false as AnyObject
        let bidsCount = Helper.toInt(tenderDetails.bidsCount)
        let bidsCountCell = CellInfo(cellType: .STBidsCountCell, placeHolder: "", value: Helper.toString(object: bidsCount), info: bidsCountCellInfo, height: Constants.CellHeightConstants.height_55)
        array.append(bidsCountCell)
        
        //BidCell
        for i in 0..<bidList.count {
            var bidCellInfo = [String: AnyObject]()
            bidCellInfo[Constants.UIKeys.bidAmount] = bidList[i].amount as AnyObject
            bidCellInfo[Constants.UIKeys.time] = bidList[i].daysForService as AnyObject
            bidCellInfo[Constants.UIKeys.serviceType] = bidList[i].getBodyWorkType().0 as AnyObject
            bidCellInfo[Constants.UIKeys.serviceTypeValue] = bidList[i].getBodyWorkType().1 as AnyObject
            bidCellInfo[Constants.UIKeys.partType] = bidList[i].getPartType() as AnyObject
            bidCellInfo[Constants.UIKeys.id] = bidList[i].bidId as AnyObject
            bidCellInfo[ConstantAPIKeys.name] = bidList[i].name as AnyObject
            bidCellInfo[ConstantAPIKeys.rating] = bidList[i].rating as AnyObject
            bidCellInfo[ConstantAPIKeys.vatAmmount] = tenderDetails.getVatPercentage() as AnyObject
            bidCellInfo[ConstantAPIKeys.image] = bidList[i].image as AnyObject

            let firstBidCell = CellInfo(cellType: .RDBidListCell, placeHolder: "", value: "", info: bidCellInfo, height: UITableView.automaticDimension)
            array.append(firstBidCell)
        }
                
        return array
    }
    
    func requestTenderDetailsAPI(params: APIParams, completion: @escaping (RDBidListModel) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderDetails(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData(),
                    let tenderDetails = RDBidListModel(jsonData: resultData) {
                    completion(tenderDetails)
                }
            }
        }
    }
    
    func requestBidListAPI(params: APIParams, completion: @escaping (Int, Int, [BidListModel]) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .bidList(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let _ = result[kData] as? [[String: AnyObject]],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let resultData = result.toJSONData(),
                    let bidList = Bids(jsonData: resultData) {
                    completion(nextPageNumber, perPage,bidList.bids ?? [])
                }
            }
        }
    }
    
    func requestAcceptBidAPI(params: APIParams, completion: @escaping (Bool) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .acceptBid(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let data = result[ConstantAPIKeys.data] as? [String: AnyObject]
                    //                    let jsonData = data.toJSONData(),
                    //                    let secondDetailData = AcceptBidModel(jsonData: jsonData)
                {
                    completion(success)
                }
            }
        }
    }
    
    func requestRejectBidAPI(params: APIParams, completion: @escaping (Bool) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .rejectBid(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                {
                    print(result)
                    completion(success)
                }
            }
        }
        
        
    }
    
    func requestCancelTenderAPI(params: APIParams, completion: @escaping (Bool) -> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .cancelTender(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject]
                {
                    completion(success)
                }
            }
        }
        
        
    }
}
