//
//  PPTenderRequestViewM.swift
//  AlbaCars
//
//  Created by Narendra on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol PPTenderRequestViewModeling: BaseVModeling {
    func getReqDataSource(carDetails: ChooseCarModel?,bodyWorkOptions: BodyWorkOptions) -> [CellInfo]
    func validateFormParameters(arrData: [CellInfo],bodyWorkOptions: BodyWorkOptions) -> Bool
    func getAPIParams(arrData: [CellInfo], vehicleId: Int,bodyWorkOptions: BodyWorkOptions) -> APIParams
}

class PPTenderRequestViewM: BaseViewM, PPTenderRequestViewModeling {
    
    func getReqDataSource(carDetails: ChooseCarModel?,bodyWorkOptions: BodyWorkOptions) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Car Plate Number
        let carPlateCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.carPlate.localizedString(), value: Helper.toString(object: carDetails?.plateNumber), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carPlateCell)
        
        //Car brand
        let carBrandCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value:  Helper.toString(object: carDetails?.brandName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carBrandCell)
        
        //Car Model
        let carModelCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value:  Helper.toString(object: carDetails?.modelName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carModelCell)
        
        //Car Type
        let carTypeCell = CellInfo(cellType: .DropDownCell, placeHolder: "", value: Helper.toString(object: carDetails?.typeName), info: nil, height: Constants.CellHeightConstants.height_80, isUserInteractionEnabled: false)
        array.append(carTypeCell)
        
        //Paint color
        let tintColorCell = CellInfo(cellType: .TextInputCell, placeHolder: StringConstants.Text.paintColor.localizedString(), value: "", info: nil, height: bodyWorkOptions == .panelPainting ? Constants.CellHeightConstants.height_80 : 0)
        array.append(tintColorCell)
        
        //Additional Information Cell
        let addInfoCell = CellInfo(cellType: .TextViewCell, placeHolder: "Additional Information".localizedString(), value: "", info: nil, height: UITableView.automaticDimension)
        array.append(addInfoCell)
        
        //choose Date time Label Cell
        var chooseLabelInfo = [String: AnyObject]()
        chooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Tender".localizedString() as AnyObject
        chooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Tender".localizedString(), value: "", info: chooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseLabelCell)
        
        //DateRange Cell
        var dateRangeInfo = [String: AnyObject]()
        dateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        dateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let dateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: dateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(dateRangeCell)
        
        //Job Date time Label Cell
        var jobVhooseLabelInfo = [String: AnyObject]()
        jobVhooseLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Date & Time Of Job".localizedString() as AnyObject
        jobVhooseLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let jobChooseLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Date & Time Of Job".localizedString(), value: "", info: jobVhooseLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(jobChooseLabelCell)
        
        //DateRange Cell
        var jobDateRangeInfo = [String: AnyObject]()
        jobDateRangeInfo[Constants.UIKeys.titlePlaceholder] = "Date".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.subTitlePlaceholder] = "Time".localizedString() as AnyObject
        jobDateRangeInfo[Constants.UIKeys.image] = #imageLiteral(resourceName: "clock")
        let jobDateRangeCell = CellInfo(cellType: .DateRangeCell, placeHolder: "", value: "", info: jobDateRangeInfo, height: Constants.CellHeightConstants.height_100)
        array.append(jobDateRangeCell)
        
        //choose Duration Label Cell
        var chooseDurationLabelInfo = [String: AnyObject]()
        chooseDurationLabelInfo[Constants.UIKeys.placeholderText] = "Please Choose Tender Duration".localizedString() as AnyObject
        chooseDurationLabelInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let chooseDurationLabelCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "Please Choose Tender Duration".localizedString(), value: "", info: chooseDurationLabelInfo, height: Constants.CellHeightConstants.height_60)
        array.append(chooseDurationLabelCell)
        
        //Tender Duration
        let tenderDurationCell = CellInfo(cellType: .ImageDropDownCell, placeHolder: "", value: StringConstants.Text.tenderDuration.localizedString(), info: nil, height: Constants.CellHeightConstants.height_80)
        array.append( tenderDurationCell)
        
        return array
    }
    
    func validateFormParameters(arrData: [CellInfo],bodyWorkOptions: BodyWorkOptions) -> Bool {
        var isValid = true
        
        if bodyWorkOptions == .panelPainting, let serviceType = arrData[4].value, serviceType.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please enter paint color".localizedString())
            isValid = false
        } else if let date = arrData[7].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of tender.".localizedString())
            isValid = false
        } else if let time = arrData[7].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of tender.".localizedString())
            isValid = false
        } else if let date = arrData[9].placeHolder, date.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select date of job.".localizedString())
            isValid = false
        } else if let time = arrData[9].value, time.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select time of job.".localizedString())
            isValid = false
        } else if let duration = arrData[11].value, (duration.isEmpty || duration == StringConstants.Text.tenderDuration.localizedString()) {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please select duration of tender.".localizedString())
            isValid = false
        }
        
        if isValid  {
            
            let dateStr = arrData[7].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let timeStr = arrData[7].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            let jobDateStr = arrData[9].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
            let jobTimeStr = arrData[9].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
            
            let tenderDateTime = Helper.toString(object: dateStr) + " " +  Helper.toString(object: timeStr)
            let jobDateTime = Helper.toString(object: jobDateStr) + " " +  Helper.toString(object: jobTimeStr)
            isValid = Helper.compareJobAndTenderDateTime(tenderDateTime: tenderDateTime, jobDateTime: jobDateTime)
            
        }
        

        
        return isValid
    }
    
    func getAPIParams(arrData: [CellInfo], vehicleId: Int,bodyWorkOptions: BodyWorkOptions) -> APIParams {
        var params: APIParams = APIParams()
        let dateStr = arrData[7].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let timeStr = arrData[7].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.additionalInformation] = arrData[5].value as AnyObject
        params[ConstantAPIKeys.vehicleId] = vehicleId as AnyObject
        params[ConstantAPIKeys.requestedDate] = dateStr as AnyObject
        params[ConstantAPIKeys.requestedTime] = timeStr as AnyObject
        
        let jobDateStr = arrData[9].placeHolder.getDateStringFrom(inputFormat: Constants.Format.dateFormatWithoutSpace, outputFormat: Constants.Format.dateFormatyyyyMMdd)
        let jobTimeStr = arrData[9].value.getDateStringFrom(inputFormat: Constants.Format.timeFormathmma, outputFormat: Constants.Format.timeFormat24hhmmss)
        params[ConstantAPIKeys.jobDate] = jobDateStr as AnyObject
        params[ConstantAPIKeys.jobTime] = jobTimeStr as AnyObject
        
        params[ConstantAPIKeys.duration] = arrData[11].placeHolder as AnyObject
        
        if bodyWorkOptions == .panelPainting {
            params[ConstantAPIKeys.paintColor] = arrData[4].value as AnyObject
            params[ConstantAPIKeys.type] = RecoveryHomeScreenType.panelPainting.rawValue as AnyObject
        } else {
            params[ConstantAPIKeys.type] = RecoveryHomeScreenType.paintlessDentRemoval.rawValue as AnyObject
        }
        return params
    }
}
