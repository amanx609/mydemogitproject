//
//  PPTenderRequestViewC.swift
//  AlbaCars
//
//  Created by Narendra on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class PPTenderRequestViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tenderRequestTableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet var dateTimePicker: UIDatePicker!
    
    //Variables
    var viewModel: PPTenderRequestViewModeling?
    var tenderRequestDataSource: [CellInfo] = []
    var isDate = true
    var vehicleId: Int?
    var addCarViewModel: CAddNewCarViewModeling?
    var requestCarDataSource: [CellInfo] = []
    var vehicleBrandDataSource: [VehicleBrand] = []
    var vehicleTypeDataSource: [[String: AnyObject]] = []
    var vehicleModelDataSource: [[String: AnyObject]] = []
    var carDetails: ChooseCarModel?
    var bodyWorkOptions: BodyWorkOptions = .panelPainting
    var selectedIndexPath = IndexPath()

    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupNavigationBarTitle(title: bodyWorkOptions == .panelPainting ? "Panel Painting Tender".localizedString() : "Paintless Dent Removal Tender".localizedString(),barColor: .white,titleColor: .black, leftBarButtonsType: [.back], rightBarButtonsType: [])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    
    //MARK: - Private Methods
    private func setup() {
        
        self.registerNibs()
        self.setupViewModel()
        self.setupView()
        self.setupTableView()
        self.loadDataSource()
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.nextButton.setTitle("Next".localizedString(), for: .normal)
    }
    
    private func setupTableView() {
        self.tenderRequestTableView.separatorStyle = .none
        self.tenderRequestTableView.backgroundColor = UIColor.white
        self.tenderRequestTableView.delegate = self
        self.tenderRequestTableView.dataSource = self
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = PPTenderRequestViewM()
        }
        if self.addCarViewModel == nil {
            self.addCarViewModel = CAddNewCarVM()
        }
    }
    
    private func registerNibs() {
        self.tenderRequestTableView.register(DropDownCell.self)
        self.tenderRequestTableView.register(DateRangeCell.self)
        self.tenderRequestTableView.register(BottomLabelCell.self)
        self.tenderRequestTableView.register(ImageDropDownCell.self)
        self.tenderRequestTableView.register(TextViewCell.self)
        self.tenderRequestTableView.register(UploadVehicleImagesCell.self)
        self.tenderRequestTableView.register(TextInputCell.self)
    }
    
    private func loadDataSource() {
        
        if let dataSource = self.viewModel?.getReqDataSource(carDetails: self.carDetails, bodyWorkOptions: bodyWorkOptions) {
            self.tenderRequestDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.tenderRequestTableView.reloadData()
            }
        }
    }
    
    private func updateDateTime(_ pickerValue: String) {
        if isDate {
            self.tenderRequestDataSource[self.selectedIndexPath.row].placeHolder = pickerValue
        } else {
            self.tenderRequestDataSource[self.selectedIndexPath.row].value = pickerValue
        }
        self.tenderRequestTableView.reloadData()
    }
    
    //MARK: - Public Methods
    func setupDateTimePicker(_ textField: UITextField) {
        textField.inputView = self.dateTimePicker
        textField.inputAccessoryView = Helper.getDoubleButtonToolBar(leftButtonTitle: StringConstants.Text.Cancel.localizedString(), rightButtonTitle: StringConstants.Text.done.localizedString(), leftButtonselector: #selector(self.cancelButtonTapped), rightButtonSelector: #selector(self.doneButtonTapped), target: self)
        if isDate {
            self.dateTimePicker.datePickerMode = .date
        } else {
            self.dateTimePicker.datePickerMode = .time
        }
        self.dateTimePicker.minimumDate = Date()
    }
    
    func setupToolBar(_ textField: UITextField) {
        textField.inputAccessoryView = Helper.getSingleButtonToolBar(buttonTitle: StringConstants.Text.done.localizedString(), selector: #selector(self.doneButtonTapped), target: self)
    }
    
    func showDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        switch indexPath.row {
        case 11:
            listPopupView.initializeViewWith(title: "Select Time Duration", arrayList: Constants.tenderDuration, key: Constants.UIKeys.duration) { [weak self] (response) in
                guard let sSelf = self,
                    let timeDuration = response[ConstantAPIKeys.duration] as? String, let apiValue = response[Constants.UIKeys.value] as? String else { return }
                sSelf.tenderRequestDataSource[indexPath.row].value = timeDuration
                sSelf.tenderRequestDataSource[indexPath.row].placeHolder = apiValue
                sSelf.tenderRequestTableView.reloadRows(at: [indexPath], with: .none)
            }
            
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
    }
        
    //MARK: - Selectors
    @objc func cancelButtonTapped() {
        self.view.endEditing(true)
    }
    
    @objc func doneButtonTapped() {
        self.view.endEditing(true)
        let selectedDate = self.dateTimePicker.date
        var convertedValue = ""
        if isDate {
            convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatWithoutSpace)
            // self.auctionConfirmationDataSource[0].info?[Constants.UIKeys.firstValue] = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatdMMMyyyy) as AnyObject
        } else {
            convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.timeFormathmma)
           // self.tenderRequestDataSource[self.selectedIndexPath.row].info?[Constants.UIKeys.secondValue] = selectedDate.dateStringWith(strFormat: Constants.Format.timeFormathhmmss) as AnyObject
        }
        updateDateTime(convertedValue)
    }

    //MARK: - IBActions
    @IBAction func tapNextButton(_ sender: Any) {
       
        if let validate = self.viewModel?.validateFormParameters(arrData: self.tenderRequestDataSource, bodyWorkOptions: bodyWorkOptions), validate == true {
//            if let apiParams = self.viewModel?.getAPIParams(arrData:self.tenderRequestDataSource, vehicleId: self.vehicleId ?? 0) {
//                let tendersViewC = DIConfigurator.sharedInstance.getPPTenderRequestUploadImageVC()
//                tendersViewC.params = apiParams
//                self.navigationController?.pushViewController(tendersViewC, animated: true)
//            }
            
            if let apiParams = self.viewModel?.getAPIParams(arrData:self.tenderRequestDataSource, vehicleId: self.vehicleId ?? 0, bodyWorkOptions: bodyWorkOptions) {
                let tendersViewC = DIConfigurator.sharedInstance.getPPTenderDentRequestVC()
                tendersViewC.params = apiParams
                tendersViewC.bodyWorkOptions = self.bodyWorkOptions
                self.navigationController?.pushViewController(tendersViewC, animated: true)
            }
        }
    }
}
