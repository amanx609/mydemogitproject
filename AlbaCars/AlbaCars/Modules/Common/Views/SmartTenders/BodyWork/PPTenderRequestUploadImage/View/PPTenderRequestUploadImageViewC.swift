//
//  PPTenderRequestUploadImageViewC.swift
//  AlbaCars
//
//  Created by Narendra on 2/26/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class PPTenderRequestUploadImageViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tenderRequestTableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    
    //Variables
    var viewModel: PPTenderRequestUploadImageViewModeling?
    var tenderRequestDataSource: [CellInfo] = []
    var params: APIParams = APIParams()
    var bodyWorkOptions: BodyWorkOptions = .panelPainting

    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title:  bodyWorkOptions == .panelPainting ? "Panel Painting Tender".localizedString() : "Paintless Dent Removal Tender".localizedString(),barColor: .white,titleColor: .black, leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.registerNibs()
        self.setupViewModel()
        self.setupView()
        self.setupTableView()
        self.loadDataSource()
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.submitButton.setTitle("Submit".localizedString(), for: .normal)
    }
    
    private func setupTableView() {
        self.tenderRequestTableView.separatorStyle = .none
        self.tenderRequestTableView.backgroundColor = UIColor.white
        self.tenderRequestTableView.delegate = self
        self.tenderRequestTableView.dataSource = self
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = PPTenderRequestUploadImageViewM()
        }
    }
    
    private func registerNibs() {
        self.tenderRequestTableView.register(LocationTextFieldsCell.self)
        self.tenderRequestTableView.register(BottomLabelCell.self)
        self.tenderRequestTableView.register(NoReserveCell.self)
        self.tenderRequestTableView.register(UploadVehicleImagesCell.self)
    }
    
    private func loadDataSource() {
        
        if let dataSource = self.viewModel?.getPPTenderRequestUploadImageDataSource() {
            Threads.performTaskInMainQueue {
                self.tenderRequestDataSource = dataSource
                self.tenderRequestTableView.reloadData()
            }
        }
    }
    
//    func uploadVehicleImages() {
//        var images = [UIImage]()
//        if let firstImg = self.tenderRequestDataSource[1].info?[Constants.UIKeys.firstImg] as? UIImage {
//            images.append(firstImg)
//        }
//        if  let secondImg = self.tenderRequestDataSource[1].info?[Constants.UIKeys.secondImg]  as? UIImage {
//            images.append(secondImg)
//        }
//        if let thirdImg = self.tenderRequestDataSource[1].info?[Constants.UIKeys.thirdImg] as? UIImage {
//            images.append(thirdImg)
//        }
//        if let fourthImg = self.tenderRequestDataSource[1].info?[Constants.UIKeys.fourthImg] as? UIImage {
//            images.append(fourthImg)
//
//        }
//        if images.count > 0 {
//            S3Manager.sharedInstance.uploadImages(images: images, imageQuality: .medium, progress: { (percentage, success) in
//                print(percentage)
//                print(success)
//            }) { (imageNames, images, error) in
//                self.tenderRequestDataSource[1].info?[Constants.UIKeys.images] = imageNames as AnyObject
//                self.requestTenderAPI()
//            }
//        } else {
//            self.requestTenderAPI()
//        }
//    }
    
    func requestTenderAPI() {
        
        self.viewModel?.requestTenderAPI(arrData: self.tenderRequestDataSource,param:self.params,  completion: { [weak self] (success) in
            guard let sSelf = self else { return }
           if let viewControllers = sSelf.navigationController?.viewControllers {
              //CreateAccountViewCIsPresentInNavigationStack
              for viewController in viewControllers {
                if viewController is RecoveryHomeViewC {
                  sSelf.navigationController?.popToViewController(viewController, animated: true)
                  return
                }
              }
            }
        })
    }
        
    //MARK: - IBActions
    @IBAction func tapSubmitButton(_ sender: Any) {
        self.requestTenderAPI()

    }
}
