//
//  PPTenderRequestUploadImageViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 2/26/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension PPTenderRequestUploadImageViewC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tenderRequestDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.tenderRequestDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let isPickUpDropOff =  self.tenderRequestDataSource[2].info?[Constants.UIKeys.isSelected] as? Bool {
            if isPickUpDropOff {
                return self.tenderRequestDataSource[indexPath.row].height
            } else {
                let cellInfo = self.tenderRequestDataSource[indexPath.row]
                guard let cellType = cellInfo.cellType else { return Constants.CellHeightConstants.height_0 }
                if cellType == .LocationTextFieldsCell {
                    return Constants.CellHeightConstants.height_0
                }
            }
        }
        return self.tenderRequestDataSource[indexPath.row].height
    }
}

extension PPTenderRequestUploadImageViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .LocationTextFieldsCell:
            let cell: LocationTextFieldsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.targetViewController = self
            cell.configureView(cellInfo: cellInfo)
            cell.delegate = self
            return cell
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureViewForRequestForm(cellInfo: cellInfo)
            return cell
        case .UploadVehicleImagesCell:
            let cell: UploadVehicleImagesCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate =  self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .NoReserveCell:
            let cell: NoReserveCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - NoReserveCellCellDelegate
extension PPTenderRequestUploadImageViewC: NoReserveCellCellDelegate {
    func didTapNoReserve(cell: NoReserveCell, isReserved: Bool) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.isSelected] = isReserved as AnyObject
        self.view.endEditing(true)
        self.tenderRequestTableView.reloadData()
        
    }
}

//MARK: - LocationTextFieldCellDelegate
extension PPTenderRequestUploadImageViewC: LocationTextFieldsCellDelegate {
    
    func didChangeSourceText(cell: LocationTextFieldsCell, text: String, latitude: Double, longitude: Double) {
         guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.sourceValue] = text as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.sourceLatitude] = latitude as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.sourceLongitude] = longitude as AnyObject
    }
    
    func didChangeDestinationText(cell: LocationTextFieldsCell, text: String, latitude: Double, longitude: Double) {
         guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
               self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.destinationValue] = text as AnyObject
               self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.destinationLatitude] = latitude as AnyObject
               self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.destinationLongitude] = longitude as AnyObject
    }
    
    func tapNextKeyboard(cell: LocationTextFieldsCell) {
        self.view.endEditing(true)
    }
}

extension PPTenderRequestUploadImageViewC: UploadVehicleImagesCellDelegate {
    
    func didTapFirstImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.firstImg] = image as AnyObject
                    self.tenderRequestTableView.reloadData()
                }
            }
        }
        
    }
    
    func didTapSecondImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.secondImg] = image as AnyObject
                    self.tenderRequestTableView.reloadData()
                }
            }
        }
        
    }
    
    func didTapThirdImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.thirdImg] = image as AnyObject
                    self.tenderRequestTableView.reloadData()
                }
            }
        }
    }
    
    func didTapFourthImgCell(cell: UploadVehicleImagesCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.fourthImg] = image as AnyObject
                    self.tenderRequestTableView.reloadData()
                }
            }
        }
    }
}
