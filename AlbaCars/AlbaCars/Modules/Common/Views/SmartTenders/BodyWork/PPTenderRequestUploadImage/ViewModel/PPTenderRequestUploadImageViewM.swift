//
//  PPTenderRequestUploadImageViewM.swift
//  AlbaCars
//
//  Created by Narendra on 2/26/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol PPTenderRequestUploadImageViewModeling: BaseVModeling {
    func getPPTenderRequestUploadImageDataSource() -> [CellInfo]
    func requestTenderAPI(arrData: [CellInfo],param: APIParams, completion: @escaping (Bool)-> Void)
}

class PPTenderRequestUploadImageViewM: PPTenderRequestUploadImageViewModeling {
    
    func getPPTenderRequestUploadImageDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Upload photo Label Cell
        var uploadPhotoInfo = [String: AnyObject]()
        uploadPhotoInfo[Constants.UIKeys.placeholderText] = "Upload Car images".localizedString() as AnyObject
        uploadPhotoInfo[Constants.UIKeys.labelAlignment] = NSTextAlignment.left as AnyObject
        let uploadPhotoCell = CellInfo(cellType: .BottomLabelCell, placeHolder: "", value: "", info: uploadPhotoInfo, height: Constants.CellHeightConstants.height_60)
        array.append(uploadPhotoCell)
        
        //Vehicle Images Cell
        var vehicleImagesCellInfo = [String: AnyObject]()
        vehicleImagesCellInfo[Constants.UIKeys.images] = [""] as AnyObject
        let vehicleImagesCell = CellInfo(cellType: .UploadVehicleImagesCell, placeHolder: "", value: StringConstants.Text.selectCarModel.localizedString(), info: vehicleImagesCellInfo, height: Constants.CellHeightConstants.height_320)
        array.append(vehicleImagesCell)
        
        // Want pickup and drop off cell
        var pickUpInfo = [String: AnyObject]()
        pickUpInfo[Constants.UIKeys.isSelected] = false as AnyObject
        pickUpInfo[Constants.UIKeys.shouldHide] = true as AnyObject
        let pickUpCell = CellInfo(cellType: .NoReserveCell, placeHolder: "I want pick Up and Drop Off".localizedString(), value:"" ,info: pickUpInfo
            , height: Constants.CellHeightConstants.height_50)
        array.append(pickUpCell)
        
        //Location cell
        var recoveryRequestInfo = [String: AnyObject]()
        recoveryRequestInfo[Constants.UIKeys.sourcePlaceHolder] = "Pick Up Location".localizedString() as AnyObject
        recoveryRequestInfo[Constants.UIKeys.destinationPlaceHolder] = "Drop Off Location".localizedString() as AnyObject
        recoveryRequestInfo[Constants.UIKeys.sourceValue] = "" as AnyObject
        recoveryRequestInfo[Constants.UIKeys.destinationValue] = "" as AnyObject
        
        let recoveryRequestCell = CellInfo(cellType: .LocationTextFieldsCell, placeHolder: "", value: "", info: recoveryRequestInfo, height: Constants.CellHeightConstants.height_220)
        array.append(recoveryRequestCell)
        
        return array
    }
    
    func requestTenderAPI(arrData: [CellInfo],param: APIParams, completion: @escaping (Bool)-> Void) {
        
        if self.validateFormParameters(arrData: arrData) {
            self.uploadImages(arrData: arrData) { (uploadedImages) in
                let tenderRequestParams = self.getAPIParams(arrData: arrData, param: param, images: uploadedImages)
                print(tenderRequestParams)
                
                APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderRequestForm(param: tenderRequestParams))) { (response, success) in
                    if success {
                        if let safeResponse =  response as? [String: AnyObject]
                        {
                            completion(success)
                        }
                    }
                }
            }
        }
    }
    
    private func uploadImages(arrData: [CellInfo], completion: @escaping(([String]) -> Void)) {
        
        var imagesToUpload: [UIImage] = []
       
        if let imagesInfo = arrData[1].info {
            if let firstImage = imagesInfo[Constants.UIKeys.firstImg] as? UIImage {
                imagesToUpload.append(firstImage)
            }
            if let secondImage = imagesInfo[Constants.UIKeys.secondImg] as? UIImage {
                imagesToUpload.append(secondImage)
            }
            if let thirdImage = imagesInfo[Constants.UIKeys.thirdImg] as? UIImage {
                imagesToUpload.append(thirdImage)
            }
            if let fourthImage = imagesInfo[Constants.UIKeys.fourthImg] as? UIImage {
                imagesToUpload.append(fourthImage)
            }
            if imagesToUpload.count > 0 {
                S3Manager.sharedInstance.uploadImages(images: imagesToUpload, imageQuality: .medium, progress: nil) { (imageUrls, imageNames, error) in
                    if let uploadedImages = imageUrls as? [String] {
                        completion(uploadedImages)
                    }
                }
            } else {
                completion([])
            }
        }
    }
    
    //    func requestTenderAPI(arrData: [CellInfo],param: APIParams, completion: @escaping (Bool)-> Void) {
    //
    //        if self.validateFormParameters(arrData: arrData) {
    //            let tenderRequestParams = self.getAPIParams(arrData: arrData, param: param)
    //            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .tenderRequestForm(param: tenderRequestParams))) { (response, success) in
    //                if success {
    //                    if let safeResponse =  response as? [String: AnyObject]
    //                    {
    //                        completion(success)
    //                    }
    //                }
    //            }
    //        }
    //    }
    
    func validateFormParameters(arrData: [CellInfo]) -> Bool {
        var isValid = true
        if let willPickDrop = arrData[2].info?[Constants.UIKeys.isSelected] as? Bool, let pickupLocation = arrData[3].info?[Constants.UIKeys.sourceValue] as? String, (willPickDrop && pickupLocation.isEmpty)  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide your pickup location.".localizedString())
            isValid = false
        } else if let willPickDrop = arrData[2].info?[Constants.UIKeys.isSelected] as? Bool, let dropOffLocation = arrData[3].info?[Constants.UIKeys.destinationValue] as? String, (willPickDrop && dropOffLocation.isEmpty)  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please provide your drop off location.".localizedString())
            isValid = false
        }
        return isValid
    }
    
    func getAPIParams(arrData: [CellInfo],param: APIParams,images: [String]) -> APIParams {
        var params: APIParams = APIParams()
        params = param
        params[ConstantAPIKeys.images] = images as AnyObject
        params[ConstantAPIKeys.pickupLocation] = arrData[3].info?[Constants.UIKeys.sourceValue] as? String == "" ? "<null>" as AnyObject : arrData[3].info?[Constants.UIKeys.sourceValue] as AnyObject
        params[ConstantAPIKeys.pickupLocationLatitude] =  arrData[3].info?[Constants.UIKeys.sourceLatitude] as AnyObject
        params[ConstantAPIKeys.pickupLocationLongitude] =  arrData[3].info?[Constants.UIKeys.sourceLongitude] as AnyObject
        params[ConstantAPIKeys.dropLocation] = arrData[3].info?[Constants.UIKeys.destinationValue] as? String == "" ? "<null>" as AnyObject : arrData[3].info?[Constants.UIKeys.destinationValue] as AnyObject
        params[ConstantAPIKeys.dropLocationLatitude] =  arrData[3].info?[Constants.UIKeys.destinationLatitude] as AnyObject
        params[ConstantAPIKeys.dropLocationLongitude] =  arrData[3].info?[Constants.UIKeys.destinationLongitude] as AnyObject
       // params[ConstantAPIKeys.images] =  arrData[1].info?[ConstantAPIKeys.images] as AnyObject
        return params
    }
    
}
