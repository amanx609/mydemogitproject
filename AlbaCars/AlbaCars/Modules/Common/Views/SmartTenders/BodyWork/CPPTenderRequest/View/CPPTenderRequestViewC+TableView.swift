//
//  CPPTenderRequestViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 2/26/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension CPPTenderRequestViewC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tenderRequestDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.tenderRequestDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let isPickUpDropOff =  self.tenderRequestDataSource[17].info?[Constants.UIKeys.isSelected] as? Bool {
            if isPickUpDropOff {
                return self.tenderRequestDataSource[indexPath.row].height
            } else {
                let cellInfo = self.tenderRequestDataSource[indexPath.row]
                guard let cellType = cellInfo.cellType else { return Constants.CellHeightConstants.height_0 }
                if cellType == .LocationTextFieldsCell {
                    return Constants.CellHeightConstants.height_0
                }
            }
        }
        return self.tenderRequestDataSource[indexPath.row].height
    }
}

extension CPPTenderRequestViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .LocationTextFieldsCell:
            let cell: LocationTextFieldsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.targetViewController = self
            cell.configureView(cellInfo: cellInfo)
            cell.delegate = self
            return cell
        case .DateRangeCell:
            let cell: DateRangeCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureAuctionConfirmationViewCell(cellInfo: cellInfo)
            return cell
            
        case .TextInputCell:
            let cell: TextInputCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .DropDownCell:
            let cell: DropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureViewForRequestForm(cellInfo: cellInfo)
            return cell
            
        case .TextViewCell:
            let cell: TextViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureViewWithBlackPlaceholder(cellInfo: cellInfo)
            return cell
            
        case .ImageDropDownCell:
            let cell: ImageDropDownCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .NoReserveCell:
            let cell: NoReserveCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - DropDownCellDelegate
extension CPPTenderRequestViewC: DateRangeCellDelegate {
    
    func didTapFromDate(cell: DateRangeCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.selectedIndexPath = indexPath
        self.isDate = true
        self.setupDateTimePicker(cell.fromTextField)
    }
    
    func didTapToDate(cell: DateRangeCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.selectedIndexPath = indexPath
        self.isDate = false
        self.setupDateTimePicker(cell.toTextField)
    }
}

//MARK: - TextInputCellDelegate
extension CPPTenderRequestViewC: TextInputCellDelegate {
    func tapNextKeyboard(cell: TextInputCell) {
        self.view.endEditing(true)
    }
    
    func didChangeText(cell: TextInputCell, text: String) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].value = text
    }
}

//MARK: - DropDownCellDelegate
extension CPPTenderRequestViewC: DropDownCellDelegate {
    func didTapDropDownCell(cell: DropDownCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.showDropDownAt(indexPath: indexPath)
    }
}

//MARK: - ImageDropDownCellDelegate
extension CPPTenderRequestViewC: ImageDropDownCellDelegate {
    func didTapImageDropDownCell(cell: ImageDropDownCell) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.showDropDownAt(indexPath: indexPath)
    }
}

//MARK: - NoReserveCellCellDelegate
extension CPPTenderRequestViewC: NoReserveCellCellDelegate {
    
    func didTapNoReserve(cell: NoReserveCell, isReserved: Bool) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.isSelected] = isReserved as AnyObject
        self.view.endEditing(true)
        self.tenderRequestTableView.reloadData()
        
    }
}

//MARK: - LocationTextFieldCellDelegate
extension CPPTenderRequestViewC: LocationTextFieldsCellDelegate {
    func didChangeSourceText(cell: LocationTextFieldsCell, text: String, latitude: Double, longitude: Double) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.sourceValue] = text as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.sourceLatitude] = latitude as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.sourceLongitude] = longitude as AnyObject
    }
    
    func didChangeDestinationText(cell: LocationTextFieldsCell, text: String, latitude: Double, longitude: Double) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.destinationValue] = text as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.destinationLatitude] = latitude as AnyObject
        self.tenderRequestDataSource[indexPath.row].info?[Constants.UIKeys.destinationLongitude] = longitude as AnyObject
    }
    
    func tapNextKeyboard(cell: LocationTextFieldsCell) {
        self.view.endEditing(true)
    }
}

//MARK: - TextViewCellDelegate
extension CPPTenderRequestViewC: TextViewCellDelegate {
    func didChangeText(cell: TextViewCell, text: String) {
        guard let indexPath = self.tenderRequestTableView.indexPath(for: cell) else { return }
        self.tenderRequestDataSource[indexPath.row].value = text
    }
}

