//
//  DentDescriptionCell.swift
//  AlbaCars
//
//  Created by Narendra on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class DentDescriptionCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    
    //MARK: - IBOutlets
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Public Methods
      func configureView(cellInfo: CellInfo) {
        self.countLabel.text = cellInfo.placeHolder
        let trimmed = cellInfo.value.trimmingCharacters(in: .whitespacesAndNewlines)
        self.titleLabel.text = trimmed
      }
    
}
