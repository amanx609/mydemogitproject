//
//  PPTenderDentRequestViewM.swift
//  AlbaCars
//
//  Created by Narendra on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol PPTenderDentRequestViewModeling: BaseVModeling {
    func getReqDataSource() -> [CellInfo]
    func getAPIParams(arrData: [CellInfo],param: APIParams) -> APIParams
}

class PPTenderDentRequestViewM: BaseViewM, PPTenderDentRequestViewModeling {
    
    func getReqDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        // Image
        let addAuctionImageCellCell = CellInfo(cellType: .DAddAuctionImageCell, placeHolder: "", value:"" ,info: nil, height: Constants.Devices.ScreenHeight - 250)
        array.append(addAuctionImageCellCell)
        
        return array
    }
    
    func getAPIParams(arrData: [CellInfo],param: APIParams) -> APIParams {
        var params: APIParams = APIParams()
        params = param
        var dentDescription = [String]()
        
        for cellInfo in arrData {
            switch cellInfo.cellType {
            case .DentDescriptionCell:
                dentDescription.append(Helper.toString(object: cellInfo.value))
            default:
                print("")
            }
        }
        
        params[ConstantAPIKeys.dentDescription] = dentDescription as AnyObject
        params[ConstantAPIKeys.noOfDents] = arrData.count - 1 as AnyObject

        return params
    }
    
}
