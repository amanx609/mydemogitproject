//
//  PPTenderDentRequestViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension PPTenderDentRequestViewC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tenderRequestDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.tenderRequestDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tenderRequestDataSource[indexPath.row].height
    }
}

extension PPTenderDentRequestViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .DentDescriptionCell:
            let cell: DentDescriptionCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .DAddAuctionImageCell:
            let cell: DAddAuctionImageCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureTenderDentView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

// DAddAuctionImageCellDelegate
extension PPTenderDentRequestViewC: DAddAuctionImageCellDelegate {
    
    func didTapDentDescription(cell: DAddAuctionImageCell, text: String, count: Int) {
        let dentDescriptionCell = CellInfo(cellType: .DentDescriptionCell, placeHolder: Helper.toString(object: count), value:text ,info: nil, height: UITableView.automaticDimension)
        self.tenderRequestDataSource.append(dentDescriptionCell)
        self.tenderRequestTableView.reloadData()
    }

}
