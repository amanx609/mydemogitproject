//
//  PPTenderDentRequestViewC.swift
//  AlbaCars
//
//  Created by Narendra on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class PPTenderDentRequestViewC:  BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tenderRequestTableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    
    //Variables
    var viewModel: PPTenderDentRequestViewModeling?
    var tenderRequestDataSource: [CellInfo] = []
    var params: APIParams = APIParams()
    var bodyWorkOptions: BodyWorkOptions = .panelPainting
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupNavigationBarTitle(title:  bodyWorkOptions == .panelPainting ? "Panel Painting Tender".localizedString() : "Paintless Dent Removal Tender".localizedString(),barColor: .white,titleColor: .black, leftBarButtonsType: [.back], rightBarButtonsType: [])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    
    //MARK: - Private Methods
    private func setup() {
        
        self.registerNibs()
        self.setupViewModel()
        self.setupView()
        self.setupTableView()
        self.loadDataSource()
        
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.nextButton.setTitle("Next".localizedString(), for: .normal)
    }
    
    private func setupTableView() {
        self.tenderRequestTableView.separatorStyle = .none
        self.tenderRequestTableView.backgroundColor = UIColor.white
        self.tenderRequestTableView.delegate = self
        self.tenderRequestTableView.dataSource = self
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = PPTenderDentRequestViewM()
        }        
    }
    
    private func registerNibs() {
        self.tenderRequestTableView.register(DAddAuctionImageCell.self)
        self.tenderRequestTableView.register(DentDescriptionCell.self)
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getReqDataSource() {
            self.tenderRequestDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.tenderRequestTableView.reloadData()
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func tapNextButton(_ sender: Any) {
        
        if self.tenderRequestDataSource.count  > 1 {
            
            if let apiParams = self.viewModel?.getAPIParams(arrData:self.tenderRequestDataSource, param: params) {
                let tendersViewC = DIConfigurator.sharedInstance.getPPTenderRequestUploadImageVC()
                tendersViewC.params = apiParams
                tendersViewC.bodyWorkOptions = self.bodyWorkOptions
                self.navigationController?.pushViewController(tendersViewC, animated: true)
            }
        } else {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please add car description by tapping on car.".localizedString())
        }
        
    }
}

