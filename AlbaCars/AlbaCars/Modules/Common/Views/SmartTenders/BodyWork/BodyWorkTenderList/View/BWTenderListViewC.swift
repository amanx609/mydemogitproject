//
//  BWTenderViewC.swift
//  AlbaCars
//
//  Created by Narendra on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class BWTenderListViewC:  BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tenderStatusTableView: UITableView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var createTenderButton: UIButton!
    
    //MARK: - Variables
    var tenderStatusDataSource: [AllTendersModel] = []
    var selectedIndex = 0
    var tenderListViewModel: AllTendersVModeling?
    var nextPageNumber = 1
    var tenderStatus: TenderStatus = .all
    var perPage = Constants.Validations.perPage
    var durationTimer : Timer?
    
    //MARK: - LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        var title = ""
        if tenderStatus == .all {
            title = ChooseServiceType.bodyWork.title
        } else {
            title = self.tenderStatus.title
        }
        self.setupNavigationBarTitle(title: title, barColor: .white,titleColor: .blackTextColor, leftBarButtonsType: [.back], rightBarButtonsType: [])
    }
    
    deinit {
        print("deinit BWTenderListViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        
        self.setupView()
        self.recheckVM()
        self.registerNibs()
        self.setupTableView()
        self.requestTenderListAPI()
    }
    
    private func setupTimer() {
        self.invalidateTimer()
        self.durationTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimeValueInDataSource), userInfo: nil, repeats: true)
    }
    
    private func invalidateTimer() {
        self.durationTimer?.invalidate()
        self.durationTimer = nil
    }
    
    
    private func recheckVM() {
      
        if self.tenderListViewModel == nil {
            self.tenderListViewModel = AllTendersViewM()
        }
    }
    
    private func setupTableView() {
        self.tenderStatusTableView.delegate = self
        self.tenderStatusTableView.dataSource = self
        self.tenderStatusTableView.separatorStyle = .none
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.createTenderButton.setTitle(StringConstants.Text.createTender.localizedString(), for: .normal)
    }
    
    
    private func registerNibs() {
        self.tenderStatusTableView.register(ServicingTenderListCell.self)
    }
    
    //MARK: - Selector Methods
    @objc func updateTimeValueInDataSource() {
        var index = 0
        for tenderData in self.tenderStatusDataSource {
            switch self.tenderStatus {
            //UpcomingTender
            case .upcomingTenders:
                if let timerDuration = tenderData.startTimerDuration {
                    tenderData.startTimerDuration = timerDuration - 1
                    //RemoveDataIfDurationIsFinished
                    if timerDuration == 0 {
                        self.tenderStatusDataSource.remove(at: index)
                        continue
                    }
                    self.tenderStatusDataSource[index] = tenderData
                    index += 1
                }
            //InProgress
            case .inProgress, .noBidAccepted:
                if let timerDuration = tenderData.endTimerDuration {
                    tenderData.endTimerDuration = timerDuration - 1
                    //RemoveDataIfDurationIsFinished
                    if timerDuration == 0 {
                        self.tenderStatusDataSource.remove(at: index)
                        continue
                    }
                    self.tenderStatusDataSource[index] = tenderData
                    index += 1
                }
            default:
                break
            }
        }
        self.tenderStatusTableView.reloadData()
    }
    
    
    func requestTenderListAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        params[ConstantAPIKeys.type] = RecoveryHomeScreenType.bodyWork.rawValue as AnyObject
        params[ConstantAPIKeys.status] = self.tenderStatus.rawValue as AnyObject
        self.tenderListViewModel?.requestTenderListAPI(params: params, completion: { [weak self] (nextPageNumber, perPage, tenderList) in
            guard let sSelf = self else { return }
            
            sSelf.tenderStatusDataSource = sSelf.tenderStatusDataSource + tenderList
            if sSelf.nextPageNumber == 1 {
                sSelf.setupTimer()
            }
            sSelf.nextPageNumber = nextPageNumber
            sSelf.perPage = perPage
            Threads.performTaskInMainQueue {
                sSelf.tenderStatusTableView.reloadData()
            }
        })
    }
    
    //MARK: - IBActions
    @IBAction func tapCreateTenderButton(_ sender: Any) {
        let chooseCarForServiceVC = DIConfigurator.sharedInstance.getChooseCarForServiceVC()
        chooseCarForServiceVC.chooseCarForServiceType = .bodyWork
        self.navigationController?.pushViewController(chooseCarForServiceVC, animated: true)
    }
}
