//
//  BWTenderViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 2/25/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//MARK:- UITableView Delegates & Datasource Methods
extension BWTenderListViewC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tenderStatusDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        getCell(tableView, indexPath: indexPath, tenderListData: self.tenderStatusDataSource[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.CellHeightConstants.height_230
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tender = self.tenderStatusDataSource[indexPath.row]
        switch tender.tenderStatus() {
        case .bidAccepted, .completedTenders:
            let recoveryRequestViewC = DIConfigurator.sharedInstance.getBWBidAcceptedVC()
            recoveryRequestViewC.tenderId = Helper.toInt(self.tenderStatusDataSource[indexPath.row].id)
            recoveryRequestViewC.tenderStatus = tender.tenderStatus()
            recoveryRequestViewC.screenType = tender.getBodyWorkType()
            self.navigationController?.pushViewController(recoveryRequestViewC, animated: true)
        default:
            let recoveryRequestViewC = DIConfigurator.sharedInstance.getBWBidListVC()
            recoveryRequestViewC.tenderId = Helper.toInt(self.tenderStatusDataSource[indexPath.row].id)
            recoveryRequestViewC.tenderStatus = tender.tenderStatus()
            recoveryRequestViewC.screenType = tender.getBodyWorkType()
            self.navigationController?.pushViewController(recoveryRequestViewC, animated: true)
        }
       
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      guard let lastVisibleRow = self.tenderStatusTableView.indexPathsForVisibleRows?.last?.row else { return }
      if (self.tenderStatusDataSource.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
        self.requestTenderListAPI()
      }
    }
}

extension BWTenderListViewC {
    func getCell(_ tableView: UITableView, indexPath: IndexPath, tenderListData: AllTendersModel) -> UITableViewCell {
        let cell: ServicingTenderListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureViewForBW(tenderModel: tenderListData, smartTenderType: .bodyWork)
        return cell
    }
}
