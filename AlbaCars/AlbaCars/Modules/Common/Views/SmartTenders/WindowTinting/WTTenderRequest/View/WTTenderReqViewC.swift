//
//  WTTenderReqViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class WTTenderReqViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tenderRequestTableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet var dateTimePicker: UIDatePicker!
    
    //Variables
    var viewModel: WTRequestVModeling?
    var tenderRequestDataSource: [CellInfo] = []
    var isDate = true
    var vehicleId: Int?
    var requestCarDataSource: [CellInfo] = []
    var carDetails: ChooseCarModel?
    var selectedIndexPath = IndexPath()

    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupNavigationBarTitle(title: ChooseCarForServiceType.windowTinting.title ?? "",barColor: .white,titleColor: .black, leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.registerNibs()
        self.setupViewModel()
        self.setupView()
        self.setupTableView()
        self.loadDataSource()
    }
    
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        }
        self.submitButton.setTitle("Submit".localizedString(), for: .normal)
    }
    
    private func setupTableView() {
        self.tenderRequestTableView.separatorStyle = .none
        self.tenderRequestTableView.backgroundColor = UIColor.white
        self.tenderRequestTableView.delegate = self
        self.tenderRequestTableView.dataSource = self
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = WTRequestViewM()
        }
    }
    
    private func registerNibs() {
        self.tenderRequestTableView.register(LocationTextFieldsCell.self)
        self.tenderRequestTableView.register(DropDownCell.self)
        self.tenderRequestTableView.register(DateRangeCell.self)
        self.tenderRequestTableView.register(BottomLabelCell.self)
        self.tenderRequestTableView.register(ImageDropDownCell.self)
        self.tenderRequestTableView.register(NoReserveCell.self)
        self.tenderRequestTableView.register(TextViewCell.self)
        self.tenderRequestTableView.register(TextInputCell.self)
        self.tenderRequestTableView.register(WheelSizeInfoCell.self)
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getWTTenderReqDataSource(carDetails: self.carDetails) {
            self.tenderRequestDataSource = dataSource
            Threads.performTaskInMainQueue {
                self.tenderRequestTableView.reloadData()
            }
        }
    }
    
    private func updateDateTime(_ pickerValue: String) {
        if isDate {
            self.tenderRequestDataSource[self.selectedIndexPath.row].placeHolder = pickerValue
        } else {
            self.tenderRequestDataSource[self.selectedIndexPath.row].value = pickerValue
        }
        
        self.tenderRequestTableView.reloadData()
    }
    
    //MARK: - Public Methods
    func setupDateTimePicker(_ textField: UITextField) {
        textField.inputView = self.dateTimePicker
        textField.inputAccessoryView = Helper.getDoubleButtonToolBar(leftButtonTitle: StringConstants.Text.Cancel.localizedString(), rightButtonTitle: StringConstants.Text.done.localizedString(), leftButtonselector: #selector(self.cancelButtonTapped), rightButtonSelector: #selector(self.doneButtonTapped), target: self)
        if isDate {
            self.dateTimePicker.datePickerMode = .date
        } else {
            self.dateTimePicker.datePickerMode = .time
        }
        self.dateTimePicker.minimumDate = Date()
    }
    
    func setupToolBar(_ textField: UITextField) {
        textField.inputAccessoryView = Helper.getSingleButtonToolBar(buttonTitle: StringConstants.Text.done.localizedString(), selector: #selector(self.doneButtonTapped), target: self)
    }
    
    func showDropDownAt(indexPath: IndexPath) {
        self.view.endEditing(true)
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        switch indexPath.row {
            
        case 6:
            listPopupView.initializeViewWith(title: DropDownType.tintPercentage.title, arrayList: DropDownType.tintPercentage.dataList, key: DropDownType.tintPercentage.rawValue) { [weak self] (response) in
                guard let sSelf = self,
                    let tintPercentage = response[DropDownType.tintPercentage.rawValue] as? String else { return }
                sSelf.tenderRequestDataSource[indexPath.row].value = tintPercentage
                sSelf.tenderRequestTableView.reloadRows(at: [indexPath], with: .none)
                
            }
        
        case 13:
            listPopupView.initializeViewWith(title: "Select Time Duration", arrayList: Constants.tenderDuration, key: Constants.UIKeys.duration) { [weak self] (response) in
                guard let sSelf = self,
                    let timeDuration = response[ConstantAPIKeys.duration] as? String, let apiValue = response[Constants.UIKeys.value] as? String else { return }
                
                sSelf.tenderRequestDataSource[indexPath.row].value = timeDuration
                sSelf.tenderRequestDataSource[indexPath.row].placeHolder = apiValue
                sSelf.tenderRequestTableView.reloadRows(at: [indexPath], with: .none)
            }
            
        default:
            break
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
    //MARK: - API Methods
    
    func requestTenderAPI() {
        self.viewModel?.requestTenderAPI(arrData: self.tenderRequestDataSource, vehicleId: vehicleId ?? 0, completion: { [weak self] (success) in
            guard let sSelf = self else { return }
            if let viewControllers = sSelf.navigationController?.viewControllers {
                //CreateAccountViewCIsPresentInNavigationStack
                for viewController in viewControllers {
                    if viewController is RecoveryHomeViewC {
                        sSelf.navigationController?.popToViewController(viewController, animated: true)
                        return
                    }
                }
                
            }
        })
    }
    
    //MARK: - Selectors
    @objc func cancelButtonTapped() {
        self.view.endEditing(true)
    }
    
    @objc func doneButtonTapped() {
        self.view.endEditing(true)
        let selectedDate = self.dateTimePicker.date
        var convertedValue = ""
        if isDate {
            convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.dateFormatWithoutSpace)
        } else {
            convertedValue = selectedDate.dateStringWith(strFormat: Constants.Format.timeFormathmma)
           // self.tenderRequestDataSource[self.selectedIndexPath.row].info?[Constants.UIKeys.secondValue] = selectedDate.dateStringWith(strFormat: Constants.Format.timeFormathhmmss) as AnyObject
        }
        updateDateTime(convertedValue)
    }
    
    
    
    //MARK: - IBActions
    @IBAction func tapSubmitButton(_ sender: Any) {
        self.requestTenderAPI()
        
    }
}
