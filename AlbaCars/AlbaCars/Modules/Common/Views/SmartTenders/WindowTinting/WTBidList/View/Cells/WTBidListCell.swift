//
//  WTBidListCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/19/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol WTBidListCellDelegate: class {
    func didTapCheck(cell: WTBidListCell)
    func didTapCross(cell: WTBidListCell)
}


class WTBidListCell: BaseTableViewCell, NibLoadableView, ReusableView {
    //MARK: - IBOutlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var agencyImageView: UIImageView!
    @IBOutlet weak var agencyNameLabel: UILabel!
    @IBOutlet weak var agencyRatingView: RatingView!
    @IBOutlet weak var tintColorLabel: UILabel!
    @IBOutlet weak var deliveryTimeLabel: UILabel!
    @IBOutlet weak var tintVisibilityLabel: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var vatLabel: UILabel!

    //MARK: - Variables
    weak var delegate: WTBidListCellDelegate?
    var bidId: Int?
    var bidAmount: Double?
    
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.shadowView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        self.shadowView.addShadow(ofColor:  UIColor.shadowColor, radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        self.containerView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.priceView.roundCorners([.bottomLeft, .bottomRight], radius: Constants.UIConstants.sizeRadius_16)
        agencyImageView.layer.cornerRadius = agencyImageView.frame.height/2

    }
    
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            
            if let image = info[Constants.UIKeys.image] as? String, !image.isEmpty {
                self.agencyImageView.setImage(urlStr: Helper.toString(object: image), placeHolderImage: #imageLiteral(resourceName: "user_placeholder"))
            }
            
            self.agencyNameLabel.text = Helper.toString(object: info[ConstantAPIKeys.name])
            self.vatLabel.text = Helper.toString(object: info[ConstantAPIKeys.vatAmmount])
            
            self.agencyRatingView.configureView(rating: Helper.toInt(info[ConstantAPIKeys.rating]))
            self.tintColorLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.tintColorTxt.localizedString(), secondText: Helper.toString(object: info[Constants.UIKeys.tintColor]), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            let visibilityPercentage = Helper.toString(object: info[Constants.UIKeys.visibilityPercentage]) == "" ? "N/A" : "\(Helper.toString(object: info[Constants.UIKeys.visibilityPercentage]))%"
            self.tintVisibilityLabel.attributedText = String.getAttributedText(firstText: StringConstants.Text.tintVisibilityTxt.localizedString(), secondText: visibilityPercentage, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            let days = Helper.toInt(info[Constants.UIKeys.time])
            var daysStr = ""
            if days == 0 || days == 1 {
                daysStr = "\(days) day"
                
            } else {
                daysStr = "\(days) days"
            }
            self.deliveryTimeLabel.attributedText = String.getAttributedText(firstText: "Time Needed For Job : ".localizedString(), secondText: daysStr, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            self.priceLabel.text = String.appendText(obj: Helper.toString(object: info[Constants.UIKeys.bidAmount]), text: StringConstants.Text.Currency.localizedString())
        }
        
        
    }
    
    //MARK: - IBActions
       @IBAction func tapCross(_ sender: UIButton) {
           if let delegate = self.delegate {
               delegate.didTapCross(cell: self)
           }
           
       }
       
       @IBAction func tapCheck(_ sender: UIButton) {
           if let delegate = self.delegate {
               delegate.didTapCheck(cell: self)
           }
       }
}
