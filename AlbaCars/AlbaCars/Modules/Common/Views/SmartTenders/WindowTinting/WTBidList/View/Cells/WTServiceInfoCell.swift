//
//  WTServiceInfoCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/19/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class WTServiceInfoCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var ServiceInfoLabel: UILabel!
    @IBOutlet weak var tintColorLabel: UILabel!
    @IBOutlet weak var tintVisibilityLabel: UILabel!
    @IBOutlet weak var infoDetailLabel: UILabel!
    @IBOutlet weak var additionalInfoLabel: UILabel!
    @IBOutlet weak var numOfWindowsLabel: UILabel!
    @IBOutlet weak var numWindowsBgView: UIView!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Public Methods
    func configureViewWT(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            //serviceType
            self.numWindowsBgView.isHidden = true
            self.tintColorLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.tintColorTxt.localizedString()) ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.tintColor]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
             let visibilityPercentage = Helper.toString(object: info[Constants.UIKeys.visibilityPercentage]) == "" ? Helper.toString(object: info[Constants.UIKeys.visibilityPercentage]) : "\(Helper.toString(object: info[Constants.UIKeys.visibilityPercentage]))%"
            
            self.tintVisibilityLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.tintVisibilityTxt.localizedString()) ", secondText: visibilityPercentage, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            
            self.infoDetailLabel.text = Helper.toString(object: info[Constants.UIKeys.additionalInformation])
            self.additionalInfoLabel.text = "Additional Information".localizedString()
        }
    }
    
    func configureViewBV(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            //serviceType
            self.ServiceInfoLabel.text = "Valuation Details".localizedString()
            self.numWindowsBgView.isHidden = false
           // let price = Helper.toString(object: info[Constants.UIKeys.visibilityPercentage]) == "" ? Helper.toString(object: info[Constants.UIKeys.visibilityPercentage]) : "AED \(Helper.toString(object: info[Constants.UIKeys.visibilityPercentage]))"
            
            
            self.infoDetailLabel.text = Helper.toString(object: info[Constants.UIKeys.additionalInformation])
            self.additionalInfoLabel.text = "Additional Information".localizedString()
            
            if let apiDetails = info[Constants.UIKeys.cellInfo] as? RDBidListModel {
            
                self.tintColorLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.carMileageText.localizedString()) ", secondText: " \(Helper.toString(object: apiDetails.mileage)) Km", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
                
                self.tintVisibilityLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.requestedCar.localizedString()) ", secondText: "AED  \(Helper.toString(object: apiDetails.carPrice))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)

                self.numOfWindowsLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.bankName.localizedString()) ", secondText: Helper.toString(object: apiDetails.bankName), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)

            }
        }
    }
    
}
