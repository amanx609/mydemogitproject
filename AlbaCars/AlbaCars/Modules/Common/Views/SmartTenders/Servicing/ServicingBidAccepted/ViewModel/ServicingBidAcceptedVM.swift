//
//  ServicingBidAcceptedVM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol ServicingBidAcceptedVModeling: BaseVModeling {
    func getServicingDataSource(tenderDetails: RDBidListModel) -> [CellInfo]
    
}

class ServicingBidAcceptedVM: BaseViewM, ServicingBidAcceptedVModeling {
    
    
    func getServicingDataSource(tenderDetails: RDBidListModel) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //CarDetailCell
        var carDetailInfo = [String: AnyObject]()
        carDetailInfo[Constants.UIKeys.cellInfo] = tenderDetails
        carDetailInfo[Constants.UIKeys.isSecondDetailPage] = true as AnyObject
        let carDetailCell = CellInfo(cellType: .STCarDetailCell, placeHolder: "", value: "", info: carDetailInfo, height: Constants.CellHeightConstants.height_240)
        array.append(carDetailCell)
        
        
        var serviceDetailInfo = [String: AnyObject]()
        serviceDetailInfo[Constants.UIKeys.serviceType] = tenderDetails.getServicingType() as AnyObject
        let frontBrakePads = Helper.toBool(tenderDetails.getServiceStatus(value: Helper.toString(object:ServiceOptions.frontBrakePads.rawValue)))
        serviceDetailInfo[Constants.UIKeys.frontBrake] = frontBrakePads == true ? "Yes".localizedString() as AnyObject : "No".localizedString() as AnyObject
        let rearBrakePads = Helper.toBool(tenderDetails.getServiceStatus(value: Helper.toString(object:ServiceOptions.rearBrakePads.rawValue)))
        serviceDetailInfo[Constants.UIKeys.rearBrake] = rearBrakePads == true ? "Yes".localizedString() as AnyObject : "No".localizedString() as AnyObject
        let brakeDiscsPads = Helper.toBool(tenderDetails.getServiceStatus(value: Helper.toString(object:ServiceOptions.brakeDiscs.rawValue)))
        serviceDetailInfo[Constants.UIKeys.brakeDiscs] = brakeDiscsPads == true ? "Yes".localizedString() as AnyObject : "No".localizedString() as AnyObject
        serviceDetailInfo[Constants.UIKeys.vinNumber] = tenderDetails.vinNumber as AnyObject
        serviceDetailInfo[Constants.UIKeys.engineSize] = tenderDetails.getEngineSize() as AnyObject
        serviceDetailInfo[Constants.UIKeys.noOfCylinders] = tenderDetails.cylinders as AnyObject
        
        let serviceDetailCell = CellInfo(cellType: .ServiceDetailCell, placeHolder: "", value: "", info: serviceDetailInfo, height: Constants.CellHeightConstants.height_170)
        array.append(serviceDetailCell)
        
        //BidsCountCell
        var bidsCountCellInfo = [String: AnyObject]()
        bidsCountCellInfo[Constants.UIKeys.isSecondDetailPage] = true as AnyObject
        let bidsCountCell = CellInfo(cellType: .STBidsCountCell, placeHolder: "", value: "", info: bidsCountCellInfo, height: Constants.CellHeightConstants.height_55)
        array.append(bidsCountCell)
        
        //Bid Detail cell
        var bidDetailCellInfo = [String: AnyObject]()
        bidDetailCellInfo[Constants.UIKeys.serviceType] = tenderDetails.getServicingType() as AnyObject
        bidDetailCellInfo[Constants.UIKeys.servicePrice] = Helper.toString(object: tenderDetails.Bids?.amount) as AnyObject
        bidDetailCellInfo[Constants.UIKeys.paymentType] = Helper.toString(object: tenderDetails.Bids?.paymentType) as AnyObject
        bidDetailCellInfo[Constants.UIKeys.transaction] = Helper.toString(object: tenderDetails.Bids?.transactionNo) as AnyObject
        let bidDetailCell = CellInfo(cellType: .BidDetailCell, placeHolder: "", value: "", info: bidDetailCellInfo, height: UITableView.automaticDimension)
        array.append(bidDetailCell)
        
        //service provider detail
        var firstBidCellInfo = [String: AnyObject]()
        firstBidCellInfo[Constants.UIKeys.agencyName] = Helper.toString(object: tenderDetails.driverName) as AnyObject
        firstBidCellInfo[Constants.UIKeys.rating] = tenderDetails.rating as AnyObject
        firstBidCellInfo[Constants.UIKeys.address] = Helper.toString(object: tenderDetails.driverAddress) as AnyObject
        firstBidCellInfo[Constants.UIKeys.driverImage] = Helper.toString(object: tenderDetails.driverImage) as AnyObject
        
        let firstBidCell = CellInfo(cellType: .SPDetailCell, placeHolder: "", value: "", info: firstBidCellInfo, height: Constants.CellHeightConstants.height_110)
        array.append(firstBidCell)
        
        //Contact Info Cell
        
         let secondBidCell = CellInfo(cellType: .VIContactInfoCell, placeHolder: "+971 \(Helper.toString(object: tenderDetails.driverMobile))", value:Helper.toString(object: tenderDetails.driverEmail), info: nil, height: Constants.CellHeightConstants.height_120)
         array.append(secondBidCell)
        
        
        return array
    }
}
