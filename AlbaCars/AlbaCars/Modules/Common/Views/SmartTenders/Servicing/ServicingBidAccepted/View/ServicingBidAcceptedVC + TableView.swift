//
//  ServicingBidAcceptedVC + TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension ServicingBidAcceptedVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.inspectionDetailDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.inspectionDetailDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.inspectionDetailDataSource[indexPath.row].height
    }
}

extension ServicingBidAcceptedVC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .STCarDetailCell:
            let cell: STCarDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .inspectionRequest, tenderStatus: .all)
            return cell
            
        case .RDContactInfoCell:
            let cell: RDContactInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .STBidsCountCell:
            let cell: STBidsCountCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, type: .carServicing)
            return cell
            
        case .BidDetailCell:
            let cell: BidDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureViewForServicing(cellInfo: cellInfo)
            
            return cell
            
        case .ServiceDetailCell:
            let cell: ServiceDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        case .BottomLabelCell:
            let cell: BottomLabelCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        
         case .VIContactInfoCell:
            let cell: VIContactInfoCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
           
        case .SPDetailCell:
        let cell: SPDetailCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(cellInfo: cellInfo)
        return cell
         
            
        default:
            return UITableViewCell()
        }
    }
}

extension ServicingBidAcceptedVC: VIContactInfoCellDelegate {
    
    func didTapPhone(cell: VIContactInfoCell, phone: String) {
        guard let url = URL(string: "tel://" + phone) else { return }
        UIApplication.shared.open(url)
    }
}

extension ServicingBidAcceptedVC: DashedTextFieldCellDelegate {
    func tapNextKeyboard(cell: DashedTextFieldCell) {
        
    }
    
    func didChangeText(cell: DashedTextFieldCell, text: String) {
        guard let indexPath = self.secondDetailTableView.indexPath(for: cell) else { return }
        self.inspectionDetailDataSource[indexPath.row].value  = text
        if text.count == Constants.Validations.dropOffMaxLength {
            //self.requestVerifyDropOffCode()
        }
    }
    
    func callVerifyAPI(cell: DashedTextFieldCell, text: String) {
        //self.requestVerifyDropOffCode()
    }
    
    
}




