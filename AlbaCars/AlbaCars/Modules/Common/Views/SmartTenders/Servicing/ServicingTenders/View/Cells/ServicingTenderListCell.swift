//
//  ServicingTenderListCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/13/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class ServicingTenderListCell: BaseTableViewCell, NibLoadableView, ReusableView {
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bidsView: UIView!
    @IBOutlet weak var bidsLabel: UILabel!
    @IBOutlet weak var bidsCountLabel: UILabel!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var timerInfo1Label: UILabel!
    @IBOutlet weak var timerInfo2Label: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var partLabel: UILabel!
    @IBOutlet weak var tenderStatusButton: UIButton!
    @IBOutlet weak var completedImageView: UIImageView!
    @IBOutlet weak var timerLabelsBgView: UIView!
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var wheelSizeInfoLabel: UILabel!
    @IBOutlet weak var wheelSizeBgView: UIView!
    
    
    //MARK: - Life Cycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK: - Private Methods
    private func setupTimerAndBanner(tenderModel: AllTendersModel) {
        switch tenderModel.tenderStatus() {
        case .inProgress:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
            self.timerInfo1Label.isHidden = false
            if let timerDuration = tenderModel.endTimerDuration {
                self.timerInfo1Label.text = timerDuration.convertToTimerFormat()
            }
            self.timerInfo1Label.textColor = .red
            self.timerInfo2Label.isHidden = true
            self.timerInfo2Label.text = ""
            self.tenderStatusButton.backgroundColor = .greenColor
            
        case .bidAccepted:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
            self.timerLabelsBgView.isHidden = true
            self.tenderStatusButton.setTitle(StringConstants.Text.tenderBidAccepted.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .yellowColor
            
        case .noBidAccepted:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
            self.timerLabelsBgView.isHidden = false
            self.timerInfo1Label.isHidden = false
            self.timerInfo1Label.text = "00:00:00 \(StringConstants.Text.tenderComplete.localizedString())"
            self.timerInfo1Label.textColor = .grayTextColor
            if let timerDuration = tenderModel.endTimerDuration {
                self.timerInfo2Label.attributedText =  String.getAttributedText(firstText: timerDuration.convertToTimerFormat(), secondText: " \(StringConstants.Text.toAcceptBid.localizedString())", firstTextColor: .redButtonColor, secondTextColor: .greenTopHeaderColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
            }
            self.tenderStatusButton.setTitle(StringConstants.Text.tenderNoBidsAccepted.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .blackColor
            
        case .allBidsRejected:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
            self.timerLabelsBgView.isHidden = false
            self.timerInfo1Label.isHidden = false
            self.timerInfo1Label.text = "00:00:00 \(StringConstants.Text.tenderComplete.localizedString())"
            self.timerInfo1Label.textColor = .grayTextColor
            self.timerInfo2Label.text = "00:00:00 \(StringConstants.Text.timeElapsed.localizedString())"
            self.timerInfo1Label.textColor = .grayTextColor
            self.tenderStatusButton.setTitle(StringConstants.Text.tenderAllBidsRejected.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .redButtonColor
            
        case .upcomingTenders:
            self.completedImageView.isHidden = true
            self.tenderStatusButton.isHidden = false
            self.timerLabelsBgView.isHidden = false
            
            if let timerDuration = tenderModel.startTimerDuration {
                self.timerInfo1Label.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.tenderStartsIn.localizedString())", secondText: " \(timerDuration.convertToTimerFormat())", firstTextColor: .grayTextColor, secondTextColor: .greenTopHeaderColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Book)
            }
            self.timerInfo2Label.attributedText =  String.getAttributedText(firstText:"\(StringConstants.Text.tenderDateTime.localizedString())", secondText: " \(Helper.toString(object:tenderModel.getDateTime()))", firstTextColor: .black, secondTextColor: .greenTopHeaderColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Book)
            self.tenderStatusButton.setTitle(StringConstants.Text.tenderUpcoming.localizedString(), for: .normal)
            self.tenderStatusButton.backgroundColor = .blueTenderButtonColor
            
        case .completedTenders:
            self.timerLabelsBgView.isHidden = true
            self.tenderStatusButton.isHidden = true
            self.completedImageView.isHidden = false
            
        default:
            break
        }
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.timerInfo1Label.text = ""
        self.timerInfo2Label.text = ""
        self.bidsLabel.text = StringConstants.Text.bids.localizedString()
        Threads.performTaskInMainQueue {
            self.bidsView.roundCorners([.bottomLeft, .bottomRight], radius: Constants.UIConstants.sizeRadius_14)
            self.bgView.roundCorners(Constants.UIConstants.sizeRadius_7)
            self.bgView.addShadow(ofColor: UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
            self.dataView.roundCorners(Constants.UIConstants.sizeRadius_7)
        }
    }
    
    //MARK: - Public Methods
    func configureViewForServicing(tenderModel: AllTendersModel, smartTenderType: ChooseServiceType) {
        //setting description labels
        self.bidsCountLabel.text = Helper.toString(object: tenderModel.bidsCount)
        self.timerInfo2Label.isHidden = false
        self.carModelLabel.text = Helper.toString(object: tenderModel.carTitle())
        if smartTenderType == .carDetailing {
            let type = Helper.toInt(tenderModel.carDetailingType)
            let carDetailType = CarDetailingOptions(rawValue: type)
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.cdServiceTypeText.localizedString())", secondText: Helper.toString(object: carDetailType?.title), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
        } else {
             self.serviceLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.serviceTypeText.localizedString())", secondText: Helper.toString(object: tenderModel.getServicingType()), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
        }
       
        self.partLabel.isHidden = true
        self.wheelSizeBgView.isHidden = true
        self.setupTimerAndBanner(tenderModel: tenderModel)
    }
    
    func configureViewForBW(tenderModel: AllTendersModel, smartTenderType: ChooseServiceType) {
        //setting description labels
        self.bidsCountLabel.text = Helper.toString(object: tenderModel.bidsCount)
        self.timerInfo2Label.isHidden = false
        self.carModelLabel.text = Helper.toString(object: tenderModel.carTitle())
        //Service Type
       let bodyWorkType = Helper.toInt(tenderModel.tenderBodyWorkType)
       self.serviceLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.cdServiceTypeText.localizedString())", secondText: Helper.toString(object: Helper.getBodyWorkType(statusCode: bodyWorkType)), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
        self.partLabel.isHidden = true
        self.wheelSizeBgView.isHidden = true
        self.setupTimerAndBanner(tenderModel: tenderModel)
    }
    
    func configureViewForSpareParts(tenderModel: AllTendersModel) {
        self.bidsCountLabel.text = Helper.toString(object: tenderModel.bidsCount)
        self.timerInfo2Label.isHidden = false
        self.carModelLabel.text = Helper.toString(object: tenderModel.carTitle())

        //PartType
        if let partType = tenderModel.partType, let partTypeTitle = SparePartType(rawValue: partType)?.title {
            self.serviceLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.partTypeText.localizedString())", secondText: partTypeTitle, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
        }
        
        self.partLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.partNumberText.localizedString())", secondText: Helper.toString(object: tenderModel.partNumber), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
        self.wheelSizeBgView.isHidden = true
        self.setupTimerAndBanner(tenderModel: tenderModel)
    }
    
    // Configure for Rims
    func configureViewForRims(tenderModel: AllTendersModel) {
        //setting description labels
        self.bidsCountLabel.text = Helper.toString(object: tenderModel.bidsCount)
        self.timerInfo2Label.isHidden = false
        self.carModelLabel.text = Helper.toString(object: tenderModel.carTitle())
        self.partLabel.isHidden = true
        self.wheelSizeBgView.isHidden = false
        self.serviceLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.serviceTypeText.localizedString())", secondText: Helper.toString(object: tenderModel.showServiceType), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
        self.wheelSizeInfoLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.wheelSizeText.localizedString())", secondText: "\(Helper.toString(object: tenderModel.wheelSize)) ", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
        self.setupTimerAndBanner(tenderModel: tenderModel)

    }
    
    // Configure for Insurance
    func configureViewForInsurance(tenderModel: AllTendersModel) {
          //setting description labels
          self.bidsCountLabel.text = Helper.toString(object: tenderModel.bidsCount)
          self.timerInfo2Label.isHidden = false
          self.carModelLabel.text = Helper.toString(object: tenderModel.carTitle())
          self.partLabel.isHidden = true
          self.wheelSizeBgView.isHidden = false
        let insuranceStr = tenderModel.getInsuranceType()
        self.serviceLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.insuranceTypeText.localizedString())", secondText: insuranceStr, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
          self.wheelSizeInfoLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.vehicleValueText.localizedString())", secondText: "AED \(Helper.toInt(tenderModel.vehiclePrice)) ", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
          self.setupTimerAndBanner(tenderModel: tenderModel)

      }
    
    // Configure for Window Tinting
      func configureViewForWT(tenderModel: AllTendersModel) {
          //setting description labels
          self.bidsCountLabel.text = Helper.toString(object: tenderModel.bidsCount)
          self.timerInfo2Label.isHidden = false
          self.carModelLabel.text = Helper.toString(object: tenderModel.carTitle())
          self.partLabel.isHidden = false
          self.wheelSizeBgView.isHidden = true
          self.serviceLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.tintColorTxt.localizedString())", secondText: Helper.toString(object: tenderModel.tintColor), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
        let visibilityPercentage = Helper.toString(object: tenderModel.visibilityPercentage) == "" ? Helper.toString(object: tenderModel.visibilityPercentage) : "\(Helper.toString(object: tenderModel.visibilityPercentage))%"
          self.partLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.tintVisibilityTxt.localizedString())", secondText: visibilityPercentage, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
          self.setupTimerAndBanner(tenderModel: tenderModel)

      }
    
    // Configure for Bank Valuation
      func configureViewForBV(tenderModel: AllTendersModel) {
          //setting description labels
          self.bidsCountLabel.text = Helper.toString(object: tenderModel.bidsCount)
          self.timerInfo2Label.isHidden = false
          self.carModelLabel.text = Helper.toString(object: tenderModel.carTitle())
          self.partLabel.isHidden = false
          self.partLabel.isHidden = true
          self.wheelSizeBgView.isHidden = false
          self.serviceLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.bankName.localizedString())", secondText: Helper.toString(object: tenderModel.bankName
          ), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
        let carPrice = Helper.toString(object: tenderModel.carPrice) == "" ? Helper.toString(object: tenderModel.carPrice) : "AED \(Helper.toString(object: tenderModel.carPrice))"
          self.wheelSizeInfoLabel.attributedText = String.getAttributedText(firstText:"\(StringConstants.Text.requestedCar.localizedString())", secondText: carPrice, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_14, secondTextSize: .size_14, fontWeight: .Medium)
          self.setupTimerAndBanner(tenderModel: tenderModel)

      }
}
