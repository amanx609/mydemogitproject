//
//  ChooseServiceOptionCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/12/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

protocol ChooseServiceOptionCellDelegate: class {
    func didSelectServiceType(cell:ChooseServiceOptionCell, value: String)
    func didUnSelectSubService(cell:ChooseServiceOptionCell, value: String)
    func didSelectSubService(cell:ChooseServiceOptionCell, value: String)
}

class ChooseServiceOptionCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var majorServiceButton: UIButton!
    @IBOutlet weak var majorServiceLabel: UILabel!
    @IBOutlet weak var minorServiceButton: UIButton!
    @IBOutlet weak var minorServiceLabel: UILabel!
    @IBOutlet weak var frontBrakeButton: UIButton!
    @IBOutlet weak var frontBrakeLabel: UILabel!
    @IBOutlet weak var rearBrakeButton: UIButton!
    @IBOutlet weak var rearBrakeLabel: UILabel!
    @IBOutlet weak var brakeDisksButton: UIButton!
    @IBOutlet weak var brakeDisksLabel: UILabel!
    
    //MARK: - Variables
    weak var delegate: ChooseServiceOptionCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    private func setup() {
        
    }
    
    //MARK: - IBActions
    
    @IBAction func tapMajorServiceButton(_ sender: Any) {
        if minorServiceButton.isSelected {
            minorServiceButton.isSelected = !minorServiceButton.isSelected
        }
        if !majorServiceButton.isSelected {
            majorServiceButton.isSelected = !majorServiceButton.isSelected
        }
        if majorServiceButton.isSelected {
            if let delegate = self.delegate {
                delegate.didSelectServiceType(cell: self, value: ChooseServiceOptions.major.rawValue)
            }
        }
        
    }
    
    @IBAction func tapMinorServiceButton(_ sender: Any) {
        if majorServiceButton.isSelected {
            majorServiceButton.isSelected = !majorServiceButton.isSelected
        }
        if !minorServiceButton.isSelected {
                   minorServiceButton.isSelected = !minorServiceButton.isSelected
               }
        if minorServiceButton.isSelected {
            if let delegate = self.delegate {
                delegate.didSelectServiceType(cell: self, value: ChooseServiceOptions.minor.rawValue)
            }
        }
    }
    
    @IBAction func tapFrontBrakeButton(_ sender: Any) {
        frontBrakeButton.isSelected = !frontBrakeButton.isSelected
        if frontBrakeButton.isSelected {
            if let delegate = self.delegate {
                delegate.didSelectSubService(cell: self, value: ChooseSubServiceOptions.frontBrakePads.rawValue)
            }
        } else {
            if let delegate = self.delegate {
                delegate.didUnSelectSubService(cell: self, value: ChooseSubServiceOptions.frontBrakePads.rawValue)
            }
        }
    }
    
    @IBAction func tapRearBrakeButton(_ sender: Any) {
        rearBrakeButton.isSelected = !rearBrakeButton.isSelected
        if rearBrakeButton.isSelected {
            if let delegate = self.delegate {
                delegate.didSelectSubService(cell: self, value: ChooseSubServiceOptions.rearBrakePads.rawValue)
            }
        } else {
            if let delegate = self.delegate {
                delegate.didUnSelectSubService(cell: self, value: ChooseSubServiceOptions.rearBrakePads.rawValue)
            }
        }
    }
    
    @IBAction func tapBrakeDiscsButton(_ sender: Any) {
        brakeDisksButton.isSelected = !brakeDisksButton.isSelected
        if brakeDisksButton.isSelected {
            if let delegate = self.delegate {
                delegate.didSelectSubService(cell: self, value: ChooseSubServiceOptions.brakeDiscs.rawValue)
            }
        } else {
            if let delegate = self.delegate {
                delegate.didUnSelectSubService(cell: self, value: ChooseSubServiceOptions.brakeDiscs.rawValue)
            }
        }
        
    }
}
