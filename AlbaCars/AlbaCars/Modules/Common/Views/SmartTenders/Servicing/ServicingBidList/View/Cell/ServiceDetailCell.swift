//
//  ServiceDetailCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/14/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class ServiceDetailCell: BaseTableViewCell,ReusableView, NibLoadableView {
    
    //MARK: - IBOutelts
    @IBOutlet weak var serviceDetailLabel: UILabel!
    @IBOutlet weak var serviceTypeLabel: UILabel!
    @IBOutlet weak var numOfCylinderLabel: UILabel!
    @IBOutlet weak var engineSizeLabel: UILabel!
    @IBOutlet weak var frontBrakeLabel: UILabel!
    @IBOutlet weak var vinNumberLabel: UILabel!
    @IBOutlet weak var brakeDiscsLabel: UILabel!
    @IBOutlet weak var rearBrakeLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            //serviceDetails
            self.serviceDetailLabel.text = StringConstants.Text.servicingDetails
            
            //serviceType
            self.serviceTypeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.typeOfService.localizedString()) ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.serviceType]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            //no of cylinders
            self.numOfCylinderLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.numOfCylindersStr.localizedString()) ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.noOfCylinders]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            //engineSize
            self.engineSizeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.engineSizeStr.localizedString()) ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.engineSize]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            //frontBrake
            self.frontBrakeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.frontBrakeStr.localizedString()) ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.frontBrake]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            //vin number
            self.vinNumberLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.vinNumber.localizedString()) ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.vinNumber]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            //brake discs
            self.brakeDiscsLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.brakeDiscs.localizedString()) ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.brakeDiscs]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
            // rear brake pads
            self.rearBrakeLabel.attributedText = String.getAttributedText(firstText: "\(StringConstants.Text.rearBrakeStr.localizedString()) ", secondText: " \(Helper.toString(object: info[Constants.UIKeys.rearBrake]))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_11, secondTextSize: .size_11, fontWeight: .Medium)
        }
    }
    
    
    
}
