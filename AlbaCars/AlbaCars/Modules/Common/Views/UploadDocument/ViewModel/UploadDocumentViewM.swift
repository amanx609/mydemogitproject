//
//  UploadDocumentViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/17/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import Foundation
import UIKit

protocol UploadDocumentVModeling: BaseVModeling {
    func requestUploadDocumentAPI(documentUrlFirst: String,documentUrlSecond: String, completion: @escaping (Bool)-> Void)
    func requestSkipUploadDocumentAPI(completion: @escaping (Bool)-> Void)
}

class UploadDocumentViewM: UploadDocumentVModeling {
    
    //MARK: - API Methods
    
    func requestSkipUploadDocumentAPI(completion: @escaping (Bool)-> Void) {
        
           
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .skipDocumentUpload)) { (response, success) in
                if success {
                    if let safeResponse =  response as? [String: AnyObject],
                        let result = safeResponse[ConstantAPIKeys.result] as? Int
                    {
                        completion(success)
                    }
                }
            }
        
        }
    
    func requestUploadDocumentAPI(documentUrlFirst: String,documentUrlSecond: String, completion: @escaping (Bool)-> Void) {
        if self.validateUrlData(documentUrlFirst: documentUrlFirst,documentUrlSecond: documentUrlSecond ) {
            let uploadDocumentParams = self.getUploadDocumentParams(documentUrlFirst: documentUrlFirst,documentUrlSecond: documentUrlSecond )
            APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .uploadDocument(param: uploadDocumentParams))) { (response, success) in
                if success {
                    if let safeResponse =  response as? [String: AnyObject],
                        let result = safeResponse[ConstantAPIKeys.result] as? Int
                    {
                        completion(success)
                    }
                }
            }
        }
    }
    
    //MARK: - Private Methods
    //DataValidationsBeforeAPICall
    private func validateUrlData(documentUrlFirst: String,documentUrlSecond: String) -> Bool {
        var isValid = true
        if documentUrlFirst.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please upload document first. ".localizedString())
            isValid = false
        } else if documentUrlSecond.isEmpty {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please upload document first. ".localizedString())
            isValid = false
        }
        
        return isValid
    }
    
    private func getUploadDocumentParams(documentUrlFirst: String,documentUrlSecond: String)-> APIParams {
        var params: APIParams = APIParams()
        var paramArray: [APIParams] = []
        let paramObj1 = [ConstantAPIKeys.name: StringConstants.Text.registrationCard, ConstantAPIKeys.image: documentUrlFirst]
        paramArray.append(paramObj1 as APIParams)
        
        let paramObj2 = [ConstantAPIKeys.name: StringConstants.Text.ownerShipCertificate, ConstantAPIKeys.image: documentUrlSecond]
        paramArray.append(paramObj2 as APIParams)
        
        params[ConstantAPIKeys.documents] = paramArray as AnyObject
        return params
    }
}

