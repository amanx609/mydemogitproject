//
//  UploadDocumentViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/17/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class UploadDocumentViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var documentFirstImageView: UIImageView!
    @IBOutlet weak var documentSecondImageView: UIImageView!
    @IBOutlet weak var documentFirstButton: UIButton!
    @IBOutlet weak var documentSecondButton: UIButton!
    @IBOutlet weak var registrationCardLabel: UILabel!
    @IBOutlet weak var ownershipCertificateLabel: UILabel!
    @IBOutlet weak var skipButton: UIButton!

    //MARK: - Variables
    var viewModel: UploadDocumentVModeling?
    var documentFirstUrl = ""
    var documentSecondUrl = ""
    var isSideMenu = false
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.roundCorners()
        self.recheckVM()
        self.setupView()
        self.setupNavigationBarTitle(title: StringConstants.Text.uploadDocument.localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = UploadDocumentViewM()
        }
    }
    
    private func roundCorners() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.proceedButton.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
        }
    }
    
    private func setupView() {
        self.registrationCardLabel.text = StringConstants.Text.registrationCard
        self.ownershipCertificateLabel.text = StringConstants.Text.ownerShipCertificate
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        self.proceedButton.setTitle(StringConstants.Text.proceed.localizedString(), for: .normal)
        self.skipButton.isHidden = isSideMenu
        
    }
  
    func selectDocument() {
        
        Threads.performTaskInMainQueue {
            ImagePickerManager.sharedInstance.getSingleImage { (image,url) in
                if let image = image {
                    self.uploadImageToS3(image: image)
                }
            }
        }
    }
    
    func uploadImageToS3(image: UIImage) {
        if self.documentFirstButton.isSelected {
            self.documentFirstImageView.image = image
            
        } else {
            self.documentSecondImageView.image = image
        }
        Loader.showLoader()
        S3Manager.sharedInstance.uploadImage(image: image, imageQuality: .low, progress: nil) { (response, imageName, error) in
            
            guard let imageName = imageName as? String else { return }
            
            if self.documentFirstButton.isSelected {
                self.documentFirstUrl = imageName
            } else {
                self.documentSecondUrl = imageName
            }
            
            Threads.performTaskInMainQueue {
                Loader.hideLoader()
            }
        }

    }
    
    //MARK: - API Methods
    private func requestUploadDocumentAPI() {
        self.viewModel?.requestUploadDocumentAPI(documentUrlFirst: self.documentFirstUrl, documentUrlSecond: self.documentSecondUrl, completion:{ (success) in
            if success {
               
                if self.isSideMenu {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    Threads.performTaskInMainQueue {
                        if let userTypeVal = UserDefaultsManager.sharedInstance.getIntValueFor(key: .userType), let userType = UserType(rawValue: userTypeVal) {
                            if userType == .supplier {
                                self.saveUserId()
                                AppDelegate.delegate.showHome()
                            } else {
                                let registrationPaymentViewC = DIConfigurator.sharedInstance.getRegisterPaymentViewC()
                                self.navigationController?.pushViewController(registrationPaymentViewC, animated: true)
                            }
                        }
                        
                    }
                }
            }
        })
    }
    
    private func saveUserId() {
        if let userTypeVal = UserDefaultsManager.sharedInstance.getIntValueFor(key: .userType), let userType = UserType(rawValue: userTypeVal) {
            if userType == .supplier {
                let userId = UserSession.shared.getUserId()
                UserDefaultsManager.sharedInstance.saveIntValueFor(key: .userId, value: userId)
            }
        }
    }
    

    
    //MARK: - IBActions
    
    @IBAction func tapProceedButton(_ sender: Any) {
        self.requestUploadDocumentAPI()
    }
    
    @IBAction func tapDocumentFirstButton(_ sender: Any) {
        self.documentFirstButton.isSelected = true
        self.documentSecondButton.isSelected = false
        self.selectDocument()
    }
    
    @IBAction func tapDocumentSecondButton(_ sender: Any) {
        self.documentFirstButton.isSelected = false
        self.documentSecondButton.isSelected = true
        self.selectDocument()
    }
    
    @IBAction func tapSkipButton(_ sender: Any) {
    
        self.viewModel?.requestSkipUploadDocumentAPI(completion:{ (success) in
            if success {
                Threads.performTaskInMainQueue {
                    if let userTypeVal = UserDefaultsManager.sharedInstance.getIntValueFor(key: .userType), let userType = UserType(rawValue: userTypeVal) {
                        if userType == .supplier {
                            self.saveUserId()
                            AppDelegate.delegate.showHome()
                        } else {
                            let registrationPaymentViewC = DIConfigurator.sharedInstance.getRegisterPaymentViewC()
                            self.navigationController?.pushViewController(registrationPaymentViewC, animated: true)
                        }
                    }
                    
                }
                
            }
        })
        
    }
    
}
