//
//  DocumentModel.swift
//  AlbaCars
//
//  Created by Narendra on 12/18/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

//Documents
class DocumentModel: Codable {
  
  //Variables
  var id: Int?
  var name: String?
  var image: String?
}
