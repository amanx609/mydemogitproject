//
//  DocumentListViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/15/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol DocumentListViewModeling {
    func getDocumentListDataSource() -> [CellInfo]
    func requestDocumentAPI(completion: @escaping (Any)-> Void)
}

class DocumentListViewM: DocumentListViewModeling {
    
    func getDocumentListDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        // documentList Cell
        var documentListCellInfo = [String: AnyObject]()
        documentListCellInfo[Constants.UIKeys.documentName] = "Emirates Id" as AnyObject
        documentListCellInfo[Constants.UIKeys.documentSize] = "167 Kb.Pdf" as AnyObject
        let documentListCell = CellInfo(cellType: .DocumentListCell, placeHolder: "", value: "", info: documentListCellInfo, height: Constants.CellHeightConstants.height_100)
        array.append(documentListCell)
        
        return array
    }
    
    //MARK: - API Methods
    func requestDocumentAPI(completion: @escaping (Any)-> Void) {
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .getDocuments)) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [[String: AnyObject]],
                    let documentsDataArray = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let documentData = try decoder.decode([DocumentModel].self, from: documentsDataArray)
                        completion(documentData)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
}



