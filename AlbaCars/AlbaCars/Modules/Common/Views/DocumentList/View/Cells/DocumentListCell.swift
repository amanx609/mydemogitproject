//
//  DocumentListCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/15/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DocumentListCell: BaseTableViewCell,NibLoadableView,ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var documentImageView: UIImageView!
    @IBOutlet weak var documentSizeLabel: UILabel!
    @IBOutlet weak var documentNameLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_7)
        self.bgView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: 1, round: Constants.UIConstants.sizeRadius_7)
        self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))

    }
    
    //MARK: - Public Methods
     func configureView(document: DocumentModel) {
//        if let info = cellInfo.info {
//           // self.documentImageView.image = info[Constants.UIKeys.documentImage]
//            self.documentNameLabel.text = info[Constants.UIKeys.documentName] as? String
//            self.documentSizeLabel.text = info[Constants.UIKeys.documentSize] as? String
//        }
        
        self.documentNameLabel.text = document.name
        if let imageName = document.image {
            self.documentImageView.setImage(urlStr:imageName, placeHolderImage: nil)
        }
    }
    
}
