//
//  DocumentListViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/15/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class DocumentListViewC: BaseViewC {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var documentListTableView: UITableView!
    @IBOutlet weak var maxSizeButton: UIButton!
    @IBOutlet weak var uploadFileButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    
    //MARK: - Variables
    var documentListDataSource = [DocumentModel]()
    var viewModel: DocumentListViewModeling?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    deinit {
        print("deinit DocumentListViewC")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.requestDocumentAPI()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupTableView()
        self.roundCorners()
        self.setupView()
        self.setupNavigationBarTitle(title: StringConstants.Text.documents.localizedString(), leftBarButtonsType: [.hamburgerBlack], rightBarButtonsType: [])
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = DocumentListViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.documentListTableView.backgroundColor = UIColor.white
        self.documentListTableView.delegate = self
        self.documentListTableView.dataSource = self
        self.documentListTableView.separatorStyle = .none
        self.documentListTableView.allowsSelection = false
    }
    
    private func roundCorners() {
        Threads.performTaskInMainQueue  {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.uploadFileButton.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
        }
    }
    
    private func setupView() {
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
        self.uploadFileButton.setTitle(StringConstants.Text.uploadNewFile.localizedString(), for: .normal)
        self.maxSizeButton.setTitle(StringConstants.Text.maxFileSize.localizedString(), for: .normal)
        self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
    }
    
    private func registerNibs() {
        self.documentListTableView.register(DocumentListCell.self)
    }
        
    //MARK: - IBActions
    
    @IBAction func maxSizeButtonTapped(_ sender: Any) {
    }
    
    @IBAction func uploadFileButtonTapped(_ sender: Any) {
        let documentViewC = DIConfigurator.sharedInstance.getUploadDocumentViewC()
        documentViewC.isSideMenu = true
        self.navigationController?.pushViewController(documentViewC, animated: true)
    }
    
    //MARK: - API Methods
    func requestDocumentAPI() {
        self.viewModel?.requestDocumentAPI() { (responseData) in
            if let data = responseData as? [DocumentModel] {
                self.documentListDataSource = data
                Threads.performTaskInMainQueue {
                    self.documentListTableView.reloadData()
                }
            }
        }
    }
}
