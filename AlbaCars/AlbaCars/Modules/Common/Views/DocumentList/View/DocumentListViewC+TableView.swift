//
//  DocumentListViewC+TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/15/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension DocumentListViewC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.documentListDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let document = self.documentListDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, document: document)
    }
}

extension DocumentListViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath, document: DocumentModel) -> UITableViewCell {
        let cell: DocumentListCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(document: document)
        return cell
    }
}

