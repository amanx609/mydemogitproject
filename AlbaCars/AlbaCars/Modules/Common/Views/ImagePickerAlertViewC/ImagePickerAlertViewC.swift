//
//  ImagePickerAlertViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol ImagePickerAlertDelegate: class {
    func didTapGallery(viewC: ImagePickerAlertViewC)
    func didTapCamera(viewC: ImagePickerAlertViewC)
    func didTapDocument(viewC: ImagePickerAlertViewC)
    
}

class ImagePickerAlertViewC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var selectPhotosLabel: UILabel!
    @IBOutlet weak var documentView: UIView!
    
    //MARK: - Variables
    weak var delegate: ImagePickerAlertDelegate?
    var isSelectSingleImage: Bool = false
    var isDocument: Bool = false
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        documentView.isHidden = !isDocument
        if isDocument {
            self.selectPhotosLabel.text = StringConstants.Text.selectFile.localizedString()
        } else {
            self.selectPhotosLabel.text = self.isSelectSingleImage ?  StringConstants.Text.selectPhoto.localizedString() : StringConstants.Text.selectPhotos.localizedString()
        }
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissController)))
    }
    
    @objc private func dismissController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: IBActions
    @IBAction func galleryButtonTapped(_ sender: Any) {
        print("gallery Tapped")
        if let delegate = self.delegate {
            delegate.didTapGallery(viewC: self)
        }
    }
    
    @IBAction func cameraButtonTapped(_ sender: Any) {
        print("camera tapped")
        if let delegate = self.delegate {
            delegate.didTapCamera(viewC: self)
        }
    }
    
    @IBAction func documentButtonTapped(_ sender: Any) {
        print("camera tapped")
        if let delegate = self.delegate {
            delegate.didTapDocument(viewC: self)
        }
    }
}
