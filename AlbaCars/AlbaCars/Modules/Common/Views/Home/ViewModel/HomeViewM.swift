//
//  HomeViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol HomeVModeling: BaseVModeling {
    func getHomeDataSource() -> [CellInfo]
}

class HomeViewM: HomeVModeling {
    
    func getHomeDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Header Cell
        let headerCell = CellInfo(cellType: .HomeHeaderCollectionCell, placeHolder: "Welcome to ALBA CARS".localizedString(), value: "", info: nil, height: Constants.CellHeightConstants.height_135)
        array.append(headerCell)
        
        let userType = UserSession.shared.userType
        print(userType as Any)
        switch userType {
        case .customer:
            array.append(contentsOf:self.getCustomerHomeDataSource())
        case .dealer:
            array.append(contentsOf:self.getDealerHomeDataSource())
        case .supplier:
            
            if let serviceList = UserSession.shared.getSelectedServiceList() {
                
                for chooseServiceType in ChooseServiceType.allValues{
                    
                    for (_, element) in serviceList.enumerated() {
                        
                        if element == chooseServiceType.rawValue {
                            
                            var info = [String: AnyObject]()
                            info[Constants.UIKeys.placeholderImage] = chooseServiceType.homeImage
                            info[Constants.UIKeys.id] = element as AnyObject
                            let cell = CellInfo(cellType: .HomeCollectionCell, placeHolder: chooseServiceType.title, value: "", info: info, height: Constants.CellHeightConstants.height_130)
                            array.append(cell)
                        }
                    }
                }
            }
            print("")
        default:
            print("")
        }
        return array
    }
    
    func getDealerHomeDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Smart Tenders Cell
        var smartTendersInfo = [String: AnyObject]()
        smartTendersInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "smart_tenders")
        let smartTendersCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Smart Tenders".localizedString(), value: "", info: smartTendersInfo, height: Constants.CellHeightConstants.height_130)
        array.append(smartTendersCell)
        
        //Auctions Cell
        var auctionsInfo = [String: AnyObject]()
        auctionsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "auctions")
        let auctionsCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Auctions".localizedString(), value: "", info: auctionsInfo, height: Constants.CellHeightConstants.height_120)
        array.append(auctionsCell)
        
        //Leads Cell
        var leadsInfo = [String: AnyObject]()
        leadsInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "leads")
        let leadsCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Leads".localizedString(), value: "", info: leadsInfo, height: Constants.CellHeightConstants.height_120)
        array.append(leadsCell)
        
        //Fleet Bidding Cell
        var fleetBiddingInfo = [String: AnyObject]()
        fleetBiddingInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "fleet_bidding")
        let fleetBiddingCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Fleet Bidding".localizedString(), value: "", info: fleetBiddingInfo, height: Constants.CellHeightConstants.height_120)
        array.append(fleetBiddingCell)
        
        //Leaderboard Cell
        var leaderboardInfo = [String: AnyObject]()
        leaderboardInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "leaderboard")
        let leaderboardCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Leaderboard".localizedString(), value: "", info: leaderboardInfo, height: Constants.CellHeightConstants.height_120)
        array.append(leaderboardCell)
        
        //Warranty Cell
        var warrantyInfo = [String: AnyObject]()
        warrantyInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "warranty")
        let warrantyCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Warranty".localizedString(), value: "", info: warrantyInfo, height: Constants.CellHeightConstants.height_120)
        array.append(warrantyCell)
        
        //History Cell
        var serviceHistoryInfo = [String: AnyObject]()
        serviceHistoryInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "service_history")
        let serviceHistoryCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Service History".localizedString(), value: "", info: serviceHistoryInfo, height: Constants.CellHeightConstants.height_120)
        array.append(serviceHistoryCell)
        
        return array
        
    }
    
    func getCustomerHomeDataSource() -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Smart Tenders Cell
        var smartTendersInfo = [String: AnyObject]()
        smartTendersInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "smart_tenders")
        let smartTendersCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Smart Tenders".localizedString(), value: "", info: smartTendersInfo, height: Constants.CellHeightConstants.height_130)
        array.append(smartTendersCell)
        
        //Buy Cell
        var buyInfo = [String: AnyObject]()
        buyInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "buy.png")
        let buyCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Buy".localizedString(), value: "", info: buyInfo, height: Constants.CellHeightConstants.height_120)
        array.append(buyCell)
        
        //Sell Cell
        var sellInfo = [String: AnyObject]()
        sellInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "car_sell.png")
        let sellCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Sell".localizedString(), value: "", info: sellInfo, height: Constants.CellHeightConstants.height_120)
        array.append(sellCell)
        
        //Request Car Cell
        var requestCarInfo = [String: AnyObject]()
        requestCarInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "request_car.png")
        let requestCarCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Request Car".localizedString(), value: "", info: requestCarInfo, height: Constants.CellHeightConstants.height_120)
        array.append(requestCarCell)
        
        //Car Finance Cell
        var carFinanceInfo = [String: AnyObject]()
        carFinanceInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "car_finance.png")
        let carFinanceCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Car Finance".localizedString(), value: "", info: carFinanceInfo, height: Constants.CellHeightConstants.height_120)
        array.append(carFinanceCell)
        
        //Offers & Deals Cell
        var offersInfo = [String: AnyObject]()
        offersInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "offers_deals.png")
        let offersCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Offers & Deals".localizedString(), value: "", info: offersInfo, height: Constants.CellHeightConstants.height_120)
        array.append(offersCell)
        
        //Warranty Cell
        var warrantyInfo = [String: AnyObject]()
        warrantyInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "warranty")
        let warrantyCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Warranty".localizedString(), value: "", info: warrantyInfo, height: Constants.CellHeightConstants.height_120)
        array.append(warrantyCell)
        
        //Service Contract Cell
        var serviceContractInfo = [String: AnyObject]()
        serviceContractInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "service_contract.png")
        let serviceContractCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Service Contract".localizedString(), value: "", info: serviceContractInfo, height: Constants.CellHeightConstants.height_120)
        array.append(serviceContractCell)
        
        //Warranty Cell
        var serviceHistoryInfo = [String: AnyObject]()
        serviceHistoryInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "service_history")
        let serviceHistoryCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: "Service History".localizedString(), value: "", info: serviceHistoryInfo, height: Constants.CellHeightConstants.height_120)
        array.append(serviceHistoryCell)
        return array
    }
    
}
