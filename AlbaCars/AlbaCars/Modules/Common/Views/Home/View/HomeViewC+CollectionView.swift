//
//  HomeViewC+CollectionView.swift
//  AlbaCars
//
//  Created by Narendra on 12/13/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension HomeViewC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.homeDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellInfo = self.homeDataSource[indexPath.row]
        return self.getCell(collectionView: collectionView, indexPath: indexPath, cellInfo: cellInfo)
    }
  
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width = collectionView.bounds.width/2-5
        
        let userType = UserSession.shared.userType
        switch userType {
        case .customer,.dealer:
            if indexPath.row == 0 || indexPath.row == 1 {
                width = collectionView.bounds.width
            }
        case .supplier:
           
            if indexPath.row == 0 {
                width = collectionView.bounds.width
            } else if let id = self.homeDataSource[indexPath.row].info?[Constants.UIKeys.id] as? Int, id == ChooseServiceType.insurance.rawValue, self.homeDataSource.count % 2 == 0 {
               width = collectionView.bounds.width
            }
           
        default:
            print("")
        }
        
        return CGSize(width: width, height: self.homeDataSource[indexPath.row].height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
      let userType = UserSession.shared.userType
      switch userType {
      case .dealer:
        self.goToDealerSelectedScreen(indx: indexPath.row)
        case .customer:
        self.goToCustomerSelectedScreen(indx: indexPath.row)
        case .supplier:
        if let id = self.homeDataSource[indexPath.row].info?[Constants.UIKeys.id] as? Int {
            self.goToSupplierSelectedScreen(serviceID: id)
        }
      default:
        print("Default")
      }
    }
    
}

extension HomeViewC {
    func getCell(collectionView: UICollectionView, indexPath: IndexPath, cellInfo: CellInfo) -> UICollectionViewCell {
        guard let cellType = cellInfo.cellType else { return UICollectionViewCell() }
        switch cellType {
        case .HomeCollectionCell:
            let cell: HomeCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            if indexPath.row == 0 || indexPath.row == 1 {
                cell.homeInfoView.placeholderImageViewWidth.constant = Constants.CellHeightConstants.height_84
            } else {
                cell.homeInfoView.placeholderImageViewWidth.constant = Constants.CellHeightConstants.height_56
            }
            return cell
        case .HomeHeaderCollectionCell:
            let cell: HomeHeaderCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UICollectionViewCell()
        }
    }
}
