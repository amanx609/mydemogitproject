//
//  HomeViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/11/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class HomeViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var homeCollectionView: UICollectionView!
    
    //MARK: - Variables
    var homeDataSource: [CellInfo] = []
    var viewModel: HomeVModeling?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "Home".localizedString(),barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.hamburgerWhite], rightBarButtonsType: [.notification])
    }
    
    deinit {
        print("deinit HomeViewC")
    }
    
    override func navigationButtonTapped(_ sender: AnyObject) {
        guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
        switch buttonType {
        case .hamburgerWhite,.hamburgerBlack: hamburgerButtonTapped(sender)
        case .notification:
           notificationButtonTapped(sender)
        default: break
        }
    }
    
    func notificationButtonTapped(_ sender: AnyObject) {
       let notificationVC = DIConfigurator.sharedInstance.getNotificationVC()
       notificationVC.isHome = true
       self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    //MARK: - Private Methods
    private func setup() {
        
        self.setupView()
        self.recheckVM()
        self.setupCollectionView()
        self.loadDataSource()
        self.getNotificationCount()
    }
    
    private func loadDataSource() {
        if let dataSource = self.viewModel?.getHomeDataSource() {
            self.homeDataSource = dataSource
        }
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_20)
     //   self.bgView.addShadow(ofColor:  UIColor.lightGray, radius: Constants.UIConstants.sizeRadius_20)
        self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_20, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_20))

    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = HomeViewM()
        }
    }
    private func getNotificationCount(){
          let loginViewM: LoginViewM  = LoginViewM()
              loginViewM.getNotificationCount { (_) in
                  let count = UserDefaultsManager.sharedInstance.getIntValueFor(key: .notificationCount)
                  if count != 0{
                      self.navigationItem.rightBarButtonItem?.addBadge(number: count ?? 0, withOffset: CGPoint(x: 5, y: 5), andColor: #colorLiteral(red: 1, green: 0.2039215686, blue: 0.2823529412, alpha: 1), andFilled: false)
                  }
                  
          }
          //viewModel?.getNotificationCount()
      }
    private func setupCollectionView() {
        self.registerNibs()
        self.homeCollectionView.delegate = self
        self.homeCollectionView.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //depending upon direction of collection view
        self.homeCollectionView?.setCollectionViewLayout(layout, animated: true)
        //self.homeCollectionView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.homeCollectionView.register(HomeHeaderCollectionCell.self)
        self.homeCollectionView.register(HomeCollectionCell.self)
    }
    
    func goToDealerSelectedScreen(indx: Int) {
        
        switch indx {
        case 1:
            // Smart Tender
            let smartTendersVC = DIConfigurator.sharedInstance.getSmartTendersVC()
            self.navigationController?.pushViewController(smartTendersVC, animated: true)
            break
        case 2:
            // Auction Home
            let auctionVC = DIConfigurator.sharedInstance.getDAuctionVC()
            self.navigationController?.pushViewController(auctionVC, animated: true)
            break
        case 3:
            // Leads
            let leadsVC = DIConfigurator.sharedInstance.getDLeadsVC()
            self.navigationController?.pushViewController(leadsVC, animated: true)
            break
        case 4:
            // Fleet Biding
            let fleetBidingVC = DIConfigurator.sharedInstance.getDFleetBidingVC()
            self.navigationController?.pushViewController(fleetBidingVC, animated: true)
            break
        case 5:
            // leader board
            let leaderboardVC = DIConfigurator.sharedInstance.getDLeaderboardVC()
            self.navigationController?.pushViewController(leaderboardVC, animated: true)
            break
        case 6:
            // Warranty
            let cChooseCarForThirdPartyVC = DIConfigurator.sharedInstance.getChooseCarForServiceVC()
            cChooseCarForThirdPartyVC.chooseCarForServiceType = .thirdPartyWarranty
            self.navigationController?.pushViewController(cChooseCarForThirdPartyVC, animated: true)
            break
        case 7:
            // Service History
            let chooseCarForServiceVC = DIConfigurator.sharedInstance.getChooseCarForServiceVC()
            chooseCarForServiceVC.chooseCarForServiceType = .serviceHistory
            self.navigationController?.pushViewController(chooseCarForServiceVC, animated: true)
            break
        default:
            break
        }
    }
    
    func goToCustomerSelectedScreen(indx: Int) {
    
        switch indx {
               case 1:
                   // Smart Tender
                let smartTendersVC = DIConfigurator.sharedInstance.getSmartTendersVC()
                self.navigationController?.pushViewController(smartTendersVC, animated: true)
                   break
               case 2:
                   // Buy car
                   let buyCarsVC = DIConfigurator.sharedInstance.getBuyCarsVC()
                   self.navigationController?.pushViewController(buyCarsVC, animated: true)
                   break
               case 3:
                   // Sell car
                   let sellCarIntroVC = DIConfigurator.sharedInstance.getCSellCarIntroVC()
                   self.navigationController?.pushViewController(sellCarIntroVC, animated: true)
                   break
               case 4:
                //RequestCar
                  let requestCarVC = DIConfigurator.sharedInstance.getCRequestCarVC()
                  self.navigationController?.pushViewController(requestCarVC, animated: true)
                   break
               case 5:
                //Finance
                let financeVC = DIConfigurator.sharedInstance.getCAutoFinanceViewC()
                self.navigationController?.pushViewController(financeVC, animated: true)
                   break
               case 6:
                //OfferAndDeals
                let cOfferAndDealsVC = DIConfigurator.sharedInstance.getCOffersAndDealsVC()
                self.navigationController?.pushViewController(cOfferAndDealsVC, animated: true)
                   break
               case 7:
                //Warranty
                let cChooseCarForThirdPartyVC = DIConfigurator.sharedInstance.getChooseCarForServiceVC()
                cChooseCarForThirdPartyVC.chooseCarForServiceType = .thirdPartyWarranty
                self.navigationController?.pushViewController(cChooseCarForThirdPartyVC, animated: true)
                   break
               case 8:
                 //Service Contract
                let cChooseCarForServiceContractVC = DIConfigurator.sharedInstance.getChooseCarForServiceVC()
                cChooseCarForServiceContractVC.chooseCarForServiceType = .serviceContract
                 self.navigationController?.pushViewController(cChooseCarForServiceContractVC, animated: true)

               case 9:
                //Service History
                let cChooseCarForServiceHistoryVC = DIConfigurator.sharedInstance.getChooseCarForServiceVC()
                cChooseCarForServiceHistoryVC.chooseCarForServiceType = .serviceHistory
                self.navigationController?.pushViewController(cChooseCarForServiceHistoryVC, animated: true)
            
               default:
                   break
               }
    }
    
    func goToSupplierSelectedScreen(serviceID: Int) {
        
        for chooseServiceType in ChooseServiceType.allValues{
            
            if serviceID == chooseServiceType.rawValue {
                
                switch chooseServiceType {
                case .recovery:
                    let recoveryTenderVC = DIConfigurator.sharedInstance.getSRecoveryTenderVC()
                    self.navigationController?.pushViewController(recoveryTenderVC, animated: true)
                    break
                case .vehicleInspection:
                    let vehicleInspectionTendersVC = DIConfigurator.sharedInstance.getSVehicleInspectionTendersVC()
                    self.navigationController?.pushViewController(vehicleInspectionTendersVC, animated: true)
                    break
                case .carServicing:
                    let carServicingTenderVC = DIConfigurator.sharedInstance.getSCarServicingTenderVC()
                    self.navigationController?.pushViewController(carServicingTenderVC, animated: true)
                    break
                case .upholstery:
                    let upholsteryTendersVC = DIConfigurator.sharedInstance.getSUpholsteryTendersVC()
                    self.navigationController?.pushViewController(upholsteryTendersVC, animated: true)
                    break
                case .spareParts:
                    let sparePartsTenderVC = DIConfigurator.sharedInstance.getSSparePartsTenderVC()
                    self.navigationController?.pushViewController(sparePartsTenderVC, animated: true)
                    break
                case .wheels:
                    let wheelsTenderVC = DIConfigurator.sharedInstance.getSWheelsTenderVC()
                    self.navigationController?.pushViewController(wheelsTenderVC, animated: true)
                    break
                case .bodyWork:
                    let bodyWorkTenderVC = DIConfigurator.sharedInstance.getSBodyWorkTenderVC()
                    self.navigationController?.pushViewController(bodyWorkTenderVC, animated: true)
                    break
                case .carDetailing:
                    let carDetailingTenderVC = DIConfigurator.sharedInstance.getSCarDetailingTenderVC()
                    self.navigationController?.pushViewController(carDetailingTenderVC, animated: true)
                    break
                case .windowTinting:
                    let windowTintingTenderVC = DIConfigurator.sharedInstance.getSWindowTintingTenderVC()
                    self.navigationController?.pushViewController(windowTintingTenderVC, animated: true)
                    break
                case .bankValuation:
                    let bankValuationTenderVC = DIConfigurator.sharedInstance.getSBankValuationTenderVC()
                    self.navigationController?.pushViewController(bankValuationTenderVC, animated: true)
                case .insurance:
                    let insuranceTenderVC = DIConfigurator.sharedInstance.getSInsuranceTenderVC()
                    self.navigationController?.pushViewController(insuranceTenderVC, animated: true)
                    break
                }
                
            }
            
        }
        
    }
}





