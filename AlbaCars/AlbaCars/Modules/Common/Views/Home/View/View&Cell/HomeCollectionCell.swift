//
//  HomeCollectionCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/13/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class HomeCollectionCell: BaseCollectionViewCell,ReusableView, NibLoadableView  {
    
    //MARK: - IBOutlets
    @IBOutlet weak var homeInfoView: HomeInfoView!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var homeViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var notificationCountLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    //MARK: - Private Methods
    private func setupView() {
        Threads.performTaskInMainQueue {
            self.bgView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
            self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
            self.homeInfoView.roundCorners(Constants.UIConstants.sizeRadius_3half)
        }
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        
        self.homeInfoView.titleLabel.text = cellInfo.placeHolder
        
        if let info = cellInfo.info {
            //Image
            if let placeholderImage = info[Constants.UIKeys.placeholderImage] as? UIImage {
                self.homeInfoView.placeholderImageView.image = placeholderImage
            }
            if let notifications = info[Constants.UIKeys.notifications] as? Int {
                if  notifications == 0 {
                    
                    
                    self.notificationView.isHidden = true
                    self.notificationCountLabel.text = Helper.toString(object: info[Constants.UIKeys.notifications])
                } else {
                    self.notificationView.isHidden = false
                    self.notificationCountLabel.text = Helper.toString(object: info[Constants.UIKeys.notifications])
                }
            } else {
                self.notificationView.isHidden = true
            }
        }
        
        self.notificationView.isHidden = true
        if let userType = UserSession.shared.userType, userType == .supplier {
            homeViewTopConstraint.constant = Constants.UIConstants.sizeRadius_10
            //self.notificationView.isHidden = false
        }
    }
    
    func configureForRecovery(cellInfo: CellInfo) {
        self.homeInfoView.titleLabel.text = cellInfo.placeHolder
        homeViewTopConstraint.constant = Constants.UIConstants.sizeRadius_10

        if let info = cellInfo.info {
            //Image
            if let placeholderImage = info[Constants.UIKeys.placeholderImage] as? UIImage {
                self.homeInfoView.placeholderImageView.image = placeholderImage
            }
            if let notifications = info[Constants.UIKeys.notifications] as? Int, notifications != 0 {
                self.notificationView.isHidden = false
                self.notificationCountLabel.text = Helper.toString(object: info[Constants.UIKeys.notifications])
            } else {
                self.notificationView.isHidden = true
                self.notificationCountLabel.text = Helper.toString(object: info[Constants.UIKeys.notifications])
            }
        }
    }
    
    func configureForSmartTenders(cellInfo: CellInfo) {
        self.homeInfoView.titleLabel.text = cellInfo.placeHolder
        homeViewTopConstraint.constant = Constants.UIConstants.sizeRadius_5

        if let info = cellInfo.info {
            //Image
            if let placeholderImage = info[Constants.UIKeys.placeholderImage] as? UIImage {
                self.homeInfoView.placeholderImageView.image = placeholderImage
            }
            if let notifications = info[Constants.UIKeys.notifications] as? Int, notifications != 0 {
                self.notificationView.isHidden = false
                self.notificationCountLabel.text = Helper.toString(object: info[Constants.UIKeys.notifications])
            } else {
                self.notificationView.isHidden = true
                self.notificationCountLabel.text = Helper.toString(object: info[Constants.UIKeys.notifications])
            }
        }
    }
}
