//
//  HomeHeaderCollectionCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/13/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class HomeHeaderCollectionCell: BaseCollectionViewCell,ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        self.titleLabel.text = cellInfo.placeHolder
    }

}
