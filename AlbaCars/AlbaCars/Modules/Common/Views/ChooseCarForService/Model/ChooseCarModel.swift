//
//  ChooseCarModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class ChooseCarModel: BaseCodable {
    var brandName: String?
    var id: Int?
    var modelName: String?
    var plateNumber: String?
    var title: String?
    var typeName: String?
    var year: Int?
    var cylinders: String?
    var engine: Double?
    var mileage: Int?
    var make_id: Int?    
}

class CarDetails: BaseCodable {
    var cars: [CarDetailModel]?
    
    private enum CodingKeys: String, CodingKey {
      case cars = "result"
    }

}
