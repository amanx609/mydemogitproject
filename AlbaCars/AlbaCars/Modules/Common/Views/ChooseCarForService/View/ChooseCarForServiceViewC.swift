//
//  ChooseCarForServiceViewC.swift
//  AlbaCars
//
//  Created by Narendra on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

enum ChooseCarForServiceType {
    case none
    case serviceHistory
    case serviceContract
    case thirdPartyWarranty
    case recoveryRequest
    case inspectionRequest
    case upholsteryRequest
    case carServicing
    case spareParts
    case wheels
    case rims
    case tyres
    case windowTinting
    case bankValuation
    case insurance
    case carDetailing
    case bodyWork
        
    var title: String? {
        switch self {
        case .serviceHistory: return "Service History".localizedString()
        case .serviceContract: return "Service Contract".localizedString()
        case .thirdPartyWarranty: return "Third Party Warranty".localizedString()
        case .recoveryRequest: return "New Recovery Request".localizedString()
        case .inspectionRequest: return "New Vehicle Inspection Tender".localizedString()
        case .upholsteryRequest: return "New Upholstery Tender".localizedString()
        case .carServicing: return "Car Servicing Tender".localizedString()
        case .spareParts: return "Spare Parts Tender".localizedString()
        case .wheels: return "Wheels Tender".localizedString()
        case .windowTinting: return "Window Tinting Tender".localizedString()
        case .tyres: return "Tires Tender".localizedString()
        case .rims: return "Rims Tender".localizedString()
        case .bankValuation: return "Bank Valuation Tender".localizedString()
        case .insurance: return "Insurance Tender".localizedString()
        case .carDetailing: return "Car Detailing Tender".localizedString()
        case .bodyWork: return "Body Work Tender".localizedString()
        case .none:
            return ""
        
        }
    }
}

class ChooseCarForServiceViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var addNewCarButtonView: AddNewCarButtonView!
    
    //MARK: - Variables
    var chooseCarForServiceType: ChooseCarForServiceType = .none
    var carListDataSource: [[String: AnyObject]] = []
    var chooseCarDataSource: [CellInfo] = []
    var viewModel: ChooseCarFormViewModeling?
    var selectedCarId: Int?
    var selectedCarDetails: ChooseCarModel? = ChooseCarModel()
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupNavigationBarTitle(title: Helper.toString(object: chooseCarForServiceType.title).localizedString(),barColor: .redButtonColor, titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
        self.requestChooseCarAPI()
    }
    
    deinit {
        print("deinit ChooseCarForServiceViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.roundCorners()
        self.setupView()
        self.addNewCarButtonView.addNewCarButton.addTarget(self, action: #selector(self.tapAddNewCar(_:)), for: .touchUpInside)
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = ChooseCarFormViewM()
        }
    }
    
    private func roundCorners() {
        Threads.performTaskInMainQueue {
            self.gradientView.roundCorners([.topLeft,.topRight], radius: Constants.UIConstants.sizeRadius_18)
            self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
            self.dropDownView.makeLayer(color: UIColor.gray, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
            self.orLabel.roundCorners(Constants.UIConstants.sizeRadius_7)
        }
    }
    
    private func setupView() {
        self.nextButton.setTitle(StringConstants.Text.next.localizedString(), for: .normal)
    }
    
    private func requestChooseCarAPI() {
        self.viewModel?.requestChooseCarAPI(completion: { (success, carList) in
          if success {
            self.carListDataSource = carList
          }
        })
    }
    
    private func goToNextPage() {
        if self.selectedCarId == nil {
            Alert.showOkAlert(title:StringConstants.Text.AppName.localizedString() , message: "Please select a car first.".localizedString())
            return
        }
        switch self.chooseCarForServiceType {
        case .thirdPartyWarranty:
            let cThirdPartyWarrantyVC = DIConfigurator.sharedInstance.getCThirdPartyWarrantyVC()
            cThirdPartyWarrantyVC.carDetails = self.selectedCarDetails
            self.navigationController?.pushViewController(cThirdPartyWarrantyVC, animated: true)
            break
        case .serviceContract:
            let cServiceContractVC = DIConfigurator.sharedInstance.getCServiceContractVC()
            cServiceContractVC.carDetails = self.selectedCarDetails
            self.navigationController?.pushViewController(cServiceContractVC, animated: true)
            break
            
        case .serviceHistory:
            let cServiceHistoryVC = DIConfigurator.sharedInstance.getCServiceHistoryVC()
            cServiceHistoryVC.carDetails = self.selectedCarDetails
            self.navigationController?.pushViewController(cServiceHistoryVC, animated: true)
            break
            
        case .recoveryRequest:
            let cServiceHistoryVC = DIConfigurator.sharedInstance.getFirstRecoveryReqVC()
            cServiceHistoryVC.vehicleId = self.selectedCarId ?? 0
            cServiceHistoryVC.carDetails = self.selectedCarDetails
            self.navigationController?.pushViewController(cServiceHistoryVC, animated: true)
            break
        case .none:
            break
        
        case .inspectionRequest:
            let tenderRequestViewC = DIConfigurator.sharedInstance.getVITenderReqVC()
            tenderRequestViewC.vehicleId = self.selectedCarId
            tenderRequestViewC.carDetails = self.selectedCarDetails
            self.navigationController?.pushViewController(tenderRequestViewC, animated: true)
            break
        
        case .upholsteryRequest:
            let tenderRequestViewC = DIConfigurator.sharedInstance.getUSTenderReqVC()
            tenderRequestViewC.vehicleId = self.selectedCarId
            tenderRequestViewC.carDetails = self.selectedCarDetails
            self.navigationController?.pushViewController(tenderRequestViewC, animated: true)
            break
            
        case .carServicing:
            let tenderRequestViewC = DIConfigurator.sharedInstance.getServicingRequestViewC()
            tenderRequestViewC.vehicleId = self.selectedCarId
            tenderRequestViewC.carDetails = self.selectedCarDetails
            self.navigationController?.pushViewController(tenderRequestViewC, animated: true)
            break
            
        case .spareParts:
            let tenderRequestViewC = DIConfigurator.sharedInstance.getPTenderRequestVC()
            tenderRequestViewC.vehicleId = self.selectedCarId
            tenderRequestViewC.carDetails = self.selectedCarDetails
            self.navigationController?.pushViewController(tenderRequestViewC, animated: true)
            break
            
        case .wheels:
            let bookNowViewC = DIConfigurator.sharedInstance.getCBookNowVC()
            bookNowViewC.vehicleId = self.selectedCarId ?? 0
            bookNowViewC.carDetails = self.selectedCarDetails
            bookNowViewC.isSentFromChooseCar = true
            self.navigationController?.pushViewController(bookNowViewC, animated: true)
            break
            
        case .windowTinting:
            let tenderRequestViewC = DIConfigurator.sharedInstance.getWTTenderReqVC()
            tenderRequestViewC.vehicleId = self.selectedCarId
            tenderRequestViewC.carDetails = self.selectedCarDetails
            self.navigationController?.pushViewController(tenderRequestViewC, animated: true)
            break
            
        case .bankValuation:
            let tenderRequestViewC = DIConfigurator.sharedInstance.getBVTenderRequestVC()
            tenderRequestViewC.vehicleId = self.selectedCarId
            tenderRequestViewC.carDetails = self.selectedCarDetails
            self.navigationController?.pushViewController(tenderRequestViewC, animated: true)
            break
            
        case .insurance:
            let tenderRequestViewC = DIConfigurator.sharedInstance.getITenderRequestVC()
            tenderRequestViewC.vehicleId = self.selectedCarId
            tenderRequestViewC.carDetails = self.selectedCarDetails
            self.navigationController?.pushViewController(tenderRequestViewC, animated: true)
            break
            
        case .carDetailing:
            let tenderRequestViewC = DIConfigurator.sharedInstance.getCDOptionsViewC()
            tenderRequestViewC.vehicleId = self.selectedCarId
            tenderRequestViewC.carDetails = self.selectedCarDetails
            tenderRequestViewC.screenType = .carDetailing
            self.navigationController?.pushViewController(tenderRequestViewC, animated: true)
            
        case .bodyWork:
            let tenderRequestViewC = DIConfigurator.sharedInstance.getCDOptionsViewC()
            tenderRequestViewC.vehicleId = self.selectedCarId
            tenderRequestViewC.carDetails = self.selectedCarDetails
            tenderRequestViewC.screenType = .bodyWork
            self.navigationController?.pushViewController(tenderRequestViewC, animated: true)
        default:
            break
        }
    }
    
    private func showAlertForNoCars() {
        Alert.showOkAlert(title: StringConstants.Text.AppName, message: StringConstants.Text.noCarsAddedAlert.localizedString())
    }
    
    //MARK: - Selector Methods
    @objc func tapAddNewCar(_ sender: Any) {
        let addNewCarViewC = DIConfigurator.sharedInstance.getAddNewCarViewC()
        self.navigationController?.pushViewController(addNewCarViewC, animated: true)
    }
    
    //MARK: - IBActions
    @IBAction func tapSelectCar(_ sender: Any) {
        guard let listPopupView = SingleSelectListPopup.inistancefromNib() else { return }
        if self.carListDataSource.count == 0 {
            self.showAlertForNoCars()
            return
        }
        listPopupView.initializeViewWith(title: "Select Car".localizedString(), arrayList: self.carListDataSource, key: ConstantAPIKeys.title) { [weak self] (response) in
            guard let sSelf = self,
                let carName = response[ConstantAPIKeys.title] as? String,let plateNumber = response[ConstantAPIKeys.plateNumber] as? String,
                let carId = response[ConstantAPIKeys.id] as? Int else { return }
            sSelf.selectedCarId = carId
            sSelf.placeholderLabel.text = "\(carName), \(plateNumber)"
            if let jsonData = response.toJSONData() {
                sSelf.selectedCarDetails = ChooseCarModel(jsonData: jsonData)
            }
            
        }
        listPopupView.showWithAnimated(animated: true)
    }
    
     
    //MARK: - IBAction
    @IBAction func tapNext(_ sender: Any) {
        
        self.goToNextPage()
    }
}
