//
//  ImageViewerViewC.swift
//  AlbaCars
//
//  Created by Narendra on 2/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class ImageViewerViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    //MARK: - Variables
    var imageDataSource: [String] = []

        //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "All".localizedString(),barColor: .redButtonColor,titleColor: .white, leftBarButtonsType: [.backWhite], rightBarButtonsType: [])
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupCollectionView()
    }
    
    private func setupCollectionView() {
        self.registerNibs()
        self.imageCollectionView.delegate = self
        self.imageCollectionView.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //depending upon direction of collection view
        self.imageCollectionView?.setCollectionViewLayout(layout, animated: true)
    }
    
    private func registerNibs() {
        self.imageCollectionView.register(ImageViewerCollectionCell.self)
    }

}
