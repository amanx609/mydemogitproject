//
//  ImageViewerViewC+CollectionView.swift
//  AlbaCars
//
//  Created by Narendra on 2/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension ImageViewerViewC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.getCell(collectionView: collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/2-5
        return CGSize(width: width, height: width)
    }
    
}

extension ImageViewerViewC {
    
    func getCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        //imageDataSource
        let cell: ImageViewerCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(image: self.imageDataSource[indexPath.row])
        return cell
    }
}
