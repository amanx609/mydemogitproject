//
//  CarInfoView.swift
//  AlbaCars
//
//  Created by Kirti Kalra on 10/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CarInfoView: UIView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var vehicleImageView: UIImageView!
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var modelNameLabel: UILabel!
    @IBOutlet weak var vehicleNameLabel: UILabel!
    @IBOutlet weak var modelYearLabel: UILabel!
    @IBOutlet weak var odometerLabel: UILabel!
    @IBOutlet weak var optionsLabel: UILabel!
    @IBOutlet weak var priceButton: UIButton!
    @IBOutlet weak var bankFinanceLabel: UILabel!
    @IBOutlet weak var financePriceLabel: UILabel!
    @IBOutlet weak var refLabel: UILabel!

    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    var contentView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadNib()
    }
    
    private func loadNib() {
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of:self))
        let nib = UINib(nibName: "CarInfoView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func setupViewForPayment() {
        self.bankFinanceLabel.isHidden = true
        self.financePriceLabel.isHidden = true
        self.optionsLabel.text = Constants.UIKeys.status.localizedString()
        self.optionsLabel.text = "Reserved".localizedString()
    }
    
    func setupViewForBankFinance(cellData: CarFinanceStatusModel) {
        self.bankFinanceLabel.isHidden = true
        self.financePriceLabel.isHidden = true
        self.vehicleNameLabel.isHidden = true
        //Image
        if let vehicleImage = cellData.vehicleImage?.first {
            self.vehicleImageView.setImage(urlStr: Helper.toString(object: vehicleImage), placeHolderImage: nil)
        }
        //Brand
        self.brandNameLabel.text = cellData.brand
        self.modelNameLabel.text = cellData.title
        
        //ModelYear
        if let brand = cellData.brand {
            let brandText = String.getAttributedText(firstText: "Brand :".localizedString(), secondText: " \(brand)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.modelYearLabel.attributedText = brandText
        }
        
        
        //ModelYear
        if let modelYear = cellData.year {
            let modelYearText = String.getAttributedText(firstText: "Year :".localizedString(), secondText: " \(modelYear)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.odometerLabel.attributedText = modelYearText
        }
        
        
        //Status
        if let status = cellData.status {
            let vehicleOptionText = String.getAttributedText(firstText: "Status :".localizedString(), secondText: " \(status)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.optionsLabel.attributedText = vehicleOptionText
        }
        
        //Price
        if let price = cellData.price {
            let priceText = "AED".localizedString() + " \(price)"
            self.priceButton.setTitle(priceText, for: .normal)
        }
        
        //Reference Number
        if let referenceNumber = cellData.getReferenceNumber() {
            let referenceNumberText = String.getAttributedText(firstText: "Ref Number :".localizedString(), secondText: referenceNumber, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.refLabel.attributedText = referenceNumberText
            self.refLabel.isHidden = false
        } else {
            self.refLabel.isHidden = true
        }
        
    }
    
    func setupViewForBuyCars(vehicle: Vehicle) {
        //Image
        if let vehicleImage = vehicle.vehicleImages?.first {
            self.vehicleImageView.setImage(urlStr: Helper.toString(object: vehicleImage), placeHolderImage: nil)
        }
        self.brandNameLabel.text = vehicle.brandName
        self.modelNameLabel.text = vehicle.modelName
        self.vehicleNameLabel.text = vehicle.carTitle
        //ModelYear
        if let modelYear = vehicle.year {
            let modelYearText = String.getAttributedText(firstText: "Model :".localizedString(), secondText: " \(modelYear)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.modelYearLabel.attributedText = modelYearText
        }
        //Odometer
        if let odometer = vehicle.mileage {
            let intOdometer = Int(odometer).formattedWithSeparator
            let odometerText = String.getAttributedText(firstText: "Odometer :".localizedString(), secondText: " \(intOdometer) Km", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.odometerLabel.attributedText = odometerText
        }
        //Options
        if let vehicleOption = vehicle.vehicleOption {
            let vehicleOptionText = String.getAttributedText(firstText: "Option :".localizedString(), secondText: " \(vehicleOption)", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.optionsLabel.attributedText = vehicleOptionText
        }
        //Price
        if let price = vehicle.price {
            let intPrice = Int(price).formattedWithSeparator
            let priceText = "AED".localizedString() + " \(intPrice)"
            self.priceButton.setTitle(priceText, for: .normal)
        }
        //FinancePrice
        if let financePrice = vehicle.monthlyInstallment {
            let intMonthlyInstalment = Int(financePrice).formattedWithSeparator
            let monthlyInstalmentText = " \(intMonthlyInstalment)/" + "Month".localizedString()
            let financePriceText = String.getAttributedText(firstText: "AED".localizedString(), secondText: monthlyInstalmentText, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.financePriceLabel.attributedText = financePriceText
        } else {
             self.financePriceLabel.text = ""
        }
    }
    
    func setupViewForCSTPaymentHistory(paymentHistory: CustomerPaymentHistoryModel) {
        //Image
        if let vehicleImage = paymentHistory.vimgs?.first {
            self.vehicleImageView.setImage(urlStr: Helper.toString(object: vehicleImage.last), placeHolderImage: nil)
        }
        self.brandNameLabel.text = paymentHistory.getSmartTenderType()
        self.modelNameLabel.text = paymentHistory.description
        self.vehicleNameLabel.isHidden = true
        //self.vehicleNameLabel.text = paymentHistory.carTitle
        //Brand
        
        let brandNameText = String.getAttributedText(firstText: "Brand :".localizedString(), secondText: Helper.toString(object: paymentHistory.brandName), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
        self.modelYearLabel.attributedText = brandNameText
        //Model
        let modelText = String.getAttributedText(firstText: "Model :".localizedString(), secondText: Helper.toString(object: paymentHistory.modelName), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
        self.odometerLabel.attributedText = modelText
        
        //Options
        
        let vehicleOptionText = String.getAttributedText(firstText: "Date :".localizedString(), secondText: paymentHistory.date?.getDateStringFrom(inputFormat: Constants.Format.dateFormatyyyyMMdd, outputFormat: Constants.Format.dateFormatdMMMyyyy) ?? "", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
        self.optionsLabel.attributedText = vehicleOptionText
        
        //Price
        if let price = paymentHistory.price {
            let intPrice = Int(price).formattedWithSeparator
            let priceText = "AED".localizedString() + " \(intPrice)"
            self.priceButton.setTitle(priceText, for: .normal)
        }
        
        //Reference Number
        if let referenceNumber = paymentHistory.getReferenceNumber() {
            let referenceNumberText = String.getAttributedText(firstText: "Ref Number :".localizedString(), secondText: referenceNumber, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.refLabel.attributedText = referenceNumberText
            self.refLabel.isHidden = false
        } else {
            self.refLabel.isHidden = true
        }
        
    }
    
    func setupViewForCustomerPaymentHistory(paymentHistory: CustomerPaymentHistoryModel) {
        //Image
        if let vehicleImage = paymentHistory.image {
            self.vehicleImageView.setImage(urlStr: Helper.toString(object: vehicleImage.last), placeHolderImage: #imageLiteral(resourceName: "profileImgPlaceholder"))
        }
        self.brandNameLabel.text = paymentHistory.title
        self.modelNameLabel.text = paymentHistory.description
        self.vehicleNameLabel.isHidden = true
        //self.vehicleNameLabel.text = paymentHistory.carTitle
        //Brand
        
        let brandNameText = String.getAttributedText(firstText: "Brand :".localizedString(), secondText: Helper.toString(object: paymentHistory.brandName), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
        self.modelYearLabel.attributedText = brandNameText
        //Model
        let modelText = String.getAttributedText(firstText: "Model :".localizedString(), secondText: Helper.toString(object: paymentHistory.modelName), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
        self.odometerLabel.attributedText = modelText
        
        //Options
        
        let vehicleOptionText = String.getAttributedText(firstText: "Date :".localizedString(), secondText: paymentHistory.date?.getDateStringFrom(inputFormat: Constants.Format.dateFormatyyyyMMdd, outputFormat: Constants.Format.dateFormatdMMMyyyy) ?? "", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
        self.optionsLabel.attributedText = vehicleOptionText
        
        //Price
        if let price = paymentHistory.price {
            let intPrice = Int(price).formattedWithSeparator
            let priceText = "AED".localizedString() + " \(intPrice)"
            self.priceButton.setTitle(priceText, for: .normal)
        }
        
        //Reference Number
        if let referenceNumber = paymentHistory.getReferenceNumber() {
            let referenceNumberText = String.getAttributedText(firstText: "Ref Number :".localizedString(), secondText: referenceNumber, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            self.refLabel.attributedText = referenceNumberText
            self.refLabel.isHidden = false
        } else {
            self.refLabel.isHidden = true
        }
        
    }
    
    func setupViewForMyWatchList(watchListData: MyWatchListModel) {
        self.brandNameLabel.text = watchListData.brandName
        self.modelNameLabel.text = watchListData.description
        self.vehicleNameLabel.isHidden = true
        //Brand
        let brandNameText = String.getAttributedText(firstText: "Brand : ".localizedString(), secondText: Helper.toString(object: watchListData.brandName), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
        self.modelYearLabel.attributedText = brandNameText
        
        //Model
        let modelText = String.getAttributedText(firstText: "Model : ".localizedString(), secondText: Helper.toString(object: watchListData.modelName), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
        self.odometerLabel.attributedText = modelText
        
        //Options
        let vehicleOptionText = String.getAttributedText(firstText: "Alert : ".localizedString(), secondText: Helper.toString(object: watchListData.getAlertType()), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
        self.optionsLabel.attributedText = vehicleOptionText
        if let vehicleImage = watchListData.images?.first {
            self.vehicleImageView.setImage(urlStr: Helper.toString(object: vehicleImage.last), placeHolderImage: nil)
        }
        //Price
//        if let price = watchListData.price {
//            let intPrice = Int(price).formattedWithSeparator
//            let priceText = "AED".localizedString() + " \(intPrice)"
//            self.priceButton.setTitle(priceText, for: .normal)
//        }
    }
}
