//
//  HomeInfoView.swift
//  AlbaCars
//
//  Created by Narendra on 12/13/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class HomeInfoView: UIView {

    //MARK: - IBOutlets
       @IBOutlet weak var bgView: UIView!
       @IBOutlet weak var placeholderImageView: UIImageView!
       @IBOutlet weak var titleLabel: UILabel!
       @IBOutlet weak var placeholderImageViewWidth: NSLayoutConstraint!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    var contentView: UIView!
       override init(frame: CGRect) {
           super.init(frame: frame)
           self.loadNib()
           
       }
       
       required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           self.loadNib()
       }
       
       private func loadNib() {
           contentView = loadViewFromNib()
           contentView.frame = bounds
           contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
           addSubview(contentView)
           self.configureView()
           
       }
       
       func loadViewFromNib() -> UIView {
           let bundle = Bundle(for: type(of:self))
           let nib = UINib(nibName: "HomeInfoView", bundle: bundle)
           let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
           return view
       }
       
       private func configureView() {
          // self.bgView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_7)
          //  self.bgView.addShadow(ofColor:  UIColor.lightGray, radius: Constants.UIConstants.sizeRadius_7)
           self.layoutIfNeeded()
       }
}
