//
//  AddNewCarButtonView.swift
//  AlbaCars
//
//  Created by Narendra on 1/10/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
import QuartzCore
class AddNewCarButtonView: UIView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var addNewCarButton: UIButton!
    
    var contentView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadNib()
    }
    
    private func loadNib() {
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView)
        self.configureView()
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of:self))
        let nib = UINib(nibName: "AddNewCarButtonView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    private func configureView() {
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_3half)
       self.layoutIfNeeded()

        Threads.performTaskInMainQueue {
            self.contentView.addRoundDashedLine(color: .redButtonColor, backgroundColor: .pinkButtonColor)
        }        
    }
    
}
