//
//  WebViewVController.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import WebKit
import MessageUI

enum WebContentType {
    
    case none
    case privacyPolicy
    case termsConditions
    
    var url: String? {
        switch self {
        case .none: return "https://www.google.com/"
        case .privacyPolicy: return "getCMSPage?key=policies"
        case .termsConditions: return "getCMSPage?key=termsConditions"
        }
    }
    
    var title: String? {
        switch self {
        case .privacyPolicy: return "Privacy policy"
        case .termsConditions: return "Terms & Conditions"
        case .none:
            return ""
        }
    }
}

class WebViewVController: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var albaCarWebView: WKWebView!
    
    //MARK: - Variables
    var webContentType: WebContentType = .none
    
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        
    }
    
    //MARK: - Private Methods
    
    private func setup() {
        self.albaCarWebView.backgroundColor = UIColor.white
        self.albaCarWebView.navigationDelegate = self
        self.albaCarWebView.uiDelegate = self

        Loader.showLoader()
        let url = APIEnvironment.Base.rawValue + Helper.toString(object: webContentType.url)
        self.loadURL(url: url)
        //    switch webContentType {
        //    case .privacyPolicy: self.loadURL(url: webContentType.url ?? "") // privacy policy
        //    case .termsConditions: self.loadURL(url: webContentType.url ?? "") // terms & conditions
        //    default:
        //      break
        //    }
        self.setupNavigationBarTitle(title: webContentType.title?.localizedString() ?? "", leftBarButtonsType: [.back], rightBarButtonsType: [])
    }
    
    private func loadURL(url: String) {
        guard let url = URL(string: url) else {
            return }
        self.albaCarWebView.load(URLRequest(url: url))
    }
}

extension WebViewVController: WKNavigationDelegate,WKUIDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Loader.hideLoader()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void) {
        
        let URLString = navigationAction.request.url?.relativeString.trimSpace().replacingOccurrences(of: "mailto:", with: "") ?? ""
        if URLString.isValidEmail(){
            self.sendEmail(email: URLString)
            decisionHandler(.cancel)
        } else {
            decisionHandler(.allow)
        }
    }
    
}

extension WebViewVController {
    
    func sendEmail(email: String) {
        print(sendEmail)
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            present(mail, animated: true)
        } else {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Please login your mail on device.".localizedString())
        }
    }
    
    func openUrl(url: URL) {
        UIApplication.shared.open(url)
    }
    
}

extension WebViewVController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
