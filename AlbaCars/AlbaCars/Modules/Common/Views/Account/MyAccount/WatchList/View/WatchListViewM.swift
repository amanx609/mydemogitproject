//
//  WatchListViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol WatchListViewModeling {
    func requestMyWatchListAPI(param: APIParams, completion: @escaping (Int, Int,[MyWatchListModel]) -> Void)
}

class WatchListViewM: WatchListViewModeling {
    
    func requestMyWatchListAPI(param: APIParams, completion: @escaping (Int, Int,[MyWatchListModel]) -> Void) {
           
           APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .myWatchList(param: param))) { (response, success) in
               if success {
                   if let safeResponse =  response as? [String: AnyObject],
                       let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                       let nextPageNumber = result[kNextPageNum] as? Int,
                       let perPage = result[kPerPage] as? Int,
                       let resultData = result.toJSONData(),
                       let payments = MyWatchList(jsonData: resultData) {
                       completion(nextPageNumber, perPage, payments.watchList ?? [])
                   }
               }
           }
       }
}
