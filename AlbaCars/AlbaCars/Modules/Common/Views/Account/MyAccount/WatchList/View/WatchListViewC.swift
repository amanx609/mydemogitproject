//
//  WatchListViewC.swift
//  AlbaCars
//
//  Created by Admin on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class WatchListViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var watchListTableView: UITableView!
    @IBOutlet weak var noDataView: UIView!
    //MARK: - Variables
    var watchListDataSource: [MyWatchListModel] = []
    var viewModel: WatchListViewModeling?
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.watchListDataSource = []
        self.requestWatchListAPI()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupTableView()
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = WatchListViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.watchListTableView.delegate = self
        self.watchListTableView.dataSource = self
        self.watchListTableView.separatorStyle = .none
        //self.watchListTableView.allowsSelection = false
    }
    
    private func registerNibs() {
        self.watchListTableView.register(BuyCarsTableCell.self)
    }
    
     func requestWatchListAPI() {
        var params = APIParams()
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        self.viewModel?.requestMyWatchListAPI(param: params, completion: { [weak self] (nextPageNumber, perPage, watchListArray) in
            guard let sSelf = self else { return }
            
            sSelf.watchListDataSource = sSelf.watchListDataSource + watchListArray
            if sSelf.watchListDataSource.count == 0{
                sSelf.noDataView.isHidden = false
            }else{
                sSelf.noDataView.isHidden = true
            }
            sSelf.nextPageNumber = nextPageNumber
            sSelf.perPage = perPage
            Threads.performTaskInMainQueue {
                sSelf.watchListTableView.reloadData()
            }
        })
    }
    
}
