//
//  WatchListViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension WatchListViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return watchListDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellData = self.watchListDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellData: cellData)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.CellHeightConstants.height_156
    }
}

extension WatchListViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellData: MyWatchListModel) -> UITableViewCell {
        let cell: BuyCarsTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureViewForWatchList(cellData: cellData)
        return cell
    }
    
}
