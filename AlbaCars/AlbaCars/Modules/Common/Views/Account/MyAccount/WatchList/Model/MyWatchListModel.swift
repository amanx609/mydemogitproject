//
//  MyWatchListModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

class MyWatchListModel: BaseCodable {
    
    var id: Int?
    var user_id: Int?
    var vehicle_id: Int?
    var alert: Int?
    var brandName: String?
    var modelName: String?
    var typeName: String?
    var year: Int?
    var brand: String?
    var description: String?
    var images: [String]?
    
    func getAlertType() -> String {
        switch self.alert {
        case 1:
            return "Price Drop".localizedString()
        case 2:
            return "Similar Cars".localizedString()
        case 3:
            return "Available".localizedString()
        default:
            
            break
        }
        return ""
        
    }
}

class MyWatchList: BaseCodable {
    var watchList: [MyWatchListModel]?
    
    private enum CodingKeys: String, CodingKey {
        case watchList = "data"
    }
}
