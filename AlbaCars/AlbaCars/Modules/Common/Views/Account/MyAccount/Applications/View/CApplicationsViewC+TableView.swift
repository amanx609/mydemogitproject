//
//  ApplicationsViewC+TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension CApplicationsViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.applicationsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.applicationsDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.applicationsDataSource[indexPath.row].height
    }
}

extension CApplicationsViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .ApplicationsTableViewCell:
            let cell: ApplicationsTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
}

extension CApplicationsViewC: ApplicationsTableViewCellDelegate {
    
    func didTapApplicationsCell(cell: ApplicationsTableViewCell) {
        guard let indexPath = self.applicationTableView.indexPath(for: cell) else { return }
        switch indexPath.row {
        case 0:
            let bankFinanceVC = DIConfigurator.sharedInstance.getCarFinanceStatusVC()
            self.navigationController?.pushViewController(bankFinanceVC, animated: true)
            break
        case 1:
            let sellCarIntroVC = DIConfigurator.sharedInstance.getSellCarListVC()
            self.navigationController?.pushViewController(sellCarIntroVC, animated: true)
            break
        case 2:
            let requestCarVC = DIConfigurator.sharedInstance.getCRequestedCarListVC()
            self.navigationController?.pushViewController(requestCarVC, animated: true)
        break
        default:
            break
        }
    }
}


