//
//  ApplicationsViewC.swift
//  AlbaCars
//
//  Created by Sakshi  on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class CApplicationsViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var applicationTableView: UITableView!
    @IBOutlet weak var noDataView: UIView!
    //MARK: - Variables
    var applicationsDataSource: [CellInfo] = []
    var viewModel: CApplicationsVModeling?
    
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupTableView()
        if let dataSource = self.viewModel?.getApplicationsDataSource() {
          self.applicationsDataSource = dataSource
        }
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
             self.viewModel = CApplicationsViewM()
           }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.applicationTableView.delegate = self
        self.applicationTableView.dataSource = self
        self.applicationTableView.separatorStyle = .none
        self.applicationTableView.allowsSelection = false
        self.applicationTableView.bounces = false
    }
    
    private func registerNibs() {
        self.applicationTableView.register(ApplicationsTableViewCell.self)
    }
}
