//
//  ApplicationsTableViewCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/23/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol ApplicationsTableViewCellDelegate: class {
    
    func didTapApplicationsCell(cell: ApplicationsTableViewCell)
}

class ApplicationsTableViewCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var applicationImageView: UIImageView!
    @IBOutlet weak var applicationNameLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    //MARK: - Variables
    weak var delegate: ApplicationsTableViewCellDelegate?
    
    //MARK: - Lifecycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.setup()
    }
    
    //MARK: - Private Methods
    func setup() {
        Threads.performTaskInMainQueue {
            self.bgView.roundCorners(Constants.UIConstants.sizeRadius_7)
            self.bgView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: 1, round: Constants.UIConstants.sizeRadius_7)
            self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
            
        }
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            //Image
            if let placeholderImage = info[Constants.UIKeys.placeholderImage] as? UIImage {
                
                self.applicationImageView.image = placeholderImage
            }
        }
        self.applicationNameLabel.text = cellInfo.placeHolder
    }
    
    //MARK: - IBActions
    
    @IBAction func cellButtonTapped(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapApplicationsCell(cell: self)
        }
    }
}
