//
//  ApplicationsViewModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol CApplicationsVModeling: BaseVModeling {
  func getApplicationsDataSource() -> [CellInfo]
}

class CApplicationsViewM: CApplicationsVModeling {
  
  func getApplicationsDataSource() -> [CellInfo] {
    
    var array = [CellInfo]()
    
    //Bank Finance Cell
    var bankFinanceInfo = [String: AnyObject]()
    bankFinanceInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "bankMoney")
    let bankFinanceCell = CellInfo(cellType: .ApplicationsTableViewCell, placeHolder: "Bank Finance".localizedString(), value: "", info: bankFinanceInfo, height: Constants.CellHeightConstants.height_80)
    array.append(bankFinanceCell)
    
    //Sell a Car Cell
    var sellCarInfo = [String: AnyObject]()
    sellCarInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "carOrange")
    let sellCarCell = CellInfo(cellType: .ApplicationsTableViewCell, placeHolder: "Sell a Car".localizedString(), value: "", info: sellCarInfo, height: Constants.CellHeightConstants.height_80)
    array.append(sellCarCell)
    
    //Request a Car Cell
    var requestCarInfo = [String: AnyObject]()
    requestCarInfo[Constants.UIKeys.placeholderImage] = #imageLiteral(resourceName: "carRed")
    let requestCarCell = CellInfo(cellType: .ApplicationsTableViewCell, placeHolder: "Request a Car".localizedString(), value: "", info: requestCarInfo, height: Constants.CellHeightConstants.height_80)
    array.append(requestCarCell)
    return array
    
  }
}

