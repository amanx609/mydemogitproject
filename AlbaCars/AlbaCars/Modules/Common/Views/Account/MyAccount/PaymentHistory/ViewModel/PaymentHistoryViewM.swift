//
//  PaymentHistoryViewM.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol PaymentHistoryVModeling: BaseVModeling {
    func getPaymentHistoryDataSource(historyDetails: [CustomerPaymentHistoryModel]?, selectedFilter: Int) -> [CellInfo]
    func getSupplierPaymentHistoryDataSource(historyDetails: [SupplierPaymentHistoryModel]?, selectedFilter: Int) -> [CellInfo]
    func requestCustomerPaymentHistoryAPI(param: APIParams, completion: @escaping (Int, Int,[CustomerPaymentHistoryModel]) -> Void)
    func requestSupplierPaymentHistoryAPI(param: APIParams, completion: @escaping (Int, Int,[SupplierPaymentHistoryModel]) -> Void)
}

class PaymentHistoryViewM: PaymentHistoryVModeling {
    
    func getPaymentHistoryDataSource(historyDetails: [CustomerPaymentHistoryModel]?, selectedFilter: Int) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Bank Finance Cell
        var filterCellInfo = [String: AnyObject]()
        //filterCellInfo[]
        filterCellInfo[Constants.UIKeys.selectedButtonIndex] = selectedFilter as AnyObject
        if let userType = UserSession.shared.userType {
            if userType == .customer {
                filterCellInfo[Constants.UIKeys.paymentFilterDataSource] = self.getCollectionViewDataSource(selectedIndex: Helper.toInt(filterCellInfo[Constants.UIKeys.selectedButtonIndex])) as AnyObject
            } else if userType == .dealer {
                filterCellInfo[Constants.UIKeys.paymentFilterDataSource] = self.getDealerCollectionViewDataSource(selectedIndex: Helper.toInt(filterCellInfo[Constants.UIKeys.selectedButtonIndex])) as AnyObject
            }
          
        }
        
        let paymentFilterCell = CellInfo(cellType: .PaymentCollectionTableViewCell, placeHolder: "".localizedString(),value: "", info: filterCellInfo, height: Constants.CellHeightConstants.height_100)
        array.append(paymentFilterCell)
        
        //Sell a Car Cell
        if let historyDetails = historyDetails {
            for i in 0..<historyDetails.count {
                var carPaymentCellInfo = [String: AnyObject]()
                carPaymentCellInfo[Constants.UIKeys.paymentHistory] = historyDetails[i]
                let carPaymentCell = CellInfo(cellType: .PaymentHistoryTableCell, placeHolder: "".localizedString(), value: "", info: carPaymentCellInfo, height: Constants.CellHeightConstants.height_180)
                array.append(carPaymentCell)
            }
        }
        
        
        
        return array
    }
    
    func getCollectionViewDataSource(selectedIndex: Int) -> [[String: AnyObject]] {
        let array = [[Constants.UIKeys.filterName: "Smart Tenders".localizedString(), Constants.UIKeys.isFilterSelected: selectedIndex == 0 ? true : false],
                     [Constants.UIKeys.filterName: "Cars".localizedString(), Constants.UIKeys.isFilterSelected:  selectedIndex == 1 ? true : false],
                     [Constants.UIKeys.filterName: "Offers & Deals".localizedString(), Constants.UIKeys.isFilterSelected:  selectedIndex == 2 ? true : false],
                     [Constants.UIKeys.filterName: "Service Contract".localizedString(), Constants.UIKeys.isFilterSelected:  selectedIndex == 3 ? true : false],
                     [Constants.UIKeys.filterName: "Warranty".localizedString(), Constants.UIKeys.isFilterSelected:  selectedIndex == 4 ? true : false],
                     [Constants.UIKeys.filterName: "Service History".localizedString(), Constants.UIKeys.isFilterSelected:  selectedIndex == 5 ? true : false]] as [[String: AnyObject]]
        return array
    }
    
    func getDealerCollectionViewDataSource(selectedIndex: Int) -> [[String: AnyObject]] {
        let array = [[Constants.UIKeys.filterName: "Smart Tenders".localizedString(), Constants.UIKeys.isFilterSelected: selectedIndex == 0 ? true : false],
                     [Constants.UIKeys.filterName: "Auctions".localizedString(), Constants.UIKeys.isFilterSelected:  selectedIndex == 1 ? true : false],
                     [Constants.UIKeys.filterName: "Leads".localizedString(), Constants.UIKeys.isFilterSelected:  selectedIndex == 2 ? true : false],
                     [Constants.UIKeys.filterName: "Fleet Bidding".localizedString(), Constants.UIKeys.isFilterSelected:  selectedIndex == 3 ? true : false],
                     [Constants.UIKeys.filterName: "Warranty".localizedString(), Constants.UIKeys.isFilterSelected:  selectedIndex == 4 ? true : false],
                     [Constants.UIKeys.filterName: "Service History".localizedString(), Constants.UIKeys.isFilterSelected:  selectedIndex == 5 ? true : false]] as [[String: AnyObject]]
        return array
    }
    
    func getSupplierPaymentHistoryDataSource(historyDetails: [SupplierPaymentHistoryModel]?, selectedFilter: Int) -> [CellInfo] {
        
        var array = [CellInfo]()
        
        //Bank Finance Cell
        var filterCellInfo = [String: AnyObject]()
        filterCellInfo[Constants.UIKeys.selectedButtonIndex] = selectedFilter as AnyObject
        filterCellInfo[Constants.UIKeys.paymentFilterDataSource] = self.getSupplierCollectionViewDataSource(selectedIndex: Helper.toInt(filterCellInfo[Constants.UIKeys.selectedButtonIndex])) as AnyObject
        let paymentFilterCell = CellInfo(cellType: .PaymentCollectionTableViewCell, placeHolder: "".localizedString(),value: "", info: filterCellInfo, height: Constants.CellHeightConstants.height_75)
        array.append(paymentFilterCell)
        
        //Sell a Car Cell
        if let historyDetails = historyDetails {
            for i in 0..<historyDetails.count {
                var carPaymentCellInfo = [String: AnyObject]()
                carPaymentCellInfo[Constants.UIKeys.paymentHistory] = historyDetails[i]
                let carPaymentCell = CellInfo(cellType: .SPaymentHistoryCell, placeHolder: "".localizedString(), value: "", info: carPaymentCellInfo, height: Constants.CellHeightConstants.height_150)
                array.append(carPaymentCell)
            }
        }
        
        
        
        return array
        
    }
    
    func requestCustomerPaymentHistoryAPI(param: APIParams, completion: @escaping (Int, Int,[CustomerPaymentHistoryModel]) -> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .customerPaymentHistory(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let resultData = result.toJSONData(),
                    let payments = CustomerPayments(jsonData: resultData) {
                    completion(nextPageNumber, perPage, payments.payments ?? [])
                }
            }
        }
    }
    
    
    func requestSupplierPaymentHistoryAPI(param: APIParams, completion: @escaping (Int, Int,[SupplierPaymentHistoryModel]) -> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .supplierPaymentHistory(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let nextPageNumber = result[kNextPageNum] as? Int,
                    let perPage = result[kPerPage] as? Int,
                    let resultData = result.toJSONData(),
                    let payments = SupplierPayments(jsonData: resultData) {
                    
                    
                    completion(nextPageNumber, perPage, payments.spayments ?? [])
                }
            }
        }
    }
    
    func getSupplierCollectionViewDataSource(selectedIndex: Int) -> [[String: AnyObject]] {
        
        let array = [[Constants.UIKeys.filterName: "Paid".localizedString(), Constants.UIKeys.isFilterSelected: selectedIndex == 0 ? true : false],
                     [Constants.UIKeys.filterName: "Pending".localizedString(), Constants.UIKeys.isFilterSelected: selectedIndex == 1 ? true : false],
                     [Constants.UIKeys.filterName: "All".localizedString(), Constants.UIKeys.isFilterSelected: selectedIndex == 2 ? true : false]] as [[String: AnyObject]]
        return array
    }
}


