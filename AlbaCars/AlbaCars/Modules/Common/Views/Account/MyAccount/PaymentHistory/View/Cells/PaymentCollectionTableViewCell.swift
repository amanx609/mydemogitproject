//
//  PaymentCollectionTableViewCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

protocol PaymentCollectionTableViewCellDelegate: class {
    func didSelectFilterForSupplier(cell: PaymentCollectionTableViewCell, filterValue: SPaymentFiltersId, filterIndex: Int)
    func didSelectFilterForCustomer(cell: PaymentCollectionTableViewCell, filterValue: CPaymentFiltersId, filterIndex: Int)
}

class PaymentCollectionTableViewCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var paymentFilterCollectionView: UICollectionView!
    
    //MARK: - Variables
    var paymentFilterDataSource: [CellInfo] = []
    var paymentFilterDelegate: PaymentCollectionTableViewCellDelegate?
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupCollectionView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK: - Private Methods
    
    private func setupCollectionView() {
        self.registerNibs()
        self.paymentFilterCollectionView.dataSource = self
        self.paymentFilterCollectionView.delegate = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //depending upon direction of collection view
        self.paymentFilterCollectionView?.setCollectionViewLayout(layout, animated: true)
    }
    
    private func registerNibs() {
        self.paymentFilterCollectionView.register(PaymentFilterCollectionCell.self)
    }
    
    private func getCollectionViewDataSource(data: [[String: AnyObject]]) -> [CellInfo] {
        var array = [CellInfo]()
        //Smart Tenders Cell
        for i in 0..<data.count {
            var paymentFilterInfo = [String: AnyObject]()
            paymentFilterInfo[Constants.UIKeys.isFilterSelected] = data[i][Constants.UIKeys.isFilterSelected]
            if let filterName = data[i][Constants.UIKeys.filterName] as? String {
                let paymentFilterCell = CellInfo(cellType: .HomeCollectionCell, placeHolder: filterName , value: "", info: paymentFilterInfo, height: Constants.CellHeightConstants.height_30)
                array.append(paymentFilterCell)
            }
            
        }
        return array
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        
        if let info = cellInfo.info, let dataSource = info[Constants.UIKeys.paymentFilterDataSource] as? [[String: AnyObject]] {
            self.paymentFilterDataSource = self.getCollectionViewDataSource(data: dataSource)
            self.paymentFilterCollectionView.reloadData()
        }
    }
    
}

extension PaymentCollectionTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.paymentFilterDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellInfo = self.paymentFilterDataSource[indexPath.row]
        return self.getCell(collectionView: collectionView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.bounds.width/3 - 10
        
        return CGSize(width: width, height: self.paymentFilterDataSource[indexPath.row].height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        if let info = self.paymentFilterDataSource[indexPath.row].info, let isCellSelected = info[Constants.UIKeys.isFilterSelected] as? Bool {
         
            for i in 0..<self.paymentFilterDataSource.count {
                if i != indexPath.row {
                     self.paymentFilterDataSource[i].info?[Constants.UIKeys.isFilterSelected] = false as AnyObject
                }
        }
            
        self.paymentFilterDataSource[indexPath.row].info?[Constants.UIKeys.isFilterSelected] = !isCellSelected as AnyObject
            
            if let delegate = self.paymentFilterDelegate {
                if let userType = UserSession.shared.userType {
                if userType == .supplier {
                    var filterValue: SPaymentFiltersId = .none
                    switch indexPath.row {
                    case 0:
                        filterValue = .paid
                    case 1:
                        filterValue = .pending
                    case 2:
                        filterValue = .all
                    default:
                        filterValue = .none
                    }
                    delegate.didSelectFilterForSupplier(cell: self, filterValue: filterValue, filterIndex: indexPath.row)
                } else if userType == .customer {
                    if let filterValue = CPaymentFiltersId(rawValue: indexPath.row + 1) {
                        delegate.didSelectFilterForCustomer(cell: self, filterValue: filterValue, filterIndex: indexPath.row)
                    }
                    
                } else if userType == .dealer {
                    var filterValue: CPaymentFiltersId = .none
                    switch indexPath.row {
                    case 0:
                        filterValue = .smartTenders
                    case 1:
                        filterValue = .auction
                    case 2:
                        filterValue = .leads
                    case 3:
                        filterValue = .fleetBidding
                    case 4:
                        filterValue = .warranty
                    case 5:
                        filterValue = .serviceHistory
                    default:
                        break
                    }
                    delegate.didSelectFilterForCustomer(cell: self, filterValue: filterValue, filterIndex: indexPath.row)
                    }
                }
            }
            self.paymentFilterCollectionView.reloadData()
            
        }
    }
    
}

extension PaymentCollectionTableViewCell {
    func getCell(collectionView: UICollectionView, indexPath: IndexPath, cellInfo: CellInfo) -> UICollectionViewCell {
        let cell: PaymentFilterCollectionCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(cellInfo: cellInfo)
        return cell
    }
}

