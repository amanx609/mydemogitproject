//
//  PaymentHistoryTableCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 1/8/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class PaymentHistoryTableCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var carInfoView: CarInfoView!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.bgView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
        self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
         self.carInfoView.roundCorners(Constants.UIConstants.sizeRadius_3half)
         self.carInfoView.setupViewForPayment()
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo, selectedFilter: CPaymentFiltersId) {
        if let info = cellInfo.info {
            if let paymentDetails = info[Constants.UIKeys.paymentHistory] as? CustomerPaymentHistoryModel {
                switch selectedFilter {
                case .smartTenders:
                     self.carInfoView.setupViewForCSTPaymentHistory(paymentHistory: paymentDetails)
                default:
                    self.carInfoView.setupViewForCustomerPaymentHistory(paymentHistory: paymentDetails)
                }
               
            }
        }
    }
    
    func configureViewForSupplier(cellInfo: CellInfo, selectedFilter: SPaymentFiltersId) {
        
    }
}
