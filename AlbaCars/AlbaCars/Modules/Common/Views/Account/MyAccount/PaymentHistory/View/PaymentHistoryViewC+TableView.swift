//
//  PaymentHistoryViewC+TableView.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension PaymentHistoryViewC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.paymentHistoryDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = self.paymentHistoryDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath, cellInfo: cellInfo)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.paymentHistoryDataSource[indexPath.row].height
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    
       guard let lastVisibleRow = self.paymentHistoryTableView.indexPathsForVisibleRows?.last?.row else { return }
        if let userType = UserSession.shared.userType, userType == .supplier {
            if (self.spaymentHistoryDetails.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
                self.requestSupplierPaymentHistory()
            }
        } else {
            if (self.cpaymentHistoryDetails.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
              self.requestCustomerPaymentHistory()
            }
        }
         
     }
}

extension PaymentHistoryViewC {
    func getCell(tableView: UITableView, indexPath: IndexPath, cellInfo: CellInfo) -> UITableViewCell {
        guard let cellType = cellInfo.cellType else { return UITableViewCell() }
        switch cellType {
        case .PaymentCollectionTableViewCell:
            let cell: PaymentCollectionTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.paymentFilterDelegate = self
            cell.configureView(cellInfo: cellInfo)
            return cell
        case .PaymentHistoryTableCell:
            let cell: PaymentHistoryTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo, selectedFilter: self.cSelectedFilter)
            return cell
        case .SPaymentHistoryCell:
            let cell: SPaymentHistoryCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureView(cellInfo: cellInfo)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}

extension PaymentHistoryViewC: PaymentCollectionTableViewCellDelegate {
    func didSelectFilterForSupplier(cell: PaymentCollectionTableViewCell, filterValue: SPaymentFiltersId, filterIndex: Int) {
        
        self.selectedFilter = filterValue
        self.selectedIndex = filterIndex
        self.nextPageNumber = 1
        self.requestSupplierPaymentHistory()
    }
    
    func didSelectFilterForCustomer(cell: PaymentCollectionTableViewCell, filterValue: CPaymentFiltersId, filterIndex: Int) {
        
        self.cSelectedFilter = filterValue
        self.selectedIndex = filterIndex
        self.nextPageNumber = 1
        self.requestCustomerPaymentHistory()
    }
    
    
}


