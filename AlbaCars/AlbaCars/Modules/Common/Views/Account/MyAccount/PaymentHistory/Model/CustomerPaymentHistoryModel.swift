//
//  CustomerPaymentHistoryModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class CustomerPaymentHistoryModel: BaseCodable {
    
    var id: Int?
    var serviceCategoryId: Int?
    var referenceId: Int?
    var tenderType: Int?
    var date: String?
    var year: Int?
    var price: Double?
    var brandName: String?
    var brand: Int?
    var modelName: String?
    var typeName: String?
    var vimgs: [String]?
    var image: String?
    var description: String?
    var carTitle: String?
    var title: String?
    var refId: String?
    
    func getSmartTenderType() -> String {
        if let tenderType = self.tenderType, let tender = RecoveryHomeScreenType(rawValue: "\(tenderType)") {
            return tender.title ?? ""
        }
        return ""
    }
    
    func getReferenceNumber() -> String? {
        return " " + Helper.toString(object: self.refId)
    }
}

class CustomerPayments: BaseCodable {
    var payments: [CustomerPaymentHistoryModel]?
    
    private enum CodingKeys: String, CodingKey {
      case payments = "data"
    }
}
