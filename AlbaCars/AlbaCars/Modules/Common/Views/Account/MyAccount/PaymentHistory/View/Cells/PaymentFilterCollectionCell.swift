//
//  PaymentFilterCollectionCell.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit




class PaymentFilterCollectionCell: BaseCollectionViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var filterNameLabel: UILabel!
    
    //MARK: - Variables
    var isCellSelected = false
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    //MARK: - Private Methods
    private func setup() {
        Threads.performTaskInMainQueue {
                   self.bgView.roundCorners(Constants.UIConstants.sizeRadius_16)
                   self.bgView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: 1, round: Constants.UIConstants.sizeRadius_16)
               }
    }
    
    private func setSelectedBgColor() {
//        Threads.performTaskInMainQueue {
//            self.bgView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
//        }
        self.bgView.backgroundColor = .redButtonColor
        self.filterNameLabel.textColor = .white
    }
    
    private func setUnselectedBgColor() {
//        Threads.performTaskInMainQueue {
//            self.bgView.backgroundColor = UIColor.coralPinkButtonColor
//        }
        self.bgView.backgroundColor = .pinkButtonColor
        self.filterNameLabel.textColor = .redButtonColor
    }
    
    //MARK: Public Methods
    func configureView(cellInfo: CellInfo) {
        self.filterNameLabel.text = cellInfo.placeHolder
        if let info = cellInfo.info, let isfilterSelected = info[Constants.UIKeys.isFilterSelected] as? Bool {
            if isfilterSelected {
                self.setSelectedBgColor()
                
            } else {
                self.setUnselectedBgColor()
            }
        }
    }

}
