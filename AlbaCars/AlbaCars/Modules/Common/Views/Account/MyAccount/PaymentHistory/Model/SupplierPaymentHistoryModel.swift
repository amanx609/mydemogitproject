//
//  SupplierPaymentHistoryModel.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 2/27/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class SupplierPaymentHistoryModel: BaseCodable {
    
    var paymentStatus: Int?
    var date: String?
    var customerName: String?
    var contact: String?
    var bidPrice: Double?
    var tenderType: Int?
    var description: String?
    var jobDescription: String?
    var additionalInformation: String?
    var recoveryType: String?
    var typeOfService: String?
    var serviceOptions: String?
    var partType: Int?
    var partName: String?
    var wheelType: Int?
    var rimServiceType: Int?
    var wheelSize: String?
    var bodyServiceType: Int?
    var paintColor: String?
    var paintSiteBody: Int?
    var paintSiteInterior: Int?
    var paintSiteGlass: Int?
    var paintSiteRims: Int?
    var noOfDents: Int?
    var paintSiteArr: [Int]?
    var tintPercentage: Double?
    var tintColor: String?
    var carDetailingServiceType: Int?
    var carDetailingOptions: Int?
    var carPrice: Double?
    var bankId: String?
    var bankName: String?
    var typeOfInsurance: Int?
    var vehiclePrice: Int?
    
    func getPartType() -> String {
      if let part = self.partType, let prtType = SparePartType(rawValue: part ) {
            return prtType.title
        }
        return ""

    }
    
    
}

class SupplierPayments: BaseCodable {
    var spayments: [SupplierPaymentHistoryModel]?
    
    private enum CodingKeys: String, CodingKey {
      case spayments = "data"
    }
}



