//
//  SPaymentHistoryCell.swift
//  AlbaCars
//
//  Created by Narendra on 1/8/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class SPaymentHistoryCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var carSparePartsLabel: UILabel!
    @IBOutlet weak var bidPriceLabel: UILabel!
    @IBOutlet weak var carDescriptionLabel: UILabel!
    @IBOutlet weak var customerLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var priceRangeLabel: UILabel!
    @IBOutlet weak var tenderDetailsLabel: UILabel!
    @IBOutlet weak var partTypeLabel: UILabel!
    @IBOutlet weak var partNameLabel: UILabel!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    private func setupView() {
        
        Threads.performTaskInMainQueue {
            self.bgView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: Constants.UIConstants.borderWidth_1, round: Constants.UIConstants.sizeRadius_3half)
            self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        }
    }
    
    //MARK: - Public Methods
    func configureView(cellInfo: CellInfo) {
        if let info = cellInfo.info {
            if let paymentDetails = info[Constants.UIKeys.paymentHistory] as? SupplierPaymentHistoryModel {
                self.customerLabel.attributedText = String.getAttributedText(firstText: "Customer Name :".localizedString(), secondText: Helper.toString(object: paymentDetails.customerName), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
                
                self.contactLabel.attributedText = String.getAttributedText(firstText: "Contact :".localizedString(), secondText: Helper.toString(object: paymentDetails.contact), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
                
                let dateStr = Helper.toString(object: paymentDetails.date?.getDateStringFrom(inputFormat: Constants.Format.dateFormatyyyyMMdd, outputFormat: Constants.Format.dateFormatdMMMyyyy))
                self.priceRangeLabel.attributedText = String.getAttributedText(firstText: "Date :".localizedString(), secondText: dateStr, firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
                
                self.tenderDetailsLabel.text = "Tender Details".localizedString()
                
                self.bidPriceLabel.attributedText = String.getAttributedText(firstText: "Date :".localizedString(), secondText: "AED \(Helper.toString(object: paymentDetails.bidPrice))", firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
                
                self.partTypeLabel.attributedText = String.getAttributedText(firstText: "Part Type :".localizedString(), secondText: Helper.toString(object: paymentDetails.getPartType()), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
                
                 self.partNameLabel.attributedText = String.getAttributedText(firstText: "Part Name :".localizedString(), secondText: Helper.toString(object: paymentDetails.partName), firstTextColor: .black, secondTextColor: .redButtonColor, firstTextSize: .size_08, secondTextSize: .size_08)
            }
        }
    }
    
}
