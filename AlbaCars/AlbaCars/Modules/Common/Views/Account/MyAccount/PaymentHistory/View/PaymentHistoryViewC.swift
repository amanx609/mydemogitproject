//
//  PaymentHistoryViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class PaymentHistoryViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var paymentHistoryTableView: UITableView!
    @IBOutlet weak var noDataView: UIView!

    //MARK: - Variables
    var paymentHistoryDataSource: [CellInfo] = []
    var viewModel: PaymentHistoryVModeling?
    var cpaymentHistoryDetails: [CustomerPaymentHistoryModel] = []
    var spaymentHistoryDetails: [SupplierPaymentHistoryModel] = []
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    var selectedFilter: SPaymentFiltersId = .paid
    var selectedIndex = 0
    var cSelectedFilter: CPaymentFiltersId = .smartTenders
    
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let userType = UserSession.shared.userType, userType == .supplier {
            self.requestSupplierPaymentHistory()
        } else {
            self.requestCustomerPaymentHistory()
        }
        
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupTableView()
        self.loadDataSource()
    }
    
    private func loadDataSource() {
        
        if let userType = UserSession.shared.userType, userType == .supplier {
            
            if let dataSource = self.viewModel?.getSupplierPaymentHistoryDataSource(historyDetails: spaymentHistoryDetails, selectedFilter: self.selectedIndex) {
                self.paymentHistoryDataSource = dataSource
            }
            
        } else {
            
            if let dataSource = self.viewModel?.getPaymentHistoryDataSource(historyDetails: cpaymentHistoryDetails, selectedFilter: self.selectedIndex) {
                self.paymentHistoryDataSource = dataSource
            }
        }
        
    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = PaymentHistoryViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.paymentHistoryTableView.delegate = self
        self.paymentHistoryTableView.dataSource = self
        self.paymentHistoryTableView.separatorStyle = .none
        self.paymentHistoryTableView.allowsSelection = false
        self.paymentHistoryTableView.bounces = false
    }
    
    private func registerNibs() {
        self.paymentHistoryTableView.register(PaymentCollectionTableViewCell.self)
        self.paymentHistoryTableView.register(PaymentHistoryTableCell.self)
        self.paymentHistoryTableView.register(SPaymentHistoryCell.self)
    }
    
    //MARK: - API Methods
    func requestCustomerPaymentHistory() {
        var params = APIParams()
        
        if self.nextPageNumber == 0 {
            self.nextPageNumber = 1
        }
        
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        params[ConstantAPIKeys.type] = cSelectedFilter.rawValue as AnyObject
        self.viewModel?.requestCustomerPaymentHistoryAPI(param: params, completion: { [weak self] (nextPageNumber, perPage, paymentDetails) in
            guard let sSelf = self else { return }
            if sSelf.nextPageNumber == 1 {
                sSelf.cpaymentHistoryDetails = []
                sSelf.cpaymentHistoryDetails = paymentDetails
            } else {
                sSelf.cpaymentHistoryDetails = sSelf.cpaymentHistoryDetails + paymentDetails
            }
            if sSelf.cpaymentHistoryDetails.count == 0{
                sSelf.noDataView.isHidden = false
            }else{
                sSelf.noDataView.isHidden = true
            }
            sSelf.nextPageNumber = nextPageNumber
            sSelf.perPage = perPage
            Threads.performTaskInMainQueue {
                sSelf.loadDataSource()
                sSelf.paymentHistoryTableView.reloadData()
            }
        })
    }
    
    func requestSupplierPaymentHistory() {
        
        if self.nextPageNumber == 0 {
            self.nextPageNumber = 1
        }
        
        var params = APIParams()
        params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        if selectedFilter == .pending || selectedFilter == .paid {
            params[ConstantAPIKeys.status] = selectedFilter.rawValue as AnyObject
        }
        self.viewModel?.requestSupplierPaymentHistoryAPI(param: params, completion: { [weak self] (nextPageNumber, perPage, paymentDetails) in
            guard let sSelf = self else { return }
            
            sSelf.spaymentHistoryDetails = sSelf.spaymentHistoryDetails + paymentDetails
            sSelf.nextPageNumber = nextPageNumber
            sSelf.perPage = perPage
            Threads.performTaskInMainQueue {
                sSelf.loadDataSource()
                sSelf.paymentHistoryTableView.reloadData()
            }
        })
    }
}

