//
//  MyAccountViewC.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/24/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class MyAccountViewC: BaseViewC {
    
    //MARK: - IBOutlets
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var paymentHistoryButtonView: UIView!
    @IBOutlet weak var applicationButtonView: UIView!
    @IBOutlet weak var myWatchListButtonView: UIView!
    @IBOutlet weak var notificationButtonView: UIView!
    //
    @IBOutlet weak var selectedPaymentHistoryView: UIView!
    @IBOutlet weak var selectedApplicationView: UIView!
    @IBOutlet weak var selectedMyWatchListView: UIView!
    @IBOutlet weak var selectedNotificationView: UIView!
    //
    @IBOutlet weak var paymentHistoryButton: UIButton!
    @IBOutlet weak var applicationButton: UIButton!
    @IBOutlet weak var myWatchListButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var ratingView: ShowRating!
    @IBOutlet weak var numberOfRatedLabel: UILabel!
    
    var paymentHistoryVC: PaymentHistoryViewC?
    var applicationsVC: CApplicationsViewC?
    var myWatchListVC: WatchListViewC?
    var notificationVC: SNotificationViewC?
    var loginViewModel: LoginVModeling?

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarTitle(title: "My Account".localizedString(),barColor: .redButtonColor,titleColor: .white, leftBarButtonsType: [.hamburgerWhite], rightBarButtonsType: [.notification])
        self.setupUserDataOnView()
    }
    
    override func navigationButtonTapped(_ sender: AnyObject) {
        guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
        switch buttonType {
        case .hamburgerWhite,.hamburgerBlack: hamburgerButtonTapped(sender)
        case .notification:
           notificationButtonTapped(sender)
        default: break
        }
    }
    
    func notificationButtonTapped(_ sender: AnyObject) {
       let notificationVC = DIConfigurator.sharedInstance.getNotificationVC()
       notificationVC.isHome = true
       self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.setupView()
        self.recheckVM()
        self.requestProfileAPI()
    }
    
    private func recheckVM() {
      if self.loginViewModel == nil {
        self.loginViewModel = LoginViewM()
      }
    }
    
    //MARK: - Private Methods
    private func setupView() {
        self.profileImageView.makeLayer(color: .white, boarderWidth: Constants.UIConstants.borderWidth_1, round: self.profileImageView.frame.width/2)
        if let userType = UserSession.shared.userType {
            switch userType {
            case .supplier, .dealer:
                self.paymentHistoryButtonView.isHidden = false
                self.notificationButtonView.isHidden = false
                self.myWatchListButtonView.isHidden = true
                self.applicationButtonView.isHidden = true
            case .customer:
                self.paymentHistoryButtonView.isHidden = false
                self.notificationButtonView.isHidden = true
                self.myWatchListButtonView.isHidden = false
                self.applicationButtonView.isHidden = false
            }
        }
        self.selectedButtonColor(selectedButton: self.paymentHistoryButton)
        self.showSelectedView(selectedView: self.selectedPaymentHistoryView)
        self.paymentHistoryVC = DIConfigurator.sharedInstance.getPaymentHistoryViewC()
        self.applicationsVC = DIConfigurator.sharedInstance.getCApplicationsViewC()
        self.myWatchListVC = DIConfigurator.sharedInstance.getWatchListVC()
        self.notificationVC = DIConfigurator.sharedInstance.getNotificationVC()
        self.showCurrentViewController(viewController: self.paymentHistoryVC!)
    }
    
    private func setupUserDataOnView() {
        guard let user = UserDefaultsManager.sharedInstance.getUserValueFor(key: .user) else { return }
        //UserImage
        if let userImage = user.image {
            self.profileImageView.setImage(urlStr: userImage, placeHolderImage: #imageLiteral(resourceName: "avatar-placeholder"))
        }
        //Name
        self.nameLabel.text = user.name
        //Location
        self.locationLabel.text = user.location
        //Phone
        if let mobileWithCode = user.mobileWithCode {
            self.phoneButton.setTitle(mobileWithCode, for: .normal)
        }
        //Email
        if let email = user.email {
            self.emailButton.setTitle(email, for: .normal)
        }
        
        self.ratingView.ratingView.settings.starSize = 20
        self.ratingView.ratingView.settings.emptyColor = .white
        self.ratingView.configureView(rating:Helper.toDouble(user.userRating()))
        self.numberOfRatedLabel.text = user.numberOfRated()
        
    }
    
    private func showSelectedView(selectedView: UIView) {
        self.selectedApplicationView.isHidden = true
        self.selectedMyWatchListView.isHidden = true
        self.selectedNotificationView.isHidden = true
        self.selectedPaymentHistoryView.isHidden = true
        selectedView.isHidden = false
    }
    
    private func selectedButtonColor(selectedButton : UIButton) {
        let  unSelectedButtonColor = UIColor.myAccountUnSelectedButtonColor
        self.paymentHistoryButton.setTitleColor(unSelectedButtonColor, for: .normal)
        self.notificationButton.setTitleColor(unSelectedButtonColor, for: .normal)
        self.myWatchListButton.setTitleColor(unSelectedButtonColor, for: .normal)
        self.applicationButton.setTitleColor(unSelectedButtonColor, for: .normal)
        selectedButton.setTitleColor(.myAccountSelectedButtonColor, for: .normal)
    }
    
    // Add childViewController
    private  func showCurrentViewController(viewController:UIViewController) {
        var childViewController:UIViewController?
        
        if self.children.count > 0 {
            childViewController = self.children[0]
        }
        
        viewController.view.frame = CGRect(x: 0, y:0, width: self.containerView.frame.size.width, height: self.containerView.frame.size.height)
        
        if let viewcontroller = childViewController{
            viewcontroller.removeFromParent()
        }
        
        for views in self.containerView.subviews {
            views.removeFromSuperview()
        }
        self.containerView.addSubview((viewController.view)!)
        self.addChild(viewController)
        viewController.didMove(toParent: self)
    }
    
    //MARK: - IBActions
    
    @IBAction func tapPaymentHistoryButton(_ sender: Any) {
        self.selectedButtonColor(selectedButton: self.paymentHistoryButton)
        self.showSelectedView(selectedView: self.selectedPaymentHistoryView)
        self.showCurrentViewController(viewController: self.paymentHistoryVC!)
    }
    
    @IBAction func tapNotificationButton(_ sender: Any) {
        self.selectedButtonColor(selectedButton: self.notificationButton)
        self.showSelectedView(selectedView: self.selectedNotificationView)
        self.showCurrentViewController(viewController: self.notificationVC!)
    }
    
    @IBAction func tapMyWatchListButton(_ sender: Any) {
        self.selectedButtonColor(selectedButton: self.myWatchListButton)
        self.showSelectedView(selectedView: self.selectedMyWatchListView)
        self.showCurrentViewController(viewController: self.myWatchListVC!)
    }
    
    @IBAction func tapApplicationButton(_ sender: Any) {
        self.selectedButtonColor(selectedButton: self.applicationButton)
        self.showSelectedView(selectedView: self.selectedApplicationView)
        self.showCurrentViewController(viewController: self.applicationsVC!)
    }
    
    @IBAction func tapEditProfile(_ sender: Any) {
        let editProfileVC = DIConfigurator.sharedInstance.getEditProfileVC()
        self.navigationController?.pushViewController(editProfileVC, animated: true)
    }
    
    //MARK: - API Methods
    func requestProfileAPI() {
        
        self.loginViewModel?.requestProfileAPI(completion: { (_) in
            Threads.performTaskInMainQueue {
                self.setupUserDataOnView()
            }
        })
    }

}
