//
//  NotificationModel.swift
//  AlbaCars
//
//  Created by Narendra on 2/26/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation


class NotificationModel: Codable {
    
    //Recovery Active Tender Variables
    var date: String?
    var code: Int?
    var message: String?
}
