//
//  SNotificationViewC+TableView.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

extension SNotificationViewC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // let document = self.documentListDataSource[indexPath.row]
        return self.getCell(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      guard let lastVisibleRow = self.notificationTableView.indexPathsForVisibleRows?.last?.row else { return }
      if (self.notificationDataArray.count - lastVisibleRow == 3) && self.nextPageNumber != 0 {
       // self.requestNotificationAPI()
      }
    }
}

extension SNotificationViewC {
    
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: NotificationCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureView(notificationModel: self.notificationDataArray[indexPath.row])
        return cell
    }
}
