//
//  NotificationCell.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class NotificationCell: BaseTableViewCell, ReusableView, NibLoadableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var timeLabel: UILabel!

    //MARK: - Variables
    
    //MARK: - Lifecycle Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    func setupView() {
        Threads.performTaskInMainQueue {
            self.bgView.roundCorners(Constants.UIConstants.sizeRadius_7)
            self.bgView.makeLayer(color: UIColor.lightBorderColor, boarderWidth: 1, round: Constants.UIConstants.sizeRadius_7)
            self.bgView.addShadow(ofColor:  UIColor.lightGray,radius: Constants.UIConstants.sizeRadius_3half, offset: CGSize(width: 0, height: Constants.UIConstants.sizeRadius_3half))
        }
    }
    
    //MARK: - Public Methods
    func configureView(notificationModel: NotificationModel) {
        self.titleLabel.text = Helper.toString(object: notificationModel.message)
        self.timeLabel.text = ""//Date.timeSince(Helper.toString(object: notificationModel.date))
    }
}
