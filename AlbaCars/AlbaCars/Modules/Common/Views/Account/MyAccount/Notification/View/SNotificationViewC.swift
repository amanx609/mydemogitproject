//
//  SNotificationViewC.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class SNotificationViewC: BaseViewC {

    //MARK: - IBOutlets
    @IBOutlet weak var notificationTableView: UITableView!
    @IBOutlet weak var noDataView: UIView!
    //MARK: - Variables
    var viewModel: NotificationViewModeling?
    var notificationDataArray:[NotificationModel] = []
    var nextPageNumber = 1
    var perPage = Constants.Validations.perPage
    var params: APIParams = APIParams()
    var isHome = false

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    deinit {
        print("deinit SNotificationViewC")
    }
    
    //MARK: - Private Methods
    private func setup() {
        self.recheckVM()
        self.setupTableView()
        self.requestNotificationAPI()
        
        if isHome {
            self.setupNavigationBarTitle(title: "Notifications".localizedString(), leftBarButtonsType: [.back], rightBarButtonsType: [])
        }

    }
    
    private func recheckVM() {
        if self.viewModel == nil {
            self.viewModel = NotificationViewM()
        }
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.notificationTableView.backgroundColor = UIColor.white
        self.notificationTableView.delegate = self
        self.notificationTableView.dataSource = self
        self.notificationTableView.separatorStyle = .none
    }
    
    private func registerNibs() {
        self.notificationTableView.register(NotificationCell.self)
    }

    //MARK: - APIMethods
     func requestNotificationAPI() {
        // params[ConstantAPIKeys.page] = self.nextPageNumber as AnyObject
        self.viewModel?.requestNotificationAPI(param: params, completion: { [weak self] (nextPage, perPage, notificationList) in
            guard let sSelf = self else { return }
            sSelf.nextPageNumber = nextPage
            sSelf.perPage = perPage
            sSelf.notificationDataArray = sSelf.notificationDataArray + notificationList
            if sSelf.notificationDataArray.count == 0{
                sSelf.noDataView.isHidden = false
            }else{
                sSelf.noDataView.isHidden = true
            }
            Threads.performTaskInMainQueue {
                sSelf.notificationTableView.reloadData()
            }
        })
    }
}
