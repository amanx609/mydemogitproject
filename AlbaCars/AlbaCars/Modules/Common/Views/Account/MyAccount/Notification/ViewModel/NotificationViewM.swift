//
//  NotificationViewM.swift
//  AlbaCars
//
//  Created by Narendra on 12/26/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol NotificationViewModeling :BaseVModeling {
    func requestNotificationAPI(param: APIParams, completion: @escaping (Int, Int, [NotificationModel]) -> Void)
}

class NotificationViewM: NotificationViewModeling {
    
    func requestNotificationAPI(param: APIParams, completion: @escaping (Int, Int, [NotificationModel]) -> Void) {
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .notificationList(param: param))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [[String: AnyObject]],
                    let resultData = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let notificationList = try decoder.decode([NotificationModel].self, from: resultData)
                        completion(1,10,notificationList)
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
    
}
