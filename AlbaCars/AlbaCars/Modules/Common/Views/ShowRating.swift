//
//  ShowRating.swift
//  AlbaCars
//
//  Created by Narendra on 4/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
import Cosmos

class ShowRating: UIView {
    
    @IBOutlet weak var ratingView: CosmosView!

    var contentView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNib()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadNib()
    }
    
    private func loadNib() {
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of:self))
        let nib = UINib(nibName: "ShowRating", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
     func configureView(rating: Double) {
        self.ratingView.rating = rating
        self.layoutIfNeeded()
    }
    
}
