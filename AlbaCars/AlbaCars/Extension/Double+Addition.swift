//
//  Double+Addition.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 28/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

extension Double {
  var formattedWithSeparator: String {
    let formatter = NumberFormatter()
    formatter.groupingSeparator = ","
    formatter.numberStyle = .decimal
    return formatter.string(for: self) ?? ""
  }
}
