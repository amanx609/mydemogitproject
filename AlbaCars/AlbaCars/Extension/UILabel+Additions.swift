//
//  UILabel+Additions.swift
//  Whystle
//
//  Created by Dharmendra Singh on 04/10/19.
//  Copyright © 2019 NS804. All rights reserved.
//

import UIKit

extension UILabel {
    func setStrikethrough(text:String, color:UIColor) {
//        let attributedText = NSAttributedString(string: text , attributes: [NSStrikethroughStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue, NSStrikethroughColorAttributeName: color])
//        self.attributedText = attributedText
    }
  
  func makeOutline(with strokeColor: UIColor, on text: String) {
    let strokeTextAttributes: [NSAttributedString.Key : Any] = [
        .strokeColor : strokeColor,
        .strokeWidth : -2.0,
        ]

    self.attributedText = NSAttributedString(string: text, attributes: strokeTextAttributes)
  }
}

