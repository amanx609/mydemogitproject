//
//  UIFont+Additions.swift
//  Whystle
//
//  Created by Dharmendra Singh on 04/10/19.
//  Copyright © 2019 NS804. All rights reserved.
//

import UIKit

enum FontSize: CGFloat {
    case size_08 = 8.0
    case size_09 = 9.0
    case size_10  = 10.0
    case size_11  = 11.0
    case size_12  = 12.0
    case size_13  = 13.0
    case size_14  = 14.0
    case size_15  = 15.0
    case size_16  = 16.0
    case size_17  = 17.0
    case size_18  = 18.0
    case size_19  = 19.0
    case size_20  = 20.0
    case size_21  = 21.0
    case size_22  = 22.0
    case size_24 = 24.0
    case size_31  = 31.0
    case size_34  = 34.0
}

enum FontFamily: String {
    case AirbnbCerealApp = "AirbnbCerealApp"
    
    func fontName(wieight: FontWeight)-> String {
        return rawValue + "-" + wieight.rawValue
    }
}

enum FontWeight: String {
    case Black              = "Black"
    case Bold               = "Bold"
    case Book               = "Book"
    case BoldItalic         = "BoldItalic"
    case ExtraBold          = "ExtraBold"
    case ExtraBoldItalic    = "ExtraBoldItalic"
    case ExtraLight         = "ExtraLight"
    case ExtraLightItalic   = "ExtraLightItalic"
    case Light              = "Light"
    case Italic             = "Italic"
    case LightItalic        = "LightItalic"
    case Medium             = "Medium"
    case MediumItalic       = "MediumItalic"
    case Regular            = "Regular"
    case SemiBold           = "Semibold"
    case SemiBoldItalic     = "SemiBoldItalic"
    case Thin               = "Thin"
    case ThinItalic         = "ThinItalic"
}

extension UIFont {
    
    class func font(name fontName: FontFamily, weight: FontWeight = .Regular, size: FontSize ) -> UIFont {
      
        var newSize = size.rawValue
        switch Constants.Devices.ScreenWidth {
        case 320.0: newSize = newSize - 1.0
        case 375.0: break
        //case 414.0: newSize = newSize + 2.0
        default: newSize = newSize + 0.0
        }
        let fontFamily = fontName.fontName(wieight: weight)
        if let font = UIFont(name: fontFamily, size: newSize) {
            return font
        } else {
            print("error while getting font")
            return UIFont.systemFont(ofSize: newSize)
        }
    }
}
