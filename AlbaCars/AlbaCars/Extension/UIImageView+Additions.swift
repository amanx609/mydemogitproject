//
//  UIImageView+Additions.swift
//  Whystle
//
//  Created by Dharmendra Singh on 04/10/19.
//  Copyright © 2019 NS804. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
  
  func setImageForRtoL() {
    if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft, let image = self.image {
      let filppedImage = image.imageFlippedForRightToLeftLayoutDirection()
      self.image = filppedImage
    }
  }
  
  func setImage(urlStr: String, placeHolderImage: UIImage?)  {
    
    if urlStr.contains("http"), let url = URL(string: urlStr) {
        self.sd_setImage(with: url, placeholderImage: placeHolderImage)
    } else {
        self.setImageWithS3Prefix(urlStr: urlStr, placeHolderImage: placeHolderImage)
    }
  }
    
    //MARK: - Private Methods
    private func setImageWithS3Prefix(urlStr: String, placeHolderImage: UIImage?)  {
      var newUrl: String = urlStr
        
      if let prefixS3Url = UserDefaultsManager.sharedInstance.getStringValueFor(key: .prefixS3Url) {
        newUrl = prefixS3Url + newUrl
      }
        
      if let url = URL(string: newUrl) {
        self.sd_setImage(with: url, placeholderImage: placeHolderImage)
      } else {
        self.image = placeHolderImage
      }
    }
}
