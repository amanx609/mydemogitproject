//
//  String+Additions.swift
//  Whystle
//
//  Created by Dharmendra Singh on 04/10/19.
//  Copyright © 2019 NS804. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    func hasSubString(_ string: String) -> Bool {
        //return self.contains(string)
        if self.lowercased().range(of: string.lowercased()) != nil {
            return true
        } else {
            return false
        }
    }
    
    func phoneFormat() -> String {
        var phoneArray = Array(self)
        if  self.count < 8 {
            return self
        }
        let endIndex = self.count - 4
        let startIndex = endIndex - 4
        for i in startIndex...endIndex {
            phoneArray[i] = "X"
        }
        
        return String(phoneArray)
    }
    
    
    func utf8DecodedString()-> String {
        let data = self.data(using: .utf8)
        if let message = String(data: data!, encoding: .nonLossyASCII) {
            return message
        }
        return ""
    }
    
    
    func utf8EncodedString()-> String {
        let messageData = self.data(using: .nonLossyASCII)
        let text = String(data: messageData!, encoding: .utf8)
        return text!
    }
    
    public func localizedString() -> String {
        
        return NSLocalizedString(self, comment: self)
    }
    
    public func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    
    public func trimSpace() -> String {
        let trimmedString = self.trimmingCharacters(
            in: CharacterSet.whitespacesAndNewlines
        )
        return trimmedString
    }
    
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func checkNoSpecialChacter() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func validateSpace() -> Bool {
        return self.rangeOfCharacter(from: CharacterSet.letters.inverted) == nil
     }
    
    func validString() -> String
    {
        if(self == "<null>" || self == "<NULL>")
        {
            return ""
        }
        else if(self == "<nil>" || self == "<NIL>")
        {
            return ""
        }
        else if(self == "null" || self == "NULL")
        {
            return ""
        }
        else if(self == "NIL" || self == "nil")
        {
            return ""
        }
        else if(self == "(null)")
        {
            return ""
        }
        
        return self
    }
    
    public func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.height)
    }
    
    public func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    //RemoveSpecialCharactersFromString
    func removeSpecialCharsFromString() -> String {
        let validChar = Set("+1234567890")
        return String(self.filter { validChar.contains($0) })
    }
    
    public func toPhoneNumber() -> String {
        return self.replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "($1) $2-$3", options: .regularExpression, range: nil)
    }
    
    public func toArray() -> [[String: AnyObject]]? {
        do{
            if let json = self.data(using: String.Encoding.utf8) {
                if let jsonArray = try JSONSerialization.jsonObject(with: json, options: .allowFragments) as? [[String:AnyObject]] {
                    return jsonArray
                }
            }
        }catch {
            return nil
        }
        return nil
    }
    
    public func toDictionary() -> [String: AnyObject]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    
    public func matches(for regex: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: self, range:  NSRange(self.startIndex..., in: self))
            return results.map {
                //self.substring(with: Range($0.range, in: self)!)
                String(self[Range($0.range, in: self)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    public func base64ToImage() -> UIImage? {
        let results = self.matches(for: "data:image\\/([a-zA-Z]*);base64,([^\\\"]*)")
        for imageString in results {
            if let url = URL(string: imageString),let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
                return image
            }
        }
        return nil
    }
    
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
   
    func numberOfSeconds() -> Int {
        let components: Array = self.components(separatedBy: ":")
        let hours = Int(components[0]) ?? 0
        let minutes = Int(components[1]) ?? 0
        let seconds = Int(components[2]) ?? 0
        return (hours * 3600) + (minutes * 60) + seconds
    }
    
    func numberOfMinutes() -> Int {
        let components: Array = self.components(separatedBy: ":")
        let hours = Int(components[0]) ?? 0
        let minutes = Int(components[1]) ?? 0
        return (hours * 60) + minutes
    }
    
    
    
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
}


//ReturnAttributedString
extension String {
    func getAttributedText(color: UIColor, size: FontSize) -> NSAttributedString {
        let textAttributes = [ NSAttributedString.Key.font: UIFont.font(name: .AirbnbCerealApp, weight: .Medium, size: size),
                               NSAttributedString.Key.foregroundColor: color]
        let attributedString = NSAttributedString(string: self, attributes: textAttributes)
        return attributedString
    }
    
    static func getAttributedText(firstText: String, secondText: String, firstTextColor: UIColor, secondTextColor: UIColor, firstTextSize: FontSize, secondTextSize: FontSize, fontWeight: FontWeight = .Medium) -> NSMutableAttributedString {
        let firstTextAttributes = [ NSAttributedString.Key.font: UIFont.font(name: .AirbnbCerealApp, weight: fontWeight, size: firstTextSize),
                                    NSAttributedString.Key.foregroundColor: firstTextColor]
        let firstAttributedString = NSAttributedString(string: firstText, attributes: firstTextAttributes)
        let secondTextAttributes = [ NSAttributedString.Key.font: UIFont.font(name: .AirbnbCerealApp, weight: .Medium, size: secondTextSize),
                                     NSAttributedString.Key.foregroundColor: secondTextColor]
        let secondAttributedString = NSAttributedString(string: secondText, attributes: secondTextAttributes)
        let attributedText = NSMutableAttributedString()
        attributedText.append(firstAttributedString)
        attributedText.append(secondAttributedString)
        return attributedText
    }
    
    static func getAttributedText(firstText: String, secondText: String, firstTextColor: UIColor, secondTextColor: UIColor, firstTextSize: FontSize, secondTextSize: FontSize, firstFontWeight: FontWeight = .Medium,secondFontWeight: FontWeight = .Medium) -> NSMutableAttributedString {
        let firstTextAttributes = [ NSAttributedString.Key.font: UIFont.font(name: .AirbnbCerealApp, weight: firstFontWeight, size: firstTextSize),
                                    NSAttributedString.Key.foregroundColor: firstTextColor]
        let firstAttributedString = NSAttributedString(string: firstText, attributes: firstTextAttributes)
        let secondTextAttributes = [ NSAttributedString.Key.font: UIFont.font(name: .AirbnbCerealApp, weight: secondFontWeight, size: secondTextSize),
                                     NSAttributedString.Key.foregroundColor: secondTextColor]
        let secondAttributedString = NSAttributedString(string: secondText, attributes: secondTextAttributes)
        let attributedText = NSMutableAttributedString()
        attributedText.append(firstAttributedString)
        attributedText.append(secondAttributedString)
        return attributedText
    }
    
    static func appendText(obj: Any?, text: String) -> String {
           if Helper.toInt(obj) == 0
           {
               return (Helper.toString(object: obj))
           } else {
            if text == StringConstants.Text.Kms {
                return " \(Helper.toInt(obj)) \(text)"
            } else {
                return "\(text) \(Helper.toInt(obj))"
            }
           }
       }
}

extension String {
    
    //Convert UTC time to Local
    func UTCToLocal(toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Calendar.current.timeZone
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.000Z"
        
        if let dt = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = toFormat
            dateFormatter.timeZone = NSTimeZone.local
            return dateFormatter.string(from: dt)
        }
        return self
    }
    
    func getDateStringFrom(inputFormat: String, outputFormat: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Calendar.current.timeZone
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = inputFormat
        guard let dateInstance = dateFormatter.date(from: self) else { return nil }
        dateFormatter.dateFormat = outputFormat
        return dateFormatter.string(from: dateInstance)
    }

}
