//
//  Print+Additions.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 13/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

public func print(_ object: Any) {
    #if DEBUG
        Swift.print(object)
    #endif
}
