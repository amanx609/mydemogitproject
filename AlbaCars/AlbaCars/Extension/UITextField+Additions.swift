//
//  UITextField+Additions.swift
//  Whystle
//
//  Created by Dharmendra Singh on 04/10/19.
//  Copyright © 2019 NS804. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    func addInputAccessoryView(title: String, target: Any, selector: Selector) {
      let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: Constants.Devices.ScreenWidth, height: 44.0))
        let barButton = UIBarButtonItem(title: title, style: .plain, target: target, action: selector)
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([flexible, barButton], animated: false)
        self.inputAccessoryView = toolBar
    }
    
    func setPlaceHolderColor(text: String, color: UIColor) {
        self.attributedPlaceholder = NSAttributedString(string: text,
        attributes: [NSAttributedString.Key.foregroundColor: color])
        
    }
}

extension UITextView {
    
    func addInputAccessoryView(title: String, target: Any, selector: Selector) {
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: Constants.Devices.ScreenWidth, height: 44.0))
        let barButton = UIBarButtonItem(title: title, style: .plain, target: target, action: selector)
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([flexible, barButton], animated: false)
        self.inputAccessoryView = toolBar
    }
}
