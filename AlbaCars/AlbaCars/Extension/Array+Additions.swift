//
//  Array+Additions.swift
//  Whystle
//
//  Created by Dharmendra Singh on 04/10/19.
//  Copyright © 2019 NS804. All rights reserved.
//

import Foundation

extension Array where Element: Equatable
{
  mutating func removeObject(object: Element) {
    if let index = self.firstIndex(of: object) {
      self.remove(at: index)
    }
  }
  
  mutating func removeObjectsInArray(array: [Element]) {
    for object in array {
      self.removeObject(object: object)
    }
  }
}

extension Array {
  func toJSONStringFormatted() -> String? {
    do {
      let jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
      return String(data: jsonData, encoding: String.Encoding.utf8)!
    }
    catch {
      return nil
    }
  }
  
  func toJSONString() -> String? {
    do {
      let jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.init(rawValue: 0))
      return String(data: jsonData, encoding: String.Encoding.utf8)!
    }
    catch {
      return nil
    }
  }
  
  func toJSONData() -> Data? {
    do {
      let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
      return jsonData
    } catch {
      return nil
    }
  }
}


//SplitArray
extension Array {
  func chunked(by chunkSize: Int) -> [[Element]] {
    return stride(from: 0, to: self.count, by: chunkSize).map {
      Array(self[$0..<Swift.min($0 + chunkSize, self.count)])
    }
  }
}
