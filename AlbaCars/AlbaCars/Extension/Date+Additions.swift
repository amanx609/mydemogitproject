//
//  Date+Additions.swift
//  Whystle
//
//  Created by Dharmendra Singh on 04/10/19.
//  Copyright © 2019 NS804. All rights reserved.
//

import Foundation

extension Date {
    
    func getMonth() -> Int? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MM"
        let strMonth = dateFormatter.string(from: self)
        return Int(strMonth)
    }
    
    func getYear() -> Int? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy"
        let strYear = dateFormatter.string(from: self)
        return Int(strYear)
    }
    
    public func dateStringWith(strFormat: String) -> String {
        let dateFormatter = DateFormatter()
        //dateFormatter.timeZone = Calendar.current.timeZone
        dateFormatter.locale = Locale(identifier: "en_UK")
        dateFormatter.dateFormat = strFormat
        return dateFormatter.string(from: self)
    }
    
    public var calendar: Calendar {
        return Calendar.current
    }
    
    /// Era.
    public var era: Int {
        return calendar.component(.era, from: self)
    }
    
    /// Year.
    public var year: Int {
        get {
            return calendar.component(.year, from: self)
        }
    }
    
    func localDate() -> Date {
        
        if let timeZone = TimeZone(abbreviation: "UTC") {
            let seconds = TimeInterval(timeZone.secondsFromGMT(for: self))
            return Date(timeInterval: seconds, since: self)
        }
        return self
        //return Formatter.preciseLocalTime.string(for: self) ?? ""
    }
    // or GMT time
    func utcDate() -> Date {
        
        if let timeZone = TimeZone(abbreviation: "UTC") {
            let seconds = -TimeInterval(timeZone.secondsFromGMT(for: self))
            return Date(timeInterval: seconds, since: self)
        }
        return self
    }
    
    static func timeSince(_ dateStr: String, numericDates: Bool = false) -> String {
        
        let utcDate = dateStr.toDateInstaceFromStandardFormat()
        if let date = utcDate?.localDate() {
            let calendar = NSCalendar.current
            let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
            let now = Date()
            let earliest = now < date ? now : date
            let latest = (earliest == now) ? date : now
            let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
            if (components.year! >= 2) {
                return "\(components.year!)yrs ago"
            } else if (components.year! >= 1){
                if (numericDates){
                    return "1yr ago"
                } else {
                    return "1yr ago"
                }
            } else if (components.month! >= 2) {
                return "\(components.month!)mon ago"
            } else if (components.month! >= 1){
                if (numericDates){
                    return "1mon ago"
                } else {
                    return "1mon ago"
                }
            } else if (components.weekOfYear! >= 2) {
                return "\(components.weekOfYear!)w ago"
            } else if (components.weekOfYear! >= 1){
                if (numericDates){
                    return "1w ago"
                } else {
                    return "1w ago"
                }
            } else if (components.day! >= 2) {
                return "\(components.day!)d ago"
            } else if (components.day! >= 1){
                if (numericDates){
                    return "1d ago"
                } else {
                    return "1d ago"
                }
            } else if (components.hour! >= 2) {
                return "\(components.hour!)h ago"
            } else if (components.hour! >= 1){
                if (numericDates){
                    return "1h ago"
                } else {
                    return "1h ago"
                }
            } else if (components.minute! >= 2) {
                return "\(components.minute!)mins ago"
            } else if (components.minute! >= 1){
                if (numericDates){
                    return "1min ago"
                } else {
                    return "1min ago"
                }
            } else if (components.second! >= 3) {
                return "\(components.second!)secs ago"
            } else {
                return "Just now"
            }
        }
        return ""
    }
    
    static func timeLeftBetween(currentDateStr: String, futureDateStr: String,  numericDates: Bool = false) -> String {
        if let utcCurrentDate = currentDateStr.toDateInstaceFromStandardFormat(),
           let utcFutureDate = futureDateStr.toDateInstaceFromStandardFormat() {
            let calendar = NSCalendar.current
            let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
            let components = calendar.dateComponents(unitFlags, from: utcCurrentDate,  to: utcFutureDate)
            if (components.year! >= 2) {
                return "\(components.year!) yrs"
            } else if (components.year! >= 1) {
                if (numericDates){
                    return "1 yr"
                } else {
                    return "1 yr"
                }
            } else if (components.month! >= 2) {
                return "\(components.month!) months"
            } else if (components.month! >= 1){
                if (numericDates){
                    return "1 month"
                } else {
                    return "1 month"
                }
            } else if (components.weekOfYear! >= 2) {
                return "\(components.weekOfYear!) weeks"
            } else if (components.weekOfYear! >= 1){
                if (numericDates){
                    return "1 week"
                } else {
                    return "1 week"
                }
            } else if (components.day! >= 2) {
                return "\(components.day!) days"
            } else if (components.day! >= 1){
                if (numericDates){
                    return "1 day"
                } else {
                    return "1 day"
                }
            } else if (components.hour! >= 2) {
                return "\(components.hour!) hrs"
            } else if (components.hour! >= 1){
                if (numericDates){
                    return "1 hr"
                } else {
                    return "1 hr"
                }
            } else if (components.minute! >= 2) {
                return "\(components.minute!) mins"
            } else if (components.minute! >= 1){
                if (numericDates){
                    return "1 min"
                } else {
                    return "1 min"
                }
            } else if (components.second! >= 3) {
                return "\(components.second!) secs"
            } else {
                return "Not Available"
            }
        }
        return ""
    }
  
  func userAge() -> Int {
    let units:NSCalendar.Unit = [.year]
    let calendar = NSCalendar.current as NSCalendar
    calendar.timeZone = NSTimeZone.default
    calendar.locale = NSLocale.current
    let components = calendar.components(units, from: self, to: Date(), options: NSCalendar.Options.wrapComponents)
    let years = components.year
    return years ?? 0
  }
}

extension String {
    func toDateInstaceFromStandardFormat() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Calendar.current.timeZone
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: self)
    }
  
    func getDateInstaceFrom(format: String) -> Date? {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = format
      return dateFormatter.date(from: self)
    }
  
  func getTimeStamp(inputFormat: String) -> Double {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = inputFormat
    if let date = dateFormatter.date(from: self) {
      let dateStamp = Double(date.timeIntervalSince1970)
      return dateStamp
    }
    return 0.0
  }
}

extension Double {
    public func convertTimeStamp(toFormat: String) -> String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = toFormat
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
  
  public func convertTimeStampForChat(toFormat: String) -> String {
    let date = Date(timeIntervalSince1970: self)
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = Calendar.current.timeZone
    dateFormatter.locale = Calendar.current.locale
    dateFormatter.dateFormat = toFormat
    let strDate = dateFormatter.string(from: date)
    return strDate
  }
}


extension TimeInterval {
    func convertToTimerFormat() -> String {
        let hours = Int(self) / 3600
        let minutes = Int(self) / 60 % 60
        let seconds = Int(self) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
}
