//
//  Dictionary+Additions.swift
//  Whystle
//
//  Created by Dharmendra Singh on 04/10/19.
//  Copyright © 2019 NS804. All rights reserved.
//

import Foundation

extension Dictionary {
  func toJSONStringFormatted() -> String? {
    do {
      let jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
      return String(data: jsonData, encoding: String.Encoding.utf8)!
    } catch {
      return nil
    }
  }
  
  func toJSONString() -> String? {
    do {
      let jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.init(rawValue: 0))
      return String(data: jsonData, encoding: String.Encoding.utf8)!
    }
    catch {
      return nil
    }
  }
  
  func toJSONData() -> Data? {
    do {
      let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
      return jsonData
    } catch {
      return nil
    }
  }
  
  mutating func merge(dict: [Key: Value]){
    for (k, v) in dict {
      updateValue(v, forKey: k)
    }
  }
}
