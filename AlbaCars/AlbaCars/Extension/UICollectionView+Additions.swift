//
//  UICollectionView+Additions.swift
//  Whystle
//
//  Created by Dharmendra Singh on 04/10/19.
//  Copyright © 2019 NS804. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func scrollToFirst() {
        if self.contentSize.width > self.frame.width {
            self.contentOffset = CGPoint(x: (self.contentSize.width) - self.frame.width, y: 0)
        }
    }
    
    func registerNib(nib nibName:String) {
        let nib = UINib(nibName: nibName, bundle: nil)
        self.register(nib, forCellWithReuseIdentifier: nibName)
    }
    
    func register<T:UICollectionViewCell>(_: T.Type) where T: ReusableView, T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        self.register(nib, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func dequeueReusableCell<T:UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.defaultReuseIdentifier)")
        }
        return cell
    }
    
    func registerReusableSupplementaryView<T: ReusableView>(elementKind: String, _: T.Type) {
        self.register(T.self, forSupplementaryViewOfKind: elementKind, withReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func dequeueReusableSupplementaryView<T: UICollectionReusableView>(elementKind: String, indexPath: IndexPath) -> T where T: ReusableView, T: NibLoadableView {
        if let view = self.dequeueReusableSupplementaryView(ofKind: elementKind, withReuseIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T {
            return view
        } else {
            return UIView() as! T
        }
    }
    
    func getIndexPathFor(view: UIView) -> IndexPath? {
        
        let point = self.convert(view.bounds.origin, from: view)
        let indexPath = self.indexPathForItem(at: point)
        return indexPath
    }
}

extension CGRect {
    func insettedBy(insets: UIEdgeInsets) -> CGRect {
        return self.inset(by: insets)
    }
}

extension Array where Element: UICollectionViewLayoutAttributes {
    var frame: CGRect {
        return map { $0.frame }.reduce(self[0].frame, { $0.union($1) })
    }
}

extension UICollectionViewFlowLayout {
    var insettedFrame: CGRect {
        guard let frame = collectionView?.frame else { return .zero }
        
        return frame.insettedBy(insets: sectionInset)
    }
}

class LastRowCenteredLayout: UICollectionViewFlowLayout {
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let originalAttributes = super.layoutAttributesForElements(in: rect) else { return nil }
        guard originalAttributes.isEmpty == false else { return originalAttributes }

        let finalAttributes = originalAttributes.map { $0.copy() as! UICollectionViewLayoutAttributes }

        // Find maxY among all element attributes, then all elements line up with that maxY. Those elements are the ones from the last row
        let maxY = finalAttributes.max { $0.frame.maxY < $1.frame.maxY }?.frame.maxY
        let lastRowAttrs = finalAttributes.filter { $0.frame.maxY == maxY }

        // If the elements in the last row don't sit flush against the right edge, then they need to be centered by shifting them a little to the right
        if lastRowAttrs.frame.maxX < insettedFrame.maxX {
            let shift = (insettedFrame.maxX - lastRowAttrs.frame.maxX) / 2.0
            
            for attrs in lastRowAttrs {
                attrs.frame = attrs.frame.offsetBy(dx: shift, dy: 0)
            }
        }
        
        return finalAttributes
    }
}
