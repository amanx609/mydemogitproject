//
//  UIColor+Additions.swift
//  Whystle
//
//  Created by Dharmendra Singh on 04/10/19.
//  Copyright © 2019 NS804. All rights reserved.
//

import UIKit

extension UIColor {
    
    class var redButtonColor: UIColor {
        return #colorLiteral(red: 1, green: 0.2039215686, blue: 0.2823529412, alpha: 1)
    }
    
    class var pinkButtonColor: UIColor {
        return #colorLiteral(red: 0.9921568627, green: 0.8941176471, blue: 0.8823529412, alpha: 1)
    }
    
    class var coralPinkButtonColor: UIColor {
        return #colorLiteral(red: 1, green: 0.3529411765, blue: 0.4156862745, alpha: 1)
    }
    
    class var facebookButtonColor: UIColor {
        return #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.6, alpha: 1)
    }
    
    class var shadowColor: UIColor {
        return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.15)
    }
    
    class var unselectedButtonColor: UIColor {
        return #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)
    }
    
    class var blackTextColor: UIColor {
        return #colorLiteral(red: 0.1568627451, green: 0.1529411765, blue: 0.1529411765, alpha: 1)
    }
    
    class var blackColor: UIColor {
        return #colorLiteral(red: 0.1568627451, green: 0.1529411765, blue: 0.1529411765, alpha: 1)
    }
    
    class var borderColor: UIColor {
        return #colorLiteral(red: 0.1568627451, green: 0.1529411765, blue: 0.1529411765, alpha: 0.5002200704)
    }
    
    class var aquaLightColor: UIColor {
        return #colorLiteral(red: 0.3529411765, green: 0.9019607843, blue: 1, alpha: 1)
    }
    
    class var aquaColor: UIColor {
        return #colorLiteral(red: 0.2745098039, green: 0.7019607843, blue: 0.9490196078, alpha: 1)
    }
    
    class var lightBorderColor: UIColor {
        return #colorLiteral(red: 0.8666666667, green: 0.8666666667, blue: 0.8666666667, alpha: 1)
    }
    
    class var yellowColor: UIColor {
        return #colorLiteral(red: 1, green: 0.8235294118, blue: 0.2039215686, alpha: 1)
    }
    
    class var purpleColor: UIColor {
        return #colorLiteral(red: 0.4352941176, green: 0.2823529412, blue: 0.8235294118, alpha: 1)
    }
    
    class var greenColor: UIColor {
        return #colorLiteral(red: 0.231372549, green: 0.7803921569, blue: 0.3607843137, alpha: 1)
    }
    
    class var lightGreenColor: UIColor {
        return #colorLiteral(red: 0.7764705882, green: 1, blue: 0.8705882353, alpha: 1)
    }
    
    class var greenTopHeaderColor: UIColor {
        return #colorLiteral(red: 0.07843137255, green: 0.6784313725, blue: 0.3333333333, alpha: 1)
    }
    
    class var blueColor: UIColor {
        return #colorLiteral(red: 0.2823529412, green: 0.5882352941, blue: 0.8235294118, alpha: 1)
    }
    
    class var unavailableBackgroundColor: UIColor {
        return #colorLiteral(red: 0.8784313725, green: 0.8784313725, blue: 0.8784313725, alpha: 1)
    }
    
    class var chooseServiceUnSelectedColor: UIColor {
        return #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
    }
    
    class var unselectedButtonTitleColor: UIColor {
        return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5047590229)
    }
    
    class var myAccountSelectedButtonColor: UIColor {
        return #colorLiteral(red: 0.9803921569, green: 0.2431372549, blue: 0.3137254902, alpha: 1)
    }
    
    class var myAccountUnSelectedButtonColor: UIColor {
        return #colorLiteral(red: 0.09411764706, green: 0.09411764706, blue: 0.09411764706, alpha: 0.4954610475)
    }
    
    class var blueTextColor: UIColor {
        return #colorLiteral(red: 0.3647058824, green: 0.6117647059, blue: 0.9215686275, alpha: 1)
    }
    
    class var orangeTextColor: UIColor {
        return #colorLiteral(red: 1, green: 0.6117647059, blue: 0.1058823529, alpha: 1)
    }
    
    class var grayTextColor: UIColor {
        return #colorLiteral(red: 0.6039215686, green: 0.6039215686, blue: 0.6039215686, alpha: 1)
    }
    
    class var blueTenderButtonColor: UIColor {
        return #colorLiteral(red: 0.2509803922, green: 0.5490196078, blue: 1, alpha: 1)
    }
    
    class var orangeGradientStartColor: UIColor {
        return #colorLiteral(red: 1, green: 0.6901960784, blue: 0.2039215686, alpha: 1)
    }
    
    class var orangeGradientEndColor: UIColor {
        return #colorLiteral(red: 1, green: 0.9921568627, blue: 0.3529411765, alpha: 1)
    }
    
    class var blackGradientStartColor: UIColor {
        return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
    }
    
    class var blackGradientEndColor: UIColor {
        return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
        
    // MARK: UIColor additional properties
    class func colorWith(hexString:String)->UIColor {
        var cString:String = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.utf16.count) != 6) {
            return UIColor.gray
        }
        var rgbValue: Int64 = 0
        Scanner(string: cString).scanInt64(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class func colorWithRGBA(redC: CGFloat, greenC: CGFloat, blueC: CGFloat, alfa: CGFloat) -> UIColor {
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = alfa
        red   = CGFloat(redC/255.0)
        green = CGFloat(greenC/255.0)
        blue  = CGFloat(blueC/255.0)
        alpha  = CGFloat(alpha)
        let color: UIColor =  UIColor(red:CGFloat(red), green: CGFloat(green), blue:CGFloat(blue), alpha: alpha)
        return color
    }
    
    class func colorWithAlpha(color: CGFloat, alfa: CGFloat) -> UIColor {
        let red:   CGFloat = CGFloat(color / 255.0)
        let green: CGFloat = CGFloat(color / 255.0)
        let blue:  CGFloat = CGFloat(color / 255.0)
        let alpha: CGFloat = alfa
        let color: UIColor =  UIColor(red:red, green:green, blue:blue, alpha:alpha)
        return color
    }
}
