//
//  AddExtraPopup.swift
//  AlbaCars
//
//  Created by Narendra on 1/16/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class AddExtraPopup: UIView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    //MARK: - Variables
    private var addButtonTapCompletion: ((Bool) -> Void)?
    var title = ""
    var price = ""
    
    //MARK: - ActionMethods
     override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
       self.fadeOutView()
     }
    
    //MARK: - Public Methods
    class func inistancefromNib() -> AddExtraPopup? {
        
        if let view = UINib(nibName:AddExtraPopup.className, bundle:nil).instantiate(withOwner:nil, options:nil)[0] as? AddExtraPopup {
            
            return view
        }
        return nil
    }
    
    func initializeViewWith(title : String, price :String,handler: ((Bool) -> Void)?) {
        self.title = title
        self.price = price
        
        if self === self.initView() {
            self.addButtonTapCompletion = handler
        }
    }
    
    func showWithAnimated(animated : Bool) {
        if #available(iOS 13.0, *) {
            self.showWithView(view: Constants.Devices.keyWindow!, animated: animated)
        } else {
            self.showWithView(view: UIApplication.shared.keyWindow!, animated: animated)
        }
    }
    
    //MARK: - Private Methods
    private func initView() -> AnyObject {
        self.isUserInteractionEnabled = true
        self.titleLabel.text = "Add".localizedString() + " \(self.title)"
        self.subTitleLabel.text = self.title
        self.priceLabel.text = "AED".localizedString() + " \(self.price)"
        self.addButton.setTitle("Add".localizedString(), for: .normal)
        return self
    }
    
    private func showWithView(view: UIView, animated : Bool) {
        let rect = view.frame
        self.frame = CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height)
        view.addSubview(self)
        if animated {
            self.fadeIn()
        }
    }
    
    private func fadeIn() {
        self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.alpha = 0
        UIView.animate(withDuration: 0.35) {
            self.alpha = 1
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
    
    private func fadeOutView() {
        UIView.animate(withDuration: 0.2, animations: {
        }) { (finished : Bool) in
            if finished {
                self.removeFromSuperview()
            }
        }
    }
    
    @IBAction func addTapped(_ sender: UIButton) {
        
        if (self.addButtonTapCompletion != nil) {
            self.addButtonTapCompletion!(true)
            self.fadeOutView()
        }
    }
}
