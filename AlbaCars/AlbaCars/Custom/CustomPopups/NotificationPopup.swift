//
//  NotificationPopup.swift
//  AlbaCars
//
//  Created by Narendra on 1/17/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class NotificationPopup: UIView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var notifyButton: UIButton!
    //ConstraintOutlets
    @IBOutlet weak var bgViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Variables
    private let headerViewHeight: CGFloat = 50.0
    private let bottomViewHeight: CGFloat = 70.0
    private let maxViewHeight: CGFloat = 320.0
    private var buttonTapCompletion: (([Int]) -> Void)?
    private var selectedNotification = 0
    fileprivate var listDataSource: [[String :AnyObject]] = []
    
    
    //MARK: - ActionMethods
     override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
       self.fadeOutView()
     }
    
    //MARK: - Public Methods
    class func inistancefromNib() -> NotificationPopup? {
        if let view = UINib(nibName:NotificationPopup.className, bundle:nil).instantiate(withOwner:nil, options:nil)[0] as? NotificationPopup {
            return view
        }
        return nil
    }
    
    func initializeViewWith(title : String, arrayList : [[String : AnyObject]], handler: (([Int]) -> Void)?) {
        if self === self.initWithTitle(title: title, array: arrayList) {
            self.buttonTapCompletion = handler
        }
    }
    
    func showWithAnimated(animated : Bool) {
        if #available(iOS 13.0, *) {
            self.showWithView(view: Constants.Devices.keyWindow!, animated: animated)
        } else {
            self.showWithView(view: UIApplication.shared.keyWindow!, animated: animated)
        }
    }
    
    //MARK: -- Private method
    private func initWithTitle(title: String, array: [[String : AnyObject]]) -> AnyObject {
      self.isUserInteractionEnabled = true
      self.bgView.roundCorners(Constants.UIConstants.sizeRadius_5)
      self.listDataSource = array.map { $0 }
      self.setupTableView()
        if title.isEmpty {
            self.headerViewHeightConstraint.constant = 0.0
        } else {
            self.titleLabel.text = title
        }
      Threads.performTaskAfterDealy(0.1) {
        let tableViewHeight = self.listTableView.contentSize.height + 20.0
        var newHeight: CGFloat = 0.0
        if title.isEmpty {
            newHeight = tableViewHeight + self.bottomViewHeight
        } else {
            newHeight = tableViewHeight + self.headerViewHeight + self.bottomViewHeight
        }
        self.bgViewHeightConstraint.constant = newHeight > self.maxViewHeight ? self.maxViewHeight : newHeight
      }
      return self
    }
    
    private func setupTableView() {
       
       self.registerNibs()
       self.listTableView.delegate = self
       self.listTableView.dataSource = self
       self.listTableView.tableFooterView = UIView.init(frame: CGRect.zero)
        self.listTableView.separatorStyle = .none
     }
     
     private func registerNibs() {
       self.listTableView.register(NotificationPopupCell.self)
     }
    
    //ViewAnimationMethods
    private func showWithView(view: UIView, animated : Bool) {
        let rect = view.frame
        self.frame = CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height)
        view.addSubview(self)
        if animated {
            self.fadeIn()
        }
    }
    
    private func fadeIn() {
        self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.alpha = 0
        UIView.animate(withDuration: 0.35) {
            self.alpha = 1
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
    
    private func fadeOutView() {
        UIView.animate(withDuration: 0.2, animations: {
        }) { (finished : Bool) in
            if finished {
                self.removeFromSuperview()
            }
        }
    }
    
    //DataSourceMethods
    private func unselectAllInDataSource() {
        var index = 0
        for data in self.listDataSource {
            var newData = data
            newData[Constants.UIKeys.isSelected] = false as AnyObject
            self.listDataSource[index] = newData
            index += 1
        }
    }
    
    private func getSelectedDataId() -> [Int] {
        var ids: [Int] = []
        for data in self.listDataSource {
            if let id = data[Constants.UIKeys.id] as? Int,
               let isSelected = data[Constants.UIKeys.isSelected] as? Bool,
               isSelected {
                ids.append(id)
                
            }
        }
       
        return ids
    }
    
    @IBAction func tapNotify(_ sender: UIButton) {
        let selectedIdArr = self.getSelectedDataId()
        if selectedIdArr.count > 0 {
            if (self.buttonTapCompletion != nil) {
                self.buttonTapCompletion!(selectedIdArr)
                self.fadeOutView()
            }
        }
        
    }
}

extension NotificationPopup: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.listDataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell: NotificationPopupCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
    let data = self.listDataSource[indexPath.row]
    cell.configureView(data: data)
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    //self.unselectAllInDataSource()
    let isSelected = Helper.toBool(self.listDataSource[indexPath.row][Constants.UIKeys.isSelected])
    if !(isSelected) {
         self.listDataSource[indexPath.row][Constants.UIKeys.isSelected] = true as AnyObject
    } else {
        self.listDataSource[indexPath.row][Constants.UIKeys.isSelected] = false as AnyObject
    }
   
    self.listTableView.reloadData()
  }
}
