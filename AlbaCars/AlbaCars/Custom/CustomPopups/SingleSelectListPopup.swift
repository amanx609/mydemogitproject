//
//  SingleSelectListPopup.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 18/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class SingleSelectListPopup: UIView {
  
  //MARK: - IBOutlets
  @IBOutlet weak var bgView: UIView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var listTableView: UITableView!
  //ConstraintOutlets
  @IBOutlet weak var bgViewHeightConstraint: NSLayoutConstraint!
    
  //MARK: - Private Variables
  private let headerViewHeight: CGFloat = 55.0
  private let maxViewHeight: CGFloat = 310.0
  private var selectionCompletion: (([String : AnyObject]) -> Void)?
  fileprivate var listDataSource = [[String :AnyObject]]()
  fileprivate var key = ""
  
  //MARK: - ActionMethods
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.fadeOutView()
  }
  
  //MARK: - Public Methods
  class func inistancefromNib() -> SingleSelectListPopup? {
      if let view = UINib(nibName:SingleSelectListPopup.className, bundle:nil).instantiate(withOwner:nil, options:nil)[0] as? SingleSelectListPopup {
          return view
      }
      return nil
  }
  
  func initializeViewWith(title : String, arrayList : [[String : AnyObject]], key: String, handler: (([String : AnyObject]) -> Void)?)
  {
    if self === self.initWithTitle(title: title, array: arrayList, keyValue: key) {
      self.selectionCompletion = handler
    }
  }
  
  func showWithAnimated(animated : Bool) {
    if #available(iOS 13.0, *) {
      self.showWithView(view: Constants.Devices.keyWindow!, animated: animated)
    } else {
      self.showWithView(view: UIApplication.shared.keyWindow!, animated: animated)
    }
  }
  
  //MARK: -- Private method
  private func initWithTitle(title: String, array: [[String : AnyObject]], keyValue: String) -> AnyObject {
    self.isUserInteractionEnabled = true
    self.bgView.roundCorners(Constants.UIConstants.sizeRadius_5)
    self.listDataSource = array.map { $0 }
    self.setupTableView()
    self.titleLabel.text = title
    self.key = keyValue
    Threads.performTaskAfterDealy(0.1) {
        let tableViewHeight = self.listTableView.contentSize.height
      let newHeight = tableViewHeight + self.headerViewHeight
      self.bgViewHeightConstraint.constant = newHeight > self.maxViewHeight ? self.maxViewHeight : newHeight
    }
    return self
  }
  
  private func setupTableView() {
    self.registerNibs()
    self.listTableView.delegate = self
    self.listTableView.dataSource = self
    self.listTableView.tableFooterView = UIView.init(frame: CGRect.zero)
  }
  
  private func registerNibs() {
    self.listTableView.register(SingleSelectPopupCell.self)
  }
  
  private func showWithView(view: UIView, animated : Bool) {
    let rect = view.frame
    self.frame = CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height)
    view.addSubview(self)
    if animated {
      self.fadeIn()
    }
  }
  
  private func fadeIn() {
    self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
    self.alpha = 0
    UIView.animate(withDuration: 0.35) {
      self.alpha = 1
      self.transform = CGAffineTransform(scaleX: 1, y: 1)
    }
  }
  
  private func fadeOutView() {
    UIView.animate(withDuration: 0.2, animations: {
    }) { (finished : Bool) in
      if finished {
        self.removeFromSuperview()
      }
    }
  }
}

extension SingleSelectListPopup: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.listDataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell: SingleSelectPopupCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
    if let text = self.listDataSource[indexPath.row][self.key] as? String {
        if let number = self.listDataSource[indexPath.row][ConstantAPIKeys.plateNumber] as? String {
            cell.titleLabel.text = "\(text), \(number)"
        }else{
            cell.titleLabel.text = text
        }
    } else {
      cell.titleLabel.text = ""
    }
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return Constants.CellHeightConstants.height_40
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    if (self.selectionCompletion != nil) {
      let dic = self.listDataSource[indexPath.row]
      self.selectionCompletion!(dic)
    }
    self.fadeOutView()
  }
}
