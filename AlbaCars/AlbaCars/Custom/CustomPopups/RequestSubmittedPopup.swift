//
//  RequestSubmiitedPopup.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 03/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class RequestSubmittedPopup: UIView {
  
  //MARK: - IBOutlets
  @IBOutlet weak var bgView: UIView!
  @IBOutlet weak var requestSubmittedLabel: UILabel!
  @IBOutlet weak var representativesLabel: UILabel!
  @IBOutlet weak var gotItButton: UIButton!
  
  //MARK: - Variables
  private var buttonTapCompletion: ((Bool) -> Void)?
  
  //MARK: - Public Methods
  class func inistancefromNib() -> RequestSubmittedPopup? {
      if let view = UINib(nibName:RequestSubmittedPopup.className, bundle:nil).instantiate(withOwner:nil, options:nil)[0] as? RequestSubmittedPopup {
          return view
      }
      return nil
  }
  
  func initializeViewWith(handler: ((Bool) -> Void)?) {
    if self === self.initView() {
      self.buttonTapCompletion = handler
    }
  }
  
  
  func showWithAnimated(animated : Bool) {
    if #available(iOS 13.0, *) {
      self.showWithView(view: Constants.Devices.keyWindow!, animated: animated)
    } else {
      self.showWithView(view: UIApplication.shared.keyWindow!, animated: animated)
    }
  }
  
  //MARK: - Private Methods
  private func initView() -> AnyObject {
    self.isUserInteractionEnabled = true
    self.bgView.roundCorners(Constants.UIConstants.sizeRadius_5)
    self.requestSubmittedLabel.text = "Request Submitted".localizedString()
    self.representativesLabel.text = "Our Reprsentative will get back to you soon".localizedString()
    self.gotItButton.setTitle("Got It".localizedString(), for: .normal)
    return self
  }
  
  private func showWithView(view: UIView, animated : Bool) {
    let rect = view.frame
    self.frame = CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height)
    view.addSubview(self)
    if animated {
      self.fadeIn()
    }
  }
  
  private func fadeIn() {
    self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
    self.alpha = 0
    UIView.animate(withDuration: 0.35) {
      self.alpha = 1
      self.transform = CGAffineTransform(scaleX: 1, y: 1)
    }
  }
  
  private func fadeOutView() {
    UIView.animate(withDuration: 0.2, animations: {
    }) { (finished : Bool) in
      if finished {
        self.removeFromSuperview()
      }
    }
  }
  
  //MARK: - IBActions
  @IBAction func tapGotIt(_ sender: UIButton) {
    if (self.buttonTapCompletion != nil) {
      self.buttonTapCompletion!(true)
      self.fadeOutView()
    }
  }
}
