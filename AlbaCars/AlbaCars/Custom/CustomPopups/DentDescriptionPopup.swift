//
//  DentDescriptionPopup.swift
//  AlbaCars
//
//  Created by Narendra on 2/26/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit
import UITextView_Placeholder
class DentDescriptionPopup: UIView, UITextViewDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var textView: UITextView!
    
    //MARK: - Variables
    private var addButtonTapCompletion: ((String) -> Void)?
    
    //MARK: - ActionMethods
     override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
       //self.fadeOutView()
     }
    
    //MARK: - Public Methods
    class func inistancefromNib() -> DentDescriptionPopup? {
        
        if let view = UINib(nibName:DentDescriptionPopup.className, bundle:nil).instantiate(withOwner:nil, options:nil)[0] as? DentDescriptionPopup {
            
            return view
        }
        return nil
    }
    
    func initializeViewWith(handler: ((String) -> Void)?) {
       
        if self === self.initView() {
            self.addButtonTapCompletion = handler
        }
    }
    
    func showWithAnimated(animated : Bool) {
        if #available(iOS 13.0, *) {
            self.showWithView(view: Constants.Devices.keyWindow!, animated: animated)
        } else {
            self.showWithView(view: UIApplication.shared.keyWindow!, animated: animated)
        }
    }
    
    //MARK: - Private Methods
    private func initView() -> AnyObject {
        self.isUserInteractionEnabled = true
        self.textView.delegate = self
        self.textView.placeholder = "Enter dent description"
        return self
    }
    
    private func showWithView(view: UIView, animated : Bool) {
        let rect = view.frame
        self.frame = CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height)
        view.addSubview(self)
        if animated {
            self.fadeIn()
        }
    }
    
    private func fadeIn() {
        self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.alpha = 0
        UIView.animate(withDuration: 0.35) {
            self.alpha = 1
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
    
    private func fadeOutView() {
        UIView.animate(withDuration: 0.2, animations: {
        }) { (finished : Bool) in
            if finished {
                self.removeFromSuperview()
            }
        }
    }
    
    @IBAction func addTapped(_ sender: UIButton) {
        
        if (self.addButtonTapCompletion != nil) {
            if self.textView.text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0{
                self.addButtonTapCompletion!(Helper.toString(object: self.textView.text))
                self.fadeOutView()
            }else{
                Alert.showOkAlert(title: "", message: "Please add a valid dent description.")
            }
            
        }
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        self.fadeOutView()
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
