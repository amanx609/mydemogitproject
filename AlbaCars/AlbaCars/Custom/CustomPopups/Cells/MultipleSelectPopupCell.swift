//
//  MultipleSelectPopupCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 20/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class MultipleSelectPopupCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkboxImageView: UIImageView!
    
    //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Public Methods
    func configureViewFor(vehicleBrand: VehicleBrand) {
        self.titleLabel.text = vehicleBrand.title
//        if let isSelected = vehicleBrand.isSelected {
//        self.checkboxImageView.image = isSelected ? #imageLiteral(resourceName: "radio-btn-active") : #imageLiteral(resourceName: "radio-btn")
//        }
        
        self.checkboxImageView.image = Helper.toBool(vehicleBrand.isSelected) ? #imageLiteral(resourceName: "radio-btn-active") : #imageLiteral(resourceName: "radio-btn")
    }
    
    func configureViewFor(vehicleYear: [String:AnyObject]) {
        self.titleLabel.text = Helper.toString(object: vehicleYear[Constants.UIKeys.year]) 
    
            
        self.checkboxImageView.image = Helper.toBool(vehicleYear[Constants.UIKeys.isSelected]) ? #imageLiteral(resourceName: "radio-btn-active") : #imageLiteral(resourceName: "radio-btn")
        }
    
    func configureViewFor(timeDuration: [String:AnyObject]) {
           self.titleLabel.text = Helper.toString(object: timeDuration[Constants.UIKeys.duration])
       
               
           self.checkboxImageView.image = Helper.toBool(timeDuration[Constants.UIKeys.isSelected]) ? #imageLiteral(resourceName: "radio-btn-active") : #imageLiteral(resourceName: "radio-btn")
    }
}
