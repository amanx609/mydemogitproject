//
//  NotificationPopupCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 21/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class NotificationPopupCell: BaseTableViewCell, NibLoadableView, ReusableView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var radioButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - IBActions
    override func awakeFromNib() {
        super.awakeFromNib()
        self.radioButton.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Public Methods
    func configureView(data: [String: AnyObject]) {
        if let titleText = data[Constants.UIKeys.titlePlaceholder] as? String {
            self.titleLabel.text = titleText
        }
        if let isSelected = data[Constants.UIKeys.isSelected] as? Bool {
            self.radioButton.isSelected = isSelected
        }
    }
}
