//
//  SingleSelectPopupCell.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 18/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

class SingleSelectPopupCell: BaseTableViewCell, ReusableView, NibLoadableView {
  
  //MARK: - IBOutlets
  @IBOutlet weak var titleLabel: UILabel!
  
  //MARK: - LifeCycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
