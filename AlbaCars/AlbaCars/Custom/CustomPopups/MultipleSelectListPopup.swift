//
//  MultipleSelectListPopup.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 20/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class MultipleSelectListPopup: UIView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet var gradientView: UIView!
    @IBOutlet var submitButton: UIButton!
    //ConstraintOutlets
    @IBOutlet var bgViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Private Variables
    private let headerViewHeight: CGFloat = 55.0
    private let maxViewHeight: CGFloat = 310.0
    private var carBrandSelectionHandler: (([VehicleBrand]) -> Void)?
    private var carYearSelectionHandler: (([[String: AnyObject]],[[String: AnyObject]]) -> Void)?
    private var timeDurationSelectionHandler: (([[String: AnyObject]]) -> Void)?
    fileprivate var vehicleBrandsDataSource: [VehicleBrand] = []
    fileprivate var vehicleYearsDataSource: [[String: AnyObject]] = []
    fileprivate var timeDurationDataSource: [[String: AnyObject]] = []
    fileprivate var popupType: PopupType = .vehicleBrands
    
    //MARK: - ActionMethods
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.fadeOutView()
    }
    
    //MARK: - Public Methods
    class func inistancefromNib() -> MultipleSelectListPopup? {
        if let view = UINib(nibName:MultipleSelectListPopup.className, bundle:nil).instantiate(withOwner:nil, options:nil)[0] as? MultipleSelectListPopup {
            return view
        }
        return nil
    }
    
    func initializeViewForVehicleBrand(brands: [VehicleBrand], handler: (([VehicleBrand]) -> Void)?)
    {
        if self === self.initViewForVehicleBrand(vehicleBrands: brands) {
        self.carBrandSelectionHandler = handler
      }
    }
    
    func initializeViewForYears(years: [[String: AnyObject]], handler: (([[String: AnyObject]],[[String: AnyObject]]) -> Void)?)
       {
           if self === self.initViewForVehicleYear(vehicleYears: years) {
           self.carYearSelectionHandler = handler
         }
       }
    
    func initializeViewForDuration(time: [[String: AnyObject]], handler: (([[String: AnyObject]]) -> Void)?)
    {
        if self === self.initViewForDuration(timeDuration: time) {
        self.timeDurationSelectionHandler = handler
      }
    }
    
    func showWithAnimated(animated : Bool) {
      if #available(iOS 13.0, *) {
        self.showWithView(view: Constants.Devices.keyWindow!, animated: animated)
      } else {
        self.showWithView(view: UIApplication.shared.keyWindow!, animated: animated)
      }
    }
    
    //MARK: -- Private method
    private func setupView() {
        self.isUserInteractionEnabled = true
        self.bgView.roundCorners(Constants.UIConstants.sizeRadius_5)
        self.setupTableView()
        Threads.performTaskInMainQueue {
       //   self.gradientView.drawGradient(startColor: .redButtonColor, endColor: .coralPinkButtonColor)
          self.gradientView.roundCorners(Constants.UIConstants.sizeRadius_7)
          self.submitButton.roundCorners(Constants.UIConstants.sizeRadius_7)
            
        }
        Threads.performTaskAfterDealy(0.1) {
            let tableViewHeight = self.listTableView.contentSize.height
            let newHeight = tableViewHeight + self.headerViewHeight
            self.bgViewHeightConstraint.constant = newHeight > self.maxViewHeight ? self.maxViewHeight : newHeight
        }
    }
    private func initViewForVehicleBrand(vehicleBrands: [VehicleBrand]) -> AnyObject {
        self.popupType = .vehicleBrands
        self.vehicleBrandsDataSource = vehicleBrands
        self.titleLabel.text = "Brands".localizedString()
        self.setupView()
        return self
    }
    
    private func initViewForVehicleYear(vehicleYears: [[String: AnyObject]]) -> AnyObject {
          self.popupType = .vehicleYear
          self.vehicleYearsDataSource = vehicleYears
          self.titleLabel.text = "Select Years".localizedString()
          self.setupView()
          return self
      }
    
    private func initViewForDuration(timeDuration: [[String: AnyObject]]) -> AnyObject {
        self.popupType = .time
        self.timeDurationDataSource = timeDuration
        self.titleLabel.text = "Select Time".localizedString()
        self.setupView()
        return self
    }
    
    private func setupTableView() {
        self.registerNibs()
        self.listTableView.delegate = self
        self.listTableView.dataSource = self
        self.listTableView.tableFooterView = UIView.init(frame: CGRect.zero)
    }
    
    private func registerNibs() {
        self.listTableView.register(MultipleSelectPopupCell.self)
    }
    
    private func showWithView(view: UIView, animated : Bool) {
        let rect = view.frame
        self.frame = CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height)
        view.addSubview(self)
        if animated {
            self.fadeIn()
        }
    }
    
    private func fadeIn() {
        self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.alpha = 0
        UIView.animate(withDuration: 0.35) {
            self.alpha = 1
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
    
    private func fadeOutView() {
        UIView.animate(withDuration: 0.2, animations: {
        }) { (finished : Bool) in
            if finished {
                self.removeFromSuperview()
            }
        }
    }
    
    private func handleSubmitAction() {
        if self.popupType == .vehicleBrands {
            var selectedVehicleBrands: [VehicleBrand] = []
            for vehicleBrand in self.vehicleBrandsDataSource {
                if let isSelected = vehicleBrand.isSelected, isSelected {
                    selectedVehicleBrands.append(vehicleBrand)
                }
            }
            if (self.carBrandSelectionHandler != nil) {
              self.carBrandSelectionHandler!(selectedVehicleBrands)
            }
        } else if self.popupType == .vehicleYear {
            var selectedVehicleYear: [[String: AnyObject]] = []
                       for vehicleYear in self.vehicleYearsDataSource {
                        if let isSelected = vehicleYear[Constants.UIKeys.isSelected] as? Bool, isSelected {
                               selectedVehicleYear.append(vehicleYear)
                           }
                       }
                       if (self.carYearSelectionHandler != nil) {
                         self.carYearSelectionHandler!(selectedVehicleYear, self.vehicleYearsDataSource)
                       }
        } else if self.popupType == .time {
            var selectedDuration: [[String: AnyObject]] = []
            for duration in self.timeDurationDataSource {
                if let isSelected = duration[Constants.UIKeys.isSelected] as? Bool, isSelected {
                    selectedDuration.append(duration)
                }
            }
            if (self.timeDurationSelectionHandler != nil) {
                self.timeDurationSelectionHandler!(selectedDuration)
            }
        }
        self.fadeOutView()
    }
    
    //MARK: - IBActions
    @IBAction func tapSubmit(_ sender: UIButton) {
        self.handleSubmitAction()
    }
}

extension MultipleSelectListPopup: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.popupType == .vehicleBrands {
            return self.vehicleBrandsDataSource.count
        } else if self.popupType == .vehicleYear {
            return self.vehicleYearsDataSource.count
        } else if self.popupType == .time {
            return self.timeDurationDataSource.count
            
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MultipleSelectPopupCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        if self.popupType == .vehicleBrands {
            let vehicleBrand = self.vehicleBrandsDataSource[indexPath.row]
            cell.configureViewFor(vehicleBrand: vehicleBrand)
        } else if self.popupType == .vehicleYear {
            let vehicleYear = self.vehicleYearsDataSource[indexPath.row]
            cell.configureViewFor(vehicleYear: vehicleYear)
        } else if self.popupType == .time {
            let timeDuration = self.timeDurationDataSource[indexPath.row]
            cell.configureViewFor(timeDuration: timeDuration)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.CellHeightConstants.height_40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.popupType == .vehicleBrands {
            self.vehicleBrandsDataSource[indexPath.row].isSelected = !Helper.toBool(self.vehicleBrandsDataSource[indexPath.row].isSelected)
            self.listTableView.reloadData()
        } else if self.popupType == .vehicleYear {
           self.vehicleYearsDataSource[indexPath.row][Constants.UIKeys.isSelected]  =  !Helper.toBool(self.vehicleYearsDataSource[indexPath.row][Constants.UIKeys.isSelected]) as AnyObject
            self.listTableView.reloadData()
        } else if self.popupType == .time {
           self.timeDurationDataSource[indexPath.row][Constants.UIKeys.isSelected]  =  !Helper.toBool(self.timeDurationDataSource[indexPath.row][Constants.UIKeys.isSelected]) as AnyObject
            self.listTableView.reloadData()
        }
    }
}
