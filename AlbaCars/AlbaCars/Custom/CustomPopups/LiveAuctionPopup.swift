//
//  LiveAuctionPopup.swift
//  AlbaCars
//
//  Created by Narendra on 2/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

class LiveAuctionPopup: UIView {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    //MARK: - Variables
    private var addButtonTapCompletion: ((LiveAuctionPopupType) -> Void)?
    var liveAuctionPopupType:LiveAuctionPopupType  = .confirmBid
    var price = ""
    
    //MARK: - ActionMethods
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.fadeOutView()
    }
    
    //MARK: - Public Methods
    class func inistancefromNib() -> LiveAuctionPopup? {
        
        if let view = UINib(nibName:LiveAuctionPopup.className, bundle:nil).instantiate(withOwner:nil, options:nil)[0] as? LiveAuctionPopup {
            
            return view
        }
        return nil
    }
    
    func initializeViewWith(liveAuctionPopupType:LiveAuctionPopupType, price :String,handler: ((LiveAuctionPopupType) -> Void)?) {
        self.price = price
        self.liveAuctionPopupType = liveAuctionPopupType
        if self === self.initView() {
            self.addButtonTapCompletion = handler
        }
    }
    
    func showWithAnimated(animated : Bool) {
        if #available(iOS 13.0, *) {
            self.showWithView(view: Constants.Devices.keyWindow!, animated: animated)
        } else {
            self.showWithView(view: UIApplication.shared.keyWindow!, animated: animated)
        }
    }
    
    //MARK: - Private Methods
    private func initView() -> AnyObject {
        self.isUserInteractionEnabled = true
        
        var title = ""
        var subTitleLabel = ""
        var buttonTitle = ""
        
        switch self.liveAuctionPopupType {
        case .confirmBid:
            title = "Confirm Bid"
            subTitleLabel = "You’re about to Bid"
            buttonTitle = "Confirm"
            self.priceLabel.attributedText = String.getAttributedText(firstText: "AED".localizedString(), secondText:" " + price, firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_17, secondTextSize: .size_17, fontWeight: .Regular)
        case .lowerBid:
            title = "Lower Bid"
            subTitleLabel = "Your Bid is Lesser than the Current Bid\nPlease Bid Higher than the Current Bid"
            buttonTitle = "Back"
            self.priceLabel.text = ""
        case .highestBidder:
            title = "Highest Bidder"
            subTitleLabel = "You’re Already the Highest Bidder\nAre You Sure you want to Bid"
            buttonTitle = "Confirm"
            self.priceLabel.attributedText = String.getAttributedText(firstText: "AED".localizedString(), secondText:" " + price, firstTextColor: .blackTextColor, secondTextColor: .redButtonColor, firstTextSize: .size_17, secondTextSize: .size_17, fontWeight: .Regular)
        default:
            print("")
        }
        
        self.titleLabel.text = title.localizedString()
        self.subTitleLabel.text = subTitleLabel.localizedString()
        self.addButton.setTitle(buttonTitle.localizedString(), for: .normal)
        
        return self
    }
    
    private func showWithView(view: UIView, animated : Bool) {
        let rect = view.frame
        self.frame = CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height)
        view.addSubview(self)
        if animated {
            self.fadeIn()
        }
    }
    
    private func fadeIn() {
        self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.alpha = 0
        UIView.animate(withDuration: 0.35) {
            self.alpha = 1
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
    
    private func fadeOutView() {
        UIView.animate(withDuration: 0.2, animations: {
        }) { (finished : Bool) in
            if finished {
                self.removeFromSuperview()
            }
        }
    }
    
    @IBAction func addTapped(_ sender: UIButton) {
        
        if (self.addButtonTapCompletion != nil) {
            self.addButtonTapCompletion!(liveAuctionPopupType)
            self.fadeOutView()
        }
    }
    
    @IBAction func crossTapped(_ sender: UIButton) {
        self.fadeOutView()
    }
    
}

