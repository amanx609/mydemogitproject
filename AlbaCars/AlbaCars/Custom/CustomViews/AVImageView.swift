//
//  AVImageView.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 02/01/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import UIKit

enum ImageStatus: Int {
  case none = 0
  case uploaded = 1
  case uploading = 2
  case uploadFailed = 3
}

class AVImageView: UIView {
  
  //MARK: - IBOutlets
  @IBOutlet weak var customImageView: UIImageView!
  //UploadingView
  @IBOutlet weak var uploadingView: UIView!
  @IBOutlet weak var indicator: UIActivityIndicatorView!
  //UploadFailedView
  @IBOutlet weak var uploadFailedView: UIView!
  @IBOutlet weak var uploadFailedLabel: UILabel!
  @IBOutlet weak var retryLabel: UILabel!
  
  //MARK: - Variables
  var contentView: UIView!
  
  //MARK: - LifeCycle Methods
  override init(frame: CGRect) {
      super.init(frame: frame)
      self.loadNib()
  }
  
  required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      self.loadNib()
  }
  
  private func loadNib() {
      contentView = loadViewFromNib()
      contentView.frame = bounds
      contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
      contentView.clipsToBounds = true
      addSubview(contentView)
  }
  
  func setImage(image: UIImage?, placeholderImage: UIImage?, with status: ImageStatus) {
    let cFrame = self.contentView.frame
    if let img = image {
      self.customImageView.image = img
      self.customImageView.frame = cFrame
    } else if let placeholderImg = placeholderImage {
      self.customImageView.image = placeholderImg
    }
    switch status {
    case .none:
      self.hideOtherViews()
    case .uploaded:
      self.showUploadedImage()
    case .uploading:
      self.showImageUploading()
    case .uploadFailed:
      self.showImageUploadFailed()
    }
  }
  
  func addTapRecognizerFailedImage(target: Any?, selector: Selector) {
    let tapGesture = UITapGestureRecognizer(target: target, action: selector)
    self.uploadFailedView.addGestureRecognizer(tapGesture)
    self.uploadingView.isUserInteractionEnabled = true
  }
  
  //MARK: - Private Methods
  private func loadViewFromNib() -> UIView {
      let bundle = Bundle(for: type(of:self))
      let nib = UINib(nibName: "AVImageView", bundle: bundle)
      let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
      return view
  }
  
  private func hideOtherViews() {
    self.uploadFailedView.isHidden = true
    self.uploadingView.isHidden = true
  }
  
  private func showUploadedImage() {
    self.indicator.stopAnimating()
    self.uploadFailedView.isHidden = true
    self.uploadingView.isHidden = true
  }
  
  private func showImageUploading() {
    self.indicator.stopAnimating()
    self.uploadFailedView.isHidden = true
    self.uploadingView.isHidden = false
    self.indicator.startAnimating()
  }
  
  private  func showImageUploadFailed() {
    self.indicator.stopAnimating()
    self.uploadFailedView.isHidden = false
    self.uploadingView.isHidden = true
    self.uploadFailedLabel.makeOutline(with: .black, on: "Upload Failed".localizedString())
    self.retryLabel.makeOutline(with: .black, on: "Tap to retry".localizedString())
  }
}
