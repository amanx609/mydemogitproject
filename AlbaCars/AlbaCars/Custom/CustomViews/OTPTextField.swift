//
//  OtpTextField.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

protocol OTPTextFieldDelegate: UITextFieldDelegate {
    func backspacePressed(textField: UITextField)
}

class OTPTextField: UITextField {
    
    override func deleteBackward() {
        super.deleteBackward()
         guard let otpDelete = self.delegate as? OTPTextFieldDelegate else { return }
           otpDelete.backspacePressed(textField: self)
    }
}
