//
//  AVTextField.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//protocol AVTextDelegate: UITextFieldDelegate {
//    func backspacePressed(textField: UITextField)
//}
//
//class AVTextField: UITextField {
//    
//    override func deleteBackward() {
//        super.deleteBackward()
//        guard let avDelete = self.delegate as? AVTextDelegate else { return }
//        avDelete.backspacePressed(textField: self)
//    }
//}
