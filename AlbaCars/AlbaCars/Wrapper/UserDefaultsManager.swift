//
//  UserDefaultsManager.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 12/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

extension DefaultsKeys {
    static let deviceToken = DefaultsKey<String?>("deviceToken")
    static let authToken = DefaultsKey<String?>("authToken")
    static let jwtToken = DefaultsKey<String?>("jwtToken")
    static let userId = DefaultsKey<Int?>("userId")
    static let userType = DefaultsKey<Int?>("userType")
    static let user = DefaultsKey<Data?>("user")
    static let token = DefaultsKey<String?>("token")
    static let notificationCount = DefaultsKey<Int?>("notificationCount")
    static let registrationAmount = DefaultsKey<Int?>("registrationAmount")
    static let s3Bucket = DefaultsKey<String?>("s3Bucket")
    static let s3PoolId = DefaultsKey<String?>("s3PoolId")
    static let s3Region = DefaultsKey<String?>("s3Region")
    static let selectedServiceList = DefaultsKey<[Int]?>("selectedServiceList")
    static let prefixS3Url = DefaultsKey<String?>("prefixS3Url")
    static let auctioneerRequestStatus = DefaultsKey<Int?>("auctioneerRequestStatus")
    static let vatPercentage = DefaultsKey<Double?>("vatPercentage")
        
}

class UserDefaultsManager {
    
    // MARK: - Singleton Instantiation
    private static let _sharedInstance: UserDefaultsManager = UserDefaultsManager()
    static var sharedInstance: UserDefaultsManager {
        return ._sharedInstance
    }
    
    //MARK: - Methods
    //SaveMethods
    func saveUserValueFor(key: DefaultsKey<Data?>, value: User) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(value) {
            Defaults[key] = encoded
            UserSession.shared.initializeValuesForUser()
        }
    }
    
    func saveIntArrayValueFor(key: DefaultsKey<[Int]?>, value: [Int]) {
        Defaults[key] = value
    }
    
    func saveStringValueFor(key: DefaultsKey<String?>, value: String) {
        Defaults[key] = value
    }
    
    func saveIntValueFor(key: DefaultsKey<Int?>, value: Int) {
        Defaults[key] = value
    }
    
    func saveBoolValueFor(key: DefaultsKey<Bool?>, value: Bool) {
        Defaults[key] = value
    }
    
    func saveValuesForNotificationSettings(key: DefaultsKey<Data?>, value: NotificationSettingModel) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(value) {
            Defaults[key] = encoded
            UserSession.shared.initializeValuesForUser()
        }
    }
    
    func saveDoubleValueFor(key: DefaultsKey<Double?>, value: Double) {
        Defaults[key] = value
    }
    
    //RetrieveMethods
    func getUserValueFor(key: DefaultsKey<Data?>) -> User? {
        if let value = Defaults[key] {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(User.self, from: value) {
                return loadedPerson
            }
        }
        return nil
    }
    
    func getStringValueFor(key: DefaultsKey<String?>) -> String? {
        let value = Defaults[key]
        return value
    }
    
    func getIntValueFor(key: DefaultsKey<Int?>) -> Int? {
        let value = Defaults[key]
        return value
    }
    
    func getIntArrayValueFor(key: DefaultsKey<[Int]?>) -> [Int]? {
        let value = Defaults[key]
        return value
    }
    
    func getBoolValueFor(key: DefaultsKey<Bool?>) -> Bool? {
        let value = Defaults[key]
        return value
    }
    func getDoubleValueFor(key: DefaultsKey<Double?>) -> Double? {
        let value = Defaults[key]
        return value
    }
    
    // Remove defaults values
    func removeAllValues() {
        Defaults[.userId] = nil
        Defaults[.token] = nil
        Defaults.synchronize()
    }
    
    
}
