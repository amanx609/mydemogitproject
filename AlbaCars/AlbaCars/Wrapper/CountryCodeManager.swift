//
//  CountryCodeManager.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 16/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import NKVPhonePicker

class CountryCodeManager: NSObject {
  
  // MARK: - Singleton Instantiation
  static let shared: CountryCodeManager = CountryCodeManager()
  fileprivate var countryCodePickerCompletion:((_ countryCode:String?) -> ())?
  
  
  //MARK: - Private Methods
  private func getCountriesVC() -> CountriesViewController? {
    let storyBoard = UIStoryboard(name: "CountriesViewController", bundle: Bundle(for: CountriesViewController.self))
    if let countryViewC = storyBoard.instantiateViewController(withIdentifier: "CountryPickerVC") as? CountriesViewController {
      return countryViewC
    }
    return nil
  }
  
  
  //MARK: - Public Methods
  func getCountryCode(completion: ((_ countryCode:String?) -> ())?) {
    self.countryCodePickerCompletion = completion
    if let topViewC: UIViewController = Helper.topMostViewController(rootViewController: Helper.rootViewController()),
      let countriesVC = self.getCountriesVC() {
      Threads.performTaskInMainQueue {
        countriesVC.delegate = self
        topViewC.present(countriesVC, animated: true, completion: nil)
      }
    } else if let topViewC = AppDelegate.delegate.window?.rootViewController,
      let countriesVC = self.getCountriesVC() {
      Threads.performTaskInMainQueue {
        countriesVC.delegate = self
        topViewC.present(countriesVC, animated: true, completion: nil)
      }
    }
  }
}

//MARK: - CountriesViewControllerDelegate
extension CountryCodeManager: CountriesViewControllerDelegate {
  func countriesViewControllerDidCancel(_ sender: CountriesViewController) {
    sender.dismiss(animated: true, completion: nil)
  }
  
  func countriesViewController(_ sender: CountriesViewController, didSelectCountry country: Country) {
    let phoneExtension = country.phoneExtension
    if self.countryCodePickerCompletion != nil {
      self.countryCodePickerCompletion!("+\(phoneExtension)")
      self.countryCodePickerCompletion = nil
    }
  }
}
