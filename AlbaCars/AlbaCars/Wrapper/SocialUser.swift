//
//  SocialUser.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 18/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

class SocialUser: NSObject {
  
  // MARK: - Variables
  var socialId: String = ""
  var socialToken: String = ""
  var socialType: String = ""
  var name: String = ""
  var email: String = ""
  var image: String = ""
  var birthday: String = ""
}
