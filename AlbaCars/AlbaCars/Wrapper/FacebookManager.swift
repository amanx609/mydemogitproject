//
//  FacebookManager.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit

enum Permission: String {
  case publicProfile
  case birthday
  case email
  
  var stringValue: String {
    switch self {
    case .publicProfile: return "public_profile"
    case .birthday: return "user_birthday"
    case .email: return "email"
    }
  }
}

class FacebookManager: NSObject
{
  // MARK: - Singleton Instantiation
  private static let _sharedInstance: FacebookManager = FacebookManager()
  static var sharedInstance: FacebookManager {
    return ._sharedInstance
  }
  
  // MARK: - Private Properties
  private var socialUserCompletion:((_ user: SocialUser?) -> ())?
    
  // MARK: - Initializers
  private override init() {
    // This will resctrict the instantiation of this class.
  }
  
  // MARK: - Public Methods
  func loginWithFacebook(readPermissions: [String], completion: ((_ user: SocialUser?) -> ())?) {
    var topVC: UIViewController?
    if let topViewC: UIViewController = Helper.topMostViewController(rootViewController: Helper.rootViewController()) {
      topVC = topViewC
    } else if let topViewC = AppDelegate.delegate.window?.rootViewController {
      topVC = topViewC
    }
    self.socialUserCompletion = completion
    let login = LoginManager()
    login.logOut()
    login.logIn(permissions: readPermissions,
                from: topVC, handler: { (result, error) in
                  if (error == nil) {
                    if let loginResult: LoginManagerLoginResult = result {
                      if(loginResult.isCancelled) {
                        //Show Cancel alert
                      } else if(loginResult.grantedPermissions.contains("email")) {
                        self.getInfoAboutUser()
                      }
                    }
                  } else { return }
    })
  }
  
  // MARK: - Private Methods
  private func getInfoAboutUser() {
    
    if AccessToken.current != nil {
      
      let requestMe = GraphRequest.init(graphPath: "me", parameters: ["fields": "id, name, email, birthday, is_verified, picture.type(large), link"])
      
      let connection = GraphRequestConnection()
      connection.add(requestMe, completionHandler: {
        (request, result, error) in
        let result = result as? [String: Any]
        if error != nil {
          LoginManager().logOut()
          return
        } else {
          
          let socialUser = SocialUser()
          
          if let birthday = result?["birthday"] as? String {
            socialUser.birthday = birthday
          }
          if let email = result?["email"] as? String {
            socialUser.email = email
          }
          if let name = result?["name"] as? String {
            socialUser.name = name
          }
          if let userID = result?["id"] as? String{
            socialUser.socialId = userID
          }
          if let profileImage = self.retrieveImageUrlFrom(dictPicture:result?["picture"] as? [String:Any]) {
            socialUser.image = profileImage
          }
          if let facebookToken = AccessToken.current?.tokenString {
            socialUser.socialToken = facebookToken
          }
          socialUser.socialType = SocialType.facebook.rawValue
          if self.socialUserCompletion != nil {
            self.socialUserCompletion!(socialUser)
            self.socialUserCompletion = nil
          }
        }
      })
      connection.start()
    }
  }
  
  private func retrieveImageUrlFrom(dictPicture:[String:Any]?) -> String?{
    if let data = dictPicture?["data"] as? [String:Any]{
      if let imgUrl = data["url"] as? String{
        return imgUrl
      }
    }
    return nil
  }
}

