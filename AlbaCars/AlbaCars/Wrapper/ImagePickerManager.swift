//
//  ImagePickerManager.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 13/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import MobileCoreServices
import DKImagePickerController
import Photos

class ImagePickerManager: NSObject {
    
    // MARK: - Singleton Instantiation
    private static let _sharedInstance: ImagePickerManager = ImagePickerManager()
    static var sharedInstance: ImagePickerManager {
        return ._sharedInstance
    }
    
    //MARK: - Initialization Method
    private override init() {
        
    }
    
    //MARK: - Variables
    lazy var nativeImagePicker = UIImagePickerController()
    fileprivate var isSelectSingleImage: Bool = false
    fileprivate var imagePickerCompletion:((_ image:UIImage?,_ url:URL?) -> ())?
    fileprivate var multipleImagesPickerCompletion:((_ images:[UIImage]?) -> ())?
    
    //MARK: - Asset Methods
    private func getAssetThumbnail(asset: PHAsset, completion: @escaping(UIImage) -> Void) {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            guard let image = result else { return }
            completion(image)
        })
    }
    
    //MARK: - Public Methods
    func getSingleImage(enableEditing: Bool = true,isDocument: Bool = false, completion: ((_ image:UIImage?,_ url:URL?) -> ())?)
    {
        self.isSelectSingleImage = true
        self.nativeImagePicker.delegate = self
        self.nativeImagePicker.allowsEditing = enableEditing
        self.nativeImagePicker.mediaTypes = [(kUTTypeImage) as String]
        self.imagePickerCompletion = completion
        
        let imagePickerVC = DIConfigurator.sharedInstance.getImagePickerAlertVC()
        if let topViewC: UIViewController = Helper.topMostViewController(rootViewController: Helper.rootViewController()) {
            // Threads.performTaskInMainQueue {
            imagePickerVC.delegate = self
            imagePickerVC.isSelectSingleImage = true
            imagePickerVC.isDocument = isDocument
            imagePickerVC.modalPresentationStyle = .overCurrentContext
            topViewC.present(imagePickerVC, animated: true, completion: nil)
            // }
        } else if let topViewC = AppDelegate.delegate.window?.rootViewController {
            //   Threads.performTaskInMainQueue {
            imagePickerVC.delegate = self
            imagePickerVC.isSelectSingleImage = true
            imagePickerVC.isDocument = isDocument
            imagePickerVC.modalPresentationStyle = .overCurrentContext
            topViewC.present(imagePickerVC, animated: true, completion: nil)
            // }
        }
    }
    
    func getMultipleImages(enableEditing: Bool = true, completion:((_ images:[UIImage]?) -> ())?) {
        self.isSelectSingleImage = false
        self.nativeImagePicker.delegate = self
        self.nativeImagePicker.allowsEditing = enableEditing
        self.nativeImagePicker.mediaTypes = [(kUTTypeImage) as String]
        self.multipleImagesPickerCompletion = completion
        let imagePickerVC = DIConfigurator.sharedInstance.getImagePickerAlertVC()
        if let topViewC: UIViewController = Helper.topMostViewController(rootViewController: Helper.rootViewController()) {
            Threads.performTaskInMainQueue {
                imagePickerVC.delegate = self
                imagePickerVC.isSelectSingleImage = false
                imagePickerVC.modalPresentationStyle = .overCurrentContext
                topViewC.present(imagePickerVC, animated: true, completion: nil)
            }
        } else if let topViewC = AppDelegate.delegate.window?.rootViewController {
            Threads.performTaskInMainQueue {
                imagePickerVC.delegate = self
                imagePickerVC.isSelectSingleImage = false
                imagePickerVC.modalPresentationStyle = .overCurrentContext
                topViewC.present(imagePickerVC, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - FilePrivate Methods
    fileprivate func openCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            self.nativeImagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.nativeImagePicker.edgesForExtendedLayout = UIRectEdge.all
            self.nativeImagePicker.showsCameraControls = true
            //PresentNativeImagePickerController
            if let topViewC: UIViewController = Helper.topMostViewController(rootViewController: Helper.rootViewController()) {
                Threads.performTaskInMainQueue {
                    topViewC.present(self.nativeImagePicker, animated: true, completion: nil)
                }
            } else if let topViewC = AppDelegate.delegate.window?.rootViewController {
                Threads.performTaskInMainQueue {
                    topViewC.present(self.nativeImagePicker, animated: true, completion: nil)
                }
            }
        } else {
            //PresentAlertInAbsenceOfCamera
            let alert = UIAlertController(title: "Warning".localizedString(), message: "Camera is not available".localizedString(), preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok".localizedString(), style: .default, handler: nil)
            alert.addAction(okAction)
            if let topViewC: UIViewController = Helper.topMostViewController(rootViewController: Helper.rootViewController()) {
                Threads.performTaskInMainQueue {
                    topViewC.present(alert, animated: true, completion: nil)
                }
            } else if let topViewC = AppDelegate.delegate.window?.rootViewController {
                Threads.performTaskInMainQueue {
                    topViewC.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    fileprivate func openGallery() {
        var selectedImages: [UIImage] = []
        let customeImagePicker = DKImagePickerController()
        customeImagePicker.allowSelectAll = false
        customeImagePicker.singleSelect = self.isSelectSingleImage
        customeImagePicker.autoCloseOnSingleSelect = self.isSelectSingleImage
        customeImagePicker.maxSelectableCount = self.isSelectSingleImage ? 1 : 0
        customeImagePicker.sourceType = .photo
        customeImagePicker.assetType = .allPhotos
        customeImagePicker.showsCancelButton = true
        customeImagePicker.didSelectAssets = { (assets: [DKAsset]) in
            for asset in assets {
                if let originalAsset = asset.originalAsset {
                    self.getAssetThumbnail(asset: originalAsset, completion: { (image) in
                        selectedImages.append(image)
                    })
                }
            }
            if self.imagePickerCompletion != nil, let selectedImage = selectedImages.first {
                self.imagePickerCompletion!(selectedImage, nil)
                self.imagePickerCompletion = nil
                customeImagePicker.dismiss(animated: true, completion: nil)
            }
            if self.multipleImagesPickerCompletion != nil {
                self.multipleImagesPickerCompletion!(selectedImages)
                self.multipleImagesPickerCompletion = nil
                customeImagePicker.dismiss(animated: true, completion: nil)
            }
        }
        //PresentCustomImagePickerController
        if let topViewC: UIViewController = Helper.topMostViewController(rootViewController: Helper.rootViewController()) {
            Threads.performTaskInMainQueue {
                topViewC.present(customeImagePicker, animated: true, completion: nil)
            }
        } else if let topViewC = AppDelegate.delegate.window?.rootViewController {
            Threads.performTaskInMainQueue {
                topViewC.present(customeImagePicker, animated: true, completion: nil)
            }
        }
    }
    
    fileprivate func openDocumentsPicker() {
        
        let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeImage), String(kUTTypeContent)], in: UIDocumentPickerMode.import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        
        if let topViewC: UIViewController = Helper.topMostViewController(rootViewController: Helper.rootViewController()) {
            Threads.performTaskInMainQueue {
                topViewC.present(documentPicker, animated: true, completion: nil)
            }
        } else if let topViewC = AppDelegate.delegate.window?.rootViewController {
            Threads.performTaskInMainQueue {
                topViewC.present(documentPicker, animated: true, completion: nil)
            }
        }
        
    }
}

//MARK: - UIImagePicker Delegates
extension ImagePickerManager: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        var image = info[.originalImage] as? UIImage
        if image == nil {
            image = info[.editedImage] as? UIImage
        }
        if let pickedImage = image,
            let compressedData = pickedImage.jpegData(compressionQuality: JPEGQuality.medium.rawValue),
            let compressedImage = UIImage(data: compressedData) {
            
            if self.imagePickerCompletion != nil {
                self.imagePickerCompletion!(compressedImage, nil)
                self.imagePickerCompletion = nil
            }
            if self.multipleImagesPickerCompletion != nil {
                self.multipleImagesPickerCompletion!([compressedImage])
                self.multipleImagesPickerCompletion = nil
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}


extension ImagePickerManager: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {

        if urls.count > 0 {
            let url = urls[0]
            self.imagePickerCompletion?(nil, url)
        }
    }
}

//MARK: - ImagePickerAlertDelegate
extension ImagePickerManager: ImagePickerAlertDelegate {
    func didTapGallery(viewC: ImagePickerAlertViewC) {
        viewC.dismiss(animated: true, completion: {
            self.openGallery()
        })
    }
    
    func didTapCamera(viewC: ImagePickerAlertViewC) {
        viewC.dismiss(animated: true, completion: {
            self.openCamera()
        })
    }
    
    func didTapDocument(viewC: ImagePickerAlertViewC) {
        viewC.dismiss(animated: true, completion: {
            self.openDocumentsPicker()
        })
    }
}
