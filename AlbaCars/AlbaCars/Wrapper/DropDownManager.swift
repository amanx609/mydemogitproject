//
//  DropDownManager.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 19/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import DropDown

class DropDownManager: NSObject {
  
  // MARK: - Singleton Instantiation
  private static let _sharedInstance: DropDownManager = DropDownManager()
  static var sharedInstance: DropDownManager {
    return ._sharedInstance
  }
  
  //MARK: - Private Variables
  private var dropDown = DropDown()
  
  //MARK: - Public Methods
  func startListeningToKeyboard() {
    DropDown.startListeningToKeyboard()
  }
  
  func showDropDown(anchorView: UIView, dataSource: [[String: AnyObject]], key: String, completion: @escaping (String)-> Void) {
    let dropDownDataSource = self.getDataSourceForDropDownFrom(dataSource: dataSource, with: key)
    self.dropDown.frame = CGRect(x: 0, y: 0, width: self.dropDown.frame.width, height: 200.0)
    self.dropDown.anchorView = anchorView
    dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
    self.dropDown.dataSource = dropDownDataSource
    self.dropDown.show()
    self.dropDown.selectionAction = { [weak self] (index: Int, item: String) in
      guard let weakSelf = self else { return }
      completion(item)
      weakSelf.dropDown.hide()
    }
  }
  
  //MARK: - Private Methods
  private func setupDropDown() {
    DropDown.appearance().textColor = UIColor.darkGray
    DropDown.appearance().backgroundColor = UIColor.white
    DropDown.appearance().cellHeight = 60
  }
  
  private func getDataSourceForDropDownFrom(dataSource: [[String: AnyObject]], with key: String) -> [String] {
    var newDataSource = [String]()
    for data in dataSource {
      if let value = data[key] as? String {
        newDataSource.append(value)
      }
    }
    return newDataSource
  }
}
