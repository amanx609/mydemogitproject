//
//  S3Manager.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit
import AWSCore
import AWSS3
import Alamofire

enum ContentType: String {
  case image
  case video
  case other
}

let s3bucketFolder = "S3Bucket"

typealias progressBlock = (_ totalByteSend: user_long_t, _ totoalByteExpectedToSend: user_long_t, _ isFinished: Bool) -> Void
typealias myProgressBlock = (_ progress: Double, _ isFinished: Bool) -> Void //2
typealias completionBlock = (_ response: Any?, _ imageName: Any?, _ error: Error?) -> Void
typealias completionHandler = (_ response : String? , _ error: Error?) -> Void

class S3Manager {
  
  // MARK: - Singleton Instantiation
  private static let _sharedInstance: S3Manager = S3Manager()
  static var sharedInstance: S3Manager {
    return ._sharedInstance
  }
  
  //MARK: - Variables
  var uploadProgress:((_ totalByteSend: user_long_t, _ totoalByteExpectedToSend: user_long_t, _ isFinished: Bool) -> Void)?
  var responseBlock:((_ response: Any?, _ error: Error?) -> Void)?
  var uploadReqCoverPic: AWSS3TransferManagerUploadRequest?
  var uploadReqDisplayPic: AWSS3TransferManagerUploadRequest?
  var uploadReqFile: AWSS3TransferManagerUploadRequest?
  
  func getS3CredenitalsAndInitialize(completion: @escaping (Bool)-> Void) {
    APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .s3Credentials)) { (response, success) in
      if success {
        print(response)
        if let safeResponse =  response as? [String: AnyObject],
          let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
          let s3BucketName = result[ConstantAPIKeys.bucket] as? String,
          let s3PoolId = result[ConstantAPIKeys.poolId] as? String,
          let s3Region = result[ConstantAPIKeys.region] as? String,
          let prefixUrl = result[ConstantAPIKeys.prefixUrl] as? String {
          UserDefaultsManager.sharedInstance.saveStringValueFor(key: .s3Bucket, value: s3BucketName)
          UserDefaultsManager.sharedInstance.saveStringValueFor(key: .s3PoolId, value: s3PoolId)
          UserDefaultsManager.sharedInstance.saveStringValueFor(key: .s3Region, value: s3Region)
          UserDefaultsManager.sharedInstance.saveStringValueFor(key: .prefixS3Url, value: prefixUrl)
          self.initializeS3()
          completion(true)
        }
      }
    }
  }
  
  // Upload image using UIImage object
  func uploadImage(image: UIImage, imageQuality: JPEGQuality, progress: myProgressBlock?, completion: completionBlock?) {
    //HaveS3CredentialsInitialized
    if let _ = UserDefaultsManager.sharedInstance.getStringValueFor(key: .s3PoolId),
      let _ = UserDefaultsManager.sharedInstance.getStringValueFor(key: .s3Bucket) {
      self.uploadImageOnS3(image: image, imageQuality: imageQuality, progress: progress, completion: completion)
    } else {
      //Don'tHaveS3CredentialsInitialized
      self.getS3CredenitalsAndInitialize { (success) in
        self.uploadImageOnS3(image: image, imageQuality: imageQuality, progress: progress, completion: completion)
      }
    }
  }
  
  func uploadImages(images: [UIImage], imageQuality: JPEGQuality, progress: myProgressBlock?, completion: completionBlock?) {
    //HaveS3CredentialsInitialized
    if let _ = UserDefaultsManager.sharedInstance.getStringValueFor(key: .s3PoolId),
      let _ = UserDefaultsManager.sharedInstance.getStringValueFor(key: .s3Bucket) {
      self.uploadMultipleImages(images: images, imageQuality: imageQuality, progress: progress, completion: completion)
    } else {
      //Don'tHaveS3CredentialsInitialized
      self.getS3CredenitalsAndInitialize { (success) in
        self.uploadMultipleImages(images: images, imageQuality: imageQuality, progress: progress, completion: completion)
      }
    }
  }
    
    func uploadOtherFile(fileUrl: URL, conentType: String, progress: myProgressBlock?, completion: completionBlock?)
    {
        if let _ = UserDefaultsManager.sharedInstance.getStringValueFor(key: .s3PoolId),
            let _ = UserDefaultsManager.sharedInstance.getStringValueFor(key: .s3Bucket) {
            self.uploadOtherFilesOnS3(fileUrl: fileUrl, conentType: conentType, progress: progress, completion: completion)
        } else {
            //Don'tHaveS3CredentialsInitialized
            self.getS3CredenitalsAndInitialize { (success) in
                self.uploadOtherFilesOnS3(fileUrl: fileUrl, conentType: conentType, progress: progress, completion: completion)
            }
        }
        
    }
  
  func cancelAllUpload() {
    let _transferUtility = AWSS3TransferUtility.default()
    _transferUtility.enumerateToAssignBlocks(forUploadTask: { (uploadTask:AWSS3TransferUtilityUploadTask, progress:AutoreleasingUnsafeMutablePointer<(@convention(block) (AWSS3TransferUtilityTask, Progress) -> Void)?>?, error: AutoreleasingUnsafeMutablePointer<(@convention(block) (AWSS3TransferUtilityUploadTask, Error?) -> Void)?>?) in
      uploadTask.cancel()
    }, downloadTask: nil)
  }
  
  func getUploadedImageNameFrom(url: String) -> String {
     return (url as NSString).lastPathComponent
  }
  
  func getImageUrlFrom(imageName: String) -> String {
    var imageUrl: String = ""
    if let prefixUrl = UserDefaultsManager.sharedInstance.getStringValueFor(key: .prefixS3Url) {
      imageUrl = prefixUrl + imageName
    }
    return imageUrl
  }
}

extension S3Manager {
  //MARK: - Private Methods
  private func initializeS3() {
      if let poolId = UserDefaultsManager.sharedInstance.getStringValueFor(key: .s3PoolId),
        let s3Region = UserDefaultsManager.sharedInstance.getStringValueFor(key: .s3Region){
          //      let credentialsProvider = AWSCognitoCredentialsProvider(regionType: .EUWest1, identityPoolId: poolId)
          //      let configuration = AWSServiceConfiguration(region: .EUWest1, credentialsProvider: credentialsProvider)
          
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType: .regionTypeForString(regionString: s3Region), identityPoolId: poolId)
          let configuration = AWSServiceConfiguration(region: .regionTypeForString(regionString: s3Region), credentialsProvider: credentialsProvider)
          AWSServiceManager.default().defaultServiceConfiguration = configuration
      }
  }
  
  private func uploadMultipleImages(images: [UIImage], imageQuality: JPEGQuality, progress: myProgressBlock?, completion: completionBlock?) {
    let uploaderGroup = DispatchGroup()
    var imageUrlArray: [String] = []
    
    Loader.showLoader(title: "Uploading Images".localizedString())
    for image in images {
      uploaderGroup.enter()
      self.uploadImageOnS3(image: image, imageQuality: imageQuality, progress: progress) { (response, imageName, error) in
        if let imageName = imageName as? String {
          imageUrlArray.append(imageName)
        }
        uploaderGroup.leave()
      }
    }
    uploaderGroup.notify(queue: DispatchQueue.main, execute: {
      let error = NSError(domain: "", code: 0, userInfo: nil)
      Loader.hideLoader()
      completion?(imageUrlArray, nil, error)
    })
  }
  
  private func getS3FolderPath() -> URL? {
    let fileManager = FileManager.default
    if let docDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
      let filePath =  docDirectory.appendingPathComponent("\(s3bucketFolder)")
      return filePath
    } else {
      return nil
    }
  }
  
  private func getFilePath(_ fileName: String) -> String {
    if let s3FolderPath = self.getS3FolderPath() {
      let filePath = s3FolderPath.appendingPathComponent(fileName)
      return filePath.path
    } else {
      return ""
    }
  }
  
  private func uploadImageOnS3(image: UIImage, imageQuality: JPEGQuality, progress: myProgressBlock?, completion: completionBlock?) {
    guard let imageData = image.jpegData(compressionQuality: imageQuality.rawValue) else {
      let error = NSError(domain:"", code:402, userInfo:[NSLocalizedDescriptionKey: "invalid image"])
      completion?(nil, nil, error)
      return
    }
    
    let tmpPath = NSTemporaryDirectory() as String
    let fileName: String = ProcessInfo.processInfo.globallyUniqueString + (".jpeg")
    let filePath = tmpPath + "/" + fileName
    let fileUrl = URL(fileURLWithPath: filePath)
    do {
      try imageData.write(to: fileUrl)
      self.uploadfile(fileUrl: fileUrl, fileName: fileName, contenType: ContentType.image.rawValue, progress: progress, completion: completion)
    } catch {
      let error = NSError(domain:"", code: 402, userInfo:[NSLocalizedDescriptionKey: "invalid image"])
      completion?(nil, nil, error)
    }
  }
  
    // Upload files like Text, Zip, etc from local path url
    func uploadOtherFilesOnS3(fileUrl: URL, conentType: String, progress: myProgressBlock?, completion: completionBlock?) {
        let fileName = self.getUniqueFileName(fileUrl: fileUrl)
        self.uploadfile(fileUrl: fileUrl, fileName: fileName, contenType: conentType, progress: progress, completion: completion)
    }
    
    // Get unique file name
    func getUniqueFileName(fileUrl: URL) -> String {
        let strExt: String = "." + (URL(fileURLWithPath: fileUrl.absoluteString).pathExtension)
        return (ProcessInfo.processInfo.globallyUniqueString + (strExt))
    }
    
  //MARK:- AWS file upload
  // fileUrl :  file local path url
  // fileName : name of file, like "myimage.jpeg" "video.mov"
  // contenType: file MIME type
  // progress: file upload progress, value from 0 to 1, 1 for 100% complete
  // completion: completion block when uplaoding is finish, you will get S3 url of upload file here
  
  private func uploadfile(fileUrl: URL, fileName: String, contenType: String, progress: myProgressBlock?, completion: completionBlock?) {
    
    let bucketName = UserDefaultsManager.sharedInstance.getStringValueFor(key: .s3Bucket) ?? ""
    
    // Upload progress block
    let expression = AWSS3TransferUtilityUploadExpression()
    expression.progressBlock = {(task, awsProgress) in
      guard let upload = progress else { return }
      DispatchQueue.main.async {
        // upload(awsProgress.completedUnitCount, awsProgress.totalUnitCount, awsProgress.isFinished)
        print(awsProgress)
        upload(awsProgress.fractionCompleted,awsProgress.isFinished)
      }
    }
    // Completion block
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    completionHandler = { (task, error) -> Void in
      DispatchQueue.main.async(execute: {
        if error == nil {
          let url = AWSS3.default().configuration.endpoint.url
          let publicURL = url?.appendingPathComponent(fileName)
          print("Uploaded to:\(String(describing: publicURL))")
          if let completionBlock = completion {
            if let imageUrl = publicURL?.absoluteString {
              let imageName = self.getUploadedImageNameFrom(url: imageUrl)
              completionBlock(publicURL?.absoluteString, imageName, nil)
            } else {
              completionBlock(publicURL?.absoluteString, nil, nil)
            }
          }
        } else {
          if let completionBlock = completion {
            completionBlock(nil, nil, error)
          }
        }
      })
    }
    // Start uploading using AWSS3TransferUtility
    let awsTransferUtility = AWSS3TransferUtility.default()
    awsTransferUtility.uploadFile(fileUrl, bucket: bucketName, key: fileName, contentType: contenType, expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
      if let error = task.error {
        print("error is: \(error.localizedDescription)")
      }
      if let _ = task.result {
        // your uploadTask
      }
      return nil
    }
  }
}

extension AWSRegionType {
    
    /**
        Return an AWSRegionType for the given string
    
        - Parameter regionString: The Region name (e.g. us-east-1) as a string
    
        - Returns: A new AWSRegionType for the given string, Unknown if no region was found.
    */
    static func regionTypeForString(regionString: String) -> AWSRegionType {
        switch regionString {
            case "us-east-1": return .USEast1
            case "us-west-1": return .USWest1
            case "us-west-2": return .USWest2
            case "eu-west-1": return .EUWest1
            case "eu-central-1": return .EUCentral1
            case "ap-northeast-1": return .APNortheast1
            case "ap-northeast-2": return .APNortheast2
            case "ap-southeast-1": return .APSoutheast1
            case "ap-southeast-2": return .APSoutheast2
            case "sa-east-1": return .SAEast1
            case "cn-north-1": return .CNNorth1
            case "us-gov-west-1": return .USGovWest1
            default: return .USEast2
        }
    }
    
    
}
