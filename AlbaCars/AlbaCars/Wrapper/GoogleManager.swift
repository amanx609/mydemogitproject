//
//  GoogleManager.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 17/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import GoogleSignIn
import GooglePlaces
import GoogleMaps

class GoogleManager: NSObject {
  
  // MARK: - Singleton Instantiation
  private static let _sharedInstance: GoogleManager = GoogleManager()
  static var sharedInstance: GoogleManager {
    return ._sharedInstance
  }
  
  //MARK: - Variables
  fileprivate var socialUserCompletion:((_ user: SocialUser?) -> ())?
  
  func initializeGoogleSignIn() {
    GIDSignIn.sharedInstance().clientID = ConstantAPITokensAndUrls.googleClientId
    GIDSignIn.sharedInstance().delegate = self
  }
    
  func initializeGooglePaces() {
        // Set the Google Place API's autocomplete UI control
        GMSPlacesClient.provideAPIKey(ConstantAPITokensAndUrls.googlePlacesKey)
        GMSServices.provideAPIKey(ConstantAPITokensAndUrls.googlePlacesKey)

   }
  
  func handleGoogleUrl(url: URL) -> Bool {
    return GIDSignIn.sharedInstance().handle(url)
  }
  
  func loginWithGoogle(completion: ((_ user: SocialUser?) -> ())?) {
    if let topViewC: UIViewController = Helper.topMostViewController(rootViewController: Helper.rootViewController()) {
      GIDSignIn.sharedInstance()?.presentingViewController = topViewC
    } else if let topViewC = AppDelegate.delegate.window?.rootViewController {
      GIDSignIn.sharedInstance()?.presentingViewController = topViewC
    }
    self.socialUserCompletion = completion
    GIDSignIn.sharedInstance().signOut()
    GIDSignIn.sharedInstance().signIn()
  }
}

//MARK: - GIDSignInDelegate
extension GoogleManager: GIDSignInDelegate {
  func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
    if let error = error {
      print("\(error.localizedDescription)")
    } else {
      let socialUser = SocialUser()
      socialUser.socialId = user.userID
      socialUser.socialToken = user.authentication.idToken
      socialUser.name = user.profile.name
      socialUser.email = user.profile.email
      if user.profile.hasImage {
        let dimension = round(100 * UIScreen.main.scale)
        if let image = user.profile.imageURL(withDimension: UInt(dimension)) {
          socialUser.image = "\(image)"
        }
      }
      socialUser.socialType = SocialType.google.rawValue
      if self.socialUserCompletion != nil {
        self.socialUserCompletion!(socialUser)
        self.socialUserCompletion = nil
      }
    }
  }
}

