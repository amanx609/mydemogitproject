//
//  APIClient.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 13/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

class APIClient: NSObject {
  
  // MARK: - Singleton Instantiation
  private static let _sharedInstance: APIClient = APIClient()
  static var sharedInstance: APIClient {
    return ._sharedInstance
  }
  
  private override init() {
    
  }
  
  //MARK: - Public Methods
  func request(apiRouter: APIRouter, completion: @escaping (Any, Bool)-> Void) {
    let _ = APIManager.shared.request(apiRouter: apiRouter, completionHandler: completion)
  }
}
