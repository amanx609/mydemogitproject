//
//  APIManager.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Alamofire
import Reachability

class APIManager: Alamofire.SessionManager {
  
  internal static let shared: APIManager = {
    let manager = Alamofire.SessionManager.default
    manager.session.configuration.timeoutIntervalForRequest = 60
    return APIManager()
  }()
  var arrOfHitsAPI = [[String: Any]]()
  var requestCache = [String: RequestCaching]()
  var requestDic = [String: DataRequest]()
  
  //MARK: - Request
  func request(apiRouter: APIRouter, completionHandler: @escaping (_ responseData: [String: AnyObject], _ success: Bool) -> Void) {
    
    let key = getUniqueForRequest(apiRouter)
    if Loader.isReachabile() == false {
      if apiRouter.showAlert {
        Alert.showOkAlert(title: StringConstants.Text.AppName, message: ConstantAPITexts.noInternetConnectionTryAgain.localize)
      }
      /*
       print(apiRouter.detail.parameter)
       if apiRouter.supportOffline, let data = DataCache.instance.readData(forKey: key) {
       do {
       let json = try JSONSerialization.jsonObject(with: data) as? [String: Any]
       if let reponse = json, let result = reponse[kResult] as? [String: AnyObject] {
       completionHandler(result, true)
       }
       } catch {
       print("error")
       }
       }*/
      completionHandler([:], false)
      return
    } //Return if found no network or use cache value
    if apiRouter.showLoader == true { Loader.showLoader() } //Start loader
    print(apiRouter.detail.parameter)
    Threads.performTaskAfterDealy(0.1) {
      
      if !apiRouter.path.isEmpty {
        Debug.Log(message: "🔴Request => \(apiRouter.baseUrl)\(apiRouter.path) : \(String(describing: apiRouter.parameters)) ☄️")
      }
      
      // Create Key
      
      let requestCaching = RequestCaching(apiRouter: apiRouter, completionHandler: completionHandler)
      self.requestCache[key] = requestCaching
      
      
      // Send request to server
      let req = self.request(apiRouter).responseJSON { (responseData: DataResponse<Any>) in
        
        self.requestDic.removeValue(forKey: key) // Removed excuted request
        
        //Debug.Log(message: "response: \(responseData)")
        if let url = responseData.request?.url {
          if let data = responseData.result.value as? [String:AnyObject] {
            Debug.Log(message: "🔴Response Of => \(url) =>\n \(data) ☄️")
          }
        }
        
        if responseData.response == nil {
          
          if apiRouter.apiCounter == 1 {
            var apiRouterNew = apiRouter
            apiRouterNew.apiCounter = 2
            self.request(apiRouter: apiRouterNew, completionHandler: completionHandler)
            return
          }
          
          /*if apiRouter.supportOffline, /*let data = DataCache.instance.readData(forKey: key)*/ {
           do {
           let json = try JSONSerialization.jsonObject(with: data) as? [String: Any]
           if let reponse = json, let result = reponse[kResult] as? [String: AnyObject] {
           completionHandler(result, true)
           }
           } catch {
           print("error")
           }
           } else {*/
          completionHandler([String: AnyObject](), false)
          Alert.showOkAlert(title: StringConstants.Text.AppName, message: ConstantAPITexts.serverNotResponding.localize)
          //}
          
          if apiRouter.showLoader == true { // Hide loader
            Loader.hideLoader()
          }
          return
        }
        
        var dataObject = [String: AnyObject]()
        if let responseData = responseData.result.value as? [String:AnyObject] {
          dataObject = responseData
        } else if let responseData = responseData.result.value {
          dataObject = ["response": responseData as AnyObject]
        }
        
        if let responseCode = dataObject[kResponseCode] as? Int,
          responseCode == ResponseCodes.blockedCode {
          Loader.hideLoader()
            Alert.showOkAlertWithCallBack(title: StringConstants.Text.AppName, message:"Your account has been blocked/deleted by admin.") { (_) in
               AppDelegate.delegate.showLogin()
            }
          return
        }
        
        if let responseCode = dataObject[kResponseCode] as? Int,
          responseCode == ResponseCodes.tokenExpireResponseCode {
          self.holdAPIRequestApi(apiRouter: apiRouter, completionClosure: completionHandler)
          self.requestRegenerateAccessTokenAPI()
          completionHandler([:], false)
          return 
        }
        
        if let success = dataObject[kSuccess] as? Int, success == 1 {
          self.removeHoldAPIRequestApi(apiRouter.path)
          completionHandler(dataObject, true)
          
          if apiRouter.showMessageOnSuccess, let msg = dataObject[kMessage] as? String {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: msg)
          }
          
          //if apiRouter.supportOffline, let data = responseData.data {
          //DataCache.instance.write(data: data, forKey: key)
          //}
        } else {
          
          completionHandler(dataObject, false)
          if let message = dataObject[kMessage] as? String {
            if apiRouter.showAlert == true {
              Alert.showOkAlert(title: StringConstants.Text.AppName, message: message)
            }
            if let responseCode = dataObject[kResponseCode] as? Int, responseCode == 2 {
              
            }
          }
        }
        if apiRouter.showLoader == true { // Hide loader
          Loader.hideLoader()
        }
      }
      self.requestDic[key] = req
    }
  }
  
  
  func holdAPIRequestApi(apiRouter: APIRouter, completionClosure:  @escaping (_ responseData: [String: AnyObject], _ success: Bool) -> Void){
    
    var holdDic = [String:Any]()
    holdDic["apiRouter"] = apiRouter
    holdDic["callBack"] = completionClosure
    arrOfHitsAPI.append(holdDic)
  }
  
  func reHitHoldAPI(){
    for holdDic in arrOfHitsAPI {
      guard let apirouter = holdDic["apiRouter"] as? APIRouter,
        let completionClosure = holdDic["callBack"] as? ([String: AnyObject], Bool)->(Void)  else{ continue}
      
      self.request(apiRouter: apirouter, completionHandler: completionClosure)
    }
  }
  
  func removeHoldAPIRequestApi(_ serviceName: String){
    let index = arrOfHitsAPI.firstIndex { (holdDic) -> Bool in
      guard let apiRouter = holdDic["apiRouter"] as? APIRouter, apiRouter.path == serviceName  else{return false}
      return true
    }
    if let tempIdex = index, tempIdex < arrOfHitsAPI.count{
      arrOfHitsAPI.remove(at: tempIdex)
    }
  }
  
  func cancelAllRequests() {
    requestCache.removeAll()
    let allKeys = requestDic.keys
    for key in allKeys {
      if let safeRequest = requestDic[key] {
        safeRequest.cancel()
        requestDic.removeValue(forKey: key)
      }
    }
    // Clear request dic
    requestDic.removeAll()
  }
  
  func dataObject(response: DataResponse<Any>) -> [String: AnyObject] {
    var data = [String: AnyObject]()
    if let dataDict = response.result.value as? [String: AnyObject] {
      data = dataDict
    }
    else if let dataArr = response.result.value as? [AnyObject] {
      data = ["response": dataArr as AnyObject]
    }
    return data
  }
  
  func onError(message: String, success: Bool, dataObject: [String: AnyObject], errorCode: Int, showAlert: Bool,  completionHandler: @escaping ([String: AnyObject], Bool) -> ()) {
    
    if !showAlert {
      completionHandler(dataObject, success)
      return
    }
    let _ = Helper.topMostViewController(rootViewController: Helper.rootViewController())
    if errorCode != 0 {
      
      // Alert.showAlert(message, okButtonTitle: ConstantTexts.cancel.localized, target: target)
    }
    completionHandler(dataObject, success)
  }
  
  
  func getUniqueForRequest(_ apiRouter: APIRouter) -> String {
    var key = "\(apiRouter.baseUrl)/\(apiRouter.path)"
    for (k,v) in Array(apiRouter.parameters).sorted(by: {$0.0 < $1.0}) {
      key.append(k)
      key.append("\(v)")
    }
    key = key.replacingOccurrences(of: "//", with: "_")
    key = key.replacingOccurrences(of: "/", with: "_")
    key = key.replacingOccurrences(of: ",", with: "_")
    key = key.replacingOccurrences(of: "@", with: "_")
    key = key.replacingOccurrences(of: ":", with: "_")
    key = key.replacingOccurrences(of: ".", with: "_")
    return key
  }
  
  
  func handleError(_ responseData: [String: AnyObject]){
    
  }
  
  private class func handleProgress(
    progress: CGFloat,
    completionProgress: @escaping(_ progress: CGFloat) -> Void) {
    completionProgress(progress)
  }
  
  //RegenerateTokenAPI
  private func requestRegenerateAccessTokenAPI() {
    var params: APIParams = APIParams()
    params[ConstantAPIKeys.userId] = UserSession.shared.getUserId() as AnyObject
    params[ConstantAPIKeys.userType] = UserSession.shared.getUsertype() as AnyObject
    params[ConstantAPIKeys.deviceType] = Constants.Devices.deviceType as AnyObject
    params[ConstantAPIKeys.deviceToken] = UserSession.shared.getDeviceToken() as AnyObject
    APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .regenerateAccessToken(param: params))) { (response, success) in
      if success {
        if let safeResponse =  response as? [String: AnyObject],
          let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
          let token = result[ConstantAPIKeys.token] as? String {
          UserDefaultsManager.sharedInstance.saveStringValueFor(key: .token, value: token)
          Threads.performTaskAfterDealy(0.1) {
            self.reHitHoldAPI()
          }
        }
      }
    }
  }
}

struct RequestCaching {
  var apiRouter: APIRouter
  var completionHandler: ((_ responseData: [String: AnyObject], _ success: Bool) -> Void)?
}

