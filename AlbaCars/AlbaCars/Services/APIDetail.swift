//
//  APIDetail.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import Alamofire

struct APIDetail {
    
    var path: String = ""
    var parameter: APIParams = APIParams()
    var method: Alamofire.HTTPMethod = .get
    var encoding: ParameterEncoding = URLEncoding.default
    var isBaseUrlNeedToAppend: Bool = true
    var showLoader: Bool = true
    var showAlert: Bool = true
    var showMessageOnSuccess = false
    var isHeaderTokenRequired: Bool = true
    var isHeaderUserTypeRequired: Bool = true
    var supportOffline = false
    var isHeaderTimeZoneRequired: Bool = true
    
    
    //method = .put .post
    //encoding = JSONEncoding.default
    
    //method = .delete .get
    //encoding = URLEncoding.default
    
    init(endpoint: APIEndpoint) {
        
        switch endpoint {
            
        case .signin(let param):
            path = "signin"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            isHeaderTokenRequired = false
            
        case .signup(let param):
            path = "signup"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            isHeaderTokenRequired = false
            
        case .changepassword(let param, let senderType):
            path = "changePassword"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            if senderType == .forgotPassword {
                isHeaderTokenRequired = false
            }
            
        case .s3Credentials:
            path = "s3Credentials"
            parameter = [:]
            method = .get
            encoding = URLEncoding.default
            showLoader = false
            showAlert = false
            
        case .forgotPassword(let param):
            path = "forgotPassword"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            isHeaderTokenRequired = false
            
        case .helpInfo:
            path = "helpInfo"
            isHeaderTokenRequired = true
            method = .get
            encoding = URLEncoding.default
            
        case .myCars(let param):
            parameter = param
            path = "car"
            method = .get
            encoding = URLEncoding.default
            
        case .verifyOTP(let param):
            path = "verifyOTP"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
            
        case .sendOTP(let param):
            path = "sendOTP"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
            
        case .uploadDocument(let param):
            path = "documents"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            
        case .updateServiceList(let param):
            path = "updateServiceList"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            
        case .feedback(let param):
            path = "feedback"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            
        case .socialSignin(let param):
            path = "socialSignin"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            showAlert = false
            isHeaderTokenRequired = false
            
        case .logout:
            path = "logout"
            method = .post
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            
        case .getDocuments:
            path = "documents"
            isHeaderTokenRequired = true
            method = .get
            encoding = URLEncoding.default
            
        case .skipRegistrationPayment:
            path = "skipRegistrationPayment"
            method = .put
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            
        case .vehicleBrand:
            path = "vehicleBrand"
            method = .get
            encoding = URLEncoding.default
            
        case .vehicleModel(let param):
            if let brandId = param[ConstantAPIKeys.brandId] as? Int {
                path = "vehicleModel/\(brandId)"
            }
            method = .get
            encoding = URLEncoding.default
            
        case .vehicleType:
            path = "vehicleType"
            method = .get
            encoding = URLEncoding.default
            
        case .postCar(let param):
            path = "car"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .fleetCompany(let param):
            parameter = param
            path = "fleetCompany"
            method = .get
            encoding = URLEncoding.default
            
        case .fleet(let param):
            if let companyId = param[ConstantAPIKeys.companyId] as? Int {
                path = "fleet/\(companyId)"
            }
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .fleetCar(let param):
            if let fleetId = param[ConstantAPIKeys.fleetId] as? Int {
                path = "fleetCar/\(fleetId)"
            }
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .submitFleetBid(let param):
            path = "submitFleetBid"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .myBids(let param):
            path = "myBids"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .postSellVehicle(let param):
            path = "sellVehicle"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .getSellVehicle(let param):
            
            path = "sellVehicle"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .postRequestVehicle(let param):
            path = "requestVehicle"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .getRequestVehicle(let param):
            path = "requestVehicle"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .updateProfile(let param):
            path = "profile"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
            
        case .postAuctionCar(let param):
            if let auctionPage = param[ConstantAPIKeys.auctionPage] as? String {
                path = "acrCar/\(auctionPage)"
            }
            parameter = param
            method = .put
            encoding = JSONEncoding.default
            
        case .regenerateAccessToken(let param):
            path = "regenerateAccessToken"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
            showAlert = false
            
        case .postAuctioneerRequestStatus:
            path = "actioneerRequestStatus"
            method = .post
            encoding = JSONEncoding.default
            
        case .getAuctioneerRequestStatus:
            path = "actioneerRequestStatus"
            parameter = [:]
            method = .get
            encoding = URLEncoding.default
            
        case .cancelPostRequest(let param):
            if let auctionId = param[ConstantAPIKeys.id] as? Int {
                path = "cancelPostRequest/\(auctionId)"
            }
            parameter = param
            method = .delete
            encoding = JSONEncoding.default
            
        case .getSavedAuctionCar(let param):
            path = "acrCar"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .postSavedAuctionCar(let param):
            if let auctionId = param[ConstantAPIKeys.id] as? Int {
                path = "postCar/\(auctionId)"
            }
            method = .put
            encoding = URLEncoding.default
            
        case .buyFleetBid(let param):
            if let bidId = param[ConstantAPIKeys.bidId] as? Int {
                path = "buyFleetBid/\(bidId)"
            }
            parameter = [:]
            method = .get
            encoding = URLEncoding.default
            
        case .buyVehicle(let param):
            path = "buyVehicle"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .vehicleOffer(let param):
            path = "vehicleOffer"
            parameter = param
            method = .get
            encoding = URLEncoding.default
        case .buyVehicleFilterCount(let param):
            showLoader = false
            path = "buyVehicleFilterCount"
            parameter = param
            method = .get
            encoding = URLEncoding.default
        case .chooseCar:
            path = "chooseCar"
            method = .get
            encoding = URLEncoding.default
            
        case .carFinance(let param):
            path = "carFinance"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .buyVehicleDetails(let param):
            path = "buyVehicleDetails"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .agencyList(let param):
            path = "agencyList"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .getWarranty(let param):
            path = "getWarranty"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .getServiceContract(let param):
            path = "getServiceContact"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .warrantyDetails(let param):
            path = "warrantyDetails"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .notifyMe(let param):
            path = "notifyMe"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .payNow(let param):
            path = "payNow"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .buyWarrantyService(let param):
            path = "buyWarrantyService"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .auctionRoomList(let param):
            path = "auctionRoomList"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .serviceDetails(let param):
            path = "serviceDetails"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .serviceHistory(let param):
            path = "serviceHistory"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .getCarFinance(let param):
            path = "carFinanceStatus"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .updateCarFinance(let param):
            path = "carFinance"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
            
        case .auctionCarList(let param):
            path = "auctionCarList"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .bankFinance(let param):
            path = "carFinance"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .tenderRequestForm(let param):
            path = "tender"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            isHeaderTimeZoneRequired = true
            
        case .tenderList(let param):
            path = "tenderListUser"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .bidList(let param):
            path = "bidList"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .tenderDetails(let param):
            path = "tenderDetailsUser"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .getBuyLeads(let param):
            path = "getBuyLeads"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .getSellLeads(let param):
            path = "getSellLeads"
            parameter = param
            
        case .tenderHomeNotifications(let param):
            path = "smartTenderScreenUser"
            method = .get
            parameter = param
            encoding = URLEncoding.default
            
        case .getAuctionLeaderBoard:
            path = "getAuctionLeaderboard"
            method = .get
            encoding = URLEncoding.default
            
            
        case .getFleetLeaderBoard:
            path = "getFleetLeaderboard"
            method = .get
            encoding = URLEncoding.default
            
        case .getLeadsLeaderBoard:
            path = "getLeadsLeaderboard"
            method = .get
            encoding = URLEncoding.default
            
        case .buyLead(let param):
            path = "buyLead"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .getMyBuyLeads(let param):
            path = "getMyBuyLeads"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .getMySellLeads(let param):
            path = "getMyBuyLeads"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .getMyLeadsStatusCount(let param):
            path = "getMyLeadsStatusCount"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .updateLeadStatus(let param):
            path = "updateLeadStatus"
            parameter = param
            method = .put
            encoding = JSONEncoding.default         
        case .acceptBid(let param):
            path = "acceptBid"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .rejectBid(let param):
            //            if let tenderId = param[ConstantAPIKeys.tenderId] as? Int {
            //                path = "rejectBid/\(tenderId)"
            //            }
            path = "rejectBid"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
            
        case .submitBid(let param):
            path = "submitBid"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
            
        case .activeTenderListServiceProvider(let param):
            path = "activeTenderListServiceProvider"
            parameter = param
            method = .get
            encoding = URLEncoding.default
        case .activeTenderListServiceProviderDetails(let param):
            path = "activeTenderListServiceProviderDetails"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .myTenderDetailServiceProvider(let param):
            path = "myTenderDetailServiceProvider"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .myTenderListServiceProvider(let param):
            path = "myTenderListServiceProvider"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .cancelTender(let param):
            //            if let tenderId = param[ConstantAPIKeys.tenderId] as? Int {
            //                path = "cancelTender?tenderId=\(tenderId)"
            //            }
            path = "cancelTender"
            parameter = param
            method = .post
            encoding = URLEncoding.default
            
        case .jobCompleted(let param):
            path = "jobCompleted"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .verifyPickupCode(let param):
            path = "verifyPickupCode"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .verifyDropoffCode(let param):
            path = "verifyDropoffCode"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .auctionNotifyMe(let param):
            path = "auctionNotifyMe"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .makeLeadExclusive(let param):
            path = "makeLeadExclusive"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
            
        case .initiatePayment(let param):
            path = "initiatePayment"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
        case .bankList:
            path = "bankList"
            method = .get
            encoding = URLEncoding.default
        case .notificationList(let param):
            path = "notificationList"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .serviceProviderMyTransactions(let param):
            path = "serviceProviderMyTransactions"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .getNotificationSettings:
            path = "notificationSettings"
            method = .get
            encoding = URLEncoding.default
            
        case .postNotificationSettings(let param):
            path = "notificationSettings"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
            
        case .acrCarDetail(let param):
            path = "acrCarDetail"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .customerPaymentHistory(let param):
            //            if let page = param[ConstantAPIKeys.page] as? Int {
            //                path = "paymentHistory?page=\(page)"
            //            }
            path = "paymentHistory"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .enterAuctionRoom(let param):
            path = "enterAuctionRoom"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
            
        case .getAuctionHighestBid(let param):
            path = "getAuctionHighestBid"
            parameter = param
            method = .post
        case .supplierPaymentHistory(let param):
            //            if let page = param[ConstantAPIKeys.page] as? Int, let type = param[ConstantAPIKeys.type] as? Int {
            //                path = "serviceProviderPaymentHistory?page=\(page)&status=\(type)"
            //            }
            path = "serviceProviderPaymentHistory"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .myWatchList(let param):
            path = "watchListListing"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .addWatchList(let param):
            path = "addWatchList"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
            
        case .updatePayment(let param):
            path = "updatePayment"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
        case .auctionPlaceBid(let param):
            path = "auctionPlaceBid"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
        case .liveAuctionBuyNow(let param):
            path = "buyNow"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
        case .winBid(let param):
            path = "winBid"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
        case .skipDocumentUpload:
            path = "skipDocumentUpload"
            method = .put
            encoding = JSONEncoding.default
        case .requestPayment:
            path = "requestPayment"
            method = .put
            encoding = JSONEncoding.default
        case .auctionPrices:
            path = "auctionPrices"
            method = .get
            encoding = URLEncoding.default
        case .listCard:
            path = "listCard"
            method = .get
            encoding = URLEncoding.default

        case .deleteCard(let param):
            path = "deleteCard"
            parameter = param
            method = .delete
            encoding = JSONEncoding.default
            
        case .updateCar(let param):
            path = "car"
            parameter = param
            method = .put
            encoding = JSONEncoding.default
            
        case .deleteCar(let param):
            path = "car"
            parameter = param
            method = .delete
            encoding = JSONEncoding.default
            
        case .profile:
            path = "profile"
            method = .get
            encoding = URLEncoding.default
            showLoader = false
            showAlert = false
            
        case .getFinanceCarDetails(let param):
            path = "getfinanceCarDetails"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .getSellVehicleDetails(let param):
            path = "getSellVehicleDetails"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .getRequestedVehicleDetails(let param):
            path = "getRequestedVehicleDetails"
            parameter = param
            method = .get
            encoding = URLEncoding.default
           
        case .getCarDetails(let param):
            path = "getCarDetails"
            parameter = param
            method = .get
            encoding = URLEncoding.default
            
        case .uploadServiceDocument(let param):
            path = "uploadServiceDocument"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            
        case .getSettings:
            path = "admin/getSettings"
            parameter = [:]
            method = .get
            encoding = URLEncoding.default
            showLoader = false
            showAlert = false
            isHeaderTokenRequired = false
        case .notificationCount:
            path = "notificationCount"
            method = .get
            encoding = URLEncoding.default
            isHeaderTokenRequired = true
            
        default:
            break
        }
    }
}
