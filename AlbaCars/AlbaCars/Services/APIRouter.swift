//
//  APIRouter.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import Alamofire

public typealias JSONDictionary = [String: AnyObject]
typealias APIParams = [String: AnyObject]

struct APIRouter: URLRequestConvertible {
  
  var apiCounter = 1
  var endpoint: APIEndpoint
  var detail: APIDetail
  init(endpoint: APIEndpoint) {
    
    self.endpoint = endpoint
    self.detail = APIDetail.init(endpoint: endpoint)
  }
  
  var baseUrl: String {
    
    return APIEnvironment.Base.rawValue
  }
  
  var method: Alamofire.HTTPMethod {
    
    return detail.method
  }
  
  var path: String {
    
    return detail.path
  }
  
  var parameters: APIParams {
    
    return detail.parameter
  }
  
  var encoding: ParameterEncoding? {
    
    return detail.encoding
  }
  
  var showAlert: Bool {
    
    return detail.showAlert
  }
  
  var showMessageOnSuccess: Bool {
    
    return detail.showMessageOnSuccess
    
  }
  
  var showLoader: Bool {
    
    return detail.showLoader
  }
  
  var supportOffline: Bool {
    
    return detail.supportOffline
  }
  
  var isBaseUrlNeedToAppend: Bool {
    
    return detail.isBaseUrlNeedToAppend
  }
  
  var isHeaderTokenRequired: Bool {
    return detail.isHeaderTokenRequired
  }
  
  var isHeaderUserTypeRequired: Bool {
    return detail.isHeaderUserTypeRequired
  }
    
  var isHeaderTimeZoneRequired: Bool {
    return detail.isHeaderTimeZoneRequired
  }
  
  var isAuthenticationRequired: Bool {
    
    switch endpoint {
      
    default:
      return true
    }
  }
  
  /// Returns a URL request or throws if an `Error` was encountered.
  /// - throws: An `Error` if the underlying `URLRequest` is `nil`.
  /// - returns: A URL request.
  
  func asURLRequest() throws -> URLRequest {
    
    let url = URL(string: baseUrl)
    var urlRequest = URLRequest(url: isBaseUrlNeedToAppend ? (url?.appendingPathComponent(self.path))! : URL(string: self.path)!)
    urlRequest.httpMethod = method.rawValue
    urlRequest.timeoutInterval = 60
    /*
     let username = "c30997b04ea83db19f8d72020fee2a9d6a73fbd6"
     let password = "d744243fb884759804ee948b867f9d1edf7f9428"
     let key = "81ad3389c7-7f3c1d40113-8dcadda-e5e796"
     let credentialData = "\(username):\(password)".data(using: String.Encoding.utf8)!
     let base64Credentials = credentialData.base64EncodedString()
     urlRequest.setValue("Basic \(base64Credentials)", forHTTPHeaderField: "Authorization")*/
    
    if isHeaderTokenRequired {
      switch endpoint {
      case .s3Credentials:
        let token = ConstantAPITokensAndUrls.s3CredentialsToken
        print("s3CredentialsToken is -------------------> " , token)
        urlRequest.setValue(token, forHTTPHeaderField:kToken)
      case .regenerateAccessToken:
        let token = ConstantAPITokensAndUrls.regenrateAccessToken
        print("regenrateAccessToken is -------------------> " , token)
        urlRequest.setValue(token, forHTTPHeaderField:kToken)
      default:
        let token = UserSession.shared.getAccessToken()
        print("User Token is -------------------> " , token)
        urlRequest.setValue(token, forHTTPHeaderField:kToken)
      }
    }
    
    if isHeaderUserTypeRequired {
      let userType = UserSession.shared.getUsertype()
      print("User-Type is -------------------> " , userType)
      urlRequest.setValue("\(userType)", forHTTPHeaderField: kUserType)
    }
    
    if isHeaderTimeZoneRequired {
        let timezone = "Dubai"
        print("Time Zone is -------------------> " , timezone)
        urlRequest.setValue("\(timezone)", forHTTPHeaderField: kTimezone)
      }
    
    return try encoding!.encode(urlRequest, with: self.parameters)
    
    /*
     let finalparam = self.getParam(param: self.parameters)
     let postData = finalparam.data(using: String.Encoding.ascii, allowLossyConversion: true)
     urlRequest.httpBody = postData
     return urlRequest*/
  }
  
  
  func getParam(param: [String: AnyObject]) -> String {
    var finalParam = ""
    for (key, value) in param {
      if finalParam.isEmpty {
        finalParam = "\(key)=\(value)"
      } else {
        finalParam = finalParam + "&" + "\(key)=\(value)"
      }
    }
    return finalParam
  }
}

