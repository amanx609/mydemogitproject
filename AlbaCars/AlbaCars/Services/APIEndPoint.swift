//
//  APIEndPoint.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//


import Foundation
import Alamofire

enum APIEndpoint {
    
    case none
    case signin(param: APIParams)
    case signup(param: APIParams)
    case s3Credentials
    case changepassword(param: APIParams, senderType: ChangePwdSenderType)
    case forgotPassword(param: APIParams)
    case helpInfo
    case myCars(param: APIParams)
    case verifyOTP(param: APIParams)
    case sendOTP(param: APIParams)
    case uploadDocument(param: APIParams)
    case updateServiceList(param: APIParams)
    case feedback(param: APIParams)
    case logout
    case getDocuments
    case socialSignin(param: APIParams)
    case skipRegistrationPayment
    case vehicleBrand
    case vehicleModel(param: APIParams)
    case vehicleType
    case postCar(param: APIParams)
    case fleetCompany(param: APIParams)
    case fleet(param: APIParams)
    case fleetCar(param: APIParams)
    case submitFleetBid(param: APIParams)
    case myBids(param: APIParams)
    case postSellVehicle(param: APIParams)
    case getSellVehicle(param: APIParams)
    case updateProfile(param: APIParams)
    case postRequestVehicle(param: APIParams)
    case getRequestVehicle(param: APIParams)
    case postAuctionCar(param: APIParams)
    case regenerateAccessToken(param: APIParams)
    case cancelPostRequest(param: APIParams)
    case getSavedAuctionCar(param: APIParams)
    case postSavedAuctionCar(param: APIParams)
    case buyFleetBid(param: APIParams)
    case postAuctioneerRequestStatus
    case getAuctioneerRequestStatus
    case buyVehicle(param: APIParams)
    case vehicleOffer(param: APIParams)
    case buyVehicleFilterCount(param: APIParams)
    case chooseCar
    case carFinance(param: APIParams)
    case buyVehicleDetails(param: APIParams)
    case agencyList(param: APIParams)
    case getWarranty(param: APIParams)
    case getServiceContract(param: APIParams)
    case notifyMe(param: APIParams)
    case payNow(param: APIParams)
    case warrantyDetails(param: APIParams)
    case buyWarrantyService(param: APIParams)
    case auctionRoomList(param: APIParams)
    case serviceDetails(param: APIParams)
    case serviceHistory(param: APIParams)
    case getCarFinance(param: APIParams)
    case updateCarFinance(param: APIParams)
    case auctionCarList(param: APIParams)
    case bankFinance(param: APIParams)
    case tenderRequestForm(param: APIParams)
    case tenderList(param: APIParams)
    case bidList(param: APIParams)
    case tenderDetails(param: APIParams)
    case getBuyLeads(param: APIParams)
    case getSellLeads(param: APIParams)
    case buyLead(param: APIParams)
    case getMyBuyLeads(param: APIParams)
    case getMySellLeads(param: APIParams)
    case getMyLeadsStatusCount(param: APIParams)
    case updateLeadStatus(param: APIParams)
    case activeTenderListServiceProvider (param: APIParams)
    case activeTenderListServiceProviderDetails (param: APIParams)
    case myTenderListServiceProvider(param: APIParams)
    case myTenderDetailServiceProvider(param: APIParams)
    case tenderHomeNotifications(param: APIParams)
    case getAuctionLeaderBoard
    case getFleetLeaderBoard
    case getLeadsLeaderBoard
    case acceptBid(param: APIParams)
    case submitBid(param: APIParams)
    case rejectBid(param: APIParams)
    case cancelTender(param: APIParams)
    case jobCompleted(param: APIParams)
    case verifyPickupCode(param: APIParams)
    case verifyDropoffCode(param: APIParams)
    case auctionNotifyMe(param: APIParams)
    case makeLeadExclusive(param: APIParams)
    case initiatePayment(param: APIParams)
    case bankList
    case notificationList(param: APIParams)
    case serviceProviderMyTransactions(param: APIParams)
    case getNotificationSettings
    case postNotificationSettings(param: APIParams)
    case acrCarDetail(param: APIParams)
    case enterAuctionRoom(param: APIParams)
    case getAuctionHighestBid(param: APIParams)
    case customerPaymentHistory(param: APIParams)
    case supplierPaymentHistory(param: APIParams)
    case myWatchList(param: APIParams)
    case addWatchList(param: APIParams)
    case updatePayment(param: APIParams)
    case auctionPlaceBid(param: APIParams)
    case liveAuctionBuyNow(param: APIParams)
    case winBid(param: APIParams)
    case skipDocumentUpload
    case requestPayment(param: APIParams)
    case auctionPrices
    case listCard
    case deleteCard(param: APIParams)
    case updateCar(param: APIParams)
    case deleteCar(param: APIParams)
    case profile
    case getFinanceCarDetails(param: APIParams)
    case getSellVehicleDetails(param: APIParams)
    case getRequestedVehicleDetails(param: APIParams)
    case getCarDetails(param: APIParams)
    case uploadServiceDocument(param: APIParams)
    case getSettings
    case notificationCount


}

