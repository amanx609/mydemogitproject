//
//  AppDelegate+Additions.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import Firebase

//MARK: - Routing Methods
extension AppDelegate {
    
    func disableDarkMode() {
        #if compiler(>=5.1)
        if #available(iOS 13.0, *) {
         // Always adopt a light interface style.
         window?.overrideUserInterfaceStyle = .light
        }
        #endif
    }
    
    func showInitialVC() {
        UserDefaultsManager.sharedInstance.saveIntValueFor(key: .userType, value: UserType.customer.rawValue)

        if UserSession.shared.isLoggedIn() {
            UserSession.shared.initializeValuesForUser()
            self.showHome()
        } else {
            self.showLoginType()
        }
    }
    
    
    func showInitialViewC() {
        let initialVC = DIConfigurator.sharedInstance.getDChooseAuctionTypeVC()
        let navC = UINavigationController(rootViewController: initialVC)
        //navC.setNavigationBarHidden(true, animated: false)
        self.window?.rootViewController = navC
    }
    
    func showHome() {
        let homeVC = DIConfigurator.sharedInstance.getHomeVC()
        let nav = UINavigationController()
        nav.setViewControllers([homeVC], animated: false)
        let mainVC = DIConfigurator.sharedInstance.getMainViewController()
        let leftSideMenuVC = DIConfigurator.sharedInstance.getSideMenuVC()
        mainVC.rootViewController = nav
        mainVC.leftViewController = leftSideMenuVC
        mainVC.setup(type: 0)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainVC
    }
    
    func showLoginType() {
        let initialVC = DIConfigurator.sharedInstance.getLoginVC()
        let navC = UINavigationController(rootViewController: initialVC)
        navC.setNavigationBarHidden(true, animated: false)
        self.window?.rootViewController = navC
    }
    
    func showLogin() {
        let initialVC = DIConfigurator.sharedInstance.getLoginVC()
        let navC = UINavigationController(rootViewController: initialVC)
        navC.setNavigationBarHidden(true, animated: false)
        self.window?.rootViewController = navC
    }
    
    func sendMessage(bidAmount:Int,vehicleId:Int) {
        
        if let mainViewController = Helper.visibleController() as? MainViewController {
            
            if let navigationController = mainViewController.rootViewController as? UINavigationController {
                
                for controller in navigationController.viewControllers {
                    
                    if controller.isKind(of:DLiveAuctionRoomViewC.self){
                        let liveAuctionRoomViewC = controller as! DLiveAuctionRoomViewC
                        liveAuctionRoomViewC.updateCurrentBid(price: bidAmount, vehicleId: vehicleId)
                    }
                }
            }
        }
    }
}

//MARK: - Push Notification Methods
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func registerForPushNotifications(application: UIApplication) {
        
        if #available(iOS 10.0, *)
        {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: { (granted, error) in
                Threads.performTaskInMainQueue {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            })
        } else { //If user is not on iOS 10 use the old methods we've been using
            let notificationSettings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
            application.registerUserNotificationSettings(notificationSettings)
        }
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //    let deviceToken = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        //    let lowerCaseDToken = deviceToken.lowercased()
        //    UserDefaultsManager.sharedInstance.saveStringValueFor(key: .deviceToken, value: lowerCaseDToken)
        //    print(lowerCaseDToken)
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print(error)
            } else if let result = result {
                
                UserDefaultsManager.sharedInstance.saveStringValueFor(key: .deviceToken, value: result.token)
                print("Device Token ->>>>>> \(result.token)")
            }
        }
        Messaging.messaging().apnsToken = deviceToken
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("i am not available in simulator \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print("didReceiveRemoteNotification: \(userInfo)")
       // self.handleNotification(userInfo:userInfo)

//        if let amount = userInfo[AnyHashable("bidAmount")], let vehicleId = userInfo[AnyHashable("vehicleId")] {
//            self.sendMessage(bidAmount: Helper.toInt(amount), vehicleId: Helper.toInt(vehicleId))
//        }
    }
    
    // Banner Tapped - App Active
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
//        if let amount = userInfo[AnyHashable("bidAmount")], let vehicleId = userInfo[AnyHashable("vehicleId")] {
//            self.sendMessage(bidAmount: Helper.toInt(amount), vehicleId: Helper.toInt(vehicleId))
//        }
        
        Threads.performTaskAfterDealy(0.2) {
            self.handleNotification(userInfo: response.notification.request.content.userInfo)
        }
        print("User info -----\(userInfo)")
        
    }
    
    // This method will be called when app receives push notifications in foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
       
        
        print("received notification\(notification.request.content.userInfo)")
        completionHandler([.alert,.sound,.badge])
    }
}

extension AppDelegate : MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("didRefreshRegistrationToken: \(fcmToken)")
        print("token: \(messaging)")
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        print("Firebase registration messaging: \(messaging)")
        
        UserDefaultsManager.sharedInstance.saveStringValueFor(key: .deviceToken, value: fcmToken!)
        
        //NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    
}

extension AppDelegate {
    
    func handleNotification(userInfo: [AnyHashable : Any]) {
        
        if let pushType = userInfo[AnyHashable("pushType")] {
            
            
            if let mainViewController = Helper.visibleController() as? MainViewController {
                
                if let navigationController = mainViewController.rootViewController as? UINavigationController {
                   
                    let selectedId = Helper.toInt(userInfo[AnyHashable("id")])
                    
                    switch Helper.toInt(pushType) {
                        
                    case 2:
                        
                        let sellCarIntroVC = DIConfigurator.sharedInstance.getSellCarListVC()
                        sellCarIntroVC.sellCarDetails = true
                        sellCarIntroVC.vehicleId = selectedId
                        navigationController.pushViewController(sellCarIntroVC, animated: true)
                        
                    case 3:
                        
                        let requestCarVC = DIConfigurator.sharedInstance.getCRequestedCarListVC()
                        requestCarVC.requestedCarDetails = true
                        requestCarVC.vehicleId = selectedId
                        navigationController.pushViewController(requestCarVC, animated: true)
                        
                    case 4:
                        let bankFinanceVC = DIConfigurator.sharedInstance.getCarFinanceStatusVC()
                        bankFinanceVC.financeDetails = true
                        bankFinanceVC.financeId = selectedId
                        navigationController.pushViewController(bankFinanceVC, animated: true)
                        
                    case 5:
                        let myCarDetailsViewC = DIConfigurator.sharedInstance.getMyCarDetailsViewC()
                        myCarDetailsViewC.carDetail = true
                        myCarDetailsViewC.vehicleId = selectedId
                        navigationController.pushViewController(myCarDetailsViewC, animated: true)
                    default:
                        break
                    }
                }
            }
            
        }
        
    }
    
}






