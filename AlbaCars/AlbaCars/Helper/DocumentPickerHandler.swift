//
//  DocumentPickerHandler.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import AVFoundation


class DocumentPickerHandler: NSObject {
  
  //MARK: - Variables
  static let sharedHandler = DocumentPickerHandler()
  var guestInstance: UIViewController? = nil
  var imageDocClosure: ((UIImage?, URL?)->())? = nil
  
  //MARK: - Private Methods
  
  private func openCamera(picker: UIImagePickerController)
  {
    if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
    {
      picker.sourceType = UIImagePickerController.SourceType.camera
      picker.edgesForExtendedLayout = UIRectEdge.all
      picker.showsCameraControls = true
      self.guestInstance?.present(picker, animated: true, completion: nil)
    }
    else
    {
      let alert = UIAlertController(title: "Warning".localizedString(), message: "Camera is not available".localizedString(), preferredStyle: .alert)
      let actionOK = UIAlertAction(title: "Ok".localizedString(), style: .default, handler: nil)
      alert.addAction(actionOK)
      self.guestInstance?.present(alert, animated: true, completion: nil)
    }
  }
  
  private func openGallery(picker: UIImagePickerController)
  {
    picker.sourceType = UIImagePickerController.SourceType.photoLibrary
    self.guestInstance?.present(picker, animated: true, completion: nil)
  }
  
  private func openDocumentsPicker()
  {
    let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeImage)], in: UIDocumentPickerMode.import)
    documentPicker.delegate = self
    documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
    self.guestInstance?.present(documentPicker, animated: true, completion: nil)
  }
  
  
  //MARK: - Public Methods
  
  func getImageDoc(instance: UIViewController, rect: CGRect?, allowEditing: Bool? = true , completion: ((UIImage?, URL?)->())?)
  {
    guestInstance = instance
    let imgPicker = UIImagePickerController()
    imgPicker.delegate = self
    imgPicker.allowsEditing = allowEditing ?? true
    imgPicker.mediaTypes = [(kUTTypeImage) as String]
    let actionSheet = UIAlertController(title: "Upload Image".localizedString(), message: nil, preferredStyle: .actionSheet)
    let actionSelectCamera = UIAlertAction(title: "Take a Photo".localizedString(), style: .default, handler: {
      UIAlertAction in
      self.openCamera(picker: imgPicker)
    })
    let actionSelectGallery = UIAlertAction(title: "Select from Camera Roll".localizedString(), style: .default, handler: {
      UIAlertAction in
      self.openGallery(picker: imgPicker)
      
    })
    let actionSelectFiles = UIAlertAction(title: "Select from My Files".localizedString(), style: .default, handler: {
      UIAlertAction in
      self.openDocumentsPicker()
      
    })
    let actionCancel = UIAlertAction(title: "Cancel".localizedString(), style: .cancel, handler: nil)
    actionSheet.addAction(actionCancel)
    actionSheet.addAction(actionSelectCamera)
    actionSheet.addAction(actionSelectGallery)
    actionSheet.addAction(actionSelectFiles)
    
    if UIDevice.current.userInterfaceIdiom == .phone
    {
      Threads.performTaskInMainQueue {
        self.guestInstance?.present(actionSheet, animated: true, completion: nil)
      }
    }
    else if rect != nil
    {
      actionSheet.popoverPresentationController?.sourceView = guestInstance?.view
      actionSheet.popoverPresentationController?.sourceRect = rect!
      actionSheet.popoverPresentationController?.permittedArrowDirections = .any
      Threads.performTaskInMainQueue {
        self.guestInstance?.present(actionSheet, animated: true, completion: nil)
      }
    }
    self.imageDocClosure = {
      (image, documentUrl) in
      completion?(image, documentUrl)
    }
  }
  
  func getImage(instance: UIViewController, rect: CGRect?, allowEditing: Bool? = true , completion: ((UIImage?, URL?)->())?)
  {
    guestInstance = instance
    let imgPicker = UIImagePickerController()
    imgPicker.delegate = self
    imgPicker.allowsEditing = allowEditing ?? true
    imgPicker.mediaTypes = [(kUTTypeImage) as String]
    let actionSheet = UIAlertController(title: "Upload Image".localizedString(), message: nil, preferredStyle: .actionSheet)
    let actionSelectCamera = UIAlertAction(title: "Take a Photo".localizedString(), style: .default, handler: {
      UIAlertAction in
      self.openCamera(picker: imgPicker)
    })
    let actionSelectGallery = UIAlertAction(title: "Select from Camera Roll".localizedString(), style: .default, handler: {
      UIAlertAction in
      self.openGallery(picker: imgPicker)
      
    })
    let actionCancel = UIAlertAction(title: "Cancel".localizedString(), style: .cancel, handler: nil)
    actionSheet.addAction(actionCancel)
    actionSheet.addAction(actionSelectCamera)
    actionSheet.addAction(actionSelectGallery)
    
    if UIDevice.current.userInterfaceIdiom == .phone
    {
      Threads.performTaskInMainQueue {
        self.guestInstance?.present(actionSheet, animated: true, completion: nil)
      }
    }
    else if rect != nil
    {
      actionSheet.popoverPresentationController?.sourceView = guestInstance?.view
      actionSheet.popoverPresentationController?.sourceRect = rect!
      actionSheet.popoverPresentationController?.permittedArrowDirections = .any
      Threads.performTaskInMainQueue {
        self.guestInstance?.present(actionSheet, animated: true, completion: nil)
      }
    }
    self.imageDocClosure = {
      (image, documentUrl) in
      completion?(image, documentUrl)
    }
  }
}

//MARK: - UIImagePicker Delegates
extension DocumentPickerHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) //Cancel button  of imagePicker
  {
    picker.dismiss(animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) //Picking Action of ImagePicker
  {
    // Local variable inserted by Swift 4.2 migrator.
    let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
    picker.dismiss(animated: true, completion: nil)
    
    if let img = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
      imageDocClosure?(img, nil)
    }
  }
}

extension DocumentPickerHandler: UIDocumentPickerDelegate {
  func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
    print(urls)
    if urls.count > 0 {
      let docUrl = urls[0]
      imageDocClosure?(nil, docUrl)
    }
  }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
  return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
  return input.rawValue
}
