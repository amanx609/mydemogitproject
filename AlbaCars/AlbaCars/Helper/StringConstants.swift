//
//  StringConstants.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation

struct StringConstants {
    
    struct SettingsURL {
        static let termsAndCondition = ""
        static let privacyPolicy = ""
    }
    
    struct LimitationTextField {
        static let fullName = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ"
    }
    
    struct Validations {
        static let enterFirstName = "Please enter first name"
        static let enterLastName = "Please enter last name"
        static let enterEmailAddress = "Please enter an email"
        static let enterAValidEmailAddress = "Enter a valid email"
        static let enterEmailOrContact = "Please enter an email or a phone Number"
        static let enterPassword = "Please enter password"
        static let enterMinLengthPassword = "Your password must be at least 8 characters"
        static let oldAndNewPasswordCannotBeSame = "Old and New password can't be same"
        static let oldAndNewPasscodeCannotBeSame = "Old and New passcode can't be same"
        static let enterConfirmPassword = "Enter confirm password"
        static let passwordAndConfirmPasswordDoesnotMatch = "Password and confirm password doesn't match"
        static let newPasswordAndConfirmNewPasswordDoesnotMatch = "New password and confirm new password doesn't match"
        static let enterOTP = "Enter OTP"
        static let enterUsername = "Enter Username"
        static let enterAValidEmailID = "Please enter a valid Email"
        static let passwordShouldBeBetween = "Your password should be between 8 - 20 characters"
        static let enterContactNo = "Enter Contact Number"
        static let enterNewPassword = "Enter New Password"
        static let enterTellAbout = "Enter About Me"
        static let enterExperience = "Enter Experience"
        static let enterPhoneNumber = "Enter Phone Number"
        static let enterLocation = "Enter Location"
        static let enterServiceCategory = "Choose Service Category"
        static let enterServiceName = "Enter Service Name"
        static let enterServiceLocation = "Choose Service Location"
        static let enterPrice = "Enter Price"
        static let enterServiceDuration = "Enter Service Duration"
        static let enterDate = "Enter Date"
        static let enterClientName = "Enter Client Name"
        static let enterFromTime = "Enter From Time"
        static let enterToTime = "Enter To Time"
        static let enterNotes = "Enter Notes"
        static let enterBuildingOrApartment = "Enter Building/Apartment"
        static let enterSuburb = "Enter Suburb"
        static let enterStreet = "Enter Street"
        static let enterCity = "Enter City"
        static let selectATimeForService = "Please select a time for service"
        static let enterReasonForCancellation = "Enter reason for cancellation"
        static let selectAReason = "Select a reason"
        static let enterTime = "Enter Time"
        static let enterReason = "Enter Reason"
        static let selectMethodForBuying = "Please select a method for buying car"
        
    }
    
    struct TimeTexts {
        static let time_0 = "00:00"
        static let time_min_3 = "03:00"
    }
    
    struct Errors {
        static let noInternet = "No Internet"
        static let serverNotResponding = "Server not responding"
    }
    
    struct Text {
        
        static let AppName = "Alba Cars"
        static let Currency = "AED"
        static let Kms = "Kms"
        static let Cancel = "Cancel"
        static let No = "No"
        static let Yes = "Yes"
        static let ok = "OK"
        static let done = "Done"
        static let cancel = "Cancel"
        static let firstName = "First Name"
        static let lastName = "Last Name"
        static let email = "Email"
        static let password = "Password"
        static let phoneOrEmail = "Email or Phone Number"
        static let username = "Username"
        static let contactNumber = "Contact Number"
        static let confirmPassword = "Confirm Password"
        static let buildingOrApartment = "Building/Apartment"
        static let streetAddress = "Street Address"
        static let suburb = "Suburb"
        static let city = "City"
        static let addAddress = "Add Address"
        static let newPassword = "New Password"
        static let name = "Name"
        static let phoneNumber = "Phone Number"
        static let location = "Location"
        static let rating = "Rating"
        static let serviceCategory = "Service Category"
        static let schedule = "Schedule"
        static let replaceByMistake = "Placed the Request by mistake"
        static let serviceAtOtherTime = "Want the service at some other time"
        static let hiredSomeone = "Hired someone else"
        static let others = "Others"
        static let reason = "Reason"
        static let specificReason = "Please specify your reason for cancelling this service."
        static let date = "Date"
        static let clientName = "Client Name"
        static let time = "Time"
        static let notes = "Notes"
        static let phoneNo = "Phone No"
        static let address = "Address"
        static let appointmentDate = "Appointment Date"
        static let subscriptionPlan = "Subscription Plans"
        static let freeTrial = "Free 7 day trial"
        static let monthly = "Monthly- $10"
        static let annually = "Annually- $10"
        static let planEnable = "Plans enable you to receive full access to iLuud Content"
        static let getFeatured = "Get Featured on iLuud"
        static let days = "14 Days"
        static let categoryName = "Category Name"
        static let serviceName = "Service Name"
        static let price = "Price: "
        static let serviceLocation = "Service Location"
        static let workAddress = "Work Address"
        static let serviceDuration = "Service Duration"
        static let experience = "Experience"
        static let workHistory = "Work History"
        static let aboutMe = "About Me"
        static let tellAbout = "Tell us about yourself"
        static let mobileNumber = "Mobile Number"
        static let enterOTP = "Enter OTP"
        static let businessName = "Business name"
        static let other = "Other"
        static let travelTime = "Travel Time"
        static let priceType = "Price Type"
        static let kFromTime = "From Time"
        static let kToTime = "To Time"
        static let kStartJob = "Are you sure you want to start the job?"
        static let kCompleteJob = "Are you sure you want to complete the job?"
        static let enterReason = "Enter reason for cancelling"
        static let subCategory = "Sub Category"
        static let rate = "Rate"
        static let totalJobs = "Total Jobs"
        static let services = "Services"
        static let workingHour = "Working Hour"
        static let gold = "Gold"
        static let silver = "Silver"
        static let description = "Description"
        static let jobDescription = "Job Description"
        static let brand = "Brand: "
        static let carType = "Car Type: "
        static let dateText = "Date: "
        static let timeText = "Time: "
        static let recoveryType = "Recovery Type: "
        static let modelText = "Model: "
        static let modelYearText = "Model Year: "
        static let vinNumber = "VIN Number"
        static let bids = "Bids"
        static let serviceDetails = "Service Details"
        static let servicingDetails = "Servicing Details"
        static let insuranceDetails = "Insurance Details"
                
        //MARK: - Description
        static let provideYourFeedback = "Provide your feedback here."
        static let incorrectDetails = "I provide the incorrect details on my booking"
        static let changedMind = "I changed my mind and no longer want the service"
        static let nameCategory = "(Name your category)"
        static let noBookingList = "No Bookings Available"
        static let noAppointmentList = "No Appointments Available"
        static let noBlockDate = "No Block Dates Available"
        static let selectImage = "Select Profile Image"
        static let uploadImageLimit = "Cannot upload more than 20 Images"
        static let uploadImage = "Upload Image"
        static let payWithCash = "Do you really want to pay with cash?"
        static let additionalInfo = "Additional Information"
        static let servicePrice = "servicePrice"
        
        //MARK: - Choose LoginType
        static let customer = "Continue as Customer"
        static let dealer  = "Dealer"
        static let supplier = "Supplier"
        
        //MARK: - OTP Verification
        static let didntReceive = "Didn’t Receive?"
        static let resendCode = "Resend Now"
        static let otpSent = "OTP has been sent to your registered mobile number"
        static let verifyOTP = "Verify OTP"
        
        //MARK: - Add New Car
        static let saveCar = "Save Car"
        static let selectCarBrand = "Select Car Brand"
        static let selectCarModel = "Car Model"
        static let selectCarType = "Select Car Type"
        static let selectCarYear = "Car Year"
        static let carPlate = "Car Plate/ VIN Number"
        static let carMileage = "Car Mileage"
        static let numOfCylinders = "No. Of Cylinders"
        static let numOfCylindersStr = "Number of Cylinders: "
        static let engineSizeStr = "Engine Size: "
        static let frontBrakeStr = "Front Brake Pads: "
        static let vinStr = "VIN Number: "
        static let brakeDiscs = "Brake Discs: "
        static let rearBrakeStr = "Rear Brake Pads: "
        static let engineSize = "Engine Size"
        static let tintColor = "Tint Color"
        static let tintPercentage = "Tint %age of Visiblity"
        static let tintColorTxt = "Tint Color : "
        static let tintVisibilityTxt = "Tint Visibility : "
        static let paintColor = "Paint Color"
        static let uploadhistory = "Upload Service History"
        
        //MARK: - Bank Valuation
        static let bankName = "Bank Name : "
        static let requestedCar = "Requested Car Value : "
        static let carMileageText = "Car Mileage : "
        
        //MARK: - My Cars
        static let addNewCar = "Add New Car"
        static let myCars = "My Cars"
        static let selectFile = "Select File From"

        
        //MARK: - Settings
        static let changePassword = "Change Password"
        static let termsConditions = "Terms & Conditions"
        static let privacyPolicy = "Privacy Policy"
        static let signOut = "Sign Out"
        
        //MARK: - Notification Settings
        static let carSelectionQuestion = "For which cars do you want to receive notificactions?"
        static let forAuctions = "For Auctions"
        static let selectYears = "Years"
        static let selectBrands = "Brands"
        static let selectMileage = "Select Mileage"
        static let minimum = "Minimum"
        static let maximum = "Maximum"
        static let options = "Options"
        static let smartTenders = "Smart Tenders"
        static let financeApplication = "Finance Application"
        static let fleetBiddings = "Fleet Biddings"
        static let leads = "Leads"
        static let sale = "Sale"
        static let offerDeals = "Offers & Deals"
        static let notificationTimeExpires = "Notification Time Before Tender Expires"
        static let notificationTimeBeforeAuction = "Notification Time Before Auction"
        static let newTender = "New Tender"
        static let bidsAccepted = "Bids Accepted"
        static let bidsRejected = "Bids Rejected"
        static let jobCompletion = "Job Completion"
        static let paymentCompletion = "Payment Completion"
        static let times = "Time"


        //MARK: - Image Picker View Controller
        static let selectPhotos = "Select Photos From"
        static let selectPhoto = "Select Photo From"
        
        //MARK: - RegisterPayment View Controller
        static let registerationPayment = "Registration Payment"
        static let payableAmount = "Total Payable Amount"
        static let skip  = "Skip"
        static let payNow = "Pay Now"
        static let paymentWarning = "Accountability: Misuse of the website or Application will lead to termination of the account and legal action will be applicable.Any dispute or claim arising out of or in connection with this website shall be governed and construed in accordance with the laws of UAE."
        
        //MARK: - DocumentListViewC
        static let documents = "Documents"
        static let uploadNewFile = "Upload New File"
        static let maxFileSize = "Max file size 5 MB"
                
        //MARK: - Choose Service

        //MARK: - Upload Document
        static let uploadDocument = "Upload Document"
        static let proceed = "Proceed"
        static let registrationCard = "Emirates ID"
        static let ownerShipCertificate = "Registration Card /\nOwnership Certificate"
        
        //MARK: -Add New Card
        static let saveCard = "Save Card"
        
        //MARK: - Upload car image
        static let submit = "Submit"
        
        //MARK: - Sell a Car
        static let sellaCar = "Sell a Car"
        static let sellYourCar = "Sell Your Car"
        static let expectedPrice = "Expected Price"
        static let odometer = "Odometer"
        static let selectBrand = "Select Brand"
        static let modelName = "Model Name"
        static let modelYear = "Model Year"
        static let next = "Next"
        static let continueButton = "Continue"
        
        //MARK: - AuctionFilter
        static let chooseBrands = "Choose Brands"
        static let selectMinimumYear = "Select Minimum Year"
        static let specs = "Specs"
        static let buyNow = "Buy Now"
        
        //MARK: - Recovery
        static let tenderInProgress = "Tender In - Progress"
        static let tenderBidAccepted = "Bid Accepted"
        static let tenderNoBidsAccepted = "No Bids Accepted"
        static let tenderAllBidsRejected = "All Bids Rejected"
        static let tenderUpcoming = "Upcoming Tenders"
        static let tenderComplete = "Tender Complete"
        static let toAcceptBid = "To Accept a Bid"
        static let timeElapsed = "Time Elapsed for Accepting Bids"
        static let tenderStartsIn = "Tender Starts in"
        static let tenderDateTime =  "Tender Date & Time :"
        static let typeOfRecovery = "Type of Recovery"
        static let tenderDuration = "Tender Duration"
        static let truckNumber = "Truck Number: "

        //MARK: - Vehicle Inspection
        static let inspectionDate = "Date of Inspection : "
        static let inspectionTime = "Time of Inspection : "
        static let bidInspectionDate = "Inspection Date : "
        static let bidInspectionTime = "Inspection Time : "
        static let pickUpService = "Providing Pickup & Drop Off : "
        static let serviceType = "Service Type : "
        static let typeOfService = "Type Of Service: "
        static let bidPrice = "Price : "
        static let paymentType = "Payment Type : "
        static let transactionNo = "Transaction No. : "
        
        //MARK: - Servicing
        static let serviceTypeText = "Service: "
        static let partTypeText = "Part Type: "
        static let partNumberText = "Part Number: "
        static let wheelSizeText = "Wheel Size: "
        
        //MARK: - Tenders
        static let createTender = "Create New Tender"
        
        //MARK: - SparePart Tenders
        static let partNumber = "Part Number (Optional)"
        static let partName = "Part Name"
        static let partDescriptiom = "Part Description"
        static let partNameText = "Part Name: "
        static let timeNeededText = "Time Needed For Job : "
        
        //MARK: - Car Detailing
        static let uploadCarImages = "Upload Images of Specific Jobs Required (Optional)".localizedString()
        static let cdServiceTypeText = "Service Type: "
        
        //MARK: - ChooseCar
        static let noCarsAddedAlert = "There are no cars added in your cars list. Add a new car to continue."
        
        //MARK: - Insurance
        static let insuranceTypeText = "Insurance Type : "
        static let vehicleValueText = "Vehicle Value : "
        static let purchasedSuccessfully = "Purchased successfully"
        static let insuranceName = "Insurer Name : "

    }
    
    struct Message {
        static let areYouSureYouWantToSignOut = "Are you sure you want to Sign Out?"
        static let areYouSureYouWantToDelete = "Are you sure you want to delete?"
        static let cancelPostRequest = "Are you sure you want to Cancel Post Request"
        static let albaDisclaimer = "Alba is not responsible for any discrepancy behavior of suppliers."

    }
    
    struct DropDownItem {
        static let clientServiceLocation = ["Client Location": "client", "Agent Location": "agent", "Both": "both"]
        static let businessServiceLocation = ["Business Location": "business", "Client Location": "client", "Both": "both"]
        static let priceType = ["Fixed":"fixed", "Hourly":"hourly"]
    }
    
    struct Popups {
        static let servicesName = "What is the name of this service?  For example, gel overlay, hair cut, hot stone massage, etc."
        static let serviceLocations = "Can this particular service be carried out at the client’s home or at your place of work, or both?"
        static let travellingTime = "If you offer services to clients at their home, how much time do you require to travel."
        static let locationRadius = "If you offer services to clients at their home, how far are you willing to travel to a client appointment?"
    }
}
