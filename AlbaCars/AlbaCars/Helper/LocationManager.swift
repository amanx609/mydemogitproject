//
//  AVLocationManager.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import CoreLocation

/**
 This class retuens the current Location and the adrress space using Location Manager
 and it's delegate.
 **/
class LocationManager: NSObject
{
    // MARK: - Singleton Instantiation
    
    private static let _sharedInstance: LocationManager = LocationManager()
    static var sharedInstance: LocationManager {
        return ._sharedInstance
    }
    
    // MARK: - Fileprivate Properties
    
    var locationManager: CLLocationManager!
    
    // MARK: - Public Properties
    
    public var currentLocationProvider: ((CLLocation)->())? = nil
    
    // MARK: - Initializers
    
    private override init() {
        // This will resctrict the instantiation of this class.
    }
    
    // MARK: - Public Methods
    
    func determineCurrentLocation(withAccuracy: CLLocationAccuracy =  kCLLocationAccuracyBest)
    {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
}

// MARK: - Location Manager Delegates

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let userLocation:CLLocation = locations[0] as CLLocation
        manager.stopUpdatingLocation()
        self.currentLocationProvider?(userLocation)
        manager.delegate = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("LocationManager Error: ********* \(error)")
    }
}

