//
//  Constant.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit

struct Constants {
  
  struct Devices {
    static let ScreenWidth = UIScreen.main.bounds.width
    static let ScreenHeight = UIScreen.main.bounds.height
    static let ScreenMaxLength = (max(ScreenWidth, ScreenHeight))
    static let ScreenMinLength = (min(ScreenWidth, ScreenHeight))
    // static let StatusBarHeight = UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
    static let NavigationBarHeight = CGFloat(44.0)
    //   static let NavigationBarHeightWithStatusBar = NavigationBarHeight+StatusBarHeight
    
    static let IsiPhone = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone
    static let IsiPhone4OrLess = (IsiPhone && ScreenMaxLength < 568.0)
    static let IsiPhone5 = (IsiPhone && ScreenMaxLength == 568.0)
    static let IsiPhone6 = (IsiPhone && ScreenMaxLength == 667.0)
    static let IsiPhone6P = (IsiPhone && ScreenMaxLength == 736.0)
    static let IsiPhoneXOr11Pro = (IsiPhone && ScreenMaxLength == 812.0)
    static let IsiPhone11 = (IsiPhone && ScreenMaxLength == 896.0)
    
    static let deviceType = 2 //ForiOS
    static var statusBarOrientation: UIInterfaceOrientation? {
      get {
        if #available(iOS 13.0, *) {
          guard let orientation = UIApplication.shared.windows.first?.windowScene?.interfaceOrientation else {
            #if DEBUG
            fatalError("Could not obtain UIInterfaceOrientation from a valid windowScene")
            #else
            return nil
            #endif
          }
           return orientation
        } else {
          // Fallback on earlier versions
          let orientation = UIApplication.shared.statusBarOrientation
          return orientation
        }
      }
    }
    
    @available(iOS 13.0, *)
    static let keyWindow = UIApplication.shared.connectedScenes
      .filter({$0.activationState == .foregroundActive})
      .map({$0 as? UIWindowScene})
      .compactMap({$0})
      .first?.windows
      .filter({$0.isKeyWindow}).first
  }
  
  struct Platform {
    static let isSimulator: Bool = {
      var isSim = false
      #if arch(i386) || arch(x86_64)
      isSim = true
      #endif
      return isSim
    }()
  }
  
  struct UIConstants {
    static let sizeRadius_3half: CGFloat = 3.5
    static let sizeRadius_2: CGFloat = 2.0
    static let sizeRadius_4: CGFloat = 4.0
    static let sizeRadius_5: CGFloat = 5.0
    static let sizeRadius_7: CGFloat = 7.0
    static let sizeRadius_10: CGFloat = 10.0
    static let sizeRadius_12: CGFloat = 12.0
    static let sizeRadius_13: CGFloat = 13.0
    static let sizeRadius_14: CGFloat = 14.0
    static let sizeRadius_16: CGFloat = 16.0
    static let sizeRadius_18: CGFloat = 18.0
    static let sizeRadius_20: CGFloat = 20.0
    static let sizeRadius_40: CGFloat = 40.0
    static let sizeRadius_48: CGFloat = 48.0
    static let shadowOpacity: Float = 0.08
    static let shadowOpacity_15: Float = 0.15
    static let borderWidth_1: CGFloat = 1.0
    static let borderWidth_07: CGFloat = 0.7
    static let borderWidth_17: CGFloat = 1.7
    static let sizeRadius_6: CGFloat = 6.0
    static let sizeHeight_14: CGFloat = 14.0
    static let sizeHeight_23: CGFloat = 24.0
    static let borderWidth_2half: CGFloat = 2.5
    static let sizeRadius_30: CGFloat = 30.0


  }
  
  struct CellHeightConstants {
    static let height_0: CGFloat = 0.0
    static let height_80: CGFloat = 80.0
    static let height_100: CGFloat = 100.0
    static let height_50: CGFloat = 50.0
    static let height_156: CGFloat = 156.0
    static let height_150: CGFloat = 150.0
    static let height_40: CGFloat = 40.0
    static let height_30: CGFloat = 30.0
    static let height_120: CGFloat = 120.0
    static let height_110: CGFloat = 110.0
    static let height_130: CGFloat = 130.0
    static let height_60: CGFloat = 60.0
    static let height_170: CGFloat = 170.0
    static let height_116: CGFloat = 116.0
    static let height_25: CGFloat = 25.0
    static let height_35: CGFloat = 35.0
    static let height_45: CGFloat = 45.0
    static let height_135: CGFloat = 135.0
    static let height_84: CGFloat = 84.0
    static let height_56: CGFloat = 56.0
    static let height_168: CGFloat = 168.0
    static let height_97: CGFloat = 97.0
    static let height_65: CGFloat = 65.0
    static let height_118: CGFloat = 118.0
    static let height_53: CGFloat = 53.0
    static let height_220: CGFloat = 220.0
    static let height_106: CGFloat = 106.0
    static let height_310: CGFloat = 310.0
    static let height_125: CGFloat = 125.0
    static let height_20: CGFloat = 20.0
    static let height_15: CGFloat = 15.0
    static let height_70: CGFloat = 70.0
    static let height_185: CGFloat = 185.0
    static let height_95: CGFloat = 95.0
    static let height_75: CGFloat = 75.0
    static let height_137: CGFloat = 137.0
    static let height_115: CGFloat = 115.0
    static let height_175: CGFloat = 175.0
    static let height_450: CGFloat = 450.0
    static let height_140: CGFloat = 140.0
    static let height_160: CGFloat = 160.0
    static let height_180: CGFloat = 180.0
    static let height_230: CGFloat = 230.0
    static let height_260: CGFloat = 260.0
    static let height_270: CGFloat = 270.0
    static let height_240: CGFloat = 240.0
     static let height_250: CGFloat = 250.0
    static let height_200: CGFloat = 200.0
    static let height_165: CGFloat = 165.0
    static let height_55: CGFloat = 55.0
    static let height_210: CGFloat = 210.0
    static let height_265: CGFloat = 265.0
    static let height_121: CGFloat = 121.0
    static let height_320: CGFloat = 320.0
    static let height_198: CGFloat = 198.0
    static let height_90: CGFloat = 90.0
    static let height_360: CGFloat = 360.0
    static let height_245: CGFloat = 245.0
    static let height_190: CGFloat = 190.0
    static let height_128: CGFloat = 128.0
    static let height_192: CGFloat = 192.0
  }
    
 struct CollectionCellFrameConstants {
    static let width_76: CGFloat = 76.0
    static let height_45: CGFloat = 45.0
 }
  
  struct UIKeys {
    static let placeholderImage = "placeholderImage"
    static let paintColor = "paintColor"
    static let paintSite = "paintSite"
    static let dents = "dents"
    
    static let backgroundColor = "backgroundColor"
    static let textColor = "textColor"
    static let plainText = "plainText"
    static let tappableText = "tappableText"
    static let placeholderText = "placeholderText"
    static let buttonType = "buttonType"
    static let switchStatus = "switchStatus"
    static let selectedImage = "selectedImage"
    static let unselectedImage = "unselectedImage"
    static let isFree = "isFree"
    static let isAvailable = "isAvailable"
    static let servicePrice = "servicePrice"
    static let inputType = "inputType"
    static let isSecuredText = "isSecuredText"
    static let videoId = "videoId"
    static let chooseServiceType = "chooseServiceType"
    static let isSelected = "isSelected"
    static let deliveryLongitude = "deliveryLongitude"
    static let deliveryLatitude = "deliveryLatitude"
    static let id = "id"
    static let optionOneSelected = "optionOneSelected"
    static let optionTwoSelected = "optionTwoSelected"
    static let optionThreeSelected = "optionThreeSelected"
    static let answerPlaceholder = "answerPlaceholder"
    static let answerValue = "answerValue"
    static let titlePlaceholder = "titlePlaceholder"
    static let title = "title"
    static let subTitlePlaceholder = "subTitlePlaceholder"
    static let firstValue = "firstValue"
    static let secondValue = "secondValue"
    static let isHide = "isHide"
    static let imageStatus = "imageStatus"
    static let imageUrl = "imageUrl"
    static let image = "image"
    static let duration = "duration"
    static let status = "Status"
    static let mileageFrom = "mileageFrom"
    static let mileageTo = "mileageTo"
    static let itemsCovered = "ItemsCovered"    
    static let labelAlignment = "labelAlignment"
    static let packageType = "packageType"
    static let emiratesID = "emiratesID"
    static let ownershipCertificate = "ownershipCertificate"
    static let cellIcon = "cellIcon"
    static let stepperValue = "stepperValue"
    static let warrantyModel = "warrantyModel"
    static let pendingStatus = "pendingStatus"
    static let contactUs = "contactUs"
    static let carDetail = "carDetail"
    static let sourcePlaceHolder = "sourcePlaceHolder"
    static let destinationPlaceHolder = "destinationPlaceHolder"
    static let sourceText = "sourceText"
    static let destinationText = "destinationText"
    static let shouldHide = "shouldHide"
    static let isExtrasAdded = "isExtrasAdded"
    static let extras = "extras"
    static let isAddAED = "isAddAED"
    static let documentUrl1 = "documentUrl1"
    static let documentUrl2 = "documentUrl2"
    static let shouldShowAnswer = "shouldShowAnswer"
    static let timerInfo1 = "timerInfo1"
    static let timerInfo2 = "timerInfo2"
    static let locationDateInfo1 = "locationDateInfo"
    static let locationDateInfo2 = "locationDateInfo2"
    static let dateTime = "dateTime"
    static let time = "time"
    static let date = "date"
    static let serviceType = "serviceType"
    static let serviceTypeValue = "serviceTypeValue"
    static let subServices =  "subServices"
    static let subServicesString =  "subServicesString"
    static let transaction = "transaction"
    static let paymentType = "paymentType"
    static let pickupService = "pickupService"
    static let boundaryColor = "boundaryColor"
    static let sourceValue = "sourceValue"
    static let destinationValue = "destinationValue"
    static let sourceLatitude = "sourceLatitude"
    static let sourceLongitude = "sourceLongitude"
    static let destinationLatitude = "destinationLatitude"
    static let destinationLongitude = "destinationLongitude"
    static let cellInfo = "cellInfo"
    static let images = "images"
    static let firstImg = "firstImg"
    static let secondImg = "secondImg"
    static let thirdImg = "thirdImg"
    static let fourthImg = "fourthImg"
    static let notifications = "notifications"
    static let value = "value"
    static let recoveryType = "recoveryType"
    static let bidAmount = "bidAmount"
    static let bidCount = "bidCount"
    static let description = "description"
    static let bidComments = "bidComments"
    static let isSecondDetailPage = "isSecondDetailPage"
    static let additionalInformation = "additionalInformation"
    static let agencyName = "agencyName"
    static let rating = "rating"
    static let address = "address"
    static let driverName = "driverName"
    static let driverContact = "driverContact"
    static let driverImage = "driverImage"
    static let engineSize = "engineSize"
    static let frontBrake = "frontBrake"
    static let vinNumber = "vinNumber"
    static let brakeDiscs = "brakeDiscs"
    static let rearBrake = "rearBrake"
    static let wheelSize = "wheelSize"
    static let rims = "rims"
    static let bankName = "bankName"
    static let brands = "brands"
    static let brandArray = "brandArray"
    static let brandId = "brandId"
    static let years = "years"
    static let dropdownType = "dropdownType"
    static let truckNumber = "truckNumber"

    
    //Window Tinting
    static let tintColor = "tintColor"
    static let visibilityPercentage = "visibilityPercentage"
    //CityListArray
    static let cityName = "cityName"
    
    //CylinderListArray
    static let noOfCylinders = "noOfCylinders"
    
    //YearsListArray
    static let year = "year"
    
    //MobileNumberCell
    static let countryCodePlaceholder = "countryCodePlaceholder"
    
    //MileageTextField Cell
    static let minimum = "minimum"
    static let maximum = "maximum"
    static let minimumValue = "minimumValue"
    static let maximumValue = "maximumValue"
    
    //DocumentList Cell
    static let documentImage = "documentImage"
    static let documentName = "documentName"
    static let documentSize = "documentSize"
    
    //Payment Filter Cell
    static let paymentFilterDataSource = "paymentFilterDataSource"
    static let filterName = "filterName"
    static let isFilterSelected = "isFilterSelected"
    static let paymentHistory = "paymentHistory"
    
    //SelectYearTableCell
    static let headerTitle = "headerTitle"
    static let valueText = "valueText"
    
    //LiveAuctionRoom
    static let sectionTitle = "sectionTitle"
    static let isExpanded = "isExpanded"
    static let cells = "cells"
    static let sectionIcon = "sectionIcon"
    static let dlaCellType = "dlaCellType"
    static let carConditionType = "carConditionType"
    
    //SparePartsTenderRequest
    static let firstButtonTitle = "firstButtonTitle"
    static let secondButtonTitle = "secondButtonTitle"
    static let thirdButtonTitle = "thirdButtonTitle"
    static let fourthButtonTitle = "fourthButtonTitle"
    static let selectedButtonIndex = "selectedButtonIndex"
    
    //SparePartsTender
    static let daysForService = "daysForService"
    static let partType = "partType"
    static let partName = "partName"
    static let isBank = "isBank"
    static let isSameAsTender = "isSameAsTender"
    
    //Bank Valuation
    static let carFront = "carFront"
    static let carRear = "carRear"
    static let carLeftSide = "carLeftSide"
    static let carRightSide = "carRightSide"
    static let carMileage = "carMileage"
    static let gccSticker = "gccSticker"
    static let firstImgType = "firstImgType"
    static let secondImgType = "secondImgType"
    static let docFrontImage = "docFrontImage"
    static let docBackImage = "docBackImage"
    static let docCertificateImage = "docCertificateImage"
    static let carPrice = "carPrice"
    
    //Insurance
    static let insuranceType = "insuranceType"
    static let vehicleValue = "vehicleValue"
    static let brandsName = "brandsName"
  
  }
  
  struct Validations {
    static let firstNameMaxLength = 35
    static let passwordMinLength = 6
    static let passwordMaxLength = 18
    static let emailMaxLength = 50
    static let companyMaxLength = 100
    static let projectTitleMaxLength = 50
    static let projectDescriptionMaxLength = 200
    static let usernameMaxLength = 30
    static let usernameMinLength = 3
    static let mobileNumberMinLength = 8
    static let mobileNumberMaxLength = 14
    static let engineSizeMaxLength = 6
    static let minTime = 0
    static let minAge = 13
    static let maxAge = 100
    static let maxBirthday = 0
    static let minBirhday = 100
    static let nameMaxLength = 30
    static let mileageMaxLength = 5
    static let plateNumMaxLength = 20
    static let dropOffMaxLength = 10
    static let priceMaxLength = 8
    static let resendOTPStartTime = 180
    static let minimumMileage = 10
    static let maximumMileage = 200000
    static let perPage = 0
    static let maxCarImagesCount = 10
    static let minCountBeforAPICall = 3
    static let maxEngineSize = 150000
    static let minCarYear = 1980
    static let maxCarYear = Date().getYear()
    static let maxExpectedPrice = 150000
    static let maxaddressLength = 80
    static let yearLenth = 4
    static let minYear = 1990
    static let lenth6 = 6
    static let lenth9 = 9


  }
  
  struct Format {
    static let counter = "%0.2d:%0.2d"
    static let mobileNumberFormate = "XXX-XXX-XXXXXX"
    static let dateFormatMMMdyyyy = "MMM d, yyyy"
    static let creditCardFormate = "XXXX-XXXX-XXXX-XXXX"
    static let creditCardExpiryFormate = "XX/XX"
    static let dateFormatMMMdha = "MMM d, h a"
    static let dateFormatMMMdhha = "MMM d, hh a"
    static let dateFormatMMMd = "MMM d"
    static let dateFormatMMMdAt = "MMM d 'at'"
    static let timeFormathmma = "hh:mm a"
    static let timeFormathhmm = "hh:mm"
    static let dateFormatdMMMMyyyy = "d MMMM yyyy"
    static let dateFormatdMMMyyyy = "d MMM yyyy"
    static let apiDateTimeFormat = "yyyy-MM-dd'T'hh:mm:ss.000Z"
    static let apiSpaceDateTimeFormat = "yyyy-MM-dd HH:mm:ss"
    static let displayDateTimeFormat = "dd MMMM yyyy, hh:mm a"
    static let apiDateFormat = "MM-dd-yyyy"
    static let dateFormatyyyyMMdd = "yyyy-MM-dd"
    static let dateFormatWithoutSpace = "dd-MM-yyyy"
    static let editProfileDateFormat = "dd - MM - yyyy"
    static let timeFormathhmmss = "hh:mm:ss"
    static let timeFormat24hhmmss = "HH:mm:ss"
    static let apiTimeFormat = "HH:mm"
}
  
  struct SocialType {
    static let normal = "normal"
    static let facebook = "facebook"
    static let google = "google"
  }
  
  //CityList
  static let uaeCityList: [[String: AnyObject]]  = [[Constants.UIKeys.cityName: "Abu Dhabi".localizedString() as AnyObject],
                                                    [Constants.UIKeys.cityName: "Ajman".localizedString() as AnyObject],
                                                    [Constants.UIKeys.cityName: "Al Ain".localizedString() as AnyObject],
                                                    [Constants.UIKeys.cityName: "Dubai".localizedString() as AnyObject],
                                                    [Constants.UIKeys.cityName: "Fujairah".localizedString() as AnyObject],
                                                    [Constants.UIKeys.cityName: "Sharjah".localizedString() as AnyObject],
                                                    [Constants.UIKeys.cityName: "Ras Al-Khaima".localizedString() as AnyObject]]
  static let carCylindersList: [[String: AnyObject]]  = [[Constants.UIKeys.noOfCylinders: "N/A".localizedString() as AnyObject],[Constants.UIKeys.noOfCylinders: "1".localizedString() as AnyObject],
  [Constants.UIKeys.noOfCylinders: "2".localizedString() as AnyObject],
  [Constants.UIKeys.noOfCylinders: "3".localizedString() as AnyObject],
  [Constants.UIKeys.noOfCylinders: "4".localizedString() as AnyObject],
    [Constants.UIKeys.noOfCylinders: "5".localizedString() as AnyObject],
    [Constants.UIKeys.noOfCylinders: "6".localizedString() as AnyObject],
    [Constants.UIKeys.noOfCylinders: "7".localizedString() as AnyObject],
    [Constants.UIKeys.noOfCylinders: "8".localizedString() as AnyObject],
    [Constants.UIKeys.noOfCylinders: "9".localizedString() as AnyObject],
    [Constants.UIKeys.noOfCylinders: "10".localizedString() as AnyObject],
    [Constants.UIKeys.noOfCylinders: "11".localizedString() as AnyObject],
    [Constants.UIKeys.noOfCylinders: "12".localizedString() as AnyObject],
    [Constants.UIKeys.noOfCylinders: "13".localizedString() as AnyObject],
    [Constants.UIKeys.noOfCylinders: "14".localizedString() as AnyObject],
    [Constants.UIKeys.noOfCylinders: "15".localizedString() as AnyObject],
    [Constants.UIKeys.noOfCylinders: "16".localizedString() as AnyObject]]
    
    static let timeDurationList: [[String: AnyObject]] = [[Constants.UIKeys.duration: "10 min".localizedString() as AnyObject], [Constants.UIKeys.duration: "20 min".localizedString() as AnyObject], [Constants.UIKeys.duration: "30 min".localizedString() as AnyObject],[Constants.UIKeys.duration: "40 min".localizedString() as AnyObject],[Constants.UIKeys.duration: "50 min".localizedString() as AnyObject]]
    
    static let tenderDuration: [[String: AnyObject]] = [[Constants.UIKeys.duration: "15 min".localizedString() as AnyObject, Constants.UIKeys.value: "00:15:00" as AnyObject, Constants.UIKeys.isSelected: false as AnyObject], [Constants.UIKeys.duration: "30 min".localizedString() as AnyObject, Constants.UIKeys.value: "00:30:00" as AnyObject, Constants.UIKeys.isSelected: false as AnyObject], [Constants.UIKeys.duration: "1 hr".localizedString() as AnyObject, Constants.UIKeys.value: "01:00:00" as AnyObject, Constants.UIKeys.isSelected: false as AnyObject], [Constants.UIKeys.duration: "6 hrs".localizedString() as AnyObject, Constants.UIKeys.value: "06:00:00" as AnyObject, Constants.UIKeys.isSelected: false as AnyObject], [Constants.UIKeys.duration: "1 day".localizedString() as AnyObject, Constants.UIKeys.value: "24:00:00" as AnyObject, Constants.UIKeys.isSelected: false as AnyObject]]
    
    static let numOfRims: [[String: AnyObject]] = [[Constants.UIKeys.rims: "1" as AnyObject],[Constants.UIKeys.rims: "2" as AnyObject],[Constants.UIKeys.rims: "3" as AnyObject],[Constants.UIKeys.rims: "4" as AnyObject],[Constants.UIKeys.rims: "5" as AnyObject],[Constants.UIKeys.rims: "6" as AnyObject]]
    
    
    //InitialValues
    static let initialMinMileage = 1
    static let initialMaxMileage = 200000
    
    static let minCountBeforeAPICall = 3

}

let awsBaseURL =  "https://albacar.s3-eu-west-1.amazonaws.com"

//

let kServiceHistoryAmount = 2500
let kRegistrationAmount =  250000
let isPaymentGateWay:Bool = true



