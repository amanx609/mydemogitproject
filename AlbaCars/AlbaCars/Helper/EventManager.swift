//
//  EventManager.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import AVKit
import EventKit

class EventManager {
  
  class func checkEventExist(projectName: String, completion: @escaping((Bool) -> ())) {
    let eventStore = EKEventStore()
    let oneMonthAgo = Date(timeIntervalSinceNow: -30*24*3600)
    let oneYearAfter = Date(timeIntervalSinceNow: +12*30*24*3600)
    let predicate = eventStore.predicateForEvents(withStart: oneMonthAgo, end: oneYearAfter, calendars: nil)
    let existingEvents = eventStore.events(matching: predicate)
    print(existingEvents)
    let joggleEvents = existingEvents.filter{ $0.title.contains(projectName)}
    if joggleEvents.count > 0 {
      completion(true)
    } else {
      completion(false)
    }
  }
  
  class func fetchEventAndDelete(projectName: String) {
    let eventStore = EKEventStore()
    let oneMonthAgo = Date(timeIntervalSinceNow: -30*24*3600)
    let oneYearAfter = Date(timeIntervalSinceNow: +12*30*24*3600)
    let predicate = eventStore.predicateForEvents(withStart: oneMonthAgo, end: oneYearAfter, calendars: nil)
    let existingEvents = eventStore.events(matching: predicate)
    let eventsToDelete = existingEvents.filter{ $0.title.contains(projectName)}
    if let eventToDelete = eventsToDelete.first {
      do {
        try eventStore.remove(eventToDelete, span: .thisEvent, commit: true)
      } catch let e as NSError {
        print(e.description)
        return
      }
    }
  }
  
  public class func addEventToCalendar(customerName: String, projectName: String, description: String?, followUpDate: Date, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
    
    let eventStore = EKEventStore()
    eventStore.requestAccess(to: .event, completion: { (granted, error) in
      if (granted) && (error == nil) {
        
        var calendar = eventStore.defaultCalendarForNewEvents;
        for ekCalender in eventStore.calendars(for: .event) {
          
          if ekCalender.title.contains("gmail.com") ==  true,
            let cal = eventStore.calendar(withIdentifier: ekCalender.calendarIdentifier) {
            
            calendar = cal
            print(ekCalender.calendarIdentifier)
            print(ekCalender.title)
            break;
          }
        }
        //DeleteEventIfAlreadyExist
        self.fetchEventAndDelete(projectName: projectName)
        
        let event = EKEvent(eventStore: eventStore)
        event.title = customerName + " " + projectName + " " + "follow-up"
        event.startDate = followUpDate
        // event.endDate = endDate as Date
        event.notes = description
        event.endDate = followUpDate.addingTimeInterval(60*60)
        event.calendar = calendar
        event.addAlarm(EKAlarm(relativeOffset: -60))
        event.addAlarm(EKAlarm(relativeOffset: -60*5))
        do
        {
          try eventStore.save(event, span: .thisEvent)
        } catch let e as NSError {
          print(e.description)
          completion?(false, e)
          return
        }
        completion?(true, nil)
      } else {
        completion?(false, error as NSError?)
      }
    })
  }
  
  class func fetchEventList(completion: @escaping(([EKEvent]) -> ())) {
    let eventStore = EKEventStore()
//    let oneMonthAgo = Date(timeIntervalSinceNow: -1*24*3600)
    let nowTime = Date()
    let threeMonthsAfter = Date(timeIntervalSinceNow: +3*30*24*3600)
    let predicate = eventStore.predicateForEvents(withStart: nowTime, end: threeMonthsAfter, calendars: nil)
    let existingEvents = eventStore.events(matching: predicate)
    completion(existingEvents)
  }
}
