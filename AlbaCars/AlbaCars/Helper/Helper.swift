//
//  Helper.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import UIKit
import AVKit
import EventKit
import Contacts
import PDFKit


class Helper {
    
    // MARK: Layout direction
    class var layoutDirectionRTL: Bool {
        // arabic layout
        return UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft
    }
    
    class func getTargetViewC(_ anyObject: UIResponder) -> UIViewController? {
        if let viewC = anyObject.owningViewController() {
            if viewC.navigationController != nil {
                return viewC
            } else if let finalVC = viewC.owningViewController(), finalVC.navigationController != nil {
                return finalVC
            }
        }
        return nil
    }
    
    // MARK: - Get root view controller
    class func rootViewController() -> UIViewController {
        return AppDelegate.delegate.window!.rootViewController!
    }
    
    // MARK: - Get topmost view controller
    class func topMostViewController(rootViewController: UIViewController) -> UIViewController? {
        if let navigationController = rootViewController as? UINavigationController {
            return topMostViewController(rootViewController: navigationController.visibleViewController!)
        }
        if let tabBarController = rootViewController as? UITabBarController {
            if let selectedTabBarController = tabBarController.selectedViewController {
                return topMostViewController(rootViewController: selectedTabBarController)
            }
        }
        if let presentedViewController = rootViewController.presentedViewController {
            return topMostViewController(rootViewController: presentedViewController)
        }
        return rootViewController
    }
    
    class func visibleController() -> UIViewController {
        if var topController = AppDelegate.delegate.window?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return UIViewController()
    }
    
    class func getIndexPathFor(view: UIView, tableView: UITableView) -> IndexPath? {
        let point = tableView.convert(view.bounds.origin, from: view)
        let indexPath = tableView.indexPathForRow(at: point)
        return indexPath
    }
    
    class func getIndexPathFor(view: UIView, collectionView: UICollectionView) -> IndexPath? {
        let point = collectionView.convert(view.bounds.origin, from: view)
        let indexPath = collectionView.indexPathForItem(at: point)
        return indexPath
    }
    
    class func getDatestring(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yy"
        let date = dateFormatter.string(from: date)
        return date
    }
    
    class func isValidEmail(emailString:String, strictFilter:Bool) -> Bool {
        let strictEmailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let laxString = ".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*"
        let emailRegex = strictFilter ? strictEmailRegEx : laxString
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: emailString)
    }
    
    class func checkValidFileSize(path: String, maxSize:Float ) -> Bool {
       
        var fileSize: Int64 = 0
        
            do {
                let fileAttributes = try FileManager.default.attributesOfItem(atPath: path)
                fileSize += fileAttributes[FileAttributeKey.size] as? Int64 ?? 0
                let size = Float(fileSize) / (1024.0 * 1024.0)
                if size > maxSize {
                    return false
                } else {
                    return true
                }
            } catch _ {
                
            }
        
        return false
    }
    
    class public func toJsonString(from object: Any) -> String? {
        if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString
        }
        return nil
    }
    
    /*
     class func getFileSize(url: URL) -> Double {
     do {
     let attribute = try FileManager.default.attributesOfItem(atPath: url.path)
     if let size = attribute[FileAttributeKey.size] as? NSNumber {
     return size.doubleValue / (Constant.byte1024 * Constant.byte1024)
     }
     } catch {
     print("Error: \(error)")
     }
     return 0.0
     }*/
    
    class func serviceHistoryCreateDirectory() {
       
        if let filePath = self.getServiceHistoryPath() {
            let fileManager = FileManager.default
            if !fileManager.fileExists(atPath: filePath.path) {
                do {
                    try fileManager.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    NSLog("Couldn't create document directory")
                }
            }
        }
        
    }
    
    class func getServiceHistoryPath() -> URL? {
        
        let fileManager = FileManager.default
        if let docDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
            let filePath =  docDirectory.appendingPathComponent("serviceHistory")
            return filePath
        } else {
            return nil
        }
    }
    
    class func isFileExistsInDirectory(fileName: String) -> Bool {
        if let resourceDocPath = Helper.getServiceHistoryPath() {
            let filePath =  resourceDocPath.appendingPathComponent(fileName)
            return FileManager.default.fileExists(atPath: filePath.path)
        } else {
            return false
        }
    }
    
    class func urlInDocumentsDirectory(fileName: String) -> URL? {
        
        if let resourceDocPath = self.getServiceHistoryPath() {
            return resourceDocPath.appendingPathComponent(fileName)
        } else {
            return nil
        }
    }
        
    class func getDeviceLanguage() -> String
    {
        var strDeviceCurrentLanguage:String
        
        let languageObj = NSLocale.preferredLanguages.first
        //let languageDic = NSLocale.components(fromLocaleIdentifier: languageObj)
        let languageDic = NSLocale.components(fromLocaleIdentifier: languageObj!)
        let language = languageDic["kCFLocaleLanguageCodeKey"]
        
        if (language == "ar") {
            strDeviceCurrentLanguage = "arabic"
        } else {
            strDeviceCurrentLanguage = ""
        }
        return strDeviceCurrentLanguage
    }
    
    public class func getSingleButtonToolBar(buttonTitle: String, selector: Selector, target: Any?) -> UIToolbar
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: Constants.Devices.ScreenWidth, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let button: UIBarButtonItem  = UIBarButtonItem(title: buttonTitle, style: UIBarButtonItem.Style.done, target: target, action: selector)
        button.tintColor = UIColor.redButtonColor
        var items = [UIBarButtonItem]()
        items = [flexSpace, button]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        return doneToolbar
    }
    
    public class func getDoubleButtonToolBar(leftButtonTitle: String, rightButtonTitle: String, leftButtonselector: Selector, rightButtonSelector: Selector, target: Any?) -> UIToolbar
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: Constants.Devices.ScreenWidth, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let leftButton: UIBarButtonItem  = UIBarButtonItem(title: leftButtonTitle, style: UIBarButtonItem.Style.done, target: target, action: leftButtonselector)
        leftButton.tintColor = UIColor.redButtonColor
        let rightButton: UIBarButtonItem  = UIBarButtonItem(title: rightButtonTitle, style: UIBarButtonItem.Style.done, target: target, action: rightButtonSelector)
        rightButton.tintColor = UIColor.redButtonColor
        var items = [UIBarButtonItem]()
        items = [leftButton, flexSpace, rightButton]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        return doneToolbar
    }
    
    public class func getCurrentDate(inFormat: String) -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = inFormat
        return formatter.string(from: date)
    }
    
    public class func getCurrentTime(inFormat: String) -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = inFormat
        return formatter.string(from: date)
    }
    
    
    // Open Dial pad with number for calling
    class func openDialPad(phone: String) {
        if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    // Open Native Email App
    class func openEmail(email: String) {
        if let url = URL(string: "mailto:\(email)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    class func compareTime(currentTime: String,selectedTime: String) -> Bool {
        
        if let currentDate = currentTime.getDateInstaceFrom(format: Constants.Format.timeFormat24hhmmss),
            let endDate = selectedTime.getDateInstaceFrom(format: Constants.Format.timeFormat24hhmmss) {
            if currentDate > endDate {
                return true
            }
        }
        
        return false
    }
    
    
    //Open URL
    class func openUrl(url: URL) {
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //ConvertDataToJsonString
    class func convertToJsonString(inputData: Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: inputData, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    //GeneratePDFFromImage
    class func convertToPDF(image: UIImage) -> Data? {
        let pdfDocument = PDFDocument()
        if let pdfPage = PDFPage(image: image) {
            pdfDocument.insert(pdfPage, at: 0)
            let data = pdfDocument.dataRepresentation()
            return data
        }
        return nil
    }
    
    class func convertImagesToPDF(images: [UIImage]) -> Data? {
        let pdfDocument = PDFDocument()
        var index = 0
        for image in images {
            if let pdfPage = PDFPage(image: image) {
                pdfDocument.insert(pdfPage, at: index)
                index += 1
            }
        }
        let data = pdfDocument.dataRepresentation()
        return data
    }
    
    class func currentTimeStamp() -> Int64 {
        return Int64(Date().timeIntervalSince1970)
    }
    
    class func getListOfYearsFrom(_ year: Int) -> [[String: AnyObject]] {
        var startYear = year
        var yearsList: [[String: AnyObject]] = []
        let currentYear = Calendar.current.component(.year, from: Date()) + 1
        while startYear <= currentYear {
            var yearDict = [String: AnyObject]()
            yearDict[Constants.UIKeys.year] = "\(startYear)" as AnyObject
            yearDict[Constants.UIKeys.isSelected] = false as AnyObject
            yearsList.append(yearDict)
            startYear += 1
        }
        return yearsList
    }
    
    class func getCurrentYear() -> Int {
        return Calendar.current.component(.year, from: Date())
    }
    
    class func getVatPercentage() -> String {
        
        if let vat = UserDefaultsManager.sharedInstance.getDoubleValueFor(key:.vatPercentage) {
            return "+" + Helper.toString(object: vat) + "% VAT"
        }

        return ""
    }
}

//Kumulos Methods
extension Helper {
    class func toString(object:Any?) -> String {
        if let str = object as? String {
            return String(format: "%@", str)
        }
        if let num = object as? NSNumber {
            return String(format: "%@", num)
        }
        return ""
    }
    
    class func getDefaultString(object:Any?,defaultString: String) -> String {
        if let str = object as? String, !str.isEmpty {
            return String(format: "%@", str)
        }
        if let num = object as? NSNumber {
            return String(format: "%@", num)
        }
        return defaultString
    }
    
    class func toInt(_ object:Any?) -> Int {
        if let obj = object as? NSObject {
            let string = String(format: "%@", obj)
            return Int(string) ?? 0
        }
        return 0
    }
    
    class func toBool(_ object:Any?) -> Bool {
        
        if let obj = object as? Bool {
            return obj
        }
        
        if let obj = object as? NSObject {
            let string = String(format: "%@", obj)
            return Bool(string) ?? false
        }
        return false
    }
    
    class func toDouble(_ object:Any?) -> Double {
       
        if let obj = object as? NSObject {
            let string = String(format: "%@", obj)
            return Double(string) ?? 0.0
        }
        return 0.0
    }
    
    class func getInt16Value(_ obj: AnyObject?) -> Int16 {
        if let object = obj as? String, let intValue = Int16(object) {
            return intValue
        } else if let intValue = obj as? Int16 {
            return intValue
        } else {
            return 0
        }
    }
    
    class func getInt64Value(_ obj: AnyObject?) -> Int64 {
        if let object = obj as? String, let intValue = Int64(object) {
            return intValue
        } else if let intValue = obj as? Int64 {
            return intValue
        } else {
            return 0
        }
    }
    
    class func getStringFrom(_ date: Date, format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        return formatter.string(from: date)
    }
    
    class func parse(from string: String) -> Int? {
        return Int(string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())
    }
    
    class func getExtraPrice(extraServices:[CellInfo]) -> Int {
        var extarPrice = 0
        for extraService in extraServices {
            
            if let info = extraService.info, let isExtrasAdded = info[Constants.UIKeys.isExtrasAdded] as? Bool, isExtrasAdded == true {
                extarPrice = extarPrice + Helper.toInt(info[Constants.UIKeys.servicePrice])
            }
            
        }
        return extarPrice
    }
    
    class func calculateVat(vatPercentage:Double?,bidAmount:Double?) -> Double {
        let vatPercentage = Helper.toDouble(vatPercentage)
        let bidsAmount = Helper.toDouble(bidAmount)
        
        let val = bidsAmount * vatPercentage
        let strVat = String(format: "%.2f", val / 100.0)

        return Helper.toDouble(strVat)
    }
    
    class func getLeadsStatus(statusCode:String) -> String {
        
        switch statusCode {
        case "1":
            return "Not Contacted".localizedString()
        case "2":
            return "Contacted".localizedString()
        case "3":
            return "Deal Closed".localizedString()
        case "4":
            return "No Deal".localizedString()
        default:
            print("")
        }
        return ""
    }
    
    class func getTenderBidStatus(statusCode:Int) -> (String, UIColor) {
        switch statusCode {
        case TenderBidStatus.accepted.rawValue:
            return (TenderBidStatus.accepted.title.localizedString(),TenderBidStatus.accepted.backgroundColor)
        case TenderBidStatus.submitted.rawValue:
            return (TenderBidStatus.submitted.title.localizedString(),TenderBidStatus.submitted.backgroundColor)
        case TenderBidStatus.rejected.rawValue:
            return (TenderBidStatus.rejected.title.localizedString(),TenderBidStatus.rejected.backgroundColor)
        case TenderBidStatus.completed.rawValue:
            return (TenderBidStatus.completed.title.localizedString(),TenderBidStatus.completed.backgroundColor)
        default:
            print("")
        }
        return ("",.white)
    }
    
    class func getPartType(statusCode:Int) -> String {
        switch statusCode {
        case 1:
            return "Original New".localizedString()
        case 2:
            return "Commercial New".localizedString()
        case 3:
            return "Original Used".localizedString()
        case 4:
            return "Commercial Used".localizedString()
        default:
            print("")
        }
        return ""
    }
    
    class func getWheelType(statusCode:Int) -> String {
        switch statusCode {
        case 0:
            return "Tires".localizedString()
        case 1:
            return "Rim".localizedString()
        default:
            print("")
        }
        return ""
    }
    
    class func getRimType(statusCode:Int) -> String {
        switch statusCode {
        case 1:
            return "Rim Restoration".localizedString()
        case 2:
            return "Rim Replacement".localizedString()
        default:
            print("")
        }
        return ""
    }
    
    class func notificationOption(option:Int) -> String {
        switch option {
        case 5:
            return "GCC".localizedString()
        case 2:
            return "American".localizedString()
        case 3:
            return "Japanese".localizedString()
        case 4:
            return "European".localizedString()
        case 1:
            return "Any".localizedString()
        case 6:
            return "N/A".localizedString()
        default:
            print("")
        }
        return ""
    }
    
    class func getBodyWorkType(statusCode:Int) -> String {
        switch statusCode {
        case BodyWorkType.panelPainting.rawValue:
            return "Panel Painting".localizedString()
        case BodyWorkType.paintlessDentRemoval.rawValue:
            return "Paintless Dent Removal".localizedString()
        case BodyWorkType.ceramicPaintProtection.rawValue:
            return "Ceramic Paint Protection".localizedString()
        default:
            print("")
        }
        return ""
    }
    
    class func getCarDetailingServiceType(statusCode:Int) -> String {
        
        switch statusCode {
        case CarDetailingServiceType.quickWashVaccum.rawValue:
            return CarDetailingServiceType.quickWashVaccum.title
        case CarDetailingServiceType.detailing.rawValue:
            return CarDetailingServiceType.detailing.title
        case CarDetailingServiceType.bodyPolish.rawValue:
            return CarDetailingServiceType.bodyPolish.title
        default:
            print("")
        }
        return ""
    }
    
    class func getDetailingService(statusCode:Int) -> String {
        
        switch statusCode {
        case CarDetailingService.exterior.rawValue:
            return CarDetailingService.exterior.title
        case CarDetailingService.interior.rawValue:
            return CarDetailingService.interior.title
        case CarDetailingService.both.rawValue:
            return CarDetailingService.both.title
        default:
            print("")
        }
        return ""
    }
    
    class func getInsuranceType(statusCode:Int) -> String {
        
        switch statusCode {
        case InsuranceType.agency.rawValue:
            return InsuranceType.agency.title
        case InsuranceType.nonAgency.rawValue:
            return InsuranceType.nonAgency.title
        case InsuranceType.thirdParty.rawValue:
            return InsuranceType.thirdParty.title
        default:
            print("")
        }
        return ""
    }
    
    class func getPaitSite(paintSiteArray:[Int]?) -> String {
        
        var paitSite = ""
        
        if let array = paintSiteArray,array.count > 0 {
            
            if let body = array[0] as? Int , body == 1 {
                paitSite = "Body".localizedString()
            }
        }
        
        if let array = paintSiteArray,array.count > 1 {
            
            if let interior = array[1] as? Int, interior == 1 {
                paitSite = paitSite == "" ? "Interior".localizedString() : paitSite + ", " + "Interior".localizedString()
            }
        }
        
        if let array = paintSiteArray,array.count > 2 {
            
            if let glass = array[2] as? Int, glass == 1 {
                paitSite = paitSite == "" ? "Glass".localizedString() : paitSite + ", " + "Glass".localizedString()
            }
        }
        
        if let array = paintSiteArray,array.count > 3 {
            
            if let rims = array[3] as? Int, rims == 1 {
                paitSite = paitSite == "" ? "Rims".localizedString() : paitSite + ", " + "Rims".localizedString()
            }
        }
        
        return paitSite
    }
    
    class func compareJobAndTenderDateTime(tenderDateTime:String , jobDateTime: String) -> Bool {
        if let tenderDate = tenderDateTime.toDateInstaceFromStandardFormat(), let jobDate = jobDateTime.toDateInstaceFromStandardFormat() {
            if tenderDate == jobDate  || tenderDate > jobDate {
                Alert.showOkAlert(title: StringConstants.Text.AppName, message: "Job date and time can't be less then Tender date and time.".localizedString())
                return false
            }
        }
        return true
    }
    
    class func checkFileSize(path: String, maxSize:Float ) -> Bool {
       
        var fileSize: Int64 = 0
        
            do {
                let fileAttributes = try FileManager.default.attributesOfItem(atPath: path)
                fileSize += fileAttributes[FileAttributeKey.size] as? Int64 ?? 0
                let size = Float(fileSize) / (1024.0 * 1024.0)
                if size > maxSize {
                    return false
                } else {
                    return true
                }
            } catch _ {
                
            }
        
        return false
    }
    
}

class Downloader {
    
       class func load(url: URL, to localUrl: URL, completion: @escaping () -> ()) {
           
          Loader.showLoader()
        
           let sessionConfig = URLSessionConfiguration.default
           let session = URLSession(configuration: sessionConfig)
           let request = try! URLRequest(url: url, method: .get)

           let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
               Loader.hideLoader()

             if let tempLocalUrl = tempLocalUrl, error == nil {
                   // Success

                   if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                       print("Success: \(statusCode)")
                   }

                   do {
                       try FileManager.default.copyItem(at: tempLocalUrl, to: localUrl)
                       completion()
                   } catch (let writeError) {
                       print("error writing file \(localUrl) : \(writeError)")
                    completion()

                   }

               } else {
                completion()
                   print("Failure: %@", error?.localizedDescription);
               }
           }
           task.resume()
       }
   }
