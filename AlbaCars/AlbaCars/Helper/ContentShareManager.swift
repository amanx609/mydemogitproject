//
//  ContentShareManager.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

class ContentShareManager {
  //SharedInstance
  static let shared = ContentShareManager()
  
  func shareContentWith(text: String, image: UIImage?) {
    
    let activityViewController : UIActivityViewController = UIActivityViewController(
      activityItems: [text, image as Any], applicationActivities: nil)
    
//    // This lines is for the popover you need to show in iPad
//    activityViewController.popoverPresentationController?.sourceView =
//
//    // This line remove the arrow of the popover to show in iPad
//    activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.allZeros
//    activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
    
    // Anything you want to exclude
    activityViewController.excludedActivityTypes = [
      UIActivity.ActivityType.postToWeibo,
      UIActivity.ActivityType.print,
      UIActivity.ActivityType.assignToContact,
      UIActivity.ActivityType.saveToCameraRoll,
      UIActivity.ActivityType.addToReadingList,
      UIActivity.ActivityType.postToFlickr,
      UIActivity.ActivityType.postToVimeo,
      UIActivity.ActivityType.postToTencentWeibo
    ]
    
    if let topViewController: UIViewController = Helper.topMostViewController(rootViewController: Helper.rootViewController()) {
      Threads.performTaskInMainQueue {
        topViewController.present(activityViewController, animated: true, completion: nil)
      }
    } else if let viewC = AppDelegate.delegate.window?.rootViewController {
      Threads.performTaskInMainQueue {
        viewC.present(activityViewController, animated: true, completion: nil)
      }
    }
  }
}
