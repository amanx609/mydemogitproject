//
//  TimerManager.swift
//  AlbaCars
//
//  Created by Sakshi Singh on 12/17/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

class TimerManager {
    
    // MARK: - Singleton Instantiation
    private static let _sharedInstance: TimerManager = TimerManager()
    static var sharedInstance: TimerManager {
        return ._sharedInstance
    }
    
    // MARK: - Variables
    var timer: Timer?
    var counter = 0
    var timeLabelText: String?
    var currentTimeLabel: String?
    
    //MARK: - Public Methods
    func initializeTimer(counter: Int) {
        self.counter = counter
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: counter, repeats: true)
    }

    func pauseTimer() {
       
        self.timer?.invalidate()
    }
    
    func stopTimer() {
        
        self.timer?.invalidate()
    }
    

    @objc func updateTimer(_ timer: Timer) {
            if counter > 0 {
                counter = counter - 1
                let seconds = counter % 60
                let minutes = counter / 60
                
                timeLabelText = String(format: "%0.2d:%0.2d", minutes, seconds)
                currentTimeLabel = timeLabelText ?? ""
                NotificationCenter.default.post(name: AppNotifications.timeUpdated.name(), object: nil, userInfo: nil)
                
            }

        
    }
}


