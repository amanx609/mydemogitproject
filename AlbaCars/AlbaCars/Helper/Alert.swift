//
//  Alert.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit


class Alert {
  
  // MARK: Alert methods
  class func showOkAlert(title: String, message: String, cancelTitle: String = "Ok")
  {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let cancelAction = UIAlertAction(title: cancelTitle.localizedString(), style: .cancel, handler: nil)
    alertController.addAction(cancelAction)
    if let topViewController: UIViewController = Helper.topMostViewController(rootViewController: Helper.rootViewController()) {
      Threads.performTaskInMainQueue {
        topViewController.present(alertController, animated: true, completion: nil)
      }
    } else if let viewC = AppDelegate.delegate.window?.rootViewController {
      Threads.performTaskInMainQueue {
        viewC.present(alertController, animated: true, completion: nil)
      }
    }
  }
  
  class func showOkAlertWithCallBack(title: String, message: String ,completeion_: @escaping (_ compl:Bool)->())
  {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let cancelAction = UIAlertAction(title: "Ok".localizedString(), style: .cancel, handler: { (action:UIAlertAction!) -> Void in
      completeion_(true) //returns true in callback to perform action on last screen
    })
    alertController.addAction(cancelAction)
    if let topViewController: UIViewController = Helper.topMostViewController(rootViewController: Helper.rootViewController()) {
      Threads.performTaskInMainQueue {
        topViewController.present(alertController, animated: true, completion: nil)
      }
    } else if let viewC = AppDelegate.delegate.window?.rootViewController {
      Threads.performTaskInMainQueue {
        viewC.present(alertController, animated: true, completion: nil)
      }
    }
  }
  
  class func showOkGrayAlertWithCallBack(title: String, message: String ,completeion_: @escaping (_ compl:Bool)->())
  {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let cancelAction = UIAlertAction(title: "Ok".localizedString(), style: .cancel, handler: { (action:UIAlertAction!) -> Void in
      completeion_(true) //returns true in callback to perform action on last screen
    })
    cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
    alertController.addAction(cancelAction)
    if let topViewController: UIViewController = Helper.topMostViewController(rootViewController: Helper.rootViewController()) {
      Threads.performTaskInMainQueue {
        topViewController.present(alertController, animated: true, completion: nil)
      }
    } else if let viewC = AppDelegate.delegate.window?.rootViewController {
      Threads.performTaskInMainQueue {
        viewC.present(alertController, animated: true, completion: nil)
      }
    }
  }
  
  class func showAlert(title: String? = nil, message: String? = nil, style: UIAlertController.Style = .alert, actions: [UIAlertAction]) {
    
    let topViewController: UIViewController? = Helper.topMostViewController(rootViewController: Helper.rootViewController())
    var strTitle: String = ""
    if let titleStr = title {
      strTitle = titleStr
    } else {
      strTitle = StringConstants.Text.AppName
    }
    let alertController = UIAlertController(title: strTitle, message: message, preferredStyle: style)
    if !actions.isEmpty {
      for action in actions {
        alertController.addAction(action)
      }
    } else {
      let cancelAction = UIAlertAction(title: StringConstants.Text.Cancel, style: .default, handler: nil)
      alertController.addAction(cancelAction)
    }
    Threads.performTaskInMainQueue {
      topViewController?.present(alertController, animated: true, completion: nil)
    }
  }
  
  //Logout Alert
  class func showAlertWithActionWithColor(title: String?, message: String?, actionTitle: String?, showCancel:Bool,_ viewC: UIViewController? = nil, action:((UIAlertAction) -> Void)?) {
    
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    //MessageFont
    if let textMessage = message {
      let font = FontFamily.AirbnbCerealApp.fontName(wieight: .Book)
      let messageFont = [NSAttributedString.Key.font: UIFont(name: font, size: 17.0)!]
      let messageAttrString = NSMutableAttributedString(string: textMessage, attributes: messageFont)
      alertController.setValue(messageAttrString, forKey: "attributedMessage")
    }
    
    if showCancel {
      let cancelAction = UIAlertAction(title: StringConstants.Text.Cancel, style: .default, handler: nil)
      alertController.addAction(cancelAction)
    }
    
    let action = UIAlertAction(title: actionTitle, style: .default, handler: action)
    alertController.addAction(action)
    
    if let viewController = viewC {
      Threads.performTaskInMainQueue {
        viewController.present(alertController, animated: true, completion: nil)
      }
    } else {
      let topViewController: UIViewController? = Helper.topMostViewController(rootViewController: Helper.rootViewController())
      Threads.performTaskInMainQueue {
        topViewController?.present(alertController, animated: true, completion: nil)
      }
    }
  }
  
  //PopupWithBothActions
  class func showAlertWithActionWithCancel(title: String?, message: String?, actionTitle: String?, cancelTitle: String?,_ viewC: UIViewController? = nil, action:((UIAlertAction) -> Void)?, cancelAction: ((UIAlertAction) -> Void)?) {
    
    
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    //MessageFont
    if let textMessage = message {
      let font = FontFamily.AirbnbCerealApp.fontName(wieight: .Book)
      let messageFont = [NSAttributedString.Key.font: UIFont(name: font, size: 16.0)!]
      let messageAttrString = NSMutableAttributedString(string: textMessage, attributes: messageFont)
      alertController.setValue(messageAttrString, forKey: "attributedMessage")
    }
    
    let action = UIAlertAction(title: actionTitle, style: .default, handler: action)
    alertController.addAction(action)
    
    let cancelAction = UIAlertAction(title: cancelTitle, style: .default, handler: cancelAction)
    alertController.addAction(cancelAction)
    
    if let viewController = viewC {
      Threads.performTaskInMainQueue {
        viewController.present(alertController, animated: true, completion: nil)
      }
    } else {
      let topViewController: UIViewController? = Helper.topMostViewController(rootViewController: Helper.rootViewController())
      Threads.performTaskInMainQueue {
        topViewController?.present(alertController, animated: true, completion: nil)
      }
    }
  }
}
