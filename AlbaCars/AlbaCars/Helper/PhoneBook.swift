//
//  PhoneBook.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import Foundation
import ContactsUI

class PhoneBook {
  
  //MARK: - Private Methods
  //FetchContactWithEmail
  class func fetchContactWithEmail(email: String, completion : @escaping (String) -> ()) {
    let contactStore = CNContactStore()
    contactStore.requestAccess(for: .contacts) { (granted, err) in
      if let err = err {
        print("Failed to fetch contacts:" ,err)
        return
      }
      if granted {
        let keys = [CNContactGivenNameKey,
                    CNContactPhoneNumbersKey,
                    CNContactFamilyNameKey,
                    CNContactEmailAddressesKey,
                    CNContactPostalAddressesKey]
        let searchPredicate = CNContact.predicateForContacts(matchingEmailAddress: email)
        do {
          let contactList = try contactStore.unifiedContacts(matching: searchPredicate, keysToFetch: keys as [CNKeyDescriptor])
          if let contactIdentifier = contactList.first?.identifier {
            completion(contactIdentifier)
          }
        }catch let err {
          print("Failed to enumerate contacts: ", err);
        }
        print("Access Granted")
      }else {
        print("Access Denied")
      }
    }
  }
  
  //FetchNativeContactList
  class func fetchContactList(completion : @escaping ([PhoneUsers]) -> ()) {
    let contactStore = CNContactStore()
    contactStore.requestAccess(for: .contacts) { (granted, err) in
      if let err = err {
        print("Failed to fetch contacts:" ,err)
        return
      }
      if granted {
        let keys = [CNContactGivenNameKey,
                    CNContactPhoneNumbersKey,
                    CNContactFamilyNameKey,
                    CNContactEmailAddressesKey,
                    CNContactPostalAddressesKey,
                    CNContactOrganizationNameKey]
        let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
        do {
          var arrPhoneUsers : [PhoneUsers] = []
          try  contactStore.enumerateContacts(with: request, usingBlock: { (contact, stopPointerIfYouwantToStopenumerating) in
            
            //ContactIdentifier
            let contactIReferenceId = contact.identifier
            //Name
            let firstName = contact.givenName
            let lastName = contact.familyName
            //CompanyName
            let companyName = contact.organizationName
            //PhoneNumbers
            var homeContactNumber = ""
            var workContactNumber = ""
            var mobileContactNumber = ""
            for phoneNumber in contact.phoneNumbers {
              if let label = phoneNumber.label {
                let number = phoneNumber.value
                let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
                switch localizedLabel {
                case "home":
                  homeContactNumber = number.stringValue
                case "work":
                  workContactNumber = number.stringValue
                case "mobile":
                  mobileContactNumber = number.stringValue
                default:
                  continue
                }
              }
            }
            //EmailAddress
            var email = ""
            if contact.emailAddresses.count > 0 {
              email = contact.emailAddresses[0].value as String
            }
            //Address
            var streetAddress = ""
            var city = ""
            var state = ""
            var zip = ""
            if contact.postalAddresses.count > 0 {
              let address = contact.postalAddresses[0]
              streetAddress = address.value.street
              city = address.value.city
              state = address.value.state
              zip = address.value.postalCode
            }
            
            let user = PhoneUsers(referenceId: contactIReferenceId, firstName: firstName, lastName: lastName, homeContactNumber: homeContactNumber, workContactNumber: workContactNumber, mobileContactNumber: mobileContactNumber, email: email, streetAddress: streetAddress, city: city, state: state, zip: zip, companyName: companyName, isSelected: false)
            arrPhoneUsers.append(user)
          })
          print("All Contacts List Fetched")
          completion(arrPhoneUsers)
        }catch let err {
          print("Failed to enumerate contacts: ", err);
        }
        print("Access Granted")
      }else {
        print("Access Denied")
      }
    }
  }
  
  class func addContactToNativeContacts(firstName: String, lastName: String, email: String, mobileNumber: String, homeContactNumber: String, workContactNumber: String, street: String, city: String, state: String, zip: String, companyName: String, completion : @escaping (Bool) -> ()) {
    let contactStore = CNContactStore()
    contactStore.requestAccess(for: .contacts) { (granted, err) in
      if let err = err {
        print("Failed to fetch contacts:" ,err)
        return
      }
      if granted {
        let contactNew = CNMutableContact()
        var phoneNumbers = [CNLabeledValue<CNPhoneNumber>]()
        //Name
        contactNew.givenName = firstName
        contactNew.familyName = lastName
        
        //CompanyName
        contactNew.organizationName = companyName
        
        //Email
        let workEmail = CNLabeledValue(label:CNLabelWork, value: email as NSString)
        contactNew.emailAddresses = [workEmail]
        //MobileContactNumber
        let mobileNumber = CNLabeledValue(label: CNLabelPhoneNumberMobile, value: CNPhoneNumber(stringValue: mobileNumber))
        phoneNumbers.append(mobileNumber)
        //HomeContactNumber
        let homeNumber = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue: homeContactNumber))
        phoneNumbers.append(homeNumber)
        //WorkContactNumber
        let workNumber = CNLabeledValue(label: CNLabelWork, value: CNPhoneNumber(stringValue: workContactNumber))
        phoneNumbers.append(workNumber)
        //AddPhoneNumbers
        contactNew.phoneNumbers = phoneNumbers
        
        //Address
        let address = CNMutablePostalAddress()
        //StreetAddress
        address.street = street
        //City
        address.city = city
        //State
        address.state = state
        //Zip
        address.postalCode = zip
        let homeAddress = CNLabeledValue<CNPostalAddress>(label:CNLabelHome, value:address)
        contactNew.postalAddresses = [homeAddress]
        
        //SaveRequest
        let request = CNSaveRequest()
        request.add(contactNew, toContainerWithIdentifier: nil)
        do {
          try contactStore.execute(request)
          self.fetchContactWithEmail(email: email, completion: { (contactReferenceId) in
            completion(true)
          })
        } catch let err {
          print("Failed to save the contact. \(err)")
          completion(false)
        }
      } else {
        print("Access Denied")
        completion(false)
      }
    }
  }
}

class PhoneUsers {
  var referenceId = ""
  var firstName = ""
  var lastName = ""
  var homeContactNumber = ""
  var workContactNumber = ""
  var mobileContactNumber = ""
  var email = ""
  var streetAddress = ""
  var city = ""
  var state = ""
  var zip = ""
  var companyName = ""
  var isSelected = false
  
  init(referenceId: String, firstName : String, lastName: String, homeContactNumber : String, workContactNumber: String, mobileContactNumber: String, email: String, streetAddress: String, city: String, state: String, zip: String, companyName: String, isSelected : Bool) {
    self.referenceId = referenceId
    self.firstName = firstName
    self.lastName = lastName
    
    let homeContact = String(homeContactNumber.filter{!" ()-".contains($0)})
    let homeNumber = homeContact.replacingOccurrences(of: " ", with: "")
    self.homeContactNumber = homeNumber.removeSpecialCharsFromString()
    let workContact = String(workContactNumber.filter{!" ()-".contains($0)})
    let workNumber = workContact.replacingOccurrences(of: " ", with: "")
    self.workContactNumber = workNumber.removeSpecialCharsFromString()
    let mobileContact = String(mobileContactNumber.filter{!" ()-".contains($0)})
    let mobileNumber = mobileContact.replacingOccurrences(of: " ", with: "")
    self.mobileContactNumber = mobileNumber.removeSpecialCharsFromString()
    
    self.streetAddress = streetAddress
    self.city = city
    self.state = state
    self.zip = zip
    self.email = email
    self.companyName = companyName
    self.isSelected = isSelected
  }
}
