//
//  Loader.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import Reachability
import SVProgressHUD

class Loader {
  
  // MARK: - Loading Indicator
  
  static let thickness = CGFloat(6.0)
  static let radius = CGFloat(22.0)
  
  class func showLoader(title: String = "") {
    
    DispatchQueue.main.async {
      SVProgressHUD.setBackgroundColor(UIColor.clear)
      SVProgressHUD.setDefaultMaskType(.clear)
      SVProgressHUD.setRingThickness(thickness)
      SVProgressHUD.setRingRadius(radius)
      SVProgressHUD.setForegroundColor(UIColor.gray)
      SVProgressHUD.setFont(UIFont.font(name: .AirbnbCerealApp, size: .size_10))
      if !SVProgressHUD.isVisible() {
        SVProgressHUD.show(withStatus: title)
      }
    }
  }
  
  class func showLoaderInView(title: String = "Loading...", view: UIView) {
    DispatchQueue.main.async {
      SVProgressHUD.setBackgroundColor(UIColor.clear)
      SVProgressHUD.setDefaultMaskType(.clear)
      SVProgressHUD.setRingThickness(thickness)
      SVProgressHUD.setRingRadius(radius)
      if !SVProgressHUD.isVisible() {
        SVProgressHUD.show()
      }
    }
  }
  
  class func hideLoader() {
    DispatchQueue.main.async {
      //            UIApplication.shared.endIgnoringInteractionEvents()
      SVProgressHUD.dismiss()
    }
  }
  
  class func hideLoaderInView(view: UIView) {
    DispatchQueue.main.async {
      SVProgressHUD.dismiss()
    }
  }
  
  class func showSuccessMessage(message: String) {
    DispatchQueue.main.async {
      SVProgressHUD.showSuccess(withStatus: message)
      SVProgressHUD.dismiss(withDelay: 1.0)
    }
  }
  
  // MARK: - Reachability method
  
  class func isReachabile() -> Bool {
    return true
    //      let reachability = Reachability(hostname: "www.google.com")
    //    return reachability.connection != .none
  }
}

let isDebugModeOn = true

class Debug {
  
  class func Log<T>(message: T, functionName: String = #function, fileNameWithPath: String = #file, lineNumber: Int = #line ) {
    
    Threads.performTaskInBackground {
      if isDebugModeOn {
        let fileNameWithoutPath:String = NSURL(fileURLWithPath: fileNameWithPath).lastPathComponent ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss.SSS"
        let output = "\r\n❗️\(fileNameWithoutPath) => \(functionName) (line \(lineNumber), at \(dateFormatter.string(from: NSDate() as Date)))\r\n => \(message)\r\n"
        print(output)
      }
    }
  }
}
