//
//  Enumerations.swift
//  AlbaCars
//
//  Created by Dharmendra Singh on 09/12/19.
//  Copyright © 2019 Appventurez. All rights reserved.
//

import Foundation
import UIKit

//ImageQuality
enum JPEGQuality: CGFloat {
    case lowest  = 0
    case low     = 0.25
    case medium  = 0.5
    case high    = 0.75
    case highest = 1
}

enum DeviceType: String {
    case iPhone5
    case iPhone6
    case iPhone6Plus
    case iPhoneX
    case iPhoneXR_XSMax
    case unknown
}

enum UserType: Int {
    case customer = 1
    case dealer = 2
    case supplier = 3
    
    var urlHeader: String {
        switch self {
        case .customer:
            return "USR"
        case .dealer:
            return "DLR"
        case .supplier:
            return "SPR"
        }
    }
}

enum TenderStatus: Int {
    case all = 0, bidAccepted, inProgress, upcomingTenders, noBidAccepted, allBidsRejected, completedTenders
    
    var title: String {
        switch self {
        case .all:
            return "Recovery".localizedString()
        case .inProgress:
            return "In Progress".localizedString()
        case .bidAccepted:
            return "Bid Accepted".localizedString()
        case .noBidAccepted:
            return "No Bid Accepted".localizedString()
        case .allBidsRejected:
            return "All Bid Rejected".localizedString()
        case .upcomingTenders:
            return "Upcoming Tenders".localizedString()
        case .completedTenders:
            return "Completed Tenders".localizedString()
        }
        
        
    }
}

enum CarParts: Int {
    case carFront = 0, carRear, carLeftSide, carRightSide, carMileage, gccSticker
}


// Type of tender

enum ServiceTenderType: Int {
    
    case recovery = 1
    case vehicleInspection = 2
    case upholstery = 3
    case carServicing = 4
    case spareParts = 5
    case wheels = 6
    case bodyWork = 7
    case windowTinting = 8
    case carDetailing = 9
    case bankValuation = 10
    case insurance = 11
}

enum CarDetailingOptions: Int {
    case quickWash = 1
    case detailing = 2
    case bodyPolishing = 3
    
    var optionImage: UIImage {
        switch self {
        case .quickWash:
            return #imageLiteral(resourceName: "quickWash")
        case .detailing:
            return #imageLiteral(resourceName: "detailing")
        case .bodyPolishing:
            return #imageLiteral(resourceName: "bodyPolishing")
            
        }
    }
    
    var title: String {
        switch self {
        case .quickWash:
            return "Quick Wash & Vaccum".localizedString()
        case .detailing:
            return "Detailing".localizedString()
        case .bodyPolishing:
            return "Body Polishing".localizedString()
            
        }
    }
    
    static let allValues = [quickWash, detailing, bodyPolishing]
}

enum BodyWorkOptions: Int {
    case panelPainting = 0
    case paintlessDentRemoval = 1
    case ceramicPaint = 2
    case none
    
    var optionImage: UIImage {
        switch self {
        case .panelPainting:
            return #imageLiteral(resourceName: "quickWash")
        case .paintlessDentRemoval:
            return #imageLiteral(resourceName: "detailing")
        case .ceramicPaint:
            return #imageLiteral(resourceName: "bodyPolishing")
            
        case .none:
            return #imageLiteral(resourceName: "bodyPolishing")
            
        }
    }
    
    var title: String {
        switch self {
        case .panelPainting:
            return "Panel Painting".localizedString()
        case .paintlessDentRemoval:
            return "Paintless Dent Removal".localizedString()
        case .ceramicPaint:
            return "Ceramic Paint Protection".localizedString()
        case .none:
            return ""
            
        }
    }
    
    static let allValues = [panelPainting, paintlessDentRemoval, ceramicPaint]
}


enum ChooseServiceType: Int {
    case recovery = 1
    case vehicleInspection = 2
    case carServicing = 3
    case upholstery = 4
    case spareParts = 5
    case wheels = 6
    case bodyWork = 7
    case carDetailing = 8
    case windowTinting = 9
    case bankValuation = 10
    case insurance = 11
    
    
    var unSelectedImage: UIImage {
        switch self {
        case .recovery:
            return #imageLiteral(resourceName: "recovery")
        case .vehicleInspection:
            return #imageLiteral(resourceName: "vehicle_inspection")
        case .carServicing:
            return #imageLiteral(resourceName: "car_servicing")
        case .upholstery:
            return #imageLiteral(resourceName: "upholstery")
        case .spareParts:
            return #imageLiteral(resourceName: "spare_parts")
        case .wheels:
            return #imageLiteral(resourceName: "wheels")
        case .bodyWork:
            return #imageLiteral(resourceName: "body_work")
        case .carDetailing:
            return #imageLiteral(resourceName: "car_detailing")
        case .windowTinting:
            return #imageLiteral(resourceName: "window_tinting")
        case .bankValuation:
            return #imageLiteral(resourceName: "bank_valuation")
        case .insurance:
            return #imageLiteral(resourceName: "insurance")
        }
    }
    
    var selectedImage: UIImage {
        switch self {
        case .recovery:
            return #imageLiteral(resourceName: "recovery_selected")
        case .vehicleInspection:
            return #imageLiteral(resourceName: "vehicle_inspection_selected")
        case .carServicing:
            return #imageLiteral(resourceName: "car_servicing_selected")
        case .upholstery:
            return #imageLiteral(resourceName: "upholstery_selected")
        case .spareParts:
            return #imageLiteral(resourceName: "spare_parts_selected")
        case .wheels:
            return #imageLiteral(resourceName: "wheels_selected")
        case .bodyWork:
            return #imageLiteral(resourceName: "body_work_selected")
        case .carDetailing:
            return #imageLiteral(resourceName: "car_detailing_selected")
        case .windowTinting:
            return #imageLiteral(resourceName: "window_tinting_selected")
        case .bankValuation:
            return #imageLiteral(resourceName: "bank_valuation_selected")
        case .insurance:
            return #imageLiteral(resourceName: "insurance_selected")
        }
    }
    
    var homeImage: UIImage {
        switch self {
        case .recovery:
            return #imageLiteral(resourceName: "home-recovery")
        case .vehicleInspection:
            return #imageLiteral(resourceName: "home-vehicle-inspection")
        case .carServicing:
            return #imageLiteral(resourceName: "home-car-servicing")
        case .upholstery:
            return #imageLiteral(resourceName: "home-upholstery")
        case .spareParts:
            return #imageLiteral(resourceName: "home-spare-parts")
        case .wheels:
            return #imageLiteral(resourceName: "home-wheels")
        case .bodyWork:
            return #imageLiteral(resourceName: "home-body-work")
        case .carDetailing:
            return #imageLiteral(resourceName: "home-car-detailing")
        case .windowTinting:
            return #imageLiteral(resourceName: "home-window-tinting")
        case .bankValuation:
            return #imageLiteral(resourceName: "home-bank-valuation")
        case .insurance:
            return #imageLiteral(resourceName: "home-insurance")
        }
    }
    
    var title: String {
        switch self {
        case .recovery:
            return "Recovery".localizedString()
        case .vehicleInspection:
            return "Vehicle Inspection".localizedString()
        case .carServicing:
            return "Car Servicing".localizedString()
        case .upholstery:
            return "Upholstery".localizedString()
        case .spareParts:
            return "Spare Parts".localizedString()
        case .wheels:
            return "Wheels".localizedString()
        case .bodyWork:
            return "Body Work".localizedString()
        case .carDetailing:
            return "Car Detailing".localizedString()
        case .windowTinting:
            return "Window Tinting".localizedString()
        case .bankValuation:
            return "Bank Valuation".localizedString()
        case .insurance:
            return "Insurance".localizedString()
        }
    }
    
    static let allValues = [recovery, vehicleInspection, carServicing,upholstery,spareParts,wheels,bodyWork,carDetailing,windowTinting,bankValuation,insurance]
    
}

enum TextInputType: Int {
    case name = 0, dob, email, password, mileage, plateNumber, cardNumber,modelYear,odometer,expectedPrice, mobile, pickupCode, dropOffCode, engineSize, bankName, partName,wheelSize, tintColor,vehicleValue,carPrice, deliveryLocation
    
}

enum AuctionType: Int {
    case live = 1, upcoming, unsoldCars, soldCars
    
    var headerTitle: String {
        switch self {
        case .live: return "Live Auctions".localizedString()
        case .upcoming: return "Upcoming Auctions".localizedString()
        case .unsoldCars: return "Unsold Cars".localizedString()
        case .soldCars: return "Sold Cars".localizedString()
        }
    }
}

enum AuctioneerRequestStatus: Int {
    case pending = 0, accepted, rejected, requestNotSent
}

enum HelpCenterType: Int {
    case email = 1, phone, video
}

enum RecoveryHomeScreenType: String {
    case none = "0"
    case recovery = "1"
    case inspection = "2"
    case upholstery = "3"
    case servicing = "4"
    case spareParts = "5"
    case wheels = "6"
    case tyres = "61"
    case rims = "62"
    case bodyWork = "7"
    case panelPainting = "71"
    case paintlessDentRemoval = "72"
    case ceramicPainting = "73"
    case windowTinting = "8"
    case carDetailing = "9"
    case quickWash = "91"
    case detailing = "92"
    case bodyPolishing = "93"
    case bankValuation = "10"
    case insurance = "11"
    
    var title: String? {
        switch self {
        case .recovery: return "Recovery".localizedString()
        case .inspection: return "Vehicle Inspection".localizedString()
        case .upholstery: return "Upholstery".localizedString()
        case .servicing: return "Car Servicing".localizedString()
        case .spareParts: return "Spare Parts".localizedString()
        case .wheels: return "Wheels".localizedString()
        case .windowTinting: return "Window Tinting".localizedString()
        case .tyres: return "Tires".localizedString()
        case .rims: return "Rims".localizedString()
        case .bankValuation: return "Bank Valuation".localizedString()
        case .insurance: return "Insurance".localizedString()
        case .carDetailing: return "Car Detailing".localizedString()
        case .bodyWork: return "Body Work".localizedString()
        default: return ""
        
        }
    }
}


enum WheelType: Int {
    case tyres = 0, rims
}

enum ChooseServiceOptions: String {
    case major = "major"
    case minor = "minor"
}

enum ChooseSubServiceOptions: String {
    case frontBrakePads = "1"
    case rearBrakePads = "2"
    case brakeDiscs = "3"
    
}

enum datePickerType: Int {
    case dob
}

enum SocialType: String {
    case email = "0"
    case facebook = "1"
    case google = "2"
}

enum FilterOptions: Int {
    case brand = 0, year, mileage, price
}

enum LeaderBoardType: Int {
    case auctions = 0, fleetBidding, leads
}

enum ChangePwdSenderType: Int {
    case forgotPassword = 0, profile
}

enum ScreenNameAfterLogin: Int {
    case otp = 1, serviceType,documents,payment,home
}

enum DocumentType: Int {
    case front = 1, back, ownerCertificate
}

enum BrandOptions: Int {
    case audi = 0, alfaRomeo, bmw, bugatti, buick, cadillac, chevrolet, citroen, dodge
}

enum TwoOptionsSelection: Int {
    case yes = 1, no
}

enum BuyNowOptions: Int {
    case cash = 2, reserve = 0, bankApplication = 1
}

enum BuyCarCellType: Int {
    case buyCars = 0, bankFinance
}

enum FinanceStatusVal: Int {
    case notSubmitted = 0, submitted, verification, approved
}

enum AppNotifications: String {
    case timeUpdated = "timeUpdated"
    
    func name() -> NSNotification.Name {
        return NSNotification.Name(rawValue: self.rawValue)
    }
}

enum AgencyType: Int {
    case Warranty = 1, ServiceContract,ServiceHistory
}

enum FinanceOptions: Int {
    case employed = 1
    case selfEmployed = 2
    case companies = 3
    
    var description: String {
        switch self {
        case .employed: return "EMPLOYED:".localizedString()
        case .selfEmployed: return "SELF EMPLOYED:".localizedString()
        case .companies: return "COMPANIES:".localizedString()
        }
    }
}

enum BidStatus: Int {
    case pending = 0
    case ongoing = 1
    case won = 2
    case rejected = 3
    case carBought = 4
    
    var title: String {
        switch self {
        case .pending: return ""
        case .won: return "Bid Won".localizedString()
        case .ongoing: return "Ongoing Bid".localizedString()
        case .rejected: return "Bid Rejected".localizedString()
        case .carBought: return "Car Bought".localizedString()
        }
    }
    
    var image: UIImage {
        switch self {
        case .pending: return #imageLiteral(resourceName: "ongoing")
        case .won: return #imageLiteral(resourceName: "trophy")
        case .ongoing: return #imageLiteral(resourceName: "ongoing")
        case .rejected: return #imageLiteral(resourceName: "rejected")
        case .carBought: return #imageLiteral(resourceName: "handshake")
        }
    }
    
    var titleColor: UIColor {
        switch self {
        case .pending: return .black
        case .won: return .greenTopHeaderColor
        case .ongoing: return .blueTextColor
        case .rejected: return .redButtonColor
        case .carBought: return .orangeTextColor
        }
    }
}

enum TenderBidStatus: Int {
    
    case submitted = 1
    case accepted = 2
    case rejected = 3
    case completed = 4
    
    var title: String {
        switch self {
        case .submitted: return "Bid\nSubmitted"
        case .accepted: return "Bid\nAccepted"
        case .rejected: return "Bid\nRejected"
        case .completed: return "Job\nCompleted"
        }
    }
    
    var backgroundColor: UIColor {
        switch self {
        case .submitted: return #colorLiteral(red: 0.462745098, green: 0.7058823529, blue: 1, alpha: 1)
        case .accepted: return #colorLiteral(red: 1, green: 0.6941176471, blue: 0.1529411765, alpha: 1)
        case .rejected: return #colorLiteral(red: 0.7568627451, green: 0.1529411765, blue: 0.1529411765, alpha: 1)
        case .completed: return #colorLiteral(red: 0.07843137255, green: 0.6784313725, blue: 0.3333333333, alpha: 1)
        }
    }
    //1 = submitted ,2 = accepted, 3 = Rejected, 4 = Completed (for all don't send status)
    var dataList: [[String: AnyObject]] {
        
        switch self {
        case .submitted,.accepted,.rejected,.completed:
            return [[ConstantAPIKeys.name: "All".localizedString() as AnyObject, ConstantAPIKeys.id: "0".localizedString() as AnyObject],[ConstantAPIKeys.name: "Bid Accepted".localizedString() as AnyObject, ConstantAPIKeys.id: "2".localizedString() as AnyObject],
                    [ConstantAPIKeys.name: "Bid Submitted".localizedString() as AnyObject, ConstantAPIKeys.id: "1".localizedString() as AnyObject],
                    [ConstantAPIKeys.name: "Bid Rejected".localizedString() as AnyObject, ConstantAPIKeys.id: "3".localizedString() as AnyObject],
                    [ConstantAPIKeys.name: "Job Completed".localizedString() as AnyObject, ConstantAPIKeys.id: "4".localizedString() as AnyObject]]
        }
    }
    
}


enum Extra: Int {
    case warranty = 1
    case registration = 2
    case salik = 3
    case windowTinting = 4
    case service = 5
    
    var title: String {
        switch self {
        case .warranty: return "warranty"
        case .registration: return "registration"
        case .salik: return "salik"
        case .windowTinting: return "window Tinting"
        case .service: return "service"
        }
    }
}

enum MileageRangeOptions: Int {
    case range0to10k = 1
    case range10to20k = 2
    case range20to30k = 3
    case range30to40k = 4
    case range40to50k = 5
    case range50to100k = 6
    
    var palceHolder: String {
        switch self {
        case .range0to10k: return "0 KM - 10,000 KM"
        case .range10to20k: return "10,000 KM - 20,000 KM"
        case .range20to30k: return "20,000 KM - 30,000 KM"
        case .range30to40k: return "30,000 KM - 40,000 KM"
        case .range40to50k: return "40,000 KM - 50,000 KM"
        case .range50to100k: return "50,000 KM - 1,00,000 KM"
        }
    }
    
    var value: String {
        switch self {
        case .range0to10k: return "0-10000"
        case .range10to20k: return "10000-20000"
        case .range20to30k: return "20000-30000"
        case .range30to40k: return "30000-40000"
        case .range40to50k: return "40000-50000"
        case .range50to100k: return "50000-100000"
        }
    }
    
    static let allValues = [range0to10k, range10to20k, range20to30k,range30to40k,range40to50k,range50to100k]
    
}

enum LeadsStatusType: String {
    case leadsStatus = "LeadsStatus"
    
    var dataList: [[String: AnyObject]] {
        switch self {
        case .leadsStatus:
            return [[LeadsStatusType.leadsStatus.rawValue: "Contacted".localizedString() as AnyObject, ConstantAPIKeys.id: "2".localizedString() as AnyObject],
                    [LeadsStatusType.leadsStatus.rawValue: "Deal Closed".localizedString() as AnyObject,ConstantAPIKeys.id: "3".localizedString() as AnyObject ],
                    [LeadsStatusType.leadsStatus.rawValue: "Not Contacted".localizedString() as AnyObject ,ConstantAPIKeys.id: "1".localizedString() as AnyObject],
                    [LeadsStatusType.leadsStatus.rawValue: "No Deal".localizedString() as AnyObject, ConstantAPIKeys.id: "4".localizedString() as AnyObject]]
        }
    }
    //1=>'notContacted',2=>'contacted',3=>'dealClosed',4=>'noDeal'
}

enum LabelDropDownType: Int {
    case year = 0, brands, time, option
}

enum DropDownType: String {
    case engineSize = "engineSize"
    case cylinders = "cylinders"
    case specs = "specs"
    case transmission = "transmission"
    case fuelType = "fuelType"
    case noOfDoors = "noOfDoors"
    case drive = "drive"
    case service = "service"
    case accident = "accident"
    case chasisDamage = "chasisDamage"
    case seats = "seats"
    case sunroof = "sunroof"
    case navigation = "navigation"
    case camera = "camera"
    case wheels = "wheels"
    case cruiseControl = "cruiseControl"
    case windows = "windows"
    case tyresConditionWeek = "tyresConditionWeek"
    case tyresConditionYear = "tyresConditionYear"
    case perfectDamaged = "perfectDamaged"
    case perfectMinorMajorDamaged = "perfectMinorMajorDamaged"
    case originalDamaged = "originalDamaged"
    case numberOfEmployees = "numberOfEmployees"
    case maximumPrice = "MaximumPrice"
    case monthlyInstallment = "MonthlyInstallment"
    case minMileageRange = "minMileageRange"
    case maxMileageRange = "maxMileageRange"
    case recoveryType = "recoveryType"
    case maxBudgetType = "maxBudgetType"
    case typeOfService = "typeOfService"
    case partType = "partType"
    case serviceType = "serviceType"
    case wheelsService = "wheelsService"
    case tintPercentage = "tintPercentage"
    case bodyServiceType = "bodyServiceType"
    case carDetailingServiceType = "carDetailingServiceType"
    case detailingService = "detailingService"
    case insuranceType = "insuranceType"
    case fullPackage = "fullPackage"
    case notificationOption = "notificationOption"
    
    var dataList: [[String: AnyObject]] {
        switch self {
        case .engineSize:
            
            var engineSize = [[String: AnyObject]]()
            for index in 0...9 {
                engineSize.append([DropDownType.engineSize.rawValue: Helper.toString(object: index).localizedString() as AnyObject])
            }
            return engineSize
            
        case .cylinders:
            var cylinders = [[String: AnyObject]]()
            for index in 1...16 {
                cylinders.append([DropDownType.cylinders.rawValue: Helper.toString(object: index).localizedString() as AnyObject])
            }
            return cylinders
        case  .numberOfEmployees:
            return [[DropDownType.numberOfEmployees.rawValue: "1-50".localizedString() as AnyObject],
                    [DropDownType.numberOfEmployees.rawValue: "51-100".localizedString() as AnyObject],
                    [DropDownType.numberOfEmployees.rawValue: "101-500".localizedString() as AnyObject],
                    [DropDownType.numberOfEmployees.rawValue: "501-1000".localizedString() as AnyObject],
                    [DropDownType.numberOfEmployees.rawValue: "1001-2000".localizedString() as AnyObject]
            ]
        case .specs:
            return [[DropDownType.specs.rawValue: "GCC".localizedString() as AnyObject],
                    [DropDownType.specs.rawValue: "American".localizedString() as AnyObject],
                    [DropDownType.specs.rawValue: "Japanese".localizedString() as AnyObject],
                    [DropDownType.specs.rawValue: "European".localizedString() as AnyObject],
                    [DropDownType.specs.rawValue: "Other".localizedString() as AnyObject]
            ]
        case .transmission:
            return [[DropDownType.transmission.rawValue: "Automatic".localizedString() as AnyObject],
                    [DropDownType.transmission.rawValue: "Manual".localizedString() as AnyObject],
            ]
        case .fuelType:
            return [[DropDownType.fuelType.rawValue: "Petrol".localizedString() as AnyObject],
                    [DropDownType.fuelType.rawValue: "Diesel".localizedString() as AnyObject],
                    [DropDownType.fuelType.rawValue: "Electric".localizedString() as AnyObject]
            ]
        case .noOfDoors:
            return [[DropDownType.noOfDoors.rawValue: "2".localizedString() as AnyObject],
                    [DropDownType.noOfDoors.rawValue: "4".localizedString() as AnyObject],
            ]
        case .drive:
            return [[DropDownType.drive.rawValue: "2WD".localizedString() as AnyObject],
                    [DropDownType.drive.rawValue: "4WD".localizedString() as AnyObject],
                    [DropDownType.drive.rawValue: "AWD".localizedString() as AnyObject],
                    [DropDownType.drive.rawValue: "Unknown".localizedString() as AnyObject]
            ]
        case .service:
            return [[DropDownType.service.rawValue: "Agency (Records Available)".localizedString() as AnyObject],
                    [DropDownType.service.rawValue: "Non Agency".localizedString() as AnyObject],
                    [DropDownType.service.rawValue: "Unverified".localizedString() as AnyObject]
            ]
        case .accident:
            return [[DropDownType.accident.rawValue: "Perfect".localizedString() as AnyObject],
                    [DropDownType.accident.rawValue: "Minor".localizedString() as AnyObject],
                    [DropDownType.accident.rawValue: "Major".localizedString() as AnyObject],
                    [DropDownType.accident.rawValue: "Unknown".localizedString() as AnyObject]
            ]
        case .chasisDamage:
            return [[DropDownType.chasisDamage.rawValue: "Perfect".localizedString() as AnyObject],
                    [DropDownType.chasisDamage.rawValue: "Repaired".localizedString() as AnyObject],
                    [DropDownType.chasisDamage.rawValue: "Damaged".localizedString() as AnyObject]
            ]
        case .seats:
            return [[DropDownType.seats.rawValue: "Leather".localizedString() as AnyObject],
                    [DropDownType.seats.rawValue: "Fabric".localizedString() as AnyObject],
                    [DropDownType.seats.rawValue: "Half Leather Half Fabric".localizedString() as AnyObject]
            ]
        case .sunroof:
            return [[DropDownType.sunroof.rawValue: "Sunroof".localizedString() as AnyObject],
                    [DropDownType.sunroof.rawValue: "Panaromic Roof".localizedString() as AnyObject],
                    [DropDownType.sunroof.rawValue: "None".localizedString() as AnyObject]
            ]
        case .navigation:
            return [[DropDownType.navigation.rawValue: "Yes".localizedString() as AnyObject],
                    [DropDownType.navigation.rawValue: "No".localizedString() as AnyObject],
                    [DropDownType.navigation.rawValue: "Yes (Not Factory)".localizedString() as AnyObject]
            ]
        case .camera:
            return [[DropDownType.camera.rawValue: "Yes".localizedString() as AnyObject],
                    [DropDownType.camera.rawValue: "Yes 360".localizedString() as AnyObject],
                    [DropDownType.camera.rawValue: "No".localizedString() as AnyObject]
            ]
        case .wheels:
            return [[DropDownType.wheels.rawValue: "Alloy".localizedString() as AnyObject],
                    [DropDownType.wheels.rawValue: "Wheel Cap".localizedString() as AnyObject]
            ]
        case .cruiseControl:
            return [[DropDownType.cruiseControl.rawValue: "Yes".localizedString() as AnyObject],
                    [DropDownType.cruiseControl.rawValue: "No".localizedString() as AnyObject],
            ]
        case .windows:
            return [[DropDownType.windows.rawValue: "Electric".localizedString() as AnyObject],
                    [DropDownType.windows.rawValue: "Manual".localizedString() as AnyObject]
            ]
        case .tyresConditionWeek:
            var weekArray = [[String: AnyObject]]()
            for index in 1...52 {
                weekArray.append([DropDownType.tyresConditionWeek.rawValue: Helper.toString(object: index).localizedString() as AnyObject])
            }
            return weekArray
            
        case .tyresConditionYear:
            var yearArray = [[String: AnyObject]]()
            for index in 1...25 {
                yearArray.append([DropDownType.tyresConditionYear.rawValue: Helper.toString(object: index).localizedString() as AnyObject])
            }
            return yearArray
        case .perfectDamaged:
            return [[DropDownType.perfectDamaged.rawValue: "Perfect".localizedString() as AnyObject],
                    [DropDownType.perfectDamaged.rawValue: "Damaged".localizedString() as AnyObject],
            ]
        case .perfectMinorMajorDamaged:
            return [[DropDownType.perfectMinorMajorDamaged.rawValue: "Perfect".localizedString() as AnyObject],
                    [DropDownType.perfectMinorMajorDamaged.rawValue: "Minor Damaged".localizedString() as AnyObject],
                    [DropDownType.perfectMinorMajorDamaged.rawValue: "Major Damaged".localizedString() as AnyObject]
            ]
        case .originalDamaged:
            return [[DropDownType.originalDamaged.rawValue: "Original".localizedString() as AnyObject],
                    [DropDownType.originalDamaged.rawValue: "Original Damaged".localizedString() as AnyObject],
                    [DropDownType.originalDamaged.rawValue: "Painted".localizedString() as AnyObject],
                    [DropDownType.originalDamaged.rawValue: "Painted Damaged".localizedString() as AnyObject],
                    [DropDownType.originalDamaged.rawValue: "Unknown".localizedString() as AnyObject]
            ]
        case .maximumPrice:
            return [[DropDownType.maximumPrice.rawValue: "All".localizedString() as AnyObject],
                    [DropDownType.maximumPrice.rawValue: "AED 24,000".localizedString() as AnyObject],
                    [DropDownType.maximumPrice.rawValue: "AED 20,0000".localizedString() as AnyObject],
            ]
        case .monthlyInstallment:
            return [[DropDownType.monthlyInstallment.rawValue: "All".localizedString() as AnyObject],
                    [DropDownType.monthlyInstallment.rawValue: "AED 1,000".localizedString() as AnyObject],
                    [DropDownType.monthlyInstallment.rawValue: "AED 10,000".localizedString() as AnyObject],
            ]
        case .minMileageRange:
            return [[DropDownType.minMileageRange.rawValue: "0".localizedString() as AnyObject],
                    [DropDownType.minMileageRange.rawValue: "10000".localizedString() as AnyObject],
                    [DropDownType.minMileageRange.rawValue: "20000".localizedString() as AnyObject],
                    [DropDownType.minMileageRange.rawValue: "30000".localizedString() as AnyObject],
                    [DropDownType.minMileageRange.rawValue: "40000".localizedString() as AnyObject],
                    [DropDownType.minMileageRange.rawValue: "50000".localizedString() as AnyObject],
            ]
        case .maxMileageRange:
            return [[DropDownType.maxMileageRange.rawValue: "10000".localizedString() as AnyObject],
                    [DropDownType.maxMileageRange.rawValue: "20000".localizedString() as AnyObject],
                    [DropDownType.maxMileageRange.rawValue: "30000".localizedString() as AnyObject],
                    [DropDownType.maxMileageRange.rawValue: "40000".localizedString() as AnyObject],
                    [DropDownType.maxMileageRange.rawValue: "50000".localizedString() as AnyObject],
                    [DropDownType.maxMileageRange.rawValue: "100000".localizedString() as AnyObject]
            ]
        case .recoveryType:
            return [[DropDownType.recoveryType.rawValue: "Flat Bed".localizedString() as AnyObject],
                    [DropDownType.recoveryType.rawValue: "Normal".localizedString() as AnyObject]]
        case .maxBudgetType:
            return [[DropDownType.maxBudgetType.rawValue: "Monthly".localizedString() as AnyObject],
                    [DropDownType.maxBudgetType.rawValue: "Yearly".localizedString() as AnyObject]]
        case .typeOfService:
            return [[DropDownType.typeOfService.rawValue: "Major".localizedString() as AnyObject],
                    [DropDownType.typeOfService.rawValue: "Minor".localizedString() as AnyObject]]
        case .partType:
            return [[DropDownType.partType.rawValue: "Original New".localizedString() as AnyObject,Constants.UIKeys.id: 1 as AnyObject],
                    [DropDownType.partType.rawValue: "Commercial New".localizedString() as AnyObject,Constants.UIKeys.id: 2 as AnyObject],
                    [DropDownType.partType.rawValue: "Original Used".localizedString() as AnyObject,Constants.UIKeys.id: 3 as AnyObject],
                    [DropDownType.partType.rawValue: "Commercial Used".localizedString() as AnyObject,Constants.UIKeys.id: 4 as AnyObject]]
        case .serviceType:
            return [
                [DropDownType.serviceType.rawValue: "Rim Restoration".localizedString() as AnyObject,Constants.UIKeys.id: 1 as AnyObject],
                [DropDownType.serviceType.rawValue: "Rim Replacement".localizedString() as AnyObject,Constants.UIKeys.id: 2 as AnyObject]]
        case .wheelsService:
            return [[DropDownType.wheelsService.rawValue: "Tires".localizedString() as AnyObject,Constants.UIKeys.id: 0 as AnyObject],
                    [DropDownType.wheelsService.rawValue: "Rim".localizedString() as AnyObject,Constants.UIKeys.id: 1 as AnyObject]]
        case .tintPercentage:
            return [[DropDownType.tintPercentage.rawValue: "5".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "10".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "15".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "20".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "25".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "30".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "35".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "40".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "45".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "50".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "55".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "60".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "65".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "70".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "75".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "80".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "85".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "90".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "95".localizedString() as AnyObject],
                    [DropDownType.tintPercentage.rawValue: "100".localizedString() as AnyObject]
            ]
        case .bodyServiceType:
            
            return [[DropDownType.bodyServiceType.rawValue: "Panel Painting".localizedString() as AnyObject,Constants.UIKeys.id: 0 as AnyObject],
                    [DropDownType.bodyServiceType.rawValue: "Paintless Dent Removal".localizedString() as AnyObject,Constants.UIKeys.id: 1 as AnyObject],
                    [DropDownType.bodyServiceType.rawValue: "Ceramic Paint Protection".localizedString() as AnyObject,Constants.UIKeys.id: 2 as AnyObject]]
        case .carDetailingServiceType:
            return [[DropDownType.carDetailingServiceType.rawValue: "Quick Wash & Vaccum".localizedString() as AnyObject,Constants.UIKeys.id: 1 as AnyObject],
                    [DropDownType.carDetailingServiceType.rawValue: "Detailing".localizedString() as AnyObject,Constants.UIKeys.id: 2 as AnyObject],
                    [DropDownType.carDetailingServiceType.rawValue: "Body Polish".localizedString() as AnyObject,Constants.UIKeys.id: 3 as AnyObject]]
        case .detailingService:
            return [[DropDownType.detailingService.rawValue: "Interior".localizedString() as AnyObject,Constants.UIKeys.id: 1 as AnyObject],
                    [DropDownType.detailingService.rawValue: "Exterior".localizedString() as AnyObject,Constants.UIKeys.id: 2 as AnyObject],
                    [DropDownType.detailingService.rawValue: "Exterior & Interior".localizedString() as AnyObject,Constants.UIKeys.id: 3 as AnyObject]]
        case .insuranceType:
            return [[DropDownType.insuranceType.rawValue: "Comprehensive (Agency Repair)".localizedString() as AnyObject,Constants.UIKeys.id: 1 as AnyObject],
                    [DropDownType.insuranceType.rawValue: "Comprehensive (Non Agency Repair)".localizedString() as AnyObject,Constants.UIKeys.id: 2 as AnyObject],
                    [DropDownType.insuranceType.rawValue: "Third Party".localizedString() as AnyObject,Constants.UIKeys.id: 3 as AnyObject]]
        case .fullPackage:
            return [[DropDownType.fullPackage.rawValue: "Full Package".localizedString() as AnyObject,Constants.UIKeys.id: 1 as AnyObject]]
        case .notificationOption:
            return [[DropDownType.notificationOption.rawValue: "GCC".localizedString() as AnyObject,Constants.UIKeys.id: 5 as AnyObject],
                    [DropDownType.notificationOption.rawValue: "American".localizedString() as AnyObject,Constants.UIKeys.id: 2 as AnyObject],
                    [DropDownType.notificationOption.rawValue: "Japanese".localizedString() as AnyObject,Constants.UIKeys.id: 3 as AnyObject],
                    [DropDownType.notificationOption.rawValue: "European".localizedString() as AnyObject,Constants.UIKeys.id: 4 as AnyObject],
                    [DropDownType.notificationOption.rawValue: "Any".localizedString() as AnyObject,Constants.UIKeys.id: 1 as AnyObject]
            ]
        }
    }
    
    var title: String {
        switch self {
        case .engineSize:
            return "Engine Size".localizedString()
        case .cylinders:
            return "Cylinders".localizedString()
        case .specs:
            return "Specs".localizedString()
        case .transmission:
            return "Transmission".localizedString()
        case .fuelType:
            return "Fuel Type".localizedString()
        case .noOfDoors:
            return "No. Of Doors".localizedString()
        case .drive:
            return "Drive".localizedString()
        case .service:
            return "Service".localizedString()
        case .accident:
            return "Accident".localizedString()
        case .chasisDamage:
            return "Chasis/Frame Damage".localizedString()
        case .seats:
            return "Seats".localizedString()
        case .sunroof:
            return "Sunroof".localizedString()
        case .navigation:
            return "Navigation".localizedString()
        case .camera:
            return "Camera".localizedString()
        case .wheels:
            return "Wheels".localizedString()
        case .cruiseControl:
            return "Cruise Control".localizedString()
        case .windows:
            return "Windows".localizedString()
        case .tyresConditionWeek:
            return ""
        case .perfectDamaged:
            return ""
        case .perfectMinorMajorDamaged:
            return ""
        case .tyresConditionYear:
            return ""
        case .originalDamaged:
            return ""
        case .numberOfEmployees:
            return "Number Of Employees".localizedString()
        case .maximumPrice:
            return "Maximum Price"
        case .monthlyInstallment:
            return "Maximum Monthly Installment"
        case .minMileageRange:
            return "Minimum Mileage"
        case .maxMileageRange:
            return "Maximum Mileage"
        case .recoveryType:
            return "Select Recovery Type".localizedString()
        case .maxBudgetType:
            return ""
        case .typeOfService:
            return "Type Of Service".localizedString()
        case .partType:
            return "Part Type".localizedString()
        case .serviceType:
            return "Service Type".localizedString()
        case .wheelsService:
            return "Service".localizedString()
        case .tintPercentage:
            return "Tint %age".localizedString()
        case .bodyServiceType:
            return "Service Type".localizedString()
        case .carDetailingServiceType:
            return "Service Type".localizedString()
        case .detailingService:
            return "Service".localizedString()
        case .insuranceType:
            return "Insurance Type".localizedString()
        case .fullPackage:
            return "Select Package".localizedString()
        case .notificationOption:
            return "Option".localizedString()
        }
    }
}

enum AuctionListType: Int {
    case none = 0, auctionOfDay, featured, live, upcoming, myAuction
}

enum PopupType: Int {
    case vehicleBrands = 0, vehicleYear, time
}

enum LiveAuctionPopupType: Int {
    case confirmBid, lowerBid, highestBidder
}

enum NotificationPopupType: Int {
    case buyCarNotify = 0
    case auctionCarNotify = 1
    
    var headerTitle: String {
        switch self {
        case .buyCarNotify:
            return ""
        case .auctionCarNotify:
            return "Notify Me Before Auction Starts".localizedString()
        }
    }
    
    var dataList: [[String: AnyObject]] {
        switch self {
        case .buyCarNotify:
            
            var buyCarNotificationList = [[String: AnyObject]]()
            let firstData = [Constants.UIKeys.id: 1 as AnyObject,
                             Constants.UIKeys.titlePlaceholder: "Notify Price Drop for this Car".localizedString() as AnyObject,
                             Constants.UIKeys.isSelected: true as AnyObject]
            buyCarNotificationList.append(firstData)
            
            let secondData = [Constants.UIKeys.id: 2 as AnyObject,
                              Constants.UIKeys.titlePlaceholder: "Notify About Similar Cars in Future".localizedString() as AnyObject,
                              Constants.UIKeys.isSelected: false as AnyObject]
            buyCarNotificationList.append(secondData)
            
            let thirdData = [Constants.UIKeys.id: 3 as AnyObject,
                             Constants.UIKeys.titlePlaceholder: "Notify If this car becomes Availble".localizedString() as AnyObject,
                             Constants.UIKeys.isSelected: false as AnyObject]
            buyCarNotificationList.append(thirdData)
            
            return buyCarNotificationList
        case .auctionCarNotify:
            var auctionCarNotificationList = [[String: AnyObject]]()
            let firstData = [Constants.UIKeys.id: 1 as AnyObject,
                             Constants.UIKeys.titlePlaceholder: "10 Mins Before Auction Starts".localizedString() as AnyObject,
                             Constants.UIKeys.isSelected: true as AnyObject]
            auctionCarNotificationList.append(firstData)
            
            let secondData = [Constants.UIKeys.id: 2 as AnyObject,
                              Constants.UIKeys.titlePlaceholder: "5 Mins Before Auction Starts".localizedString() as AnyObject,
                              Constants.UIKeys.isSelected: false as AnyObject]
            auctionCarNotificationList.append(secondData)
            
            let thirdData = [Constants.UIKeys.id: 3 as AnyObject,
                             Constants.UIKeys.titlePlaceholder: "1 Min Before Auction Starts".localizedString() as AnyObject,
                             Constants.UIKeys.isSelected: false as AnyObject]
            auctionCarNotificationList.append(thirdData)
            
            return auctionCarNotificationList
        }
    }
}

enum CPaymentFiltersId: Int {
    case none = 0, smartTenders = 1, buyCars, offersnDeals,warranty, serviceContract, serviceHistory, auction, leads, fleetBidding
}

enum SPaymentFiltersId: Int {
    case none = 1, paid = 2, pending = 0, all = 3
    
}

enum PaymentServiceCategoryId: Int {
    
    case smartTenders = 1, auctions = 2 , leads = 3, fleetBidding = 4, leaderboard = 5, warranty = 16 ,serviceHistory = 18,buyVehicle  = 8,sell = 9,requestCar = 10,carFinance = 11, offersDeals = 12, serviceContract = 17,cars = 14,registration = 19
    
    //1 > Smart Tenders
    //2 > Auctions
    //3 > Leads
    //4 > Fleet Bidding
    //5 > Leaderboard
    //8 > Buy
    //9 > Sell
    //10 > Request Car
    //11 > Car Finance
    //12 > Offers & Deals
    //14 > Cars
    //16 > Warrenty(0)
    //17 > Service Contact(1)
    //18 > Service History
    //19 > Registration
    
}

enum PaymentStatus: Int {
    
    case pending = 0, initiated = 1, success = 2, failed = 3, cancelled = 4
    
    //Status of payment -
    //0 > pending
    //1 > initiated
    //2 > success
    //3 > failed
}

enum ServiceOptions: Int {
    
    case frontBrakePads = 1
    case rearBrakePads = 2
    case brakeDiscs = 3
    
    var title: String {
        switch self {
        case .frontBrakePads:
            return "Front Brake Pads".localizedString()
        case .rearBrakePads:
            return "Rear Brake Pads".localizedString()
        case .brakeDiscs:
            return "Brake Discs".localizedString()
        }
    }
}

//DealerLiveAuctionCellType
enum DLACellType: Int {
    case imageLabelCell = 1, specsCell, weekCell
}

enum CAddNewCarType: Int {
    case add = 1, edit
}

enum CarConditions: String {
    case perfect = "Perfect", minorDamaged = "Damaged" , damaged = "Minor Damage"
    
    var title: String {
        switch self {
        case .perfect:
            return "Perfect".localizedString()
        case .damaged:
            return "Damaged".localizedString()
        case .minorDamaged:
            return "Minor Damage".localizedString()
        }
    }
    
    var icon: UIImage {
        switch self {
        case .perfect:
            return #imageLiteral(resourceName: "checkGreen")
        case .damaged, .minorDamaged:
            return #imageLiteral(resourceName: "cross_red")
        }
    }
}

//SparePartType
enum SparePartType: Int {
    case originalNew = 1, commercialNew, originalUsed, commercialUsed
    
    var title: String {
        switch self {
        case .originalNew:
            return "Original New".localizedString()
        case .commercialNew:
            return "Commercial New".localizedString()
        case .originalUsed:
            return "Original Used".localizedString()
        case .commercialUsed:
            return "Commercial Used".localizedString()
        }
    }
}

enum InsuranceCellDataType: Int {
    case comprehensive = 0, AgencyRepair, NonAgencyRepair, ThirdParty
}

//SparePartType
enum CarDetailServiceType: Int {
    case interior = 1, exterior, both
    
    var title: String {
        switch self {
        case .interior:
            return "Interior".localizedString()
        case .exterior:
            return "Exterior".localizedString()
        case .both:
            return "Both".localizedString()
            
        }
    }
}

// Roadside Assistance
enum RoadsideAssistanceType: Int {
    case oman = 1, offRoad, carHire
    
    var title: String {
        switch self {
        case .oman:
            return "Oman".localizedString()
        case .offRoad:
            return "Off-Road".localizedString()
        case .carHire:
            return "Car Hire".localizedString()
        }
    }
}

//RimServiceType
enum RimServiceType: Int {
    case rimRestoration = 1, rimReplacement
    
    var title: String {
        switch self {
        case .rimRestoration:
            return "Rim Restoration"
        case .rimReplacement:
            return "Rim Replacement"
        }
        
    }
}


//BodyWork Type
enum BodyWorkType: Int {
    case panelPainting = 0, paintlessDentRemoval = 1, ceramicPaintProtection = 2
}

//Car Detailing Type
enum CarDetailingServiceType: Int {
    case quickWashVaccum = 1
    case detailing = 2
    case bodyPolish = 3
    
    var title: String {
        switch self {
        case .quickWashVaccum:
            return "Quick Wash & Vaccum".localizedString()
        case .detailing:
            return "Detailing".localizedString()
        case .bodyPolish:
            return "Body Polish".localizedString()
        }
    }
    
}


enum CarDetailingService: Int {
    case interior = 1
    case exterior = 2
    case both = 3
    
    var title: String {
        switch self {
        case .exterior:
            return "Exterior".localizedString()
        case .interior:
            return "Interior".localizedString()
        case .both:
            return "Exterior & Interior".localizedString()
        }
    }
    
}

//MARK: - Insurance

enum InsuranceType: Int {
    case agency = 1
    case nonAgency = 2
    case thirdParty = 3
    
    var title: String {
        switch self {
        case .agency:
            return "Comprehensive (Agency Repair)".localizedString()
        case .nonAgency:
            return "Comprehensive (Non Agency Repair)".localizedString()
        case .thirdParty:
            return "Third Party".localizedString()
        }
    }
}
