//
//  Encryptor.swift
//  PayfortiOSSDKExample
//
//  Created by abdelrahman mohamed on 7/6/18.
//  Copyright © 2018 abdelrahman mohamed. All rights reserved.
//

import Foundation
//import CommonCrypto

struct Encryption {
    static func sha256Hex(string: String) -> String? {
        guard let messageData = string.data(using: String.Encoding.utf8) else { return nil }
        var digestData = Data(count: Int(CC_SHA256_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_SHA256(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
//        digestData.withUnsafeMutableBytes { (digestBytes: UnsafeMutableRawBufferPointer)->Void in
//                   messageData.withUnsafeBytes { (messageBytes: UnsafeRawBufferPointer)->Void in
//                       CC_MD5(messageBytes.baseAddress, CC_LONG(messageData.count), digestBytes.bindMemory(to: UInt8.self).baseAddress)
//                   }
//               }
        
        return digestData.map { String(format: "%02hhx", $0) }.joined()
    }
    
    static func ccSha256(data: Data) -> Data {
        var digest = Data(count: Int(CC_SHA256_DIGEST_LENGTH))
        
        _ = digest.withUnsafeMutableBytes { (digestBytes) in
            data.withUnsafeBytes { (stringBytes) in
                CC_SHA256(stringBytes, CC_LONG(data.count), digestBytes)
            }
        }
        
//        digest.withUnsafeMutableBytes { (digestBytes: UnsafeMutableRawBufferPointer)->Void in
//            data.withUnsafeBytes { (messageBytes: UnsafeRawBufferPointer)->Void in
//                CC_MD5(messageBytes.baseAddress, CC_LONG(data.count), digestBytes.bindMemory(to: UInt8.self).baseAddress)
//            }
//        }
        
        return digest
    }
}
