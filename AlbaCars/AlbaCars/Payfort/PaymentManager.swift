//
//  PaymentManager.swift
//  AlbaCars
//
//  Created by Narendra on 2/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//
import UIKit
import Foundation

enum PaymentManagerType {
    case payWithNewCard
    case payWithAddedCard
}

class PaymentManager {
    
    //MARK: - Variables
    private var paymentStatusCompletion: ((Bool,String) -> Void)?
    private var viewController: UIViewController?
    private var paymentManagerType: PaymentManagerType = .payWithNewCard
    private var isProduction: Bool = false
    private var savedToken: String = ""

    // MARK: - Singleton Instantiation
    private static let _sharedInstance: PaymentManager = PaymentManager()
    static var sharedInstance: PaymentManager {
        return ._sharedInstance
    }
    
    func initiatePayment(paymentDetailsData:PaymentDetailsData, completion: @escaping (Bool,String)-> Void) {
        
        paymentStatusCompletion = completion
        self.viewController = paymentDetailsData.viewController
        
        let viewModel: PaymentViewModeling = PaymentViewM()
        viewModel.requestCardListAPI() { (responseData) in
            
            if let data = responseData as? [CardList], data.count > 0 {
                
                let paymentVC = DIConfigurator.sharedInstance.getPaymentVC()
                paymentVC.isSideMenu = false
                paymentVC.paymentDataSource = data
                paymentVC.completionBlock = { token in
                    self.savedToken = token
                    self.initiate(paymentDetailsData: paymentDetailsData)
                }
                let navC = UINavigationController(rootViewController: paymentVC)
                self.viewController?.present(navC, animated: true, completion: nil)
                
            } else {
                self.initiate(paymentDetailsData: paymentDetailsData)
            }
        }

    }
    
    private func initiate(paymentDetailsData:PaymentDetailsData) {
        
        let vat = Helper.toDouble(UserDefaultsManager.sharedInstance.getDoubleValueFor(key:.vatPercentage))
        
        var vatAmmount = Helper.calculateVat(vatPercentage:Helper.toDouble(vat), bidAmount: paymentDetailsData.totalAmmount)
        var totalAmmount = paymentDetailsData.totalAmmount + vatAmmount
        
        if paymentDetailsData.paymentServiceCategoryId.rawValue == PaymentServiceCategoryId.registration.rawValue {
            
            totalAmmount = paymentDetailsData.totalAmmount
            vatAmmount = 0
        }
        
        var params = APIParams()
        params[ConstantAPIKeys.totalAmmount] = Helper.toString(object:totalAmmount) as AnyObject
        params[ConstantAPIKeys.paymentType] = "online" as AnyObject
        params[ConstantAPIKeys.vat] = vatAmmount as AnyObject
        params[ConstantAPIKeys.serviceCategoryId] = Helper.toString(object:paymentDetailsData.paymentServiceCategoryId.rawValue) as AnyObject
        params[ConstantAPIKeys.referenceId] = Helper.toString(object:paymentDetailsData.referenceId) as AnyObject
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .initiatePayment(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let result = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject],
                    let resultData = result.toJSONData() {
                    do {
                        let decoder = JSONDecoder()
                        let initiatePaymentModel = try decoder.decode(InitiatePaymentModel.self, from: resultData)
                        if let token = initiatePaymentModel.token_id {
                           // Loader.showLoader()
                            self.showPaymentController(totalAmmount: totalAmmount, initiateToken: token, paymentID: Helper.toInt(initiatePaymentModel.id))
                        }
                        
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }
    
    func updatePayment(paymentId:String,paymentStatus:PaymentStatus, cardNumber:String = "", paymentToken:String = "") {
        
        var params = APIParams()
        params[ConstantAPIKeys.paymentId] = paymentId as AnyObject
        params[ConstantAPIKeys.status] = Helper.toString(object: paymentStatus.rawValue) as AnyObject
        params[ConstantAPIKeys.cardNumber] = cardNumber as AnyObject
        if paymentToken != ""{
            params[ConstantAPIKeys.paymentToken] = paymentToken as AnyObject
        }
        
        APIClient.sharedInstance.request(apiRouter: APIRouter.init(endpoint: .updatePayment(param: params))) { (response, success) in
            if success {
                if let safeResponse =  response as? [String: AnyObject],
                    let _ = safeResponse[ConstantAPIKeys.result] as? [String: AnyObject]
                {
                    switch paymentStatus {
                    case .success:
                        self.paymentStatusCompletion?(success,paymentId)
                    //self.showError(sub: "Payment successful")
                    case .failed:
                        self.showError(sub: "Payment failed")
                    case .cancelled:
                        self.showError(sub: "Payment cancelled")
                    default:
                        print("")
                    }
                }
            }
        }
    }
    
    func showPaymentController(totalAmmount:Double,initiateToken:String,paymentID: Int) {
        
       // Loader.showLoader()
        
        self.viewController?.view.isUserInteractionEnabled = false
        
        if self.isProduction {
            
            guard let payFortController = PayFortController(enviroment: KPayFortEnviromentProduction) else { return }
            
            PayFortCredintials.production(udid: payFortController.getUDID()!).send(PayfortResponse.self) { (results) in
                
                switch results {
                case .success(let key):
                    
                    if let token = key.sdkToken {
                        self.ShowPayfort(controller: payFortController, with: CurrentOrder(orderTotalSar: Double(totalAmmount), initiateToken: initiateToken, paymentID: paymentID), token: token)
                    }else {
                        Loader.hideLoader()

                    }
                case .failure(let error):
                    Loader.hideLoader()

                    break
                }
            }
            
        } else {
            
            guard let payFortController = PayFortController(enviroment: KPayFortEnviromentSandBox) else { return }
            
            PayFortCredintials.development(udid: payFortController.getUDID()!).send(PayfortResponse.self) { (results) in
                
                switch results {
                case .success(let key):
                    
                    if let token = key.sdkToken {
                        
                        self.ShowPayfort(controller: payFortController, with: CurrentOrder(orderTotalSar: Double(totalAmmount), initiateToken: initiateToken, paymentID: paymentID), token: token)
                    }else {
                        Loader.hideLoader()
                    }
                case .failure(let error):
                    Loader.hideLoader()

                    break
                }
            }
            
        }
        
    }
    
    func ShowPayfort(controller: PayFortController, with order: CurrentOrder, token sdkToken: String) {
        Loader.hideLoader()
        self.viewController?.view.isUserInteractionEnabled = true
        let request = NSMutableDictionary()
        let updatedAmount: Float = Float(order.orderTotalSar * 100)
        request.setValue(updatedAmount, forKey: "amount")
        request.setValue("PURCHASE", forKey: "command")//PURCHASE - AUTHORIZATION
        request.setValue("AED", forKey: "currency")
        request.setValue(Helper.toString(object: UserSession.shared.email) , forKey: "customer_email")
        request.setValue("en", forKey: "language")
        request.setValue(order.initiateToken, forKey: "merchant_reference")
        request.setValue(sdkToken, forKey: "sdk_token")
        
        if !savedToken.isEmpty {
            request.setValue(savedToken, forKey: "token_name")
        }

        Threads.performTaskInMainQueue {
            
            controller.callPayFort(withRequest: request,
                                   currentViewController: self.viewController,
                                   success: { (_, response) in
                                    
                                    let cardNumber = Helper.toString(object: response?[AnyHashable("card_number")])
                                    let tokenName = Helper.toString(object: response?[AnyHashable("token_name")])
                                    //let tokenName = Helper.toString(object: response?[AnyHashable("sdk_token")])
                                    self.updatePayment(paymentId: Helper.toString(object: order.paymentID), paymentStatus: PaymentStatus.success, cardNumber:cardNumber, paymentToken: tokenName)
                                    
            }, canceled: { (request, response) in
                self.updatePayment(paymentId: Helper.toString(object: order.paymentID), paymentStatus: PaymentStatus.cancelled)
            }, faild: { (requst, response, message) in
                self.showError(sub: "Failed, Please try again")
                //self.updatePayment(paymentId: Helper.toString(object: order.paymentID), paymentStatus: PaymentStatus.failed)
            })
            
        }
        
    }
    
    func showError(sub: Any?) {
        
        if let message =  sub as? String, !message.isEmpty  {
            Alert.showOkAlert(title: StringConstants.Text.AppName, message: message)
        }
    }
}

struct CurrentOrder {
    var orderTotalSar: Double
    var initiateToken: String
    var paymentID: Int
}

struct PaymentDetailsData {
    
    var viewController: UIViewController!
    var totalAmmount:Double!
    var referenceId:Int!
    var paymentServiceCategoryId:PaymentServiceCategoryId!
    var vatAmmount:Double
    
    init(viewController: UIViewController, totalAmmount:Double, referenceId:Int, paymentServiceCategoryId:PaymentServiceCategoryId,vatAmmount:Double = 0.0) {
        
        self.viewController = viewController
        self.totalAmmount = totalAmmount
        self.referenceId = referenceId
        self.paymentServiceCategoryId = paymentServiceCategoryId
        self.vatAmmount = vatAmmount
    }
    
}
