//
//  InitiatePaymentModel.swift
//  AlbaCars
//
//  Created by Narendra on 2/28/20.
//  Copyright © 2020 Appventurez. All rights reserved.
//

import Foundation

class InitiatePaymentModel: Codable {
    
    // Variables
    var id: Int?
    var user_id: Int?
    var total_amount: String?
    var service_category_id: String?
    var reference_id: String?
    var payment_status: String?
    var token_id: String?
    
}
