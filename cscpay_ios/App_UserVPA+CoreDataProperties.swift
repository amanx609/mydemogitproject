//
//  App_UserVPA+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Aman Pandey on 30/07/21.
//
//

import Foundation
import CoreData


extension App_UserVPA {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<App_UserVPA> {
        return NSFetchRequest<App_UserVPA>(entityName: "App_UserVPA")
    }

    @NSManaged public var vpaSuggestion: String?

}

extension App_UserVPA : Identifiable {

}
