//
//  Transaction_Response+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Aman Pandey on 08/08/21.
//
//

import Foundation
import CoreData


extension Transaction_Response {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Transaction_Response> {
        return NSFetchRequest<Transaction_Response>(entityName: "Transaction_Response")
    }

    @NSManaged public var encKey: String?
    @NSManaged public var isMerchant: Bool
    @NSManaged public var pspRefNo: String?
    @NSManaged public var resHash: String?
    @NSManaged public var status: String?
    @NSManaged public var statusDesc: String?
    @NSManaged public var transactionHistroy: NSSet?

}

// MARK: Generated accessors for transactionHistroy
extension Transaction_Response {

    @objc(addTransactionHistroyObject:)
    @NSManaged public func addToTransactionHistroy(_ value: Transaction_History)

    @objc(removeTransactionHistroyObject:)
    @NSManaged public func removeFromTransactionHistroy(_ value: Transaction_History)

    @objc(addTransactionHistroy:)
    @NSManaged public func addToTransactionHistroy(_ values: NSSet)

    @objc(removeTransactionHistroy:)
    @NSManaged public func removeFromTransactionHistroy(_ values: NSSet)

}

extension Transaction_Response : Identifiable {

}
