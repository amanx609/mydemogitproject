//
//  BaseViewController.swift
//  VLE Survey
//
//  Created by Gowtham on 15/10/20.
//  Copyright © 2020 CSC eGovernance. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit

class BaseViewController : UIViewController, LocationUpdateProtocol {
    
    let LocationMgr = UserLocationManager.SharedManager
    var location = [String : String]()
    var geo_lat :String?
    var geo_long :String?
    var userLocation :CLLocation?
    var showAlert : Bool = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //NotificationCenter.default.addObserver(self, selector: #selector(locationUpdateNotification), name: NSNotification.Name(rawValue: kLocationDidChangeNotification), object: nil)
        if showAlert {
            LocationMgr.delegate = self
            LocationMgr.getUserLocation() }
    }
    func showLocationEnableAlert(msg : String = "Enable Location from your device") {
        var messageString = msg
        if (UD.SharedManager.getValues(key: Constants.LOCATION_ALERT, type: .bool) as? Bool ?? false) {
            messageString = Constants.alert_enable_location_register
        }
        self.displayFailiureAlert(msg : messageString, isSuccess: false, isFailiure: false, isWarning : true,buttonText : "Close", isClose : true, tag : 0, isDelagete : true, enableExtraButton : true, tag1: 10, tag2 : 20,isClose2 : false, btn1Text :  "  Close  ", btn2Text : "  Ok  ",isBase : true)
        /*
        Alert.showAlert(title: Constants.ALERT_HEADER, message: "Enable Location from your device", cancelTitle: "Cancel", otherTitle: "Settings") { (index) in
            if index == 1 {
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }

                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        //print("Settings opened: \(success)") // Prints true
                    })
                }
            }
        }
        */
    }
    
    func updateStatus(status: CLAuthorizationStatus) {
        print("Status will show \(status)")
        switch status {
        case .restricted, .denied:
            showLocationEnableAlert()
        case .authorizedAlways, .authorizedWhenInUse, .notDetermined :
            LocationMgr.getUserLocation()
        @unknown default:
            break
        }
    }
    
    // MARK: - Notifications
    @objc func locationUpdateNotification(notification: NSNotification) {
        let userinfo = notification.userInfo
        if let currentLocation = userinfo!["location"] as? CLLocation {
            print("Latitude : \(String(describing: currentLocation.coordinate.latitude))")
            print("Longitude : \(String(describing: currentLocation.coordinate.longitude))")
            self.geo_lat = String(format: "%.04f", currentLocation.coordinate.latitude)
            self.geo_long = String(format: "%.4f", currentLocation.coordinate.longitude)
            self.userLocation = currentLocation
            //geocodeReverse(userLocation : currentLocation)
        }
    }
    // MARK: - LocationUpdateProtocol

    func locationDidUpdateToLocation(location: CLLocation) {
        //currentLocation = location
         print("Latitude : \(location.coordinate.latitude)")
        print("Longitude : \(location.coordinate.longitude)")
        self.geo_lat = String(format: "%.04f", location.coordinate.latitude)
        self.geo_long = String(format: "%.4f", location.coordinate.longitude)
        self.userLocation = location
        geocodeReverse(userLocation : location)
    }
    
    func geocodeReverse(userLocation : CLLocation) {
        var resp = [String : String]()
        location = [String : String]()
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation, completionHandler: { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            guard let placemarks = placemarks else {
                return
            }
            let placemarkOut = placemarks as [CLPlacemark]
            if placemarkOut.count>0{
                let placemark = placemarks[0]
                if let name = placemark.name {
                    resp["name"] = name
                }
                if let thoroughfare = placemark.thoroughfare {
                    resp["thoroughfare"] = thoroughfare
                }
                if let subThoroughfare = placemark.subThoroughfare {
                    resp["subThoroughfare"] = subThoroughfare
                }
                if let locality = placemark.locality {
                    resp["locality"] = locality
                }
                if let subLocality = placemark.subLocality {
                    resp["subLocality"] = subLocality
                }
                if let subAdministrativeArea = placemark.subAdministrativeArea {
                    resp["subAdministrativeArea"] = subAdministrativeArea
                }
                if let postalCode = placemark.postalCode {
                    resp["postalCode"] = postalCode
                }
                if let isoCountryCode = placemark.isoCountryCode {
                    resp["isoCountryCode"] = isoCountryCode
                }
                if let country = placemark.country {
                    resp["country"] = country
                }
                if let inlandWater = placemark.inlandWater {
                    resp["inlandWater"] = inlandWater
                }
                if let ocean = placemark.ocean {
                    resp["ocean"] = ocean
                }
                if let areasOfInterest = placemark.areasOfInterest {
                    for area in areasOfInterest {
                        if resp["areasOfInterest"] == nil {
                            resp["areasOfInterest"] = area
                        } else {
                            resp["areasOfInterest"]! += area }
                    }
                }
                resp["latitude"] = String(format: "%.04f", userLocation.coordinate.latitude)
                resp["longitude"] = String(format: "%.04f", userLocation.coordinate.longitude)
                
                Constants.geolat = String(format: "%.04f", userLocation.coordinate.latitude)
                Constants.geolong = String(format: "%.04f", userLocation.coordinate.longitude)
                
                if resp.count > 2 {
                    //UserDefaults.standard.set(resp, forKey: "Location")
                }
                self.location = resp
                
                print(resp)
            }
        })
    }
}
/*
extension BaseViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
            moveTextField(textField, moveDistance: -250, up: true)
        }

        // Finish Editing The Text Field
        func textFieldDidEndEditing(_ textField: UITextField) {
            moveTextField(textField, moveDistance: -250, up: false)
        }

        // Hide the keyboard when the return key pressed
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }

        // Move the text field in a pretty animation!
        func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
            let moveDuration = 0.3
            let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
            UIView.animate(withDuration: moveDuration, animations: {
            }) { _ in
                self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
            }
        }
} */

 extension BaseViewController : BaseMessageDelegate {
     func baseokButtonTapped(tag: Int) {
         //Do something
        if tag == 12 {
            self.dismiss(animated: true, completion: {
                            self.dismiss(animated: true, completion: nil)})
        }
     }
     func basebtn1ButtonTapped(tag : Int) {
         //Do nothing
     }
     func basebtn2ButtonTapped(tag : Int) {
         //print(tag)
         self.dismiss(animated: true, completion: {
             guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                 return
             }

             if UIApplication.shared.canOpenURL(settingsUrl) {
                 UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                     //print("Settings opened: \(success)") // Prints true
                 })
             }
         })
     }
    
     func displayFailiureAlert(msg : String, isSuccess: Bool = false, isFailiure: Bool = true, isWarning : Bool = false,buttonText : String = "  Close  ", isClose : Bool = true, tag : Int = 0, isDelagete : Bool = true, enableExtraButton : Bool = false, tag1: Int = 1, tag2 : Int = 2,isClose2 : Bool = true, btn1Text : String = "Close", btn2Text : String = "Ok",isBase : Bool = true) {
         DispatchQueue.main.async {
             let vc = presentFailiureVC(vc: AlertMessageViewController(), msg: msg , isWarning : isWarning, isFailiure: isFailiure, isSuccess: isSuccess, buttonText: buttonText, isClose: isClose, tag: tag, img: nil, enableExtraButton : enableExtraButton, tag1: tag1, tag2 : tag2,isClose2 : isClose2, btn1Text : btn1Text, btn2Text : btn2Text,isBase : isBase)
             if isDelagete {
                 vc.delegate1 = self
             }
             self.present(vc, animated: true, completion: nil)
         }
         //self.displayFailiureAlert(msg: Constants.NO_NEW_MESSAGE)
     }
 }

extension UIViewController{
    func geocodeReverseVC(userLocation : CLLocation) -> [String:String]{
        var resp = [String : String]()
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation, completionHandler: { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            guard let placemarks = placemarks else {
                return
            }
            let placemarkOut = placemarks as [CLPlacemark]
            if placemarkOut.count>0{
                let placemark = placemarks[0]
                if let name = placemark.name {
                    resp["name"] = name
                }
                if let thoroughfare = placemark.thoroughfare {
                    resp["thoroughfare"] = thoroughfare
                }
                if let subThoroughfare = placemark.subThoroughfare {
                    resp["subThoroughfare"] = subThoroughfare
                }
                if let locality = placemark.locality {
                    resp["locality"] = locality
                }
                if let subLocality = placemark.subLocality {
                    resp["subLocality"] = subLocality
                }
                if let subAdministrativeArea = placemark.subAdministrativeArea {
                    resp["subAdministrativeArea"] = subAdministrativeArea
                }
                if let postalCode = placemark.postalCode {
                    resp["postalCode"] = postalCode
                }
                if let isoCountryCode = placemark.isoCountryCode {
                    resp["isoCountryCode"] = isoCountryCode
                }
                if let country = placemark.country {
                    resp["country"] = country
                }
                if let inlandWater = placemark.inlandWater {
                    resp["inlandWater"] = inlandWater
                }
                if let ocean = placemark.ocean {
                    resp["ocean"] = ocean
                }
                if let areasOfInterest = placemark.areasOfInterest {
                    for area in areasOfInterest {
                        if resp["areasOfInterest"] == nil {
                            resp["areasOfInterest"] = area
                        } else {
                            resp["areasOfInterest"]! += area }
                    }
                }
                resp["latitude"] = String(format: "%.04f", userLocation.coordinate.latitude)
                resp["longitude"] = String(format: "%.04f", userLocation.coordinate.longitude)
                if resp.count > 2 {
                    //UserDefaults.standard.set(resp, forKey: "Location")
                }
                print(resp)
            }
        })
        return resp
    }
}
