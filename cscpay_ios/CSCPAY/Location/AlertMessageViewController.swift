//
//  AlertMessageViewController.swift
//  MLink
//
//  Created by madept on 18/01/19.
//  Copyright © 2019 madept. All rights reserved.
//

import UIKit

protocol AlertMessageDelegate {
    func okButtonTapped(tag : Int)
    func btn1ButtonTapped(tag : Int)
    func btn2ButtonTapped(tag : Int)
}
protocol BaseMessageDelegate {
    func baseokButtonTapped(tag : Int)
    func basebtn1ButtonTapped(tag : Int)
    func basebtn2ButtonTapped(tag : Int)
}

class AlertMessageViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var imageViewHeighConstraint: NSLayoutConstraint!
    @IBOutlet weak var extraButtonsView: UIView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    
    var delegate: AlertMessageDelegate?
    var delegate1: BaseMessageDelegate?
    var message: String = ""
    var head:String = ""
    var buttonText:String = "  Close  "
    var btn1Text:String = "  Close  "
    var btn2Text:String = "  Ok  "
    var img:UIImage?
    var imgTintColor : UIColor = .blue
    var close : Bool = true
    var successMsg : Bool = false
    var failiureMsg : Bool = false
    var warningMsg : Bool = false
    var close2 : Bool = true
    var tag = 0
    var tag1 = 1
    var tag2 = 2
    var haveExtraButton : Bool = false
    var isBase : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        //contentView.layer.cornerRadius = 20
        //contentView.layer.borderWidth = 2
        messageLabel.text = message
        if message == Constants.alert_enable_location_register {
            messageLabel.halfTextColorChange2(fullText: Constants.alert_enable_location_register, changeText: Constants.alert_enable_location_register_change, color: .systemRed, textSize: 10, extra: true, extraText: Constants.alert_enable_location_register_title)
        }
        titleLabel.text = head
        okButton.setTitle(buttonText, for: .normal)
        okButton.tag = tag
        btn1.setTitle(btn1Text, for: .normal)
        btn1.tag = tag1
        btn2.setTitle(btn2Text, for: .normal)
        btn2.tag = tag2
        if img != nil {
            logoImageView.image = img
        }else {
            logoImageView.image = nil
        }
        logoImageView.tintColor = imgTintColor
        if successMsg {
            logoImageView.image = UIImage.init(systemName: "checkmark.circle")
            logoImageView.tintColor = UIColor.systemGreen
        }else if failiureMsg {
            logoImageView.image = UIImage.init(systemName: "xmark.circle")
            logoImageView.tintColor = UIColor.systemRed
        }else if warningMsg {
            logoImageView.image = UIImage.init(systemName: "info.circle")
            logoImageView.tintColor = UIColor.systemOrange
        }
        if haveExtraButton {
            extraButtonsView.isHidden = false
            okButton.isHidden = true
        } else {
            extraButtonsView.isHidden = true
            okButton.isHidden = false
        }
    }


    @IBAction func okButtonClicked(_ sender: UIButton) {
        if close {
            self.dismiss(animated: true, completion: nil)
            return
        }
        if isBase {
            self.delegate1?.baseokButtonTapped(tag : self.tag)
        } else {
            self.delegate?.okButtonTapped(tag : self.tag)
        }
    }
        
    @IBAction func btn1Tapped(_ sender: UIButton) {
        if close {
            self.dismiss(animated: true, completion: nil)
            return
        }
        if isBase {
            self.delegate1?.basebtn1ButtonTapped(tag : self.tag)
        } else {
            self.delegate?.btn1ButtonTapped(tag : self.tag)
        }
    }
    @IBAction func btn2Tapped(_ sender: UIButton) {
        if close2 {
            self.dismiss(animated: true, completion: nil)
            return
        }
        if isBase {
            self.delegate1?.basebtn2ButtonTapped(tag : self.tag)
        } else {
            self.delegate?.btn2ButtonTapped(tag : self.tag)
        }
    }
}
func presentFailiureVC(vc : AlertMessageViewController, msg : String, header: String = "", isWarning: Bool = false , isFailiure : Bool = false, isSuccess : Bool = false, buttonText : String = "Close", isClose : Bool = true,tag : Int = 0, img : UIImage?, enableExtraButton : Bool = false, tag1: Int = 1, tag2 : Int = 2,isClose2 : Bool = true, btn1Text : String = "Close", btn2Text : String = "Ok", isBase : Bool = false) -> AlertMessageViewController {
    vc.message = msg // "No new message, Please try later."
    vc.failiureMsg = isFailiure //true
    vc.successMsg = isSuccess
    vc.warningMsg = isWarning
    vc.img = img
    vc.head = header // "WHOOPS!!!"
    vc.buttonText = buttonText
    vc.close = isClose
    vc.tag = tag
    vc.tag1 = tag1
    vc.tag2 = tag2
    vc.close2 = isClose2
    vc.haveExtraButton = enableExtraButton
    vc.isBase = isBase
    vc.modalPresentationStyle = .overCurrentContext
    return vc
}
