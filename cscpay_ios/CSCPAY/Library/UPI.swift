//
//  UPI.swift
//  CSCPAY
//
//  Created by Gowtham on 06/07/21.
//

import Foundation
import CommonLibrary

class UPI {
    static func getChallenge() -> NSString? {
        
        var previous_date : Date!
        var updated_Date : String?
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let user = CoredataManager.getDetail(App_Config.self) , let isoDate = user.lastUpdated{
            if isoDate != ""{
                previous_date = dateFormatter.date(from: isoDate)
            }else{
                previous_date = dateFormatter.date(from: "2021-09-09 16:02:01")
            }
        }
        var a : NSString?

        
        if CoredataManager.getDetail(List_Key.self) != nil{
            let interval = previous_date.timeIntervalSince(Date())
            if abs(interval) > 2592000 {
                var success = false
                do {
                    try CLServices.getChallengeForDeviceId(
                        getUUID() ?? "na",
                            appId: "in.cscpay.upi",
                            type: "Rotate",
                            challenge: &a)
                    success = true
                } catch {
                    print(error)
                }
                
                updated_Date = dateFormatter.string(from: Date())
                CoredataManager.updateDetail(App_Config.self, key: "lastUpdated", value: updated_Date ?? "")
                CoredataManager.updateDetail(List_Key.self, key: "keyValue", value: a! as String)
            }else{
                a = CoredataManager.getDetail(List_Key.self)?.keyValue as? NSString
            }
            return a
        }else{
            do {
                let _: () =  try CLServices.getChallengeForDeviceId(getUUID() ?? "na", appId: "in.cscpay.upi", type: "Initial", challenge: &a)
            } catch {
                print(error)
            }
            CoredataManager.updateDetail(List_Key.self, key: "keyValue", value: a! as String)
            return a
        }
        
        
        
        
//        var a : NSString?
//        do {
//            let _: () =  try CLServices.getChallengeForDeviceId(getUUID() ?? "na", appId: "in.cscpay.upi", type: "Initial", challenge: &a)
//        } catch {
//            print(error)
//        } ////initial
//        let interval = previous_date.timeIntervalSinceNow
//        if interval > 2592000 {
//            var success = false
//            do {
//                try CLServices.getChallengeForDeviceId(
//                    getUUID() ?? "na",
//                        appId: "in.cscpay.upi",
//                        type: "Rotate",
//                        challenge: &a)
//                success = true
//            } catch {
//            }
//        }
//        return a

        
        
        
        
        
        /*
        var a : NSString?
        do {
            let _: () = try CLServices.getChallengeForDeviceId(getUUID() ?? "na", appId: "in.cscpay.upi", type: "Initial", challenge: &a)
            } catch {
                print(error)
            }
        return a
         */
    }
    
    static func registerAppHmac(withHmac: String = "NA" , appID: String = "in.cscpay.upi") -> Bool?{
        guard let user = CoredataManager.getDetail(App_User.self) else {
            return false
        }
        do {
            let _: () = try CLServices.registerApp(withHmac: withHmac,appID: appID,mobile: user.mobile,deviceID: getUUID() ?? "NA")
            return true
        } catch{
            print(error)
            return false
        }
    }
}
