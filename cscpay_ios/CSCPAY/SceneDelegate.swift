//
//  SceneDelegate.swift
//  CSCPAY
//
//  Created by Gowtham on 03/05/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var imageview : UIImageView?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        switch getDeviceStatus() {
        case .current:
            presentAlert()
            break
        case .fine:
            presentAlert()
            break
        case .used:
            break
        case .illegal:
            presentAlert()
            break
        case .simulator:
            presentAlert()
            break
        case .notInUse:
            presentAlert()
            break
        }
            if #available(iOS 11.0, *) {
                if !UIScreen.main.isCaptured {
                    DispatchQueue.main.async {
                        //self.blockImageView.removeFromSuperview()
                    }
                }
            }
        if (imageview != nil){
            imageview?.removeFromSuperview()
            imageview = nil
        }
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
//        imageview = UIImageView.init(image: UIImage.init(named: "bg_splash"))
//        self.window?.addSubview(imageview!)
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        //(UIApplication.shared.delegate as? AppDelegate)?.saveContext()
        AppDelegate.getAppDelegate().saveContext()
    }
    
    //Check device is unlocked or nor
    
    func presentAlert() {
        let alert = UIAlertController(title: "Error", message: "We found some security issue with this device. So you are not allowed to access this app.\n", preferredStyle: UIAlertController.Style.alert)
            let jailBrokenView = UIViewController()

            jailBrokenView.view.frame = UIScreen.main.bounds
            jailBrokenView.view.backgroundColor = .white
            self.window?.rootViewController = jailBrokenView
            jailBrokenView.present(alert, animated: true, completion: nil)
    }
    
    private func getDeviceStatus() -> Status {
        if Constants.isSimulator {
            return Status.used
        }
        // Check 1 : existence of files that are common for jailbroken devices
        if FileManager.default.fileExists(atPath: "/Applications/Cydia.app")
            || FileManager.default.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib")
            || FileManager.default.fileExists(atPath: "/bin/bash")
            || FileManager.default.fileExists(atPath: "/usr/sbin/sshd")
            || FileManager.default.fileExists(atPath: "/etc/apt")
            || FileManager.default.fileExists(atPath: "/private/var/lib/apt/")
            || UIApplication.shared.canOpenURL(URL(string:"cydia://package/com.example.package")!)
        {
            return Status.notInUse
        }
        // Check 2 : Reading and writing in system directories (sandbox violation)
        let stringToWrite = "Jailbreak Test"
        do {
            try stringToWrite.write(toFile:"/private/JailbreakTest.txt", atomically:true, encoding:String.Encoding.utf8)
            //Device is jailbroken
            return Status.notInUse
        } catch {
            return Status.used
        }
    }

}

