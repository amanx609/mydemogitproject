//
//  Crypt Operations.swift
//  VLE Survey
//
//  Created by Abhishek Ranjan on 05/10/20.
//  Copyright © 2020 CSC eGovernance. All rights reserved.
//

import Foundation
import CommonCrypto
import CryptoKit

class cryptOperatons {
    static func randomGenerateBytes(count: Int) -> Data? {
        let bytes = UnsafeMutableRawPointer.allocate(byteCount: count, alignment: 1)
        defer { bytes.deallocate() }
        let status = CCRandomGenerateBytes(bytes, count)
        guard status == kCCSuccess else { return nil }
        return Data(bytes: bytes, count: count)
    }
    static func encryptUsingGCM(plainText: String , key: Data) -> Data {
        let plainData = plainText.data(using: .utf8)
        let iv      = cryptOperatons.randomGenerateBytes(count: 12)!
        let keyData = SymmetricKey(data: key)
        let sealedData = try! AES.GCM.seal(plainData!, using: keyData, nonce: AES.GCM.Nonce(data:iv))
        let result = iv + sealedData.tag + sealedData.ciphertext
        return result
    }
    static func decryptUsingGCM(encStr: Data,key : Data) -> String {
        let iv         = encStr.prefix(12)
        let tagPreview  = encStr.dropFirst(12)
        let tag        = tagPreview.prefix(16)
        let ciphertext        = tagPreview.dropFirst(16)
        
        let keyD = SymmetricKey(data: key)
        let sealedBox = try! AES.GCM.SealedBox(nonce: AES.GCM.Nonce(data:iv),
                                               ciphertext: ciphertext,
                                               tag: tag)

        let decryptedData = try! AES.GCM.open(sealedBox, using: keyD)
        return String(decoding: decryptedData, as: UTF8.self)
    }
    static func crypt(operation: Int, algorithm: Int, options: Int, key: Data,
            initializationVector: Data, dataIn: Data) -> Data? {
        return key.withUnsafeBytes { keyUnsafeRawBufferPointer in
            return dataIn.withUnsafeBytes { dataInUnsafeRawBufferPointer in
                return initializationVector.withUnsafeBytes { ivUnsafeRawBufferPointer in
                    // Give the data out some breathing room for PKCS7's padding.
                    let dataOutSize: Int = dataIn.count + kCCBlockSizeAES128*2
                    let dataOut = UnsafeMutableRawPointer.allocate(byteCount: dataOutSize,
                        alignment: 1)
                    defer { dataOut.deallocate() }
                    var dataOutMoved: Int = 0
                    let status = CCCrypt(CCOperation(operation), CCAlgorithm(algorithm),
                        CCOptions(options),
                        keyUnsafeRawBufferPointer.baseAddress, key.count,
                        ivUnsafeRawBufferPointer.baseAddress,
                        dataInUnsafeRawBufferPointer.baseAddress, dataIn.count,
                        dataOut, dataOutSize, &dataOutMoved)
                    guard status == kCCSuccess else { return nil }
                    return Data(bytes: dataOut, count: dataOutMoved)
                }
            }
        }
    }
}




//MARK: - GCM Enc Reference :
/*
let encryptedContent = try! sealedData.combined!

let nonce = (sealedData.nonce.withUnsafeBytes { Data(Array($0)).hexadecimal })
let tag = sealedData.tag //.hexadecimal
let dataT = sealedData.ciphertext.base64EncodedString()

print("Nonce: \(sealedData.nonce.withUnsafeBytes { Data(Array($0)).hexadecimal })")
print("Tag: \(sealedData.tag.hexadecimal)")
print("Data: \(sealedData.ciphertext.base64EncodedString())")

//let a = decryptUsingGCM(ciphertext: dataT, key: keyData, nonce: nonce, tag: tag)
 
 
 
 static func cryptoGCMCall(plainText: String , key: String, iv:String, tag: String, taglen: String) {
     
     if #available(iOS 13.0, *) {
         let key = SymmetricKey(size: .bits256)
         print("Key32:\n\(key.withUnsafeBytes { Data(Array($0)).base64EncodedString() })\n")
     } else {
         // Fallback on earlier versions
     }
     

     if #available(iOS 13.0, *) {
         cryptOperatons.cryptoDemoCombinedData(plainText: plainText, keyStr: key)
     } else {
         // Fallback on earlier versions
     }
     //cryptOperatons.cryptoDemoCipherText()
 }
 
 static func decryptUsingGCM1(ciphertext: String,key : Data, nonce : String, tag : String ) -> String {
     let keyD = SymmetricKey(data: key)
     let ciphertextD = Data(base64Encoded: ciphertext)!
     let nonceD = Data(base64Encoded: nonce)! // = initialization vector
     let tagD = Data(base64Encoded: tag)!
     
     let sealedBox = try! AES.GCM.SealedBox(nonce: AES.GCM.Nonce(data: nonceD),
                                            ciphertext: ciphertextD,
                                            tag: tagD)

     let decryptedData = try! AES.GCM.open(sealedBox, using: keyD)
     print(String(decoding: decryptedData, as: UTF8.self))
     return String(decoding: decryptedData, as: UTF8.self)
 }
 
 
 static func crypt(operation: Int, algorithm: Int, options: Int, key: Data,
         initializationVector: Data, dataIn: Data) -> Data? {
     return key.withUnsafeBytes { keyUnsafeRawBufferPointer in
         return dataIn.withUnsafeBytes { dataInUnsafeRawBufferPointer in
             return initializationVector.withUnsafeBytes { ivUnsafeRawBufferPointer in
                 // Give the data out some breathing room for PKCS7's padding.
                 let dataOutSize: Int = dataIn.count + kCCBlockSizeAES128*2
                 let dataOut = UnsafeMutableRawPointer.allocate(byteCount: dataOutSize,
                     alignment: 1)
                 defer { dataOut.deallocate() }
                 var dataOutMoved: Int = 0
                 let status = CCCrypt(CCOperation(operation), CCAlgorithm(algorithm),
                     CCOptions(options),
                     keyUnsafeRawBufferPointer.baseAddress, key.count,
                     ivUnsafeRawBufferPointer.baseAddress,
                     dataInUnsafeRawBufferPointer.baseAddress, dataIn.count,
                     dataOut, dataOutSize, &dataOutMoved)
                 guard status == kCCSuccess else { return nil }
                 return Data(bytes: dataOut, count: dataOutMoved)
             }
         }
     }
 }
 
 /// Encrypt: Using plain text only • nonce & tag are randomly created
 /// Decrypt: Specify all 3 parameters: nonce + cipher text + tag
 @available(iOS 13.0, *)
 static func cryptoDemoCipherText(plainText : String) {
     let key = SymmetricKey(size: .bits256)
     // Encrypt
     let sealedBox = try! AES.GCM.seal(plainText.data(using: .utf8)!, using: key)

     // Decrypt
     let sealedBoxRestored = try! AES.GCM.SealedBox(nonce: sealedBox.nonce, ciphertext: sealedBox.ciphertext, tag: sealedBox.tag)
     let decrypted = try! AES.GCM.open(sealedBoxRestored, using: key)

     print("Crypto Demo I\n••••••••••••••••••••••••••••••••••••••••••••••••••\n")
     print("Combined:\n\(sealedBox.combined!.base64EncodedString())\n")
     print("Cipher:\n\(sealedBox.ciphertext.base64EncodedString())\n")
     print("Nonce:\n\(sealedBox.nonce.withUnsafeBytes { Data(Array($0)).base64EncodedString() })\n")
     print("Tag:\n\(sealedBox.tag.base64EncodedString())\n")
     print("Decrypted:\n\(String(data: decrypted, encoding: .utf8)!)\n")
 }

 /// Encrypt: Specify all 3 parameters yourself: nonce + cipher text + tag
 /// Decrypt: Using combined data (nonce + cipher text + tag) and tag to open
 @available(iOS 13.0, *)
 static func cryptoDemoCombinedData(plainText : String, keyStr: String) {
     let key = SymmetricKey(data: Data(hexString:keyStr)!)
     
     //let key = SymmetricKey(size: .bits256)
     let nonce = try! AES.GCM.Nonce(data: Data(base64Encoded: "fv1nixTVoYpSvpdA")!)
     let tag = Data(base64Encoded: "e1eIgoB4+lA/j3KDHhY4BQ==")!

     // Encrypt
     let sealedBox = try! AES.GCM.seal(plainText.data(using: .utf8)!, using: key, nonce: nonce, authenticating: tag)

     // Decrypt
     let sealedBoxRestored = try! AES.GCM.SealedBox(combined: sealedBox.combined!)
     let decrypted = try! AES.GCM.open(sealedBoxRestored, using: key, authenticating: tag)

     print("Crypto Demo II\n••••••••••••••••••••••••••••••••••••••••••••••••••\n")
     print("Combined:\n\(sealedBox.combined!.base64EncodedString())\n")
     print("Cipher:\n\(sealedBox.ciphertext.base64EncodedString())\n")
     print("Nonce:\n\(nonce.withUnsafeBytes { Data(Array($0)).base64EncodedString() })\n")
     print("Tag:\n\(tag.base64EncodedString())\n")
     print("Decrypted:\n\(String(data: decrypted, encoding: .utf8)!)\n")
 }
 
 */
