//
//  DatabaseManager.swift
//  CSCPAY
//
//  Created by Gowtham on 15/06/21.
//

import Foundation
import CoreData

class CoredataManager {
    static func saveContext(){
      let appDelegate =  AppDelegate.getAppDelegate()
        appDelegate.saveContext()
    }
    static func getContext() -> NSManagedObjectContext {
        let appDelegate = AppDelegate.getAppDelegate()
        let  managedObjectContext = appDelegate.persistentContainer.viewContext
        return managedObjectContext
    }
    
    static func doesExistAny<T: NSManagedObject>(_ entity: T.Type) -> Bool {
        let appDelegate =  AppDelegate.getAppDelegate()
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let entityName = String(describing: entity)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        //fetchRequest.predicate = NSPredicate(format: "\(key) == \(value)")
        //fetchRequest.returnsObjectsAsFaults = false
        //let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            let results = try (managedObjectContext.fetch(fetchRequest)) as? [T] ?? [T]()
            if results.count > 0 {
                return true
            } else {
                return false
            }
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    static func cleanEntity<T: NSManagedObject>(_ entity: T.Type) {
        let appDelegate =  AppDelegate.getAppDelegate()
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let entityName = String(describing: entity)
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try managedObjectContext.execute(deleteRequest)
            //return true
        } catch {
            print(error.localizedDescription)
            //return false
        }
        //return true
    }
    static func getDetails<T: NSManagedObject>(_ entity: T.Type, all : Bool = false) -> [T]? {
        let appDelegate =  AppDelegate.getAppDelegate()
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let entityName = String(describing: entity)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.returnsObjectsAsFaults = false
        //let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            let results = try (managedObjectContext.fetch(fetchRequest)) as? [T] ?? [T]()
            if results.count > 0 {
                return results
            } else {
                return nil
            }
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    static func returnBankCount() -> Int{
        if let count = CoredataManager.getDetails(Account_List.self)?.count{
            return count
        }else{
            return 0
        }
    }
    
    
    
    
    
    static func getDetail<T: NSManagedObject>(_ entity: T.Type, all : Bool = false) -> T? {
        let appDelegate =  AppDelegate.getAppDelegate()
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let entityName = String(describing: entity)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.returnsObjectsAsFaults = false
        //let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            let results = try (managedObjectContext.fetch(fetchRequest)) as? [T] ?? [T]()
            if results.count > 0 {
                return results[0]
            } else {
                return nil
            }
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    static func getALLDetailsByGroup<T: NSManagedObject>(_ entity: T.Type, sortBytTime: Bool = false, key: String? = "addedAt", ascending : Bool = false, recordCount: Int = 0, sortCaseSensitive: Bool = false, groupKey:String = "addedAt", fetchKey:[String] = ["from","id", "to", "toName", "type", "amount", "status", "remark", "emoji", "addedAt"]) -> NSArray? {
        let appDelegate =  AppDelegate.getAppDelegate()
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let entityName = String(describing: entity)
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        //request.returnsObjectsAsFaults = false
            request.propertiesToGroupBy = [groupKey]
            request.propertiesToFetch = ["addedAt"] // fetchKey
        request.resultType = .dictionaryResultType
        
        if sortBytTime {
            var sort = NSSortDescriptor(key: key, ascending: ascending)
            if sortCaseSensitive {
                sort = NSSortDescriptor(key: key, ascending: ascending, selector: #selector(NSString.caseInsensitiveCompare))
            }
            request.sortDescriptors = [sort]
        }
        
        do {
            let results = try (managedObjectContext.fetch(request)) as? NSArray // [T] ?? [T]()
            if results?.count ?? 0 > 0 {
                return results
            } else {
                return nil
            }
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    static func getALLDetails<T: NSManagedObject>(_ entity: T.Type, sortBytTime: Bool = false, key: String? = "addedAt", ascending : Bool = false, recordCount: Int = 0, sortCaseSensitive: Bool = false, key2: String? = nil) -> [T]? {
        let appDelegate =  AppDelegate.getAppDelegate()
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let entityName = String(describing: entity)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.returnsObjectsAsFaults = false
        if sortBytTime {
            var sort = NSSortDescriptor(key: key, ascending: ascending)
            if sortCaseSensitive {
                sort = NSSortDescriptor(key: key, ascending: ascending, selector: #selector(NSString.caseInsensitiveCompare))
            }
            if let key2 = key2 {
            let sort2 = NSSortDescriptor(key: key2, ascending: ascending, selector: #selector(NSString.caseInsensitiveCompare))
                fetchRequest.sortDescriptors = [sort, sort2]
            } else {
                fetchRequest.sortDescriptors = [sort]
            }
            
        }
        if recordCount > 0 {
            fetchRequest.fetchLimit = recordCount
        }
        //let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            let results = try (managedObjectContext.fetch(fetchRequest)) as? [T] ?? [T]()
            if results.count > 0 {
                return results
            } else {
                return nil
            }
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    static func updateDetail<T: NSManagedObject>(_ entity: T.Type, key: String, value: String, isSave : Bool = true) -> T? {
        let appDelegate =  AppDelegate.getAppDelegate()
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let entityName = String(describing: entity)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.returnsObjectsAsFaults = false
        //let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            let results = try (managedObjectContext.fetch(fetchRequest)) as? [T] ?? [T]()
            if results.count > 0 {
                let item = results[0]
                item.setValue(value, forKey: key)
                if isSave { saveContext() }
                return item
            } else {
                return nil
            }
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    
    static func saveVpaSuggetions(){
        
    }
    
    
    static func updateDetailsBank<T: NSManagedObject>(_ entity: T.Type, key: String, value: String , isSave : Bool = true) -> T?{
        let appDelegate =  AppDelegate.getAppDelegate()
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let entityName = String(describing: entity)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.returnsObjectsAsFaults = false
        //let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            let results = try (managedObjectContext.fetch(fetchRequest)) as? [T] ?? [T]()
            if results.count > 0 {
                let item = results[0]
                item.setValue(value, forKey: key)
                if isSave { saveContext() }
                return item
            } else {
                return nil
            }
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    
    
    
    
    static func updateDetails<T: NSManagedObject>(_ entity: T.Type, values: [String: String], isSave: Bool = true) -> T? {
        let appDelegate =  AppDelegate.getAppDelegate()
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let entityName = String(describing: entity)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.returnsObjectsAsFaults = false
        //let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            let results = try (managedObjectContext.fetch(fetchRequest)) as? [T] ?? [T]()
            if results.count > 0 {
                let item = results[0]
                for val in values {
                    //let value = toString(val.value)
                    item.setValue(val.value, forKey: val.key)
                }
                if isSave { saveContext() }
                return item
            } else {
                return nil
            }
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    

    
    
    
    
    
    
    
    static func updateUserVPA(info : UserInfo, isSave: Bool = true) {
        if let user = CoredataManager.getDetail(App_User.self) {
            user.accountId  = info.accountId
            user.customerId = info.customerId
            user.defVPAStatus   = info.defVPAStatus ?? false
            user.email          = info.email
            user.isMerchant     = info.isMerchant ?? false
            user.mobileNo       = info.mobileNo
            user.vpaName        = info.name
            user.regDate        = info.regDate
            user.showMerchant   = info.showMerchant ?? false
            user.virtualAddress = info.virtualAddress
            
            if isSave { saveContext() }
        }
    }
    
    static func save<M , E>(_ info : M ,_ entity: E.Type , isSave: Bool = true , req : String = "NA") where M : Decodable , E: NSManagedObject{
        
        let appDelegate =  AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
        
       
        let entityName = String(describing: entity)
        
        
        switch req{
        case "reqAction":
            let res = info as! TransactionHistoryResponse
            let trans = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context) as! Transaction_Response
            
            trans.encKey = res.encKey
            trans.pspRefNo = res.pspRefNo
            trans.resHash = res.resHash
            trans.status = res.status
            trans.statusDesc = res.statusDesc
            trans.isMerchant = res.isMerchant ?? false
            if let result = res.transDetails{
                for i in result{
                    
                }
            }
        default:
            print("")
            
        }
        if isSave{
            saveContext()
        }
    }
    
    
    static func decode<T , E>(modelType: T.Type , entity: E.Type , data: Data , req: String = "NA") where T : Decodable , E: NSManagedObject {
        
        let jsonDecoder = JSONDecoder()
        do {
            let myStruct = try jsonDecoder.decode(modelType, from: data)
            save(myStruct, entity , req: req)
        }catch let err{
            print(err.localizedDescription)
        }
    }
    

    
    
    
    static func getHighestValue<T: NSManagedObject>(_ entity: T.Type, key: String) -> T? {
        let appDelegate =  AppDelegate.getAppDelegate()
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let entityName = String(describing: entity)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let sort = NSSortDescriptor(key: key, ascending: false)
        fetchRequest.sortDescriptors = [sort]
        fetchRequest.fetchLimit = 1
        
        //let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        //var maxValue: Int64? = nil
        do {
            let result = try managedObjectContext.fetch(fetchRequest).first
            return result as? T
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    static func getSpecificRecord<T: NSManagedObject>(_ entity: T.Type, key: String, value: String) -> [T]? {
        let appDelegate =  AppDelegate.getAppDelegate()
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let entityName = String(describing: entity)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = NSPredicate(format: "\(key) == %@", value)
        fetchRequest.returnsObjectsAsFaults = false
        //let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            let results = try (managedObjectContext.fetch(fetchRequest)) as? [T] ?? [T]()
            if results.count > 0 {
                return results
            } else {
                return nil
            }
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    static func saveBanks(resp : BankInfo, isSave : Bool = true) {
        let appDelegate =  AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
        
        let bank = NSEntityDescription.insertNewObject(forEntityName: "Master_BankList", into: context) as! Master_BankList
        bank.bankCode = resp.bankCode
        bank.bankId   = Int64(resp.bankId ?? 0)
        bank.bankName = resp.bankName
        bank.ifsc     = resp.ifsc
        bank.iin      = Int64(resp.iin ?? 0)
        bank.statusCode = Int64(resp.statusCode ?? 0)
        
        if isSave {
            saveContext()
        }
    }

    
    
    static func saveCred(resp : OTPResponse, isSave : Bool = true) {
        let appDelegate =  AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
        
        let cred = NSEntityDescription.insertNewObject(forEntityName: "App_Cred", into: context) as! App_Cred
            cred.aes    = resp.cred?.aesKey
            cred.hashToken  = resp.cred?.hashToken
            cred.hmac   = resp.head?.refId
            cred.deviceId =  resp.cred?.deviceId
        
        if isSave {
            saveContext()
        }
    }
    static func saveUser(resp : OTPResponse,mobile: String?, isSave : Bool = true, appPin: String) {
        let appDelegate =  AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
        
        let user = NSEntityDescription.insertNewObject(forEntityName: "App_User", into: context) as! App_User
        user.appPin         = appPin
        user.aesKey         = resp.cred?.aesKey
        user.appName        = resp.head?.appName
        user.appVer         = resp.head?.appVer
        user.bluetoothMac   = resp.head?.bluetoothMac
        user.clientIp       = resp.head?.clientIp
        user.deviceId       = resp.head?.deviceId
        user.deviceType     = resp.head?.deviceType
        user.geoLat         = resp.head?.geoLat
        user.geoLong        = resp.head?.geoLong
        user.hashToken      = resp.cred?.hashToken
        user.hmac           = resp.cred?.hmac
        user.location       = resp.head?.location
        user.osVer          = resp.head?.osVer
        user.refId          = resp.head?.refId
        user.simId          = resp.head?.simId
        user.ts             = resp.head?.ts
        user.wifiMac        = resp.head?.wifiMac
        if let mob = mobile {
            user.mobile = mob
        }
        
        if isSave {
            saveContext()
        }
    }
    
    
    static func saveUserBankDetails(saveBankDetails : BankAccountsList , isSave: Bool = true){
        let appDelegate = AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
        
        let saveBank = NSEntityDescription.insertNewObject(forEntityName: "App_UserBankAccount_Details", into: context) as! App_UserBankAccount_Details
    
        saveBank.status = saveBankDetails.status
        saveBank.pspRespRefNo = saveBankDetails.pspRespRefNo ?? "NA"
        saveBank.pspRefNo = saveBankDetails.pspRefNo
        saveBank.resHash = saveBankDetails.resHash
        if isSave{
            saveContext()
        }
    }
    
    
    static func saveConfig(config : ConfigResponse, appStage: String?, isSave : Bool = true) {
        let appDelegate =  AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
        
        let conf = NSEntityDescription.insertNewObject(forEntityName: "App_Config", into: context) as! App_Config
        conf.androidGrace = config.body?.androidGrace
        conf.androidVer   = config.body?.androidVer
        conf.apiEndpoint  = config.body?.apiEndpoint
        conf.appLabel     = config.body?.appLabel
        conf.appSalt      = config.body?.appSalt
        conf.bankMaster   = config.body?.bankMaster
        conf.faqData      = config.body?.faqData
        conf.iosGrace     = config.body?.iosGrace
        conf.iosVer       = config.body?.iosVer
        conf.lastUpdated  = config.body?.lastUpdated
        
        conf.masterDelay     = config.body?.masterDelay
        conf.masterNextDate  = config.body?.masterNextDate
        conf.merchantBank    = config.body?.merchantBank
        conf.merchantVersion = config.body?.merchantVersion
        conf.otpReValidate  = config.body?.otpReValidate
        conf.otpValidity    = config.body?.otpValidity
        conf.pspVersion     = config.body?.pspVersion
        conf.pspBank        = config.body?.pspVersion
        conf.refId          = config.head?.refId
        conf.respUID        = config.head?.respUID
        conf.ts             = config.head?.ts
        
        if let stage = appStage {
            conf.appStage = stage
        }
        
        if isSave {
            saveContext()
        }
    }
    static func saveMasterBanks(bank : PSPBank, version : String, isSave : Bool = true) {
        let appDelegate =  AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
        
        let conf = NSEntityDescription.insertNewObject(forEntityName: "App_Config_pspBank", into: context) as! App_Config_pspBank
        conf.bankCode = bank.bankCode
        conf.bankName   = bank.bankName
        conf.bankStatus  = bank.bankStatus
        conf.bankVA     = bank.bankVA
        conf.pspVersion      = version
        
        if isSave {
            saveContext()
        }
    }
        
    static func saveTransaction(from : String, to: String,type : String,toName: String, amount: Double = 0,remark: String, emoji: String, status: String,expiry: Date = Date(), isSave :Bool = true) -> Transactions? {
          let appDelegate =  AppDelegate.getAppDelegate()
          let context = appDelegate.persistentContainer.viewContext
        //  do {
        var id = 0
        if CoredataManager.doesExistAny(Transactions.self) {
            if let maxVal = CoredataManager.getHighestValue(Transactions.self, key: "id") {
                id =  Int(maxVal.id)
            }
        }
        let sync = NSEntityDescription.insertNewObject(forEntityName: "Transactions", into: context) as! Transactions
        sync.id         = Int64(id + 1)
        sync.from       = from
        sync.to         = to
        sync.type       = type
        sync.toName     = toName
        sync.amount     = amount
        sync.remark     = remark
        sync.status     = status
        sync.emoji      = emoji
        sync.addedAt    = Date()
        sync.expiry     = expiry
        if isSave {
            saveContext()
        }
        return sync
      }
    
    
    static func transactionData(transactionHistory : TransactionHistoryDetails , isSave : Bool = true) -> User_Transaction?{
        let appDelegate =  AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
        
        
        let sync = NSEntityDescription.insertNewObject(forEntityName: "User_Transaction", into: context) as! User_Transaction
        sync.custRefNo = transactionHistory.custRefNo
        sync.drCrFlag = transactionHistory.drCrFlag
        sync.payType = transactionHistory.payType
        sync.payeeAccountNo = transactionHistory.drCrFlag
        sync.payeeBankCode = transactionHistory.payeeIfsc
        sync.payeeIfsc = transactionHistory.drCrFlag
        sync.payeeMaskAccountNo = transactionHistory.payeeMaskAccountNo
        sync.payeeName = transactionHistory.drCrFlag
        sync.payeeVirtualAddress = transactionHistory.payeeVirtualAddress
        sync.payerAccountNo = transactionHistory.payerAccountNo
        sync.payerBankCode = transactionHistory.drCrFlag
        sync.payerIfsc = transactionHistory.drCrFlag
        sync.payerName = transactionHistory.drCrFlag
        sync.payerVirtualAddress = transactionHistory.drCrFlag
        sync.paymentmethod = transactionHistory.drCrFlag
        sync.reason_desc =  transactionHistory.reason_desc
        sync.refFlag = transactionHistory.drCrFlag
        sync.trnDate = transactionHistory.drCrFlag
        sync.trnRefNo = Int64(transactionHistory.trnRefNo!)
        sync.txnAmount = transactionHistory.txnAmount
        sync.txnNote = transactionHistory.txnNote
        
        //transactionHistory.trnDate
        if isSave {
            saveContext()
        }
        return sync

        
        
    }
    
    
    
    
    static func saveUserBankList(bankList : AccountList , isSave : Bool = true){
        let appDelegate =  AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
        
        let account = NSEntityDescription.insertNewObject(forEntityName: "Account_List", into: context) as! Account_List
        account.accId = Int64(bankList.accId ?? 0)
        account.accountType = bankList.accountType
        account.bankCode = bankList.bankCode
        account.bankId = Int64(bankList.bankId ?? 0)
        account.ifscCode = bankList.ifscCode
        account.maskedAccountNumber = bankList.maskedAccountNumber
        account.mpinFlag = bankList.mpinFlag
        account.isPrimary = "F"
        
        if isSave{
            saveContext()
        }
    }
    
    
    
    
    static func saveNpciTokenListKey(getNpciData : GetTokenResponse , isSave : Bool = true){
        let appDelegate = AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
        
        let npciDataBase = NSEntityDescription.insertNewObject(forEntityName: "NPCI_Data", into: context) as! NPCI_Data
        npciDataBase.refId = getNpciData.refId
        npciDataBase.refUrl = getNpciData.refUrl
        npciDataBase.reqMsgId = getNpciData.reqMsgId
        npciDataBase.status = getNpciData.status
        npciDataBase.txnId = getNpciData.txnId
        npciDataBase.xmlResp = getNpciData.xmlResp
        
        npciListData(getNpciData.listKeys ?? [], npciDataBase)
        
        
        if isSave{
            saveContext()
        }
        
    }
    
    
    
    static func npciListData(_ listModel: [ListKeys] , _ npci: NPCI_Data){
        let appdelegate = AppDelegate.getAppDelegate()
        let context = appdelegate.persistentContainer.viewContext
        let npciListDB = NSEntityDescription.insertNewObject(forEntityName: "List_Key", into: context) as! List_Key
         
         for listData in listModel{
             npciListDB.code     = listData.code
             npciListDB.keyValue = listData.keyValue
             npciListDB.ki       = Int64(listData.ki ?? 0)
             npciListDB.owner    = listData.owner
             npciListDB.type     = listData.type
         }
        npciListDB.relationshipNpciData = npci
        
         saveContext()
    }
    
    
    
    
    
    
    
    
    
    static func getResponsefromListKey<T: NSManagedObject>(_ entity: T.Type, all : Bool = false) -> [T]?{
        let appDelegate =  AppDelegate.getAppDelegate()
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let entityName = String(describing: entity)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.returnsObjectsAsFaults = false
        //let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            let results = try (managedObjectContext.fetch(fetchRequest)) as? [T] ?? [T]()
            if results.count > 0 {
                return results
            } else {
                return nil
            }
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    
    static func storeNpciTokenList(npciResponse : GetTokenResponse){
        let appDelegate = AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
    }
    
    
    
    
    
    
    
    
    
    static func deleteDetail(myData : Account_List){
        let appDelegate = AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
        context.delete(myData)
        saveContext()
    }
    
    static func saveTransactions(payments : PaymentResponse , isSave : Bool = true){
        let appDelegate = AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
        
        let transaction = NSEntityDescription.insertNewObject(forEntityName: "App_User_Transactions", into: context) as! App_User_Transactions
        
        transaction.amount              = payments.amount
        transaction.approvalNumber      = payments.approvalNumber
        transaction.custRefNo           = payments.custRefNo
        transaction.npciTransId         = payments.npciTransId
        transaction.payeeVPA            = payments.payeeVPA
        transaction.payerVPA            = payments.payerVPA
        transaction.pspRefNo            = payments.pspRefNo
        transaction.refId               = payments.refId
        transaction.responseCode        = payments.responseCode
        transaction.status              = payments.status
        transaction.statusDesc          = payments.statusDesc
        transaction.txnAuthDate         = payments.txnAuthDate
        transaction.upiTransRefNo       = Int64(payments.upiTransRefNo ?? 0000)
        
        if isSave{
             saveContext()
         }
    }
    
    
    


     
    
    static func saveTransactionHistory(transactions: [TransactionHistoryDetails]) {
        let appDelegate =  AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext

        _ = transactions.map{CoredataManager().createPhotoEntityFrom(dictionary: $0)}
            do {
                try CoredataManager.saveContext()
            } catch let error {
                print(error)
            }
        }
    
    func createPhotoEntityFrom(dictionary: TransactionHistoryDetails) -> NSManagedObject? {
            let appDelegate = AppDelegate.getAppDelegate()
            let context = appDelegate.persistentContainer.viewContext
           
           if let transactionHis = NSEntityDescription.insertNewObject(forEntityName: "Transaction_History", into: context) as? Transaction_History {
               transactionHis.custRefNo =  dictionary.custRefNo
               transactionHis.drCrFlag = dictionary.drCrFlag
               transactionHis.payType = dictionary.payType
               transactionHis.payeeAccountNo = dictionary.payeeAccountNo
               transactionHis.payeeBankCode = dictionary.payeeBankCode
               transactionHis.payeeIfsc = dictionary.payeeIfsc
               transactionHis.payeeMaskAccountNo = dictionary.payeeMaskAccountNo
               transactionHis.payeeName = dictionary.payeeName
               transactionHis.payeeVirtualAddress = dictionary.payeeVirtualAddress
               transactionHis.payerAccountNo = dictionary.payerAccountNo
               transactionHis.payerBankCode = dictionary.payerBankCode
               transactionHis.payerIfsc = dictionary.payerIfsc
               transactionHis.payerName = dictionary.payerName
               transactionHis.payerVirtualAddress = dictionary.payerVirtualAddress
               transactionHis.paymentmethod = dictionary.paymentmethod
               transactionHis.reason_desc = dictionary.reason_desc
               transactionHis.refFlag = dictionary.refFlag
               transactionHis.trnDate = dictionary.trnDate
               transactionHis.trnRefNo = Int64(dictionary.trnRefNo ?? 000)
               transactionHis.txnAmount = dictionary.txnAmount
               transactionHis.txnNote = dictionary.txnNote
               transactionHis.txnStatus = dictionary.txnStatus
               transactionHis.txnStatusDesc = dictionary.txnStatusDesc
               transactionHis.txnType = dictionary.txnType
//                let mediaDictionary = dictionary["media"] as? [String: AnyObject]
//                photoEntity.mediaURL = mediaDictionary?["m"] as? String
               return transactionHis
           }
           return nil
       }
    
    func saveMerchantDetails(merchantDetails : [String:Any]){
        let appDelegate =  AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
        
        let bank = NSEntityDescription.insertNewObject(forEntityName: "Merchant_Details", into: context) as! Merchant_Details
       

    }
    
    
    static func saveMerchantConfig(config : MerchantConfigResponse, appStage: String?, isSave : Bool = true) {
        let appDelegate =  AppDelegate.getAppDelegate()
        let context = appDelegate.persistentContainer.viewContext
        
        let conf = NSEntityDescription.insertNewObject(forEntityName: "Merchant_App_Cred", into: context) as! Merchant_App_Cred
        conf.androidGrace = config.body?.androidGrace
        conf.androidVer   = config.body?.androidVer
        conf.apiEndpoint  = config.body?.apiEndpoint
        conf.appLabel     = config.body?.appLabel
        conf.appSalt      = config.body?.appSalt
        conf.faqData      = config.body?.faqData
        conf.iosGrace     = config.body?.iosGrace
        conf.iosVer       = config.body?.iosVer
        conf.lastUpdated  = config.body?.lastUpdated
        
        conf.merchantVersion = config.body?.merchantVersion
        conf.otpReAuth  =   config.body?.otpReAuth
        conf.otpValidity    = config.body?.otpValidity
        conf.refId          = config.head?.refId
        conf.respUID        = config.head?.respUID
        conf.ts             = config.head?.ts
//
        if let stage = appStage {
            conf.appStage = stage
        }
        
        if isSave {
            saveContext()
        }
    }
    
    
}
