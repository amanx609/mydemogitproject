//
//  APIManager.swift
//  CSCPAY
//
//  Created by Gowtham on 06/05/21.
//

import Foundation
import CommonLibrary
import CoreLocation



enum apiType {
    case otpResend
    case otpRequest
    case otpVerify
}

class APIManager {
    
    

//MARK: - Common fucntion to get request
    static func call<T: Decodable>(_ for: T.Type = T.self, urlString: String, reqAction : String, category: String = "NA", refId: String = "NA", resHash: String = "NA", otpHash : String = "NA", pspId: String? = nil, extra: String? = nil, mobCrypt: Bool = false, isNew : Bool = false, isMerchant : Bool = false , expiryDate: String = "NA", lastSixDebCard : String = "NA",npciTransId: String = "NA" , otpNpciTranId: String = "NA", credentialDataValue : String = "NA", bankCode : String = "NA" , channel : String = "M", completion : @escaping (T?, Error? ) -> Void) {
        
        
        guard let reqData = HelperUtil.getRequest(reqAction: reqAction, category: category, refId: refId, resHash: resHash, otpHash: otpHash, pspId: pspId,extra: extra, mobCrypt: mobCrypt, isNew: isNew , isMerchant: isMerchant , expiryDate: expiryDate , lastSixDebCard: lastSixDebCard ,npciTransId: npciTransId , otpNpciTranId: otpNpciTranId, credentialDataValue: credentialDataValue , bankCode: bankCode, channel: channel ) else {
            completion(nil,RuntimeError("001", "Invalid Request"))
            return
        }
    
        let parameters : [String: Any] =
            ["reqData": reqData]
        
        APIOperations.fetchResourcesPost(urlString: urlString, params: parameters) { (result) in
            switch result {
            case .success(let jsonData) :
                 let jsonDecoder = JSONDecoder()
                 do {
                    let endResult = try jsonDecoder.decode(APIResponse.self, from: jsonData!)
                    if endResult.resCode != "000" || endResult.resData == "NA" || endResult.resData == "" || endResult.resData == nil {
                        completion(nil, RuntimeError(endResult.resCode ?? "001", endResult.resMsg ?? Constants.SOMETHING_WRONG))
                        return
                    }
                     guard let dec = HelperUtil.decrypt(data : endResult.resData ?? "NA", mobCrypt : mobCrypt, isNew : isNew) else {
                        completion(nil,RuntimeError("001", "Cannot read the response"))
                        return
                    }
                    if Constants.printResponse {
                        let json = try JSONSerialization.jsonObject(with: Data(dec.utf8), options: .allowFragments)
                        print(json)

                        if reqAction == "getHistory" {
                            let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)

//                            if let result : TransactionHistoryResponse = resp as? TransactionHistoryResponse {
                                DispatchQueue.main.async {
   //                                 for trans in result.transDetails! {
   //                                     CoredataManager.decode(modelType: TransactionHistoryResponse.self, entity: App_User_Transactions, data: (resp as! Data))
   //                                 }
                                    

                                    CoredataManager.decode(modelType: TransactionHistoryResponse.self, entity: Transaction_History.self, data: data , req: reqAction)
                                    
//                                    CoredataManager.decode(modelType: TransactionHistoryResponse.self, entity: Transaction_History.self, data: data , req: reqAction)
                                    //CoredataManager.saveContext()
                                }
//                            }
                        }
                    }
                    let resp = try jsonDecoder.decode(T.self, from: Data(dec.utf8))
                    completion(resp,nil)
                 } catch {
                    print("we have an error \(error)")
                    completion(nil,error)
                }
            case .failure(let error) :
                print("we have an error \(error)")
                completion(nil,error)
            }
        }
    }
    
    
    //MARK: - getDetails
    static func getDetails<T: Decodable>(_ for: T.Type = T.self, mobile : String, reqAction : String = "generate",refId: String = "NA", resHash : String = "NA", otpHash: String = "NA", channel: String = "M", geoLong: String="NA",geoLat : String = "NA",location: String = "na",isMaster: Bool = false, isUser: Bool = false, category : String = "psp",mobCrypt: Bool = true, completion : @escaping (T?, Error? ) -> Void) {
        
        
        completion(nil,nil)
    }
    
    //MARK: - otpRequest
    static func otpRequest<T: Decodable>(_ for: T.Type = T.self, mobile : String, reqAction : String = "generate",refId: String = "NA", resHash : String = "NA", otpHash: String = "NA", channel: String = "M", geoLong: String="NA",geoLat : String = "NA",location: String = "na",isMaster: Bool = false, isUser: Bool = false, category : String = "psp",mobCrypt: Bool = true, completion : @escaping (T?, Error? ) -> Void) {
        var urlString = "start/otp"
        if isMaster { urlString = "start/master" }
        let appVersion = Bundle.main.versionNumber
        let deviceType  = UIDevice.current.systemName
        let osVersion   = UIDevice.current.systemVersion
        let ts = Date().getCurrentDate(format: "yyyy-MM-dd'T'H:m:s") //"2021-05-21T19:08:12"
        let tss = ts.1.getCurrentDate(format : "yyyyMMddHms")
        var refIds : String = "\(mobile)\(tss.0)"
        refIds    = refId == "NA" ? refIds : refId
        guard let ip = getIPAddress() else {
            completion(nil,RuntimeError("001", "IP unavailable."))
            return
        }
        //let simSeriralId = "\(mobile)\(mobile.prefix(8))"
        
        var params = [String: Any]()
        //let head    = ["appName":"CSC Pay", "appVer": appVersion ?? "1.0", "deviceId": getUUID() ?? "na", "deviceType":deviceType, "simId":"NA", "osVer":osVersion, "bluetoothMac":"02:00:00:00:00:00", "wifiMac":"02:00:00:00:00:00", "refId":refIds, "ts":ts.0, "reqAction":reqAction, "clientIp":ip, "location":location, "geoLat":geoLat, "geoLong":geoLong]
        let head = HelperUtil.getCommonHeader(reqAction: reqAction, isNew: true)
        var body = [String: Any]()
        
        if isUser {
            body = ["mobileNo":mobile, "userType":category]
            urlString = "device/user"
        } else if isMaster {
            body = ["mobileNo":mobile, "category":category, "deviceVer":"2"]
        } else {
            body = ["mobileNo":mobile, "otpChannel":channel, "resHash":resHash, "otpHash":otpHash]
        }
        
        params = ["head" : head, "body" : body]
        // For resend :- reqAction, resHash
        
        //params = ["head":["appName":"CSC Pay","appVer":"1.0","deviceId":"ios11","deviceType":"ios11","simId":"NA","osVer":"na","bluetoothMac":"na","wifiMac":"na","refId":"999989487011052106092101","ts":"2021-05-13T12:43:12","reqAction":"validate","clientIp":"127.0.0.1","location":"na","geoLat":"NA","geoLong":"NA"],"body":["mobileNo":"9999894870","otpChannel":"M","resHash":"NA","otpHash":"dc1ec82972913e0c5becb30ba8abb317c7682ff8e77bc4fb17dd86c54dfe85e7"]]
        
//        let creds = CoredataManager.getDetail(App_Cred.self)
        
        let creds = CoredataManager.getDetail(App_User.self)
        
        
        let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
        let decoded = String(data: jsonData, encoding: .utf8)!
        var reqData = ""
        if mobCrypt {
            reqData = AppSecure.encrypt(decoded, keyData: mobile)
        } else {
//            reqData = AppSecure.tokenEncryptWithGCM(decoded, aesKey: creds?.aes ?? "", token: creds?.hashToken ?? "")
            
            reqData = AppSecure.tokenEncryptWithGCM(decoded, aesKey: creds?.aesKey ?? "", token: creds?.hashToken ?? "")
        }
        let parameters : [String: Any] =
            ["reqData": reqData]
        print(params)
        
        APIOperations.fetchResourcesPost(urlString: urlString, params: parameters) { (result) in
            switch result {
            case .success(let jsonData) :
                 let jsonDecoder = JSONDecoder()
                 do {
                    let endResult = try jsonDecoder.decode(APIResponse.self, from: jsonData!)
                    if endResult.resCode != "000" || endResult.resData == "NA" || endResult.resData == "" || endResult.resData == nil {
                        completion(nil, RuntimeError(endResult.resCode ?? "001", endResult.resMsg ?? Constants.SOMETHING_WRONG))
                        return
                    }
                    var dec = ""
                    if mobCrypt {
                        dec = AppSecure.decrypt(endResult.resData ?? "")
                    } else {
                        
//                        dec = AppSecure.tokenDecryptWithGCM(endResult.resData ?? "", aesKey: creds?.aes ?? "")
//
                        
                        dec = AppSecure.tokenDecryptWithGCM(endResult.resData ?? "", aesKey: creds?.aesKey ?? "")
                        
                        
                    }
                    if Constants.printResponse {
                        //print(dec)
                        let json = try JSONSerialization.jsonObject(with: Data(dec.utf8), options: .allowFragments)
                        print(json)
                    }
                    let resp = try jsonDecoder.decode(T.self, from: Data(dec.utf8))
                    
                    //Store config values into DB (CoreData)
                    if isMaster && reqAction == "config" {
                        
                    }
                    completion(resp,nil)
                    /*
                    if isMaster {
                        let otpResponse = try jsonDecoder.decode(T.self, from: Data(dec.utf8))
                        completion(otpResponse,nil)
                    } else {
                        let otpResponse = try jsonDecoder.decode(OTPResponse.self, from: Data(dec.utf8))
                        completion(otpResponse,nil)
                    } */
                 } catch {
                    print("we have an error \(error)")
                    completion(nil,error)
                }
            case .failure(let error) :
                print("we have an error \(error)")
                completion(nil,error)
            }
        }
    }
   
    //MARK: - get
    static func get<T: Decodable>(_ for: T.Type = T.self, mobile : String = "NA", reqAction : String = "checkDevice", urlString: String = "indus/device", refId: String = "NA", tst: String = "NA", resHash : String = "NA", otpHash: String = "NA", channel: String = "M", geoLong: String="NA",geoLat : String = "NA",location: String = "na",pspBank: String = "NA", pspId : String , isCheckDevice: Bool = false,saveBank: Bool = false, completion : @escaping (T?, Error?, String? ) -> Void) {
        
        let creds = CoredataManager.getDetail(App_User.self)
        
        guard let mobile = creds?.mobile else {return}
        print(mobile)
        
       
        let appVersion = Bundle.main.versionNumber
        let deviceType  = UIDevice.current.systemName
        let osVersion   = UIDevice.current.systemVersion
        let ts = Date().getCurrentDate(format: "yyyy-MM-dd'T'H:m:s") //"2021-05-21T19:08:12"
        let tss = ts.1.getCurrentDate(format : "yyyyMMddHms")
        var refIds : String = "\(mobile)\(tss.0)"
        refIds    = refId == "NA" ? refIds : refId
        let timeStampt = tst == "NA" ? ts.0 : tst
        //if isCheckDevice { refIds = creds?.hmac ?? "na" }
        guard let ip = getIPAddress() else {
            completion(nil,RuntimeError("001", "IP unavailable."), nil)
            return
        }
        let simSeriralId = "\(mobile)\(mobile.prefix(8))"
       
        
       
        
        var params = [String: Any]()
        var head    = ["appName":"CSC Pay", "appVer": appVersion ?? "1.0", "deviceId": getUUID() ?? "na", "deviceType":deviceType, "simId":"\(simSeriralId)0", "osVer":osVersion, "bluetoothMac":"02:00:00:00:00:00", "wifiMac":"02:00:00:00:00:00", "refId":refIds, "ts":timeStampt, "reqAction":reqAction, "clientIp":ip, "location":location, "geoLat":geoLat ?? "", "geoLong":geoLong ?? ""]
        
        if reqAction == "bankList"{
            if let pspData = HelperUtil().getPSPData(pspId: pspId){
                head = ["appName":"CSC Pay", "appVer": appVersion ?? "1.0", "deviceId": getUUID() ?? "na" ,"deviceType":deviceType, "osVer":osVersion , "refId":refIds, "ts":timeStampt, "reqAction":reqAction, "clientIp":ip, "location":location, "geoLat":geoLat, "geoLong":geoLong , "pspId" : pspId ,"pspData":
                        "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTFFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiNDU6MjI6NjU6Q0U6MDA6QzEiLA0KCSJibHVldG9vdGhNYWMiOiAiMjc6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUxRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjg5OTE3NTIxNzAwNjA3NDU0MjUiLA0KCSJvcyI6ICJJT1MgMTMuMi4zIiwNCgkicmVnSWQiOiAiTkEiLA0KCSJzZWxlY3RlZFNpbVNsb3QiOiAiMCIsDQoJImZjbVRva2VuIjogIiINCn0"
//                        pspData
                ]
            }
        }
                
        var body = [String: Any]()
        body = ["mobileNo": mobile , "pspBank": pspBank]
        params = ["head" : head, "body" : body]
        
        let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
        let decoded = String(data: jsonData, encoding: .utf8)!
       
       let reqData = AppSecure.tokenEncryptWithGCM(decoded, aesKey: creds?.aesKey ?? "", token: creds?.hashToken ?? "")
        
        let parameters : [String: Any] =
            ["reqData": reqData]
        print(params)
        
        APIOperations.fetchResourcesPost(urlString: urlString, params: parameters) { (result) in
            switch result {
            case .success(let jsonData) :
                 let jsonDecoder = JSONDecoder()
                 do {
                    let endResult = try jsonDecoder.decode(APIResponse.self, from: jsonData!)
                    if endResult.resCode != "000" || endResult.resData == "NA" || endResult.resData == "" || endResult.resData == nil {
                        completion(nil, RuntimeError(endResult.resCode ?? "001", endResult.resMsg ?? Constants.SOMETHING_WRONG), nil)
                        return
                    }
                    let dec = AppSecure.tokenDecryptWithGCM(endResult.resData ?? "", aesKey: creds?.aesKey ?? "")
                    
                    if Constants.printResponse {
                        //print(dec)
                        let json = try JSONSerialization.jsonObject(with: Data(dec.utf8), options: .allowFragments)
                        print(json)
                    }
                    let resp = try jsonDecoder.decode(T.self, from: Data(dec.utf8))
                    if saveBank {
                        if let result : BankList = resp as? BankList {
                            DispatchQueue.main.async {
                                for bank in result.bankMasterList! {
                                    CoredataManager.saveBanks(resp : bank, isSave : false)
                                }
                                CoredataManager.saveContext()
                            }
                        } }
                    completion(resp,nil,reqData)
                 } catch {
                    print("we have an error \(error)")
                    completion(nil,error,nil)
                }
            case .failure(let error) :
                print("we have an error \(error)")
                completion(nil,error,nil)
            }
        }
    }
    
    //MARK: - Validate
    static func validate<T: Decodable>(_ for: T.Type = T.self, reqData: String, completion : @escaping (T?, Error?, String? ) -> Void) {
        
        let parameters : [String: Any] =
            ["reqData": reqData]
        print(reqData)
        
        APIOperations.fetchResourcesPost(urlString: "indus/device", params: parameters) { (result) in
            switch result {
            case .success(let jsonData) :
                 let jsonDecoder = JSONDecoder()
                 do {
                    let endResult = try jsonDecoder.decode(APIResponse.self, from: jsonData!)
                    if endResult.resCode != "000" || endResult.resData == "NA" || endResult.resData == "" || endResult.resData == nil {
                        completion(nil, RuntimeError(endResult.resCode ?? "001", endResult.resMsg ?? Constants.SOMETHING_WRONG), nil)
                        return
                    }
                    let creds = CoredataManager.getDetail(App_Cred.self)
                    let dec = AppSecure.tokenDecryptWithGCM(endResult.resData ?? "", aesKey: creds?.aes ?? "")
                    
                    if Constants.printResponse {
                        //print(dec)
                        let json = try JSONSerialization.jsonObject(with: Data(dec.utf8), options: .allowFragments)
                        print(json)
                    }
                    let resp = try jsonDecoder.decode(T.self, from: Data(dec.utf8))
                    completion(resp,nil,nil)
                 } catch {
                    print("we have an error \(error)")
                    completion(nil,error,nil)
                }
            case .failure(let error) :
                print("we have an error \(error)")
                completion(nil,error,nil)
            }
        }
    }

    
    
    
    
    
    
    //MARK: - Merchant Api
    static func getMerchant<T: Decodable>(_ for: T.Type = T.self, reqAction : String = "directMerchant", pspBank:String = "NA" , virtualAddress: String = "NA", urlString: String = "indus/merchant/directmerchant",accountNo : String = "NA" ,contactName: String = "NA", contactPhone: String = "NA", contactEmail: String = "NA", loginEmail: String = "NA", merchantType: String = "NA", integrationType: String = "NA", settleType: String = "NA", ifscCode: String = "NA", requestUrl1: String = "NA", requestUrl2: String = "NA", requestUrl3: String = "NA", requestUrl4: String = "NA", merCategoryCode: String = "NA", panNo: String = "NA", gstin: String = "NA", udyogId: String = "NA", cityName: String = "NA", stateName: String = "NA", pinCode: String = "NA", address1: String = "NA", address2: String = "NA", address3: String = "NA", merchantId : String = "NA" , terminalId: String = "NA", pgMerchantId : String = "NA", isMerchnt: Bool = false , legalName : String = "NA", requestModel : CommonModel?, merchantRegresponse : MerchantRegistrationModel? = nil ,completion : @escaping (T?, Error?, String? ) -> Void) {
        
        let creds = CoredataManager.getDetail(App_User.self)
        var params = [String: Any]()
        
        
        guard let requestData = HelperUtil.getRequestMerchat(reqAction: reqAction, pspBank: pspBank  , virtualAddress: virtualAddress , contactName: contactName, contactPhone: contactPhone, contactEmail: contactEmail, loginEmail: loginEmail,accountNo: accountNo, ifscCode: ifscCode, panNo: panNo, gstin: gstin, cityName: cityName, stateName: stateName, pinCode: pinCode, address1: address1, address2: address2, address3: address3 ,pgMerchantId: pgMerchantId, isMerchant: true , merchantId: merchantId , terminalId : terminalId , requestModel: requestModel, merchantRegModel: merchantRegresponse)else{
            completion(nil , RuntimeError("001", "Invalid Request") , "invalid params")
            return
        }
        
        
        let parameter : [String:Any] = ["reqData" : requestData]
        print(requestData)
        

        
        
        APIOperations.fetchResourcesPost(urlString: urlString, params: parameter) { (result) in
            switch result {
            case .success(let jsonData) :
                 let jsonDecoder = JSONDecoder()
                 do {
                    let endResult = try jsonDecoder.decode(APIResponse.self, from: jsonData!)
                    if endResult.resCode != "000" || endResult.resData == "NA" || endResult.resData == "" || endResult.resData == nil {
                        completion(nil, RuntimeError(endResult.resCode ?? "001", endResult.resMsg ?? Constants.SOMETHING_WRONG), nil)
                        return
                    }
                    let dec = AppSecure.tokenDecryptWithGCM(endResult.resData ?? "", aesKey: creds?.aesKey ?? "")
                    
                    if Constants.printResponse {
                        let json = try JSONSerialization.jsonObject(with: Data(dec.utf8), options: .allowFragments)
                        print(json)
                        
                        if reqAction == "activateMerchant"{
                            UD.SharedManager.saveToUserDefaults(key: reqAction, value: dec.utf8)
                        }
                    }
                    let resp = try jsonDecoder.decode(T.self, from: Data(dec.utf8))
                    completion(resp,nil,requestData)
                 } catch {
                    print("we have an error \(error)")
                    completion(nil,error,nil)
                }
            case .failure(let error) :
                print("we have an error \(error)")
                completion(nil,error,nil)
            }
        }
    }
    
    
    
    

    //MARK: - Load Json

   static func loadJson(filename fileName: String) -> [BankInfo]? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(BankList.self, from: data)
                if let result : BankList = jsonData as? BankList {
                    DispatchQueue.main.async {
                        CoredataManager.cleanEntity(Master_BankList.self)
                        for bank in result.bankMasterList! {
                            CoredataManager.saveBanks(resp : bank, isSave : false)
                        }
                        CoredataManager.saveContext()
                    }
                }
                return jsonData.bankMasterList
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
    
    
    
    
    
    
    
    
    //MARK:- Common JSON Function
   static func loadAllJson<T:Decodable>(_ for: T.Type = T.self ,filename fileName: String) -> T? {
         if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
             do {
                 let data = try Data(contentsOf: url)
                 let decoder = JSONDecoder()
                 let jsonData = try decoder.decode(T.self, from: data)
                 if let result : T.Type = jsonData as? T.Type {
                     DispatchQueue.main.async {
                         print(result)
//                         for bank in result! {
//                             CoredataManager.saveBanks(resp : bank, isSave : false)
//                         }
//                         CoredataManager.saveContext()
                     }
                 }
                 return jsonData
             } catch {
                 print("error:\(error)")
             }
         }
         return nil
     }
    
    
    
    //MARK: - MPIN API
    static func setMpinSetup<T: Decodable>(_ for: T.Type = T.self, urlString:String =  "indus/accountmpin", reqAction : String = "setMpinInit", category: String = "NA", refId: String = "NA", resHash: String = "NA", otpHash : String = "NA", pspId: String? = nil, extra: String? = nil, mobCrypt: Bool = false, isNew : Bool = false, isMerchant : Bool = false , accountId: Int = 00000,cardType: String = "O", expiryDate:String = "0524", lastSixDebCard: String = "NA",  virtualAddress : String = "NA",  completion : @escaping (T?, Error? ) -> Void){
    
        //MARK:- Request
    guard let reqData = HelperUtil.getRequest(reqAction: reqAction, category: category, refId: refId, resHash: resHash, otpHash: otpHash, pspId: pspId,extra: extra, mobCrypt: mobCrypt, isNew: isNew , isMerchant: isMerchant , expiryDate: expiryDate , lastSixDebCard: lastSixDebCard , virtualAddress : virtualAddress , accountId: accountId) else {
            completion(nil,RuntimeError("001", "Invalid Request"))
            return
        }
        
        
        let parameters : [String: Any] =
            ["reqData": reqData]
        
        APIOperations.fetchResourcesPost(urlString: urlString, params: parameters) { (result) in
            switch result {
            case .success(let jsonData) :
                let jsonDecoder = JSONDecoder()
                do {
                    let endResult = try jsonDecoder.decode(APIResponse.self, from: jsonData!)
                    if endResult.resCode != "000" || endResult.resData == "NA" || endResult.resData == "" || endResult.resData == nil {
                        completion(nil, RuntimeError(endResult.resCode ?? "001", endResult.resMsg ?? Constants.SOMETHING_WRONG))
                        return
                    }
                    guard let dec = HelperUtil.decrypt(data : endResult.resData ?? "NA", mobCrypt : mobCrypt, isNew : isNew) else {
                        completion(nil,RuntimeError("001", "Cannot read the response"))
                        return
                    }
                    if Constants.printResponse {
                        let json = try JSONSerialization.jsonObject(with: Data(dec.utf8), options: .allowFragments)
                        print(json)
                    }
                    let resp = try jsonDecoder.decode(T.self, from: Data(dec.utf8))
                    if reqAction == "getHistory"{
                        
                    }
                    completion(resp,nil)
                } catch {
                    print("we have an error \(error)")
                    completion(nil,error)
                }
            case .failure(let error) :
                print("we have an error \(error)")
                completion(nil,error)
            }
        }
    }
    
    
    static func RegisterAppServices() -> String?{
        var param = [String: String]()
        var isValid = true
        guard let user = CoredataManager.getDetail(App_User.self) else {
            return "NA"
        }
       
        guard let token = CoredataManager.getDetail(List_Key.self)else{
            return "NA"
        }
        
        
        
        if user.deviceId == getUUID() {
            param["deviceId"] = getUUID() ?? "NA"
        } else { isValid = false }
        
        if user.appName == "CSC Pay" {
            param["appName"] = "CSC Pay"
        } else { isValid = false }
        
        guard let mobile = user.mobile, mobile.count >= 10 else {
            return "NA"
        }
        let hmac = "\(param["appName"]!)|\(mobile)|\(param["deviceId"]!)".sha256() as! String
        print(hmac)
        
        print(hmac.hexaToBytes)
        print(token.keyValue!.hexaToBytes)
//        cDVyTEZPTWl5Q1FRcVZaNTVCSzlGNVZKR0FISFlhc0w=
      
        token.keyValue = "cDVyTEZPTWl5Q1FRcVZaNTVCSzlGNVZKR0FISFlhc0w="
        
        let hmacHesh = AppSecure.AES_Encrypt(plainText: hmac, key: token.keyValue! , hexaToBytes: true)
        
//        7035724c464f4d697943515171565a3535424b394635564a474148485961734c
//        encrypt(hmac, keyData: token.keyValue! , mobileEnc: false)
        print(hmacHesh)
        return hmacHesh as! String
    }
    
    

    
    //MARK: - Call Merchant
 
    static func callMerchant<T: Decodable>(_ for: T.Type = T.self, urlString: String = "indus/merchant/", reqAction : String, category: String = "NA", refId: String = "NA", resHash: String = "NA", otpHash : String = "NA", pspId: String? = nil, extra: String? = nil, mobCrypt: Bool = false, isNew : Bool = false, isMerchant : Bool = false , expiryDate: String = "NA", lastSixDebCard : String = "NA",npciTransId: String = "NA" , otpNpciTranId: String = "NA", credentialDataValue : String = "NA", completion : @escaping (T?, Error? ) -> Void) {
        
        
        guard let reqData = HelperUtil.getRequest(reqAction: reqAction, category: category, refId: refId, resHash: resHash, otpHash: otpHash, pspId: pspId,extra: extra, mobCrypt: mobCrypt, isNew: isNew , isMerchant: isMerchant , expiryDate: expiryDate , lastSixDebCard: lastSixDebCard ,npciTransId: npciTransId , otpNpciTranId: otpNpciTranId, credentialDataValue: credentialDataValue) else {
            completion(nil,RuntimeError("001", "Invalid Request"))
            return
        }
    
        let parameters : [String: Any] =
            ["reqData": reqData]
        
        APIOperations.fetchResourcesPost(urlString: urlString, params: parameters) { (result) in
            switch result {
            case .success(let jsonData) :
                 let jsonDecoder = JSONDecoder()
                 do {
                    let endResult = try jsonDecoder.decode(APIResponse.self, from: jsonData!)
                    if endResult.resCode != "000" || endResult.resData == "NA" || endResult.resData == "" || endResult.resData == nil {
                        completion(nil, RuntimeError(endResult.resCode ?? "001", endResult.resMsg ?? Constants.SOMETHING_WRONG))
                        return
                    }
                    guard let dec = HelperUtil.decrypt(data : endResult.resData ?? "NA", mobCrypt : mobCrypt, isNew : isNew) else {
                        completion(nil,RuntimeError("001", "Cannot read the response"))
                        return
                    }
                    if Constants.printResponse {
                        let json = try JSONSerialization.jsonObject(with: Data(dec.utf8), options: .allowFragments)
                        print(json)
                    }
                    let resp = try jsonDecoder.decode(T.self, from: Data(dec.utf8))
                    completion(resp,nil)
                 } catch {
                    print("we have an error \(error)")
                    completion(nil,error)
                }
            case .failure(let error) :
                print("we have an error \(error)")
                completion(nil,error)
            }
        }
    }
    
    
    //MARK: - Bank Call

    static func bankCall<T:Decodable>(_ for: T.Type = T.self, urlString: String = "indus/account"
        , reqAction : String, category: String = "NA", pspId: String? = nil, pspBank:String = "NA", extra: String? = nil, mobCrypt : Bool = false, isNew : Bool = false , pspRespRefNo : String = "NA" , accountStatus : String = "S" ,
        customerName : String = "NA" , bankCode: String = "NA" , accountId : Int = 1234 , virtualAddress : String = "NA" , reqType : String = "NA" , accountList: [String:Any] = ["NA":"NA"] ,
        completion : @escaping (T?, Error? ) -> Void){
            
            guard let reqData = HelperUtil.getBankRequest(reqAction: reqAction, pspId: pspId ?? "IBL", pspBank: pspBank, extra: extra, virtualAddress: virtualAddress, accountId: accountId, accountStatus: accountStatus, customerName: customerName, bankCode: bankCode, pspRespRefNo: pspRespRefNo) else {
                completion(nil,RuntimeError("001", "Invalid Request"))
                return
            }
            
            
            let parameters : [String: Any] = ["reqData": reqData]
            
            APIOperations.fetchResourcesPost(urlString: urlString, params: parameters) { (result) in
                switch result {
                case .success(let jsonData) :
                     let jsonDecoder = JSONDecoder()
                     do {
                        let endResult = try jsonDecoder.decode(APIResponse.self, from: jsonData!)
                        if endResult.resCode != "000" || endResult.resData == "NA" || endResult.resData == "" || endResult.resData == nil {
                            completion(nil, RuntimeError(endResult.resCode ?? "001", endResult.resMsg ?? Constants.SOMETHING_WRONG))
                            return
                        }
                        guard let dec = HelperUtil.decrypt(data : endResult.resData ?? "NA", mobCrypt : mobCrypt, isNew : isNew) else {
                            completion(nil,RuntimeError("001", "Cannot read the response"))
                            return
                        }
                        if Constants.printResponse {
                            let json = try JSONSerialization.jsonObject(with: Data(dec.utf8), options: .allowFragments)
                            print(json)
                        }
                        let resp = try jsonDecoder.decode(T.self, from: Data(dec.utf8))
                         
                        completion(resp,nil)
                     } catch {
                        print("we have an error \(error)")
                        completion(nil,error)
                    }
                case .failure(let error) :
                    print("we have an error \(error)")
                    completion(nil,error)
                }
            }
            
    }
    
    //MARK: - Payment Call

    static func payment<T:Decodable>(_ for: T.Type = T.self, urlString: String = "indus/transact/payment"
        , reqAction : String, refId : String = "NA", isMerchant: Bool = false ,  category: String = "IndusInd", pspId: String? = "IBL", pspBank:String = "NA", extra: String? = nil , payeeType: [String:Any] = ["NA":"NA"] , payerType: [String:Any] = ["NA":"NA"] ,upiTransRefNo : Int = 0 , transactionNote: String = "NA" , isBharatQr: Bool = false,
        amount :String =  "1.00", mobCrypt : Bool = false , isNew : Bool = false ,expiryTime: Int = 0,initiationMode : String = "NA", saveTrans: Bool = false,completion : @escaping (T?, Error? ) -> Void){
            
            guard let reqData = HelperUtil.getRequestPayment(reqAction: reqAction, category: category, refId: refId, pspId: pspId, extra: extra, mobCrypt: mobCrypt, isNew: isNew, isMerchant: isMerchant, payeeType: payeeType, payerType: payerType, amount: amount, upiTransRefNo: upiTransRefNo, transactionNote: transactionNote, isBharatQr: isBharatQr, initiationMode: initiationMode, expiryTime: expiryTime) else {
                completion(nil,RuntimeError("001", "Invalid Request"))
                return
            }
        
            let parameters : [String: Any] =
                ["reqData": reqData]
            
            APIOperations.fetchResourcesPost(urlString: urlString, params: parameters) { (result) in
                switch result {
                case .success(let jsonData) :
                     let jsonDecoder = JSONDecoder()
                     do {
                        let endResult = try jsonDecoder.decode(APIResponse.self, from: jsonData!)
                        if endResult.resCode != "000" || endResult.resData == "NA" || endResult.resData == "" || endResult.resData == nil {
                            completion(nil, RuntimeError(endResult.resCode ?? "001", endResult.resMsg ?? Constants.SOMETHING_WRONG))
                            return
                        }
                        guard let dec = HelperUtil.decrypt(data : endResult.resData ?? "NA", mobCrypt : mobCrypt, isNew : isNew) else {
                            completion(nil,RuntimeError("001", "Cannot read the response"))
                            return
                        }
                        if Constants.printResponse {
                            let json = try JSONSerialization.jsonObject(with: Data(dec.utf8), options: .allowFragments)
                            print(json)
                        }
                        let resp = try jsonDecoder.decode(T.self, from: Data(dec.utf8))
                         
                        completion(resp,nil)
                     } catch {
                        print("we have an error \(error)")
                        completion(nil,error)
                    }
                case .failure(let error) :
                    print("we have an error \(error)")
                    completion(nil,error)
                }
            }
    }
    
    
    static func dispute<T:Decodable>(_ for : T.Type , urlString :String = "indus/support", reqAction: String = "NA" , pspId: String = "NA", upiTranRefNo: String = "NA" , ticketNo: String = "NA" , requestType: String = "NA" , statusIs: String = "NA" , disputeRemark: String = "NA" , disputeType : String = "NA" , custRefNo: String = "NA" , completion: @escaping (T? , Error?) -> Void){
        
        var pspBank = "IndusInd"
        if pspId == "IBL"{
            pspBank = "IndusInd"
        }
        
        
        guard let reqData = HelperUtil.getRequestDispute(reqAction: reqAction, pspId: pspId, pspBank: pspBank, upiTrasRefNo: upiTranRefNo, requestType: requestType, statusIs: statusIs, disputeRemark: disputeRemark, disputeType: disputeType, custRefNo: custRefNo, ticketNo: ticketNo) else{
            completion(nil , RuntimeError("001", "InvalidRequest"))
            return
        }
        
        let parameters : [String:Any] = ["reqData" : reqData]
        print(reqData)
        
        APIOperations.fetchResourcesPost(urlString: urlString, params: parameters) { (result) in
            switch result {
            case .success(let jsonData) :
                 let jsonDecoder = JSONDecoder()
                 do {
                    let endResult = try jsonDecoder.decode(APIResponse.self, from: jsonData!)
                    if endResult.resCode != "000" || endResult.resData == "NA" || endResult.resData == "" || endResult.resData == nil {
                        completion(nil, RuntimeError(endResult.resCode ?? "001", endResult.resMsg ?? Constants.SOMETHING_WRONG))
                        return
                    }
                    guard let dec = HelperUtil.decrypt(data : endResult.resData ?? "NA", mobCrypt : false, isNew : false) else {
                        completion(nil,RuntimeError("001", "Cannot read the response"))
                        return
                    }
                    if Constants.printResponse {
                        let json = try JSONSerialization.jsonObject(with: Data(dec.utf8), options: .allowFragments)
                        print(json)
                    }
                    let resp = try jsonDecoder.decode(T.self, from: Data(dec.utf8))
                     
                    completion(resp,nil)
                 } catch {
                    print("we have an error \(error)")
                    completion(nil,error)
                }
            case .failure(let error) :
                print("we have an error \(error)")
                completion(nil,error)
            }
        }
    }
    
    
        
    func transactionHistory(getHistory : String = "getHistory" , resp: TransactionHistoryResponse?){
        if getHistory == "getHistory" {
            if let result = resp{
                CoredataManager.saveTransactionHistory(transactions: result.transDetails ?? [])
            }
        }
    }
    
}


