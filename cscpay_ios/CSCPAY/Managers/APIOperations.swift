//
//  APIOperations.swift
//  VLE Survey
//
//  Created by Abhishek Ranjan on 06/10/20.
//  Copyright © 2020 CSC eGovernance. All rights reserved.
//

import Foundation

class APIOperations {
    
    static func fetchResourcesPost(urlString: String,params : [String: Any]?, completion: @escaping (Result<Data?, Error>) -> Void) {
         let url = Constants.BASE_URL + urlString
        
         let Url = String(format: url) // "https://devupi.csccloud.in/gw1/"
         guard let serviceUrl = URL(string: Url) else { return }
         var request = URLRequest(url: serviceUrl)
         request.httpMethod = "POST"
         do {
             request.httpBody = try JSONSerialization.data(withJSONObject: params!, options: .prettyPrinted) // pass dictionary to data object and set it as request body
         } catch let error {
             print(error.localizedDescription)
             completion(.failure(error))
         }
         request.addValue("application/json", forHTTPHeaderField: "Content-Type")
         request.addValue("application/json", forHTTPHeaderField: "Accept")
         request.timeoutInterval = 60
         let session = URLSession.shared
         session.dataTask(with: request) { (data, response, error) in
             if let response = response {
                if Constants.printResponse {
                    print(response) }
             }
            if let err = error {
                if Constants.printResponse {
                    print(err) }
                completion(.failure(err))
            }
             if let data = data {
                completion(.success(data))
             }
         }.resume()
    }
    func simpleGetUrlRequest(urlString: String, completion: @escaping (Result<Data?, Error>) -> Void)
    {
        //If there is a parameter then add that into urlString
        let url = URL(string: urlString)!

        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else {
                completion(.failure(error!))
                return }
            if Constants.printResponse {
                print("The response is : ",String(data: data, encoding: .utf8)!) }
            completion(.success(data))
            //print(NSString(data: data, encoding: String.Encoding.utf8.rawValue) as Any)
        }
        task.resume()
    }
}
