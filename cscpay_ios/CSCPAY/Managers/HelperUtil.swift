//
//  HelperUtil.swift
//  CSCPAY
//
//  Created by Gowtham on 06/07/21.
//

import Foundation
import UIKit
import CoreLocation


class HelperUtil {
    
    
  
    
    
//MARK: - Request Function For Response ----------------------------------------------------------------------------------------------------


    static func getRequest(reqAction: String, category: String = "NA", refId: String = "NA", resHash: String = "NA", otpHash : String = "NA", pspId: String? = nil, extra: String? = nil, mobCrypt: Bool = false, isNew : Bool = false , isMerchant : Bool = false , expiryDate: String = "NA" , lastSixDebCard: String = "NA" , virtualAddress : String = "NA" , accountId : Int = 00000 , npciTransId: String = "NA" , otpNpciTranId: String = "NA" , credentialDataValue : String = "NA" , bankCode : String = "NA",  payeeType: [String:Any] = ["NA": "NA"] , payerType: [String:Any] = ["NA":"NA"] , amount: Int = 0 , channel : String = "M") -> String? {
        
        var params = [String: Any]()
        var head = [String: Any]()
        var body = [String: Any]()
        
        
        
        
        if let header = HelperUtil.getCommonHeader(reqAction: reqAction, mobile: category, refId : refId, pspId: pspId, extra : extra , isNew: isNew , isMerchant: isMerchant) {
            head = header
        } else { return nil }
        
        

        if let bodyData = HelperUtil.getBody(reqAction: reqAction, category: category, resHash: resHash, otpHash: otpHash, pspId: pspId,extra: extra, isNew: isNew , expiryDate: expiryDate , lastSixDebCard: lastSixDebCard , accountId : accountId, virtualAddress: virtualAddress , npciTransId: npciTransId , otpNpciTranId: otpNpciTranId  , credDataValue: credentialDataValue , bankCode: bankCode , channel: channel) {
            body = bodyData
        } else { return nil }
        
        
        params = ["head" : head, "body" : body]
        if Constants.printResponse { print(params) }
        let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
        let decode = String(data: jsonData, encoding: .utf8)
        guard let decoded = decode else { return nil }
        
        guard let reqData = encrypt(data : decoded, reqAction: reqAction, mobCrypt : mobCrypt, mob: category, isNew : isNew) else {
            return nil
        }
        return reqData
    }
    
    
    
    
    

    
    
    
    
    
    
    //MARK: - Encryption -------------------------------------------------------------------------------------------------------------------
    static func encrypt(data : String, reqAction :String, mobCrypt : Bool = false, mob: String? = nil, isNew : Bool = false) -> String? {
        var mobile : String?
        if isNew { mobile = "9876543210"
            if let user = CoredataManager.getDetail(App_User.self) {
                let mob = user.mobile ?? "NA"
                if mob.count >= 10 { mobile = mob }
                else { return nil }
            }
            if reqAction == "generate" || reqAction == "resend" || reqAction == "validate" {
                if let mobNum = mob { mobile = mobNum } }
        } else {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            guard let mob = user.mobile, mob.count >= 10 else {
                return nil
            }
            mobile = mob
        }
        
        if mobCrypt {
            return AppSecure.encrypt(data, keyData: mobile ?? "NA")
        } else {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            if let aes = user.aesKey, let token = user.hashToken {
                return AppSecure.tokenEncryptWithGCM(data, aesKey: aes, token: token)
            } else { return nil }
            
        }
    }
    
    
    //MARK: - Decryption -------------------------------------------------------------------------------------------------------------------
    
    static func decrypt(data : String, mobCrypt : Bool = false, isNew : Bool = false) -> String? {
        
        if mobCrypt {
            return AppSecure.decrypt(data)
        } else {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            if let aes = user.aesKey {
                return AppSecure.tokenDecryptWithGCM(data, aesKey: aes)
            } else { return nil }
            
        }
    }
    
    
//MARK: - Common Header Function---------------------------------------------------------------------------------------------------------
    
    static func getCommonHeader(reqAction: String, mobile: String? = nil, refId: String = "NA",pspId: String? = nil, extra : String? = nil , isNew: Bool = false , isMerchant: Bool = false) -> [String: String]? {
        //Some static need to be change dynamic
//        let location = "Delhi"
//        let geoLat = "72.223"
//        let geoLong = "77.223"
        
        let location = Constants.location
        let geoLat = Constants.geolat
        let geoLong = Constants.geolong
        
        
//        let location = HelperUtil().currentLocation ?? "Delhi"
//        let geoLat =  HelperUtil().lat ?? "72.223"
//        let geoLong = HelperUtil().long ?? "77.223"
//
        
        let appVersion = Bundle.main.versionNumber
        let deviceType  = UIDevice.current.systemName
        let osVer   = UIDevice.current.systemVersion
        let ts = Date().getCurrentDate(format: "yyyy-MM-dd'T'H:m:s") //"2021-05-21T19:08:12"
        let tss = ts.1.getCurrentDate(format : "yyyyMMddHms")
        
        var ip = "NA"
        if let ips = getIPAddress() {
            ip = ips
        }
        
        if isNew {
            var tempMobile = "9876543210"
            if let user = CoredataManager.getDetail(App_User.self) {
                let mob = user.mobile ?? "NA"
                if mob.count >= 10 { tempMobile = mob }
                else { return nil }
            }
            if reqAction == "generate" || reqAction == "resend" || reqAction == "validate" {
                if let mobile = mobile { tempMobile = mobile } }
            var refIds : String = "\(tempMobile)\(tss.0)"
            refIds = refId == "NA" ? refIds : refId
            let simSeriralId = "\(tempMobile)\(tempMobile.prefix(8))0"
            
            return ["appName":"CSC Pay", "appVer": appVersion ?? "1.0", "deviceId": getUUID() ?? "NA", "deviceType":deviceType, "simId": simSeriralId, "osVer":osVer, "bluetoothMac":"02:00:00:00:00:00", "wifiMac":"02:00:00:00:00:00", "refId":refIds, "ts":ts.0, "reqAction":reqAction, "clientIp":ip, "location":location, "geoLat":geoLat, "geoLong":geoLong]
        }
        
        guard let user = CoredataManager.getDetail(App_User.self) else {
            return nil
        }
        guard let mobile = user.mobile, mobile.count >= 10 else {
            return nil
        }
        var param = [String: String]()
        var refIds : String = "\(mobile)\(tss.0)"
        refIds = refId == "NA" ? refIds : refId
        
        param["refId"] = refIds
        param["ts"] = ts.0
        param["reqAction"]  = reqAction
        param["clientIp"]   = ip
        param["location"]   = location
        param["geoLat"]     = geoLat
        param["geoLong"]     = geoLong
        
        var isValid = true
        if user.appName == "CSC Pay" {
            param["appName"] = "CSC Pay"
        } else { isValid = false }
        
        if user.appVer == appVersion ?? "1" {
            param["appVer"] = appVersion ?? "1"
        } else { isValid = false }
        
        if user.deviceId == getUUID() {
            param["deviceId"] = getUUID() ?? "NA"
        } else { isValid = false }
        
        if user.deviceType == deviceType {
            param["deviceType"] = deviceType
        } else { isValid = false }
        
        if user.simId == "\(mobile)\(mobile.prefix(8))0" {
            param["simId"] = "\(mobile)\(mobile.prefix(8))0"
        } else { isValid = false }
        
        if user.osVer == osVer {
            param["osVer"] = osVer
        } else { isValid = false }
        
        if user.bluetoothMac == "02:00:00:00:00:00" {
            param["bluetoothMac"] = "02:00:00:00:00:00"
        } else { isValid = false }
        
        if user.wifiMac == "02:00:00:00:00:00" {
            param["wifiMac"] = "02:00:00:00:00:00"
        } else { isValid = false }
        
        
        
        
        if reqAction == "getDisputeList" || reqAction == "raiseDispute" || reqAction == "getdisputestatus"{
            if let pspData = HelperUtil().getPSPData(pspId: pspId){
               return [ "appName": "CSC Pay","appVer": appVersion ?? "1.0","deviceId": getUUID() ?? "NA","deviceType": deviceType,"osVer": osVer,"refId": refIds,"ts": ts.0,"reqAction": reqAction,"clientIp": ip,"location": location,"geoLat": geoLat,"geoLong": geoLong,"pspId": pspId ?? "IBL","pspData": "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTBFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiMTAuOTEuMjYuMzQiLA0KCSJibHVldG9vdGhNYWMiOiAiMjI6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUwRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUwRSIsDQoJIm9zIjogIklPUyAxMy4yLjMiLA0KCSJyZWdJZCI6ICJOQSIsDQoJInNlbGVjdGVkU2ltU2xvdCI6ICIwIiwNCgkiZmNtVG9rZW4iOiAiIg0KfQ"]
            }
        }
        
        
        
        
        
        
        
        
        
        if reqAction == "setMpinInit"{
            if let pspData  = HelperUtil().getPSPData(pspId: pspId){
                return ["appName":"CSC Pay", "pspId": "IBL" , "appVer": appVersion ?? "1.0", "deviceId": getUUID() ?? "NA", "deviceType":deviceType, "osVer":osVer, "bluetoothMac":"02:00:00:00:00:00", "wifiMac":"02:00:00:00:00:00", "refId":refIds, "ts":ts.0, "reqAction":reqAction, "clientIp":ip, "location":location, "geoLat":geoLat, "geoLong":geoLong , "pspData": "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTFFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiNDU6MjI6NjU6Q0U6MDA6QzEiLA0KCSJibHVldG9vdGhNYWMiOiAiMjc6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUxRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjg5OTE3NTIxNzAwNjA3NDU0MjUiLA0KCSJvcyI6ICJJT1MgMTMuMi4zIiwNCgkicmVnSWQiOiAiTkEiLA0KCSJzZWxlY3RlZFNpbVNsb3QiOiAiMCIsDQoJImZjbVRva2VuIjogIiINCn0"]
//                "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTFFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiNDU6MjI6NjU6Q0U6MDA6QzEiLA0KCSJibHVldG9vdGhNYWMiOiAiMjc6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUxRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjg5OTE3NTIxNzAwNjA3NDU0MjUiLA0KCSJvcyI6ICJJT1MgMTMuMi4zIiwNCgkicmVnSWQiOiAiTkEiLA0KCSJzZWxlY3RlZFNpbVNsb3QiOiAiMCIsDQoJImZjbVRva2VuIjogIiINCn0"
            }
            
        }
        //MARK: - "setMpinConf"

        if reqAction == "setMpinConf"{
            return ["appName":"CSC Pay", "pspId": "IBL" , "appVer": appVersion ?? "1.0", "deviceId": getUUID() ?? "NA", "deviceType":deviceType, "osVer":osVer, "bluetoothMac":"02:00:00:00:00:00", "wifiMac":"02:00:00:00:00:00", "refId":refIds, "ts":ts.0, "reqAction":reqAction, "clientIp":ip, "location":location, "geoLat":geoLat, "geoLong":geoLong , "pspData": "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTFFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiNDU6MjI6NjU6Q0U6MDA6QzEiLA0KCSJibHVldG9vdGhNYWMiOiAiMjc6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUxRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjg5OTE3NTIxNzAwNjA3NDU0MjUiLA0KCSJvcyI6ICJJT1MgMTMuMi4zIiwNCgkicmVnSWQiOiAiTkEiLA0KCSJzZWxlY3RlZFNpbVNsb3QiOiAiMCIsDQoJImZjbVRva2VuIjogIiINCn0"]
        }
        
        //MARK: - AddAccount and Init Payment
        if reqAction == "addAccount" || reqAction == "initPayment"{
            if let pspData = HelperUtil().getPSPData(pspId: pspId){
                return ["appName":"CSC Pay", "pspId": pspId ?? "IBL" , "appVer": appVersion ?? "1.0", "deviceId": getUUID() ?? "NA", "deviceType":deviceType, "osVer":osVer, "bluetoothMac":"02:00:00:00:00:00", "wifiMac":"02:00:00:00:00:00", "refId":refIds, "ts":ts.0, "reqAction":reqAction, "clientIp":ip, "location":location, "geoLat":geoLat, "geoLong":geoLong , "pspData": "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTFFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiNDU6MjI6NjU6Q0U6MDA6QzEiLA0KCSJibHVldG9vdGhNYWMiOiAiMjc6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUxRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjg5OTE3NTIxNzAwNjA3NDU0MjUiLA0KCSJvcyI6ICJJT1MgMTMuMi4zIiwNCgkicmVnSWQiOiAiTkEiLA0KCSJzZWxlY3RlZFNpbVNsb3QiOiAiMCIsDQoJImZjbVRva2VuIjogIiINCn0"]
            }
        }
        
        
        
        
        if reqAction == "getHistory"{
            if let pspData = HelperUtil().getPSPData(pspId: pspId){
                print(pspData)
                return ["appName": "CSC Pay","appVer": appVersion ?? "1.0","deviceId": getUUID() ?? "NA","deviceType": deviceType,"osVer": osVer,"refId": refIds,"ts": ts.0,"reqAction": "getHistory","clientIp": ip,"location": location,"geoLat": geoLat,"geoLong": geoLong,"pspId": pspId ?? "IBL","pspData":  "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTBFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiMTAuOTEuMjYuMzQiLA0KCSJibHVldG9vdGhNYWMiOiAiMjI6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUwRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUwRSIsDQoJIm9zIjogIklPUyAxMy4yLjMiLA0KCSJyZWdJZCI6ICJOQSIsDQoJInNlbGVjdGVkU2ltU2xvdCI6ICIwIiwNCgkiZmNtVG9rZW4iOiAiIg0KfQ"
                ]
            }
           
        }
        if reqAction == "confPayment"{
            if let pspData = HelperUtil().getPSPData(pspId: pspId){
                print(pspData)
                return ["appName": "CSC Pay","appVer": appVersion ?? "1.0","deviceId": getUUID() ?? "NA","deviceType": deviceType,"osVer": osVer,"refId": refIds,"ts": ts.0,"reqAction": "confPayment","clientIp": ip,"location": location,"geoLat": geoLat,"geoLong": geoLong,"pspId": pspId ?? "IBL","pspData":  "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTBFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiMTAuOTEuMjYuMzQiLA0KCSJibHVldG9vdGhNYWMiOiAiMjI6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUwRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUwRSIsDQoJIm9zIjogIklPUyAxMy4yLjMiLA0KCSJyZWdJZCI6ICJOQSIsDQoJInNlbGVjdGVkU2ltU2xvdCI6ICIwIiwNCgkiZmNtVG9rZW4iOiAiIg0KfQ"
                        //pspData
                ]
            }
        }
        //MARK: - Merchant
        
        if isMerchant{
            var tempMobile = "9876543210"
            if let user = CoredataManager.getDetail(App_User.self) {
                let mob = user.mobile ?? "NA"
                if mob.count >= 10 { tempMobile = mob }
                else { return nil }
            }
            if let pspData = HelperUtil().getPSPData(pspId: pspId){
                param["pspData"] =  "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTFFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiNDU6MjI6NjU6Q0U6MDA6QzEiLA0KCSJibHVldG9vdGhNYWMiOiAiMjc6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUxRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjg5OTE3NTIxNzAwNjA3NDU0MjUiLA0KCSJvcyI6ICJJT1MgMTMuMi4zIiwNCgkicmVnSWQiOiAiTkEiLA0KCSJzZWxlY3RlZFNpbVNsb3QiOiAiMCIsDQoJImZjbVRva2VuIjogIiINCn0"
               // pspData
                if reqAction == "subMerchant"{
                    return ["appName":"CSC Pay", "pspId": pspId ?? "IBL" , "appVer": appVersion ?? "1.0", "deviceId": getUUID() ?? "NA", "deviceType":deviceType, "osVer":osVer,"refId":refIds, "ts":ts.0, "reqAction":reqAction, "clientIp":ip, "location":location, "geoLat":geoLat, "geoLong":geoLong , "pspData": "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTFFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiNDU6MjI6NjU6Q0U6MDA6QzEiLA0KCSJibHVldG9vdGhNYWMiOiAiMjc6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUxRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjg5OTE3NTIxNzAwNjA3NDU0MjUiLA0KCSJvcyI6ICJJT1MgMTMuMi4zIiwNCgkicmVnSWQiOiAiTkEiLA0KCSJzZWxlY3RlZFNpbVNsb3QiOiAiMCIsDQoJImZjbVRva2VuIjogIiINCn0"]
                }else if reqAction == "directMerchant"{
                    return ["appName":"CSC Pay", "pspId": pspId ?? "IBL" , "appVer": appVersion ?? "1.0", "deviceId": getUUID() ?? "NA", "deviceType":deviceType, "osVer":osVer, "bluetoothMac":"02:00:00:00:00:00", "wifiMac":"02:00:00:00:00:00", "refId":refIds, "ts":ts.0, "reqAction":reqAction, "clientIp":ip, "location":location, "geoLat":geoLat, "geoLong":geoLong , "pspData": "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTFFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiNDU6MjI6NjU6Q0U6MDA6QzEiLA0KCSJibHVldG9vdGhNYWMiOiAiMjc6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUxRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjg5OTE3NTIxNzAwNjA3NDU0MjUiLA0KCSJvcyI6ICJJT1MgMTMuMi4zIiwNCgkicmVnSWQiOiAiTkEiLA0KCSJzZWxlY3RlZFNpbVNsb3QiOiAiMCIsDQoJImZjbVRva2VuIjogIiINCn0"]
                }else if reqAction == "checkVPA" || reqAction == "txnStatus" || reqAction == "txnHistory" || reqAction == "txnRefund" {
                    return ["appName":"CSC Pay", "pspId": pspId ?? "IBL" , "appVer": appVersion ?? "1.0", "deviceId": getUUID() ?? "NA", "deviceType":deviceType, "osVer":osVer,"refId":refIds, "ts":ts.0, "reqAction":reqAction, "clientIp":ip, "location":location, "geoLat":geoLat, "geoLong":geoLong , "pspData": "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTFFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiNDU6MjI6NjU6Q0U6MDA6QzEiLA0KCSJibHVldG9vdGhNYWMiOiAiMjc6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUxRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjg5OTE3NTIxNzAwNjA3NDU0MjUiLA0KCSJvcyI6ICJJT1MgMTMuMi4zIiwNCgkicmVnSWQiOiAiTkEiLA0KCSJzZWxlY3RlZFNpbVNsb3QiOiAiMCIsDQoJImZjbVRva2VuIjogIiINCn0"]
                }else if reqAction == "initCollect"{
                    return ["appName":"CSC Pay", "pspId": pspId ?? "IBL" , "appVer": appVersion ?? "1.0", "deviceId": getUUID() ?? "NA", "deviceType":deviceType, "osVer":osVer,"refId":refIds, "ts":ts.0, "reqAction":reqAction, "clientIp":ip, "location":location, "geoLat":geoLat, "geoLong":geoLong , "pspData": "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTFFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiNDU6MjI6NjU6Q0U6MDA6QzEiLA0KCSJibHVldG9vdGhNYWMiOiAiMjc6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUxRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjg5OTE3NTIxNzAwNjA3NDU0MjUiLA0KCSJvcyI6ICJJT1MgMTMuMi4zIiwNCgkicmVnSWQiOiAiTkEiLA0KCSJzZWxlY3RlZFNpbVNsb3QiOiAiMCIsDQoJImZjbVRva2VuIjogIiINCn0"]
                }else if reqAction == "checkMerchant" || reqAction == "regMerchant" || reqAction == "activateMerchant" || reqAction == "docUpload" {
                    return ["appName":"CSC Pay", "pspId": pspId ?? "IBL" , "appVer": appVersion ?? "1.0", "deviceId": getUUID() ?? "NA", "deviceType":deviceType, "osVer":osVer,"refId":refIds, "ts":ts.0, "reqAction":reqAction, "clientIp":ip, "location":location, "geoLat":geoLat, "geoLong":geoLong , "pspData": "NA"]
                }
            }
        }
        //TODO: Chekc why isValid use
       // if !isValid { return nil }
        
        //
        if reqAction == "checkDevice" || reqAction == "secretQuestion" || reqAction == "otpService" || reqAction == "customerProfle" || reqAction == "bankList"  || reqAction == "checkVPA" || reqAction == "getToken" || reqAction == "listKeys" || reqAction == "registerVPA" || reqAction == "getDisputeList" || reqAction == "addAccount" ||  reqAction == "getBankAccountList" || reqAction == "setDefault" || reqAction == "removeAccount" || reqAction == "initBalance" || reqAction == "confBalance" || reqAction == "directMerchant" || reqAction == "setMpinInit" {
            if let pspData = HelperUtil().getPSPData(pspId: pspId) {
                print(pspData)
                return ["appName":"CSC Pay", "pspId": pspId ?? "IBL" , "appVer": appVersion ?? "1.0", "deviceId": getUUID() ?? "NA", "deviceType":deviceType, "osVer":osVer, "refId":refIds, "ts":ts.0, "reqAction":reqAction, "clientIp":ip, "location":location, "geoLat":geoLat, "geoLong":geoLong ,"pspData": "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTFFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiNDU6MjI6NjU6Q0U6MDA6QzEiLA0KCSJibHVldG9vdGhNYWMiOiAiMjc6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUxRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjg5OTE3NTIxNzAwNjA3NDU0MjUiLA0KCSJvcyI6ICJJT1MgMTMuMi4zIiwNCgkicmVnSWQiOiAiTkEiLA0KCSJzZWxlY3RlZFNpbVNsb3QiOiAiMCIsDQoJImZjbVRva2VuIjogIiINCn0"] //
                //pspData
            } else { return nil }
        }
        return param
    }
    
    
//MARK: -  Common Body Function ---------------------------------------------------------------------------------------

    static func getBody(reqAction: String, category: String = "NA", resHash: String = "NA", otpHash : String = "NA",pspId: String? = nil,extra: String? = nil, isNew: Bool = false , expiryDate: String = "NA" , lastSixDebCard : String = "NA" , accountId : Int = 00000 , virtualAddress: String = "NA" , npciTransId: String = "NA" , otpNpciTranId : String = "NA" , credDataValue : String = "NA", pspRespRefNo: String = "NA" , accountStatus : String = "NA" , customerName : String = "NA" , bankCode : String = "NA" , channel: String = "M") -> [String: Any]? {
        var mobile = "9876543210"
        
        if let user = CoredataManager.getDetail(App_User.self) {
            let mob = user.mobile ?? "NA"
            if mob.count >= 10 { mobile = mob }
            else { return nil }
        }
        if reqAction == "config" {
            return ["mobileNo":mobile, "category":category, "deviceVer":"2"]
        } else if reqAction == "label" {
            return ["mobileNo":mobile, "category":category, "deviceVer":"2"]
        } else if reqAction == "bank" {
            return ["mobileNo":mobile, "category":category, "deviceVer":"2"]
        } else if reqAction == "generate" || reqAction == "resend" {
            return ["mobileNo":category, "otpChannel":channel, "resHash": resHash, "otpHash" : otpHash]
        } else if reqAction == "validate" {
            return ["mobileNo":category, "otpChannel":"M", "resHash": resHash, "otpHash" : otpHash]
        } else if reqAction == "check" {
            return ["mobileNo":mobile, "userType":category]
        } else if reqAction == "checkDevice" {
            return ["mobileNo":mobile, "pspBank":category]
        } else if reqAction == "secretQuestion" {
            return ["mobileNo":mobile, "pspBank":category]
        } else if reqAction == "otpService" {
            return ["mobileNo":mobile, "pspBank":category]
        } else if reqAction == "customerProfle" {
            return ["mobileNo":mobile, "pspBank":category]
        } else if reqAction == "bankList" {
            return ["mobileNo":mobile, "pspBank":category]
        }else if reqAction == "ifsc"{
            return ["mobileNo":mobile, "category":category , "deviceVer":"2"]
        } else if reqAction == "getBankAccountList" {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            guard let psp = pspId else {
                return nil
            }
            // Need to get bankCode based on pspId
            return ["mobileNo":mobile, "pspBank":category, "regType": "R", "customerName" : user.name ?? user.mobile ?? "NA", "bankCode": "INDB"]
        } else if reqAction == "getToken" || reqAction == "listKeys" {
            guard let npciData = CoredataManager.getDetail(List_Key.self) else{ return nil }
            
            let credDataValue = npciData.keyValue ?? "sdsdsdsdsds"
            
            guard let txnType = extra else {
                return nil
            }
            if txnType == "dispute" {
                return ["mobileNo":mobile, "pspBank":category, "upiTranRefNo" :"upiTranRefNo", "ticketNo":"4064", "requestType":"A", "statusIs":"O", "disputeRemark":"test", "disputeType":"U008", "custRefNo": "031015064387"]
            }
            return ["mobileNo":mobile, "pspBank":category, "txnType" :txnType , "credType":"CHALLENGE","credSubType":"INITIAL","credDataCode":"NPCI","credDataKi":"20150822","credDataValue":credDataValue]
        } else if reqAction == "checkVPA" {
            guard let vpa = extra else {
                return nil
            }
            return ["mobileNo":mobile, "pspBank":category, "virtualAddress" : vpa, "vpaMode":"R"]
        } else if reqAction == "registerVPA" {
//            guard let vpa = extra else {
//                return nil
//            }
            guard let user = CoredataManager.getDetail(App_User.self) else {return nil}
            
            guard let bankAccountDetails = CoredataManager.getDetail(App_UserBankAccount_Details.self) else{return nil}
            
            
            //Need to make dynamic values - get bank details using bankCode
//            return ["mobileNo":mobile, "pspBank":category, "pspRespRefNo" : "41269", "accountStatus":"S", "customerName" : user.name ?? user.mobile ?? "NA", "bankCode" : "INDB", "accId": "45117","virtualAddress": "9999894870@cscindus", "email": user.mail ?? "gowtham@gmail.com","secretAnswer": "test","quesId": "1"]
            
            return ["mobileNo":mobile, "pspBank":category, "pspRespRefNo" : bankAccountDetails.pspRespRefNo ?? "41269" , "accountStatus":bankAccountDetails.status ?? "O", "customerName" : user.name ?? user.mobile ?? "NA", "bankCode" : "INDB", "accId": user.accountId ?? "45117","virtualAddress": extra, "email": user.mail ?? "gowtham@gmail.com","secretAnswer": "test","quesId": "1"]
            
            
        } else if reqAction == "getDisputeList" {
            
            return ["mobileNo":mobile, "pspBank":category, "upiTranRefNo" :"1234", "ticketNo":"4064", "requestType":"A", "statusIs":"O", "disputeRemark":"test", "disputeType":"U008", "custRefNo": "031015064387"]
            
        } else if reqAction == "addAccount" {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            
            guard let bankdata = CoredataManager.getDetail(App_UserBankAccount_Details.self) else {return nil}
            
//            return ["mobileNo":mobile, "pspBank":category, "pspRespRefNo" : bankdata.pspRespRefNo ?? "41269", "accountStatus": bankdata.status ?? "NA", "customerName":user.vpaName ?? user.name ?? "NA", "statusIs":"O", "disputeRemark":"test", "disputeType":"U008", "custRefNo": "031015064387"]
            
            
            return ["pspBank":category,"mobileNo":mobile, "pspRespRefNo": bankdata.pspRespRefNo ?? "41269", "accountStatus":bankdata.status ?? "NA","customerName": user.name ?? user.mobile ?? "NA", "bankCode": bankCode ,  "accountId": user.accountId ?? "45117","virtualAddress": extra]
            
            
        } else if reqAction == "setDefault" {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            guard let vpa = user.virtualAddress else {
                return nil
            }
            
            guard let bankList = CoredataManager.getDetails(Account_List.self) else {return nil}
            
            
            
            //Need to get Accoun details from the database and to pass in accountList
            
//            ["accId":"28836", "accountNumber":"878998998998898", "maskedAccountNumber" : "XXXXXX8898", "aepsFlag":"N", "bankCode" : "AABD", "bankId": "0" ,"bankName":"ABC","checkedAccount":false,"ifscCode":"AABD0000001","preferredFlag":"F","mpinFlag":"Y","uPinLength": "6" ]
            
            return ["mobileNo":mobile, "pspBank":category, "virtualAddress" : vpa, "reqType":"DA", "accountList" : ["accId":"28836", "accountNumber":"878998998998898", "maskedAccountNumber" : "XXXXXX8898", "aepsFlag":"N", "bankCode" : "AABD", "bankId": "0" ,"bankName":"ABC","checkedAccount":false,"ifscCode":"AABD0000001","preferredFlag":"F","mpinFlag":"Y","uPinLength": "6" ] ]
        } else if reqAction == "removeAccount" {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            guard let vpa = user.virtualAddress else {
                return nil
            }
            
            guard let bankdata = CoredataManager.getDetail(App_UserBankAccount_Details.self) else {return nil}

            
            //Need to get Account details from the database and to pass in accountList
            
            return ["mobileNo":mobile, "pspBank":category, "virtualAddress" : vpa, "reqType":"RA", "accountList" : ["accId":"28836", "accountNumber":"878998998998898", "maskedAccountNumber" : "XXXXXX8898", "aepsFlag":"N", "bankCode" : "AABD", "bankId": "0" ,"bankName":"ABC","checkedAccount":false,"ifscCode":"AABD0000001","preferredFlag":"F","mpinFlag":"Y","uPinLength": "6" ] ]
        } else if reqAction == "initBalance" {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            // Make default vpa address and store into this User DB
            guard let vpa = user.virtualAddress else {
                return nil
            }
            // account id will get from the default bankaccount
            return ["mobileNo":mobile, "pspBank":category, "virtualAddress" :vpa, "accountId":"28063", "npciTransId": "NA"]
        } else if reqAction == "confBalance" {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            // Make default vpa address and store into this User DB
            guard let vpa = user.virtualAddress else {
                return nil
            }
            // account id will get from the default bankaccount
            return ["mobileNo":mobile, "pspBank":category, "virtualAddress": vpa, "accountId": user.accountId ?? "28063", "npciTransId":"INDBB334F4F3EFAB095AE0539F42180A086", "mpinCred": ["atmCrdLength":"0", "credentialDataCode":"NPCI", "credentialDataKi":"20150822", "credentialDataLength":"0", "credentialDataValue":"Mi4wfE84TjZOckRqUjhsWGxKYkRJbnQzOHM1U1ZGTlc5L0d4MmZWcVlLR3RYNXVBSzNVRi81K0Z3\nZUNEeGVlQlBwUGhTY21wSm0yVTM0TGgwTndxWUNTUXVac2l0NW5xa1pjTTg3am1zakdYK2tUTkVC\nUlN2SmhWMDE4ZC9DR3FVMnFZWVFEdEhOODF6RE1xL1F2UFhOczlHNEVsYXpPb1RxZmwxcytrNWhF\nNmF1TWcrRC9FbXNWcVkrVDZQK1g1SnJtTGtTcUl1OFZzNW5renppdjlzY0RQdXgyR2VlME5WUi9T\nME02VnBXdCtYbGNidWNvNTdIbERRRHlUVVFCYWh3MXB2Q1IwQWcyRlRpVk9lUmRTMmI1bTNFTmFk\\nYWR3dz09\n", "credentialSubType":"MPIN", "credentialType":"PIN", "otpCrdLength":0]]
        }else if reqAction == "directMerchant"{
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            return  ["pspBank": category,"mobileNo": user.mobile,"pgMerchantId": "INDB000000029154","isMerchant": "yes","merchantName": "kesavaindus008","legalName": "Thoppil Mart","virtualAddress": "kesava008@indus","merCategoryCode": "6012","panNo": "AFGHQY007Q","gstin": "ABCD","udyogId": "ABCD1234","cityName": "Parel","stateName": "13","pinCode": "402201","address1": "RCF","address2": "THAL","address3": "TKIET","billingFax": "224504","contactName": "vicky","contactPhone": "0222255885508","contactEmail": "kesava008@gmail.com","loginEmail": "kesava008indus@gmail.com","merchantType": "DIRMER","integrationType": "WEBAPI","settleType": "GROSS","accountNo": "1456955456323568","ifscCode": "ICIC0001214","bankCode": "ICICI","requestUrl1": "https://indusupiuat.indusind11111.com","requestUrl2": "https://ind.indust222222.com","requestUrl3": "https://ind.indust3333.com","requestUrl4": "https://ind.indust44444.com"]
        }else if reqAction == "setMpinInit"{
            guard let user = CoredataManager.getDetail(App_User.self) , let virtualAddress = user.virtualAddress else {
                return nil
            }
            return ["pspBank":"IndusInd","mobileNo":category,"accountId": accountId,"cardType":"O", "expiryDate": expiryDate, "lastSixDebCard": lastSixDebCard,  "virtualAddress": virtualAddress]
        }else if reqAction == "setMpinConf"{
            if let otpCred = HelperUtil().getOtpCred(credentialDataValue: credDataValue, reqAction: "setMpinConf"){
                return ["pspBank": extra,"mobileNo":category,"accountId": accountId, "npciTransId" : npciTransId , "otpNpciTranId": otpNpciTranId , "otpCred": otpCred,
                        "mpinCred" :["atmCrdLength": 0,"credentialDataCode": "NPCI","credentialDataKi": "20150822","credentialDataLength": 0,"credentialDataValue": credDataValue , "credentialSubType": "MPIN","credentialType": "PIN","otpCrdLength": 0]
                ]
            }
            
        }else if reqAction == "getHistory"{
            guard let user = CoredataManager.getDetail(App_User.self) , let virtualAddress = user.virtualAddress else {
                return nil
            }
            return ["pspBank": extra ?? "NA","mobileNo": user.mobile ?? "NA","virtualAddress": user.virtualAddress ?? "NA","requestType": "QT"]
            
        }
        return nil

        }
  
    
    
    
    
    func getOtpCred(atmCrdLength: Int = 0 , credentialDataCode: String = "NPCI" ,credentialDataKi : String = "NA", credentialDataLength: Int = 0 , credentialDataValue: String = "NA", reqAction: String = "NA" , credentialSubType: String = "SMS" ,credentialType: String = "OTP", otpCrdLength: Int = 0) -> [String:Any]?{
    var otpCred = [String:Any]()
        if reqAction == "setMpinConf"{
            return [ "atmCrdLength": atmCrdLength,"credentialDataCode":credentialDataCode,"credentialDataKi": "20150822","credentialDataLength":credentialDataLength,"credentialDataValue":credentialDataValue, "credentialSubType": credentialSubType , "credentialType": credentialType , "otpCrdLength": otpCrdLength]
        }
    return nil
    }
    
    
    
    
    //MARK:- Body Merchant -----------------------------------------------------------------
    
    static func getBodyMerchant(reqAction: String, category: String = "NA", resHash: String = "NA", otpHash : String = "NA",pspId: String? = nil,extra: String? = nil, isNew: Bool = false , expiryDate: String = "NA" , lastSixDebCard : String = "NA" , accountId : Int = 00000 , virtualAddress: String = "NA" , npciTransId: String = "NA" , otpNpciTranId : String = "NA" , credDataValue : String = "NA") -> [String: Any]? {
        var mobile = "9876543210"
        
        guard let user = CoredataManager.getDetail(App_User.self)else { return nil }
        
        let mob = user.mobile ?? "NA"
        if mob.count >= 10 { mobile = mob }
        
        //MARK: - Direct Merchant
        
//        switch reqAction{
//        case "directMerchant":
//            return ["pspBank": category,"mobileNo": user.mobile,"pgMerchantId": "INDB000000029154","isMerchant": "yes","merchantName": "kesavaindus008","legalName": "Thoppil Mart","virtualAddress": "kesava008@indus","merCategoryCode": "6012","panNo": "AFGHQY007Q","gstin": "ABCD","udyogId": "ABCD1234","cityName": "Parel","stateName": "13","pinCode": "402201","address1": "RCF","address2": "THAL","address3": "TKIET","billingFax": "224504","contactName": "vicky","contactPhone": "0222255885508","contactEmail": "kesava008@gmail.com","loginEmail": "kesava008indus@gmail.com","merchantType": "DIRMER","integrationType": "WEBAPI","settleType": "GROSS","accountNo": "1456955456323568","ifscCode": "ICIC0001214","bankCode": "ICICI","requestUrl1": "https://indusupiuat.indusind11111.com","requestUrl2": "https://ind.indust222222.com","requestUrl3": "https://ind.indust3333.com","requestUrl4": "https://ind.indust44444.com"]
//        default:
//            return ["pspBank":category]
//        }
        
        
        
        if reqAction == "directMerchant" {
            return ["mobileNo":mobile, "category":category, "deviceVer":"2"]
        } else if reqAction == "getBankAccountList" {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            guard let psp = pspId else {
                return nil
            }
            // Need to get bankCode based on pspId
            return ["mobileNo":mobile, "pspBank":category, "regType": "R", "customerName" : user.name ?? user.mobile ?? "NA", "bankCode": "INDB"]
        } else if reqAction == "subMerchant" {
            guard let txnType = extra else {
                return nil
            }
            if txnType == "checkVPA" {
                return ["mobileNo":mobile, "pspBank":category, "upiTranRefNo" :"upiTranRefNo", "ticketNo":"4064", "requestType":"A", "statusIs":"O", "disputeRemark":"test", "disputeType":"U008", "custRefNo": "031015064387"]
            }
            return ["mobileNo":mobile, "pspBank":category, "txnType" :txnType]
        } else if reqAction == "initCollect" {
            guard let vpa = extra else {
                return nil
            }
            return ["mobileNo":mobile, "pspBank":category, "virtualAddress" : vpa, "vpaMode":"R"]
        } else if reqAction == "txnStatus" {
//            guard let vpa = extra else {
//                return nil
//            }
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            //Need to make dynamic values - get bank details using bankCode
            return ["mobileNo":mobile, "pspBank":category, "pspRespRefNo" : "41269", "accountStatus":"S", "customerName" : user.name ?? user.mobile ?? "NA", "bankCode" : "INDB", "accId": "45117","virtualAddress": "9999894870@cscindus", "email": user.mail ?? "gowtham@gmail.com","secretAnswer": "test","quesId": "1"]
        } else if reqAction == "txnHistory" {
            
            return ["mobileNo":mobile, "pspBank":category, "upiTranRefNo" :"1234", "ticketNo":"4064", "requestType":"A", "statusIs":"O", "disputeRemark":"test", "disputeType":"U008", "custRefNo": "031015064387"]
            
        } else if reqAction == "txnRefund" {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            return ["mobileNo":mobile, "pspBank":category, "pspRespRefNo" :"41269", "accountStatus":"S", "customerName":user.vpaName ?? user.name ?? "NA", "statusIs":"O", "disputeRemark":"test", "disputeType":"U008", "custRefNo": "031015064387"]
        } else if reqAction == "setDefault" {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            guard let vpa = user.virtualAddress else {
                return nil
            }
            //Need to get Accoun details from the database and to pass in accountList
            
            return ["mobileNo":mobile, "pspBank":category, "virtualAddress" : vpa, "reqType":"DA", "accountList" : ["accId":"28836", "accountNumber":"878998998998898", "maskedAccountNumber" : "XXXXXX8898", "aepsFlag":"N", "bankCode" : "AABD", "bankId": "0" ,"bankName":"ABC","checkedAccount":false,"ifscCode":"AABD0000001","preferredFlag":"F","mpinFlag":"Y","uPinLength": "6" ] ]
        } else if reqAction == "removeAccount" {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            guard let vpa = user.virtualAddress else {
                return nil
            }
            //Need to get Accoun details from the database and to pass in accountList
            
            return ["mobileNo":mobile, "pspBank":category, "virtualAddress" : vpa, "reqType":"DA", "accountList" : ["accId":"28836", "accountNumber":"878998998998898", "maskedAccountNumber" : "XXXXXX8898", "aepsFlag":"N", "bankCode" : "AABD", "bankId": "0" ,"bankName":"ABC","checkedAccount":false,"ifscCode":"AABD0000001","preferredFlag":"F","mpinFlag":"Y","uPinLength": "6" ] ]
        } else if reqAction == "initBalance" {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            // Make default vpa address and store into this User DB
            guard let vpa = user.virtualAddress else {
                return nil
            }
            // account id will get from the default bankaccount
            return ["mobileNo":mobile, "pspBank":category, "virtualAddress" :vpa, "accountId":"28063", "npciTransId": "NA"]
        } else if reqAction == "confBalance" {
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            // Make default vpa address and store into this User DB
            guard let vpa = user.virtualAddress else {
                return nil
            }
            // account id will get from the default bankaccount
            return ["mobileNo":mobile, "pspBank":category, "virtualAddress": vpa, "accountId": "28063", "npciTransId":"INDBB334F4F3EFAB095AE0539F42180A086", "mpinCred": ["atmCrdLength":"0", "credentialDataCode":"NPCI", "credentialDataKi":"20150822", "credentialDataLength":"0", "credentialDataValue":"Mi4wfE84TjZOckRqUjhsWGxKYkRJbnQzOHM1U1ZGTlc5L0d4MmZWcVlLR3RYNXVBSzNVRi81K0Z3\nZUNEeGVlQlBwUGhTY21wSm0yVTM0TGgwTndxWUNTUXVac2l0NW5xa1pjTTg3am1zakdYK2tUTkVC\nUlN2SmhWMDE4ZC9DR3FVMnFZWVFEdEhOODF6RE1xL1F2UFhOczlHNEVsYXpPb1RxZmwxcytrNWhF\nNmF1TWcrRC9FbXNWcVkrVDZQK1g1SnJtTGtTcUl1OFZzNW5renppdjlzY0RQdXgyR2VlME5WUi9T\nME02VnBXdCtYbGNidWNvNTdIbERRRHlUVVFCYWh3MXB2Q1IwQWcyRlRpVk9lUmRTMmI1bTNFTmFk\\nYWR3dz09\n", "credentialSubType":"MPIN", "credentialType":"PIN", "otpCrdLength":0]]
        }else if reqAction == "directMerchant"{
            guard let user = CoredataManager.getDetail(App_User.self) else {
                return nil
            }
            return  ["pspBank": category,"mobileNo": user.mobile,"pgMerchantId": "INDB000000029154","isMerchant": "yes","merchantName": "kesavaindus008","legalName": "Thoppil Mart","virtualAddress": "kesava008@indus","merCategoryCode": "6012","panNo": "AFGHQY007Q","gstin": "ABCD","udyogId": "ABCD1234","cityName": "Parel","stateName": "13","pinCode": "402201","address1": "RCF","address2": "THAL","address3": "TKIET","billingFax": "224504","contactName": "vicky","contactPhone": "0222255885508","contactEmail": "kesava008@gmail.com","loginEmail": "kesava008indus@gmail.com","merchantType": "DIRMER","integrationType": "WEBAPI","settleType": "GROSS","accountNo": "1456955456323568","ifscCode": "ICIC0001214","bankCode": "ICICI","requestUrl1": "https://indusupiuat.indusind11111.com","requestUrl2": "https://ind.indust222222.com","requestUrl3": "https://ind.indust3333.com","requestUrl4": "https://ind.indust44444.com"]
        }else if reqAction == "setMpinInit"{
            guard let user = CoredataManager.getDetail(App_User.self) , let virtualAddress = user.virtualAddress else {
                return nil
            }
            return ["pspBank":"IndusInd","mobileNo":category,"accountId": accountId,"cardType":"O", "expiryDate": expiryDate, "lastSixDebCard": lastSixDebCard,  "virtualAddress": virtualAddress]
        }else if reqAction == "setMpinConf"{
            if let otpCred = HelperUtil().getOtpCred(credentialDataValue: credDataValue, reqAction: "setMpinConf"){
                return ["pspBank": extra,"mobileNo":category,"accountId": accountId, "npciTransId" : npciTransId , "otpNpciTranId": otpNpciTranId , "otpCred": otpCred,
                        "mpinCred" :["atmCrdLength": 0,"credentialDataCode": "NPCI","credentialDataKi": "20150822","credentialDataLength": 0,"credentialDataValue": credDataValue , "credentialSubType": "MPIN","credentialType": "PIN","otpCrdLength": 0]
                ]
            }
            
        }
        return nil

        }
    
    //MARK:- Body and Request for Payments APIs
    
    
    static func getRequestPayment(reqAction: String, category: String = "NA", refId: String = "NA",  pspId: String? = nil, extra: String? = nil, mobCrypt: Bool = false, isNew : Bool = false , isMerchant : Bool = false ,payeeType: [String:Any] = ["NA": "NA"] , payerType: [String:Any] = ["NA":"NA"] , amount: String = "1.00" , upiTransRefNo:Int = 0 , transactionNote: String = "NA" , isBharatQr: Bool = false , initiationMode : String = "00" ,expiryTime : Int = 0) -> String?{
        
        var params = [String: Any]()
        var head = [String: Any]()
        var body = [String: Any]()
        
        if let header = HelperUtil.getCommonHeader(reqAction: reqAction, mobile: category, refId : refId, pspId: pspId, extra : extra , isNew: isNew , isMerchant: isMerchant) {
            head = header
        } else { return nil }
    

        if let bodyData = HelperUtil.getBodyPayment(reqAction: reqAction, category: category, pspId: pspId, extra: extra, isNew: isNew, expiryDate: "NA", amount: amount, expiryTime: expiryTime, initiationMode: initiationMode, isBharatQr: isBharatQr, transactionNote: transactionNote, upiTransRefNo: upiTransRefNo, payeeType: payeeType, payerType: payerType) {
            body = bodyData
        } else { return nil }
        
        
        
        params = ["head" : head, "body" : body]
        if Constants.printResponse { print(params) }
        let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
        let decode = String(data: jsonData, encoding: .utf8)
        guard let decoded = decode else { return nil }
        
        guard let reqData = encrypt(data : decoded, reqAction: reqAction, mobCrypt : mobCrypt, mob: category, isNew : isNew) else {
            return nil
        }
        return reqData
    }
    
    
    static func getBodyPayment(reqAction: String, category: String = "NA",pspId: String? = nil,extra: String? = nil, isNew: Bool = false , expiryDate: String = "NA" , amount : String = "1.00" , expiryTime : Int = 0 ,initiationMode : String = "NA" , isBharatQr : Bool = false, transactionNote :String = "NA" , upiTransRefNo : Int = 0 , payeeType : [String:Any] = ["NA":"NA"] , payerType : [String:Any] = ["NA":"NA"]) -> [String: Any]? {
        var mobile = "7007587639"
        if let user = CoredataManager.getDetail(App_User.self) {
            let mob = user.mobile ?? "NA"
            if mob.count >= 10 { mobile = mob }
            else { return nil }
        }
        
        if reqAction == "initPayment"{
            return ["mobileNo":mobile, "pspBank":category, "amount":  amount , "expiryTime" : expiryTime , "initiationMode" : initiationMode , "isBharatQr" : isBharatQr, "transactionNote" : transactionNote , "upiTransRefNo" : upiTransRefNo , "payeeType": payeeType , "payerType": payerType]
        }
        if reqAction == "confPayment"{
            
            var creds = ["atmCrdLength": 0,"credentialDataCode": "NPCI","credentialDataKi": "20150822","credentialDataLength": 0,"credentialDataValue": "Mi4wfGEwTDlTY1pEdnJ5SEdwTktJSGNmZDh0dzJCc3FQK2tTSEVLY3NIT0RzQlBHQjRsSlFkMnRZ\\nM2xJTnVTV0hKYXNlZi9lQzBwV0M5cXBKK3JoYU9xZVlzZHBvNjBDc2RLM1M4RU44T09IdXAwaHhv\nMUhqcXR5L1lJTzBqanNsMndKdlVWTzIxNSttQUxBTytNNTdNSjRyK2VZa1VDLzlZb3NOaDk3cHlh\nS2J1VVYySDVqNG4rL1h3RUllREQ0NTVack50cEJzaXorbjJyWmtta290WW5uVmxNZHVqbm5vaWpP\nbjlKYnBTdnFmOVdFWWg0dHJCYVpQczJwTGdVNXR0TlM3WGVTbzEyYjVDRjhsQjAzeldjeGFQbU9z\ncnRCQT09\n","credentialSubType": "MPIN","credentialType": "PIN","otpCrdLength": 0] as [String : Any]
            
            return ["mobileNo":mobile, "pspBank":category, "amount":  amount , "expiryTime" : expiryTime ,  "isBharatQr" : isBharatQr,"upiTransRefNo" : upiTransRefNo  , "cred": creds ,"npciTranId": "INDBF7C3A76B1DE9489E8F16152CE1A0CF1","resHash": "xyyyxyyasas"]
        }
        
        
       
           /*
            "payeeType": {
              "name": "Gowtham",
              "virtualAddress": "gowtham@cscindus",
              "vpaType": "VA"
            }
        
        "payerType": {
          "accountId": "28063",
          "accountNo": "158097840416",
          "virtualAddress": "abhishek@cscindus"
        }*/
        
        return nil
    }
    
    
    static func getRequestDispute(reqAction: String , pspId: String , pspBank: String , upiTrasRefNo : String , requestType :String , statusIs : String , disputeRemark: String , disputeType: String , custRefNo:String , ticketNo:String) -> String?{
        var params = [String: Any]()
        var head = [String: Any]()
        var body = [String: Any]()
        
        if let header = HelperUtil.getCommonHeader(reqAction: reqAction, pspId: pspId) {
            head = header
        } else { return nil }
    

        if let bodyData = HelperUtil.getBodyDispute(reqAction: reqAction, pspBank: pspBank, upiTrasRefNo: upiTrasRefNo, requestType: requestType, statusIs: statusIs, disputeRemark: disputeRemark, disputeType: disputeType, custRefNo: custRefNo, ticketNo: ticketNo) {
            body = bodyData
        } else { return nil }
        
        
        
        params = ["head" : head, "body" : body]
        if Constants.printResponse { print(params) }
        let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
        let decode = String(data: jsonData, encoding: .utf8)
        guard let decoded = decode else { return nil }
        
        guard let reqData = encrypt(data : decoded, reqAction: reqAction, mobCrypt : false, mob: "NA") else {
            return nil
        }
        return reqData
        
    }
    
    
    
    static func getBodyDispute(reqAction: String , pspBank: String , upiTrasRefNo: String , requestType : String , statusIs: String , disputeRemark: String , disputeType: String , custRefNo:String , ticketNo:String) -> [String:Any]?{
       var mob = "7007587639"
        if let user = CoredataManager.getDetail(App_User.self){
            let mobile = user.mobile ?? "NA"
            if mobile.count >= 10{ mob = mobile} else{ return nil }
        }
        if reqAction == "getDisputeList" || reqAction == "raiseDispute" || reqAction == "getdisputestatus"{
            return ["pspBank": pspBank , "mobileNo" : mob , "upiTranRefNo": upiTrasRefNo , "ticketNo" : ticketNo , "requestType" : requestType, "statusIs" : statusIs, "disputeRemark" : disputeRemark , "disputeType" : disputeType ,  "custRefNo" : custRefNo]
        }
        return nil
    }
    
    
    
    
  
    
    /*
    
     return ["pspBank" : ,"mobileNo": , "virtualAddress": "abhishekstore@cscindus" , "pgMerchantId" : , "orderNo" : , "orgOrderNo": , "orgIndRefNo": , "orgCustRefNo" : , "transactionNote" : ,  "amount" : , "currencyCode": , "payType": , "reqType": "T" ,"txnType" : , "fromDate": , "toDate" :  , "fromIndex" : , "toIndex" : , "pgMerchantId": , "expiryTime": 120 , "isMerchant" : , "emailId": "as@as.com" , "panNo": "ADHPI5728H" , "gstin": "ABCD" , "udyogId": "ABCD1234" , "cityName": "Parel",
     "stateName": "13",
     "pinCode": "402201",
     "address1": "RCF",
     "address2": "THAL",
     "address3": "TKIET",
     "billingFax": "224504",
     "contactName": "vicky",
     "contactPhone": "0222255885508",
     "contactEmail": "kesava008@gmail.com",
     "loginEmail": "kesava008indus@gmail.com",
     "accountNo": "1456955456323568" ,
     "ifscCode": "ICIC0001214",
     "bankCode": "ICICI",
     "requestUrl1": "https://indusupiuat.indusind11111.com",
     "requestUrl2": "https://ind.indust222222.com",
     "requestUrl3": "https://ind.indust3333.com",
     "requestUrl4": "https://ind.indust44444.com",
     
     
     
     
     "merchantName": "abhishek" , "legalName": "abhishek" , "merCategoryCode": "6012" , "merchantType": "AGGMER", "integrationType": "WEBAPI" , "settleType": "NET" , "ifsc": "icic0000175" , "merchantId": "abnsg1455" ,
     "terminalId": "vgay5567575",
     "requestUrl1": "https://indusupiuat.indusind11111.com",
     "requestUrl2": "https://ind.indust2222.com" ,"custRefNo": , "npciTranId":  , "resHash": "reshash" , "payerType": {"virtualAddress": "gowtham@cscindus","isMerchant": "false","showMerchant": "false","defVPAStatus": "false"
   }]

    */
    
    
    
    
//    ["pspBank": "IndusInd",
//        "mobileNo": "9999894870",
//        "pgMerchantId": "INDB000000029154",
//        "isMerchant": "yes",
//        "merchantName": "kesavaindus008",
//        "legalName": "Thoppil Mart",
//        "virtualAddress": "kesava008@indus",
//        "merCategoryCode": "6012",
//        "panNo": "AFGHQY007Q",
//        "gstin": "ABCD",
//        "udyogId": "ABCD1234",
//        "cityName": "Parel",
//        "stateName": "13",
//        "pinCode": "402201",
//        "address1": "RCF",
//        "address2": "THAL",
//        "address3": "TKIET",
//        "billingFax": "224504",
//        "contactName": "vicky",
//        "contactPhone": "0222255885508",
//        "contactEmail": "kesava008@gmail.com",
//        "loginEmail": "kesava008indus@gmail.com",
//        "merchantType": "DIRMER",
//        "integrationType": "WEBAPI",
//        "settleType": "GROSS",
//        "accountNo": "1456955456323568",
//        "ifscCode": "ICIC0001214",
//        "bankCode": "ICICI",
//        "requestUrl1": "https://indusupiuat.indusind11111.com",
//        "requestUrl2": "https://ind.indust222222.com",
//        "requestUrl3": "https://ind.indust3333.com",
//        "requestUrl4": "https://ind.indust44444.com"]
    
    
    
    
    
    
    
    
    
    
    
    
//MARK:- PSP Generator Function -------------------------------------------------------------------------------------
    
    func getPSPData(pspId : String? = "IBL") -> String? {
        //Some static need to be change dynamic
        let location = "Delhi"
        let geoLat = "72.223"
        let geoLong = "77.223"
        
        let appVersion = Bundle.main.versionNumber
        var pspParam = [String: String]()
        var ip = "NA"
        if let ips = getIPAddress() {
            ip = ips
        }
        
        // This pspS will come based on the pspId , need to add this method in future for dynamic get bank list
        let pspS = ["androidId", "appName", "appVersionCode", "appVersionName", "wifiMac", "bluetoothMac", "capability", "deviceId", "deviceType", "geoCode", "location", "ip" , "mobileNo", "relayButton", "simId", "os", "regId", "selectedSimSlot" ,"fcmToken" ]
        
        guard let user = CoredataManager.getDetail(App_User.self) else {
            return nil
        }
        guard let mobile = user.mobile, mobile.count >= 10 else {
            return nil
        }
        var isValid = true
        
        for psp in pspS {
            if psp == "androidId" {
                if user.deviceId == getUUID() {
                    pspParam["androidId"] = getUUID() ?? "NA"
                } else { isValid = false; break }
            }
            if psp == "appName" {
                if user.appName == "CSC Pay" {
                    pspParam["appName"] = "CSC Pay"
                } else { isValid = false; break }
            }
            if psp == "appVersionCode" {
                if user.appVer == appVersion ?? "1" {
                    pspParam["appVersionCode"] = appVersion ?? "1"
                } else { isValid = false; break }
            }
            if psp == "appVersionName" {
                pspParam["appVersionName"] = "Pay UAT 1"
            }
            if psp == "wifiMac" {
                if user.wifiMac == "02:00:00:00:00:00" {
                    pspParam["wifiMac"] = "02:00:00:00:00:00"
                } else { isValid = false; break }
            }
            if psp == "bluetoothMac" {
                if user.bluetoothMac == "02:00:00:00:00:00" {
                    pspParam["bluetoothMac"] = "02:00:00:00:00:00"
                } else { isValid = false; break }
            }
            if psp == "capability" {
                pspParam["capability"] = "5200000200010004000639292929292"
            }
            if psp == "deviceId" {
                if user.deviceId == getUUID() {
                    pspParam["deviceId"] = getUUID() ?? "NA"
                } else { isValid = false; break }
            }
            if psp == "deviceType" {
                pspParam["deviceType"] = "MOB"
            }
            if psp == "geoCode" {
                pspParam["geoCode"] = "\(geoLat),\(geoLong)"
            }
            if psp == "location" {
                pspParam["location"] = location
            }
            if psp == "location" {
                pspParam["location"] = location
            }
            if psp == "ip" {
                pspParam["ip"] = ip
            }
            if psp == "mobileNo" {
                pspParam["mobileNo"] = mobile
            }
            if psp == "relayButton" {
                pspParam["relayButton"] = "NEZGNzAyNkVENzE4ODZDODg2QTlBNTBF"
            }
            if psp == "simId" {
                if user.simId == "\(mobile)\(mobile.prefix(8))0" {
                    pspParam["simId"] = "\(mobile)\(mobile.prefix(8))0"
                } else { isValid = false; break }
            }
            if psp == "selectedSimSlot" {
                pspParam["selectedSimSlot"] = "0"
            }
            if psp == "fcmToken" {
                pspParam["fcmToken"] = ""
            }
            
        }
        if !isValid { return nil }
        
        let jsonData = try! JSONSerialization.data(withJSONObject: pspParam, options: [])
        let decoded = String(data: jsonData, encoding: .utf8)!
        let enc64   = decoded.base64Encoded() ?? "NA"
        
        return enc64
    }





    static func getBankRequest(reqAction: String, pspId : String = "IBL", pspBank: String = "NA" ,category: String = "NA", refId: String = "NA", resHash: String = "NA", otpHash : String = "NA", extra: String? = nil, mobCrypt: Bool = false, isNew : Bool = false , isMerchant : Bool = false , expiryDate: String = "NA" , lastSixDebCard: String = "NA" , virtualAddress : String = "NA" , accountId : Int = 00000 , accountStatus : String = "S" , customerName : String = "NA" , bankCode : String = "NA"  , pspRespRefNo : String = "41299") -> String?{
        
        var params = [String: Any]()
        var head = [String: Any]()
        var body = [String: Any]()
        
        
        //MARK: - Common Header
        
        if let header = HelperUtil.getCommonHeader(reqAction: reqAction, mobile: category, refId : refId, pspId: pspId, isNew: isNew , isMerchant: isMerchant) {
            head = header
        } else { return nil }
        
        //MARK: - Common Body

        if let bodyData = HelperUtil.getBody(reqAction: reqAction, category: pspBank, pspId: pspId, accountId: accountId, virtualAddress: virtualAddress, pspRespRefNo: pspRespRefNo, accountStatus: accountStatus, customerName: customerName, bankCode: bankCode){
            body = bodyData
        }else{return nil}
        
        params = ["head" : head, "body" : body]
        if Constants.printResponse { print(params) }
        let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
        let decode = String(data: jsonData, encoding: .utf8)
        guard let decoded = decode else { return nil }
        
        guard let reqData = encrypt(data : decoded, reqAction: reqAction, mobCrypt : mobCrypt, mob: category, isNew : isNew) else {
            return nil
        }
        return reqData
}
    
    
    static func getRequestMerchat(reqAction: String, pspId : String = "IBL", pspBank: String = "NA" ,category: String = "NA", legalName: String = "NA", virtualAddress: String = "NA", contactName : String = "NA", contactPhone: String = "NA", contactEmail: String = "NA", loginEmail : String = "NA" , merchantType : String = "NA" , integrationType: String = "NA", settleType: String = "NA", accountNo: String = "NA", ifscCode: String = "NA", requestUrl1: String = "NA", requestUrl2: String = "NA", requestUrl3: String = "NA", requestUrl4: String = "NA", merCategoryCode: String = "NA", panNo: String = "NA", gstin: String = "NA", udyogId: String = "NA", cityName: String = "NA", stateName: String = "NA", pinCode: String = "NA", address1: String = "NA", address2: String = "NA", address3: String = "NA", billingFax: String = "NA" , mobCrypt : Bool = false , isNew : Bool = false , pgMerchantId: String = "NA" , isMerchant: Bool = false , merchantId : String = "NA" , terminalId: String = "NA", requestModel : CommonModel? , merchantRegModel: MerchantRegistrationModel?) -> String?{
        var params = [String:Any]()
        var head = [String:Any]()
        var body = [String:Any]()
        
        if let header = HelperUtil.getCommonHeader(reqAction: reqAction, pspId: pspId, isNew: false, isMerchant: true){
            head = header
        }else{return nil}
        
        if let bodyData = HelperUtil.getBodyMerchants(reqAction: reqAction ,pspBank: pspBank, pgMerchantId: pgMerchantId , isMerchant: isMerchant, legalName: legalName, virtualAddress: virtualAddress, contactName: contactName, contactPhone: contactPhone, contactEmail: contactEmail, loginEmail: loginEmail, merchantType: merchantType, integrationType: integrationType, settleType: settleType, accountNo: accountNo, ifscCode: ifscCode, requestUrl1: requestUrl1, requestUrl2: requestUrl2, requestUrl3: requestUrl3, requestUrl4: requestUrl4, merCategoryCode: merCategoryCode, panNo: panNo, gstin: gstin, udyogId: udyogId, cityName: cityName, stateName: stateName, pinCode: pinCode, address1: address1, address2: address2, address3: address3, billingFax : billingFax ,terminalId : terminalId , merchantId: merchantId , requestModel: requestModel, merchantRegModel: merchantRegModel){
            body = bodyData
        }else{return nil}
        
        params = ["head":head , "body": body]
        if Constants.printResponse { print(params) }
        let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
        let decode = String(data: jsonData, encoding: .utf8)
        guard let decoded = decode else { return nil }
        
        guard let reqData = encrypt(data : decoded, reqAction: reqAction, mobCrypt : mobCrypt, mob: category, isNew : isNew) else {
            return nil
        }
        return reqData
    }
    
    
    
    //MARK: - Merchant Body Function
    
    static func getBodyMerchants(reqAction: String = "NA" ,pspBank : String = "NA" , mobileNo : String = "NA" , pgMerchantId : String = "INDB000000029154" ,isMerchant : Bool = true, merchantName : String = "kesavaindus008" ,legalName : String = "Thoppil Mart", virtualAddress : String = "kesava008@indus", contactName : String = "vicky" , contactPhone : String = "0222255885508" ,contactEmail : String = "kesava008@gmail.com" , loginEmail : String = "kesava008indus@gmail.com" , merchantType : String = "DIRMER" , integrationType : String = "WEBAPI" , settleType : String = "GROSS" , accountNo : String = "1456955456323568", ifscCode : String = "ICIC0001214" ,  requestUrl1 : String = "https://indusupiuat.indusind11111.com", requestUrl2 : String = "https://ind.indust222222.com" ,requestUrl3 : String = "https://ind.indust3333.com" , requestUrl4 : String = "https://ind.indust44444.com", merCategoryCode : String = "6012" , panNo : String = "AFGHQY007Q", gstin : String = "ABCD" , udyogId : String = "ABCD1234", cityName : String = "Parel" , stateName : String = "13" , pinCode : String = "402201" , address1 : String = "RCF" , address2 : String = "THAL" ,address3  : String = "TKIET", billingFax : String = "224504" , mobCrypt : Bool = false , isNew  : Bool = false , terminalId : String = "NA" , merchantId : String = "NA" , requestModel : CommonModel? , merchantRegModel: MerchantRegistrationModel?) -> [String:Any]?{
        
        var mobile = "9876543210"
        var bankcode = "ICICI"
        guard let user = CoredataManager.getDetail(App_User.self)else { return nil }
        
        let mob = user.mobile ?? "NA"
        if mob.count >= 10 { mobile = mob }
        
        if reqAction == "directMerchant"{
            return ["pspBank":pspBank,"mobileNo":mobile,"pgMerchantId":pgMerchantId,"isMerchant":isMerchant,"merchantName":merchantName,"legalName":legalName,"virtualAddress":virtualAddress,"merCategoryCode":merCategoryCode,"panNo":panNo,"gstin":gstin,"udyogId":udyogId,"cityName":cityName,"stateName":stateName,"pinCode":pinCode,"address1":address1,"address2":address2,"address3":address3,"billingFax":billingFax,"contactName":contactName,"contactPhone":contactPhone,"contactEmail":contactEmail,"merchantType":merchantType,"integrationType":integrationType,"settleType":settleType,"accountNo":accountNo,"ifscCode":ifscCode,"bankCode":bankcode,"requestUrl1":requestUrl1,"requestUrl2":requestUrl2]
        }else if reqAction == "subMerchant"{
            return ["pspBank": "IndusInd",
                    "mobileNo": mobile,
                    "virtualAddress": "abhishekstore@cscindus",
                    "isMerchant": "yes",
                    "emailId": "as@as.com",
                    "panNo": "ADHPI5728H",
                    "gstin": "ABCD",
                    "udyogId": "ABCD1234",
                    "pgMerchantId": "UPI0000000000204",
                    "merchantName": "abhishek",
                    "legalName": "abhishek",
                    "merCategoryCode": "6012",
                    "merchantType": "AGGMER",
                    "integrationType": "WEBAPI",
                    "settleType": "NET",
                    "ifscCode": "icic0000175",
                    "accountNo": "324234234",
                    "merchantId": "abnsg1455",
                    "terminalId": "vgay5567575",
                    "requestUrl1": "https://indusupiuat.indusind11111.com",
                    "requestUrl2": "https://ind.indust2222.com",
                    "bankCode" : "ICIC"]
        }else if reqAction == "checkVPA"{
            return ["pspBank": "IndusInd",
                    "mobileNo": mobile,
                    "virtualAddress": "abhishekstore@cscindus",
                    "pgMerchantId": "UPI0000000000204",
                    "reqType": "T"]
        }else if reqAction == "initCollect"{
            return ["pspBank": "IndusInd",
            "mobileNo": mobile,
            "amount": "1.00",
            "expiryTime": 120,
            "transactionNote": "asa",
            "upiTransRefNo": 0,
            "pgMerchantId": "UPI0000000000204",
            "resHash": "reshash",
            "payerType": [
                "virtualAddress": "gowtham@cscindus",
                "isMerchant": "false",
                "showMerchant": "false",
                "defVPAStatus": "false"
            ]
        ]
        }else if reqAction == "txnStatus"{
            return ["pspBank": "IndusInd",
                    "mobileNo": mobile,
                    "pgMerchantId": "UPI0000000000204",
                    "custRefNo": "012119047380",
                    "npciTranId": "INDBAA4F5A16C75A4C5F8988CBEA8022FDB"]
        }else if reqAction == "txnHistory"{
           return ["pspBank": "IndusInd",
            "mobileNo": mobile,
            "pgMerchantId": "UPI0000000000204",
            "fromDate": "01-01-2005 11:01:01",
            "toDate": "11-02-2019 11:01:01",
            "fromIndex": "1",
            "toIndex": "3"]
        }else if reqAction == "txnRefund"{
            return ["pspBank": "IndusInd",
                    "mobileNo": mobile,
                    "pgMerchantId": "UPI0000000000204",
                    "orderNo": "33047064569176",
                    "orgOrderNo": "1501053045868596",
                    "orgIndRefNo": "68956",
                    "orgCustRefNo": "722319161232",
                    "transactionNote": "test",
                    "amount": "1",
                    "currencyCode": "INR",
                    "payType": "P2P",
                    "txnType": "PAY"]
        }else if reqAction == "checkMerchant"{
            return ["mobileNo": requestModel?.mobileNo ?? "NA" ,"legalName":requestModel?.legalName ?? "NA","panNo":requestModel?.panNo ?? "NA"]
        }else if reqAction == "regMerchant" {
            return ["mobileNo": requestModel?.mobileNo ?? "NA",
                    "legalName": requestModel?.legalName ?? "NA",
                    "bizName": requestModel?.bizName ?? "NA",
                    "bizGroup": "MER",
                    "bizDoReg": requestModel?.bizDoReg ?? "NA",
                    "bizCategory": requestModel?.bizCategory ?? "NA",
                    "mcCode": requestModel?.mcCode ?? "6012",
                    "address": requestModel?.address ?? "NA",
                    "city": requestModel?.city ?? "Delhi",
                    "stateCode": requestModel?.stateCode ?? "07",
                    "stateName": requestModel?.stateName ?? "Delhi",
                    "districtCode": requestModel?.districtCode ?? "83",
                    "districtName": requestModel?.districtName ?? "South Delhi",
                    "authPerson": requestModel?.authPerson ?? "Abhishek Ranjan",
                    "authContact": requestModel?.authContact ?? "9999894870",
                    "authKycType": requestModel?.authKycType ?? "PAN",
                    "authKycId": requestModel?.authKycId ?? "AHCPR2625P",
                    "email": requestModel?.email ?? "abhishek@digimail.in",
                    "panNo": requestModel?.panNo ?? "AHCPR2625P",
                    "ifscCode": requestModel?.ifscCode ?? "ICIC0000171",
                    "accountNo": requestModel?.accountNo ?? "017101015050",
                    "gstin": requestModel?.gstin ?? "NA",
                    "registerId": requestModel?.registerId ?? "na",
                    "docCount": requestModel?.docCount ?? "2",
                    "docList": requestModel?.docList ?? "9999894870",
                    "cscId": requestModel?.cscId ?? "943111134902",
                    "fcmDeviceId" : requestModel?.fcmDeviceId ?? "abcd",
                    "pinCode" : requestModel?.pinCode ?? "NA"]
        }else if reqAction == "docUpload"{
            return [
                "mobileNo": merchantRegModel?.body.mobileNo ?? "NA",
                "seqKey": merchantRegModel?.body.seqKey ?? "NA",
                "bizId": merchantRegModel?.body.bizId ?? "NA",
                "docId": merchantRegModel?.body.docId ?? 1,
                "docType": merchantRegModel?.body.docType ?? "NA",
                "docName": merchantRegModel?.body.docName ?? "NA",
                "docMime": merchantRegModel?.body.docMime ?? "NA",
                "docSize": merchantRegModel?.body.docSize ?? 1234,
                "docData": merchantRegModel?.body.docData ?? "NA",
                "checkSum": merchantRegModel?.body.checkSum ?? "NA"]
        }else if reqAction == "activateMerchant"{
            return [
                "mobileNo": merchantRegModel?.body.mobileNo   ?? "NA",
                    "legalName":merchantRegModel?.body.legalName  ?? "NA",
                    "panNo":merchantRegModel?.body.panNo  ?? "NA",
                    "bizId":merchantRegModel?.body.bizId  ?? "NA",
                    "seqKey" : merchantRegModel?.body.seqKey ?? ""
            ]
        }
        return nil
}








}

