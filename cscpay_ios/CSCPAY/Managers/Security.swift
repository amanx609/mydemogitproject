//
//  SHA256.swift
//  VLE Survey
//
//  Created by Abhishek Ranjan on 03/10/20.
//  Copyright © 2020 CSC eGovernance. All rights reserved.
//

import Foundation
import CryptoKit
import CommonCrypto

class AppSecure  {
    
    static func tokenEncryptWithGCM(_ inpStr: String, aesKey: String, token : String) -> String {
        let MOB_ENC_SALT = "5acfbac31a52b54862f8f8cc9f63f6qw"
        
        let ts = Date().getCurrentDate(format: "YmdHis") //20210118
        let token = token
        let binTok      = token.base64Decode()
        let lenTok      = binTok?.count ?? 0
        let lenTokLen   = "\(lenTok)".count
        let tokLenPad   = "\(lenTokLen)" + "\(lenTok)"
        
        let redLen      = "\(tokLenPad)".count
        let rndHash     = ts.0.sha256()
        let length      = 64 - redLen
        let subHash     = "\(rndHash)".suffix(length)
        let padLenHash  = "\(tokLenPad)" + subHash
        let lenPad      = padLenHash.hexaToBytes
        
        let keyStr      = aesKey + MOB_ENC_SALT
        let keyData     = keyStr.sha256() as? String ?? ""
        let aesKeys     = keyData.hexaToBytes
        //let iv          = cryptOperatons.randomGenerateBytes(count: 12)!
        let cipherData  = cryptOperatons.encryptUsingGCM(plainText: inpStr, key: Data(aesKeys))
        let encrypted   = cipherData // (cipherData.0 ?? Data()) + (cipherData.1 ?? Data()) + iv
        let retDat      = encrypted + (binTok ?? Data()) + lenPad
        let result      = retDat.base64EncodedString()
        //let decrypt     = tokenDecryptWithGCM(result, aesKey: aesKey, iv: iv, tag: (cipherData.1 ?? Data()), ref: retDat)
        return result
    }
    
    static func tokenDecryptWithGCM(_ cipherText : String, aesKey: String, iv : Data = Data(), tag: Data = Data(), ref: Data = Data()) -> String {
        let MOB_ENC_SALT = "5acfbac31a52b54862f8f8cc9f63f6qw"
        
        let enc         = cipherText.base64Decode()
        let padStr      = enc?.dropLast(32)
        let lenPadStr   = padStr?.count ?? 0
        let hexTok      = (enc?.suffix(32) ?? Data()).hexEncodedString()
        let lenTokLen   = hexTok.prefix(1)
        let tokLenPreview = hexTok.dropFirst(1)
        let tokLen      = tokLenPreview.prefix(Int(lenTokLen) ?? 0)
        let encStr      = padStr?.dropLast(Int(tokLen) ?? 0)
        let token       = padStr?.suffix(Int(tokLen) ?? 0)
        
        let keyStr      = aesKey + MOB_ENC_SALT
        let keyData     = keyStr.sha256() as? String ?? ""
        let aesKeys     = keyData.hexaToBytes
        
        //let aesKeys     = aesKey.hexaToBytes
        let ivs         = encStr?.suffix(12)
        let tagPreview  = encStr?.dropLast(12)
        let tags        = tagPreview?.suffix(16)
        let encS        = tagPreview?.dropLast(16)
        let decrypted   = cryptOperatons.decryptUsingGCM(encStr: encStr!, key: Data(aesKeys))
        
        return decrypted
    }
    
    static func encrypt(_ inpStr : String, keyData: String , mobileEnc: Bool = true) -> String {
        //let keyData = "7418612973" // "c57f4d4bcf08dcb7300e8bca8e5d3e22cedbaaee73bec7b027561975a825f54e"
        let MOB_ENC_SALT = "5acfbac31a52b54862f8f8cc9f63f6qw"
        let ts = Date().getCurrentDate(format: "YYYYMMdd") //20210118
        let kmd     = "\(keyData)\(MOB_ENC_SALT)\(ts.0)" //Mobile + 20210118
        let hash    = kmd.sha256()
        
        let mobHex  = keyData.decimalToHexa
        let mobLen  = mobHex.count
        
        let hexLen  = "\(mobLen)".count <= 1 ? "0\(mobLen)" : "\(mobLen)"
        
        let lenHex  = hexLen.count
        let count   = 64 - (mobLen + lenHex)
        let subHash = "\(hash)".suffix(count)
        
        let keyHash = "\(hash)".hexaToBytes
        let padHash = "\(subHash)\(mobHex)\(hexLen)".hexaToBytes
        
        
        if mobileEnc{
            let cipherData = cryptOperatons.encryptUsingGCM(plainText: inpStr, key: Data(keyHash ))
            let retDat  = cipherData + padHash
            let result  = retDat.base64EncodedString()
            return retDat.base64EncodedString()
        }else{
            let result = cryptOperatons.encryptUsingGCM(plainText: inpStr, key: Data(keyData.hexaToBytes))
            return result.base64EncodedString()
        }
        
        //let decrypt = decryptMobileGCM(aa)
    }
    static func decrypt(_ cipherText : String, tag: Data = Data(), ref: Data = Data()) -> String {
        let MOB_ENC_SALT = "5acfbac31a52b54862f8f8cc9f63f6qw"
        let ts = Date().getCurrentDate(format: "YYYYMMdd") //20210118
        let enc         = cipherText.base64Decode()
        let encStr      = enc?.dropLast(32)
        let hexMob      = (enc?.suffix(32) ?? Data()).hexEncodedString()
        let mobLen      = Int(String(hexMob.suffix(2)), radix: 16)! // String(hexMob.suffix(32))
        let mobHex      = (hexMob.dropLast(2)).suffix(mobLen)
        let mobile      = Int64(mobHex, radix: 16)!
        
        let kmd         = "\(mobile)\(MOB_ENC_SALT)\(ts.0)" //Mobile + 20210118
        let hash        = kmd.sha256()
        let keyHash     = "\(hash)".hexaToBytes
        
        let decrypt     = cryptOperatons.decryptUsingGCM(encStr: encStr ?? Data(), key: Data(keyHash))
        return decrypt
    }
    static func AES_Encrypt(plainText: String, key: String, outForm: String = "B64" , hexaToBytes: Bool = false) -> Any {
        
        
        let keyData : Data!
        let data : Data!
        
        
        if hexaToBytes{
            let hexString = (key.base64Decoded()!).toHexEncodedString()
            keyData = Data(hexString.hexaToBytes)
            data = Data(plainText.hexaToBytes)
        }else{
            keyData = key.data(using: String.Encoding.utf8) ?? Data() // Data(base64Encoded: key, options: .ignoreUnknownCharacters) ?? Data()
            data = plainText.data(using: String.Encoding.utf8) ?? Data()
        }
//        let keyData =  Data(plainText.hexaToBytes)
//        let data = Data(key.hexaToBytes)
        
        
        /*
//        let keyData = key.data(using: String.Encoding.utf8) ?? Data() // Data(base64Encoded: key, options: .ignoreUnknownCharacters) ?? Data()
//        let data = plainText.data(using: String.Encoding.utf8) ?? Data()
        */
        
        let iv =  cryptOperatons.randomGenerateBytes(count: kCCBlockSizeAES128)! //ivSize = kCCBlockSizeAES128
        // No option is needed for CBC, it is on by default.
        let cipherText = cryptOperatons.crypt(operation: kCCEncrypt,
                                    algorithm: kCCAlgorithmAES,
                                    options: kCCOptionPKCS7Padding,
                                              key: keyData,
                                    initializationVector: iv,
                                              dataIn: data)
        
        let enc_data = iv + cipherText!
        if outForm == "BYT" {
            return enc_data
        } else if outForm == "HEX"{
            return enc_data.hexEncodedString()
        } else if outForm == "B64" {
            return enc_data.base64EncodedString()
        }
        return enc_data
    }
    static func AES_Decrypt(encData: Any, key: String, outForm: String = "B64") -> Any {
        var ciperData : Data?
        if outForm == "BYT" {
            ciperData = encData as? Data
        } else if outForm == "HEX"{
            ciperData = hex(from: encData as! String)
        } else if outForm == "B64" {
            ciperData =  Data(base64Encoded: encData as! String, options: .ignoreUnknownCharacters)! //(encData as! String).data(using: String.Encoding.utf8) ?? Data() //
        }
        let keyData = key.data(using: String.Encoding.utf8) ?? Data() // Data(base64Encoded: key, options: .ignoreUnknownCharacters) ?? Data()
        
        if ciperData!.count > kCCBlockSizeAES128 {
        let iv = ciperData!.prefix(kCCBlockSizeAES128)
        let ciphertext = ciperData!.suffix(from: kCCBlockSizeAES128)
            let dec =  cryptOperatons.crypt(operation: kCCDecrypt, algorithm: kCCAlgorithmAES,
                         options: kCCOptionPKCS7Padding, key: keyData, initializationVector: iv,
            dataIn: ciphertext)
            
        if outForm == "BYT" {
            return dec as Any
        } else if outForm == "HEX"{
            return dec!.hexEncodedString()
        } else if outForm == "B64" {
            return dec!.base64EncodedString()
        }
            return dec as Any
    }
        return "nil"
    }
}

//MARK: - References

/*
 
 static func encryptMobileGCM1(_ inpStr : String, keyData: String) -> String {
     //let keyData = "7418612973" // "c57f4d4bcf08dcb7300e8bca8e5d3e22cedbaaee73bec7b027561975a825f54e"
     let MOB_ENC_SALT = "lo5WtxUJ@yS4XMJEeXvKRA80vunFZ81X"
     let ts = Date().getCurrentDate(format: "Ymd") //20210118
     let kmd     = "\(keyData)\(MOB_ENC_SALT)\(ts.0)" //Mobile + 20210118
     let hash    = kmd.sha256()
     let mobHex  = keyData.decimalToHexa
     let mobLen  = mobHex.length
     let hexLen  = "\(mobLen)".decimalToHexa
     let lenHex  = hexLen.count
     let count   = 64 - (mobLen + lenHex)
     let subHash = "\(hash)".suffix(count)
     let keyHash = "\(hash)".hexaToBytes
     let padHash = "\(subHash)\(mobHex)\(hexLen)".hexaToBytes
     let iv      = cryptOperatons.randomGenerateBytes(count: 12)!
     let cipherData = cryptOperatons.encryptUsingGCM(plainText: inpStr, key: Data(keyHash ))
     
     let retDat  = Data() // (cipherData.0 ?? Data()) + (cipherData.1 ?? Data()) + iv + padHash
     let result  = retDat.base64EncodedString()
     
     //let decrypt = decryptMobileGCM(result, iv : iv, tag: cipherData.1!, ref: retDat ?? Data())
     return result
 }
 
 static func getAESkey(_ saltHash : String, _ userId : String, outForm : String = "B64") -> String {
     var i = [String : String]()
     i["dt"]  = "\(Date().getCurrentDate().0)"
     i["conString"]  = userId + saltHash + i["dt"]!
     i["keyHash"] = "\(i["conString"]!.sha256(outForm))"
     i["keyLength"] = String(["keyHash"].count)
     return i["keyHash"]!
 }
 static func getSalt(_ inp: String, outForm: String = "HEX") -> String {
     var i = [String : String]()
     i["dt"]  = "\(Date().getCurrentDate().0)"
     i["input"] = inp + i["dt"]!
     i["inpLength"] = "\(i["input"]!.count)"
     i["lenLength"] = "\(i["inpLength"]!.count)"
     i["inpHash"] = "\(i["input"]!.sha256(outForm))"
     i["hshString"] = i["inpHash"]![Int(i["lenLength"]!)!..<(Int(i["lenLength"]!)! + 29)]
     i["hshStringlen"] = "\(i["hshString"]!.count)"
     i["hshDate"] = "\(i["dt"]!.sha256(outForm))"
     i["saltString"] = (i["hshString"]! + i["inpLength"]! + i["hshDate"]!).toBase64()
     i["saltHash"] = "\(i["saltString"]!.sha256(outForm))"
     return i["saltHash"]!
 }
 static func getKey(keyValue: String) -> String {
     let salt = AppSecure.getSalt(keyValue)
     return AppSecure.getAESkey(salt, keyValue)
 }
 
 class func dataWithHexString(_ hexString: String) -> Data? {
     let len = hexString.count / 2
       var data = Data(capacity: len)
       var i = hexString.startIndex
       for _ in 0..<len {
         let j = hexString.index(i, offsetBy: 2)
         let bytes = hexString[i..<j]
         if var num = UInt8(bytes, radix: 16) {
           data.append(&num, count: 1)
         } else {
           return nil
         }
         i = j
       }
     return data
 }
 
 static func enc(_ mobile : String, plainText: String) -> String {
             var i = [String : Any]()
             let ts = Date().getCurrentDate(format: "Ymd") //20210118
             let MOB_ENC_SALT = "lo5WtxUJ@yS4XMJEeXvKRA80vunFZ81X"
             let mbs = "\(mobile)\(MOB_ENC_SALT)\(ts.0)" //Mobile + 20210118
             i["hash"]  = mbs.sha256() //sha
             i["key"]  = (i["hash"] as? String)!.suffix(32) //last 32 characters
             i["iv"]  = (i["hash"] as? String)!.prefix(16)  // first 16 characters
             let ivs = "\(i["iv"] ?? "")\(ts.0)"
             i["padhash"]  = ivs.sha256()
             //method aes-256-cbc
             i["enc"]  = AES_Encrypt(plainText: plainText, key: "\(i["key"] ?? "")", iv: "\(i["iv"] ?? "")")

             i["encb64"]  = (i["enc"] as! Data).base64EncodedString()
             i["encpad"]   =  "\(i["encb64"] ?? "")\(i["padhash"] ?? "")\(mobile)"
             i["enc_dat"]   =  "\(i["encpad"] ?? "")".base64Encoded()
             return i["enc_dat"] as! String
         }
 static func encrypt(_ mobile: String, input: String, outForm : String = "HEX") -> String? {
     let MOB_ENC_SALT  = "lo5WtxUJ@yS4XMJEeXvKRA80vunFZ81X"
     var i             = [String : String]()
     let date          = "\(Date().getCurrentDate(format: "Ymd").0)"
     i["conString"]    = mobile + MOB_ENC_SALT + date
     i["hash"]         = "\(i["conString"]!.sha256(outForm))"
     i["mobHex"]        = mobile.toHexEncodedString() // hex(from: mobile as! String)   // i["mobHex"]
     i["key"]          = String(i["hash"]!.suffix(32))
     i["iv"]           = String(i["hash"]!.prefix(16))
     i["pHash"]        = "\((i["iv"]! + date).sha256(outForm))"
     i["paHash"]       = String(i["hash"]!.suffix(55))
     let enc           = AES_Encrypt(plainText: input, key: i["iv"]!, iv: "", outForm: "BYT") as! Data
     let conEnc        = i["paHash"]! + i["mobHex"]!
     if let hexData    = AppSecure.dataWithHexString(conEnc) {
         let ecnDat    = enc + hexData
         return ecnDat.base64EncodedString()
     }
     return nil
 }
 static func decrypt( encData: Any, key: String, outForm: String = "B64") -> String? {
     let MOB_ENC_SALT  = "lo5WtxUJ@yS4XMJEeXvKRA80vunFZ81X"
     var i             = [String : String]()
     var decodedData : Data?
     decodedData = Data(base64Encoded: encData as! String, options: .ignoreUnknownCharacters)!
     i["enc"]          = String(data: decodedData!, encoding: .utf8)!
     let date          = "\(Date().getCurrentDate(format: "Ymd").0)"
     
     i["encStr"]       = String(i["enc"]!.dropLast(32))
     i["binMob"]       = "\(i["enc"]!.suffix(32))".toHexEncodedString()
     i["mobHex"]       = String(i["binMob"]!.suffix(9))
     //i["mobile"]
     let mobile        = (i["mobHex"]!)
     i["conString"]    = mobile + MOB_ENC_SALT + date
     i["hash"]         = "\(i["conString"]!.sha256(outForm))"
     
     i["key"]          = String(i["hash"]!.suffix(32))
     i["iv"]           = String(i["hash"]!.prefix(16))
     
     let dec           = AppSecure.AES_Decrypt(encData: i["encStr"]!, key: i["key"]!, ivKey: i["iv"]!)
     return dec as? String
 }
 static func tokenEncrypt(_ aesKey : String, _ token : String, plainText: String, outForm : String = "B64") -> String {
     var i = [String : Any]()
     i["aesKey"]  = aesKey
     i["token"]  = token
     i["tokenLen"]  = token.count
     i["tokenHex"]  = String(format:"%02X", token.count)
     i["enc_dat"]  = AES_Encrypt(plainText: plainText, key: aesKey, iv: "String", outForm: outForm) as! String
     i["concatText"] = "\(i["token"]!)\(i["enc_dat"]!)\(i["tokenHex"]!)"
     i["req_dat"] = (i["concatText"]! as! String).base64Encoded()
     return i["req_dat"]! as! String
 }
 
 static func AES_Encrypt(plainText: String, key: String, iv: String, outForm: String = "B64") -> Any {
     
     let keyData = Data(base64Encoded: key, options: .ignoreUnknownCharacters) ?? Data()
     let data = plainText.data(using: String.Encoding.utf8) ?? Data()
     
     var ivs : Data?
     if iv == "" {
         ivs = cryptOperatons.randomGenerateBytes(count: kCCBlockSizeAES128)! //ivSize = kCCBlockSizeAES128
     } else {
         ivs = Data(base64Encoded: iv, options: .ignoreUnknownCharacters) ?? Data() }
     // No option is needed for CBC, it is on by default.
     let cipherText = cryptOperatons.crypt(operation: kCCEncrypt,
                                 algorithm: kCCAlgorithmAES,
                                 options: kCCOptionPKCS7Padding,
                                 key: keyData,
                                 initializationVector: ivs!,
                                 dataIn: data)
     
     let enc_data = ivs! + cipherText!
     if outForm == "BYT" {
         return enc_data
     } else if outForm == "HEX"{
         return enc_data.hexEncodedString()
     } else if outForm == "B64" {
         return enc_data.base64EncodedString()
     }
     return enc_data
 }
 
     static func AES_Decrypt(encData: Any, key: String,ivKey : String?, outForm: String = "B64") -> Any {
     var ciperData : Data?
     if outForm == "BYT" {
         ciperData = encData as? Data
     } else if outForm == "HEX"{
         ciperData = hex(from: encData as! String)
     } else if outForm == "B64" {
         ciperData = Data(base64Encoded: encData as! String, options: .ignoreUnknownCharacters)!
     }
     let keyData = Data(base64Encoded: key, options: .ignoreUnknownCharacters) ?? Data()
     
     if ciperData!.count > kCCBlockSizeAES128 {
     var iv : Data?
     if let ivs = ivKey {
         iv = ivs.data(using: .utf8)
     }
     iv = ciperData!.prefix(kCCBlockSizeAES128)
     let ciphertext = ciperData!.suffix(from: kCCBlockSizeAES128)
         let dec =  cryptOperatons.crypt(operation: kCCDecrypt, algorithm: kCCAlgorithmAES,
                      options: kCCOptionPKCS7Padding, key: keyData, initializationVector: iv!,
         dataIn: ciphertext)
         
     if outForm == "BYT" {
         return dec as Any
     } else if outForm == "HEX"{
         return dec!.hexEncodedString()
     } else if outForm == "B64" {
         return dec!.base64EncodedString()
     }
         return dec as Any
 }
     return "nil"
 }
 static func encrypt(data : String, keyValue : String, outForm: String = "B64") -> Any {
     return AES_Encrypt(plainText: data, key: getKey(keyValue: keyValue), iv: "",outForm: outForm)
 }
 static func decrypt(encdata : String, keyValue : String, outForm: String = "B64") -> Any {
     return AES_Decrypt(encData: encdata,key: getKey(keyValue: keyValue), ivKey: nil, outForm: outForm)
 }
 
 
// cryptOperatons.decryptUsingGCM(ciphertext: sealedData.ciphertext!, key: Data("c57f4d4bcf08dcb7300e8bca8e5d3e22cedbaaee73bec7b027561975a825f54e".hexaToBytes as! [UInt8]), nonce: nil, tag: sealedData.tag!, ivv: sealedData.nonce)
 
static func decryptGCM(_ enc : Data, tag: Data, ivv: AES.GCM.Nonce) -> String {
    let key         = "c57f4d4bcf08dcb7300e8bca8e5d3e22cedbaaee73bec7b027561975a825f54e"
    let keyHex      = key.hexaToBytes
    
    let decry       = cryptOperatons.decryptUsingGCM(ciphertext: Data(), key: Data(keyHex as! [UInt8]), nonce: nil, tag: tag, ivv: ivv)
    return ""
}
 */

/*
static func encryptGCM(_ mobile : String, plainText: String) -> String {
    let key = "c57f4d4bcf08dcb7300e8bca8e5d3e22cedbaaee73bec7b027561975a825f54e"
    var i = [String : Any]()
    i["key"]         = key.hexaToBytes
    i["keylen"]      = (i["key"] as? String)?.length ?? 0
    i["taglen"]      = 16
    i["ivLn"]        = 12
    i["iv"]          = cryptOperatons.randomGenerateBytes(count: 12)!
    i["ivlen"]       = (i["iv"] as? String)?.length ?? 0
    let cipherData   = Data() // cryptOperatons.encryptUsingGCM(plainText: plainText, key: Data(i["key"]! as! [UInt8]))
    let concat       = (i["iv"]! as! Data) //+ (cipherData.1 ?? Data()) + (cipherData.0 ?? Data())
    let base64       = concat.base64EncodedString()

    //let decrypt = decryptGCM1(base64 )
    return base64
}
static func decryptGCM(_ enc : String) -> String {
    let key = "c57f4d4bcf08dcb7300e8bca8e5d3e22cedbaaee73bec7b027561975a825f54e"
    let keyData     = key.hexaToBytes
    let cipherText  = enc.base64Decode()
    let ivs         = cipherText?.prefix(12)
    let tags        = cipherText?.dropFirst(12)
    let tag         = tags?.prefix(16)
    let encrpt      = tags?.dropFirst(16)
    let decrypt     = cryptOperatons.decryptUsingGCM(ciphertext: encrpt!, key: Data(keyData), nonce: ivs!, tag: tag!)
    return decrypt
} */
