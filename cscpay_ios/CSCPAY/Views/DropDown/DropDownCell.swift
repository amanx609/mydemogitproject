//
//  DropDownCell.swift
//  CSCPAY
//
//  Created by Aman Pandey on 26/08/21.
//

import UIKit

class DropDownCell: UITableViewCell {

    @IBOutlet weak var vmonthView: UIView!
    @IBOutlet weak var monthLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
