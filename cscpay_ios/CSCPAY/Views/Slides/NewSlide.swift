//
//  NewSlide.swift
//  DottedQR
//
//  Created by Gowtham on 25/11/21.
//

import UIKit

class NewSlide: UIView {
    @IBOutlet weak var onboardingImg: UIImageView!
    @IBOutlet weak var onboardingTitleLbl: UILabel!
    @IBOutlet weak var onboardingDescriptionLbl: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
