import UIKit

class Slide: UIView {

    @IBOutlet weak var viewStack: UIStackView!
    @IBOutlet weak var imageView: UIImageView!
    //@IBOutlet weak var labelTitle: UITextView!
    @IBOutlet weak var labelDesc: UITextView!
    
    
}
extension UITextView {

    func centerText() {
        let fittingSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let size = sizeThatFits(fittingSize)
        let topOffset = (bounds.size.height - size.height * zoomScale) / 2
        let positiveTopOffset = max(0, topOffset)
        contentOffset.y = -positiveTopOffset
    }

}
