//
//  SplashViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 12/01/21.
//

import UIKit

class SplashViewController2: UIViewController {
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var qrView: UIImageView!
    //@IBOutlet weak var splashView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.animate(withDuration: 0.8, delay: 0, options: .curveEaseIn, animations: {
            //self.splashView.frame = CGRect(x: self.splashView.frame.origin.x + UIScreen.main.bounds.width, y: self.splashView.frame.minY, width: self.splashView.frame.size.width, height: self.splashView.frame.size.height)

                }, completion: nil)
        
        UIView.animate(withDuration: 1.8, delay: 0.8, options: .curveEaseOut, animations: {
            self.logoView.alpha = 1.0
            self.qrView.alpha = 1.0
        }, completion: nil)
        
//        UIView.animate(withDuration: 1.2, delay: 1.2, options: .curveEaseIn, animations: {
//            self.logoView.frame = CGRect(x: UIScreen.main.bounds.width, y: self.logoView.frame.minY, width: self.logoView.frame.size.width, height: self.logoView.frame.size.height)
//
//                }, completion: nil)
        
        
        //logoView.blink(duration: 2.1, repeatCount: 1, autoRevers: false, visible: true)
        //self.view.setLeftTriangle(targetView: self.view)
         
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.6) {
            //self.present(getOTPViewController(), animated: true, completion: nil)
            self.callMaster()
        }
    }
    func callMaster() {
        APIManager.otpRequest(OTPResponse.self, mobile: "9999894870", reqAction: "config",isMaster: true, category: "na") { (res, err) in
            print(res)
            print(err)
            DispatchQueue.main.async {
                self.present(getPageViewController(), animated: true, completion: nil)
            }
        } //getOTPViewController
    }

}
