//
//  LinkBankAccountViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 05/02/21.
//

import UIKit

class LinkBankAccountViewController: UIViewController {

    @IBOutlet weak var linkNowView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.linkNowView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        let screenHeight = UIScreen.main.bounds.height
        if screenHeight < 770 {
            self.imageView.isHidden = true
            self.imageHeightConstraint.priority = UILayoutPriority(rawValue: 1000)
        }
    }
    @IBAction func becomeMerchantButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.present(getGeneralDetailsViewController(), animated: true, completion: nil)
        }
    }
    @IBAction func linkNowButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.present(getSelectYourBankViewController(), animated: true, completion: nil)
        }
    }
    @IBAction func inviteFriendButtonTapped(_ sender: Any) {
        notImplementedYetAlert(base: self)
    }
    @IBAction func settingButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.present(getSettingsViewController(), animated: true, completion: nil)
        }
    }
    @IBAction func helpButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.present(getFAQViewController(), animated: true, completion: nil)
        }
    }
}
