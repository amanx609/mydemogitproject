//
//  BankAccountCell.swift
//  CSCPAY
//
//  Created by Gowtham on 03/05/21.
//

import UIKit

protocol RadioButtonDelegate {
    func radioButtonTapped(indexPath: IndexPath)
}
class BankAccountCell: UITableViewCell {
    @IBOutlet weak var nameTagLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var accountTypeLabel: UILabel!
    @IBOutlet weak var radioButton: UIButton!
    
    var delegate: RadioButtonDelegate?
    
    @IBAction func radioBtnTapped(_ sender: UIButton) {
        let collectionView = self.superview as! UITableView
        let tappedCellIndexPath = collectionView.indexPath(for: self)!
        self.delegate?.radioButtonTapped(indexPath: tappedCellIndexPath)
    }
    func setup(bankName: String, title: String? = nil, icon: String? = nil, type: String? = nil) {
        self.nameTagLabel.setup(fontSize: 36, fontType: .regular, textColor: .titleColor, bgColor: .clear, text: title)
        self.titleLabel.setup(fontSize: 15, fontType: .regular, textColor: .titleColor, bgColor: .clear, text: bankName)
        if let img = UIImage(named: icon ?? ""){
            self.imgView.image = img } else {
                self.imgView.image = UIImage(named: "bankPlaceholder")
            }
        self.accountTypeLabel.setup(fontSize: 15, fontType: .regular, textColor: .titleColor, bgColor: .clear, text: type)
    }
}
