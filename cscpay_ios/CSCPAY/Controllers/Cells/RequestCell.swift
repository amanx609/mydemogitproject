//
//  RequestCell.swift
//  CSCPAY
//
//  Created by Gowtham on 03/05/21.
//

import UIKit

protocol ButtonActionDelegate {
    func acceptButtonTapped(indexPath: IndexPath)
    func rejectButtonTapped(indexPath: IndexPath)
}

class RequestCell: UITableViewCell {
    
    @IBOutlet weak var ImgProfile: UIImageView!
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var referenceLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var lblExpiry: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var viewSeperator: UIView!
    
    func setupValues(requestedBy: String, date: String, expiry : String, amount: String) {
        self.referenceLabel.setup(fontSize: 15.0, fontType: .regular, textColor: .customBlack, text: "\(Language.getText(with: LanguageConstants.lbl_requestedBy) ?? Constants.lbl_requestedBy) \(requestedBy)")
        self.msgLabel.setup(fontSize: 12.0, fontType: .regular, textColor: .greyColor, text: date) // 05 Sep 2020, 02:05pm
        self.lblExpiry.setup(fontSize: 10.0, fontType: .semiBold, textColor: .subtitleColor, text: "\(Language.getText(with: LanguageConstants.lbl_expiry) ?? Constants.lbl_expiry) \(expiry) \(Language.getText(with: LanguageConstants.lbl_expiry_type) ?? Constants.lbl_expiry_type)")
        self.amountLabel.setup(fontSize: 18.0, fontType: .bold, textColor: .customBlack, text: "\(Language.getText(with: LanguageConstants.lbl_currency) ?? Constants.lbl_currency) \(amount)")
        self.ImgProfile.image = UIImage.init(named: "avatar")
        self.viewSeperator.backgroundColor = .seperatorColor
    }
    
    var delegate: ButtonActionDelegate?
    
    @IBAction func acceptButtonTapped(_ sender: Any) {
        let collectionView = self.superview as! UITableView
        let tappedCellIndexPath = collectionView.indexPath(for: self)!
        self.delegate?.acceptButtonTapped(indexPath: tappedCellIndexPath)
    }
    @IBAction func rejectButtonTapped(_ sender: Any) {
        let collectionView = self.superview as! UITableView
        let tappedCellIndexPath = collectionView.indexPath(for: self)!
        self.delegate?.rejectButtonTapped(indexPath: tappedCellIndexPath)
    }
    
}
