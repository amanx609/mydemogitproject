//
//  Cells.swift
//  CSCPAY
//
//  Created by Gowtham on 03/05/21.
//

import Foundation
import UIKit

class FAQHeaderCell : UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel!
}

class FAQFooterCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}

class FAQSectionCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}

class FAQCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}

class GuideHeaderCell : UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}

class MerchantTransactionHistoryCell : UITableViewCell {
    @IBOutlet weak var paidToLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var bankLogoImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var viewSeperator: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    
    func setupValue(isPaid:Bool = true,isSuccess : Bool = true, person: String, date: String, amount: String, img: UIImage, seperatorColor: UIColor = .seperatorColor) {
        var lblPaid = Language.getText(with: LanguageConstants.lbl_paid_to) ?? Constants.lbl_paid_to
        var amountColor : UIColor = .customRed
        //var amountSymbol    = "-"
        if !isPaid {
            lblPaid = Language.getText(with: LanguageConstants.lbl_paid_to) ?? Constants.lbl_received_from
            amountColor = .customGreen
            //amountSymbol = "+"
        }
        self.paidToLabel.setup(fontSize: 15.0, textColor: .customBlack, text: "\(lblPaid) \(person)")
        self.amountLabel.setup(fontSize: 18.0, fontType: .bold, textColor: amountColor, text: "\(Language.getText(with: LanguageConstants.lbl_currency) ?? Constants.lbl_currency) \(amount)") // 05 Sep 2020, 02:05pm
        self.timeLabel.setup(fontSize: 10.0, fontType: .semiBold, textColor: .subtitleColor, text: date)
        self.viewSeperator.backgroundColor = seperatorColor
        self.bankLogoImageView.image = img
        self.lblStatus.isHidden = isSuccess
        if !isSuccess {
            self.amountLabel.textColor = .customBlack
        }
    }
}

class TransactionHistoryCell : UITableViewCell {
    @IBOutlet weak var paidToLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var bankLogoImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var viewSeperator: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    
    func setupValue(isPaid:Bool = true,isSuccess : Bool = true, person: String, date: String, amount: String, img: UIImage, seperatorColor: UIColor = .seperatorColor) {
        var lblPaid = Language.getText(with: LanguageConstants.lbl_paid_to) ?? Constants.lbl_paid_to
        var amountColor : UIColor = .customRed
        //var amountSymbol    = "-"
        if !isPaid {
            lblPaid = Language.getText(with: LanguageConstants.lbl_paid_to) ?? Constants.lbl_received_from
            amountColor = .customGreen
            //amountSymbol = "+"
        }
        self.paidToLabel.setup(fontSize: 15.0, textColor: .customBlack, text: "\(lblPaid) \(person)")
        self.amountLabel.setup(fontSize: 18.0, fontType: .bold, textColor: amountColor, text: "\(Language.getText(with: LanguageConstants.lbl_currency) ?? Constants.lbl_currency) \(amount)") // 05 Sep 2020, 02:05pm
        self.timeLabel.setup(fontSize: 10.0, fontType: .semiBold, textColor: .subtitleColor, text: date)
        self.viewSeperator.backgroundColor = seperatorColor
        self.bankLogoImageView.image = img
        self.lblStatus.isHidden = isSuccess
        if !isSuccess {
            self.amountLabel.textColor = .customBlack
        }
    }
}

class HeaderCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}

class AdCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var textLabel: UILabel!
}

class PendingRequestCell: UICollectionViewCell {
    @IBOutlet weak var lblNameTag: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lblExpiry: UILabel!
    
    var text: String? {
          didSet {
            self.nameLabel.setup(fontSize: 15.0, fontType: .semiBold, textColor: .customBlack, text: text)
          }}
    var img: UIImage? {
          didSet {
            self.imageView.image = img
          }}
}

class RecentCell: UICollectionViewCell {
    @IBOutlet weak var lblNameTag: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var text: String? {
          didSet {
            self.nameLabel.setup(fontSize: 15.0, fontType: .semiBold, textColor: .customBlack, text: text)
          }}
    var isMore: Bool? {
          didSet {
            self.nameLabel.setup(fontSize: 15.0, fontType: .semiBold, textColor: .darkGray, text: "More")
            self.imageView.image = UIImage.init(named: "moreContacts")
          }}
    var img: UIImage? {
          didSet {
            self.imageView.image = img
          }}
}

class ContactCell : UITableViewCell {
    @IBOutlet weak var contactImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var inviteLabel: UILabel!
    
    func setData(contactImage: String? , name : String , fullname: String , descLabel: String , inviteLabel : Bool = false){
        self.contactImageView.image = UIImage(named: contactImage ?? "NA")
        self.nameLabel.text = name.first?.uppercased() ?? "C"
        self.fullNameLabel.text = fullname
        self.descLabel.text = descLabel
        if inviteLabel{
            self.inviteLabel.text = "Invite"
        }else{
            self.inviteLabel.text = ""
        }
    }
}

class NotificationCell : UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var ImgProfile: UIImageView!
    
    func setupValues(title: String, msg: String, profileImg: UIImage?, date: String, seperatorColor: UIColor = .seperatorColor)  {
        self.titleLabel.setup(fontSize: 15.0, textColor: .customBlack, text: "\(title)")
        self.descLabel.setup(fontSize: 11.0, textColor: .darkGray, text: "\(msg)")
        self.timeLabel.setup(fontSize: 12.0, textColor: .subtitleColor, text: "\(date)")
        if let img = profileImg {
            self.ImgProfile.image = img
        }
    }
}

class PaymentMethodCell: UITableViewCell {
    @IBOutlet weak var nameTagLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var accountTypeLabel: UILabel!
    @IBOutlet weak var lblIsPrimary: UILabel!
    @IBOutlet weak var ViewSeperator: UIView!
    
    
    
    
    func setupValues(title: String, acType: String, isPrimary: Bool = false, icon: String?,seperatorColor: UIColor = .seperatorColor)  {
        
        let bankName = CoredataManager.getDetails(Master_BankList.self)?.filter({$0.bankCode == title})
        print(bankName)
        self.titleLabel.setup(fontSize: 15.0, textColor: .titleColor, text: "\(bankName?[0].bankName ?? "icici")")
        self.accountTypeLabel.setup(fontSize: 13.0, textColor: .subtitleColor, text: "\(acType)")
        let primary = isPrimary ? (Language.getText(with: LanguageConstants.btn_add_bank_account) ?? Constants.lbl_primary_account) : ""
        self.lblIsPrimary.setup(fontSize: 13.0, textColor: .subtitleColor, text: "\(primary)")
        if let img = icon {
            self.imgView.image = UIImage(named: img)
        }else{
            self.imgView.image = UIImage(named: "bankPlaceholder")
        }
        self.ViewSeperator.backgroundColor = seperatorColor
    }
}
class AddNewBankCell : UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    func setupValues(icon: UIImage? = nil) {
        lblTitle.setup(fontSize: 15.0, fontType: .semiBold, textColor: .customBlue, text: Language.getText(with: LanguageConstants.btn_add_bank_account))
        if let img = icon {
            imgView.image = img
        }
    }
}
class BankCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bankNameLabel: UILabel!
    
    func setup(bankName: String, title: String? = nil, icon: String? = nil) {
        self.titleLabel.setup(fontSize: 36, fontType: .regular, textColor: .titleColor, bgColor: .clear, text: title)
        self.bankNameLabel.setup(fontSize: 12, fontType: .regular, textColor: .titleColor, bgColor: .clear, text: bankName)
        if let img = UIImage(named: icon ?? ""){
            self.imageView.image = img } else {
                self.imageView.image = UIImage(named: "bankPlaceholder")
            }
    }
}

class BankDetailCell: UITableViewCell {
    @IBOutlet weak var nameTagLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    func setup(bankName: String, title: String? = nil, icon: String? = nil) {
        self.nameTagLabel.setup(fontSize: 36, fontType: .regular, textColor: .titleColor, bgColor: .clear, text: title)
        self.titleLabel.setup(fontSize: 15, fontType: .regular, textColor: .titleColor, bgColor: .clear, text: bankName)
        if let img = UIImage(named: icon ?? ""){
            self.imgView.image = img } else {
                self.imgView.image = UIImage(named: "bankPlaceholder")
            }
    }
}

class CollectionHeaderView: UICollectionReusableView {
  @IBOutlet weak var titleLabel: UILabel!
}

class MerchantHistoryCell : UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameTagLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var referenceLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var fromAccount: UILabel!
    @IBOutlet weak var toAccount: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var bgView: UIView!
}

class settlementCell: UITableViewCell {
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var referenceLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var toAccount: UILabel!
    @IBOutlet weak var bgView: UIView!
    
}

 
class RequestHistoryCell : UITableViewCell {
    @IBOutlet weak var lblRequestedFrom: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblExpiry: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    
}

class RequestsCell : UITableViewCell {
    @IBOutlet weak var lblRequestedFrom: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblExpiry: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    
}



protocol DeleteButtonDelegate {
    func deleteButtonTapped(indexPath : IndexPath)
}

class UpiAddressCell : UITableViewCell{
    
    @IBOutlet weak var upiIdLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var primaryLabel: UILabel!
    @IBOutlet weak var tickImg: UIImageView!
    @IBOutlet weak var tickButton: UIButton!
    @IBOutlet weak var setPrimaryView: UIView!
    @IBOutlet weak var ViewSeperator : UIView!
    
    var deleteDelegate : DeleteButtonDelegate?
    
    func setupValues(title: String, acType: String, isPrimary: Bool = false, icon: UIImage?,seperatorColor: UIColor = .seperatorColor)  {
        self.upiIdLabel.setup(fontSize: 15.0, textColor: .titleColor, text: "\(title)")
        self.primaryLabel.setup(fontSize: 13.0, textColor: .subtitleColor, text: "Primary UPI ID")
        let primary = isPrimary ? (Language.getText(with: LanguageConstants.lbl_primary_upi) ?? Constants.lbl_primary_upiID) : ""
        self.primaryLabel.setup(fontSize: 13.0, textColor: .subtitleColor, text: "\(primary)")
//        if let img = icon {
//            self.imgView.image = img
//        }
//        if isPrimary{
//            tickImg.image = UIImage(named: "tick_blue")
//        }else{tickImg.image = UIImage(named: "Tick Square")}
        
//        isPrimary == true ? (tickImg.image = UIImage(named: "tick_blue")) : (tickImg.image = UIImage(systemName: "square"))
//        isPrimary == true ? (self.primaryLabel.setup(fontSize: 13.0, textColor: .subtitleColor, text: "Primary UPI ID")) : (self.primaryLabel.isHidden = true)
//
        if isPrimary{
            tickImg.image = UIImage(named: "tick_blue")
        }else{
            tickImg.image =  UIImage(systemName: "square")
            self.primaryLabel.isHidden = true
        }
        self.ViewSeperator.backgroundColor = seperatorColor
    }
    
    
    @IBAction func deleteButtonTap(_ sender : UIButton){
        let tableView = self.superview as! UITableView
        let tappedCellIndexPath = tableView.indexPath(for: self)!
        self.deleteDelegate?.deleteButtonTapped(indexPath: tappedCellIndexPath)
    }
    
}





class AddNewUpiIdCell : UITableViewCell{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    func setupValues(icon: UIImage? = nil) {
        lblTitle.setup(fontSize: 15.0, fontType: .semiBold, textColor: .customBlue, text: Language.getText(with: LanguageConstants.lbl_add_new_upi))
        if let img = icon {
            imgView.image = img
        }
    }
}




class ReceiveChatCell : UITableViewCell{
    @IBOutlet weak var messagetext: UILabel!
    @IBOutlet weak var botImageView: UIImageView!
    @IBOutlet weak var bgImage: UIImageView!
    
    
    func uiDesign(){
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.frame = CGRect(x: 0.0, y: 0.0, width: 120.0, height: 120.0)
        shapeLayer.lineWidth = 2.0
        shapeLayer.lineCap = .round
        shapeLayer.fillColor = UIColor.red.cgColor
        
        let arcCenter = shapeLayer.position
        let radius = shapeLayer.bounds.size.width / 2.0
        let startAngle = CGFloat(0.0)
        let endAngle = CGFloat(2.0 * .pi)
        let clockwise = true
        
        let circlePath = UIBezierPath(arcCenter: arcCenter,
                                      radius: radius,
                                      startAngle: startAngle,
                                      endAngle: endAngle,
                                      clockwise: clockwise)
        shapeLayer.path = circlePath.cgPath
        self.layer.addSublayer(shapeLayer)
    }
    
    func line(){
        let lineShapeLayer2 = CAShapeLayer()
        lineShapeLayer2.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width - 20, height: self.frame.size.height)
        lineShapeLayer2.lineWidth = 2.0
        lineShapeLayer2.lineCap = .round
        lineShapeLayer2.fillColor = UIColor.red.cgColor
        
        
        
        let arcCentre = lineShapeLayer2.position
        let radius = lineShapeLayer2.bounds.size.width / 2.0
        let startAngle = CGFloat(0.0)
        let endAngle = CGFloat(2.0 * .pi)
        let clockwise = true
        
        let circlePath = UIBezierPath(arcCenter: arcCentre, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: clockwise)
        
        lineShapeLayer2.path = UIBezierPath(rect: circlePath.bounds).cgPath
        self.layer.addSublayer(lineShapeLayer2)
    }
    
    func tri(){
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: self.frame.size.width - 10))
        path.addLine(to: CGPoint(x: 100, y: 0))
        path.addLine(to: CGPoint(x: 200, y: 200))
        path.addLine(to: CGPoint(x: 0, y: 200))
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.fillColor = UIColor.green.cgColor
        shapeLayer.lineWidth = 3
        self.layer.addSublayer(shapeLayer)
    }
   
    func showOutgoingMessage(text: String) {
        let label =  UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .white
        label.text = text

        let constraintRect = CGSize(width: 0.95 * self.frame.width,
                                    height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [.font: label.font],
                                            context: nil)
        label.frame.size = CGSize(width: ceil(boundingBox.width),
                                  height: ceil(boundingBox.height))

        let bubbleSize = CGSize(width: label.frame.width,
                                     height: label.frame.height)

        let width = bubbleSize.width
        let height = bubbleSize.height

        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: width - 22, y: height))
        bezierPath.addLine(to: CGPoint(x: 17, y: height))
        bezierPath.addCurve(to: CGPoint(x: 0, y: height - 17), controlPoint1: CGPoint(x: 7.61, y: height), controlPoint2: CGPoint(x: 0, y: height - 7.61))
        bezierPath.addLine(to: CGPoint(x: 0, y: 17))
        bezierPath.addCurve(to: CGPoint(x: 17, y: 0), controlPoint1: CGPoint(x: 0, y: 7.61), controlPoint2: CGPoint(x: 7.61, y: 0))
        bezierPath.addLine(to: CGPoint(x: width - 21, y: 0))
        bezierPath.addCurve(to: CGPoint(x: width - 4, y: 17), controlPoint1: CGPoint(x: width - 11.61, y: 0), controlPoint2: CGPoint(x: width - 4, y: 7.61))
        bezierPath.addLine(to: CGPoint(x: width - 4, y: height - 11))
        bezierPath.addCurve(to: CGPoint(x: width, y: height), controlPoint1: CGPoint(x: width - 4, y: height - 1), controlPoint2: CGPoint(x: width, y: height))
        bezierPath.addLine(to: CGPoint(x: width + 0.05, y: height - 0.01))
        bezierPath.addCurve(to: CGPoint(x: width - 11.04, y: height - 4.04), controlPoint1: CGPoint(x: width - 4.07, y: height + 0.43), controlPoint2: CGPoint(x: width - 8.16, y: height - 1.06))
        bezierPath.addCurve(to: CGPoint(x: width - 22, y: height), controlPoint1: CGPoint(x: width - 16, y: height), controlPoint2: CGPoint(x: width - 19, y: height))
        bezierPath.close()

        let outgoingMessageLayer = CAShapeLayer()
        outgoingMessageLayer.path = bezierPath.cgPath
        outgoingMessageLayer.frame = CGRect(x: 10,
                                            y: 0,
                                            width: width,
                                            height: height)
        outgoingMessageLayer.fillColor = UIColor(red: 0.09, green: 0.54, blue: 1, alpha: 1).cgColor

        self.layer.addSublayer(outgoingMessageLayer)

        label.center = self.center
        self.addSubview(label)
    }
    
    
    func showMessage(text: String){
        self.messagetext.text = text
    }
    
}

class SendChatCell : UITableViewCell{
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var sendMessageText : UILabel!
    
    
    func sendMsg(text:String){
        self.sendMessageText.text = text
    }
}



protocol DetailButtonDelegate{
    func detailButtonTapped(indexPath : IndexPath)
}




class ClosedTicketsCell : UITableViewCell{
    
    @IBOutlet weak var fraudLblTxt: UILabel!
    @IBOutlet weak var satusLblTxt: UILabel!
    @IBOutlet weak var dateLblTxt: UILabel!
    @IBOutlet weak var transLblTxt: UILabel!
    @IBOutlet weak var cellBackImg: UIImageView!
    @IBOutlet weak var arrowButton: UIButton!
    
    var delegate : DetailButtonDelegate?
    @IBAction func arrowButtonTapped(_ sender: UIButton){
        let tableView = self.superview as! UITableView
        let tappedCellIndexPath = tableView.indexPath(for: self)!
        self.delegate?.detailButtonTapped(indexPath: tappedCellIndexPath)

    }
}

class OpenTicketCell : UITableViewCell{
    @IBOutlet weak var fraudLblTxt: UILabel!
    @IBOutlet weak var satusLblTxt: UILabel!
    @IBOutlet weak var dateLblTxt: UILabel!
    @IBOutlet weak var transLblTxt: UILabel!
    @IBOutlet weak var cellBackImg: UIImageView!
    @IBOutlet weak var arrowButton: UIButton!
    var delegate : DetailButtonDelegate?
    
    
    @IBAction func arrowButtonTapped(_ sender: UIButton){
        let tableView = self.superview as! UITableView
        let tappedCellIndexPath = tableView.indexPath(for: self)!
        self.delegate?.detailButtonTapped(indexPath: tappedCellIndexPath)
    }
    
}


class MerchantChooseCell : UITableViewCell{
    
    @IBOutlet weak var mainView : UIView!
    @IBOutlet weak var cellImageView : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var descriptionLabel : UILabel!
}


class CommonFieldsCell : UITableViewCell{
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var optionLabel: UILabel!
    @IBOutlet weak var seperatorView: UIView!
}


protocol PendingDocument{
    func openViewControllerOnButtonTap(index : IndexPath)
}



class PendingDocumentCell : UITableViewCell{
    @IBOutlet weak var markTickImage: UIImageView!
    @IBOutlet weak var cameraImage: UIImageView!
    @IBOutlet weak var documentNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var docNameLabel: UILabel!
    @IBOutlet weak var cameraButton : UIButton!
    
    
    var delegate : PendingDocument?

    
    @IBAction func cameraButtonTapped(_ sender : UIButton){
        let tableView = self.superview as! UITableView
        let tappedCellIndexPath = tableView.indexPath(for: self)!
        delegate?.openViewControllerOnButtonTap(index: tappedCellIndexPath)
    }
}


class MerchantRecentHistory: UICollectionViewCell{
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    
    
}
