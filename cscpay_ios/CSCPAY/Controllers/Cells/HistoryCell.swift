//
//  HistoryCell.swift
//  CSCPAY
//
//  Created by Gowtham on 03/05/21.
//

import UIKit

protocol ShareButtonDelegate {
    func shareButtonTapped(indexPath: IndexPath)
}

class historyCell: UITableViewCell {
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var referenceLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var fromAccount: UILabel!
    @IBOutlet weak var toAccount: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var bgView: UIView!
    
    
    var delegate: ShareButtonDelegate?
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        let collectionView = self.superview as! UITableView
        let tappedCellIndexPath = collectionView.indexPath(for: self)!
        self.delegate?.shareButtonTapped(indexPath: tappedCellIndexPath)
    }
    
}
