//
//  UserDetialsViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 03/06/21.
//

import UIKit
import CoreLocation
import CommonLibrary

class UserDetialsViewController : BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblNameField: IuFloatingTextFiledPlaceHolder!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var imgMale: UIImageView!
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var imgFemale: UIImageView!
    @IBOutlet weak var lblOtherGender: UILabel!
    @IBOutlet weak var btnOtherGender: UIButton!
    @IBOutlet weak var imgOtherGender: UIImageView!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var txtDOB: IuFloatingTextFiledPlaceHolder!
    @IBOutlet weak var txtEmail: IuFloatingTextFiledPlaceHolder!
    @IBOutlet weak var lblEmailSent: UILabel!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var txtOTP: IuFloatingTextFiledPlaceHolder!
    @IBOutlet weak var lblResponse: UILabel!
    @IBOutlet weak var btnProceed: UIButton!
    @IBOutlet weak var otpBorder: UIView!
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var enterOTPView: UIView!
    @IBOutlet weak var btnDoItLater: UIButton!
    
    var isValidDate = false
    var isValidMonth = false
    var isValidYear = false
    var isEdit = false
    var userDetails : App_User?
    var otpResponse : OTPResponse?
    var mobile : String?

    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular,text: Language.getText(with: LanguageConstants.lbl_user_details_title))
        lblSubtitle.setup(textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_user_detials_subtitle))
        lblNameField.setup(placeholderText: Language.getText(with: LanguageConstants.txt_name_placeholder))
        lblGender.setup(textColor: .darkGray, text: Language.getText(with: LanguageConstants.lbl_user_details_gender))
        lblMale.setup(textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_male))
        btnMale.setup(textColor: .clear, bgColor: .clear)
        lblFemale.setup(textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_female))
        btnFemale.setup(textColor: .clear, bgColor: .clear)
        lblOtherGender.setup(textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_other_gender))
        btnOtherGender.setup(textColor: .clear, bgColor: .clear)
        lblDOB.setup(textColor: .darkGray, text: Language.getText(with: LanguageConstants.lbl_user_details_dob))
        txtDOB.setup(placeholderText: Language.getText(with: LanguageConstants.txt_dob_placehloder))
        lblEmailSent.setup(fontSize:14, textColor: .customBlue, text: Language.getText(with: LanguageConstants.lbl_otp_sent_to_email))
        txtEmail.setup(placeholderText: Language.getText(with: LanguageConstants.txt_emil_placehloder))
        btnVerify.setup(fontSize: 12, fontType: .bold, textColor: .customBlue, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        txtOTP.setup(placeholderText: Language.getText(with: LanguageConstants.txt_enter_otp_placeholder))
        btnProceed.setup(fontSize: 16, fontType: .semiBold, textColor: .customWhite,bgColor: .taleColor, text: Language.getText(with: LanguageConstants.btn_user_proceed))
        if isEdit {
            setupValues()
        } else {
            getUserDetails()
            getPSPBank()
            if let _ = CoredataManager.getALLDetails(Master_BankList.self, sortBytTime: true, key: "bankName", ascending: true, sortCaseSensitive: true) {
            } else {
                self.getBankInfo()
            }
        }
    }
    func setupValues() {
        if let user = CoredataManager.getDetail(App_User.self) {
            if let name = user.name {
                self.lblNameField.text = name
            }
            if let dob = user.dob {
                self.txtDOB.text = dob
            }
            if let gender = user.gender {
                if gender == "Male" {
                    btnMale.sendActions(for: .touchUpInside)
                }
                if gender == "Female" {
                    btnFemale.sendActions(for: .touchUpInside)
                }
                if gender == "Other" {
                    btnOtherGender.sendActions(for: .touchUpInside)
                }
            }
            if let email = user.mail {
                self.txtEmail.text = email
            }
        }
        self.btnDoItLater.isHidden = true
        self.btnProceed.setTitle("UPDATE DETAILS", for: .normal)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupUI()
        //getBankInfo()
       // getSecretQuestion()
       // getOTPService()
        //getCustomerProfile()
        //checkDevice()
    }
    func getPSPBank() {
        self.showActivityIndicator()
        APIManager.otpRequest(PSPBankResponse.self, mobile: "7418612973", reqAction: "pspBank",isMaster: true, isUser: false, category: "na") { (res, err) in
            self.hideActivityIndicator()
            print(res)
            print(err)
            DispatchQueue.main.async {
                
            }
        }
    }
    func getCustomerProfile() {
        APIManager.get(secQuesList.self, mobile: "7418612973", reqAction: "customerProfle", geoLong: "77.1873", geoLat: "28.4817", location: "Delhi", pspBank: "IndusInd", pspId: "IBL", isCheckDevice : true) { (res, err, msg) in
            self.hideActivityIndicator()
            print(res)
            print(err)
            DispatchQueue.main.async {
                
            }
        }
    }
    func getOTPService() {
        APIManager.get(secQuesList.self, mobile: "7418612973", reqAction: "otpService", geoLong: "77.1873", geoLat: "28.4817", location: "Delhi", pspBank: "IndusInd", pspId: "IBL", isCheckDevice : true) { (res, err, msg) in
            self.hideActivityIndicator()
            print(res)
            print(err)
            DispatchQueue.main.async {
                
            }
        }
    }
    func getSecretQuestion() {
        APIManager.get(secQuesList.self, mobile: "7418612973", reqAction: "secretQuestion", geoLong: "77.1873", geoLat: "28.4817", location: "Delhi", pspBank: "IndusInd", pspId: "IBL", isCheckDevice : true) { (res, err, msg) in
            self.hideActivityIndicator()
            print(res)
            print(err)
            DispatchQueue.main.async {
                
            }
        }
    }
    func checkDevice() {
        self.showActivityIndicator()
        APIManager.get(CheckDevice.self, mobile: "7418612973",geoLong: "77.1873", geoLat: "28.4817", location: "Delhi", pspBank: "IndusInd", pspId: "IBL", isCheckDevice : true) { (res, err, msg) in
            self.hideActivityIndicator()
            print(res)
            print(err)
            DispatchQueue.main.async {
                
            }
        }
    }
    func getBankInfo() {
        self.showActivityIndicator()
        APIManager.get(BankList.self, mobile: "7418612973", reqAction: "bankList", geoLong: "77.1873", geoLat: "28.4817", location: "Delhi", pspBank: "IndusInd", pspId: "IBL", isCheckDevice : true, saveBank : true) { (res, err, msg) in
            self.hideActivityIndicator()
            print(res)
            print(err)
        }
    }
    /*
    func checkDeviceID() {
        let relayId = "in.cscpay.upi".sha256()
        let relay   = "\(relayId)".decimalToHexa
        
        let params = ["androidId" : getUUID() ?? "na", "appName" : "in.cscpay.upi", "appVersionCode" : Bundle.main.versionNumber ?? "1.0", "appVersionName" : "Pay UAT 1", "wifiMac" : "na", "bluetoothMac" : "na", "capability" : "5200000200010004000639292929292", "deviceId" : getUUID() ?? "na", "deviceType" : "MOB", "geoCode" : "28.4817,77.1873", "location" : "city", "ip" : getIPAddress() ?? "", "mobileNo" : "7418612973", "relayButton" : "NEZGNzAyNkVENzE4ODZDODg2QTlBNTBF", "simId" : "na", "os": UIDevice.current.systemVersion, "regId" : "NA", "selectedSimSlot":"0","fcmToken": "" ]
        
        let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
        let decoded = String(data: jsonData, encoding: .utf8)!
        let enc64   = decoded.base64Encoded() ?? "NA"
        
        APIManager.get(CheckDevice.self, mobile: "7418612973",geoLong: "77.1873", geoLat: "28.4817", location: "Delhi", pspBank: "IndusInd", pspId: "IBL", pspData: enc64) { (res, err) in
            self.hideActivityIndicator()
            print(res)
            print(err)
            DispatchQueue.main.async {
                
            }
        }
    } */
    func getUserDetails() {
        self.showActivityIndicator()
        //Refid , ts ?
        
        if let userData = CoredataManager.getDetail(App_User.self){self.userDetails = userData}
        APIManager.otpRequest(UserResponse.self, mobile: userDetails?.mobile ?? "7418612973", reqAction: "check", isUser: true,category: "PSP", mobCrypt: false) { (res, err) in
            self.hideActivityIndicator()
            DispatchQueue.main.async {
                if err != nil {
//                    if let err = err as? RuntimeError {
//                        self.lblResponse.text = err.message
//                    } else {
//                        self.lblResponse.text = err?.localizedDescription ?? Language.getText(with: LanguageConstants.general_error_generic) ?? Constants.SOMETHING_WRONG
//                    }
                } else {
                    if res?.info?.count ?? 0 > 0 {
                        print("User already registered")
                    } else {
                        print("New user")
                    }
                }
            }
        }
    }
    @IBAction func backBtntTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func proceedBtnTapped(_ sender: Any) {
        checkValues()
    }
    @IBAction func doItLaterBtnTapped(_ sender: Any) {
        if !isEdit {
            if let _ = CoredataManager.updateDetail(App_Config.self, key: "appStage", value: "3") { }
        }
        self.presentHomeVC(values: nil)
    }
    @IBAction func verifyEmailBtnTapped(_ sender: Any) {
//        self.enterOTPView.isHidden = false
        callOtp(mob: txtEmail.text ?? "NA")
    }
    func callOtp(mob: String) {
        self.showActivityIndicator()
        APIManager.call(OTPResponse.self, urlString: "start/otp", reqAction: "generate", category: mob, isNew: false , channel: "E") { result, err in
            self.hideActivityIndicator()
            DispatchQueue.main.async {
                if err != nil {
                    if let err = err as? RuntimeError {
                        self.lblResponse.text = err.message
                    } else {
                        self.lblResponse.text = err?.localizedDescription ?? Language.getText(with: LanguageConstants.general_error_generic) ?? Constants.SOMETHING_WRONG
                    }
                } else {
                    print(result)
                    self.otpResponse = result
                    self.enterOTPView.isHidden = false
                    self.lblEmailSent.text = "OTP Sent to your mail id."
                }
            }
        }
    }
    @IBAction func btnMaleTapped(_ sender: Any) {
        if btnMale.tag == 0 {
            btnMale.tag = 1
            btnFemale.tag = 0
            btnOtherGender.tag = 0
            changeImage(male: "record.circle", female:"circle", others:"circle")
            changeButtonState(male: true)
        } else {
            btnMale.tag = 0
            changeImage(male: "circle", female:"circle", others:"circle")
            changeButtonState()
        }
    }
    @IBAction func btnFemaleTapped(_ sender: Any) {
        if btnFemale.tag == 0 {
            btnMale.tag = 0
            btnFemale.tag = 1
            btnOtherGender.tag = 0
            changeImage(male: "circle", female:"record.circle", others:"circle")
            changeButtonState(female: true)
        } else {
            btnFemale.tag = 0
            changeImage(male: "circle", female:"circle", others:"circle")
            changeButtonState()
        }
    }
    @IBAction func btnOthersTapped(_ sender: Any) {
        if btnOtherGender.tag == 0 {
            btnMale.tag = 0
            btnFemale.tag = 0
            btnOtherGender.tag = 1
            changeImage(male: "circle", female:"circle", others:"record.circle")
            changeButtonState(others: true)
        } else {
            btnOtherGender.tag = 0
            changeImage(male: "circle", female:"circle", others:"circle")
            changeButtonState()
        }
    }
    func changeButtonState(male: Bool = false, female: Bool = false, others: Bool = false) {
        DispatchQueue.main.async {
            if male {
                self.lblMale.setup(textColor: .customBlue)
                self.imgMale.tintColor = .customBlue
            } else {
                self.lblMale.setup(textColor: .subtitleColor)
                self.imgMale.tintColor = .subtitleColor
            }
            if female {
                self.lblFemale.setup(textColor: .customBlue)
                self.imgFemale.tintColor = .customBlue
            } else {
                self.lblFemale.setup(textColor: .subtitleColor)
                self.imgFemale.tintColor = .subtitleColor
            }
            if others {
                self.lblOtherGender.setup(textColor: .customBlue)
                self.imgOtherGender.tintColor = .customBlue
            } else {
                self.lblOtherGender.setup(textColor: .subtitleColor)
                self.imgOtherGender.tintColor = .subtitleColor
            }
        }
        
    }
    func changeImage(male: String, female:String, others:String) {
        DispatchQueue.main.async {
            self.imgMale.image = UIImage(systemName: male)
            self.imgFemale.image = UIImage(systemName: female)
            self.imgOtherGender.image = UIImage(systemName: others)
        }
    }
    func showAlert(msg: String) {
        DispatchQueue.main.async {
            self.lblResponse.text = msg
        }
    }
    func checkValues() {
        
      
        
        
        
        var values = [String: String]()
        
        showAlert(msg: "")
        guard let name = lblNameField.text, name.count > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_name) ?? Constants.alert_enter_name)
            return
        }
        if !name.isValidName() {
            showAlert(msg: Constants.alert_enter_valid_name)
            return
        }
        var gender : String? = nil
        if btnMale.tag == 1 {
            gender = "Male"
        } else if btnFemale.tag == 1 {
            gender = "Female"
        } else if btnOtherGender.tag == 1 {
            gender = "Others"
        } else {
            gender = nil
        }
        guard (gender?.count ?? 0) > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_select_gender) ?? Constants.alert_select_gender)
            return
        }
        guard let dob = txtDOB.text, dob.count == 10 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_dob) ?? Constants.alert_enter_dob)
            return
        }
        
        if isEdit{
            if dob.count == 10{
                expDateValidation(dateStr: dob)
            }
        }
            if !isValidMonth && !isValidYear {
                showAlert(msg: Constants.alert_enter_valid_dob)
                return
            }
        
        
            
        
//        if !isValidMonth && !isValidYear {
//            showAlert(msg: Constants.alert_enter_valid_dob)
//            return
//        }
        var mail = ""
        if txtEmail.text?.count != 0 {
            guard let email = txtEmail.text, email.count > 0 else {
                showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_email) ?? Constants.alert_enter_email)
                return
            }
            if !email.isValidEmail() {
                showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_email) ?? Constants.alert_enter_valid_email)
                return
            }
            if otpView.isHidden {
                showAlert(msg: Constants.alert_verify_email)
                return
            }
            if !isEdit{
                guard let otp = txtOTP.text, otp.count > 0 else {
                    showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_user_otp) ?? Constants.alert_enter_user_otp)
                    return
                }
                self.mobile = CoredataManager.getDetail(App_User.self)?.mobile
                let otpHash = "\(otp)\(self.mobile ?? "")\(self.otpResponse?.head?.refId ?? "")".sha256() as? String
                if otpHash != (self.otpResponse?.body?.otpHash ?? "NA") {
                    self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
                } else {
                    if self.otpResponse?.body?.verifyCall ?? "" == "N" {
//                        self.presentOnboardVC()
                        return
                    }
                    self.otpValidate(otpHash: otpHash ?? "NA")
                }
                
                
                
                
                
                
            }
            
            mail = email
        }
        if location.count > 2 {
            LocationMgr.getUserLocation()
        }
        
        values["name"]      = name
        values["gender"]    = gender
        values["dob"]       = dob
        if mail.count > 0 {
            values["mail"]  = mail
        }
        self.presentHomeVC(values : values)
    }
    func presentHomeVC(values: [String: String]?) {
        
        DispatchQueue.main.async {
            guard let _ = self.geo_lat,
            let _ = self.geo_long else {
                self.showLocationEnableAlert()
                return
            }
            if let value = values {
                if value.count > 0 {
                    if let _  = CoredataManager.updateDetails(App_User.self, values: value) { }
                }
            }
            if !self.isEdit {
                if let _ = CoredataManager.updateDetail(App_Config.self, key: "appStage", value: "3") { }
            }
            
            let vc = getHomeViewController() as! HomeViewController
            if !self.isEdit {
                vc.isFirst = true
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension UserDetialsViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtDOB {
            guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                return true
            }
            let updatedText = oldText.replacingCharacters(in: r, with: string)

            if string == "" {
                if updatedText.count == 2 {
                    textField.text = "\(updatedText.prefix(1))"
                    return false
                }
                if updatedText.count == 5 {
                    textField.text = "\(updatedText.prefix(4))"
                    return false
                }
            } else if updatedText.count == 1 {
                if updatedText > "1" {
                    return false
                }
            } else if updatedText.count == 2 {
                let date = Int(updatedText.suffix(2)) ?? 32
                if date <= 31 { //Prevent user to not enter month more than 12
                    textField.text = "\(updatedText)/" //This will add "/" when user enters 2nd digit of month
                    print("Date cant be this")
                }
                //self.lblHelper.text = "Expiry date format is MM/YYYY"
                return false
            } else if updatedText.count == 5 {
                let month = Int(updatedText.suffix(2)) ?? 13
                if month <= 12 { //Prevent user to not enter month more than 12
                    textField.text = "\(updatedText)/" //This will add "/" when user enters 2nd digit of month
                    print("month cant be this")
                }
                //self.lblHelper.text = "Expiry date format is MM/YYYY"
                return false
            } else if updatedText.count == 10 {
                //self.lblHelper.text = ""
                self.expDateValidation(dateStr: updatedText)
                return true
            } else if updatedText.count > 10 {
                return false
            }
            //self.lblHelper.text = "Expiry date format is MM/YYYY"
            return true
        } else {
            return true }
    }
    func expDateValidation(dateStr:String) {

        let currentYear = Calendar.current.component(.year, from: Date()) // % 100   // This will give you current year (i.e. if 2019 then it will be 19)
        let currentMonth = Calendar.current.component(.month, from: Date()) // This will give you current month (i.e if June then it will be 6)
        let currentDay = Calendar.current.component(.day, from: Date())

        let enteredYear = Int(dateStr.suffix(4)) ?? 0 // get last two digit from entered string as year
        var monthsPreview = dateStr.prefix(5)
        let enteredMonth = Int(monthsPreview.suffix(2)) ?? 0 // get first two digit from entered string as month
        print(dateStr) // This is MM/YY Entered by user

        if enteredYear < currentYear {
            if (1 ... 12).contains(enteredMonth) && (enteredYear <= currentYear) && (enteredYear >= (currentYear - 120)) {
                //self.viewModel.expiryDate.value = dateStr
                //self.lblHelper.text = ""
                print("Entered Date Is Right")
                self.isValidYear = true
            } else {
                //self.lblHelper.text = "Entered expiry date is invalid"
                print("Entered Date Is Wrong")
                self.isValidYear = false
            }
        } else if currentYear == enteredYear {
            if enteredMonth <= currentMonth {
                if (1 ... 12).contains(enteredMonth) {
                   print("Entered Date Is Right")
                    self.isValidMonth = true
                } else {
                   print("Entered Date Is Wrong")
                    self.isValidMonth = false
                }
            } else {
                self.isValidMonth = false
                print("Entered Date Is Wrong")
            }
        } else {
            self.isValidMonth = false
           print("Entered Date Is Wrong")
        }

    }
}


extension UserDetialsViewController{
    func otpValidate(otpHash: String) {
        self.showActivityIndicator()
        APIManager.call(OTPResponse.self, urlString: "start/otp", reqAction : "validate", category: self.mobile ?? "NA", refId:otpResponse?.head?.refId ?? "NA", otpHash: otpHash, mobCrypt: true, isNew : false) { (res1, err) in
            
            self.hideActivityIndicator()
            if let result = res1 {
                CoredataManager.saveCred(resp: result) }
            GlobalData.sharedInstance.otpValid = res1
            
            DispatchQueue.main.async {
                if err != nil {
                    if let err = err as? RuntimeError {
                        //self.responseLabel.text = err.message
                        self.showAlert(msg: err.message)
                    } else {
                        //self.responseLabel.text = err?.localizedDescription ?? Language.getText(with: LanguageConstants.general_error_generic) ?? Constants.SOMETHING_WRONG
                        self.showAlert(msg: err?.localizedDescription ?? Language.getText(with: LanguageConstants.general_error_generic) ?? Constants.SOMETHING_WRONG)

                    }
                } else {
                    self.present(getHomeViewController(), animated: true, completion: nil)
                }
            }
        }
    }
}




/*
 
 if let userInfo = UserDefaults.standard.value(forKey: Constants.User_Info) as? [String: String] {
     if let name = userInfo["name"] {
         self.lblNameField.text = name
     }
     if let dob = userInfo["dob"] {
         self.txtDOB.text = dob
     }
     if let gender = userInfo["gender"] {
         if gender == "Male" {
             btnMale.sendActions(for: .touchUpInside)
         }
         if gender == "Female" {
             btnFemale.sendActions(for: .touchUpInside)
         }
         if gender == "Other" {
             btnOtherGender.sendActions(for: .touchUpInside)
         }
     }
     if let email = userInfo["email"] {
         self.txtEmail.text = email
     }
 }
 
 
 if let userInfo = UserDefaults.standard.value(forKey: Constants.User_Info) as? [String: String] {
     if let mob = userInfo["mobile"] {
         let user = ["name": name, "gender" : gender, "dob": dob, "email" : mail, "mobile" : mob] as! [String: String]
         UserDefaults.standard.setValue(user, forKey: Constants.User_Info)
     } else {
         let user = ["name": name, "gender" : gender, "dob": dob, "email" : mail] as! [String: String]
         UserDefaults.standard.setValue(user, forKey: Constants.User_Info)
     }
 } else {
     let user = ["name": name, "gender" : gender, "dob": dob, "email" : mail] as! [String: String]
     UserDefaults.standard.setValue(user, forKey: Constants.User_Info)
 }
 
 */
