import UIKit

struct onBoardingStruct {
    let image: UIImage
    let title: String
    let description: String
}

class PageViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!{
        didSet{
            scrollView.delegate = self
        }
    }
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var skipBtn: UIButton!
    
    
    //skipBtn
    
    var slides:[NewSlide] = [];
    var onboardingArray = [onBoardingStruct]()
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 14.0, *) {
            pageControl.backgroundStyle = .minimal
            pageControl.allowsContinuousInteraction = false
        }
        setupOnBoardingData()
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
    }
    
    func setupOnBoardingData(){
        
        onboardingArray.append(onBoardingStruct(
                                image: #imageLiteral(resourceName: "ic_onboarding_1"),
                                title: "UPI and track your expense",
                                description: "Easily track and analyze your payment trends and spend smartly."))
        
        onboardingArray.append(onBoardingStruct(
                                image: #imageLiteral(resourceName: "ic_onboarding_2"),
                                title: "CSC Pay is literally everywhere you go!",
                                description: "From urban to rural, landscape may change but not your payment method. "))
        
        onboardingArray.append(onBoardingStruct(
                                image: #imageLiteral(resourceName: "ic_onboarding_3"),
                                title: "Your customers’ experience matters!",
                                description: "Register your business on CSC Pay and get your own All-in -One QR. "))
        
    }
    
    func createSlides() -> [NewSlide] {
        var slideArray = [NewSlide]()
        for screenData in onboardingArray {
            let slide:NewSlide = Bundle.main.loadNibNamed("NewSlide", owner: self, options: nil)?.first as! NewSlide
            slide.onboardingImg.image = screenData.image
            slide.onboardingTitleLbl.text = screenData.title
            slide.onboardingDescriptionLbl.text = screenData.description
            slideArray.append(slide)
            
        }
        return slideArray
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func nextButtonTapped(_ sender: UIButton) {
        DispatchQueue.main.async {
            if sender.tag == 0 {
                //
                self.scrollView.scrollTo(horizontalPage: self.pageControl.currentPage + 1, animated: true)
            } else {
                if Constants.forMerchant{
                    self.present(getOTPViewController(), animated: true, completion: nil)
                }else{
                    if let _ = CoredataManager.updateDetail(App_Config.self, key: "appStage", value: "2") {
                        self.present(getUserDetialsViewController(), animated: true, completion: nil)
                    } else {
                        self.present(getSplashViewController(), animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    @IBAction func skipButtonTapped(_ sender: UIButton) {
        if Constants.forMerchant{
            self.present(getOTPViewController(), animated: true, completion: nil)
        }else{
            if let _ = CoredataManager.updateDetail(App_Config.self, key: "appStage", value: "2") {
                self.present(getUserDetialsViewController(), animated: true, completion: nil)
            } else {
                self.present(getSplashViewController(), animated: true, completion: nil)
            }
        }
        
    }
    
    
    
    
    
//    func createSlides() -> [NewSlide] {
//
//
//
//        let slide1:NewSlide = Bundle.main.loadNibNamed("NewSlide", owner: self, options: nil)?.first as! NewSlide
//        slide1.mainImage.image = UIImage(named: "cscPay")
//        //slide1.labelTitle.text = "Sending Money" // "Secured Payment"
//        slide1.descriptionTextView.halfTextColorChange(fullText: """
//            UPI and track your expense
//
//            Easily track and analyze your payment trends and spend smartly.
//            """, changeText: "UPI and track your expense", color: .onBoardingBlack, textSize: 20)
////        slide1.labelDesc.text = """
////Hi, I am Mala!
////
////I'm here to show you how cool CSC Pay is and how it will make your life easy.
////"""
//        // "Send money to your friends & family with one click" //"Now send money to anyone with our secured payment system
//
//
//
//
//        let slide2:NewSlide = Bundle.main.loadNibNamed("NewSlide", owner: self, options: nil)?.first as! NewSlide
//        slide2.mainImage.image = UIImage(named: "ic_onboarding_2")
//        //slide2.labelTitle.text = "Receiving Money" // "Spend Trends"
//        slide2.descriptionTextView.halfTextColorChange(fullText: """
//            CSC Pay is literally everywhere you go!
//
//            From urban to rural, landscape may change but not your payment method.
//            """, changeText: "CSC Pay is literally everywhere you go!", color: .onBoardingBlack, textSize: 20)
//
//
//
//        /*
//        if let myView = slide2.viewStack.subviews.first {
//            slide2.viewStack.removeArrangedSubview(myView)
//            slide2.viewStack.setNeedsLayout()
//            slide2.viewStack.layoutIfNeeded()
//            slide2.viewStack.insertArrangedSubview(myView, at: 1)
//            slide2.viewStack.setNeedsLayout()
//        }
//         */
//
//
//
//
//        //"Receive Money in your account without the hassle of provinding confidential bank details." // "Receive money anywhere at anytime across India" // "Now monitor your expenses and spend wisely"
//
//        let slide3:NewSlide = Bundle.main.loadNibNamed("NewSlide", owner: self, options: nil)?.first as! NewSlide
//        slide3.mainImage.image = UIImage(named: "ic_onboarding_3")
//        //slide3.labelTitle.text = "Grow Your Business" // "Scan QR"
//        slide3.descriptionTextView.halfTextColorChange(fullText: """
//            Your customers’ experience matters!
//
//            Register your business on CSC Pay and get your own All-in -One QR.
//            """, changeText: "Your customers’ experience matters!", color: .onBoardingBlack, textSize: 20)
//
//        //"Grow your customers need not having any cash on them. They can simply CSCPAY you!"// "Handling business payments is easier than ever before with merchant profile" //"Pay at your favourite online shops and stores near you"
//
//        let slide4:NewSlide = Bundle.main.loadNibNamed("NewSlide", owner: self, options: nil)?.first as! NewSlide
//        slide4.mainImage.image = UIImage(named: "ic_onboarding_4")
//        //slide4.labelTitle.text = "A real-life bear"
//        slide4.descriptionTextView.halfTextColorChange(fullText: """
//            Are you a business owner?
//            Cscpay has a merchant profile for you fully loaded with features to make your and your customers' lives easy and  cashless.
//
//            Cscpay does it all within one application!
//            """, changeText: "Are you a business owner?", color: .lightBlue, textSize: 20)
//
////        slide4.labelDesc.text = """
////            Are you a business owner?
////            Cscpay has a merchant profile for you fully loaded with features to make your and your customers' lives easy and  cashless.
////
////            Cscpay does it all within one application!
////            """ //Did you know that Winnie the chubby little cubby was based on a real, young bear in London"
//        if let myView = slide4.stackView.subviews.first {
//            slide4.stackView.removeArrangedSubview(myView)
//            slide4.stackView.setNeedsLayout()
//            slide4.stackView.layoutIfNeeded()
//            slide4.stackView.insertArrangedSubview(myView, at: 1)
//            slide4.stackView.setNeedsLayout()
//        }
//        let slide5:NewSlide = Bundle.main.loadNibNamed("NewSlide", owner: self, options: nil)?.first as! NewSlide
//        slide5.mainImage.image = UIImage(named: "ic_onboarding_5")
//        //slide5.labelTitle.text = "A real-life bear"
//        slide5.descriptionTextView.text = "Did you know that Winnie the chubby little cubby was based on a real, young bear in London"
//
////        let slide1Gif = UIImage.gifImageWithName("1")
////        slide1.imageView.image = slide1Gif
////        let slide2Gif = UIImage.gifImageWithName("2")
////        slide2.imageView.image = slide2Gif
////        let slide3Gif = UIImage.gifImageWithName("3")
////        slide3.imageView.image = slide3Gif
////        let slide4Gif = UIImage.gifImageWithName("3")
////        slide4.imageView.image = slide4Gif
//
//        slide1.descriptionTextView.centerText()
//        slide2.descriptionTextView.centerText()
//        slide3.descriptionTextView.centerText()
//        slide4.descriptionTextView.centerText()
//        return [slide1, slide2, slide3]  // , slide4] // , slide5]
//    }
    
    
    func setupSlideScrollView(slides : [NewSlide]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
      //  * CGFloat(slides.count)
        scrollView.contentSize.height = 1
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.addSubview(slides[i])
        }
    }
    
    
    /*
     * default function called when view is scolled. In order to enable callback
     * when scrollview is scrolled, the below code needs to be called:
     * slideScrollView.delegate = self or
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
        let maximumHorizontalOffset: CGFloat = scrollView.contentSize.width - scrollView.frame.width
        let currentHorizontalOffset: CGFloat = scrollView.contentOffset.x
        
        // vertical
        let maximumVerticalOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.height
        let currentVerticalOffset: CGFloat = scrollView.contentOffset.y
        
        let percentageHorizontalOffset: CGFloat = currentHorizontalOffset / maximumHorizontalOffset
        let percentageVerticalOffset: CGFloat = currentVerticalOffset / maximumVerticalOffset
        
        if pageControl.currentPage != 2 {
            self.nextBtn.setTitle("", for: .normal)
            self.nextBtn.setImage(UIImage(named: "arrow_Right"), for: .normal)
            self.nextBtn.tag = 0
            self.skipBtn.setTitle("Skip", for: .normal)
//            self.skipBtn.isHidden = false
        } else {
            self.skipBtn.setTitle("", for: .normal)
//            self.skipBtn.isHidden = true
            self.nextBtn.setImage(UIImage(named: ""), for: .normal)
            self.nextBtn.setTitle("   GET STARTED   ", for: .normal)
            self.nextBtn.tag = 1
        }
        /*
         * below code changes the background color of view on paging the scrollview
         */
//        self.scrollView(scrollView, didScrollToPercentageOffset: percentageHorizontalOffset)
        
    
        /*
         * below code scales the imageview on paging the scrollview
         */
        /*
        let percentOffset: CGPoint = CGPoint(x: percentageHorizontalOffset, y: percentageVerticalOffset)
        
        if(percentOffset.x > 0 && percentOffset.x <= 0.25) {
            
            slides[0].imageView.transform = CGAffineTransform(scaleX: (0.25-percentOffset.x)/0.25, y: (0.25-percentOffset.x)/0.25)
            slides[1].imageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.25, y: percentOffset.x/0.25)
            
        } else if(percentOffset.x > 0.25 && percentOffset.x <= 0.50) {
            slides[1].imageView.transform = CGAffineTransform(scaleX: (0.50-percentOffset.x)/0.25, y: (0.50-percentOffset.x)/0.25)
            slides[2].imageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.50, y: percentOffset.x/0.50)
            
        } else if(percentOffset.x > 0.50 && percentOffset.x <= 0.75) {
            slides[2].imageView.transform = CGAffineTransform(scaleX: (0.75-percentOffset.x)/0.25, y: (0.75-percentOffset.x)/0.25)
            slides[3].imageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.75, y: percentOffset.x/0.75)
            
        } else if(percentOffset.x > 0.75 && percentOffset.x <= 1) {
            slides[3].imageView.transform = CGAffineTransform(scaleX: (1-percentOffset.x)/0.25, y: (1-percentOffset.x)/0.25)
            slides[4].imageView.transform = CGAffineTransform(scaleX: percentOffset.x, y: percentOffset.x)
        } */
    }
    
    
    
    
    func scrollView(_ scrollView: UIScrollView, didScrollToPercentageOffset percentageHorizontalOffset: CGFloat) {
        
        if(pageControl.currentPage == 0) {
            //Change background color to toRed: 103/255, fromGreen: 58/255, fromBlue: 183/255, fromAlpha: 1
            //Change pageControl selected color to toRed: 103/255, toGreen: 58/255, toBlue: 183/255, fromAlpha: 0.2
            //Change pageControl unselected color to toRed: 255/255, toGreen: 255/255, toBlue: 255/255, fromAlpha: 1
            
            let pageUnselectedColor: UIColor = fade(fromRed: 255/255, fromGreen: 255/255, fromBlue: 255/255, fromAlpha: 1, toRed: 103/255, toGreen: 58/255, toBlue: 183/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
            pageControl.pageIndicatorTintColor = pageUnselectedColor
        
            
            let bgColor: UIColor = fade(fromRed: 103/255, fromGreen: 58/255, fromBlue: 183/255, fromAlpha: 1, toRed: 255/255, toGreen: 255/255, toBlue: 255/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
            slides[pageControl.currentPage].backgroundColor = bgColor
            
            let pageSelectedColor: UIColor = fade(fromRed: 81/255, fromGreen: 36/255, fromBlue: 152/255, fromAlpha: 1, toRed: 103/255, toGreen: 58/255, toBlue: 183/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
            pageControl.currentPageIndicatorTintColor = pageSelectedColor
        }
    }
    
    
    func fade(fromRed: CGFloat,
              fromGreen: CGFloat,
              fromBlue: CGFloat,
              fromAlpha: CGFloat,
              toRed: CGFloat,
              toGreen: CGFloat,
              toBlue: CGFloat,
              toAlpha: CGFloat,
              withPercentage percentage: CGFloat) -> UIColor {
        
        let red: CGFloat = (toRed - fromRed) * percentage + fromRed
        let green: CGFloat = (toGreen - fromGreen) * percentage + fromGreen
        let blue: CGFloat = (toBlue - fromBlue) * percentage + fromBlue
        let alpha: CGFloat = (toAlpha - fromAlpha) * percentage + fromAlpha
        
        // return the fade colour
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}

extension UIScrollView {

    func scrollTo(horizontalPage: Int? = 0, verticalPage: Int? = 0, animated: Bool? = true) {
        var frame: CGRect = self.frame
        frame.origin.x = frame.size.width * CGFloat(horizontalPage ?? 0)
        frame.origin.y = frame.size.width * CGFloat(verticalPage ?? 0)
        self.scrollRectToVisible(frame, animated: animated ?? true)
    }

}
