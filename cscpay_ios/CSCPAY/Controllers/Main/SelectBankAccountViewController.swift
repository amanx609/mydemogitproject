//
//  SelectBankAccountViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 22/02/21.
//

import UIKit

class SelectBankAccountViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnStart: UIButton!
    
    var bankAccountsList : [Account_List]?
    var userBankAccounts : BankAccountsList?
    var userDetilas : App_User?
    var selectedIndex:IndexPath?
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .semiBold,text: Language.getText(with: LanguageConstants.lbl_select_account_title))
        lblSubtitle.setup(textColor: .subtitleColor2,text: (Language.getText(with: LanguageConstants.lbl_select_account_subtitle)))
        btnStart.setup(fontType: .semiBold, textColor: .customWhite, bgColor: .taleColor, text: Language.getText(with: LanguageConstants.btn_select_account_start))
        bankAccountsList = CoredataManager.getDetails(Account_List.self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        self.tableView.tableFooterView = UIView()
        
    }
    
    
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func startButtonTapped(_ sender: Any) {
        guard self.selectedIndex != nil else {
            return
        }
        let user = CoredataManager.getDetail(App_User.self)
        if let vpa = user?.virtualAddress, vpa.count > 0 {
            addAccountToVpa(virtualAddress: vpa)
            return
        } else {
            self.checkVPA()
            return
        }
    }
    
}


//MARK: - TableView Delegate , DataSource

extension SelectBankAccountViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let bankList = userBankAccounts?.accountList?.count else{return 3}
        return bankList
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BankAccountCell", for: NSIndexPath(row: 0, section: 0) as IndexPath) as! BankAccountCell
        if let bank = userBankAccounts?.accountList![indexPath.row]{
            let bankName = CoredataManager.getDetails(Master_BankList.self)?.filter({$0.bankCode == bank.bankCode})
            cell.setup(bankName:  "\(bankName![indexPath.row].bankName ?? "ICICI")" + "\(bank.maskedAccountNumber ?? "XXXXXX7800")" ,icon: bankName?[indexPath.row].bankCode ?? "icici", type: "Savings account")
        }else{
          cell.setup(bankName: "ICICI Bank 1234",icon: "ICIC", type: "Savings account")
        }
        
        
        
        
        
//        if let bankCode = bankAccountsList?.accountList![indexPath.row].bankCode{
//            let name = CoredataManager.getSpecificRecord(Master_BankList.self, key: "bankCode", value: bankCode)
//            cell.setup(bankName: bankCode ,icon: "icici", type: "Savings account")
//        }else{
//            cell.setup(bankName: "ICICI Bank 1234",icon: "icici", type: "Savings account")
//        }
        
        if let selectedINdex = self.selectedIndex
        {
                if (selectedINdex as IndexPath == indexPath) {
                        cell.radioButton.isSelected = true
                }
                else {
                        cell.radioButton.isSelected = false
                }
        }
        else {
                cell.radioButton.isSelected = false
        }
        cell.delegate = self
            return cell
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

//MARK: - Radio Radio Button Action

extension SelectBankAccountViewController : RadioButtonDelegate {
    func radioButtonTapped(indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
            if selectedIndex == indexPath{
                selectedIndex = nil
            }else{
                selectedIndex = indexPath
            }
        self.tableView.reloadData()
    }
}

extension SelectBankAccountViewController{
    
    //MARK: - checkVPA
    
    func checkVPA() {
        self.showActivityIndicator()
        APIManager.call(CheckVPA.self, urlString: "indus/check", reqAction : "checkVPA",category: "IndusInd", pspId: "IBL", extra: userBankAccounts?.userInfo?.virtualAddress) { (res, err) in
            self.hideActivityIndicator()
            GlobalData.sharedInstance.checkVPA = res
            //            CoredataManager.saveUserBankList(bankList: <#T##AccountList#>)
            print(res)
            print(err)
            print("Done**************")
            self.registration()
        }
    }
    
    //MARK: - registration
    
    func registration() {
        self.showActivityIndicator()
        APIManager.call(RegisterVPA.self, urlString: "indus/register", reqAction : "registerVPA",category: "IndusInd", pspId: "IBL" , extra: userBankAccounts?.userInfo?.virtualAddress) { (res, err) in
            self.hideActivityIndicator()
            GlobalData.sharedInstance.registerVPA = res
            //self.refId = msg ?? "NA"
            print(res)
            print(err)
            print("Done**************")
            
            if err != nil {
                if let err = err as? RuntimeError {
                     print(err.message)
                }
            }else{
                DispatchQueue.main.async {
                    if let userInfo = res?.userInfo {
                        CoredataManager.updateUserVPA(info : userInfo)
                    }
                    guard let selectedBankAccount = self.userBankAccounts?.accountList![self.selectedIndex!.row] else{return}
                    print(selectedBankAccount)
                    CoredataManager.saveUserBankList(bankList: selectedBankAccount)
                    
    //                let vc = getSuccessViewController() as! SuccessViewController
    //                vc.isFromLink = true
//                    let vc = getChangeUPIThroughCardViewController() as! ChangeUPIThroughCardViewController

                    
                    
                    
                    let vc = getAddNewUPIViewController() as! AddNewUPIViewController
                    
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    //MARK: - addAccountToVpa
    
    func addAccountToVpa(virtualAddress : String) {
        guard let selectedBank = userBankAccounts?.accountList![selectedIndex!.row] else {return}
        self.showActivityIndicator()
        APIManager.call(AddAccountToVPA.self, urlString: "indus/account", reqAction : "addAccount",category: "IndusInd", pspId: "IBL", extra: virtualAddress , bankCode: selectedBank.bankCode ?? "INDB") { (res, err) in
            self.hideActivityIndicator()
            GlobalData.sharedInstance.addAccountToVPA = res
            print(res)
            print(err)
            print("Done**************")
            DispatchQueue.main.async {
                CoredataManager.saveUserBankList(bankList: selectedBank)

//                self.linkBankToVpa()
//                linkBank()
//                let vc = getSuccessViewController() as! SuccessViewController
//                vc.isFromLink = true
                
                
                /*
                if let npciData = CoredataManager.getDetails(NPCI_Data.self){
                    print(npciData)
                 getToken(status: false)
                }else{
                  getToken(status: true)
                }
                */
                
                
                
                
                
               /* let vc = getChangeUPIThroughCardViewController() as! ChangeUPIThroughCardViewController
                vc.bankId = self.selectedIndex?.row
                self.present(vc, animated: true, completion: nil) */
            }
        }
    }
}


extension SelectBankAccountViewController{
    
    //MARK: - GET NPCI TOKEN
//    func getNpciToken(status : Bool = false) {
//        self.showActivityIndicator()
//        APIManager.call(GetTokenResponse.self, urlString: "indus/apptoken", reqAction : "getToken",category: "IndusInd", pspId: "IBL", extra: "GET_TOKEN") { (res, err) in
//            self.hideActivityIndicator()
//
//            if err == nil{
//                if !status{
//                    if let response = res , let list = response.listKeys{
//                        print(response)
//                        GlobalData.sharedInstance.setToken = res
//                        CoredataManager.saveNpciTokenListKey(getNpciData: response)
//                    }
//                }else{
//                    let localNpciData =  APIManager.loadAllJson(GetTokenResponse.self, filename: "npci")
//                    print(localNpciData)
//                    GlobalData.sharedInstance.setToken = res
//                    CoredataManager.saveNpciTokenListKey(getNpciData: localNpciData!)
//                }
//            }
//
//            DispatchQueue.main.async {
//                let vc = getChangeUPIThroughCardViewController() as! ChangeUPIThroughCardViewController
//                vc.bankId = self.selectedIndex?.row
//                self.present(vc, animated: true, completion: nil)
//            }
//        }
//    }
    
    
    func listKeyService(){
        self.showActivityIndicator()
        APIManager.call(GetTokenResponse.self, urlString: "indus/apptoken", reqAction: "listKeys", category: "IndusInd", pspId: "IBL", extra: "LIST_KEYS") { res, err in
            self.hideActivityIndicator()
            if let response = res , let list = response.listKeys{
                print(list)
            }
        }
    }
    
}
















/*
let refreshAlert = UIAlertController(title: "Alert", message: "Are you sure , you want to add this account?", preferredStyle: UIAlertController.Style.alert)

refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
    linkBank()
    self.present(getHomeViewController(), animated: true, completion: nil)
   }))

refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
            refreshAlert .dismiss(animated: true, completion: nil)
   }))

    self.present(refreshAlert, animated: true, completion: nil)
 */
