//
//  OTPViewController.swift
//  CSC PAY
//
//  Created by Abhishek Ranjan on 07/01/21.
//

import UIKit
import CoreLocation

class OTPViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var mobileField: CustomTextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var topImage: UIImageView!
    
    
    
    
    func setupUI() {
//        LocationMgr.delegate = self
        lblTitle.setup(fontSize: 24, fontType: .semiBold,text: Language.getText(with: LanguageConstants.lbl_otp_title))
        lblDescription.setup(textColor: .subtitleColor,text: Language.getText(with: LanguageConstants.lbl_otp_description))
        mobileField.setup(textColor: .subtitleColor, text: nil, placeholderText: Language.getText(with: LanguageConstants.txt_mobile_placeholder))
        btnSend.setup(fontType: .semiBold, textColor: .customWhite, bgColor: .taleColor, text: Language.getText(with: LanguageConstants.btn_send_otp))
        if let tower = UIImage.gifImageWithName("tower") {
            self.topImage.image = tower }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        LocationMgr.getUserLocation()
        print(location)
        self.hideKeyboardWhenTappedAround()
        
            setupUI()
        

        
        
        
        
    }
    @IBAction func sendOTPBtnTapped(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
            self.view.isUserInteractionEnabled = true
        }
        
//        if Constants.forMerchant{
//            self.present(getOTPVerificationViewController(), animated: true, completion: nil)
//        }else{
            DispatchQueue.main.async {
                self.responseLabel.text = ""
                self.view.isUserInteractionEnabled = false
                
                guard let mob = self.mobileField.text, mob.count > 0 else {
                self.responseLabel.text = Language.getText(with: LanguageConstants.alert_enter_mobile) ?? Constants.alert_enter_mobile
                return
            }
            if !mob.isValidContact {
                self.responseLabel.text = Language.getText(with: LanguageConstants.alert_enter_valid_mobile) ?? Constants.alert_enter_valid_mobile
                return
            }
            //Check Internet availability
                if self.currentReachabilityStatus == .notReachable {
                self.responseLabel.text = Language.getText(with: LanguageConstants.alert_enable_internet) ?? Constants.alert_enable_internet
                return
            }
                if Constants.api_off {
                    DispatchQueue.main.async {
                        let vc = getOTPVerificationViewController() as! OTPVerificationViewController
                        vc.mobile       = mob
                        self.present(vc, animated: true, completion: nil)
                    }
                } else {
                    if Constants.byPass{
                        let vc = getOTPVerificationViewController() as! OTPVerificationViewController
                        vc.mobile       = mob
                        self.present(vc, animated: true, completion: nil)
                    }
                    self.callOtp(mob: mob)
                }
                /*
                let msg = "thisissampletext2" // "\(getUUID() ?? "")|\(getUUID() ?? "")0|\(getUUID() ?? ""))"
                let key = "ADEEC43E866BF59E"
                let enc = AppSecure.AES_Encrypt(plainText: msg, key: key ) // getUUID() ?? "")
                print("enc")
                print(enc)
                   
               // let a = "bn6NSrquwO0oGU3PaPBcoQ"
                let dec    = AppSecure.AES_Decrypt(encData: enc, key: key)
                let decrypted = (dec as! String).base64Decoded()
                print("Dec")
                print(decrypted)
                //String smstxt = smskeyword(from checkDevice response) + " " + URLEncoder.encode(Base64.encodeToString(encsms, Base64.NO_WRAP), "utf-8");
                
                let vc = getOTPVerificationViewController() as! OTPVerificationViewController
                vc.mobile       = mob
                self.present(vc, animated: true, completion: nil)
                 */
           // }
        }
        
        
        
    }
    
    
    
    override func geocodeReverse(userLocation: CLLocation) {
        print(userLocation)
        Constants.geolat = String(format: "%.04f", userLocation.coordinate.latitude)
        Constants.geolong = String(format: "%.04f", userLocation.coordinate.longitude)
    }
    func callOtp(mob: String) {
        self.showActivityIndicator()
        APIManager.call(OTPResponse.self, urlString: "start/otp", reqAction : "generate",category: mob, mobCrypt: true, isNew : true) { (result, err) in
            self.hideActivityIndicator()
            DispatchQueue.main.async {
            if err != nil {
                if let err = err as? RuntimeError {
                    self.responseLabel.text = err.message
                } else {
                    self.responseLabel.text = err?.localizedDescription ?? Language.getText(with: LanguageConstants.general_error_generic) ?? Constants.SOMETHING_WRONG
                }
            } else {
                GlobalData.sharedInstance.otpResp = result
                    let vc = getOTPVerificationViewController() as! OTPVerificationViewController
                    vc.mobile       = mob
                    vc.otpResponse  = result
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}
extension OTPViewController : UITextFieldDelegate {
    func animateTextField(textField: UITextField, up: Bool)
        {
            let movementDistance:CGFloat = -160
            let movementDuration: Double = 0.3

            var movement:CGFloat = 0
            if up
            {
                movement = movementDistance
            }
            else
            {
                movement = -movementDistance
            }
            UIView.beginAnimations("animateTextField", context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            UIView.setAnimationDuration(movementDuration)
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
            UIView.commitAnimations()
        }
    public func textFieldDidBeginEditing(_ textField: UITextField) {
            //moveTextField(textField, moveDistance: -250, up: true)
        self.animateTextField(textField: textField, up:true)
        }

        // Finish Editing The Text Field
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.animateTextField(textField: textField, up:false)
           // moveTextField(textField, moveDistance: -250, up: false)
        }

        // Hide the keyboard when the return key pressed
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }

        // Move the text field in a pretty animation!
        func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
            let moveDuration = 0.3
            let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
            UIView.animate(withDuration: moveDuration, animations: {
            }) { _ in
                self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
            }
        }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
                let str = text + string
                if str.count <= 10 {
                    if (string.isEmpty && range.length > 0) {
                        textField.text = text.count > range.length ? String(text.dropLast(range.length)) : ""
                        return false
                    }
                    return true
                }
                return false
    }
}

/*
 
 
//        detectScreenShot {
//            let imageview = UIImageView.init(image: UIImage.init(named: "bg_splash"))
//            imageview.center = self.view.center
//            self.view.addSubview(imageview)
//            print("Screen shot taken")
//        }
//        if UIScreen.main.isCaptured {
//            print("Screen shot taken2")
//        }
    }
    func detectScreenShot(action: @escaping () -> ()) {
        let mainQueue = OperationQueue.main
        NotificationCenter.default.addObserver(forName: UIApplication.userDidTakeScreenshotNotification, object: nil, queue: mainQueue) { notification in
            // executes after screenshot
            action()
        }
    }

*/
