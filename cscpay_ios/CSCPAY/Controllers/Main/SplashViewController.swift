//
//  ViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 03/05/21.
//

import UIKit

class SplashViewController : UIViewController {
    
    @IBOutlet weak var lblPayTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    var appStage: String?
    var merchantRequestResponse: CommonModel?
    var docTypeUpload : CompanyDocModel?
    var merchantRegResponse : MerchantRegistrationModel?
    
    
    
      override func viewDidLoad() {
          super.viewDidLoad()
          checkSimulator()
          if !Constants.presentVCForTest {
              setup() }
      }
      override func viewDidAppear(_ animated: Bool) {
          if Constants.presentVCForTest {
              self.present(Constants.vc, animated: true, completion: nil)
              return
          }
      }
      
      func checkSimulator() {
          #if targetEnvironment(simulator)
          // your code
          Constants.isSimulator = true
          if Constants.printResponse {
              print("In Simulator") }
          #else
          Constants.isSimulator = false
          if Constants.printResponse {
              print("Not In Simulator") }
          #endif
          
          
      }

    func setup() {
        UIView.animate(withDuration: 1.2, delay: 0.4, options: .curveEaseOut, animations: {
            self.imgView.alpha = 1.0
            self.lblPayTitle.alpha = 1.0
            self.lblPayTitle.text = "© 2021 CSC E- Governance Services India Ltd"
        }, completion: nil)
        
        if self.currentReachabilityStatus != .notReachable {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.6) {
                self.callMaster()
            }
        } else {
//            if let configDB = CoredataManager.getDetail(App_Config.self) {
//                self.appStage = configDB.appStage
//                self.presentVC(appStage: self.appStage)
//            } else {
//                notImplementedYetAlert(base: self, msg: "Please Enable Internet Connection.")
//                return
//            }
            
            if let configDB = CoredataManager.getDetail(Merchant_App_Cred.self){
                self.appStage = configDB.appStage
                self.presentVC(appStage: self.appStage)
            }else {
                notImplementedYetAlert(base: self, msg: "Please Enable Internet Connection.")
                return
            }
        }
    }
    func callMaster() {
        APIManager.call(MerchantConfigResponse.self, urlString: "start/master", reqAction : "config", mobCrypt: true, isNew : true) { (res, err) in
            
            DispatchQueue.main.async {
                if let error = err {
                    if Constants.printResponse { print(error) }
                } else if let resp = res {
//                    GlobalData.sharedInstance.config = resp
                    
                    GlobalData.sharedInstance.merchantConfig = resp
                    
                    //Check there is any changes
                    
                    
                    
//                    self.appStage = self.configChanges(config: resp)
                    //Cleaning Entity
//                    CoredataManager.cleanEntity(App_Config.self)
                    CoredataManager.cleanEntity(Merchant_App_Cred.self)
                    
//                    if let _ = resp.body?.pspBank {
//                        CoredataManager.cleanEntity(Master_BankList.self)
//                    }
                    //Storing into DB
                    
                    CoredataManager.saveMerchantConfig(config: resp, appStage: self.appStage , isSave: false)
                    
//                    CoredataManager.saveConfig(config: resp,appStage: self.appStage, isSave: false)
                    
                    
//                    for bank in resp.body?.pspBank ?? [PSPBank]() {
//                        CoredataManager.saveMasterBanks(bank : bank, version: resp.body?.pspVersion ?? "0", isSave :false)
//                    }
                    CoredataManager.saveContext()
                }
                self.presentVC(appStage: self.appStage)
                
            }
        }
    }
    func presentVC(appStage: String?) {
        DispatchQueue.main.async {
            if Constants.forMerchant{
                if Constants.byPass{
                    self.present(getOTPViewController(), animated: true, completion: nil)
                }else{
                    if (UD.SharedManager.getValues(key: "AlreadyRegWithOtp", type: .bool) as? Bool ?? false){
                        self.checkMerchantService()
                    }else{
                        self.present(getPageViewController(), animated: true, completion: nil)
                    }
                }
            }else{
                switch appStage{
                case "1":
                    self.present(getPageViewController(), animated: true, completion: nil)
                case "2":
                    self.present(getUserDetialsViewController(), animated: true, completion: nil)
                case "3" :
                    self.present(getHomeViewController(), animated: true, completion: nil)
                case "4":
                    self.present(getMerchantHomeViewController(), animated: true , completion: nil)
                default :
                    self.present(getOTPViewController(), animated: true, completion: nil)
                }
            }
            
        }
        
    }
    func configChanges(config: ConfigResponse) -> String? {
        var changes : [String]? = nil
        
        if let configDB = CoredataManager.getDetail(App_Config.self) {
            changes = [String]()
            //Check there is any changes in the api
            if config.body?.appLabel != configDB.appLabel {
                changes?.append("appLabel")
            }
            if config.body?.apiEndpoint != configDB.apiEndpoint {
                changes?.append("apiEndpoint")
            }
            if config.body?.bankMaster != configDB.bankMaster {
                changes?.append("bankMaster")
            }
            if config.body?.faqData != configDB.faqData {
                changes?.append("faqData")
            }
            if changes?.count ?? 0 > 0 {
                UserDefaults.standard.setValue(changes, forKey: Constants.Master_Changes)
                UserDefaults.standard.synchronize()
            }
            return configDB.appStage
        }
        
        return nil
    }
}


extension SplashViewController{
    func checkMerchantService(){
        
        if let status = UD.SharedManager.getValues(key: "ActiveStatus", type: .int) as? Int{
            if  let panNo = UD.SharedManager.getValues(key: "PANNo.", type: .string) as? String{
                if  let legalName = UD.SharedManager.getValues(key: "LegalName", type: .string) as? String{
                    if  let mobileNo = UD.SharedManager.getValues(key: "MobileNo.", type: .string) as? String{
                        if  let actvStatus = UD.SharedManager.getValues(key: "ActiveStatus", type: .int) as? Int{
                            
                            print(panNo , legalName , mobileNo , actvStatus)
                            merchantRequestResponse = CommonModel()

                            merchantRequestResponse?.mobileNo = "\(mobileNo)"
                            merchantRequestResponse?.panNo = panNo
                            merchantRequestResponse?.legalName = legalName
                        }
                    }
                }
            }
        }
        
        
        
        
        
        
        
        print(merchantRequestResponse ?? CommonModel())
        
        
        guard let requestResponse = merchantRequestResponse else{
            self.present(getPanCardViewController(), animated: true, completion: nil)
            return
        }
        
        print(requestResponse)
        
        self.showActivityIndicator()
        
        APIManager.getMerchant(MerchantRegistrationModel.self, reqAction: "checkMerchant", urlString: "merchant/check", requestModel: requestResponse , merchantRegresponse: nil) { res, err, text in
            self.hideActivityIndicator()
            DispatchQueue.main.async {
                if err != nil {
                    if let err = err as? RuntimeError {
                        if err.code == "701"{
                            DispatchQueue.main.async {
                                let vc = getGeneralDetailsViewController() as! GeneralDetailsViewController
                                self.present(vc, animated: true, completion: nil)
                            }
                        }
                        print(err.message)
                    } else {
                        print(err?.localizedDescription ?? Language.getText(with: LanguageConstants.general_error_generic) ?? Constants.SOMETHING_WRONG)
                    }
                } else {
                    if Constants.authenticationStatus != "Done"{
                        self.authenticationWithTouchID {
                            DispatchQueue.main.async {
                                if let result : MerchantRegistrationModel = res {
                                    let vc = getMerchantSuccessViewController() as! MerchantSucessVC
                                    vc.showSucessFrom = .splash
                                    GlobalData.sharedInstance.merchantRegistrationResponse = result
                                    
                                    CoredataManager.updateDetail(App_User.self, key: "aesKey", value: result.cred?.aesKey ?? "")
                                    CoredataManager.updateDetail(App_User.self, key: "deviceId", value: result.head.deviceID ?? "")
                                    CoredataManager.updateDetail(App_User.self, key: "hashToken", value: result.cred?.hashToken ?? "")
                                    CoredataManager.updateDetail(App_User.self, key: "hmac", value: result.cred?.hmac ?? "")
                                    
                                    
                                    vc.merchantRegResponse = result
                                    if result.body.activeStatus == "10"{
                                        vc.msgString = "You're suceessfully registered with CSC PAY. Please upload your documents to continue"
                                        self.present(vc, animated: true, completion: nil)
                                    }else if result.body.activeStatus == "20"{
                                        vc.msgString = "Documents suceessfully uploaded. Our verification team contact you soon , please wait untill then."
                                        self.present(vc, animated: true, completion: nil)
                                    }else{
                                        let vc = getMerchantHomeViewController() as! MerchantHomeViewController
                                        vc.merchantRegResponse = result
                                        self.present(vc, animated: true, completion: nil)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

extension SplashViewController{
    func loadUploadDoc(){
        docTypeUpload = APIManager.loadAllJson(CompanyDocModel.self, filename: "CompanyDoc")
        Constants.uploadDocData = docTypeUpload!
    }
}
