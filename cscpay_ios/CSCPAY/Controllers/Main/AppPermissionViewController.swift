//
//  AppPermissionViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 03/06/21.
//

import UIKit
import CoreLocation

class AppPermissionViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblLocationInfo: UILabel!
    @IBOutlet weak var lblContacts: UILabel!
    @IBOutlet weak var lblContactsinfo: UILabel!
    @IBOutlet weak var lblCamera: UILabel!
    @IBOutlet weak var lblCameraInfo: UILabel!
    @IBOutlet weak var lblGallery: UILabel!
    @IBOutlet weak var lblGalleryInfo: UILabel!
    @IBOutlet weak var btnProceed: UIButton!
    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .bold, textColor: .customBlue, text: Language.getText(with: LanguageConstants.lbl_app_permission_title))
        lblSubtitle.setup(textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_app_permission_subtitle))
        lblLocation.setup(fontSize: 18, fontType: .semiBold, textColor: .titleColor, text: Language.getText(with: LanguageConstants.lbl_location))
        lblLocationInfo.setup(fontSize: 14, fontType: .regular, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_location_info))
        lblContacts.setup(fontSize: 18, fontType: .semiBold, textColor: .titleColor, text: Language.getText(with: LanguageConstants.lbl_contacts))
        lblContactsinfo.setup(fontSize: 14, fontType: .regular, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_contacts_info))
        lblCamera.setup(fontSize: 18, fontType: .semiBold, textColor: .titleColor, text: Language.getText(with: LanguageConstants.lbl_camera))
        lblCameraInfo.setup(fontSize: 14, fontType: .regular, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_camera_info))
        lblGallery.setup(fontSize: 18, fontType: .semiBold, textColor: .titleColor, text: Language.getText(with: LanguageConstants.lbl_gallery))
        lblGalleryInfo.setup(fontSize: 14, fontType: .regular, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_gallery_info))
        btnProceed.setup(fontType: .semiBold, textColor: .customWhite, bgColor: .taleColor, text: Language.getText(with: LanguageConstants.btn_allow_to_proceed))
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
        
    @IBAction func allowBtnTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.present(getUserDetialsViewController(), animated: true, completion: nil)
        }
        
    }
}
