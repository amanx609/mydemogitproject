//
//  OTPVerificationViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 16/02/21.
//

import UIKit

class OTPVerificationViewController: UIViewController {
    
    @IBOutlet weak var first: UITextField!
    @IBOutlet weak var second: UITextField!
    @IBOutlet weak var third: UITextField!
    @IBOutlet weak var fourth: UITextField!
    @IBOutlet weak var fifth: UITextField!
    @IBOutlet weak var sixth: UITextField!
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var resendOTPLabel: UILabel!
    @IBOutlet weak var lblVerificationTitle: UILabel!
    @IBOutlet weak var lblotpSent: UILabel!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var topImage: UIImageView!
    
    var mobile      : String?
    var otpResponse : OTPResponse?
    var resendTimer : Timer?
    var count = 60  // 60sec
    
    func setupUI() {
        lblVerificationTitle.setup(fontSize: 24, fontType: .semiBold,text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblotpSent.setup(textColor: .subtitleColor,text: (Language.getText(with: LanguageConstants.lbl_otp_sent) ?? Constants.otpSentHint) + (mobile?.suffix(2) ?? ""))
        btnVerify.setup(fontType: .semiBold, textColor: .customWhite, bgColor: .taleColor, text: Language.getText(with: LanguageConstants.btn_verify))
        if let verify = UIImage.gifImageWithName("verify") {
            self.topImage.image = verify }
        if let respMsg = otpResponse?.body?.resMsg {
            lblotpSent.text = respMsg
        }
        guard let _ = otpResponse, let _ = mobile else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        setupUI()
        self.setupResend()
    }
    func setupResend() {
        self.resendOTPLabel.halfTextColorChange(fullText: Language.getText(with: LanguageConstants.lbl_resend_otp) ?? Constants.otpResend, changeText: Language.getText(with: LanguageConstants.btn_resend_otp) ?? Constants.otpResendBtn, color: .lightBlue, textSize: 11)
        self.resendOTPLabel.isUserInteractionEnabled = true
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnLabel(_ :)))
        tapgesture.numberOfTapsRequired = 1
        self.resendOTPLabel.addGestureRecognizer(tapgesture)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.first.becomeFirstResponder()
        }
    }
    
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    //MARK:- tappedOnLabel
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = self.resendOTPLabel.text else { return }
        let termsRange = (text as NSString).range(of: Language.getText(with: LanguageConstants.btn_resend_otp) ?? Constants.otpResendBtn)
        if gesture.didTapAttributedTextInLabel(label: resendOTPLabel, inRange: termsRange) {
            //print("Tapped Resend Button")
            self.otpResend()
        }
    }
    @IBAction func verifyBtnTapped(_ sender: Any) {
        //        if Constants.forMerchant{
        //            self.present(getChooseYourOptionViewController(), animated: true, completion: nil)
        //        }else{
        //            validateOTP()
        //        }
        validateOTP()
        
    }
    func showAlert(msg: String) {
        DispatchQueue.main.async {
            self.responseLabel.text = msg
        }
    }
    func validateOTP() {
        DispatchQueue.main.async {
            self.showAlert(msg: "")
            if self.first.text?.count == 0 && self.second.text?.count == 0 && self.third.text?.count == 0 && self.fourth.text?.count == 0 && self.fifth.text?.count == 0 && self.sixth.text?.count == 0 {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_otp) ?? Constants.alert_enter_otp)
            }
            guard let one = self.first.text, one.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
                return
            }
            guard let two = self.second.text, two.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
                return
            }
            guard let three = self.third.text, three.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
                return
            }
            guard let four = self.fourth.text, four.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
                return
            }
            guard let five = self.fifth.text, five.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
                return
            }
            guard let six = self.sixth.text, six.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
                return
            }
            
            //MARK: - For Bypass only
            
            if Constants.byPass{
                self.present(getPanCardViewController(), animated: true, completion: nil)
            }
            
            //MARK: - For Bypass only

            
            
            if var userInfo = UserDefaults.standard.value(forKey: Constants.User_Info) as? [String: String] {
                userInfo["mobile"] = self.mobile
                UserDefaults.standard.setValue(userInfo, forKey: Constants.User_Info)
            } else {
                let user = ["mobile" : self.mobile ]
                UserDefaults.standard.setValue(user, forKey: Constants.User_Info)
            }
            if Constants.api_off {
                self.present(getPageViewController(), animated: true, completion: nil)
                return
            }
            //self.present(getPageViewController(), animated: true, completion: nil)
            //self.present(getAppPermissionViewController(), animated: true, completion: nil)
            //self.otpValidate(otpHash: self.otpResponse?.body?.otpHash ?? "NA")
            
            let otpHash = "\(one)\(two)\(three)\(four)\(five)\(six)\(self.mobile ?? "")\(self.otpResponse?.head?.refId ?? "")".sha256() as? String
            if otpHash != (self.otpResponse?.body?.otpHash ?? "NA") {                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
            } else {
                if self.otpResponse?.body?.verifyCall ?? "" == "N" {
                    self.presentOnboardVC()
                    return
                }
                self.otpValidate(otpHash: otpHash ?? "NA")
            }
            self.otpValidate(otpHash: self.otpResponse?.body?.otpHash ?? "NA")
        }
    }
    func presentOnboardVC() {
        if let resp = self.otpResponse, let mob = mobile {
            let vc = getSetMerchantPinViewController() as! SetMerchantPinViewController
            vc.mobileNumber = mob
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    func otpValidate(otpHash: String) {
        self.showActivityIndicator()
        APIManager.call(OTPResponse.self, urlString: "start/otp", reqAction : "validate", category: self.mobile ?? "NA", refId:otpResponse?.head?.refId ?? "NA", otpHash: otpHash, mobCrypt: true, isNew : true) { (res1, err) in
            
            self.hideActivityIndicator()
            if let result = res1 {
                CoredataManager.saveCred(resp: result)
            }
            GlobalData.sharedInstance.otpValid = res1
            
            DispatchQueue.main.async {
                if err != nil {
                    if let err = err as? RuntimeError {
                        self.responseLabel.text = err.message
                    } else {
                        self.responseLabel.text = err?.localizedDescription ?? Language.getText(with: LanguageConstants.general_error_generic) ?? Constants.SOMETHING_WRONG
                    }
                } else {
                    self.presentOnboardVC()
                }
            }
        }
    }
    func otpResend() {
        self.showActivityIndicator()
        APIManager.call(OTPResponse.self, urlString: "start/otp", reqAction : "resend",category: mobile! , refId: otpResponse?.head?.refId ?? "NA", resHash: otpResponse?.body?.resHash ?? "NA", mobCrypt: true, isNew : true) { (result, err) in
            print(result)
            self.hideActivityIndicator()
            DispatchQueue.main.async {
                if err != nil {
                    if let err = err as? RuntimeError {
                        self.responseLabel.text = err.message
                    } else {
                        self.responseLabel.text = err?.localizedDescription ?? Language.getText(with: LanguageConstants.general_error_generic) ?? Constants.SOMETHING_WRONG
                    }
                } else {
                    //Success goes here
                    GlobalData.sharedInstance.otpResp = result
                    self.otpResponse = result
                    self.resendTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
                }
            }
        }
    }
    @objc func update() {
        if(count > 0) {
            count = count - 1
            self.resendOTPLabel.isUserInteractionEnabled = false
            self.resendOTPLabel.text    = "\(Language.getText(with: LanguageConstants.btn_resent_count) ?? Constants.otpResendCount) \(self.count)s"
        }
        else {
            resendTimer?.invalidate()
            count = 60
            DispatchQueue.main.async {
                self.setupResend()
            }
        }
    }
}
extension OTPVerificationViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (range.length == 1) {
            if textField == sixth {
                fifth?.becomeFirstResponder()
            }
            if textField == fifth {
                fourth?.becomeFirstResponder()
            }
            if textField == fourth {
                third?.becomeFirstResponder()
            }
            if textField == third {
                second?.becomeFirstResponder()
            }
            if textField == second {
                first?.becomeFirstResponder()
            }
            if textField == first {
                first?.resignFirstResponder()
            }
            textField.text? = ""
            return false
        } else {
            
            //This lines allows the user to delete the number in the textfield.
            if string.isEmpty{
                return true
            }
            //----------------------------------------------------------------
            
            //This lines prevents the users from entering any type of text.
            if Int(string) == nil {
                return false
            }
            //----------------------------------------------------------------
            
            //This lines lets the user copy and paste the One Time Code.
            //For this code to work you need to enable subscript in Strings https://gist.github.com/JCTec/6f6bafba57373f7385619380046822a0
            if string.count == 6 {
                first.text = "\(string[0])"
                second.text = "\(string[1])"
                third.text = "\(string[2])"
                fourth.text = "\(string[3])"
                fifth.text = "\(string[4])"
                sixth.text = "\(string[5])"
                
                DispatchQueue.main.async {
                    self.dismissKeyboard()
                    self.validateOTP()
                }
            }
            //----------------------------------------------------------------
            
            //This is where the magic happens. The OS will try to insert manually the code number by number, this lines will insert all the numbers one by one in each TextField as it goes In. (The first one will go in normally and the next to follow will be inserted manually)
            if string.count == 1 {
                if (textField.text?.count ?? 0) == 1 && textField.tag == 0{
                    if (second.text?.count ?? 0) == 1{
                        if (third.text?.count ?? 0) == 1{
                            if (fourth.text?.count ?? 0) == 1{
                                if (fifth.text?.count ?? 0) == 1{
                                    sixth.text = string
                                    DispatchQueue.main.async {
                                        self.dismissKeyboard()
                                        //self.validCode()
                                    }
                                    return false
                                }else{
                                    fifth.text = string
                                    return false
                                }
                            }else{
                                fourth.text = string
                                return false
                            }
                        }else{
                            third.text = string
                            return false
                        }
                    }else{
                        second.text = string
                        return false
                    }
                }
            }
            //----------------------------------------------------------------
            
            
            //This lines of code will ensure you can only insert one number in each UITextField and change the user to next UITextField when function ends.
            guard let textFieldText = textField.text,
                  let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                      return false
                  }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            
            
            if count == 1{
                if textField.tag == 0{
                    DispatchQueue.main.async {
                        self.second.becomeFirstResponder()
                    }
                    
                }else if textField.tag == 1{
                    DispatchQueue.main.async {
                        self.third.becomeFirstResponder()
                    }
                    
                }else if textField.tag == 2{
                    DispatchQueue.main.async {
                        self.fourth.becomeFirstResponder()
                    }
                    
                }else if textField.tag == 3{
                    DispatchQueue.main.async {
                        self.fifth.becomeFirstResponder()
                    }
                    
                }else if textField.tag == 4{
                    DispatchQueue.main.async {
                        self.sixth.becomeFirstResponder()
                    }
                    
                }else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                        self.dismissKeyboard()
                        self.validateOTP()
                    })
                }
            }
            
            return count <= 1
            //----------------------------------------------------------------
        }
    }
    
    func animateTextField(textField: UITextField, up: Bool)
    {
        let movementDistance:CGFloat = -160
        let movementDuration: Double = 0.3
        
        var movement:CGFloat = 0
        if up
        {
            movement = movementDistance
        }
        else
        {
            movement = -movementDistance
        }
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        //moveTextField(textField, moveDistance: -250, up: true)
        self.animateTextField(textField: textField, up:true)
    }
    
    // Finish Editing The Text Field
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.animateTextField(textField: textField, up:false)
        // moveTextField(textField, moveDistance: -250, up: false)
    }
    
    // Hide the keyboard when the return key pressed
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Move the text field in a pretty animation!
    func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        UIView.animate(withDuration: moveDuration, animations: {
        }) { _ in
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        }
    }
}


extension OTPVerificationViewController {
    func onSuccess() {
        if Constants.forMerchant{
            if Constants.byPass{
                let vc = getPanCardViewController() as! PANCheckVC
                self.present(vc, animated: true, completion: nil)
            }else{
                CoredataManager.saveContext()
                if Constants.authenticationStatus != "Done"{
                    DispatchQueue.main.async {
                        let vc = getCheckMerchantPinViewController() as! CheckMerchantPinViewController
                        vc.delegate = self
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        }else{
            if let _ = CoredataManager.updateDetail(App_Config.self, key: "appStage", value: "1", isSave: false) {}
            CoredataManager.saveContext()
            self.present(getPageViewController(), animated: true, completion: nil)
        }
    }
}

extension OTPVerificationViewController: SMPDelegate {
    func shareData(data: String) {
        if Constants.printResponse{print(data)}
        if let resp = self.otpResponse, let mob = mobile {
            CoredataManager.saveUser(resp: resp,mobile: mob, appPin: data)
            onSuccess()
        }
    }
    
    
}

extension OTPVerificationViewController: VerificationPinDelegate {
    
    func successfullPin() {
        DispatchQueue.main.async {
            let vc = getPanCardViewController() as! PANCheckVC
            vc.mobileNumber = self.mobile
            self.present(vc, animated: true, completion: nil)
        }
    }
    
}
