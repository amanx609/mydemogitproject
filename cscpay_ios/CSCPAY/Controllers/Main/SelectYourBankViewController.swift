//
//  SelectYourBankViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 22/02/21.
//

import UIKit
import MessageUI
import CoreLocation
import MapKit

class SelectYourBankViewController: UIViewController   , LocationUpdateProtocol{
    func locationDidUpdateToLocation(location: CLLocation) {
        print(location)
        print(geocodeReverseVC(userLocation: location))
    }
    
    
    
    
    

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var myCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var fetchingAlertView: UIView!
    @IBOutlet weak var fetchAkertView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var tryAgainButton: UIButton!
    
    
    let inset: CGFloat = 16
    let minimumLineSpacing: CGFloat = 10
    let minimumInteritemSpacing: CGFloat = 10
    let cellsPerRow = 4
    let cellsPerColumn = 2
    
    let LocationMgr = UserLocationManager.SharedManager

    var bankList = [Master_BankList]()
    var filteredBankList = [Master_BankList]()
    var popularBankList = [Master_BankList]()
    var userDetails : App_User?
    var refId = "NA"
    var timer : Timer?
    var currentLocation : String?
    var loc : CLLocation?

    var geo_long : String?
    var geo_lat : String?

    
    var searchActive : Bool = false
    var data = ["A P Mahesh Bank","Abhyudaya Cooperative Bank Ltd","Adarsh Co-operative Bank Limited","Airtel Payments Bank","Allahabad Bank","Andhra Bank", "Apna Sahakari Bank", "Axis Bank", "BHARAT COOPERATIVE BANK MUMBAI LTD", "Bank of Maharashtra", "Bank of Baroda", "Bank of India", "Canara Bank", "Catholic Syrian Bank", "Central Bank of India", "Citibank", "Citizens Co Operative Bank Ltd", "DBS Bank", "DCB", "Dena Bank"]
    var bankIcons = ["", "", "", "airtel", "allahabad", "andhra", "asb", "axis", "bcb", "bom", "bob", "boi", "canara", "catholic", "CBI", "citi", "ccob", "dbs", "DCB", "dena"]
    var filtered:[String] = []
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        LocationMgr.delegate = self
        LocationMgr.getUserLocation()
        hideKeyboardWhenTappedAround()
        self.tableView.tableFooterView = UIView()
        self.searchTableView.tableFooterView = UIView()
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.setup(placeholderText: "Type here...")
        userDetails = CoredataManager.getDetail(App_User.self)
        tryAgainButton.setTitleColor(UIColor.white, for: .normal)
//        locationDidUpdateToLocation(location: self.loc!)
         
        
        DispatchQueue.main.async {
            if let banks = CoredataManager.getALLDetails(Master_BankList.self, sortBytTime: true, key: "bankName", ascending: true, sortCaseSensitive: true) {
                DispatchQueue.main.async {
                    self.popularBankList.removeAll()
                    for (key , value) in Constants.popularBanks.enumerated(){
                        let popularBank = banks.filter{$0.bankCode == Constants.popularBanks[key]["title" as! String] as? String}
                        self.popularBankList.append(contentsOf: popularBank)
                        self.collectionView.reloadData()
                    }
                    print(self.popularBankList)
                    self.bankList = banks
                    self.tableView.reloadData()
                }
//                if let npciData = CoredataManager.getDetails(NPCI_Data.self){
//                    print(npciData)
//                    getToken(status: false)
//                }else{
//                    getToken(status: true)
//                }
            }
            //just to check
            self.getBankAccounts()
            //self.checkDevice()
        }
        
        

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
       
    }
    
    
    @IBAction func tryAgainButtonTappped(_ sender: Any) {
        self.backgroundView.isHidden = true
    }
    
    
    
    
    
    

    
    
    
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        switch kind {

        case UICollectionView.elementKindSectionHeader:
            guard
                  let headerView = collectionView.dequeueReusableSupplementaryView(
                    ofKind: kind,
                    withReuseIdentifier: "CollectionHeaderView",
                    for: indexPath) as? CollectionHeaderView
                  else {
                    fatalError("Invalid view type")
                }
                headerView.titleLabel.text = "Popular Banks"
                return headerView
            

        default:

            print("Unexpected element kind")
            return UICollectionReusableView()
        }
    }
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        var height = collectionView.collectionViewLayout.collectionViewContentSize.height
//        if searchActive { height = 10 }
//        myCollectionViewHeight.constant = height
//        collectionView.contentSize.height = 10
//        self.view.layoutIfNeeded()
//    }
    
    
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.fetchAkertView.isHidden = true
            self.backgroundView.isHidden = true
        }
//        self.present(getSelectBankAccountViewController(), animated: true, completion: nil)
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func callDeviceBtn(_ sender: Any) {
        getBankAccounts()
    }
    
}
extension SelectYourBankViewController : UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
            //searchActive = true;
        }

        func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
            searchActive = false;
        }

        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            searchActive = false;
        }

        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchActive = false;
        }

        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

            if !Constants.api_off {
                
            filteredBankList = bankList.filter { ($0.bankName?.lowercased().contains(searchText.lowercased()))! }
            } else {
            
            filtered = data.filter({ (text) -> Bool in
                let tmp: NSString = text as NSString
                let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
            }
            DispatchQueue.main.async {
                var banksCount = 0
                if Constants.api_off {
                    banksCount = self.filtered.count
                } else {
                    banksCount = self.filteredBankList.count
                }
                if(banksCount == 0){
                    self.searchActive = false;
                    self.searchTableView.isHidden = true
                } else {
                    self.searchActive = true;
                    self.searchTableView.isHidden = false
                    //self.myCollectionViewHeight.constant = 1
                }
                self.tableView.reloadData()
                self.searchTableView.reloadData()
                self.collectionView.reloadData()
                self.viewDidLayoutSubviews()
            }
            
        }
}
extension SelectYourBankViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if searchActive {
            return 0 }
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if searchActive {
            return CGSize(width: 1, height: 1)
        } else {
            return CGSize(width: collectionView.frame.width, height: 35) }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(searchActive) {
            return 0
        } else {
            return popularBankList.count
           // return Constants.Bank_Items.count
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: "BankCell", for: indexPath) as! BankCell
        self.collectionView = collectionView
        
        //cell.setup(bankName: self.bankList[indexPath.row].bankName ?? "-", icon: nil)
        cell.setup(bankName: popularBankList[indexPath.row].bankName ?? "ICICI",  icon: popularBankList[indexPath.row].bankCode)
       
        
        
        //cell.setup(bankName: Constants.Bank_Items[indexPath.row]["title"]! as! String, icon: Constants.Bank_Items[indexPath.row]["icon"] as? String)

        
        //        //cell.titleLabel.text = (Constants.Bank_Items[indexPath.row]["title"]! as! String)
//        if indexPath.row > 7 {
//            cell.imageView?.image = UIImage(named: Constants.Bank_Items[0]["icon"]! as! String)
//        } else {
//            cell.imageView?.image = UIImage(named: Constants.Bank_Items[indexPath.row]["icon"]! as! String)
//            cell.bankNameLabel.text = Constants.Bank_Items[indexPath.row]["title"]! as! String
//        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if #available(iOS 11.0, *) {
            let marginsAndInsets = inset * 2 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
            let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
            let marginsAndInsetsForColumn = inset * 2 + collectionView.safeAreaInsets.top + collectionView.safeAreaInsets.bottom + minimumLineSpacing * CGFloat(cellsPerColumn - 1)
            var itemHeight = ((collectionView.bounds.size.height - marginsAndInsetsForColumn ) / CGFloat(cellsPerColumn)).rounded(.down) - 8
            if itemHeight < 0 {
                itemHeight = 0
            }
            return CGSize(width: itemWidth, height: itemHeight)
        } else {
            // Fallback on earlier versions
        }
        return CGSize(width: 320, height: 90)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
        viewDidLayoutSubviews()
        super.viewWillTransition(to: size, with: coordinator)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if Constants.isSimulator {
            //self.alertShow()
            //TODO: Uncomment for simulator
//            DispatchQueue.main.async {
//                self.fetchAkertView.isHidden = false
//                self.backgroundView.isHidden = false
//            }
            
            DispatchQueue.main.async {
                let user = CoredataManager.getDetail(App_User.self)
                if let vpa = user?.virtualAddress, vpa.count > 0 {
                    self.getBankAccounts()
                    return
                }
                
                if let device = GlobalData.sharedInstance.checkDevice {
                    if let msg = device.devMsgTxt , let mobile = device.smsGateWayNo {
                        self.sendSMS(with: msg, mobile: mobile) } }
                else {
                    self.checkDevice()
                } }
            
            
        } else {
            DispatchQueue.main.async {
               /*
                let user = CoredataManager.getDetail(App_User.self)
                if let vpa = user?.virtualAddress, vpa.count > 0 {
                    self.getBankAccounts()
                    return
                }
                */
                
                if let device = GlobalData.sharedInstance.checkDevice {
                    if let msg = device.devMsgTxt , let mobile = device.smsGateWayNo {
                        self.sendSMS(with: msg, mobile: mobile) } }
                else {
                    self.checkDevice()
                } }
        }
        //
        //guard let cell = collectionView.cellForItem(at: indexPath) else { return }
        //self.passTitle = (Constants.Menu_Items[indexPath.row]["title"]! as! String)
        //self.performSelectedCell(index: indexPath.row)
    
    }
}
extension SelectYourBankViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == searchTableView {
            return 0
        }
        return 40
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
            let reuseIdentifier : String!
        reuseIdentifier = String(format: "HeaderCell")

        let headerView = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: section) as IndexPath) as? HeaderCell
        headerView?.titleLabel.text = "ALL Banks"
            return headerView
        }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Constants.api_off {
            if(searchActive) {
                return filtered.count
            } else {
                return data.count
            }
        }
        
        if(searchActive) {
            return filteredBankList.count
        } else {
            return bankList.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BankDetailCell", for: NSIndexPath(row: 0, section: 0) as IndexPath) as! BankDetailCell
            if(searchActive){
//                cell.titleLabel?.text = filtered[indexPath.row]
//                cell.imgView.image = UIImage.init(named: "bankPlaceholder")
                if Constants.api_off {
                    cell.setup(bankName: filtered[indexPath.row] ?? " - ")
                } else {
                cell.setup(bankName: filteredBankList[indexPath.row].bankName ?? " - ", icon: self.filteredBankList[indexPath.row].bankCode)
                }
            } else {
//                cell.titleLabel?.text = data[indexPath.row];
//                if let img = UIImage.init(named: bankIcons[indexPath.row]) {
//                    cell.imgView.image = img
//                } else {
//                    cell.imgView.image = UIImage.init(named: "bankPlaceholder")
//                }
                if Constants.api_off {
                    cell.setup(bankName: data[indexPath.row], icon: bankIcons[indexPath.row])
                } else {
                //cell.setup(bankName: data[indexPath.row], icon: bankIcons[indexPath.row])
                cell.setup(bankName: self.bankList[indexPath.row].bankName ?? " - ", icon: self.bankList[indexPath.row].bankCode)
                }
            }
                
            return cell
            //return UITableViewCell()
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.present(getPayeeInfoViewController(), animated: true, completion: nil)
        if searchActive {
            return
        }
        if Constants.isSimulator {
            //self.alertShow()
            
            //TODO: Uncomment for simulator
//            DispatchQueue.main.async {
//                self.fetchAkertView.isHidden = false
//                self.backgroundView.isHidden = false
//            }
            
            DispatchQueue.main.async {
                let user = CoredataManager.getDetail(App_User.self)
               /*
                if let vpa = user?.virtualAddress, vpa.count > 0 {
//                    if self.registerApp()!{
//                        self.getBankAccounts()
//                    }else{
//                       print("false")
//                    }
                    self.getBankAccounts()
                    return
                }
                */
                
                if let device = GlobalData.sharedInstance.checkDevice {
                    if let msg = device.devMsgTxt , let mobile = device.smsGateWayNo {
                        self.sendSMS(with: msg, mobile: mobile) } }
                else {
                    self.checkDevice()
                }
            }
        } else {
            DispatchQueue.main.async {
                let user = CoredataManager.getDetail(App_User.self)
                //TODO: - Just to check message status whether it's failed or deliver uncomment after

                
               /*
                if let vpa = user?.virtualAddress, vpa.count > 0 {
//                    if self.registerApp()!{
//                        self.getBankAccounts()
//                    }else{
//                       print("false")
//                    }
                    self.getBankAccounts()
                    return
                }
*/
                
                if let device = GlobalData.sharedInstance.checkDevice {
                    if let msg = device.devMsgTxt , let mobile = device.smsGateWayNo {
                        self.sendSMS(with: msg, mobile: mobile) } }
                else {
                    self.checkDevice()
                }
                
                
            }
            
        }
    }
    
    
    
    func alertShow() {
        let refreshAlert = UIAlertController(title: "Success", message: "Your bank added successfully", preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    print("Handle Ok logic here")
            DispatchQueue.main.async {
                self.fetchAkertView.isHidden = false
                self.backgroundView.isHidden = false
            }
           }))

        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                    print("Handle Cancel Logic here")
                    refreshAlert .dismiss(animated: true, completion: nil)
           }))

            self.present(refreshAlert, animated: true, completion: nil)
    }
}

extension SelectYourBankViewController : MFMessageComposeViewControllerDelegate {
    func sendSMS(with text: String, mobile: String) {
        if MFMessageComposeViewController.canSendText() {
            DispatchQueue.main.async {
                let messageComposeViewController = MFMessageComposeViewController()
                let recipients:[String] = [mobile]
                messageComposeViewController.messageComposeDelegate  = self
                messageComposeViewController.recipients = recipients
                messageComposeViewController.body = text
                self.present(messageComposeViewController, animated: true, completion: nil)
            }
        }
    }
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
                case .cancelled:
                    controller.dismiss(animated: true, completion: {
//                        DispatchQueue.main.async {
//                            self.fetchAkertView.isHidden = false
//                            self.backgroundView.isHidden = false
//                        }
                    })
                case .failed:
                    controller.dismiss(animated: true, completion: {
                        self.presentAlert(title: "Error", msg: "Message Failed")
                    })
                case .sent:

            
                    print("Message was sent")
            controller.dismiss(animated: true, completion: {
                self.showActivityIndicator()
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.5) {
                    self.hideActivityIndicator()
                    
                    //TODO: Just only for message delivery check uncomment line 675 after check
                    
                    self.checkDevice(updated: true)
                    
                    
                    /*
                     APIManager.validate(CheckDevice.self, reqData: self.refId) { (res, err, msg) in
                     print(res)
                     print(err)
                     print(msg)
                     }
                    self.progressbar.setProgress(0.25, animated: true)
                    self.verificationButton.addAnimation()
                     */
                } })
                    
                default:
                    return
                }
    }
}



//myCollectionViewHeight.constant = 100.0
//self.collectionView!.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "CollectionHeaderView")
//        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
//                    flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
//                }




//    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//
//        // Get the view for the first header
//        let indexPath = IndexPath(row: 0, section: section)
//        let headerView = self.collectionView(collectionView, viewForSupplementaryElementOfKind: UICollectionView.elementKindSectionHeader, at: indexPath)
//
//        // Use this view to calculate the optimal size based on the collection view's width
//        return headerView.systemLayoutSizeFitting(CGSize(width: collectionView.frame.width, height: UIView.layoutFittingExpandedSize.height),
//                                                  withHorizontalFittingPriority: .required, // Width is fixed
//                                                  verticalFittingPriority: .fittingSizeLevel) // Height can be as large as needed
//    }
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        0 //1
//    }



//MARK: - API Calling

extension SelectYourBankViewController{
    
    func registration() {
        self.showActivityIndicator()
        APIManager.call(RegisterVPA.self, urlString: "indus/register", reqAction : "registerVPA",category: "IndusInd", pspId: "IBL", extra: "GET_TOKEN") { (res, err) in
            self.hideActivityIndicator()
            GlobalData.sharedInstance.registerVPA = res
            //self.refId = msg ?? "NA"
            print(res)
            print(err)
            print("Done**************")
        }
    }
    
    
    func getListKey() {
        APIManager.call(GetTokenResponse.self, urlString: "indus/apptoken", reqAction : "listKeys",category: "IndusInd", pspId: "IBL", extra: "LIST_KEYS") { (res, err) in
            
            self.hideActivityIndicator()
            
            if let response = res , let list = response.listKeys{
                print(list)
            }
            //self.refId = msg ?? "NA"
            print(res)
            print(err)
            print("Done LIST_KEYS")
        }
    }
  
//    func registerApp() -> Bool?{
//        guard let hmac = APIManager.RegisterAppServices()else{
//            return false
//        }
//        let result = UPI.registerAppHmac(withHmac: hmac)
//        return result
//    }
    
    func checkDevice(updated: Bool = false) {
        self.showActivityIndicator()
        APIManager.call(CheckDevice.self, urlString: "indus/device", reqAction : "checkDevice",category: "IndusInd", pspId: "IBL") { (res, err) in
            self.hideActivityIndicator()
            
          
            
            
            
            GlobalData.sharedInstance.checkDevice = res
            if !updated {
                if let device = GlobalData.sharedInstance.checkDevice {
                    if let msg = device.devMsgTxt , let mobile = device.smsGateWayNo {
                        self.sendSMS(with: msg, mobile: mobile) } }
            } else {
                self.getBankAccounts()
            }
            print(res)
            print(err)
            print("Done**************")
        }
    }
    
    func getBankAccounts() {
        self.showActivityIndicator()
        APIManager.call(BankAccountsList.self, urlString: "indus/getbank", reqAction : "getBankAccountList",category: "IndusInd", pspId: "IBL") { (res, err) in
            self.hideActivityIndicator()
            //MARK:------------------------------
            
            
            if let result : BankAccountsList = res , (result.userInfo != nil) {
//                GlobalData.sharedInstance.bankAccounts = res
//                CoredataManager.updateDetail(App_User.self, key: "virtualAddress", value:  result.userInfo!.virtualAddress!)
                
                //TODO: Uncomment Default Account and Accont List Management
                /*
                if let accountList = CoredataManager.getDetails(Account_List.self){
                    if let bankAccountList = result.accountList?.compactMap({$0.maskedAccountNumber}){
                        for i in 0..<bankAccountList.count{
                            if accountList.contains(where: {$0.maskedAccountNumber == bankAccountList[i]}){
                                result.accountList?.remove(at: i)
                                GlobalData.sharedInstance.bankAccounts = result
                               
                            }else{
                                GlobalData.sharedInstance.bankAccounts = result
                            }
                        }
                    }
                }else{
                    CoredataManager.cleanEntity(App_UserBankAccount_Details.self)
                    GlobalData.sharedInstance.bankAccounts = result
                    CoredataManager.saveUserBankDetails(saveBankDetails: result)
                }
                
                DispatchQueue.main.async {
                    if GlobalData.sharedInstance.bankAccounts?.accountList?.count == 0{
                        self.backgroundView.isHidden = false
                    }else{
                        let vc = getSelectBankAccountViewController() as! SelectBankAccountViewController
                        vc.userBankAccounts = GlobalData.sharedInstance.bankAccounts
                        self.present(vc, animated: true, completion: nil)
                    }
                }
                
                */
                
//                if accountList.contains(where: {$0.maskedAccountNumber ==  bankAccountList?.compactMap({$0})}){
//
//                }
                
                
                
                    CoredataManager.cleanEntity(App_UserBankAccount_Details.self)
                    GlobalData.sharedInstance.bankAccounts = res
                    
                    CoredataManager.saveUserBankDetails(saveBankDetails: res!)

                    
                    DispatchQueue.main.async {
                        let challengetext = UPI.getChallenge()
                        
                        getToken(status: true)
                        
                        
                        
                        if registerApp()!{
                            let vc = getSelectBankAccountViewController() as! SelectBankAccountViewController
                            vc.userBankAccounts = res
                            self.present(vc, animated: true, completion: nil)
                        }else{
                            print("error")
                        }
                    }
            }
            
            if err != nil{
                DispatchQueue.main.async {
                    let challengetext = UPI.getChallenge()
                    
                    
                    if registerApp()!{
                        let vc = getSelectBankAccountViewController() as! SelectBankAccountViewController
                        vc.userBankAccounts = res
                        self.present(vc, animated: true, completion: nil)
                    }else{
                        
                    }
                }
            }
            
            //MARK:----------------------------------
//            GlobalData.sharedInstance.bankAccounts = res
//            print(res)
//            print(err)
//            print("Done**************")
//            DispatchQueue.main.async {
//                let vc = getSelectBankAccountViewController() as! SelectBankAccountViewController
//                vc.userBankAccounts = res
//                self.present(vc, animated: true, completion: nil)
//            }
        }
    }
    
    
    func getBankInfo() {
        self.showActivityIndicator()
        APIManager.get(BankList.self,  mobile: userDetails?.mobile ?? "7007587639", reqAction: "bankList", geoLong: self.geo_long ?? "77.1873", geoLat: self.geo_lat ?? "28.4817", location: currentLocation ?? "Delhi", pspBank: "IndusInd", pspId: "IBL", isCheckDevice : true, saveBank : true) { (res, err, msg) in
            self.hideActivityIndicator()
            
            print(res)
            print(err)
            
            
            //TODO:- DELETE WHEN BANKLIST API WORKING FINE
//            if err != nil{
//                if let localBank = CoredataManager.getDetails(Master_BankList.self){
//                    self.bankList = localBank
//                }else{
//                    let bankData =  APIManager.loadJson(filename: "BankList")
//                }
//            }
                DispatchQueue.main.async {
                    if let banks = CoredataManager.getALLDetails(Master_BankList.self, sortBytTime: true, key: "bankName", ascending: true, sortCaseSensitive: true) {
                        DispatchQueue.main.async {
                            for name in Constants.Bank_Items{
                                print(name["title"])
                                //                            self.popularBankList = banks.filter{$0.bankName, name["title"]}
                            }
                            //
                            
                            self.bankList = banks
                            self.tableView.reloadData()
                        }
                    }
                }
            
        }
    }
    
    
    
}



/*
extension UIViewController{
    func getToken(status : Bool = false) {
        self.showActivityIndicator()
        APIManager.call(GetTokenResponse.self, urlString: "indus/apptoken", reqAction : "getToken",category: "IndusInd", pspId: "IBL", extra: "GET_TOKEN") { (res, err) in
            self.hideActivityIndicator()

            if err == nil{
                if !status{
                    if let response = res , let list = response.listKeys{
                        print(response)
                        GlobalData.sharedInstance.setToken = res
                        CoredataManager.saveNpciTokenListKey(getNpciData: response)
                    }
                }else{
                    let localNpciData =  APIManager.loadAllJson(GetTokenResponse.self, filename: "npci")
                    print(localNpciData)
                    GlobalData.sharedInstance.setToken = res
                    CoredataManager.saveNpciTokenListKey(getNpciData: localNpciData!)
                }
            }else{
               if let npciData = CoredataManager.getDetail(NPCI_Data.self){
                   if let listKey = CoredataManager.getDetail(List_Key.self){
                       print(npciData , listKey)

                   }
               }else{
                   let localNpciData =  APIManager.loadAllJson(GetTokenResponse.self, filename: "npci")
                   CoredataManager.saveNpciTokenListKey(getNpciData: localNpciData!)
               }
            }
        }
    }
}
*/
