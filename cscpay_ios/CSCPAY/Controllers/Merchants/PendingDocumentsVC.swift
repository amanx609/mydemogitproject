//
//  PendingDocumentsVC.swift
//  CSCPAY
//
//  Created by Aman Pandey on 06/12/21.
//

import UIKit
import CoreMedia

class PendingDocumentsVC: UIViewController , UploadedDocumentFromUploadDocVC , PendingDocument {
    
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var proceedforVerificationButton: UIButton!
    @IBOutlet weak var nameOfDocLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var merchantRequestModel : CommonModel?
    var merchantRegistrationResponse : MerchantRegistrationModel?
    var docTypeUpload : CompanyDocModel?
    
    
    
    var optionsArray : [MerchantOptions] = []
    var uploadDoc : [Company]?
    
    
    var uploadDocumentArray : [Int] = []
    var activeStatus = Int()
     
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if  let req : MerchantRegistrationModel = merchantRegistrationResponse {
            merchantRegistrationResponse = req
        }
        docTypeUpload = APIManager.loadAllJson(CompanyDocModel.self, filename: "CompanyDoc")
        let doc = docTypeUpload?.companies?.filter({$0.type == merchantRegistrationResponse?.body.bizCategory})
        uploadDoc = doc
        updaateAsPerActiveStatus()
    }
    
    
    
    @IBAction func backButtonTapped(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func proceedToVerifyDocuments(_ sender : UIButton){
        if Constants.byPass{
            self.present(getMerchantSuccessViewController(), animated: true, completion: nil)
        }else{
            if uploadDocumentArray.count == uploadDoc![0].docs!.count{
                let vc = getMerchantSuccessViewController() as! MerchantSucessVC
                vc.merchantRegResponse = merchantRegistrationResponse
                vc.msgString = "Documents suceessfully uploaded. Our verification team contact you soon , please wait untill then."
                self.present(vc, animated: true, completion: nil)
            }else{
                showAlert(msg: "Please upload all the documents")
            }
        }
    }
    
    
    
}

extension PendingDocumentsVC : UITableViewDelegate , UITableViewDataSource{
    func uploadedDocument(index: Int, isSelect: Bool) {
        uploadDocumentArray.append(index)
        updaateAsPerActiveStatus()
    }

    func showAlert(msg: String) {
        DispatchQueue.main.async {
            self.responseLabel.text = msg
        }
    }
    
    func openViewControllerOnButtonTap(index : IndexPath){
        print(index)
        if index.row == 0 {
            if uploadDoc![0].docs![0].upload == false{
                goUploadDocumentVC(index: index)
            }
        }
        if index.row > 0  {
            if uploadDoc![0].docs![index.row - 1].upload == true{
                goUploadDocumentVC(index: index)
            }
        }
    }
    
    
    func goUploadDocumentVC(index: IndexPath){
        let vc = getMerchantUploadDocsViewController() as! UploadDocsVC
        vc.merchantRequestRespose = self.merchantRequestModel
        vc.merchantRegistrationResponse = self.merchantRegistrationResponse
        vc.documentsRequiredArray = uploadDoc![0].docs![index.row].doc!
        vc.docReqArr = uploadDoc![0].docs![index.row]
        vc.selectedDocToUpload = index.row
        vc.uploadedDocumentCount = index.row + 1
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }

    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let array = uploadDoc else{ return 0}
        return array[section].docs!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PendingDocumentCell", for: indexPath) as? PendingDocumentCell else {return UITableViewCell()}
        cell.delegate = self
        cell.descriptionLabel.text = "Make sure you capture a clear front view"
        cell.documentNameLabel.text = uploadDoc![0].docs![indexPath.row].docType?.rawValue
        cell.docNameLabel.text = uploadDoc![0].docs![indexPath.row].doc?.joined(separator: "/")
        cell.cameraImage.image = uploadDoc![0].docs![indexPath.row].upload == true ? UIImage(named: "submit_radio") : UIImage(named: "cam")
        cell.isUserInteractionEnabled = uploadDoc![0].docs![indexPath.row].upload == true ? false : true
        return cell
    }
}

extension PendingDocumentsVC{
    func activateMerchantVPA(request: CommonModel?){
        APIManager.getMerchant(MerchantResponse.self, reqAction: "activateMerchant", urlString: "merchant/activate", requestModel: request) { res, err, text in
            print(res)
            print(err)
            print(text)
        }
    }
    
    func updaateAsPerActiveStatus() {
       activeStatus = UD.SharedManager.getValues(key: "ActiveStatus", type: .int) as! Int
       
//       if Constants.printResponse {
//           print("Active Status: \(activeStatus)")
//           print("Docs Count: \(uploadDoc?[0].docs?.count ?? 0)")
//       }
       
       let alreadySendDocsCount = activeStatus - 10
       
       for index in 0 ..< alreadySendDocsCount {
           uploadDoc?[0].docs?[index].upload = true
       }
       
       tableView.reloadData()
   }
}
