//
//  MerchantOnBoardingVC.swift
//  CSCPAY
//
//  Created by Aman Pandey on 26/11/21.
//

import UIKit

class MerchantOnBoardingVC: UIViewController {

    //@IBOutlet weak var udyogIdTextField: UITextField!
    
    @IBOutlet weak var udyogIdTextField: IuFloatingTextFiledPlaceHolder!
    @IBOutlet weak var msmeTextFieldView: UIView!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var otpTextField: IuFloatingTextFiledPlaceHolder!
    @IBOutlet weak var otpLabel: UILabel!
    @IBOutlet weak var merchantOnboardingLabel: UILabel!
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var sendOtpButtonHeight: NSLayoutConstraint!
    var typeOfMerchant : String!
    var otpText : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        otpLabel.text = "Please enter OTP send to your mobile number 70********21"
        merchantOnboardingLabel.text = "Please enter \(typeOfMerchant ?? "NA") to proceed"
    }
    

    @IBAction func sendOtp(_ sender: UIButton) {
        sendOtpButtonHeight.constant = 0
        otpView.isHidden = false
    }
    
    @IBAction func verifyOtp(_ sender: UIButton) {
        self.view.isUserInteractionEnabled = true
        self.present(getSummaryViewController(), animated: true, completion: nil)
    }
    
    
    @IBAction func backButtonTapped(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
