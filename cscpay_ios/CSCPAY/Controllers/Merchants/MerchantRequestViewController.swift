//
//  MerchantReviewViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 19/03/21.
//
import UIKit

class MerchantRequestViewController: UIViewController {
    
    @IBOutlet weak var makeRequestBtn: UIButton!
    @IBOutlet weak var historyBtn: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var isFromMerchant: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        DispatchQueue.main.async {
            let vc = getContactsViewController() as! ContactsViewController
            //vc.isFromMerchant = self.isFromMerchant
            self.embed(vc, inView: self.containerView)
            self.makeRequestBtn.tag = 1
        }
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func makeRequestButtonClicked(_ sender: Any) {
        if self.makeRequestBtn.tag == 0 {
            self.makeRequestBtn.tag = 1
            self.historyBtn.tag = 0
        } else {
            return
        }
        changeButtonsState(btn : makeRequestBtn, otherBtns : [historyBtn])
        embed(getContactsViewController(), inView: self.containerView)
    }
    @IBAction func incomingRequestButtonTapped(_ sender: Any) {
        if self.historyBtn.tag == 0 {
            self.historyBtn.tag = 1
            self.makeRequestBtn.tag = 0
        } else {
            return
        }
        changeButtonsState(btn : historyBtn, otherBtns : [makeRequestBtn])
        //embed(getRequestHistoryViewController(), inView: self.containerView)
    }
}
