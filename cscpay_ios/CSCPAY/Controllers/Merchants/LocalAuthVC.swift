//
//  LocalAuthVC.swift
//  CSCPAY
//
//  Created by Aman Pandey on 16/12/21.
//

import UIKit

class LocalAuthVC: UIViewController {

    @IBOutlet weak var localAuthButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func localAuthButton(_ sender: UIButton) {
        if Constants.authenticationStatus != "Done"{
            self.authenticationWithTouchID(completion: nil)
        }
    }
    
}
