//
//  PanCardAndBankViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 24/02/21.
//

import UIKit
import Vision
import VisionKit


class PanCardAndBankViewController: UIViewController , UITextFieldDelegate , VNDocumentCameraViewControllerDelegate{

    @IBOutlet weak var pinCodeTextField: UITextField!
    @IBOutlet weak var gstinField: UITextField!
    @IBOutlet weak var bankAccountField: UITextField!
    @IBOutlet weak var ifscField: UITextField!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var responseLabel : UILabel!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var legalStateTextField: UITextField!
    @IBOutlet weak var legalDistricttextField: UITextField!
    
    @IBOutlet weak var legalStateButton: UIButton!
    @IBOutlet weak var legalDistrictButton: UIButton!
    @IBOutlet weak var verifiedButton : UIButton!
    @IBOutlet weak var ifscLabel: UILabel!
    
    
    
    
    
    var merchantRequestResponse : CommonModel?
    private let statePicker = StateCity()
    private let cityPicker = CityPicker()
    var selectedState : String?
    var allDistrict = [String]()
    var index : Int?
    var stateCode : String?
    var disCode : String?
    var verifiedIfsc : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pinCodeTextField.delegate = self
        gstinField.delegate = self
        addressTextField.delegate = self
        hideKeyboardWhenTappedAround()
        setupUI()
    }
    func setupUI() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 2.0) {
                self.progressView.setProgress(1.0, animated: true)
            }
        }
        
        self.legalStateTextField.inputView = self.statePicker
        self.statePicker.onStateSelected = {(state : String , disArr : [String]) in
            self.selectedState = state
            self.legalStateTextField.text = state
            self.allDistrict = disArr
            self.legalDistricttextField.inputView = self.cityPicker
            self.cityPicker.allDistrict = self.allDistrict
            self.cityPicker.onCitySelect = {(state : String) in
                self.legalDistricttextField.text = state
            }
        }
    }
    
    

    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func nextButtonTapped(_ sender: Any) {
        if Constants.byPass{
            self.present(getSummaryViewController(), animated: true, completion: nil)
        }else{
            checkValues()
        }
        
    }
    
    @IBAction func findIfscButtonTapped(_ sender: Any) {
        guard let text = ifscField.text else {return }
        if !verifiedIfsc{
            verifyIFSCCodeSerivces(ifsc: text)
            
        }
    }
    
    
        
    
    @IBAction func legalStateButtonTapped(_ sender: UIButton) {
        legalStateTextField.delegate = self
        let vc = getCommonFieldsViewController() as! CommonFeildVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.type = .state
        vc.typeBusiness = {stateCode , stateName , index in
            self.legalStateTextField.text = stateName
            self.index = index
            self.stateCode = stateCode
            self.legalDistricttextField.text = ""
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func legalDistrictButtonTapped(_ sender: UIButton) {
        guard index != nil else {
            showAlert(msg: "Please select state first")
            return
        }
        legalDistricttextField.delegate = self
        let vc = getCommonFieldsViewController() as! CommonFeildVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.type = .district
        vc.selectedStateIndex = index
        vc.typeBusiness = {disCode , disName , index in
            self.legalDistricttextField.text = disName
            self.disCode = disCode
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    
    func showAlert(msg: String) {
        DispatchQueue.main.async {
            self.responseLabel.text = msg
        }
    }
    
    
    func animateTextField(textField: UITextField, up: Bool)
    {
        let movementDistance:CGFloat = -160
        let movementDuration: Double = 0.3
        
        var movement:CGFloat = 0
        if up{
            movement = movementDistance
        }
        else{
            movement = -movementDistance
        }
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.animateTextField(textField: textField, up:true)
    }

        // Finish Editing The Text Field
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.animateTextField(textField: textField, up:false)
    }

        // Hide the keyboard when the return key pressed
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    
    @IBAction func uplaodPanCardButtonTapped(_ sender: UIButton){
        
    }
    
    
    
    
    
    func checkValues(){
        
        
//        guard let panCardDetails = self.pancardNumberField.text, panCardDetails.count > 0 else {
//            return
//        }
        
//        if !panCardDetails.validatePANCardNumber(){
//            showAlert(msg: Constants.alert_enter_valid_pancard)
//            return
//        }
        
//        guard let gstDetails = self.gstinField.text, gstDetails.count > 0 else {
//                showAlert(msg: "Enter GSTIN Number")
//            return
//        }
//
//
//        if !gstDetails.validateGSTnumber(){
//            showAlert(msg: "Enter correct GST Number")
//            return
//        }
        
        guard let ifsc = self.ifscField.text, ifsc.count > 0 else {
            return
        }
        
        
        
        
        
        guard let bankDetails = self.bankAccountField.text, bankDetails.count > 0 else {
            showAlert(msg: "Bank Account can't be empty")
            return
        }
        
        var GST = "NA"
        
        if let gstDetails = self.gstinField.text , gstDetails.count > 0 {
            if gstDetails.validateGSTnumber(){
                GST = gstDetails
            }else{
                GST = "NA"
            }
        }
        
        guard let address = self.addressTextField.text, address.count > 0 else {
            showAlert(msg: "Please enter address")
            return
        }
        
        guard let state = self.legalStateTextField.text, state.count > 0 else {
            showAlert(msg: "Please enter state")
            return
        }
        
        guard let district = self.legalDistricttextField.text, district.count > 0 else {
            showAlert(msg: "Please enter district")
            return
        }
        
        guard let pincode = self.pinCodeTextField.text, district.count > 0 else {
            showAlert(msg: "Please enter Pincode")
            return
        }
        
        
        
        
        
        
        
//        guard let confirmBankAccount = self.confirmBankAccountField.text, confirmBankAccount.count > 0 else {
//            return
//        }
//        
//        guard bankDetails == confirmBankAccount else{
//            showAlert(msg: "Account donot match ")
//            return
//        }
        
        
        
        
     /*
        merchantRequestResponse?.bizCategory == "SOC" ? panCardDetails : ""
        
        
        if merchantRequestResponse?.bizCategory == "SOC"{
            self.merchantRequestResponse?.registerId = panCardDetails
        }
        self.merchantRequestResponse?.panNo = panCardDetails
      */
        
        if let fcmToken = UD.SharedManager.getValues(key: "fcmDeviceId", type: .string) as? String{
            self.merchantRequestResponse?.fcmDeviceId = fcmToken
        }
        self.merchantRequestResponse?.address = address
        self.merchantRequestResponse?.city = Constants.location
        self.merchantRequestResponse?.stateCode = stateCode
        self.merchantRequestResponse?.stateName = state
        self.merchantRequestResponse?.districtCode = disCode
        self.merchantRequestResponse?.districtName = district
        self.merchantRequestResponse?.ifscCode = ifsc
        self.merchantRequestResponse?.accountNo = bankDetails
        self.merchantRequestResponse?.gstin = GST
        self.merchantRequestResponse?.pinCode = pincode
        
        

        /*
        self.merchantRequestResponse?.docCount = "2"
        self.merchantRequestResponse?.docList = "9999894870"
        self.merchantRequestResponse?.cscId = "943111134902"
        */

        //          "document": [
        //            {
        //              "type": "kyc",
        //              "mime": "application/pdf",
        //              "size": 12343332,
        //              "data": "base64string"
        //            },
        //            {
        //              "type": "cheque",
        //              "mime": "image/jpeg",
        //              "size": 20032,
        //              "data": "base64string"
        //            }
        //          ]
        //        }
        
        
        let vc = getSummaryViewController() as! SummaryViewController
        vc.merchantRequestResponse = self.merchantRequestResponse
        
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func verifyIFSCCodeSerivces(ifsc : String){
        APIManager.call(BankIfscResponse.self, urlString: "start/master", reqAction: "ifsc", category: ifsc , mobCrypt: true) { res, err in
            if res != nil{
                DispatchQueue.main.async {
                    if let result = res?.bankBody{
                        self.ifscLabel.text = "\(result.bankName ?? "NA")" + ", \(result.branchName ?? "NA")" + ", \(result.branchAddress ?? "NA")"
                        self.verifiedButton.setTitle("Verified", for: .normal)
                        self.verifiedIfsc = true
                    }
                }
                
            }
        }
    }
}







//extension PanCardAndBankViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
//    func showAlert() {
//        let alert = UIAlertController(title: "", message: Constants.alert_choose_image, preferredStyle: .actionSheet)
////        alert.addAction(UIAlertAction(title: Constants.alert_open_gallery, style: .default, handler: {(action: UIAlertAction) in
////            self.getImage(fromSourceType: .photoLibrary)
////        }))
//        alert.addAction(UIAlertAction(title: Constants.alert_capture_image2, style: .default, handler: {(action: UIAlertAction) in
//            self.getImage(fromSourceType: .camera)
//        }))
//        alert.addAction(UIAlertAction(title: Constants.alert_remove_image, style: .default, handler: { (action: UIAlertAction) in
//            self.imageView.image = UIImage()
//            self.imageView.isHidden = true
//            //UD.SharedManager.removeValue(key: Constants.User_Image)
//        }))
//        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
//        self.present(alert, animated: true, completion: nil)
//    }
//
//    //get image from source type
//    func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
//        //Check is source type available
////        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
////
////            let imagePickerController = UIImagePickerController()
////            imagePickerController.delegate = self
////            imagePickerController.sourceType = sourceType
////            //imagePickerController.cameraViewTransform = imagePickerController.cameraViewTransform.scaledBy(x: -1, y: 1)
////            if sourceType == .camera {
////                imagePickerController.cameraDevice = UIImagePickerController.CameraDevice.front }
////            imagePickerController.allowsEditing = true
////            self.present(imagePickerController, animated: true, completion: nil)
////        }
//
//
//
//        if UIImagePickerController.isSourceTypeAvailable(.camera){
//            let scannerViewController = VNDocumentCameraViewController()
//            scannerViewController.delegate = self
//            present(scannerViewController, animated: true)
//        }else{
//            let alertController = UIAlertController(title: "Camera", message: "No camera avilable", preferredStyle: .alert)
//            let okAction = UIAlertAction(title: "OK", style: .default) { (_) in }
//            alertController.addAction(okAction)
//            present(alertController, animated: true, completion: nil)
//        }
//    }
//
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        let image = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.editedImage.rawValue)] as! UIImage
//        let afterResizeImg = image.resize(to: imageView.frame.size)
//        self.imageView.image = afterResizeImg //.
//        //self.imageView.cornerRadius = imageView.frame.size.height / 2
//        //UD.SharedManager.saveImageInUserDefault(img:afterResizeImg, key:Constants.User_Image)
//        self.imageView.isHidden = false
//        self.dismiss(animated: true, completion: nil)
//    }
//}
//27AAPFU0939F1ZV
