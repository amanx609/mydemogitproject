//
//  PANCheckVC.swift
//  CSCPAY
//
//  Created by Aman Pandey on 13/12/21.
//

import UIKit

class PANCheckVC: UIViewController {

    @IBOutlet weak var legalBusinessName: UITextField!
    @IBOutlet weak var panCardTextField: UITextField!
    @IBOutlet weak var bacnkButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var businessTypeTextField: UITextField!
    @IBOutlet weak var businessTypeButton: UIButton!
    @IBOutlet weak var panCardView: UIView!
    @IBOutlet weak var panCardViewLabel: UILabel!
    
    
    
    
    
    
    
    var merchantRequestResponse = CommonModel()
    var mobileNumber : String?
    var bizCode : String?
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegates()
    }
    
    func appDelegates(){
        self.businessTypeTextField.delegate = self
        self.legalBusinessName.delegate = self
        self.panCardTextField.delegate = self
    }
    
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func continueButtonTapped(_ sender: UIButton) {
        if Constants.byPass{
            self.present(getGeneralDetailsViewController(), animated: true, completion: nil)
        }else{
            self.checkvalues()
        }
    }
    
    
    @IBAction func businesstypeButtonTapped(_ sender: UIButton) {
        let vc = getCommonFieldsViewController() as! CommonFeildVC
        vc.type = .businessType
        vc.typeBusiness = {codeBusiness , textBusiness , index in
            self.businessTypeTextField.text = textBusiness
            self.bizCode = codeBusiness
            switch codeBusiness{
            case "PRO":
                self.panCardViewLabel.text = "Enter Indivisual / Company PAN No."
            default:
                self.panCardViewLabel.text = "Enter your Company PAN No."
            }
            self.panCardView.isHidden = false
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    
    
    func showAlert(msg: String) {
        DispatchQueue.main.async {
            self.responseLabel.text = msg
        }
    }
    
    func checkvalues(){
        
        guard let legalName = self.legalBusinessName.text , legalName.count > 0 else{
            showAlert(msg: "Enter your legal business name")
            return
        }
        
        guard let businessType = self.businessTypeTextField.text , businessType.count > 0 else{
            showAlert(msg: "Select your business type")
            return
        }
        
        guard let panCardDetails = self.panCardTextField.text, panCardDetails.count > 0 else {
            showAlert(msg: Constants.alert_enter_valid_pancard)
            return
        }
                
        if !panCardDetails.validatePANCardNumber(){
            showAlert(msg: Constants.alert_enter_valid_pancard)
            return
        }
        
        
        if let user =  CoredataManager.getDetail(App_User.self) {
            mobileNumber = user.mobile
        }
        
        merchantRequestResponse.panNo = panCardDetails
        merchantRequestResponse.bizName = businessType
        merchantRequestResponse.mobileNo = mobileNumber
        merchantRequestResponse.legalName = legalName
        merchantRequestResponse.bizCategory = bizCode
        
        
        checkMerchantService()
        
    }
    
    
    func checkMerchantService(){
        APIManager.getMerchant(MerchantRegistrationModel.self, reqAction: "checkMerchant", urlString: "merchant/check", requestModel: merchantRequestResponse , merchantRegresponse: nil) { res, err, text in
            print(res)
            if let err = err as? RuntimeError {
                if err.code == "701"{
                    DispatchQueue.main.async {
                        let vc = getGeneralDetailsViewController() as! GeneralDetailsViewController
                        vc.panCardNumber = self.merchantRequestResponse.panNo ?? "NA"
                        vc.mobileNumber = self.merchantRequestResponse.mobileNo ?? "NA"
                        vc.legalName = self.merchantRequestResponse.legalName ?? "NA"
                        vc.bizCode = self.merchantRequestResponse.bizCategory
                        vc.businessName = self.merchantRequestResponse.bizName
                        vc.merchantRequestResponse = self.merchantRequestResponse
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }else if res != nil {
                self.removeUserDefaultValues()
                if let result : MerchantRegistrationModel = res {
                    UD.SharedManager.saveToUserDefaults(key: "AlreadyRegWithOtp", value: true)
                    UD.SharedManager.saveToUserDefaults(key: "PANNo.", value: result.body.panNo)
                    UD.SharedManager.saveToUserDefaults(key: "LegalName", value: result.body.legalName)
                    UD.SharedManager.saveToUserDefaults(key: "MobileNo.", value: result.body.mobileNo)
                    let actStatus = Int(result.body.activeStatus ?? "") ?? 0
                    UD.SharedManager.saveToUserDefaults(key: "ActiveStatus", value: actStatus)
                    UD.SharedManager.saveToUserDefaults(key: "bizId", value: result.body.bizId)
                    UD.SharedManager.saveToUserDefaults(key: "seqKey", value: result.body.seqKey)
                }
                
                DispatchQueue.main.async {
                    
                    CoredataManager.updateDetail(App_User.self, key: "aesKey", value: res?.cred?.aesKey ?? "")
                    CoredataManager.updateDetail(App_User.self, key: "deviceId", value: res?.head.deviceID ?? "")
                    CoredataManager.updateDetail(App_User.self, key: "hashToken", value: res?.cred?.hashToken ?? "")
                    CoredataManager.updateDetail(App_User.self, key: "hmac", value: res?.cred?.hmac ?? "")
                    
                    if res?.body.activeStatus == "10"{
                        if let registerResponse : MerchantRegistrationModel = res{
                            let vc = getMerchantPendingViewController() as! PendingDocumentsVC
                            vc.merchantRegistrationResponse = registerResponse
                            self.present(vc, animated: true, completion: nil)
                        }
                    }else if res?.body.activeStatus == "20"{
                        let vc = getMerchantSuccessViewController() as! MerchantSucessVC
                        vc.merchantRegResponse = res
                        vc.msgString = "Documents suceessfully uploaded. Our verification team contact you soon , please wait untill then."
                        self.present(vc, animated: true, completion: nil)
                    }else{
                        let vc = getMerchantHomeViewController() as! MerchantHomeViewController
                        vc.merchantRegResponse = res
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func removeUserDefaultValues(){
        UD.SharedManager.removeValue(key: "PANNo.")
        UD.SharedManager.removeValue(key: "LegalName")
        UD.SharedManager.removeValue(key: "MobileNo.")
        UD.SharedManager.removeValue(key: "ActiveStatus")
        UD.SharedManager.removeValue(key: "bizId")
        UD.SharedManager.removeValue(key: "seqKey")
    }
    
    
    func setValuesInUserDefault(){
        
    }
}

extension PANCheckVC : UITextFieldDelegate{
    
        
        func animateTextField(textField: UITextField, up: Bool)
        {
            let movementDistance:CGFloat = -160
            let movementDuration: Double = 0.3
            
            var movement:CGFloat = 0
            if up
            {
                movement = movementDistance
            }
            else
            {
                movement = -movementDistance
            }
            UIView.beginAnimations("animateTextField", context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            UIView.setAnimationDuration(movementDuration)
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
            UIView.commitAnimations()
        }
        public func textFieldDidBeginEditing(_ textField: UITextField) {
            //moveTextField(textField, moveDistance: -250, up: true)
            self.animateTextField(textField: textField, up:true)
        }
        
        // Finish Editing The Text Field
        public func textFieldDidEndEditing(_ textField: UITextField) {
            self.animateTextField(textField: textField, up:false)
            // moveTextField(textField, moveDistance: -250, up: false)
        }
        
        // Hide the keyboard when the return key pressed
        public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
    }
