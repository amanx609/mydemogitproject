//
//  GenerateQRViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 18/06/21.
//

import UIKit

class GenerateMerchantQRViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblQrDescription: UILabel!
    @IBOutlet weak var borderImageView: UIImageView!
    @IBOutlet weak var qrImageView: UIImageView!
    //@IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var lblUPI: UIButton!
    
    
    var brightness : CGFloat = 0.5
    let inset: CGFloat = 10
    let minimumLineSpacing: CGFloat = 10
    let minimumInteritemSpacing: CGFloat = 10
    let cellsPerRow = 5
    let cellsPerColumn = 1
    var recents = [Transactions]()
    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular, text: Language.getText(with: LanguageConstants.lbl_Qr_title))
        lblSubtitle.setup(fontSize : 18, text: Language.getText(with: LanguageConstants.lbl_Qr_Subtitle))
        lblQrDescription.setup(fontSize: 14, fontType: .regular, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_Qr_Description))
        lblUPI.setup(fontSize: 16, fontType: .regular, textColor: .titleColor) //UPI ID:
        
        if let recent = CoredataManager.getSpecificRecord(Transactions.self, key: "type", value: "R") {
            self.recents = recent
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupUI()
        /*
        let yourAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont (name: "Nunito-Semibold", size: 15) ?? UIFont.systemFont(ofSize: 15),
              .foregroundColor: hexStringToUIColor(hex: "1160DA"),
              .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "SHARE",
                                                             attributes: yourAttributes)
        shareButton.setAttributedTitle(attributeString, for: .normal)
        */
        brightness = UIScreen.main.brightness
        
        //let swiftLeeOrangeColor = UIColor(red:0.93, green:0.31, blue:0.23, alpha:1.00)
        let swiftLeeLogo = UIImage(named: "logo_solo")!

        let qrURLImage = URL(string: "Gowtham_9744484730_Gautam@cscpay")?.qrImage(using: .customBlack, logo: swiftLeeLogo)
        
        self.qrImageView.image = UIImage.init(ciImage: qrURLImage!)
            UIScreen.main.brightness = 1.0
        
        
//        if let image = generateQRCode(from: "This QR is only for Testing purpose!") {
//        self.qrImageView.image = image
//            UIScreen.main.brightness = 1.0
//        }
    }
    
    func saveImage() {

        guard let selectedImage = qrImageView.image else {
            return
        }

        UIImageWriteToSavedPhotosAlbum(selectedImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }

    //MARK: - Save Image callback

    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {

        if let error = error {

            print(error.localizedDescription)

        } else {

            print("Success")
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        UIScreen.main.brightness = brightness
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        shareQRCode()
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            //filter.setValue("H", forKey: "inputCorrectionLevel")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
    private func shareQRCode() {
        if let image = qrImageView.image {
            let vc = UIActivityViewController(activityItems: [image], applicationActivities: [])
            present(vc, animated: true)
        }
    }
}
