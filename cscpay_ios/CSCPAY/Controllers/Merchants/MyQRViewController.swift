//
//  MyQRViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 17/03/21.
//

import UIKit

class MyQRViewController: UIViewController {
    
    var brightness : CGFloat = 0.5
    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var exportQRView: UIView!
    @IBOutlet weak var qrImgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        brightness = UIScreen.main.brightness
        
        //let swiftLeeOrangeColor = UIColor(red:0.93, green:0.31, blue:0.23, alpha:1.00)
        let swiftLeeLogo = UIImage(named: "Icon-App-20x20")!

        let qrURLImage = URL(string: "This_is_CSC_PAY_Testing_QR")?.qrImage(using: .customBlack, logo: swiftLeeLogo)
        
        self.qrImageView.image = UIImage.init(ciImage: qrURLImage!)
            UIScreen.main.brightness = 1.0
        self.qrImgView.image = self.qrImageView.image
    }
    override func viewWillDisappear(_ animated: Bool) {
        UIScreen.main.brightness = brightness
    }
    @IBAction func shareButtonTapped(_ sender: Any) {
//        let view2 = self.exportQRView
//        view2?.isHidden = false
        //let img = qrImageView.image // view2?.takeScreenshot()
        self.exportQRView.isHidden = false
        let img = self.exportQRView.takeScreenshot()
        self.exportQRView.isHidden = true
        shareQRCode(img: img)
    }
    
    @IBAction func downloadBtnTapped(_ sender: Any) {
        
//        notImplementedYetAlert(base: self)
    }
    @IBAction func proceedHomeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
    private func shareQRCode(img : UIImage?) {
        if let image = img {
            let vc = UIActivityViewController(activityItems: [image], applicationActivities: [])
            present(vc, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
