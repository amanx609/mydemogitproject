//
//  GeneralDetailsViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 24/02/21.
//

import UIKit

class GeneralDetailsViewController: UIViewController , UITextFieldDelegate {
    
    
    
    @IBOutlet weak var legalBusinessName : UITextField!
    @IBOutlet weak var name : UITextField!
    @IBOutlet weak var dateOfRegistration : UITextField!
    @IBOutlet weak var businessCategoryTextField : UITextField!
    @IBOutlet weak var natureOfBusinessTextField : UITextField!
    @IBOutlet weak var panTextField : UITextField!
    @IBOutlet weak var registerIdTextField : UITextField!
    @IBOutlet weak var progressView1: UIProgressView!
//    @IBOutlet weak var legalStateTextField : UITextField!
//    @IBOutlet weak var stateButton : UIButton!
//    @IBOutlet weak var districtButton : UIButton!
    @IBOutlet weak var dateOfRegistrationButton : UIButton!
    @IBOutlet weak var businessCategoryButton : UIButton!
    @IBOutlet weak var natureOfBUsinessButton : UIButton!
    @IBOutlet weak var nextButton : UIButton!
    @IBOutlet weak var registeredID: UITextField!
    @IBOutlet weak var registeredIDView: UIView!
    @IBOutlet weak var underRegisterView: UIView!
    
    
    
    
    
    @IBOutlet weak var responseLabel: UILabel!
    
    var panCardNumber : String?
    var mobileNumber : String?
    var legalName : String?
    var businessName : String?
    var bizCode : String?

    
    private let statePicker = StateCity()
    private let cityPicker = CityPicker()
    var selectedState : String?
    var allDistrict = [String]()
    var merchantRequestResponse : CommonModel?
    var mcCode : String?
    var regID : String = "NA"

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//            UIView.animate(withDuration: 2.0) {
//                self.progressView1.setProgress(0.33, animated: true)
//            }
//        }
        addDelegates()
        hideKeyboardWhenTappedAround()
        dateOfRegistration.addInputViewDatePicker(target: self, selector: #selector(doneButtonPressed))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setData()
    }
    
    func setData(){
//        self.legalBusinessName.text = legalName
//        self.businessCategoryTextField.text = businessName
//        self.panTextField.text = panCardNumber
//        self.bizCode == "SOC" ? (self.registeredIDView.isHidden = true, self.underRegisterView.isHidden = true) : (self.registeredIDView.isHidden = false , self.underRegisterView.isHidden = false)

        self.legalBusinessName.text = merchantRequestResponse?.legalName
        self.businessCategoryTextField.text = merchantRequestResponse?.bizName
        self.panTextField.text = merchantRequestResponse?.panNo
//        self.bizCode == "SOC" ? (self.registeredIDView.isHidden = true, self.underRegisterView.isHidden = true) : (self.registeredIDView.isHidden = false , self.underRegisterView.isHidden = false)

        merchantRequestResponse?.bizCategory == "SOC" ? (self.registeredIDView.isHidden = true, self.underRegisterView.isHidden = true) : (self.registeredIDView.isHidden = false , self.underRegisterView.isHidden = false)


    }
    
    
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        if Constants.byPass{
            self.present(getAddressDetailsViewController(), animated: true, completion: nil)
        }else{
            self.checkValues()
        }
    }
    
    
    
    @IBAction func typeOfBusinessButtonTapped(_ sender: UIButton) {
        let vc = getCommonFieldsViewController() as! CommonFeildVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.type = .businessType
        vc.typeBusiness = {codeBusiness , textBusiness , index in self.businessCategoryTextField.text = textBusiness ; self.bizCode = codeBusiness
            self.bizCode == "SOC" ? (self.registeredIDView.isHidden = true, self.underRegisterView.isHidden = true) : (self.registeredIDView.isHidden = false , self.underRegisterView.isHidden = false)
        }
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func businessCategoryButtonTapped(_ sender: UIButton) {
        let vc = getCommonFieldsViewController() as! CommonFeildVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.type = .businessCat
        vc.catBusiness = {textBusiness in self.natureOfBusinessTextField.text = textBusiness}
        vc.typeBusiness = {codeBusiness , textBusiness , index in self.natureOfBusinessTextField.text = textBusiness ; self.mcCode = codeBusiness
        }
        
        
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    
        
    @objc func doneButtonPressed() {
        if let  datePicker = self.dateOfRegistration.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            datePicker.sizeToFit()
            self.dateOfRegistration.text = dateFormatter.string(from: datePicker.date)
        }
        self.dateOfRegistration.resignFirstResponder()
     }
    
    func addDelegates(){
        registeredID.delegate = self
        panTextField.delegate = self
//        addressTextField.delegate = self
//        addressTextField.delegate = self
//        legalStateTextField.delegate = self
//        legalDistrictTextField.delegate = self
    }
    
//    func setupUI() {
//            self.legalStateTextField.inputView = self.statePicker
//            self.statePicker.onStateSelected = {(state : String , disArr : [String]) in
//                self.selectedState = state
//                self.legalStateTextField.text = state
//                self.allDistrict = disArr
//                self.legalDistrictTextField.inputView = self.cityPicker
//                self.cityPicker.allDistrict = self.allDistrict
//                self.cityPicker.onCitySelect = {(state : String) in
//                    self.legalDistrictTextField.text = state
//                }
//            }
//
//
////            UIView.animate(withDuration: 1.0, delay: 2.0, options:[.repeat, .autoreverse], animations: {
////                self.taskView.backgroundColor = .systemGreen
////                }, completion:nil)
//    }
}

extension GeneralDetailsViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func showAlert(msg: String) {
        DispatchQueue.main.async {
            self.responseLabel.text = msg
        }
    }
    
    func checkValues(){
        showAlert(msg: "")
        guard let businessName = legalBusinessName.text, businessName.count > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_business_name) ?? Constants.alert_enter_business_name)
            return
        }
        //        if !name.isValidName() {
        //            showAlert(msg: Constants.alert_enter_valid_name)
        //            return
        //        }
        guard let Name = name.text, Name.count > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_shop_name) ?? Constants.alert_enter_shop_name)
            return
        }
        
        guard let DOR = dateOfRegistration.text , !DOR.isEmpty else{
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_business_name) ?? Constants.alert_enter_business_name)
            return
        }
        
        guard let businessCategory = businessCategoryTextField.text, businessCategory.count > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_merchant_category) ?? Constants.alert_enter_merchant_category)
            return
        }
        
        guard let natureOfBusiness = natureOfBusinessTextField.text, natureOfBusiness.count > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_choose_business_type) ?? Constants.alert_choose_business_type)
            return
        }
        
      /*
        if bizCode != "SOC"{
            guard let register = registeredID.text, !register.isEmpty else {
                showAlert(msg: Language.getText(with: LanguageConstants.alert_register_ID) ?? Constants.alert_register_ID)
                return
            }
            self.regID = register
        }else{
            self.regID = panCardNumber ?? "na"
        }
        */
        
      if merchantRequestResponse?.bizCategory  != "SOC"{
            guard let register = registeredID.text, !register.isEmpty else {
                showAlert(msg: Language.getText(with: LanguageConstants.alert_register_ID) ?? Constants.alert_register_ID)
                return
            }
            self.regID = register
        }else{
            self.regID = panCardNumber ?? "na"
        }
        
        
        
        guard let pan = panTextField.text, !pan.isEmpty else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_pan_details) ?? Constants.alert_pan_details)
            return
        }
        
        
        
//        guard let state = legalStateTextField.text , !state.isEmpty else {
//            showAlert(msg: "State field can't be empty")
//            return
//        }
        
//        guard let district = legalDistrictTextField.text , !district.isEmpty else{
//            showAlert(msg: "District field can't be empty")
//            return
//        }
        
               
        
        
        
        merchantRequestResponse?.panNo = panCardNumber
        merchantRequestResponse?.bizName = Name
        merchantRequestResponse?.legalName = businessName
        merchantRequestResponse?.bizGroup = "MER"
        merchantRequestResponse?.bizCategory = bizCode
        merchantRequestResponse?.registerId = regID
        merchantRequestResponse?.bizNature = natureOfBusiness
        merchantRequestResponse?.bizDoReg = DOR
        merchantRequestResponse?.mcCode = mcCode
        merchantRequestResponse?.mobileNo = mobileNumber
        
        print(merchantRequestResponse , "mcCode need to change & mobile number")
        
        let vc = getAddressDetailsViewController() as! AddressDetailsViewController
        vc.merchantRequestResponse = merchantRequestResponse
        self.present(vc, animated: true, completion: nil)
        
    }
}

extension GeneralDetailsViewController{
    func animateTextField(textField: UITextField, up: Bool){
        let movementDistance:CGFloat = -160
        let movementDuration: Double = 0.3
        var movement:CGFloat = 0
        if up{
            movement = movementDistance
        }else{
            movement = -movementDistance
        }
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.animateTextField(textField: textField, up:true)
    }
    
    // Finish Editing The Text Field
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.animateTextField(textField: textField, up:false)
    }
    
    // Hide the keyboard when the return key pressed
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
