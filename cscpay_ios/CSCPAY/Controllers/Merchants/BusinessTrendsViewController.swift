//
//  BusinessTrendsViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 25/02/21.
//

import UIKit
import Charts

class BusinessTrendsViewController: UIViewController {

    @IBOutlet weak var weeklyButton: UIButton!
    @IBOutlet weak var monthlyButton: UIButton!
    @IBOutlet weak var fromDateField: UITextField!
    @IBOutlet weak var toDateField: UITextField!
    @IBOutlet weak var volumeGraphView: BarChartView!
    @IBOutlet weak var transactionGraph: BarChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callCharts()
        hideKeyboardWhenTappedAround()
    }
    func callCharts() {
        daysChart()
        daysTransChart()
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func makeRequestButtonClicked(_ sender: Any) {
        if self.weeklyButton.tag == 0 {
            self.weeklyButton.tag = 1
            self.monthlyButton.tag = 0
        } else {
            return
        }
        changeButtonsState(btn : weeklyButton, otherBtns : [monthlyButton])
        callCharts()
        //embed(getMakeRequestViewController(), inView: self.containerView)
    }
    @IBAction func monthlyButtonTapped(_ sender: Any) {
        if self.monthlyButton.tag == 0 {
            self.monthlyButton.tag = 1
            self.weeklyButton.tag = 0
        } else {
            return
        }
        changeButtonsState(btn : monthlyButton, otherBtns : [weeklyButton])
        callCharts()
        //embed(getIncomingRequestViewController(), inView: self.containerView)
    }
    
    func daysChart() {
        
        let weekday = Calendar.current.component(.weekday, from: Date())
        var days = getlast7days(day: weekday)
        let totalDays = days
        days.append(" ")
        
        var highCount = 6
        var startDay1 = 5
        for _ in totalDays {
            if let day = Calendar.current.date(byAdding: .day, value: -(startDay1), to: Date()) {
                let exactDay = Calendar.current.component(.day, from: day)
                let exactMonth = Calendar.current.component(.month, from: day)
                let exactYear = Calendar.current.component(.year, from: day)
                let val = 2
                highCount = highCount > val ? highCount : val
                startDay1 -= 1
            }
            }
        
        var startDays1 = 5
        for _ in totalDays {
            if let day = Calendar.current.date(byAdding: .day, value: -(startDays1), to: Date()) {
                let exactDay = Calendar.current.component(.day, from: day)
                let exactMonth = Calendar.current.component(.month, from: day)
                let exactYear = Calendar.current.component(.year, from: day)// component(.weekday, from: Date())
                //let sevenDaysAgo = calendar.dateByAddingUnit(.day, value: -(startday), toDate: now, options: [])!
                //let val = CoreDataOperations.getDaysRecordsCounts(Statistics.self,type : type, key: "year", value: Int16(exactYear), key2: "month", value2: Int16(exactMonth), key3 : "date", value3 : Int16(exactDay))
                let val = 2
                highCount = highCount > val ? highCount : val
                startDays1 -= 1
            }
        }
        
        //print("Highest number \(highCount)")
        
        let hc = highCount % 5
        if hc != 0 {
            let distance = 5 - hc
            highCount += distance
        }
        // Customization
        //chartView.description = ""
        volumeGraphView.xAxis.axisMinimum = 0.0
        volumeGraphView.xAxis.axisMaximum = Double(days.count - 1) //12.0
        volumeGraphView.xAxis.labelPosition = .bottom
        volumeGraphView.xAxis.centerAxisLabelsEnabled = true
        volumeGraphView.xAxis.setLabelCount(days.count, force: true)// 13.0 setLabelsToSkip(0)
        volumeGraphView.leftAxis.axisMinimum = 0.0
        volumeGraphView.leftAxis.axisMaximum = Double(highCount) // 10.0
        
        volumeGraphView.rightAxis.enabled = false
        volumeGraphView.xAxis.drawGridLinesEnabled = false
        volumeGraphView.leftAxis.drawGridLinesEnabled = false
        volumeGraphView.legend.enabled = false
        volumeGraphView.scaleYEnabled = false
        volumeGraphView.scaleXEnabled = false
        volumeGraphView.pinchZoomEnabled = false
        volumeGraphView.doubleTapToZoomEnabled = false
        volumeGraphView.highlighter = nil
        volumeGraphView.xAxis.labelFont = .systemFont(ofSize: 9, weight: .semibold)
        volumeGraphView.xAxis.axisLineColor = .clear
        volumeGraphView.leftAxis.axisLineColor = .clear
        
        
        
        var surveyEntries = [ChartDataEntry]()
        var i = 0.5
        var startday = 5 // day <= 6 ? 6 : day - 6
        for _ in days {
            if Int(i) == 0 || days.count == Int(i) {
                let entry = BarChartDataEntry(x: Double(i), yValues: [Double(1)])
                surveyEntries.append(entry)
            } else {
                if let day = Calendar.current.date(byAdding: .day, value: -(startday), to: Date()) {
                    let exactDay = Calendar.current.component(.day, from: day)
                    let exactMonth = Calendar.current.component(.month, from: day)
                    let exactYear = Calendar.current.component(.year, from: day)// component(.weekday, from: Date())
                    let val = 2
                    let entry = BarChartDataEntry(x: Double(i), yValues: [Double(val)])
                    surveyEntries.append(entry)
                    startday -= 1
                }
            }
            i += 1
        }
        
        
        // Create bar chart data set containing salesEntries
        let chartDataSet = BarChartDataSet(entries: surveyEntries, label: "Survey")
        //chartDataSet.colors = ChartColorTemplates.joyful()
        //let chartDataSet2 = BarChartDataSet(entries: feedbackEntries, label: "Feedback")
        chartDataSet.drawValuesEnabled = false
        //chartDataSet2.drawValuesEnabled = false
        let formatter = BarChartFormatter(values: days)
        let xAxis = XAxis()
        xAxis.valueFormatter = formatter
        volumeGraphView.xAxis.valueFormatter = xAxis.valueFormatter
        volumeGraphView.xAxis.granularity = 1.0
        
        chartDataSet.setColor(hexStringToUIColor(hex: "715BFE"))
       // chartDataSet2.setColor(UIColor.systemOrange)
        
        // Create bar chart data with data set and array with values for x axis
        let chartData = BarChartData(dataSets: [chartDataSet,  ])
        
        chartData.barWidth = 0.2
        volumeGraphView.data = chartData
        
        // Animation
        volumeGraphView.animate(yAxisDuration: 1.5, easingOption: .easeInOutQuad)
    }
    func daysTransChart() {
        
        let weekday = Calendar.current.component(.weekday, from: Date())
        var days = getlast7days(day: weekday)
        let totalDays = days
        days.append(" ")
        
        var highCount = 6
        var startDay1 = 5
        for _ in totalDays {
            if let day = Calendar.current.date(byAdding: .day, value: -(startDay1), to: Date()) {
                let exactDay = Calendar.current.component(.day, from: day)
                let exactMonth = Calendar.current.component(.month, from: day)
                let exactYear = Calendar.current.component(.year, from: day)
                let val = 2
                highCount = highCount > val ? highCount : val
                startDay1 -= 1
            }
            }
        
        var startDays1 = 5
        for _ in totalDays {
            if let day = Calendar.current.date(byAdding: .day, value: -(startDays1), to: Date()) {
                let exactDay = Calendar.current.component(.day, from: day)
                let exactMonth = Calendar.current.component(.month, from: day)
                let exactYear = Calendar.current.component(.year, from: day)// component(.weekday, from: Date())
                //let sevenDaysAgo = calendar.dateByAddingUnit(.day, value: -(startday), toDate: now, options: [])!
                //let val = CoreDataOperations.getDaysRecordsCounts(Statistics.self,type : type, key: "year", value: Int16(exactYear), key2: "month", value2: Int16(exactMonth), key3 : "date", value3 : Int16(exactDay))
                let val = 2
                highCount = highCount > val ? highCount : val
                startDays1 -= 1
            }
        }
        
        //print("Highest number \(highCount)")
        
        let hc = highCount % 5
        if hc != 0 {
            let distance = 5 - hc
            highCount += distance
        }
        // Customization
        //chartView.description = ""
        transactionGraph.xAxis.axisMinimum = 0.0
        transactionGraph.xAxis.axisMaximum = Double(days.count - 1) //12.0
        transactionGraph.xAxis.labelPosition = .bottom
        transactionGraph.xAxis.centerAxisLabelsEnabled = true
        transactionGraph.xAxis.setLabelCount(days.count, force: true)// 13.0 setLabelsToSkip(0)
        transactionGraph.leftAxis.axisMinimum = 0.0
        transactionGraph.leftAxis.axisMaximum = Double(highCount) // 10.0
        
        transactionGraph.rightAxis.enabled = false
        transactionGraph.xAxis.drawGridLinesEnabled = false
        transactionGraph.leftAxis.drawGridLinesEnabled = false
        transactionGraph.legend.enabled = false
        transactionGraph.scaleYEnabled = false
        transactionGraph.scaleXEnabled = false
        transactionGraph.pinchZoomEnabled = false
        transactionGraph.doubleTapToZoomEnabled = false
        transactionGraph.highlighter = nil
        transactionGraph.xAxis.labelFont = .systemFont(ofSize: 9, weight: .semibold)
        transactionGraph.xAxis.axisLineColor = .clear
        transactionGraph.leftAxis.axisLineColor = .clear
        
        
        
        var surveyEntries = [ChartDataEntry]()
        var i = 0.5
        var startday = 5 // day <= 6 ? 6 : day - 6
        for _ in days {
            if Int(i) == 0 || days.count == Int(i) {
                let entry = BarChartDataEntry(x: Double(i), yValues: [Double(1)])
                surveyEntries.append(entry)
            } else {
                if let day = Calendar.current.date(byAdding: .day, value: -(startday), to: Date()) {
                    let exactDay = Calendar.current.component(.day, from: day)
                    let exactMonth = Calendar.current.component(.month, from: day)
                    let exactYear = Calendar.current.component(.year, from: day)// component(.weekday, from: Date())
                    let val = 2
                    let entry = BarChartDataEntry(x: Double(i), yValues: [Double(val)])
                    surveyEntries.append(entry)
                    startday -= 1
                }
            }
            i += 1
        }
        
        
        // Create bar chart data set containing salesEntries
        let chartDataSet = BarChartDataSet(entries: surveyEntries, label: "Survey")
        //chartDataSet.colors = ChartColorTemplates.joyful()
        //let chartDataSet2 = BarChartDataSet(entries: feedbackEntries, label: "Feedback")
        chartDataSet.drawValuesEnabled = false
        //chartDataSet2.drawValuesEnabled = false
        let formatter = BarChartFormatter(values: days)
        let xAxis = XAxis()
        xAxis.valueFormatter = formatter
        transactionGraph.xAxis.valueFormatter = xAxis.valueFormatter
        transactionGraph.xAxis.granularity = 1.0
        
        chartDataSet.setColor(hexStringToUIColor(hex: "67D55D"))
       // chartDataSet2.setColor(UIColor.systemOrange)
        
        // Create bar chart data with data set and array with values for x axis
        let chartData = BarChartData(dataSets: [chartDataSet,  ])
        
        chartData.barWidth = 0.5
        transactionGraph.data = chartData
        
        // Animation
        transactionGraph.animate(yAxisDuration: 1.5, easingOption: .easeInOutQuad)
    }
}


class BarChartFormatter: NSObject,IAxisValueFormatter,IValueFormatter {


    var values : [String]
    required init (values : [String]) {
        self.values = values
        super.init()
    }


    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
//        if value < 0 {
//            return ""
//        }
//        if value > 4.98 && value < 6.20 {
//            return "Jun"
//        }
        return values[Int(value)]

    }

    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        return values[Int(entry.x)]

    }
}
