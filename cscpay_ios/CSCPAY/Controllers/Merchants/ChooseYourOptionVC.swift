//
//  ChooseYourOptionVC.swift
//  CSCPAY
//
//  Created by Aman Pandey on 27/11/21.
//

import UIKit

struct MerchantOptions{
    let title : String
    let description : String
    let icon : UIImage
    var isSelected :Bool
}


struct MerchantUploadDocuments{
    var docType : String
    var doc : [String]
    var upload : Bool
    var icon : UIImage
}



class ChooseYourOptionVC: UIViewController  {
    
    
    //MARK: - Outlets
    @IBOutlet weak var msmeView: UIView!
    @IBOutlet weak var vleView: UIView!
    @IBOutlet weak var otherView: UIView!
    
    @IBOutlet weak var msmeImageView: UIImageView!
    @IBOutlet weak var vleImageView : UIImageView!
    @IBOutlet weak var otherImageView : UIImageView!
    
    @IBOutlet weak var msmeLabel: UILabel!
    @IBOutlet weak var vleLabel: UILabel!
    @IBOutlet weak var otherLabel: UILabel!
    
    @IBOutlet weak var msmeButton: UIButton!
    @IBOutlet weak var vleButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    var panCardNumber : String = "NA"
    
    var ButtonArray : [UIButton] = []
    var viewArray : [UIView] = []
    var optionsArray : [MerchantOptions] = []
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        ButtonArray = [vleButton,msmeButton , otherButton]
//        viewArray = [vleView ,msmeView, otherView]
        
        if Constants.authenticationStatus != "Done"{
         authenticationWithTouchID(completion: nil)
        }
        optionsArray.append(MerchantOptions(title: "Common Services Centre", description: "Choose this option if you are a CSC village level entrepreneur", icon: UIImage(named: "csc") ?? UIImage(), isSelected: false))
//        optionsArray.append(MerchantOptions(title: "MSME", description: "Choose this option if your business is registered as MSME", icon: UIImage(named: "msme") ?? UIImage(), isSelected: false))
        optionsArray.append(MerchantOptions(title: "Business Entitiy", description: "Choose this option if your are registered business entity", icon: UIImage(named: "user") ?? UIImage(), isSelected: false))
    }
    
    
    @IBAction func msmeButtonTapped(_ sender: UIButton) {
//        selectedButton(tag: sender.tag , viewHidden: false)
    }
    
    
    @IBAction func vleButtonTapped(_ sender: UIButton) {
//        selectedButton(tag: sender.tag , viewHidden: false)
    }
    
    @IBAction func otherButtonTapped(_ sender: UIButton) {
//        selectedButton(tag: sender.tag)
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
  
    
    
    
    
    
    
    
//    func selectedButton(tag: Int , viewHidden: Bool = true){
//        for i in ButtonArray.enumerated(){
//            if i.element.tag == tag{
//                viewArray[i.offset].layer.borderWidth = 1
//                viewArray[i.offset].layer.borderColor = UIColor.lightBlue.cgColor
//            }else{
//                viewArray[i.offset].layer.borderColor = UIColor.clear.cgColor
//                viewArray[i.offset].layer.borderWidth = 0
//            }
//        }
//    }
}





extension ChooseYourOptionVC : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MerchantChooseCell", for: indexPath) as? MerchantChooseCell else {return UITableViewCell()}
        
        let data = optionsArray[indexPath.row]
        print(data.isSelected)
        cell.mainView.layer.borderWidth = data.isSelected == false ? 0 : 1
        cell.mainView.layer.borderColor = data.isSelected == false ? UIColor.msmseNoColor.cgColor : UIColor.lightBlue.cgColor
        cell.mainView.backgroundColor = data.isSelected == false ? UIColor.msmseNoColor : UIColor.msmseYesColor
        
        cell.titleLabel.text = data.title
        cell.descriptionLabel.text = data.description
        cell.cellImageView.image = data.icon
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionsArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in optionsArray.enumerated(){
            if i.offset == indexPath.row{
                optionsArray[indexPath.row].isSelected = !optionsArray[indexPath.row].isSelected
            }else{
                optionsArray[i.offset].isSelected = false
            }
        }
        tableView.reloadData()
        
        if indexPath.row == 1{
            let vc = getGeneralDetailsViewController() as! GeneralDetailsViewController
            vc.panCardNumber = panCardNumber
            self.present(vc, animated: true, completion: nil)
        }else{
            let vc = getMerchantOnBoardingViewController() as! MerchantOnBoardingVC
//            vc.typeOfMerchant = optionsArray[indexPath.row].title == "MSME" ? "Udyog ID" : optionsArray[indexPath.row].title == "VLE" ? "CSC VLE ID" : "OTHER"
            vc.typeOfMerchant = optionsArray[indexPath.row].title == "Common Services Centre" ? "CSC ID" : "Business Entity"
            vc.otpText = "70******90"
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableView.automaticDimension
    }
    
    
}

