//
//  ReceiveConfirmViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 18/06/21.
//

import UIKit

class ReceiveConfirmViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameTagLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileUpiLabel: UILabel!
    @IBOutlet weak var noteView: UITextView!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var responseLabel: UILabel!
    //@IBOutlet weak var alertView: UIView!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var paymentDropdownView: UIView!
    @IBOutlet weak var emojiView: UICollectionView!
    @IBOutlet weak var lblPayFrom: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var imgEmoji: UIImageView!
    @IBOutlet weak var viewBankList: UIView!
    //@IBOutlet weak var lblEmoji: UILabel!
    //@IBOutlet weak var paymentModeDropdown: DropDownTextField!
    
    
    var isFromRequest : Bool = false
    var isFromPendingRequest : Bool = false
    //var titleStr = "Pay"
    //var btnTitle = "PROCEED TO PAY"
    var isFromContacts : Bool = false
    //var isReceive = false
    
    //Values
    var name: String?
    var mobile: String?
    var item : Transactions?
    
    let guestArray : [String]? = ["Pay", "Request"]
    let emojis = ["e1", "e2", "e3", "e4", "e5", "e6" ]
    var currentEmojis = ""
    
    func setupValues() {
        if let names = name {
            self.nameLabel.text = names }
        if let mob = mobile {
            self.mobileUpiLabel.text = mob }
    }
    
    func setupUI() {
        //titleLabel.setup(fontSize: 24, text: titleStr)
        nameLabel.setup(textColor: .customBlack)
        mobileUpiLabel.setup(fontSize: 15, fontType: .semiBold, textColor: .customBlue)
        seperatorView.backgroundColor = .seperatorColor
        
        amountField.setup(fontSize: 24, fontType: .bold, placeholderText: Language.getText(with: LanguageConstants.txt_amount_placeholder) ?? Constants.txt_amount_placeholder)
        noteView.setup(fontSize: 15, fontType: .regular, textColor: .subtitleColor, bgColor: .customWhite, text: "Make a note")
        //lblPayFrom.setup(fontSize: 15, fontType: .regular, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_pay_from) ?? Constants.lbl_pay_from)
        proceedButton.setup(fontType: .semiBold, textColor: .customWhite, bgColor: .taleColor)
        if let to = item {
            self.nameLabel.text = to.toName
            self.mobileUpiLabel.text = to.to
//            self.amountField.text = "\(to.amount)"
//            if let note = to.remark {
//                if note.count != 0 {
//                    self.noteView.text = note
//                }
//            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupUI()
        setupValues()
        //paymentModeDropdown.dropDownDelegate = self
        //updateDropdown(index : 0)
        //self.titleLabel.text = titleStr
        //self.proceedButton.setTitle(btnTitle, for: .normal)
        //self.noteView.textColor = UIColor.lightGray
        
        DispatchQueue.main.async {
            if self.isFromPendingRequest {
                self.amountField.text = "200"
                self.amountField.isUserInteractionEnabled = false
                self.noteView.isUserInteractionEnabled = false
            } else if self.isFromRequest {
                //self.titleLabel.text = self.titleStr //"Request"
                //self.proceedButton.setTitle(self.btnTitle, for: .normal)
            } else if self.isFromContacts {
                self.paymentDropdownView.isHidden = false
            }
        }
    } /*
    func updateDropdown(index : Int){
            self.paymentModeDropdown.addDropDownasInput(withArray: guestArray ?? [])
            if let ga = guestArray, ga.count > 0 {
                self.paymentModeDropdown.text = ga[index]
            }
        
    } */
    override func viewDidLayoutSubviews() {
        //noteView.frame.size.width = noteView.intrinsicContentSize.width
    }
    
    @IBAction func okButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            //self.alertView.isHidden = true
            self.present(getSuccessViewController(), animated: true, completion: nil)
        }
    }
    @IBAction func qrButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.present(getMyQRViewController(), animated: true, completion: nil)
        }
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func proceedButtonTapped(_ sender: Any) {
        guard let amount = amountField.text else {
            DispatchQueue.main.async {
                self.responseLabel.text = Language.getText(with: LanguageConstants.alert_enter_amount) ?? Constants.alert_enter_amount
            }
            return
        }
        guard amount.count > 0 else {
            DispatchQueue.main.async {
                self.responseLabel.text = Language.getText(with: LanguageConstants.alert_enter_valid_amount) ??  Constants.alert_enter_valid_amount
            }
            return
        }
        
        if isFromRequest {
            DispatchQueue.main.async {
                //self.alertView.isHidden = false
            }
            return
        }
        let type = "MR"
        
        //let item = CoredataManager.saveTransaction(from: "Gowtham", to: mobile ?? self.mobileUpiLabel.text ?? "nil", type:type, toName: name ?? self.nameLabel.text ?? "nil", amount: Double(amount) ?? 0, remark: noteView.text ?? "", emoji: self.currentEmojis, status: "success", isSave: true)
        //let vc = getSuccessViewController() as! SuccessViewController
        //vc.isFromPay = true
        let vc = getGenerateMerchantQRViewController() as! GenerateMerchantQRViewController
        //vc.transaction = item
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func decreaseAmountButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            if let amt = Int(self.amountField.text ?? "0") {
                self.amountField.text = amt <= 99 ? "\(amt)" : "\(amt - 100)"
            } else {
                self.amountField.text = "100"
            }
        }
    }
    @IBAction func increaseAmountButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            if let amt = Int(self.amountField.text ?? "0") {
                self.amountField.text = amt > 99900 ? "\(amt)" : "\(amt + 100)"
            } else {
                self.amountField.text = "100"
            }
        }
    }
    @IBAction func closeBtnTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.viewBankList.isHidden = true
        }
    }
}

extension ReceiveConfirmViewController : UITextViewDelegate, UITextFieldDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.subtitleColor {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = Language.getText(with: LanguageConstants.txt_note_placeholder) ?? Constants.txt_note_placeholder
            textView.textColor = UIColor.subtitleColor
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var newText = textView.text!
        newText.removeAll { (character) -> Bool in
            return character == " " || character == "\n"
        }
        guard text.rangeOfCharacter(from: CharacterSet.newlines) == nil else {
                // textView.resignFirstResponder() // uncomment this to close the keyboard when return key is pressed
                return false
            }

        return (newText.count + text.count) <= 100
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string) as String
        if let num = Int(newText), num >= 0 && num <= 100000 {
//            let formatter = NumberFormatter()
//                formatter.numberStyle = NumberFormatter.Style.decimal
//            if let number = Double(textField.text!.replacingOccurrences(of: ",", with: "")) {
//                let result = formatter.string(from: NSNumber(value: number))
//                    textField.text = result!
//            }
            
            return true
        } else if textField.text?.count ?? 0 <= 1 && newText == "" {
            return true
        } else {
            return false
        }
    }
}

extension ReceiveConfirmViewController : DropDownDelegate {
    func dropdownDidChangeValue(textfield: UITextField, title: String, index: Int) {
        /*if textfield == paymentModeDropdown {
            updateDropdown(index : index)
        }*/
    }
    
    
}
extension ReceiveConfirmViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.emojis.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmojiCell", for: indexPath) as? EmojiCell {
            if let img = UIImage.init(named: "\(self.emojis[indexPath.row])") {
            cell.imgEmoji.image = img
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if let img = UIImage.init(named: "\(self.emojis[indexPath.row])") {
                self.imgEmoji.image = img
                self.currentEmojis  = self.emojis[indexPath.row]
            } else {
                self.imgEmoji.image = UIImage()
                self.currentEmojis  = ""
            }
            UIView.animate(withDuration: 0.6, animations: {
                self.imgEmoji.frame.origin.x -= 100
            }) {_ in
                UIView.animateKeyframes(withDuration: 0.6, delay: 0.20, options: .beginFromCurrentState, animations: {
                    self.imgEmoji.frame.origin.x += 100
                }, completion: {_ in
                    UIView.animateKeyframes(withDuration: 0.6, delay: 0.25, options: .beginFromCurrentState, animations: {
                        self.imgEmoji.frame.origin.x += 100
                    }) { (_) in
                        UIView.animate(withDuration: 0.6, animations: {
                            self.imgEmoji.frame.origin.x -= 100
                        })
                    }
                })
            }
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.imgEmoji.image = UIImage()
            self.currentEmojis  = ""
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.bounds.size.width) / CGFloat(6).rounded(.down)
            return CGSize(width: width, height: 40)
    }
        
}
extension ReceiveConfirmViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    
}

