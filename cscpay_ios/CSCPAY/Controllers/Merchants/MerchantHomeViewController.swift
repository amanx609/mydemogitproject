//
//  MerchantHomeViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 16/06/21.
//

import UIKit
import LocalAuthentication

class MerchantHomeViewController: UIViewController {

    @IBOutlet weak var lblGreeting: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    //@IBOutlet weak var upiLabel: UILabel!
    @IBOutlet weak var lblSend: UILabel!
    @IBOutlet weak var lblReceive: UILabel!
    @IBOutlet weak var lblScan: UILabel!
    @IBOutlet weak var lblRequests: UILabel!
    @IBOutlet weak var lblFavourites: UILabel!
    @IBOutlet weak var lblForyou: UILabel!
    @IBOutlet weak var lblHistory: UILabel!
    @IBOutlet weak var lblhistoryDescription: UILabel!
    @IBOutlet weak var lblOffer: UILabel!
    
    @IBOutlet weak var recentHistoryCollectionView: UICollectionView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var myCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var adCollectionView: UICollectionView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var viewLinkBankAccount: UIView!
    //@IBOutlet weak var viewFavourites: UIView!
    @IBOutlet weak var doDontAlertView: UIView!
    @IBOutlet weak var lblDo: UILabel!
    @IBOutlet weak var lblDont: UILabel!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var viewNoFavorite: UIView!
    
    let inset: CGFloat = 0
    let minimumLineSpacing: CGFloat = 0
    let minimumInteritemSpacing: CGFloat = 5
    let cellsPerRow = 4
    let cellsPerColumn = 1
    
    var index = 0
    var inForwardDirection = true
    var timer: Timer?
    var isFirst: Bool = false
    var accountLinked = false //false
    var favorites : [Transactions]?
    var merchantRegResponse : MerchantRegistrationModel?
    var requestMerchantResponse : CommonModel?
    
    func setupUI() {
        lblDo.setup(fontSize: 14, fontType: .regular, textColor: .titleColor, bgColor: .clear, text: "• Make sure you login and initiate UPI transaction in complete privacy. \n• Change your UPI application password and UPI PIN / MPIN frequently. \n• Incase of transactional or non-transactional issue, explore the support section of the application.")
        lblDont.setup(fontSize: 14, fontType: .regular, textColor: .titleColor, bgColor: .clear, text: "• Please do not share your PIN or MPIN / do not store it in your Mobile handset. \n• Never let anyone see you entering your application password or UPI PIN / MPIN.")
        lblGreeting.setup(textColor: .darkGray, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        usernameLabel.setup(fontSize: 20.0, fontType: .bold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        //upiLabel.setup(fontSize: 14.0, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblSend.setup(fontSize: 14.0, fontType: .semiBold, textColor: .darkGray, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblReceive.setup(fontSize: 14.0, fontType: .semiBold, textColor: .darkGray, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblScan.setup(fontSize: 14.0, fontType: .semiBold, textColor: .darkGray, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblRequests.setup(fontSize: 14.0, fontType: .semiBold, textColor: .darkGray, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblFavourites.setup(fontSize: 16.0, fontType: .semiBold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblForyou.setup(fontSize: 16.0, fontType: .semiBold, textColor: .skyBlueTitle, text: Language.getText(with: LanguageConstants.lbl_verification_title))
//        lblOffer.setup(fontSize: 16.0, fontType: .semiBold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblHistory.setup(fontSize: 16.0, fontType: .semiBold, textColor: .titleColor, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblhistoryDescription.setup(fontSize: 14.0, fontType: .regular, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        
        self.bottomView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.adCollectionView.showsHorizontalScrollIndicator = false
        
        if let recent = CoredataManager.getSpecificRecord(Transactions.self, key: "type", value: "P") {
            self.favorites = recent
        }
        
        
//        if isFirst {
//            self.doDontAlertView.isHidden = false
//        } else { self.doDontAlertView.isHidden = true }
//        if accountLinked {
//            //self.viewFavourites.isHidden = false
//            self.viewLinkBankAccount.isHidden = true
//        } else {
//            //self.viewFavourites.isHidden = true
//            self.viewLinkBankAccount.isHidden = true
//        }
        self.viewLinkBankAccount.isHidden = false
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if let activeStatus = UD.SharedManager.getValues(key: "ActiveStatus", type: .int) as? Int{
            if activeStatus == 30{
               self.registerVPA()
            }
        }
        self.hideKeyboardWhenTappedAround()
        self.setupUI()
        swipeAction()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        startTimer()
    }
    func startTimer() {
        if timer == nil {
            timer =  Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
        }
    }
    @objc func scrollAutomatically(_ timer1: Timer) {

        //scroll to next cell
           let items = adCollectionView.numberOfItems(inSection: 0)
           if (items - 1) == index {
            adCollectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
           } else if index == 0 {
            adCollectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
           } else {
            adCollectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
           }

           if inForwardDirection {
               if index == (items - 1) {
                   index -= 1
                   inForwardDirection = false
               } else {
                   index += 1
               }
           } else {
               if index == 0 {
                   index += 1
                   inForwardDirection = true
               } else {
                   index -= 1
               }
           }
    }
    func swipeAction() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)

    }
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                DispatchQueue.main.async {
                    self.presentDetail(getSettingsViewController(), subType: .fromLeft)
                }
            case UISwipeGestureRecognizer.Direction.left:
                DispatchQueue.main.async {
                    self.present(getScannerViewController(), animated: true, completion: nil)
                }
                
            default:
                break
            }
        }
    }
    
    @IBAction func okBtnTapped(_ sender: Any) {
        self.doDontAlertView.isHidden = true
    }
    
    @IBAction func receiveButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            let vc = getContactsViewController() as! ContactsViewController
            vc.isReceive = true
            vc.isMerchant = true
            self.present(vc , animated: true, completion: nil)
        }
    }
    
    @IBAction func makePaymentButtonTapped(_ sender: Any) {
        self.present(getSettlementViewController(), animated: true, completion: nil)
    }
    
    @IBAction func requestPaymentButtonTapped(_ sender: Any) {
        notImplementedYetAlert(base: self)
        //self.present(getIncomingRequestViewController(), animated: true, completion: nil)
    }
    
    @IBAction func historyButtonTapped(_ sender: Any) {
//        notImplementedYetAlert(base: self)
        self.present(getMerchantHistoryViewController(), animated: true, completion: nil)
    }
    
    @IBAction func profileButtonTapped(_ sender: Any) {
        let vc = getSettingsViewController() as! SettingsViewController
        vc.isMerchant = true
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func notificationButtonTapped(_ sender: Any) {
        self.present(getNotificationViewController(), animated: true, completion: nil)
    }
    
    @IBAction func qrButtonTapped(_ sender: Any) {
        self.present(getMyQRViewController(), animated: true, completion: nil)
    }
    
    @IBAction func transactionButtonTapped(_ sender: Any) {
        let vc = getMerchantHistoryViewController() as! MerchantHistoryViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func becomeMerchantBtnTapped(_ sender: Any) {
        notImplementedYetAlert(base: self)
        //self.present(getGeneralDetailsViewController(), animated: true, completion: nil)
    }
    
    @IBAction func linkNowBtnTapped(_ sender: Any) {
        notImplementedYetAlert(base: self)
        //self.present(getSelectYourBankViewController(), animated: true, completion: nil)
    }
}
extension MerchantHomeViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == adCollectionView {
            return 3
        }else if collectionView == recentHistoryCollectionView{
            return 4
        } else {
            if self.favorites?.count ?? 0 == 0 {
                self.viewNoFavorite.isHidden = false
                return 0
            } else {
                self.viewNoFavorite.isHidden = true
            }
            if self.favorites?.count ?? 0 <= 3 {
                return (self.favorites?.count ?? 0)
            }
            return Constants.contactCount }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == adCollectionView {
            let cell = collectionView
                .dequeueReusableCell(withReuseIdentifier: "AdCell", for: indexPath) as! AdCell
            return cell
        } else if collectionView == recentHistoryCollectionView {
            let cell = collectionView
                .dequeueReusableCell(withReuseIdentifier: "MerchantRecentHistory", for: indexPath) as! MerchantRecentHistory
            cell.amountLabel.text = "\u{20B9} "+"600"
            cell.dateTimeLabel.text = "Oct 24 2021 at 06:47 PM"
            return cell
        }else {
            let cell = collectionView
                .dequeueReusableCell(withReuseIdentifier: "RecentCell", for: indexPath) as! RecentCell
            
            if self.favorites?.count ?? 0 <= 3 {
                
                    let item = self.favorites?[indexPath.row]
                    cell.text = item?.toName ?? "-"
                
            } else if indexPath.row == (Constants.contactCount - 1) {
//                cell.imageView.image = UIImage.init(named: "moreContacts")
//                cell.nameLabel.text = "More"
//                cell.nameLabel.textColor = .systemBlue
                cell.isMore = true
            } else {
//                cell.imageView.image = UIImage.init(named: "avatar")
//                cell.nameLabel.text = "Gowtham"
//                cell.nameLabel.textColor = .black
                let item = self.favorites?[indexPath.row]
                cell.text = item?.toName ?? "-"
                //cell.text = "GowthamS"
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == adCollectionView {
            let itemHeight = collectionView.bounds.size.height
            return CGSize(width: collectionView.bounds.size.width , height: itemHeight)
        }else if collectionView == recentHistoryCollectionView{
//            let width = (self.collectionView.bounds.size.width) / CGFloat(cellsPerRow).rounded(.down)
                return CGSize(width: 141, height: 78)
        } else  if collectionView == self.collectionView {
            let width = (self.collectionView.bounds.size.width) / CGFloat(cellsPerRow).rounded(.down)
                return CGSize(width: width, height: 90)
            }else{
                return CGSize(width: 0, height: 0)
            }
        }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionView {
            DispatchQueue.main.async {
                if indexPath.row == 3 {
                    self.present(getContactsViewController(), animated: true, completion: nil)
                } else {
                    let vc = getPayConfirmViewController() as! PayConfirmViewController
                    vc.item = self.favorites?[indexPath.row]
                    //vc.isFromContacts = true
                    self.present(vc, animated: true, completion: nil) }
            }
        }
    }
    
}

extension MerchantHomeViewController{
    
    
    func registerVPA(){
        self.showActivityIndicator()
        guard let requestModel = merchantRegResponse else{
            presentAlert(title: "Response Err", msg: "Response nil")
            return
        }
        print(requestModel)
        APIManager.getMerchant(MerchantRegistrationModel.self, reqAction: "activateMerchant", pspBank: "IBL",  urlString: "merchant/activate", requestModel: nil ,merchantRegresponse: requestModel) { res, err, text in
            self.hideActivityIndicator()
            print(res)
            print(err)
            if res != nil{
                
                if let result = res{
                    UD.SharedManager.saveToUserDefaults(key: "ActiveStatus", value: 100)
                    
                    
                    
                    
                    CoredataManager.updateDetail(App_User.self, key: "aesKey", value: result.cred?.aesKey ?? "")
                    CoredataManager.updateDetail(App_User.self, key: "deviceId", value: result.head.deviceID ?? "")
                    CoredataManager.updateDetail(App_User.self, key: "hashToken", value: result.cred?.hashToken ?? "")
                    CoredataManager.updateDetail(App_User.self, key: "hmac", value: result.cred?.hmac ?? "")
                    
                }
            }
        }
    }
    
    
    
    
}





// MARK:-
extension MerchantHomeViewController {
    
    // MARK: Present Alert
    func notifyUser(_ msg: String, err: String?) {
        let alert = UIAlertController(title: msg,
                                      message: err,
                                      preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "OK",
                                         style: .cancel, handler: nil)
        
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true,
                     completion: nil)
    }
    
    
    // MARK:-
    // MARK: Get error message
    func errorMessage(errorCode:Int) -> String{
        
        var strMessage = ""
        
        switch errorCode {
        case LAError.authenticationFailed.rawValue:
            strMessage = "Authentication Failed"
            
        case LAError.userCancel.rawValue:
            strMessage = "User Cancel"
            
        case LAError.userFallback.rawValue:
            strMessage = "User Fallback"
            
        case LAError.systemCancel.rawValue:
            strMessage = "System Cancel"
            
        case LAError.passcodeNotSet.rawValue:
            strMessage = "Passcode Not Set"
        case LAError.biometryNotAvailable.rawValue:
            strMessage = "TouchI DNot Available"
            
        case LAError.biometryNotEnrolled.rawValue:
            strMessage = "TouchID Not Enrolled"
            
        case LAError.biometryLockout.rawValue:
            strMessage = "TouchID Lockout"
            
        case LAError.appCancel.rawValue:
            strMessage = "App Cancel"
            
        case LAError.invalidContext.rawValue:
            strMessage = "Invalid Context"
            
        default:
            strMessage = "Some error found"
            
        }
        
        return strMessage
        
    }
}

