//
//  SummaryViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 25/02/21.
//

import UIKit
import  CoreLocation





class SummaryViewController: UIViewController {


    @IBOutlet weak var contactNumberLbl: UILabel!
    @IBOutlet weak var businessNameLbl: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var panCardGstDetailsLbl: UILabel!
    @IBOutlet weak var businessNameLabel: UILabel!
    @IBOutlet weak var shopNameLabel: UILabel!
    @IBOutlet weak var typeOfBusinessLabel: UILabel!
    @IBOutlet weak var merchantCategoryLabel: UILabel!
    @IBOutlet weak var ownerNameLabel: UILabel!
    @IBOutlet weak var panCardLabel: UILabel!
    @IBOutlet weak var gstInLabel: UILabel!
    @IBOutlet weak var accountnumberLabel: UILabel!
    @IBOutlet weak var ifscCodeLabel: UILabel!
    
    
    var merchantRequestResponse : CommonModel?
    var fromOtpionOne = true
    var getlocation : CLLocation = CLLocation()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateData()
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func verifyButtonTapped(_ sender: Any) {
        if Constants.byPass{
            self.present(getMerchantPendingViewController(), animated: true, completion: nil)
        }else{
            self.directMerchantRegistration()
        }
    }
    
    
    
    
    
    func directMerchantRegistration(){
        self.showActivityIndicator()
        guard let request = merchantRequestResponse else {
            return
        }
        print(request)
        APIManager.getMerchant(MerchantRegistrationModel.self, reqAction: "regMerchant", urlString: "merchant/register" , requestModel: request , merchantRegresponse: nil) { res, err, text in
            self.hideActivityIndicator()
            if res != nil{
                if let result : MerchantRegistrationModel = res {

                    
                    UD.SharedManager.saveToUserDefaults(key: "PANNo.", value: result.body.panNo)
                    UD.SharedManager.saveToUserDefaults(key: "LegalName", value: result.body.legalName)
                    UD.SharedManager.saveToUserDefaults(key: "MobileNo.", value: result.body.mobileNo)
                    let actStatus = Int(result.body.activeStatus ?? "") ?? 0
                    UD.SharedManager.saveToUserDefaults(key: "ActiveStatus", value: actStatus)
                    UD.SharedManager.saveToUserDefaults(key: "bizId", value: result.body.bizId)
                    UD.SharedManager.saveToUserDefaults(key: "seqKey", value: result.body.seqKey)
                    
                    
                    CoredataManager.updateDetail(App_User.self, key: "aesKey", value: result.cred?.aesKey ?? "")
                    CoredataManager.updateDetail(App_User.self, key: "deviceId", value: result.head.deviceID ?? "")
                    CoredataManager.updateDetail(App_User.self, key: "hashToken", value: result.cred?.hashToken ?? "")
                    CoredataManager.updateDetail(App_User.self, key: "hmac", value: result.cred?.hmac ?? "")
                    
//
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        let vc = getMerchantSuccessViewController() as! MerchantSucessVC
                        vc.showSucessFrom = .Summary
//                        vc.merchantRequestModel = request
                        vc.merchantRegResponse = result
                        vc.msgString = "You're suceessfully registered with CSC PAY. Please upload your documents to continue"
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }else{
                print(err)
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    func merchantCheckVPA(){
        APIManager.getMerchant(MerchantResponse.self, reqAction: "checkVPA", pspBank: "IndusInd", virtualAddress: "abhishekstore@cscindus", urlString: "indus/merchant/checkvpa", requestModel: merchantRequestResponse , merchantRegresponse: nil) { res, err, text in
            print(res)
            print(err)
        }
    }
   
    
    func merchantCollectRequest(){
        APIManager.getMerchant(MerchantResponse.self, reqAction: "initCollect", pspBank: "IndusInd", virtualAddress: "abhishekstore@cscindus", urlString: "indus/merchant/collect" , requestModel: merchantRequestResponse , merchantRegresponse: nil) { res, err, text in
            print(res)
            print(err)
        }
    }
 
    
    func merchantTransactionStatus(){
        APIManager.getMerchant(MerchantResponse.self, reqAction: "txnStatus", pspBank: "IndusInd", virtualAddress: "abhishekstore@cscindus", urlString: "indus/merchant/status" , requestModel: merchantRequestResponse , merchantRegresponse: nil) { res, err, text in
            print(res)
            print(err)
        }
    }
    
    func merchantTransactionHistory(){
        APIManager.getMerchant(MerchantResponse.self, reqAction: "txnHistory", pspBank: "IndusInd", urlString: "indus/merchant/history" , requestModel: merchantRequestResponse , merchantRegresponse: nil) { res, err, text in
            print(res)
            print(err)
        }
    }
    
    func merchantTransactionRefund(){
        APIManager.getMerchant(MerchantResponse.self, reqAction: "txnRefund", pspBank: "IndusInd", urlString: "indus/merchant/refund" , requestModel: merchantRequestResponse , merchantRegresponse: nil) { res, err, text in
            print(res)
            print(err)
        }
    }
    
    
    
    
    func populateData(){
        guard let request = merchantRequestResponse else{
            return
        }
        print(request)
        contactNumberLbl.numberOfLines = 4
        addressLabel.numberOfLines = 2
        panCardGstDetailsLbl.numberOfLines = 3
        contactNumberLbl.text = "Contact Number: \(request.mobileNo ?? "NA")"
        addressLabel.text = "Address: \(request.address ?? "NA"), "+" \(request.city ?? "NA"), " + " \(request.districtName ?? "DELHI")"+" \(request.stateName ?? "NA")"
        
        businessNameLabel.text = "Business Name: \(request.legalName ?? "NA") "
        shopNameLabel.text = " Name: \(request.bizName ?? "NA") "
        typeOfBusinessLabel.text = " Type of Business: \(request.bizNature ?? "NA")"
        merchantCategoryLabel.text = " Category: \(request.bizCategory ?? "NA")"
        ownerNameLabel.text = " Owner Name: \(request.authPerson ?? "NA")"
        
        panCardLabel.text = " PAN Card: \(request.panNo ?? "NA")"
        gstInLabel.text = " GSTIN: \(request.gstin ?? "NA")"
        accountnumberLabel.text = " Account No:\(request.accountNo?.masked(6, reversed: true) ?? "NA")"
        ifscCodeLabel.text = " IFSC Code: \(request.ifscCode ?? "NA")"
    }
    
}
extension StringProtocol {
    func masked(_ n: Int = 5, reversed: Bool = false) -> String {
        let mask = String(repeating: "•", count: Swift.max(0, count-n))
        return reversed ? mask + suffix(n) : prefix(n) + mask
    }
}
