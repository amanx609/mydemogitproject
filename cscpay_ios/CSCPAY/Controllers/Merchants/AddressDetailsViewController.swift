//
//  AddressDetailsViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 24/02/21.
//

import UIKit

class AddressDetailsViewController: UIViewController , UITextFieldDelegate  {
  
    

    @IBOutlet weak var contactNameField: UITextField!
    @IBOutlet weak var contactMobileTextField: UITextField!
    @IBOutlet weak var authKYCType: UITextField!
    @IBOutlet weak var kycNumberTextField: UITextField!
    @IBOutlet weak var emailIdTextField: UITextField!
    @IBOutlet weak var progressView1: UIProgressView!
//    @IBOutlet weak var progressView2: UIProgressView!
//    @IBOutlet weak var taskView: UIView!
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var authKYCButton: UIButton!
    
    private let statePicker = StateCity()
    private let cityPicker = CityPicker()
    var selectedState : String?
    var allDistrict = [String]()
    var merchantRequestResponse : CommonModel?
    


    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        addDelegates()
//        cityField.dropDownDelegate = self
//        stateField.dropDownDelegate = self
    }
    func setupUI() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 2.0) {
                self.progressView1.setProgress(0.66, animated: true)
            }
//            self.stateField.inputView = self.statePicker
//            self.statePicker.onStateSelected = {(state : String , disArr : [String]) in
//                self.selectedState = state
//                self.stateField.text = state
//                self.allDistrict = disArr
//                self.cityField.inputView = self.cityPicker
//                self.cityPicker.allDistrict = self.allDistrict
//                self.cityPicker.onCitySelect = {(state : String) in
//                    self.cityField.text = state
//                }
//            }
            
            
//            UIView.animate(withDuration: 1.0, delay: 2.0, options:[.repeat, .autoreverse], animations: {
//                self.taskView.backgroundColor = .systemGreen
//                }, completion:nil)
        }
    }
    
    
        
    
    
    func showAlert(msg: String) {
        DispatchQueue.main.async {
            self.responseLabel.text = msg
        }
    }
    
    func addDelegates(){
        authKYCType.delegate = self
        kycNumberTextField.delegate = self
        emailIdTextField.delegate = self
    }
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func nextButtonTapped(_ sender: Any) {
        if Constants.byPass{
            self.present(getPanCardAndBankViewController(), animated: true, completion: nil)
        }else{
            checkValues()
        }
    }
    
    
    @IBAction func authKycTypeButtonTapped(_ sender: UIButton) {
        let vc = getCommonFieldsViewController() as! CommonFeildVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.type = .docType
        vc.catBusiness = {textBusiness in self.authKYCType.text = textBusiness}
        self.present(vc, animated: true, completion: nil)
    }
}

extension AddressDetailsViewController : DropDownDelegate {
    func dropdownDidChangeValue(textfield: UITextField, title: String, index: Int) {
        
    }
    
    func checkValues(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
            self.view.isUserInteractionEnabled = true
        }
        DispatchQueue.main.async {
            self.responseLabel.text = ""
            self.view.isUserInteractionEnabled = false
            
            guard let contactName = self.contactNameField.text, contactName.count > 0 else {
                self.responseLabel.text = "Please enter contact name"
                return
            }
            guard let contactMobile = self.contactMobileTextField.text, contactMobile.count > 0 else {
                self.responseLabel.text = "Please enter contact mobile number"
                return
            }
            guard let kycType = self.authKYCType.text, kycType.count > 0 else {
                self.responseLabel.text = "Please select KYC type"
                return
            }
            guard let kycNumber = self.kycNumberTextField.text, kycNumber.count > 0 else {
                self.responseLabel.text = "Please enter KYC Number"
                return
            }
            
            
            guard let emailID = self.emailIdTextField.text, emailID.count > 0 else {
                self.responseLabel.text = "Please enter email id"
                return
            }
            
            if !emailID.isValidEmail() {
                self.showAlert(msg: "Please enter valid email id")
                return
            }
            
            
            
            
            self.merchantRequestResponse?.authPerson = contactName
            self.merchantRequestResponse?.authContact = contactMobile
            self.merchantRequestResponse?.authKycType = kycType
            self.merchantRequestResponse?.authKycId = kycNumber
            self.merchantRequestResponse?.email = emailID
            
            
            
            
            let vc = getPanCardAndBankViewController() as! PanCardAndBankViewController
            vc.merchantRequestResponse = self.merchantRequestResponse
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension AddressDetailsViewController{
    func animateTextField(textField: UITextField, up: Bool){
        let movementDistance:CGFloat = -160
        let movementDuration: Double = 0.3
        var movement:CGFloat = 0
        if up{
            movement = movementDistance
        }else{
            movement = -movementDistance
        }
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.animateTextField(textField: textField, up:true)
    }
    
    // Finish Editing The Text Field
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.animateTextField(textField: textField, up:false)
    }
    
    // Hide the keyboard when the return key pressed
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
