//
//  MerchantHomeViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 25/02/21.
//

import UIKit

class MerchantHomeViewController2: UIViewController {

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var upiLabel: UILabel!
    @IBOutlet weak var requestMoneyView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.requestMoneyView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    @IBAction func requestMoneyButtonTapped(_ sender: Any) {
        let vc = getMerchantRequestViewController() as! MerchantRequestViewController
        vc.isFromMerchant = true
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func profileButtonTapped(_ sender: Any) {
        self.present(getSettingsViewController(), animated: true, completion: nil)
    }
    @IBAction func notificationButtonTapped(_ sender: Any) {
        self.present(getNotificationViewController(), animated: true, completion: nil)
    }
    @IBAction func qrButtonTapped(_ sender: Any) {
        self.present(getScannerViewController(), animated: true, completion: nil)
    }
    @IBAction func transactionButtonTapped(_ sender: Any) {
    }
    @IBAction func businessTrendsButtonTapped(_ sender: Any) {
    }
    @IBAction func settleButtonTapped(_ sender: Any) {
        self.present(getSettlementsViewController(), animated: true, completion: nil)
        
    }
}
