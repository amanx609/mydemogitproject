//
//  SetMerchantPinViewController.swift
//  CSCPAY
//
//  Created by Himanshu Chimanji on 20/12/21.
//

import UIKit
protocol SMPDelegate {
    func shareData(data: String)
}

class SetMerchantPinViewController: UIViewController {
    
    @IBOutlet weak var npFirst: UITextField!
    @IBOutlet weak var npSecond: UITextField!
    @IBOutlet weak var npThird: UITextField!
    @IBOutlet weak var npFourth: UITextField!
    
    @IBOutlet weak var cpFirst: UITextField!
    @IBOutlet weak var cpSecond: UITextField!
    @IBOutlet weak var cpThird: UITextField!
    @IBOutlet weak var cpFourth: UITextField!
    @IBOutlet weak var lblResponse: UILabel!
    var delegate: SMPDelegate?
    var mobileNumber = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func confirmAction(_ sender: UIButton) {
        let npPin = "\(npFirst.text ?? "")\(npSecond.text ?? "")\(npThird.text ?? "")\(npFourth.text ?? "")"
        let cpPin = "\(cpFirst.text ?? "")\(cpSecond.text ?? "")\(cpThird.text ?? "")\(cpFourth.text ?? "")"
        lblResponse.text = ""
        if npPin.count == 4 && cpPin.count == 4 {
            if npPin == cpPin {
                DispatchQueue.main.async {
                    self.lblResponse.text = ""
                    let mPinSha = "\(self.mobileNumber)\(npPin)".sha256() as? String ?? ""
                    self.delegate?.shareData(data: mPinSha)
                    self.dismiss(animated: true, completion: nil)
                }
            }
            else {
                self.lblResponse.text = "Pin and Confirm Pin not matched"
            }
        }
        else if npPin.count < 4 {
            self.lblResponse.text = "Please Enter 4 digits pin "
        }
        else if cpPin.count < 4 {
            self.lblResponse.text = "Please Enter 4 digits confirm pin "
        }
    }

    
    
    @IBAction func backAction(_ sender: UIButton) {
    }
    
}

extension SetMerchantPinViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if npFirst.isFirstResponder || npSecond.isFirstResponder || npThird.isFirstResponder || npFourth.isFirstResponder {
            
            if ((textField.text?.count)! <= 1  && string.count > 0){
                if(textField == npFirst)
                {
                    npSecond.becomeFirstResponder()
                }
                if(textField == npSecond)
                {
                    npThird.becomeFirstResponder()
                }
                if(textField == npThird)
                {
                    npFourth.becomeFirstResponder()
                }
                
                textField.text = string
                return false
            }
            else if ((textField.text?.count)! >= 1  && string.count == 0){
                // on deleting value from Textfield
                if(textField == npSecond)
                {
                    npFirst.becomeFirstResponder()
                }
                if(textField == npThird)
                {
                    npSecond.becomeFirstResponder()
                }
                if(textField == npFourth)
                {
                    npThird.becomeFirstResponder()
                }
                textField.text = ""
                return false
            }
            else if ((textField.text?.count)! >= 1  ) {
                textField.text = string
                return false
            }
        }
        
        if cpFirst.isFirstResponder || cpSecond.isFirstResponder || cpThird.isFirstResponder || cpFourth.isFirstResponder {
            
            if ((textField.text?.count)! <= 1  && string.count > 0){
                if(textField == cpFirst)
                {
                    cpSecond.becomeFirstResponder()
                }
                if(textField == cpSecond)
                {
                    cpThird.becomeFirstResponder()
                }
                if(textField == cpThird)
                {
                    cpFourth.becomeFirstResponder()
                }
                
                textField.text = string
                return false
            }
            else if ((textField.text?.count)! >= 1  && string.count == 0){
                // on deleting value from Textfield
                if(textField == cpSecond)
                {
                    cpFirst.becomeFirstResponder()
                }
                if(textField == cpThird)
                {
                    cpSecond.becomeFirstResponder()
                }
                if(textField == cpFourth)
                {
                    cpThird.becomeFirstResponder()
                }
                textField.text = ""
                return false
            }
            else if ((textField.text?.count)! >= 1  ) {
                textField.text = string
                return false
            }
        }
        
        return true
        
        
    }
}
