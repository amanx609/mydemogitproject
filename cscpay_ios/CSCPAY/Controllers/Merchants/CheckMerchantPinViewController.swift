//
//  CheckMerchantPinViewController.swift
//  CSCPAY
//
//  Created by Himanshu Chimanji on 20/12/21.
//

import UIKit
protocol VerificationPinDelegate {
    func successfullPin()
}

class CheckMerchantPinViewController: UIViewController {
    
    @IBOutlet weak var npFirst: UITextField!
    @IBOutlet weak var npSecond: UITextField!
    @IBOutlet weak var npThird: UITextField!
    @IBOutlet weak var npFourth: UITextField!
    @IBOutlet weak var lblResponse: UILabel!
    @IBOutlet weak var mobileNumberLbl: UILabel!
    
    var delegate: VerificationPinDelegate?
    var user: App_User?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        user  = CoredataManager.getDetail(App_User.self)
        
        mobileNumberLbl.text = user?.mobile
    }
    
    @IBAction func doneAction(_ sender: UIButton) {
        let npPin = "\(npFirst.text ?? "")\(npSecond.text ?? "")\(npThird.text ?? "")\(npFourth.text ?? "")"
        lblResponse.text = ""
        if npPin.count == 4  {
                DispatchQueue.main.async {
                    self.lblResponse.text = ""
                    
                    let nPin = "\(self.user?.mobile ?? "")\(npPin)".sha256() as? String ?? ""
                    
                    if self.user?.appPin == nPin {
                        print("Successfull")
                        self.delegate?.successfullPin()
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        self.lblResponse.text = "Please enter correct pin."
                    }
                    
                  
                }
        }
        else if npPin.count < 4 {
            self.lblResponse.text = "Please Enter 4 digits pin "
        }
        

    }
    
    

}
extension CheckMerchantPinViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if npFirst.isFirstResponder || npSecond.isFirstResponder || npThird.isFirstResponder || npFourth.isFirstResponder {
            
            if ((textField.text?.count)! <= 1  && string.count > 0){
                if(textField == npFirst)
                {
                    npSecond.becomeFirstResponder()
                }
                if(textField == npSecond)
                {
                    npThird.becomeFirstResponder()
                }
                if(textField == npThird)
                {
                    npFourth.becomeFirstResponder()
                }
                
                textField.text = string
                return false
            }
            else if ((textField.text?.count)! >= 1  && string.count == 0){
                // on deleting value from Textfield
                if(textField == npSecond)
                {
                    npFirst.becomeFirstResponder()
                }
                if(textField == npThird)
                {
                    npSecond.becomeFirstResponder()
                }
                if(textField == npFourth)
                {
                    npThird.becomeFirstResponder()
                }
                textField.text = ""
                return false
            }
            else if ((textField.text?.count)! >= 1  ) {
                textField.text = string
                return false
            }
        }
        return true
        
        
    }
}
