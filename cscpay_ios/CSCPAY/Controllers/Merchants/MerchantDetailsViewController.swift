//
//  MerchantDetailsViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 25/02/21.
//

import UIKit

class MerchantDetailsViewController: UIViewController {
    @IBOutlet weak var generalButton: UIButton!
    @IBOutlet weak var addressButton: UIButton!
    @IBOutlet weak var panButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var generalView: UIView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var panView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.embed(getMerchantGeneralDetailsViewController(), inView: self.containerView)
            self.generalButton.tag = 1
        }
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func generalDetailButtonTapped(_ sender: Any) {
        if self.generalButton.tag == 0 {
            self.generalButton.tag = 1
            self.addressButton.tag = 0
            self.panButton.tag = 0
        } else {
            return
        }
        generalView.backgroundColor = .black
        addressView.backgroundColor = .clear
        panView.backgroundColor = .clear
        
        //embed(getMakeRequestViewController(), inView: self.containerView)
    }
    @IBAction func addressDetailButtonTapped(_ sender: Any) {
        if self.addressButton.tag == 0 {
            self.generalButton.tag = 0
            self.addressButton.tag = 1
            self.panButton.tag = 0
        } else {
            return
        }
        generalView.backgroundColor = .clear
        addressView.backgroundColor = .black
        panView.backgroundColor = .clear
    }
    @IBAction func panAndBankButtonTapped(_ sender: Any) {
        if self.panButton.tag == 0 {
            self.generalButton.tag = 0
            self.addressButton.tag = 0
            self.panButton.tag = 1
        } else {
            return
        }
        generalView.backgroundColor = .clear
        addressView.backgroundColor = .clear
        panView.backgroundColor = .black
    }
}
