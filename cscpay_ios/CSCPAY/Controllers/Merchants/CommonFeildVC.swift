//
//  CommonFeildVC.swift
//  CSCPAY
//
//  Created by Aman Pandey on 29/11/21.
//

import UIKit


enum TypeBusiness{
    case businessCat, businessType , state , district , docType
}



class CommonFeildVC: UIViewController {

    @IBOutlet weak var headLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var type : TypeBusiness = .businessType
    var businessCatego : [BusinessCategory] = []
    var businessType : [BusinessType] = []
    var typeBusiness : ((String? , String? , Int?) -> ())?
    var catBusiness : ((String?) -> ())?
    
    var natureOfBusiness : BusinessN?
    var typeOfBusiness : BusinessT?
    
    var legalStateList : StatesResponse?
    var legalDistrictList : [DistrictResponse]?
    var docTypeUpload : CompanyDocModel?
    var docType = ["Aadhaar Card","Driving License","Passport No","Voter ID"]
    
    var selectedStateIndex : Int!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDataForMerchantBusiness()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUi(type: type)
    }
    

    func setUi(type: TypeBusiness = .businessType){
        switch type{
        case .businessType:
            self.headLabel.text = "Choose Business Type"
            self.descriptionLabel.text = "Please enter your basic details to connect your business with your bank"
        case .district:
            self.headLabel.text = "Select your district"
            self.descriptionLabel.text = "Please select your district name"
        case .state:
            self.headLabel.text = "Select your state"
            self.descriptionLabel.text = "Please select your state name"
        case .businessCat:
            self.headLabel.text = "Choose Business Nature"
            self.descriptionLabel.text = "Please enter your business nature to connect your business with your bank"
        case .docType:
            self.headLabel.text = "Choose Document type"
            self.descriptionLabel.text = "Please select your document type"
        }
    }
    
    
    
    func setDataForMerchantBusiness(){
        natureOfBusiness =  APIManager.loadAllJson(BusinessN.self, filename: "BusinessNature")
        typeOfBusiness = APIManager.loadAllJson(BusinessT.self, filename: "BusinessType")
        legalStateList = APIManager.loadAllJson(StatesResponse.self, filename: "StateDis")
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
  
}

extension CommonFeildVC : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch type{
        case .businessType:
            return typeOfBusiness!.count
        case .businessCat:
            return natureOfBusiness!.count
        case .state:
            return legalStateList?.states?.count ?? 1
        case .district:
            return legalStateList?.states?[selectedStateIndex].districts?.count ?? 1
        case .docType:
            return docType.count
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommonFieldsCell", for: indexPath) as? CommonFieldsCell else { return UITableViewCell()}
        switch type{
        case .businessType:
            cell.optionLabel.text = typeOfBusiness![indexPath.row].name
        case .state:
            cell.optionLabel.text = legalStateList!.states?[indexPath.row].stateName
        case .district:
            cell.optionLabel.text = legalStateList!.states?[selectedStateIndex].districts![indexPath.row].districtName
        case .docType:
            cell.optionLabel.text = docType[indexPath.row]
        default:
            cell.optionLabel.text = natureOfBusiness![indexPath.row].mcName
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch type{
        case .businessType:
            typeBusiness!(typeOfBusiness![indexPath.row].code , typeOfBusiness![indexPath.row].name, 0)
        case .businessCat:
            catBusiness!(natureOfBusiness![indexPath.row].mcName)
            typeBusiness!(natureOfBusiness![indexPath.row].mcCode , natureOfBusiness![indexPath.row].mcName, 0)
        case .state:
            typeBusiness!(legalStateList!.states![indexPath.row].stateCode , legalStateList!.states![indexPath.row].stateName, indexPath.row)
        case .district:
            typeBusiness!(legalStateList!.states![selectedStateIndex].districts![indexPath.row].districtCode, legalStateList!.states![selectedStateIndex].districts![indexPath.row].districtName , 0)
        case .docType:
            catBusiness!(docType[indexPath.row])
        default:
            catBusiness!(businessCatego[indexPath.row].businessCategoryName)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
}
