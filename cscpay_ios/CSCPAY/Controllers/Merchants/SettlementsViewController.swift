//
//  SettlementsViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 19/03/21.
//

import UIKit

class SettlementsViewController: UIViewController {

    @IBOutlet weak var todaySettlementAmountLabel: UILabel!
    @IBOutlet weak var totalSettlementAmountLabel: UILabel!
    @IBOutlet weak var selectDateButton: UIButton!
    @IBOutlet weak var printerButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func selectDateButtonTapped(_ sender: Any) {
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension SettlementsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier : String!
    reuseIdentifier = String(format: "settlementCell")

        let historyCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath) as? settlementCell
        

        return historyCell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        notImplementedYetAlert(base: self)
//        let vc = getTransactionDetialsViewController() as! TransactionDetialsViewController
//        self.present(vc, animated: true, completion: nil)
    }
    
}
