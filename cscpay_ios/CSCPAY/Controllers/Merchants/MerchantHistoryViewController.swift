//
//  MerchantHistoryViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 28/06/21.
//

import UIKit

class MerchantHistoryViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var printerBtn: UIButton!
    
    var transactions = [Transactions]()
    override func viewDidLoad() {
        super.viewDidLoad()
        if let recent = CoredataManager.getALLDetails(Transactions.self) {
            self.transactions = recent
            self.tableView.reloadData()
        }
    }
    @IBAction func printerBtnTapped(_ sender: Any) {
        notImplementedYetAlert(base: self)
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension MerchantHistoryViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        //if(section == 0) {
            //return nil //As you do not have to display header in 0th section, return nil
            let reuseIdentifier : String!
        reuseIdentifier = String(format: "FAQHeaderCell")

            let headerView = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath)

            return headerView
        //}
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if self.transactions.count == 0 || Constants.historyEmptyTest {
            return 0
        }
        return 25
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.transactions.count == 0 || Constants.historyEmptyTest {
            //self.ViewHeader.isHidden = true
            tableView.setEmptyMessage(UIImage(named: "historyNotFound")!)
            return 0
        } else {
            tableView.restore()
            //self.ViewHeader.isHidden = false
            return self.transactions.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier : String!
    reuseIdentifier = String(format: "MerchantTransactionHistoryCell") // historyCell

        let historyCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath) as? MerchantTransactionHistoryCell
        let item = self.transactions[indexPath.row]
        let isSuccess = item.status == "success" ? true : false
        let isPaid       = item.type == "P" ? true : false
        historyCell?.setupValue(isPaid: isPaid, isSuccess : isSuccess, person: item.toName ?? "Saloni Rao", date: "\(String(describing: item.addedAt ?? Date()))", amount: "\(item.amount)", img: UIImage.init(named: "iaxis")!) // // "05 Sep 2020, 02:05pm"
        /*
        if indexPath.row % 2 == 0 {
            if isPaid {
                historyCell?.setupValue(person: "Saloni Rao", date: "05 Sep 2020, 02:05pm", amount: "250", img: UIImage.init(named: "iaxis")!)
            } else if isReceived {
                historyCell?.setupValue(isPaid:  false,isSuccess: false, person: "Gowtham", date: "05 Sep 2020, 02:05pm", amount: "180", img: UIImage.init(named: "iaxis")!)
            } else {
                    historyCell?.setupValue(person: "Saloni Rao", date: "05 Sep 2020, 02:05pm", amount: "250", img: UIImage.init(named: "iaxis")!)}
        } else {
            if isPaid {
                historyCell?.setupValue(person: "Saloni Rao", date: "05 Sep 2020, 02:05pm", amount: "250", img: UIImage.init(named: "iaxis")!)
            } else if isReceived {
                historyCell?.setupValue(isPaid:  false, person: "Saloni Rao", date: "05 Sep 2020, 02:05pm", amount: "250", img: UIImage.init(named: "iaxis")!)
            } else {

                    historyCell?.setupValue(isPaid:  false,person: "Gowtham", date: "05 Sep 2020, 02:05pm", amount: "180", img: UIImage.init(named: "iaxis")!)
            }
        } */
        if indexPath.row == 3 {
            historyCell?.viewSeperator.backgroundColor = .clear
        }
        return historyCell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = getTransactionDetialsViewController() as! TransactionDetialsViewController
        vc.transaction = self.transactions[indexPath.row]
        vc.isMerchant = true
        vc.titleStr   = "Customer Payment Log"
//        if indexPath.row % 2 == 0 {
//            vc.isFailed = false
//        } else {
//            vc.isFailed = true
//        }
        
        
        self.present(vc, animated: true, completion: nil)
    }
    // MARK:- Delete cells
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
    
}
