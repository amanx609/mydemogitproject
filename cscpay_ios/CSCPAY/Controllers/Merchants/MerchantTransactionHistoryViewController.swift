//
//  MerchantTransactionHistoryViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 25/02/21.
//

import UIKit

class MerchantTransactionHistoryViewController: UIViewController {

    @IBOutlet weak var totalTransactionAmountLabel: UILabel!
    @IBOutlet weak var todayTransactionAmountLabel: UILabel!
    @IBOutlet weak var selectDateButton: UIButton!
    @IBOutlet weak var printerButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
    }

    @IBAction func selectDateButtonTapped(_ sender: Any) {
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension MerchantTransactionHistoryViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier : String!
    reuseIdentifier = String(format: "MerchantHistoryCell")

        let historyCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath) as? MerchantHistoryCell
        

        return historyCell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = getTransactionDetialsViewController() as! TransactionDetialsViewController
        self.present(vc, animated: true, completion: nil)
    }
    
}
