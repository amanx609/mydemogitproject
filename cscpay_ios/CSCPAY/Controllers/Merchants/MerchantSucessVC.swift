//
//  MerchantSucessVC.swift
//  CSCPAY
//
//  Created by Aman Pandey on 08/12/21.
//

import UIKit

enum SucessFrom{
    case Summary , Pending , splash
}


class MerchantSucessVC: UIViewController {
    
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var msgString : String?
    var merchantRequestModel : CommonModel?
    var companyUploadDoc : CompanyDocModel?
    var merchantRegResponse : MerchantRegistrationModel?
    
    
    var showSucessFrom : SucessFrom?
    override func viewDidLoad() {
        super.viewDidLoad()

        switch showSucessFrom{
        case .Summary:
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                print(self.merchantRegResponse)
                let vc = getMerchantPendingViewController() as! PendingDocumentsVC
//                vc.merchantRequestModel = self.merchantRequestModel
                vc.merchantRegistrationResponse = self.merchantRegResponse
                self.present(vc, animated: true, completion: nil)
            }
        case .splash:
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                if self.merchantRegResponse?.body.activeStatus == "30"{
                    let vc = getMerchantHomeViewController() as! MerchantHomeViewController
                    vc.merchantRegResponse = self.merchantRegResponse
                    self.present(vc, animated: true, completion: nil)
                }else if self.merchantRegResponse?.body.activeStatus == "10"{
                    let vc = getMerchantPendingViewController() as! PendingDocumentsVC
                    vc.merchantRegistrationResponse = self.merchantRegResponse
                    self.present(vc, animated: true, completion: nil)
                }
            }
        default:
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
//                self.present(getMerchantHomeViewController(), animated: true, completion: nil)
//            }
            print("")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let success = UIImage.gifImageWithName("merchantSucess")
        self.imageView.image = success
        self.msgLabel.text = msgString
    }
}
