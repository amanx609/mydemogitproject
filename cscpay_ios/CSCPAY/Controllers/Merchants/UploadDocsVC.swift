//
//  UploadDocsVC.swift
//  CSCPAY
//
//  Created by Aman Pandey on 07/12/21.
//

import UIKit
import Vision
import VisionKit


protocol UploadedDocumentFromUploadDocVC{
    func uploadedDocument(index: Int , isSelect : Bool)
}


class UploadDocsVC: UIViewController , VNDocumentCameraViewControllerDelegate{

    @IBOutlet weak var imageClearInfoLabel: UILabel!
    @IBOutlet weak var docTypeLabel: UILabel!
    @IBOutlet weak var docImageview: UIImageView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var selectTypeOfDocButton: UIButton!
    @IBOutlet weak var typeOfDocView: UIView!
    @IBOutlet weak var captureView: UIView!
    @IBOutlet weak var dropDownShowView: UIView!
    
    var selectedDocToUpload : Int!
    var uploadedDocumentCount : Int!
    var imagePicker = UIImagePickerController()
    var delegate : UploadedDocumentFromUploadDocVC?
    var merchantRequestRespose : CommonModel?
    var merchantRegistrationResponse : MerchantRegistrationModel?
    var imageData : Data?
    let dropDown = MakeDropDown()
    var dropDownRowHeight: CGFloat = 50
    
    var docReqArr : Doc?
    var documentName : String?

    var documentsRequiredArray : [String] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        merchantRequestRespose = CommonModel()
        dropDown.makeDropDownDataSourceProtocol = self
    }

    override func viewDidAppear(_ animated: Bool) {
        
        setUpDropDown()
    }
    
    @IBAction func backButonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func captureImageButtonTapped(_ sender: UIButton) {
        
        
//        self.getImage(fromSourceType: .camera)
        
//        showAlert()
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let scannerViewController = VNDocumentCameraViewController()
            scannerViewController.delegate = self
            present(scannerViewController, animated: true)
        }else{
            let alertController = UIAlertController(title: "Camera", message: "No camera available", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (_) in }
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func submitbuttonTapped(_ sender: UIButton) {
        guard documentName != nil else {
            showAlert(msg: "Please select document name")
            return
        }
        uploadDocumentService(imageString: imageData)
    }
    
    
    func showAlert(msg: String) {
        DispatchQueue.main.async {
            self.imageClearInfoLabel.text = msg
            self.imageClearInfoLabel.textColor = .red
        }
    }
    
    
    @IBAction func typeOfDocButtonTapped(_ sender: UIButton) {
        self.dropDown.showDropDown(height: self.dropDownRowHeight * CGFloat(documentsRequiredArray.count))
    }
    
    
    
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
//        guard scan.pageCount <= 1 else {
//            controller.dismiss(animated: true) {
//                self.presentAlert(title: "Error", msg: "Only one image at a time extra images")
//            }
//            return
//        }
        DispatchQueue.main.async {
            for i in 0 ..< scan.pageCount {
                if let image = scan.imageOfPage(at: i).jpegData(compressionQuality: 0.1){
//                    if let img = UIImage(data: image){
//                        let pdfPage = PDFPage(image: img)
//                        pdfDocument.insert(pdfPage!, at: i)
//                        self.showSaveDialog(scannedImage: img , index: i, doc: pdfDocument)
//                    }
                    self.imageData = image
                    self.docImageview.image = UIImage(data: image)
                }
            }
        }
        controller.dismiss(animated: true)
    }
}

extension UploadDocsVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func showAlert() {
        let alert = UIAlertController(title: "", message: Constants.alert_choose_image, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: Constants.alert_open_gallery, style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: Constants.alert_capture_image, style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: Constants.alert_remove_image, style: .default, handler: { (action: UIAlertAction) in
            self.docImageview.image = UIImage(named: "avatarPlaceholder")
//            UD.SharedManager.removeValue(key: Constants.User_Image)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            if sourceType == .camera {
                imagePickerController.cameraDevice = UIImagePickerController.CameraDevice.rear }
            imagePickerController.allowsEditing = true
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.editedImage.rawValue)] as! UIImage
        let resizeImg = image.jpegData(compressionQuality: 0.7)
        let afterResizeImg = image.resize(to: docImageview.frame.size)
        self.docImageview.image = UIImage(data: resizeImg!)
        self.imageData = resizeImg
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension UploadDocsVC{
    
    func uploadDocumentService(imageString : Data?){
        self.showActivityIndicator()
        
//        merchantRequestRespose?.docSize = imageString?.count
//        merchantRequestRespose?.docData = imageString?.base64EncodedString()
//        merchantRequestRespose?.docName = docTypeLabel.text
//        merchantRequestRespose?.docMime = "image/jpeg"
//        merchantRequestRespose?.docType = docReqArr?.docType?.rawValue
//        merchantRequestRespose?.docId   = uploadedDocumentCount
//        merchantRequestRespose?.mobileNo = merchantRegistrationResponse?.body.mobileNo
//        merchantRequestRespose?.seqKey = merchantRegistrationResponse?.body.seqKey
//        merchantRequestRespose?.bizId = merchantRegistrationResponse?.body.bizId
//        if let bizHash = UD.SharedManager.getValues(key: "bizHash", type: .string) as? String{
//            merchantRequestRespose?.bizHash = bizHash
//        }
        
        
        merchantRegistrationResponse?.body.docSize =  imageString?.count
        merchantRegistrationResponse?.body.docData =  imageString?.base64EncodedString()
        merchantRegistrationResponse?.body.docName =  docTypeLabel.text
        merchantRegistrationResponse?.body.docMime =  "image/jpeg"
        merchantRegistrationResponse?.body.docType =  docReqArr?.docType?.rawValue
        merchantRegistrationResponse?.body.docId   =  uploadedDocumentCount
        
        var actStatus = UD.SharedManager.getValues(key: "ActiveStatus", type: .int) as! Int
        
        guard let request = merchantRegistrationResponse else {
            return
        }
        print(request)
        APIManager.getMerchant(MerchantRegistrationModel.self, reqAction: "docUpload", urlString: "merchant/register", requestModel: nil, merchantRegresponse: request) { res, err, text in
            self.hideActivityIndicator()
            print(res)
            print(err)
            
            DispatchQueue.main.async {
                if err == nil{
                    actStatus += 1
                    UD.SharedManager.saveToUserDefaults(key: "ActiveStatus", value: actStatus)
                    self.delegate?.uploadedDocument(index: self.selectedDocToUpload, isSelect: true)
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
}


extension UploadDocsVC: MakeDropDownDataSourceProtocol{
   
    func getDataToDropDown(cell: UITableViewCell, indexPos: Int, makeDropDownIdentifier: String) {
        if makeDropDownIdentifier == "DROP_DOWN_NEW"{
            let customCell = cell as! DropDownCell
            customCell.monthLabel.text = self.documentsRequiredArray[indexPos]
        }
    }
    
    func numberOfRows(makeDropDownIdentifier: String) -> Int {
        return self.documentsRequiredArray.count
    }
    
    func selectItemInDropDown(indexPos: Int, makeDropDownIdentifier: String) {
        let text = documentsRequiredArray[indexPos]
        
        self.docTypeLabel.text = text
        self.imageClearInfoLabel.textColor = .labelDescColor
        self.imageClearInfoLabel.text = "Make sure you capture a clear front view"
        self.documentName = text
        self.dropDown.hideDropDown()
    }
    
    
    
    
    
    func setUpDropDown(){
        dropDown.makeDropDownIdentifier = "DROP_DOWN_NEW"
        dropDown.cellReusableIdentifier = "dropDownCell"
        dropDown.makeDropDownDataSourceProtocol = self
        dropDown.setUpDropDown(viewPositionReference: docTypeLabel.frame, offset: 0)
        dropDown.nib = UINib(nibName: "DropDownCell", bundle: nil)
        dropDown.setRowHeight(height: self.dropDownRowHeight)
        
        
        self.dropDownShowView.addSubview(dropDown)
//        self.buttonStackView.addSubview(dropDown)
    }
    
    
    
    
    
}
    
    
    
    
    
    

