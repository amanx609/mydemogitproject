//
//  SettlementViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 28/06/21.
//

import UIKit

class SettlementViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewHeader: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func printerBtnTapped(_ sender: Any) {
        notImplementedYetAlert(base: self)
    }
    
}
extension SettlementViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier : String!
    reuseIdentifier = String(format: "TransactionHistoryCell")

        let historyCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath) as? TransactionHistoryCell
        

        return historyCell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        notImplementedYetAlert(base: self)
//        let vc = getTransactionDetialsViewController() as! TransactionDetialsViewController
//        self.present(vc, animated: true, completion: nil)
    }
    
}
