//
//  TransactionDetialsViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 22/02/21.
//

import UIKit


enum fromScreen{
    case isFailed , isFromReceive , isMerchant , isHistory , isPaid
}


class TransactionDetialsViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var viewDetail: UITextView!
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backButtonImg: UIImageView!
    @IBOutlet weak var lblRefId: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    //@IBOutlet weak var lblFrom: UILabel!
   // @IBOutlet weak var lblFromUpi: UILabel!
   // @IBOutlet weak var lblTo: UILabel!
   // @IBOutlet weak var lblToUpi: UILabel!
    @IBOutlet weak var lblUpiId: UILabel!
    @IBOutlet weak var btnProceed: UIButton!
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var lblRef: UILabel!
    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var viewDetails: UIView!
    @IBOutlet weak var imgEmoji: UIImageView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var viewAmount: UIView!
    @IBOutlet weak var viewRefId: UIView!
    @IBOutlet weak var noteViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblRequestTitle: UILabel!
    
    @IBOutlet weak var viewRepeatView: UIView!
    @IBOutlet weak var shareButtonView: UIStackView!
    
    @IBOutlet weak var disputeView: UIView!
    
    
    var transaction : Transactions?
    //when from History
    var transactionDetails : TransactionHistoryDetails?
    //When from Pay
    var paymentDetails : PaymentResponse?
    
    //Common Transaction Details
    var transDetails : Transaction_History?
    
    var isFailed = false
    var isFromReceive = false
    var isMerchant = false
    var isHistory = false
    var titleStr : String?
    var isFrom : fromScreen = .isPaid
    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular,text: Language.getText(with: LanguageConstants.lbl_transaction_detail_title))
        lblName.setup(fontSize: 22, fontType: .semiBold, textColor: .titleColor, bgColor: .clear)
        lblMobile.setup(fontType: .semiBold)
        lblAmount.setup(fontSize: 46, fontType: .bold, textColor: .customGreen)
        lblRef.setup(fontSize: 11, fontType: .regular, textColor: .subtitleColor, text:  Language.getText(with: LanguageConstants.lbl_transaction_ref_id))
        lblRefId.setup(fontSize: 15)
        lblTime.setup(fontSize: 12, textColor: .darkGray)
        //lblFrom.setup(fontSize: 14, textColor: .customBlack)
        //lblFromUpi.setup(fontSize: 12, textColor: .subtitleColor)
        //lblTo.setup(fontSize: 14, textColor: .customBlack)
        //lblToUpi.setup(fontSize: 12, textColor: .subtitleColor)
        lblId.setup(fontSize: 11, fontType: .regular, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_transaction_id))
        lblUpiId.setup(fontSize: 15)
        btnProceed.setup(fontSize: 16, fontType: .semiBold, textColor: .customWhite, bgColor: .taleColor, text: Language.getText(with: LanguageConstants.btn_repeat_transacation))
        btnHome.setup(fontSize: 15, fontType: .semiBold, textColor: .taleColor, bgColor: .clear, text: Language.getText(with: LanguageConstants.btn_back_to_home))
        let raiseDispute = UITapGestureRecognizer(target: self, action: #selector(tapOnRaiseDispute))
        disputeView.addGestureRecognizer(raiseDispute)
        let dismissView = UITapGestureRecognizer(target: self, action: #selector(tapViewWhenTappedAround))
        view.addGestureRecognizer(dismissView)
        if isFromReceive {
            self.btnProceed.setTitle("New Request", for: .normal)
            self.viewRefId.isHidden = true
            self.lblRequestTitle.text = "You have requested from"
        }
        if isMerchant {
            self.shareButtonView.isHidden = false
            self.viewRepeatView.isHidden  = true
        } else {
            self.shareButtonView.isHidden = true
            self.viewRepeatView.isHidden  = false
        }
        if let title = self.titleStr {
            self.lblTitle.text = title
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        if let trans = self.transaction {
            self.lblName.text = trans.toName
            self.lblMobile.text = trans.to
            self.lblAmount.text = "\(trans.amount)"
            //self.lblFrom.text = "Gowtham"
            //self.lblTo.text = trans.toName
            //self.lblToUpi.text = trans.to
            self.lblUpiId.text = "\(trans.id)"
            self.lblRefId.text = "\(trans.id)"
            let date = trans.addedAt?.getCurrentDate(format: "dd MMM yyyy, hh:mma")
            self.lblTime.text = date?.0 ?? ""
            if trans.remark?.count ?? 0 > 0 {
                self.viewDetail.text = trans.remark
                self.borderView.isHidden = false
                self.noteViewHeightConstraint.isActive = false
            } else {
                self.viewDetail.frame = CGRect.init(x: 0, y: 0, width: 0, height: 0)
            }
            
            if let img = UIImage.init(named: "\(trans.emoji ?? "")") {
                self.imgEmoji.image = img
                self.imgEmoji.isHidden = false
            }
        }
        if isFailed {
            //self.statusView.backgroundColor = hexStringToUIColor(hex: "FFF1F0")
            //statusLabel.text = "Failed"
            //statusLabel.textColor = hexStringToUIColor(hex: "EA1601")
            //let fail = UIImage.gifImageWithName("failiure")
            statusImageView.image = UIImage.init(named: "failiure")
            self.viewAmount.backgroundColor = hexStringToUIColor(hex: "FFFAFA")
            self.viewRefId.backgroundColor = hexStringToUIColor(hex: "FFFAFA")
            self.lblAmount.textColor = .customRed
            //statusImageView.tintColor = hexStringToUIColor(hex: "EA1601")
            //amountLabel.textColor = hexStringToUIColor(hex: "EA1601")
        } else {
            //let success = UIImage.gifImageWithName("success1")
            statusImageView.image = UIImage.init(named: "success1")
        }
        setValues()

        
    }
    @IBAction func homeButtonTapped(_ sender: Any) {
        self.present(getHomeViewController(), animated: true, completion: nil)
    }
    @IBAction func shareButtonTapped(_ sender: Any) {
        let img = self.viewDetails.takeScreenshot()
        shareQRCode(img: img)
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func moreButtonTapped(_ sender: Any){
        UIView.animate(withDuration: 1.0, delay: 0.4, options: .curveEaseOut, animations: {
            self.disputeView.alpha = 1.0
            self.disputeView.isHidden = false
        }, completion: nil)
    }
    

    
    
    @IBAction func repearTransactionButtonTapped(_ sender: Any) {
        if isFromReceive {
            let vc = getPSPReceiveConfirmViewController() as! PSPReceiveConfirmViewController
            vc.item = self.transaction
            vc.newTransaction = transactionDetails
            self.present(vc, animated: true, completion: nil)
        } else {
            let vc = getPayConfirmViewController() as! PayConfirmViewController
            vc.item = self.transaction
            vc.repeatTransaction = transactionDetails
            self.present(vc, animated: true, completion: nil)
        }
    }
    private func shareQRCode(img :UIImage?) {
        if let image = img {
            let vc = UIActivityViewController(activityItems: [image], applicationActivities: [])
            present(vc, animated: true)
        }
    }
    @objc func tapOnRaiseDispute(_ gesture : UITapGestureRecognizer){
        present(getTicketsViewController(), animated: true, completion: nil)
    }


    @objc func tapViewWhenTappedAround(_ gesture : UITapGestureRecognizer){
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseInOut, animations: {
            self.disputeView.alpha = 0.0
            self.disputeView.isHidden = true
        }, completion: nil)
    }
}

extension TransactionDetialsViewController{
    
    func setValues(){
        switch isFrom{
        case .isPaid: 
            self.backButtonImg.isHidden = true
            self.backButton.isUserInteractionEnabled = false
        case .isHistory:
          setValueHistory()
        case .isFailed:
           if isFailed {
                //self.statusView.backgroundColor = hexStringToUIColor(hex: "FFF1F0")
                //statusLabel.text = "Failed"
                //statusLabel.textColor = hexStringToUIColor(hex: "EA1601")
                //let fail = UIImage.gifImageWithName("failiure")
                statusImageView.image = UIImage.init(named: "failiure")
                self.viewAmount.backgroundColor = hexStringToUIColor(hex: "FFFAFA")
                self.viewRefId.backgroundColor = hexStringToUIColor(hex: "FFFAFA")
                self.lblAmount.textColor = .customRed
                //statusImageView.tintColor = hexStringToUIColor(hex: "EA1601")
                //amountLabel.textColor = hexStringToUIColor(hex: "EA1601")
            } else {
                //let success = UIImage.gifImageWithName("success1")
                statusImageView.image = UIImage.init(named: "success1")
            }
        case .isMerchant:
            if isMerchant {
                self.shareButtonView.isHidden = false
                self.viewRepeatView.isHidden  = true
            } else {
                self.shareButtonView.isHidden = true
                self.viewRepeatView.isHidden  = false
            }
        case .isFromReceive:
            self.btnProceed.setTitle("New Request", for: .normal)
            self.viewRefId.isHidden = true
            self.lblRequestTitle.text = "You have requested from"
            print("isFromReceive")
        }
    }
    
    func setValueHistory(){
        if let transaction = transDetails{
            let status = transaction.txnStatus
            if transaction.drCrFlag == "C"{
                lblRequestTitle.text = "Received from"
            }else{
                lblRequestTitle.text = "Paid to"
            }

            if status == "SUCCESS"{
                statusImageView.image = UIImage.init(named: "success1")
            }else if status == "PENDING"{
                statusImageView.image = UIImage.init(named: "failiure")
                self.viewAmount.backgroundColor = hexStringToUIColor(hex: "FFFAFA")
                self.viewRefId.backgroundColor = hexStringToUIColor(hex: "FFFAFA")
                self.lblAmount.textColor = .customRed
            }else{
                statusImageView.image = UIImage.init(named: "failiure")
                self.viewAmount.backgroundColor = hexStringToUIColor(hex: "FFFAFA")
                self.viewRefId.backgroundColor = hexStringToUIColor(hex: "FFFAFA")
                self.lblAmount.textColor = .customRed
            }
            
            lblName.text = transaction.payerName
            lblMobile.text = transaction.payeeVirtualAddress
            lblAmount.text = "\(Language.getText(with: LanguageConstants.lbl_currency) ?? Constants.lbl_currency) \(transaction.txnAmount ?? "")"
            lblTime.text = transaction.trnDate
            lblRefId.text = transaction.custRefNo
            lblUpiId.text = "\(transaction.trnRefNo ?? 1)"
        }
    }
    
}
