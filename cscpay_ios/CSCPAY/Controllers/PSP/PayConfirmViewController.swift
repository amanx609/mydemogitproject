//
//  PayConfirmViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 30/03/21.
//

import UIKit

class PayConfirmViewController: UIViewController, RadioButtonDelegate {
  
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameTagLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileUpiLabel: UILabel!
    @IBOutlet weak var noteView: UITextView!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var paymentDropdownView: UIView!
    @IBOutlet weak var emojiView: UICollectionView!
    @IBOutlet weak var lblPayFrom: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var imgEmoji: UIImageView!
    @IBOutlet weak var viewBankList: UIView!
    //@IBOutlet weak var lblEmoji: UILabel!
    @IBOutlet weak var paymentModeDropdown: DropDownTextField!
    @IBOutlet weak var bankListTableView: UITableView!
    @IBOutlet weak var bankMaskedAccountLabel: UILabel!
    @IBOutlet weak var bankNameLabel: UILabel!
    @IBOutlet weak var bankIcon: UIImageView!
//    @IBOutlet weak var bankNameLabel: UILabel!
    @IBOutlet weak var moreBankListButton: UIButton!
    @IBOutlet weak var bankName: UILabel!
    
    
    var selectedBankAccountList : [Account_List]?
    var isFromRequest : Bool = false
    var isFromPendingRequest : Bool = false
    var titleStr = "Pay"
    var btnTitle = "PROCEED TO PAY"
    var isFromContacts : Bool = false
    var isReceive = false
    
    //Values
    var name: String?
    var mobile: String?
    var upiId : String?
    var extra : [String]?
    var item : Transactions?
    var updateMoney = false
    var repeatTransaction : TransactionHistoryDetails?
    var selectedIndex:IndexPath?
    
    
    
    
    
    
    var payeeType = [String:Any]()
    var payerType = [String:Any]()
    
    let guestArray : [String]? = ["Pay", "Request"]
    let emojis = ["e1", "e2", "e3", "e4", "e5", "e6" ]
    var currentEmojis = ""
    
    func setupValues() {
        if let names = name {
            self.nameLabel.text = names }
        if let mob = mobile {
            self.mobileUpiLabel.text = mob }
        guard let bankList = CoredataManager.getDetails(Account_List.self)else{
            return
        }
        self.selectedBankAccountList = bankList
        if let primary = selectedBankAccountList?.filter({$0.isPrimary == "T"}){
         //   bankNameLabel.text = primary[0].bankCode
            bankIcon.image = UIImage(named: primary[0].bankCode ?? "AXIS")
            bankMaskedAccountLabel.text = primary[0].maskedAccountNumber
            let BName = CoredataManager.getDetails(Master_BankList.self)
            let name = BName?.filter({$0.bankCode == primary[0].bankCode})
            bankName.text = name?[0].bankName
        }
        
    }
    
    func fromBankToPay(selectedBankName: String, maskedAccount: String? = nil, icon: String? = nil, type: String? = nil){
        bankIcon.image = UIImage(named: icon ?? "AXIS")
        bankMaskedAccountLabel.text = maskedAccount
        let BName = CoredataManager.getDetails(Master_BankList.self)
        let name = BName?.filter({$0.bankCode == selectedBankName})
        bankName.text =  name?[0].bankName
    }
    
    
    func setupUI() {
        titleLabel.setup(fontSize: 24, text: titleStr)
        nameLabel.setup(textColor: .customBlack)
        mobileUpiLabel.setup(fontSize: 15, fontType: .semiBold, textColor: .customBlue)
        seperatorView.backgroundColor = .seperatorColor
        
        amountField.setup(fontSize: 24, fontType: .bold, placeholderText: Language.getText(with: LanguageConstants.txt_amount_placeholder) ?? Constants.txt_amount_placeholder)
        noteView.setup(fontSize: 15, fontType: .regular, textColor: .subtitleColor, bgColor: .customWhite, text: "Make a note")
        lblPayFrom.setup(fontSize: 15, fontType: .regular, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_pay_from) ?? Constants.lbl_pay_from)
        proceedButton.setup(fontType: .semiBold, textColor: .customWhite, bgColor: .taleColor, text:btnTitle)
     /*
        if let to = item {
            self.nameLabel.text = to.toName
            self.mobileUpiLabel.text = to.to
            if updateMoney {
                self.amountField.text = "\(to.amount)"
                if let note = to.remark {
                    if note.count != 0 {
                        self.noteView.text = note
                    }
                }
                if let img = UIImage.init(named: "\(to.emoji ?? "")") {
                    self.imgEmoji.image = img
                    self.imgEmoji.isHidden = false
                }
            }
        }
        */
        if let to = repeatTransaction {
            self.nameLabel.text = to.payeeName
            self.mobileUpiLabel.text = to.payeeVirtualAddress
            if updateMoney {
                self.amountField.text = "\(to.txnAmount ?? "")"
                if let note = to.txnNote {
                    if note.count != 0 {
                        self.noteView.text = note
                    }
                }
//                if let img = UIImage.init(named: "\(to.emoji ?? "")") {
//                    self.imgEmoji.image = img
//                    self.imgEmoji.isHidden = false
//                }
            }
        }
        
        
        
        
        
        if isReceive {
            self.titleLabel.text = "Receive"
            self.lblPayFrom.text = "Receive In"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupUI()
        setupValues()
        //paymentModeDropdown.dropDownDelegate = self
        //updateDropdown(index : 0)
        //self.titleLabel.text = titleStr
        //self.proceedButton.setTitle(btnTitle, for: .normal)
        //self.noteView.textColor = UIColor.lightGray
        
        DispatchQueue.main.async {
            if self.isFromPendingRequest {
                self.amountField.text = "200"
                self.amountField.isUserInteractionEnabled = false
                self.noteView.isUserInteractionEnabled = false
            } else if self.isFromRequest {
                //self.titleLabel.text = self.titleStr //"Request"
                //self.proceedButton.setTitle(self.btnTitle, for: .normal)
            } else if self.isFromContacts {
                self.paymentDropdownView.isHidden = false
            }
        }
    } /*
    func updateDropdown(index : Int){
            self.paymentModeDropdown.addDropDownasInput(withArray: guestArray ?? [])
            if let ga = guestArray, ga.count > 0 {
                self.paymentModeDropdown.text = ga[index]
            }
        
    } */
    override func viewDidLayoutSubviews() {
        //noteView.frame.size.width = noteView.intrinsicContentSize.width
    }
    
    @IBAction func okButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.alertView.isHidden = true
            self.present(getSuccessViewController(), animated: true, completion: nil)
        }
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func proceedButtonTapped(_ sender: Any) {
        guard let amount = amountField.text else {
            DispatchQueue.main.async {
                self.responseLabel.text = Language.getText(with: LanguageConstants.alert_enter_amount) ?? Constants.alert_enter_amount
            }
            return
        }
        guard amount.count > 0 else {
            DispatchQueue.main.async {
                self.responseLabel.text = Language.getText(with: LanguageConstants.alert_enter_valid_amount) ??  Constants.alert_enter_valid_amount
            }
            return
        }
        
        if isFromRequest {
            DispatchQueue.main.async {
                self.alertView.isHidden = false
            }
            return
        }
        var type = "P"
        if isReceive {
            type = "R"
        }
        var note = ""
        if noteView.textColor == .black {
            note = noteView.text ?? ""
        }
        
        if let payerUser = CoredataManager.getDetail(App_User.self){
            
            payerType = ["accountId": selectedBankAccountList![selectedIndex?.row ?? 0].accId ?? "28063" , "accountNo": "158097840416", "virtualAddress": payerUser.virtualAddress ?? "samreennew@indus" ]
            payeeType = ["name": name ?? "NA","virtualAddress": upiId ?? mobile ?? "gowtham@cscindus","vpaType": "VA"]
        }
        
//        payerType = [ "accountId": "28063","accountNo": "158097840416","virtualAddress": "abhishek@cscindus"]
        //payeeType = ["name": name,"virtualAddress": upiId,"vpaType": "VA"]
        
       
            initPayment(payeeType: payeeType, payerType: payerType, amount: amount ?? "0", note: note)
        
        
        
        
        
//        let item = CoredataManager.saveTransaction(from: "Gowtham", to: mobile ?? self.mobileUpiLabel.text ?? "nil", type:type, toName: name ?? self.nameLabel.text ?? "nil", amount: Double(amount) ?? 0, remark: note, emoji: self.currentEmojis, status: "success", isSave: true)


        //let vc = getSuccessViewController() as! SuccessViewController
        //vc.isFromPay = true
        //let vc = getTransactionDetialsViewController() as! TransactionDetialsViewController
        
        
        
        
        
        
        
        
//        let vc = getSuccessViewController() as! SuccessViewController
//        vc.transaction = item
//        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func decreaseAmountButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            if let amt = Int(self.amountField.text ?? "0") {
                self.amountField.text = amt <= 99 ? "\(amt)" : "\(amt - 100)"
            } else {
                self.amountField.text = "100"
            }
        }
    }
    @IBAction func increaseAmountButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            if let amt = Int(self.amountField.text ?? "0") {
                self.amountField.text = amt > 99900 ? "\(amt)" : "\(amt + 100)"
            } else {
                self.amountField.text = "100"
            }
        }
    }
    @IBAction func closeBtnTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.viewBankList.isHidden = true
        }
    }
    
    @IBAction func bankListButtonTapped(_ sender: Any) {
        if CoredataManager.returnBankCount() == 0{
            self.viewBankList.isHidden = true
        }else{
            self.viewBankList.isHidden = false
        }
    }
    
    
    
}

extension PayConfirmViewController : UITextViewDelegate, UITextFieldDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.subtitleColor {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = Language.getText(with: LanguageConstants.txt_note_placeholder) ?? Constants.txt_note_placeholder
            textView.textColor = UIColor.subtitleColor
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var newText = textView.text!
        newText.removeAll { (character) -> Bool in
            return character == " " || character == "\n"
        }
        guard text.rangeOfCharacter(from: CharacterSet.newlines) == nil else {
                // textView.resignFirstResponder() // uncomment this to close the keyboard when return key is pressed
                return false
            }

        return (newText.count + text.count) <= 100
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string) as String
        if let num = Int(newText), num >= 0 && num <= 100000 {
//            let formatter = NumberFormatter()
//                formatter.numberStyle = NumberFormatter.Style.decimal
//            if let number = Double(textField.text!.replacingOccurrences(of: ",", with: "")) {
//                let result = formatter.string(from: NSNumber(value: number))
//                    textField.text = result!
//            }
            
            return true
        } else if textField.text?.count ?? 0 <= 1 && newText == "" {
            return true
        } else {
            return false
        }
    }
}

extension PayConfirmViewController : DropDownDelegate {
    func dropdownDidChangeValue(textfield: UITextField, title: String, index: Int) {
        /*if textfield == paymentModeDropdown {
            updateDropdown(index : index)
        }*/
    }
    
    
}
extension PayConfirmViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.emojis.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmojiCell", for: indexPath) as? EmojiCell {
            if let img = UIImage.init(named: "\(self.emojis[indexPath.row])") {
            cell.imgEmoji.image = img
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if let img = UIImage.init(named: "\(self.emojis[indexPath.row])") {
                self.imgEmoji.image = img
                self.currentEmojis  = self.emojis[indexPath.row]
            } else {
                self.imgEmoji.image = UIImage()
                self.currentEmojis  = ""
            }
            UIView.animate(withDuration: 0.6, animations: {
                self.imgEmoji.frame.origin.x -= 100
            }) {_ in
                UIView.animateKeyframes(withDuration: 0.6, delay: 0.20, options: .beginFromCurrentState, animations: {
                    self.imgEmoji.frame.origin.x += 100
                }, completion: {_ in
                    UIView.animateKeyframes(withDuration: 0.6, delay: 0.25, options: .beginFromCurrentState, animations: {
                        self.imgEmoji.frame.origin.x += 100
                    }) { (_) in
                        UIView.animate(withDuration: 0.6, animations: {
                            self.imgEmoji.frame.origin.x -= 100
                        })
                    }
                })
            }
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.imgEmoji.image = UIImage()
            //self.currentEmojis  = ""
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.bounds.size.width) / CGFloat(6).rounded(.down)
            return CGSize(width: width, height: 40)
    }
        
}
extension PayConfirmViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CoredataManager.returnBankCount()
        //1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if CoredataManager.returnBankCount() >= 1{
            let bankAccountCell = tableView.dequeueReusableCell(withIdentifier: "BankAccountCell", for: indexPath) as! BankAccountCell
            bankAccountCell.delegate = self
            bankAccountCell.setup(bankName: self.selectedBankAccountList![indexPath.row].bankCode ?? "axis" + (self.selectedBankAccountList![indexPath.row].maskedAccountNumber ?? "****5656") , title: "", icon: self.selectedBankAccountList![indexPath.row].bankCode, type: "Savings Account")
            if let selectedINdex = self.selectedIndex
            {
                if (selectedINdex as IndexPath == indexPath) {
                    bankAccountCell.radioButton.isSelected = true
                    fromBankToPay(selectedBankName: selectedBankAccountList![indexPath.row].bankCode ?? "AXIS", maskedAccount: selectedBankAccountList![indexPath.row].maskedAccountNumber ?? "****5656", icon: selectedBankAccountList![indexPath.row].bankCode, type: "Savings Account")
                    self.viewBankList.isHidden = true
                }
                else{
                    bankAccountCell.radioButton.isSelected = false
                }
            }
            else {
                bankAccountCell.radioButton.isSelected = false
            }
            return bankAccountCell
        }
        
//        BankAccountCell
        
        return UITableViewCell()
    }
    
    
        func radioButtonTapped(indexPath: IndexPath) {
            self.bankListTableView.deselectRow(at: indexPath, animated: true)
                if selectedIndex == indexPath{
                    selectedIndex = IndexPath(row: 0, section: 0)
                }else{
                    selectedIndex = indexPath
                }
            self.bankListTableView.reloadData()
        }
    
    
}

extension PayConfirmViewController{
    func initPayment(payeeType: [String:Any] , payerType : [String:Any] , amount : String , note: String){
        APIManager.payment(InitPaymentResponse.self, urlString: "indus/transact/payment", reqAction: "initPayment", category: "IndusInd", pspId: "IBL", payeeType: payeeType, payerType: payerType, upiTransRefNo: 0, transactionNote: note, isBharatQr: false, amount: amount, expiryTime: 0, initiationMode: "00") { res, err in
            print(res)
            if err == nil{
                DispatchQueue.main.async {
                    if let result = res{
                        let vc1 = getUpiPinViewController() as! UpiPinViewController
                        vc1.paymentInformation = res
                        self.present(vc1, animated: true, completion: nil)
                    }
                }
                
                
                
//                let vc = getSuccessViewController() as! SuccessViewController
//    //            vc.transaction = item
//                vc.newTransaction = self.repeatTransaction
//                self.present(vc, animated: true, completion: nil)

            }
            
//            DispatchQueue.main.async {
//                if let result = res{
//                    let vc1 = getUpiPinViewController() as! UpiPinViewController
//                    vc1.paymentInformation = res
//                    self.present(vc1, animated: true, completion: nil)
//                }
//            }
            
            
        }
    }
}








class EmojiCell : UICollectionViewCell {
    //@IBOutlet weak var lblEmoji: UILabel!
    @IBOutlet weak var imgEmoji: UIImageView!
}
