//
//  HomeViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 17/02/21.
//

import UIKit
import LocalAuthentication

class HomeViewController: UIViewController {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var lblProfileCompletion: UILabel!
    @IBOutlet weak var lblGreeting: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    //@IBOutlet weak var upiLabel: UILabel!
    @IBOutlet weak var lblSend: UILabel!
    @IBOutlet weak var lblReceive: UILabel!
    @IBOutlet weak var lblScan: UILabel!
    @IBOutlet weak var lblRequests: UILabel!
    @IBOutlet weak var lblFavourites: UILabel!
    @IBOutlet weak var lblForyou: UILabel!
    @IBOutlet weak var lblHistory: UILabel!
    @IBOutlet weak var lblhistoryDescription: UILabel!
    @IBOutlet weak var lblOffer: UILabel!
    
    @IBOutlet weak var favCollectionView: UICollectionView!
    @IBOutlet weak var requestCollectionView: UICollectionView!
    @IBOutlet weak var myCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var adCollectionView: UICollectionView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var viewLinkBankAccount: UIView!
    @IBOutlet weak var viewFavourites: UIView!
    @IBOutlet weak var doDontAlertView: UIView!
    @IBOutlet weak var lblDo: UILabel!
    @IBOutlet weak var lblDont: UILabel!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var viewNoFavorite: UIView!
    @IBOutlet weak var alertViewLinkBank: UIView!
    @IBOutlet weak var profileCompletionView: UIView!
    @IBOutlet weak var merchantMenuView: UIView!
    @IBOutlet weak var viewNoRequests: UIView!
    @IBOutlet weak var viewPendingRequest: UIView!
    @IBOutlet weak var payStackView: UIStackView!
    @IBOutlet weak var allPayView: UIView!
    
    let inset: CGFloat = 0
    let minimumLineSpacing: CGFloat = 0
    let minimumInteritemSpacing: CGFloat = 5
    let cellsPerRow = 4
    let cellsPerColumn = 1
    
    var index = 0
    var inForwardDirection = true
    var timer: Timer?
    var isFirst: Bool = false
    var accountLinked = false //false
    var favorites : [Transactions]?
    var recents : [Transactions]?
    var payFavorites : [App_User_Transactions]?
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupValues()
        self.favCollectionView.reloadData()
        self.requestCollectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.setupUI()
        swipeAction()
        if Constants.authenticationStatus != "Done"{
        authenticationWithTouchID(completion: nil)
        }
        //removeLinkedBank()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        startTimer()
    }
    func startTimer() {
        if timer == nil {
            timer =  Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
        }
    }
    @objc func scrollAutomatically(_ timer1: Timer) {

        //scroll to next cell
           let items = adCollectionView.numberOfItems(inSection: 0)
           if (items - 1) == index {
            adCollectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
           } else if index == 0 {
            adCollectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
           } else {
            adCollectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
           }

           if inForwardDirection {
               if index == (items - 1) {
                   index -= 1
                   inForwardDirection = false
               } else {
                   index += 1
               }
           } else {
               if index == 0 {
                   index += 1
                   inForwardDirection = true
               } else {
                   index -= 1
               }
           }
    }
    func swipeAction() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)

    }
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                DispatchQueue.main.async {
                    self.presentDetail(getSettingsViewController(), subType: .fromLeft)
                }
            case UISwipeGestureRecognizer.Direction.left:
                DispatchQueue.main.async {
                    self.present(getScannerViewController(), animated: true, completion: nil)
                }
                
            default:
                break
            }
        }
    }
    
    func checkBankNotLinked() -> Bool {
        if CoredataManager.returnBankCount() == 0 {
            return true
        }
        return false
    }
    
    @IBAction func okBtnTapped(_ sender: Any) {
        self.doDontAlertView.isHidden = true
    }
    
    @IBAction func closeLinkAlertBtnTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.alertViewLinkBank.isHidden = true
        }
    }
    @IBAction func linkNowButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.alertViewLinkBank.isHidden = true
        }
        self.present(getSelectYourBankViewController(), animated: true, completion: nil)
    }
    @IBAction func receiveButtonTapped(_ sender: Any) {
        if checkBankNotLinked() {
            DispatchQueue.main.async {
                self.alertViewLinkBank.isHidden = false
            }
            return
        }
        let vc = getContactsViewController() as! ContactsViewController
        vc.isReceive = true
        self.present(vc , animated: true, completion: nil)
    }
    @IBAction func merchantButtonTapped(_ sender: Any) {
        self.present(getMerchantHomeViewController(), animated: true, completion: nil)
    }
    @IBAction func makePaymentButtonTapped(_ sender: Any) {
        if checkBankNotLinked() {
            DispatchQueue.main.async {
                self.alertViewLinkBank.isHidden = false
            }
            return
        }
        self.present(getContactsViewController(), animated: true, completion: nil)
    }
    @IBAction func requestPaymentButtonTapped(_ sender: Any) {
        if checkBankNotLinked() {
            DispatchQueue.main.async {
                self.alertViewLinkBank.isHidden = false
            }
            return
        }
        self.present(getIncomingRequestViewController(), animated: true, completion: nil)
    }
    @IBAction func profileButtonTapped(_ sender: Any) {
        self.present(getSettingsViewController(), animated: true, completion: nil)
    }
    @IBAction func notificationButtonTapped(_ sender: Any) {
        self.present(getNotificationViewController(), animated: true, completion: nil)
    }
    @IBAction func qrButtonTapped(_ sender: Any) {
        if checkBankNotLinked() {
            DispatchQueue.main.async {
                self.alertViewLinkBank.isHidden = false
            }
            return
        }
        self.present(getScannerViewController(), animated: true, completion: nil)
    }
    @IBAction func transactionButtonTapped(_ sender: Any) {
        let vc = getTransactionHistoryViewController() as! TransactionHistoryViewController
        vc.isFrom = .isHistory
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func becomeMerchantBtnTapped(_ sender: Any) {
       // getGeneralDetailsViewController
        self.present(getChooseYourOptionViewController(), animated: true, completion: nil)
    }
    @IBAction func linkNowBtnTapped(_ sender: Any) {
        self.present(getSelectYourBankViewController(), animated: true, completion: nil)
    }
}
extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == adCollectionView {
            return 3
        } else if collectionView == requestCollectionView {
            
//            if self.recents?.count ?? 0 <= 3{
//                guard let expired = recents?.filter({$0.expiry! > Date()})else{
//                    viewPendingRequest.isHidden = false
//                    return (self.recents?.count ?? 0)
//                }
//                viewPendingRequest.isHidden = true
//                return 0
//            }
//            viewNoRequests.isHidden = false
//            viewPendingRequest.isHidden = false
//            return 1
            
            if self.recents?.count ?? 0 > 0{
                guard let expired = recents?.filter({$0.expiry! < Date()})else{
                    viewPendingRequest.isHidden = false
                    return (self.recents?.count ?? 0)
                }
               // viewPendingRequest.isHidden = true
                viewNoRequests.isHidden = true
                return expired.count
            }else{
                viewNoRequests.isHidden = false
                viewPendingRequest.isHidden = false
                return 0
            }
//            viewNoRequests.isHidden = false
//            viewPendingRequest.isHidden = false
//            return 0
            
        } else{
            if self.payFavorites?.count == nil{
                self.viewFavourites.isHidden = true
                return 0
            }else{
                self.viewFavourites.isHidden = false
                if (self.payFavorites?.count ?? 0) > 3{
                    return  Constants.contactCount
                }else{
                    return (payFavorites?.count ?? 0)
                }
            }
            
            
            //return Constants.contactCount
        }
        
        
        
        
        // Fav transaction goutham's sir
        //        {
        //            if self.favorites?.count ?? 0 == 0 {
        //                self.viewNoFavorite.isHidden = false
        //                return 0
        //            } else {
        //                self.viewNoFavorite.isHidden = true
        //            }
        //            if self.favorites?.count ?? 0 <= 3 {
        //                return (self.favorites?.count ?? 0)
        //            }
        //            return Constants.contactCount }
        
//        switch collectionView{
//        case adCollectionView:
//            return 3
//        case requestCollectionView:
//            if recents?.count == nil || recents?.count == 0 {
//                return 0
//            }else
////            {
////                viewPendingRequest.isHidden = false
////                guard let expired = recents?.filter({$0.expiry! > Date()})else{
////                    viewPendingRequest.isHidden = false
////                 //   viewNoRequests.isHidden = true
////                    //                    viewNoFavorite.isHidden = true
////                    return (self.recents?.count ?? 0)
////                }
////             //   viewNoRequests.isHidden = false
////                viewPendingRequest.isHidden = true
////                return expired.count
////            }
//            {return recents?.count ?? 0}
//
//        case favCollectionView:
////            if self.payFavorites?.count == nil{
////                self.viewNoFavorite.isHidden = false
////                return 0
////            }else{
////                self.viewNoFavorite.isHidden = true
////            }
////
////            if (self.payFavorites?.count ?? 0) <= 3{
////                return (payFavorites?.count ?? 0)
////            }
////            return Constants.contactCount
//
//
//            if payFavorites?.count == nil || payFavorites?.count == 0{
////                viewNoFavorite.isHidden = false
//                return 0
//            }else{
//                viewFavourites.isHidden = false
//                return payFavorites!.count
//            }
//
//
//        default:
//            return 0
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == adCollectionView {
            let cell = collectionView
                .dequeueReusableCell(withReuseIdentifier: "AdCell", for: indexPath) as! AdCell
            return cell
        } else if collectionView == requestCollectionView {
            
            let cell = collectionView
                .dequeueReusableCell(withReuseIdentifier: "PendingRequestCell", for: indexPath) as! PendingRequestCell
            
            if indexPath.row % 2 == 0 {
                cell.lblNameTag.backgroundColor = .systemIndigo
            } else {
                cell.lblNameTag.backgroundColor = .systemGreen
            }
            
            let item = self.recents?[indexPath.row]
            cell.text = item?.toName ?? "-"
            cell.lblNameTag.text = item?.toName?.first?.uppercased() ?? "C"
            
            if let time = getMinutesDifferenceFromTwoDates(start: Date(), end: item?.expiry ?? Date()) {
                cell.lblExpiry.text = "Expires in \(time)"
            } else {
                cell.lblExpiry.text = "Expired"
            }
            
            return cell
        } else  {
            let cell = collectionView
                .dequeueReusableCell(withReuseIdentifier: "RecentCell", for: indexPath) as! RecentCell

            if indexPath.row % 2 == 0 {
                cell.lblNameTag.backgroundColor = .systemIndigo
            } else {
                cell.lblNameTag.backgroundColor = .systemGreen
            }
          /*
            if self.favorites?.count ?? 0 <= 3 {

                    let item = self.favorites?[indexPath.row]
                    cell.text = item?.toName ?? "-"
                    cell.lblNameTag.text = item?.toName?.first?.uppercased() ?? "C"

            } else if indexPath.row == (Constants.contactCount - 1) {
//                cell.imageView.image = UIImage.init(named: "moreContacts")
//                cell.nameLabel.text = "More"
//                cell.nameLabel.textColor = .systemBlue
                cell.isMore = true
                cell.lblNameTag.backgroundColor = .clear
            } else {
//                cell.imageView.image = UIImage.init(named: "avatar")
//                cell.nameLabel.text = "Gowtham"
//                cell.nameLabel.textColor = .black
                let item = self.favorites?[indexPath.row]
                cell.text = item?.toName ?? "-"
                //cell.text = "GowthamS"
                cell.lblNameTag.text = item?.toName?.first?.uppercased() ?? "C"
            }
            */

//            cell.isMore = false
//            if self.payFavorites?.count ?? 0 <= 3 || self.payFavorites?.count ?? 0 > 3{
//                let item = self.payFavorites?[indexPath.row]
//                cell.text =  "-"
//                cell.lblNameTag.text =  "C"
//                if indexPath.row == (Constants.contactCount - 1){
//                    cell.isMore = true
//                    cell.lblNameTag.backgroundColor = .clear
//                }
//            }else if indexPath.row == (Constants.contactCount - 1){
////                cell.isMore = true
//                cell.lblNameTag.backgroundColor = .clear
//            }
            
            
//            if let favourites = payFavorites{
//                let item = favourites[indexPath.row]
//                cell.text =  "-"
//                cell.lblNameTag.text =  "C"
//                cell.lblNameTag.backgroundColor = .clear
//                if favourites.count <= 3{
//                    cell.isMore = false
//                }else if favourites.count > 3{
//                    if indexPath.row == (Constants.contactCount - 1){
//                        cell.isMore = true
//                        cell.lblNameTag.backgroundColor = .clear
//                    }
//                }
//            }
            
            
            if self.payFavorites?.count ?? 0 <= 3 {

                    let item = self.payFavorites?[indexPath.row]
                    cell.text = "item?.toName"
                    cell.lblNameTag.text = "C"
                cell.img = UIImage(named: "e4")

            } else if indexPath.row == (Constants.contactCount - 1) {
//                cell.imageView.image = UIImage.init(named: "moreContacts")
//                cell.nameLabel.text = "More"
//                cell.nameLabel.textColor = .systemBlue
                cell.isMore = true
                cell.lblNameTag.backgroundColor = .clear
            } else {
//                cell.imageView.image = UIImage.init(named: "avatar")
//                cell.nameLabel.text = "Gowtham"
//                cell.nameLabel.textColor = .black
                let item = self.payFavorites?[indexPath.row]
                cell.text =  "GowthamS"
                //cell.text = "GowthamS"
                cell.img = UIImage(named: "e4")
                cell.lblNameTag.text = "C"
            }
            
            
            
            
            
            
            return cell
        }
        
        
        
//        switch collectionView{
//        case adCollectionView:
//            let cell = collectionView
//                .dequeueReusableCell(withReuseIdentifier: "AdCell", for: indexPath) as! AdCell
//            return cell
//        case requestCollectionView:
//            let cell = collectionView
//                .dequeueReusableCell(withReuseIdentifier: "PendingRequestCell", for: indexPath) as! PendingRequestCell
//
//            if indexPath.row % 2 == 0 {
//                cell.lblNameTag.backgroundColor = .systemIndigo
//            } else {
//                cell.lblNameTag.backgroundColor = .systemGreen
//            }
//
//            let item = self.recents?[indexPath.row]
//            cell.text = item?.toName ?? "-"
//            cell.lblNameTag.text = item?.toName?.first?.uppercased() ?? "C"
//
//            if let time = getMinutesDifferenceFromTwoDates(start: Date(), end: item?.expiry ?? Date()) {
//                cell.lblExpiry.text = "Expires in \(time)"
//            } else {
//                cell.lblExpiry.text = "Expired"
//            }
//            return cell
//        case favCollectionView:
//            let cell = collectionView
//                .dequeueReusableCell(withReuseIdentifier: "RecentCell", for: indexPath) as! RecentCell
//            if indexPath.row % 2 == 0 {
//                cell.lblNameTag.backgroundColor = .systemIndigo
//            } else {
//                cell.lblNameTag.backgroundColor = .systemGreen
//            }
//
//            if self.payFavorites?.count ?? 0 <= 3{
//                let item = self.payFavorites?[indexPath.row]
//                cell.text =  "-"
//                cell.lblNameTag.text =  "C"
//            }else if indexPath.row == (Constants.contactCount - 1){
//                cell.isMore = true
//                cell.lblNameTag.backgroundColor = .clear
//            }else{
//                let item = self.payFavorites?[indexPath.row]
//                cell.text = "-"
//                cell.lblNameTag.text =  "C"
//            }
//            return cell
//        default:
//            return UICollectionViewCell()
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == adCollectionView {
            let itemHeight = collectionView.bounds.size.height
            return CGSize(width: collectionView.bounds.size.width , height: itemHeight)
        } else if collectionView == self.requestCollectionView {
            let width = (self.requestCollectionView.bounds.size.width) / CGFloat(cellsPerRow).rounded(.down)
                return CGSize(width: width, height: 100)
            }else  if collectionView == self.favCollectionView {
            let width = (self.favCollectionView.bounds.size.width) / CGFloat(cellsPerRow).rounded(.down)
                return CGSize(width: width, height: 90)
            }else{
                return CGSize(width: 0, height: 0)
            }
        }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.favCollectionView {
            DispatchQueue.main.async {
                if indexPath.row == 3 {
                    self.present(getContactsViewController(), animated: true, completion: nil)
                } else {
                    let vc = getPayConfirmViewController() as! PayConfirmViewController
                    //vc.item = self.payFavorites?[indexPath.row]
                    
                    self.present(vc, animated: true, completion: nil) }
            }
        } else if collectionView == self.requestCollectionView {
            DispatchQueue.main.async {
                if let item = self.recents?[indexPath.row] {
                let vc = getRequestDetailsViewController() as! RequestDetailsViewController
                vc.transaction = item
                    self.present(vc, animated: true, completion: nil) }
            }
        }
    }
    
}

// MARK:-
extension HomeViewController {
    
//    func authenticationWithTouchID() {
//        let localAuthenticationContext = LAContext()
//        localAuthenticationContext.localizedFallbackTitle = Constants.alert_enter_passcode
//
//        var authorizationError: NSError?
//        let reason = Constants.alert_unlock_cscapp
//
//        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: &authorizationError) {
//            
//            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) { success, evaluateError in
//                
//                if success {
//                    DispatchQueue.main.async() {
//                        self.view.removeBlurEffect()
//                        //self.checkItsFirstTime()
//                        }
//                        /*let alert = UIAlertController(title: "Success", message: "Authenticated succesfully!", preferredStyle: UIAlertController.Style.alert)
//                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in
//                            self.view.removeBlurEffect()
//                        }))
//                        self.present(alert, animated: true, completion: nil) */
//                    //}
//                    
//                } else {
//                    // Failed to authenticate
//                    guard let error = evaluateError else {
//                        return
//                    }
//                    print(error)
//                    DispatchQueue.main.async() {
//                        self.presentAlertWithTitle(title: "Unlock To Continue", message: Constants.alert_use_passcode, options: "Retry") { (option) in
//                        //print("option: \(option)")
//                        self.authenticationWithTouchID()
//                    }
//                    }
//                }
//            }
//        } else {
//            
//            guard let error = authorizationError else {
//                return
//            }
//            print(error)
//        }
//    }
    
    // MARK:-
    // MARK: Present Alert
    func notifyUser(_ msg: String, err: String?) {
        let alert = UIAlertController(title: msg,
                                      message: err,
                                      preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "OK",
                                         style: .cancel, handler: nil)
        
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true,
                     completion: nil)
    }
    
    
    // MARK:-
    // MARK: Get error message
    func errorMessage(errorCode:Int) -> String{
        
        var strMessage = ""
        
        switch errorCode {
        case LAError.authenticationFailed.rawValue:
            strMessage = "Authentication Failed"
            
        case LAError.userCancel.rawValue:
            strMessage = "User Cancel"
            
        case LAError.userFallback.rawValue:
            strMessage = "User Fallback"
            
        case LAError.systemCancel.rawValue:
            strMessage = "System Cancel"
            
        case LAError.passcodeNotSet.rawValue:
            strMessage = "Passcode Not Set"
        case LAError.biometryNotAvailable.rawValue:
            strMessage = "TouchI DNot Available"
            
        case LAError.biometryNotEnrolled.rawValue:
            strMessage = "TouchID Not Enrolled"
            
        case LAError.biometryLockout.rawValue:
            strMessage = "TouchID Lockout"
            
        case LAError.appCancel.rawValue:
            strMessage = "App Cancel"
            
        case LAError.invalidContext.rawValue:
            strMessage = "Invalid Context"
            
        default:
            strMessage = "Some error found"
            
        }
        
        return strMessage
        
    }
}


//        APIManager.performRequest { (boolString, err) in
//            print(boolString)
//        }
//AppSecure.encryptGCM("dd", plainText: "Abhishek1234")
//AppSecure.encryptMobileGCM("Abhishek1234", key: "98")
//AppSecure.tokenEncryptWithGCM("Abhishek123456", aesKey: "c57f4d4bcf08dcb7300e8bca8e5d3e22cedbaaee73bec7b027561975a825f54e")
//otp.sha256("HEX") as? String ?? "NA"


/*
DispatchQueue.main.async {
    let RandomNumber = Int(arc4random_uniform(UInt32(Constants.Illustrations.count)))
        //imageArr is array of images
    self.illustratorView.image = UIImage.init(named: "\(Constants.Illustrations[RandomNumber])")
    
} */


/*
override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    let height = collectionView.collectionViewLayout.collectionViewContentSize.height
    myCollectionViewHeight.constant = height
    self.view.layoutIfNeeded()
}
 
 @IBAction func backButtonTapped(_ sender: Any) {
     self.dismiss(animated: true, completion: nil)
 }
 @IBAction func accountBalanceButtonTapped(_ sender: Any) {
     self.present(getCheckBalanceViewController(), animated: true, completion: nil)
 }
 
 @IBAction func checkTrendButtonTapped(_ sender: Any) {
     //self.present(getSpendTrendsViewController(), animated: true, completion: nil)
     //self.present(getBusinessTrendsViewController(), animated: true, completion: nil)
 }
 
 //self.collectionView = collectionView
 //.nameLabel.text = "jhgjhg" // (Constants.Bank_Items[indexPath.row]["title"]! as! String)
 //cell.imageView?.image = UIImage(named: Constants.Bank_Items[indexPath.row]["icon"]! as! String)
 
 */

/*
override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    collectionView?.collectionViewLayout.invalidateLayout()
    super.viewWillTransition(to: size, with: coordinator)
} */

//let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow

//let marginsAndInsets = inset * 2 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
//let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
//let width = (collectionView.bounds.size.width) / CGFloat(cellsPerRow).rounded(.down)


/*
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return minimumLineSpacing
}

func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return minimumInteritemSpacing
}

func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    if collectionView == adCollectionView {
        let itemHeight = collectionView.bounds.size.height
        return CGSize(width: collectionView.bounds.size.width , height: itemHeight)
    } else {
        if #available(iOS 11.0, *) {
            let marginsAndInsets = inset * 2 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
            let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
            //let itemWidth = (collectionView.bounds.size.width / CGFloat(cellsPerRow)).rounded(.down)
            //let marginsAndInsetsForColumn = inset * 2 + collectionView.safeAreaInsets.top + collectionView.safeAreaInsets.bottom + minimumLineSpacing * CGFloat(cellsPerColumn - 1)
            //let itemHeight = ((collectionView.bounds.size.height - marginsAndInsetsForColumn ) / CGFloat(cellsPerColumn)).rounded(.down)
            return CGSize(width: itemWidth, height: 85)
        } else {
            // Fallback on earlier versions
        }
        return CGSize(width: 320, height: 90)
    }
} */

extension HomeViewController{
    func setupValues() {
//        if let recent = CoredataManager.getSpecificRecord(Transactions.self, key: "type", value: "P") {
//            self.favorites = recent
//            self.viewFavourites.isHidden = false
//        }else{
////            self.favorites = nil
//            self.viewFavourites.isHidden = true
////        }
//        if let recent = CoredataManager.getSpecificRecord(Transactions.self, key: "type", value: "R") {
//            self.recents = recent
//           // requestCollectionView.reloadData()
//        }
//
//        if let payFav = CoredataManager.getDetails(App_User_Transactions.self){
//            self.payFavorites = payFav
//           // favCollectionView.reloadData()
//        }
//
        if isFirst {
            self.doDontAlertView.isHidden = false
        } else { self.doDontAlertView.isHidden = true }
//
        var progress = 0.0
        
        var bLink = 0
        
        if CoredataManager.getDetails(Account_List.self) != nil{
            bLink = CoredataManager.getDetails(Account_List.self)?.count ?? 0
        }
        
        if accountLinked || bLink > 0 {
            progress += 20
           // self.viewFavourites.isHidden = false
            if self.payFavorites?.count ?? 0 == 0 {
                self.viewFavourites.isHidden = true
            }
            self.viewLinkBankAccount.isHidden = true
        } else {
            self.viewFavourites.isHidden = true
            self.viewLinkBankAccount.isHidden = false
        }
        
        
        
        if let img = UD.SharedManager.getImageFromUserDefault(key:Constants.User_Image) {
            self.imgProfile.image = img
            progress += 20
        }
        
        if let user = CoredataManager.getDetail(App_User.self) {
            if let mob = user.mobile {
                self.usernameLabel.text = mob
                progress += 10
            }
            if let name = user.name {
                self.usernameLabel.text = name
                progress += 10
            }
            if let _ = user.gender {
                progress += 10
            }
            if let _ = user.dob {
                progress += 10
            }
            if let _ = user.mail {
                progress += 10
            }
//            if let _ = userInfo["upi"] {
//                progress += 10
//            }
        }
        if progress >= 99 {
            self.profileCompletionView.isHidden = true
        } else {
            self.profileCompletionView.isHidden = false
        }
        self.lblProfileCompletion.text = "Profile Completion \(Int(progress))%"
        if progress > 49 && progress < 81 {
            progressBar.progressTintColor = .systemYellow
        } else if progress > 81 {
            progressBar.progressTintColor = .customGreen
        }
        self.progressBar.progress = Float(progress / 100)
        
        
        
        if let recent = CoredataManager.getSpecificRecord(Transactions.self, key: "type", value: "R") {
            self.recents = recent
            //self.requestCollectionView.reloadData()
        }

        if let payFav = CoredataManager.getDetails(App_User_Transactions.self){
            self.payFavorites = payFav
            //self.favCollectionView.reloadData()
        }
        
//        DispatchQueue.main.async {
//
//            if let recent = CoredataManager.getSpecificRecord(Transactions.self, key: "type", value: "R") {
//                self.recents = recent
//                //self.requestCollectionView.reloadData()
//            }
//
//            if let payFav = CoredataManager.getDetails(App_User_Transactions.self){
//                self.payFavorites = payFav
//                //self.favCollectionView.reloadData()
//            }
//
//        }
        
        
        
    }
    
    
    func setupUI() {
        lblDo.setup(fontSize: 14, fontType: .regular, textColor: .titleColor, bgColor: .clear, text: "• Make sure you login and initiate UPI transaction in complete privacy. \n• Change your UPI application password and UPI PIN / MPIN frequently. \n• Incase of transactional or non-transactional issue, explore the support section of the application.")
        lblDont.setup(fontSize: 14, fontType: .regular, textColor: .titleColor, bgColor: .clear, text: "• Please do not share your PIN or MPIN / do not store it in your Mobile handset. \n• Never let anyone see you entering your application password or UPI PIN / MPIN.")
        lblGreeting.setup(textColor: .darkGray, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        usernameLabel.setup(fontSize: 20.0, fontType: .bold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        //upiLabel.setup(fontSize: 14.0, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblSend.setup(fontSize: 14.0, fontType: .semiBold, textColor: .darkGray, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblReceive.setup(fontSize: 14.0, fontType: .semiBold, textColor: .darkGray, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblScan.setup(fontSize: 14.0, fontType: .semiBold, textColor: .darkGray, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblRequests.setup(fontSize: 14.0, fontType: .semiBold, textColor: .darkGray, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblFavourites.setup(fontSize: 16.0, fontType: .semiBold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblForyou.setup(fontSize: 16.0, fontType: .semiBold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblOffer.setup(fontSize: 16.0, fontType: .semiBold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblHistory.setup(fontSize: 16.0, fontType: .semiBold, textColor: .titleColor, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        lblhistoryDescription.setup(fontSize: 14.0, fontType: .regular, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_verification_title))
        
        self.bottomView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.adCollectionView.showsHorizontalScrollIndicator = false
        
    }
}
