//
//  GenerateQRViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 08/02/21.
//

import UIKit

class GenerateQRViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblQrDescription: UILabel!
    @IBOutlet weak var borderImageView: UIImageView!
    @IBOutlet weak var qrImageView: UIImageView!
    //@IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var lblUPI: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewNoRecents: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    
    var brightness : CGFloat = 0.5
    let inset: CGFloat = 10
    let minimumLineSpacing: CGFloat = 10
    let minimumInteritemSpacing: CGFloat = 10
    let cellsPerRow = 5
    let cellsPerColumn = 1
    var recents = [Transactions]()
    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular, text: Language.getText(with: LanguageConstants.lbl_Qr_title))
        lblSubtitle.setup(fontSize : 18, text: Language.getText(with: LanguageConstants.lbl_Qr_Subtitle))
        lblQrDescription.setup(fontSize: 14, fontType: .regular, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_Qr_Description))
        lblUPI.setup(fontSize: 16, fontType: .regular, textColor: .titleColor) //UPI ID:
        textField.setup(fontSize: 16.0, fontType: .regular, textColor: .subtitleColor, bgColor: .lightGreyColor, text: nil, placeholderText: Language.getText(with: LanguageConstants.lbl_enter_mobile_placeholder))
        
        if let recent = CoredataManager.getSpecificRecord(Transactions.self, key: "type", value: "R") {
            self.recents = recent
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupUI()
        setupResend()
        /*
        let yourAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont (name: "Nunito-Semibold", size: 15) ?? UIFont.systemFont(ofSize: 15),
              .foregroundColor: hexStringToUIColor(hex: "1160DA"),
              .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "SHARE",
                                                             attributes: yourAttributes)
        shareButton.setAttributedTitle(attributeString, for: .normal)
        */
        brightness = UIScreen.main.brightness
        
        //let swiftLeeOrangeColor = UIColor(red:0.93, green:0.31, blue:0.23, alpha:1.00)
        let swiftLeeLogo = UIImage(named: "logo_solo")!

        let qrURLImage = URL(string: "Gowtham_9744484730_Gautam@cscpay")?.qrImage(using: .customBlack, logo: swiftLeeLogo)
        
        self.qrImageView.image = UIImage.init(ciImage: qrURLImage!)
            UIScreen.main.brightness = 1.0
        
        
//        if let image = generateQRCode(from: "This QR is only for Testing purpose!") {
//        self.qrImageView.image = image
//            UIScreen.main.brightness = 1.0
//        }
    }
    func setupResend() {
        //self.lblNoData.halfTextColorChange(fullText: Language.getText(with: LanguageConstants.lbl_resend_otp) ?? Constants.otpResend, changeText: Language.getText(with: LanguageConstants.btn_resend_otp) ?? Constants.otpResendBtn, color: .lightBlue, textSize: 11)
        self.lblNoData.halfTextColorChange(fullText: "People who you’ve recently paid will show up here. Find people to pay", changeText: "Find people to pay", color: .lightBlue, textSize: 11)
        self.lblNoData.isUserInteractionEnabled = true
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnLabel(_ :)))
        tapgesture.numberOfTapsRequired = 1
        self.lblNoData.addGestureRecognizer(tapgesture)
    }
    //MARK:- tappedOnLabel
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = self.lblNoData.text else { return }
        let termsRange = (text as NSString).range(of: "Find people to pay")
        if gesture.didTapAttributedTextInLabel(label: lblNoData, inRange: termsRange) {
            DispatchQueue.main.async {
                self.view.endEditing(true)
                let vc = getContactsViewController() as! ContactsViewController
                vc.isReceive = true
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    func saveImage() {
        
        guard let ciImage = qrImageView.image?.ciImage else { return }
        guard let cgImage = cgImage(from: ciImage) else { return } // the above function
        let newImage = UIImage(cgImage: cgImage) // ready to be saved!

        UIImageWriteToSavedPhotosAlbum(newImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }

    //MARK: - Save Image callback

    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {

        if let error = error {
            notImplementedYetAlert(base: self, msg: "Please try again after sometime.")
            print(error.localizedDescription)

        } else {
            notImplementedYetAlert(base: self, title: "Success", msg: "QR Image stored into your photos.")
            print("Success")
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        UIScreen.main.brightness = brightness
    }
    @IBAction func downloadButtonTapped(_ sender: Any) {
        saveImage()
    }
    @IBAction func shareButtonTapped(_ sender: Any) {
        shareQRCode()
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            //filter.setValue("H", forKey: "inputCorrectionLevel")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
    private func shareQRCode() {
        if let image = qrImageView.image {
            let vc = UIActivityViewController(activityItems: [image], applicationActivities: [])
            present(vc, animated: true)
        }
    }
}
extension GenerateQRViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if recents.count == 0 {
            self.viewNoRecents.isHidden = false
        } else {
            self.viewNoRecents.isHidden = true
        }
        return recents.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: "RecentCell", for: indexPath) as! RecentCell
        let item = recents[indexPath.row]
        cell.text = item.toName
        
        cell.lblNameTag.text = item.toName?.first?.uppercased() ?? "C"
        if indexPath.row % 2 == 0 {
            cell.lblNameTag.backgroundColor = .systemIndigo
        } else {
            cell.lblNameTag.backgroundColor = .systemGreen
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        DispatchQueue.main.async {
            let vc = getPSPReceiveConfirmViewController() as! PSPReceiveConfirmViewController
            //vc.titleStr = "Request"
            //vc.btnTitle = Language.getText(with: LanguageConstants.btn_proceed_to_pay) ?? Constants.btn_proceed_to_pay
            self.present(vc, animated: true, completion: nil)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
                let marginsAndInsets = inset * 2 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
                let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
                let marginsAndInsetsForColumn = inset * 2 + collectionView.safeAreaInsets.top + collectionView.safeAreaInsets.bottom + minimumLineSpacing * CGFloat(cellsPerColumn - 1)
                let itemHeight = ((collectionView.bounds.size.height - marginsAndInsetsForColumn ) / CGFloat(cellsPerColumn)).rounded(.down)
                return CGSize(width: itemWidth, height: 85)
        
           // return CGSize(width: 320, height: 90)
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
        super.viewWillTransition(to: size, with: coordinator)
    }
    
}
extension GenerateQRViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.async {
            self.view.endEditing(true)
            let vc = getContactsViewController() as! ContactsViewController
            vc.isReceive = true
            self.present(vc, animated: true, completion: nil)
        }
    }
}
