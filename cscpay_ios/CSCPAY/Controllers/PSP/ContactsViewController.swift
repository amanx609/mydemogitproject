//
//  ContactsViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 08/02/21.
//

import UIKit
import Contacts
import ContactsUI

class ContactsViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    //@IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var responsibleLabel: UILabel!
    @IBOutlet weak var historyBtn: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    
    var results: [CNContact] = []
    
    var contacts = [CNContact]()
    var NameArray = [String]()
    var NumberArray = [String]()
    var ImageArray = [UIImage]()

    
    var filteredName = [String]()
    var filteredNumber = [String]()
    var filteredImage = [UIImage]()
    
    
    
    
    
    var tempName = [String]()
    var tempNumber = [String]()
    var contactDict = [String:Any]()
    
    
    
    var isSearch: Bool = false
    var searchActive : Bool = false
    var isReceive = false
    var isMerchant = false
    
    var recents = [Transactions]()
    var objInvited = [Invited]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        self.tableView.tableFooterView = UIView()
        getContacts()
        if isReceive {
            self.titleLabel.text = "Collect"
            self.lblDescription.text = "You can create a request to collect money from your friends & family"
            if !isMerchant {
                self.historyBtn.isHidden = false }
            if let recent = CoredataManager.getSpecificRecord(Transactions.self, key: "type", value: "R") {
                self.recents = recent
                self.tableView.reloadData()
            }
        } else {
            if let recent = CoredataManager.getSpecificRecord(Transactions.self, key: "type", value: "P") {
                self.recents = recent
                self.tableView.reloadData()
            }
        }
        /*
        let directionalMargins = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0)
        searchBar.directionalLayoutMargins = directionalMargins
        searchBar.searchTextField.layoutMargins = UIEdgeInsets(top: -10, left: -20, bottom: 0, right: 0)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).leftViewMode = .never */
        
//        if let result = getContactFromCNContact() {
//            results = result
//        }
        
        //searchTextField.delegate = self
//        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 120, height: 50))
//        button.backgroundColor = hexStringToUIColor(hex: "1160DA")
//        button.cornerRadius = 5
//        button.setTitle("PROCEED", for: .normal)
//        button.setTitleColor(UIColor.white, for: .normal)
//        button.addTarget(self, action: #selector(self.nextButton), for: .touchUpInside)
        //searchTextField.inputAccessoryView = inputAccessoryView
    }
    /*
    override var inputAccessoryView: UIView? {
        get {
            //Set up the container
            let containerView = UIView()
            containerView.backgroundColor = #colorLiteral(red: 0.9784782529, green: 0.9650371671, blue: 0.9372026324, alpha: 1)
            containerView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 60)

            let proceedButton = UIButton()
            containerView.addSubview(proceedButton)
            proceedButton.translatesAutoresizingMaskIntoConstraints = false
            proceedButton.setTitle("PROCEED", for: .normal)
            proceedButton.backgroundColor = .systemBlue
            proceedButton.layer.cornerRadius = 10
            proceedButton.layer.masksToBounds = true
            proceedButton.addTarget(self, action: #selector(self.nextButton), for: .touchUpInside)
            proceedButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10).isActive = true
            proceedButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10).isActive = true
            proceedButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -8).isActive = true
            proceedButton.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 8).isActive = true
            
            // Negative values for constraints can be avoided if we change order of views when applying constrains
            // f.e. instead of button.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8).isActive = true
            // write containerView.trailingAnchor.constraint(equalTo: button.trailingAnchor, constant: 8).isActive = true

            return containerView
        }
    } */
    @objc func nextButton()
    {
        if responsibleLabel.text == "" {
            responsibleLabel.text = "You have entered an incorrect UPI ID. Try again." } else {
                responsibleLabel.text = ""
            }
        print("do something")
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnHistoryTapped(_ sender: Any) {
        self.present(getReceiveHistoryViewController(), animated: true, completion: nil)
    }
    func getContacts()
    {
        let status = CNContactStore.authorizationStatus(for: .contacts)
        if status == .denied || status == .restricted {
            //presentSettingsActionSheet()
            return
        }

        // open it

        let contactStore = CNContactStore()
        contactStore.requestAccess(for: .contacts) { granted, error in
            guard granted else {
                DispatchQueue.main.async {
                   // self.presentSettingsActionSheet()
                }
                return
            }

            // get the contacts
            //let keys = [
               // CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
               // CNContactPhoneNumbersKey as CNKeyDescriptor] as [Any]
            let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey , CNContactImageDataKey , CNContactImageDataAvailableKey, CNContactThumbnailImageDataKey]
            let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
            request.sortOrder = CNContactSortOrder.userDefault
            do {
                try contactStore.enumerateContacts(with: request){ [self]
                    (contact, stop) in
                    // Array containing all unified contacts from everywhere
                    self.contacts = self.contacts.sorted {
                                $0.givenName < $1.givenName
                            }
                    //self.contacts.append(contact)
                    var i = 0
                    for phoneNumber in contact.phoneNumbers {
                        
                        //print("\(contact.givenName) \(contact.familyName)\n \(phoneNumber.value.stringValue)")
                        
                        self.NameArray.append("\(contact.givenName) \(contact.familyName)")
                        self.NumberArray.append(phoneNumber.value.stringValue)
                        if contact.imageDataAvailable{
                                self.ImageArray.append(UIImage(data: contact.imageData!)!)
                        }
                        
                        contactDict["name"] = NameArray
                        contactDict["number"] = NumberArray
                        contactDict["image"] = ImageArray
                        
                        
                        i = i+1
                        
                    }
                    i = 0

                    self.filteredName   = self.NameArray //.sorted(by: <)
                    self.filteredNumber = self.NumberArray
                    self.filteredImage = self.ImageArray
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            } catch {
                print("unable to fetch contacts")
            }
        }
    }
    

}
extension ContactsViewController : UITableViewDelegate, UITableViewDataSource {
    /*func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "Y", "Z" ]
    } */
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "A title for section: \(section)"
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchActive {
            return 1
        }
        if self.recents.count > 0 {
            return 2
        }
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if(section == 0) {
//             return results.count
//           } else {
//            return 3
//           }
        if searchActive {
            return self.filteredName.count
        }
        if section == 0 && self.recents.count != 0 {
            //return Set(recents.map({$0.to})).count
            
//            return recents.uniqueArray().count
            
             recents = removeDuplicateElements(post: recents)
            
            //return newRecents.count
            return self.recents.count
        }
        return self.filteredName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let contactCell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: NSIndexPath(row: 0, section: 0) as IndexPath) as! ContactCell
        // `contacts` Contains all details of Phone Contacts
            
//            for contact in results {
//
//                print(contact.middleName)
//                print(contact.familyName)
//                print(contact.givenName)
//                if let img = contact.thumbnailImageData {
//                    //print(img)
//                    contactCell.contactImageView.image = UIImage.init(data: img)
//                } else {
//                    contactCell.nameLabel.text = String(contact.nickname.prefix(1))
//                }
//                contactCell.fullNameLabel.text = contact.givenName
////                contactCell.descLabel.text = contact.phoneNumbers.compactMap { (phoneNumber: CNLabeledValue) in
////                    guard let number = phoneNumber.value.value(forKey: "digits") as? String else { return nil }
////                    return number
////                }
//            }
        if indexPath.section != 0 || searchActive {
           
//            contactCell.fullNameLabel.text = self.filteredName[indexPath.row]
//            contactCell.descLabel.text = self.filteredNumber[indexPath.row]
//            contactCell.nameLabel.text = self.filteredName[indexPath.row].first?.uppercased() ?? "C"
            
            if let img = contactDict["image"]{
              print(img)
            }
            
            
            contactCell.setData(contactImage: "", name: filteredName[indexPath.row], fullname: self.filteredName[indexPath.row], descLabel: filteredNumber[indexPath.row])
            
            
        }
        else if indexPath.section == 0 && self.recents.count != 0 {
            
//            let recentsItemName = Array(Set(recents.map({$0.toName})))
//            let recentsItemNumber = Array(Set(recents.map({$0.to})))
//
//
//
//            contactCell.setData(contactImage: "", name: recentsItemName[indexPath.row] ?? "NA", fullname: recentsItemName[indexPath.row] ?? "NA", descLabel: recentsItemNumber[indexPath.row] ?? "NA")

            
            let item = self.recents[indexPath.row]
            
            
            
//            contactCell.fullNameLabel.text = item.toName
//            contactCell.descLabel.text = item.to
//            contactCell.nameLabel.text = item.toName?.first?.uppercased() ?? "C"
            
            
            contactCell.setData(contactImage: "", name: item.toName ?? "NA", fullname: item.toName ?? "NA", descLabel: item.to ?? "NA")

            
            
        } else  if indexPath.section == 0 {
//            contactCell.fullNameLabel.text = self.filteredName[indexPath.row]
//            contactCell.descLabel.text = self.filteredNumber[indexPath.row]
//            contactCell.nameLabel.text = self.filteredName[indexPath.row].first?.uppercased() ?? "C"
           // contactCell.contactImageView.image = self.filteredImage[indexPath.row]
            
            contactCell.setData(contactImage: "", name: filteredName[indexPath.row] , fullname: filteredName[indexPath.row], descLabel: filteredName[indexPath.row])
            
        }
        if indexPath.row % 2 == 0 {
            contactCell.nameLabel.backgroundColor = .systemIndigo
        } else {
            contactCell.nameLabel.backgroundColor = .systemGreen
        }
        
        return contactCell
        //return UITableViewCell()
    }
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        3
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            let name = self.filteredName[indexPath.row]
            let mobile = self.filteredNumber[indexPath.row]
            
            if self.isMerchant {
                let vc = getReceiveConfirmViewController() as! ReceiveConfirmViewController
                //vc.isFromRequest = self.isFromRequest
                //vc.isReceive = self.isReceive
                vc.name = name
                vc.mobile = mobile
                self.present(vc, animated: true, completion: nil)
                return
            } else {
            
                if self.isReceive {
                    let vc = getPSPReceiveConfirmViewController() as! PSPReceiveConfirmViewController
                    //vc.isFromRequest = self.isFromRequest
                    vc.isReceive = self.isReceive
                    vc.name = name
                    vc.mobile = mobile
                    self.present(vc, animated: true, completion: nil)
                } else {
                    let vc = getPayConfirmViewController() as! PayConfirmViewController
                    //vc.isFromRequest = self.isFromRequest
                    vc.isReceive = self.isReceive
                    if self.recents.count != 0{
                        vc.name = self.recents[indexPath.row].toName
                    }
                    vc.name = name
                    vc.mobile = mobile
                    
//                    vc.mobile = self.filteredNumber[indexPath.row]

                    self.present(vc, animated: true, completion: nil)
                }
            
            }
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if searchActive {
            return nil
        }
        if(section == 0 && self.recents.count != 0) {
            //return nil //As you do not have to display header in 0th section, return nil
            let reuseIdentifier : String!
        reuseIdentifier = String(format: "HeaderCell")

            let headerView: HeaderCell = self.tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell // tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath) as! HeaderCell
            headerView.titleLabel.text = "Recents"

            return headerView
        } else {
            //return <Return your header view>

           //You can design your header view inside tableView as you design cell normally, set identifier to it and dequeue cell/headerView as:
           //However this may result in unexpected behavior since this is not how the tableview expects you to use the cells.

            let reuseIdentifier : String!
        reuseIdentifier = String(format: "HeaderCell")

            let headerView: HeaderCell = self.tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell // tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 1) as IndexPath) as! HeaderCell
            
            headerView.titleLabel.text = "All Contacts"

            return headerView
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if searchActive {
            return 0
        }
        if self.recents.count == 0 && section == 0 {
            return 0
        }
        return 40
//        if(section == 0) {
//            return 40
//        } else {
//            return 40
//        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.filteredName.removeAll()
        self.filteredNumber.removeAll()
        if(self.NameArray.count != 0){
            var mystring = "\(textField.text ?? "")\(string)"
            if(textField.text?.count == 1 && string == ""){
                mystring = ""
            }
            
            if let _ = Int(mystring) {
                var i = 0
                for ContactName in self.NumberArray {
                    let name = ContactName
                    let range = name.lowercased().range(of: mystring, options: .caseInsensitive, range: nil,   locale: nil)

                    if range != nil {
                        if(filteredName.count == 0){

                            filteredName = [NameArray[i]]
                            filteredNumber = [ContactName]

                        }else{

                            filteredName.append(NameArray[i])
                            filteredNumber.append(ContactName)
                        }

                    }else{
                        tempNumber.append(mystring)
                    }
                    i = i+1
                }
            } else {
                
                var i = 0
                for ContactName in self.NameArray {
                    let name = ContactName
                    let range = name.lowercased().range(of: mystring, options: .caseInsensitive, range: nil,   locale: nil)
                    if range != nil {
                        if(filteredName.count == 0){
                            filteredName = [ContactName]
                            filteredNumber = [NumberArray[i]]
                        }else{
                            filteredName.append(ContactName)
                            filteredNumber.append(NumberArray[i])
                        }
                    }else{
                        tempName.append(mystring)
                    }
                    i = i+1
                }
            }
//            if(string == "" && (textField.text?.count == 1)){
//                self.filteredName   = self.NameArray
//                self.filteredNumber = self.NumberArray
//            }
//            self.tableView.reloadData()
//            }
        
        DispatchQueue.main.async {
        if(string == "" && (textField.text?.count == 1)){
            self.filteredName   = self.NameArray
            self.filteredNumber = self.NumberArray
            self.searchActive = false;
            //self.searchTableView.isHidden = true
        } else {
            self.searchActive = true;
            //self.searchTableView.isHidden = false
        }
        self.tableView.reloadData()
        //self.searchTableView.reloadData()
        } }
        
            return true
        }
}
extension ContactsViewController : UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
            //searchActive = true;
        }

        func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
            searchActive = false;
        }

        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            searchActive = false;
        }

        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchActive = false;
        }

        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            
            self.filteredName.removeAll()
            self.filteredNumber.removeAll()
            if(self.NameArray.count != 0){
                var mystring = searchText
                
                if let _ = Int(mystring) {
                    var i = 0
                    for ContactName in self.NumberArray {
                        let name = ContactName
                        let range = name.lowercased().range(of: mystring, options: .caseInsensitive, range: nil,   locale: nil)

                        if range != nil {
                            if(filteredName.count == 0){

                                filteredName = [NameArray[i]]
                                filteredNumber = [ContactName]

                            }else{

                                filteredName.append(NameArray[i])
                                filteredNumber.append(ContactName)
                            }

                        }else{
                            print("not in contacts")
                        }
                        i = i+1
                    }
                } else {
                    
                    var i = 0
                    for ContactName in self.NameArray {
                        let name = ContactName
                        let range = name.lowercased().range(of: mystring, options: .caseInsensitive, range: nil,   locale: nil)
                        if range != nil {
                            if(filteredName.count == 0){
                                filteredName = [ContactName]
                                filteredNumber = [NumberArray[i]]
                            }else{
                                filteredName.append(ContactName)
                                filteredNumber.append(NumberArray[i])
                            }

                        }else{
                            print("not in contacts")
                        }
                        
                        i = i+1
                    }
                }
                
                DispatchQueue.main.async {
                if(searchText == ""){
                    self.filteredName   = self.NameArray
                    self.filteredNumber = self.NumberArray
                    self.searchActive = false;
                    //self.searchTableView.isHidden = true
                } else {
                    self.searchActive = true;
                    //self.searchTableView.isHidden = false
                }
                self.tableView.reloadData()
                //self.searchTableView.reloadData()
                }
                
                }
                
                    /*

            filtered = data.filter({ (text) -> Bool in
                let tmp: NSString = text as NSString
                let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
            DispatchQueue.main.async {
                if(self.filtered.count == 0){
                    self.searchActive = false;
                    self.searchTableView.isHidden = true
                } else {
                    self.searchActive = true;
                    self.searchTableView.isHidden = false
                    //self.myCollectionViewHeight.constant = 1
                }
                self.tableView.reloadData()
                self.searchTableView.reloadData()
                self.collectionView.reloadData()
                self.viewDidLayoutSubviews()
            }
            */
        }
}

extension ContactsViewController{
    
    func removeDuplicateElements(post: [Transactions]) -> [Transactions] {
        var uniquePosts = [Transactions]()
        for posts in post {
            if !uniquePosts.contains(where: {$0.toName == posts.toName }) {
                uniquePosts.append(posts)
            }
        }
        return uniquePosts
    }
}
