//
//  ScannerViewController.swift
//  CSC PAY
//
//  Created by Abhishek Ranjan on 07/01/21.
//

import Foundation
import AVFoundation
import UIKit

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    let inset: CGFloat = 10
    let minimumLineSpacing: CGFloat = 10
    let minimumInteritemSpacing: CGFloat = 10
    let cellsPerRow = 5
    let cellsPerColumn = 1
    var recents = [Transactions]()
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var borderView: UIImageView!
    @IBOutlet weak var scanView: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var blinkView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var lblRecent: UILabel!
    @IBOutlet weak var viewNoRecents: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular,textColor: .customWhite, text: Language.getText(with: LanguageConstants.lbl_scan_pay_title))
        textField.setup(fontSize: 16.0, fontType: .regular, textColor: .subtitleColor, bgColor: .lightGreyColor, text: nil, placeholderText: Language.getText(with: LanguageConstants.lbl_enter_mobile_placeholder))
        
        if let recent = CoredataManager.getSpecificRecord(Transactions.self, key: "type", value: "P") {
            self.recents = recent
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        setupUI()
        setupResend()
        if !Constants.isSimulator { addScanner() }
    }
    func setupResend() {
        //self.lblNoData.halfTextColorChange(fullText: Language.getText(with: LanguageConstants.lbl_resend_otp) ?? Constants.otpResend, changeText: Language.getText(with: LanguageConstants.btn_resend_otp) ?? Constants.otpResendBtn, color: .lightBlue, textSize: 11)
        self.lblNoData.halfTextColorChange(fullText: "People who you’ve recently paid will show up here. Find people to pay", changeText: "Find people to pay", color: .lightBlue, textSize: 11)
        self.lblNoData.isUserInteractionEnabled = true
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnLabel(_ :)))
        tapgesture.numberOfTapsRequired = 1
        self.lblNoData.addGestureRecognizer(tapgesture)
    }
    //MARK:- tappedOnLabel
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = self.lblNoData.text else { return }
        let termsRange = (text as NSString).range(of: "Find people to pay")
        if gesture.didTapAttributedTextInLabel(label: lblNoData, inRange: termsRange) {
            DispatchQueue.main.async {
                self.view.endEditing(true)
                let vc = getContactsViewController() as! ContactsViewController
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func qrGenerateTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.present(getGenerateQRViewController(), animated: true, completion: nil)
        }
    }
    @IBAction func torchBtnTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.toggleFlash() }
    }
    @IBAction func selectImageBtnTapped(_ sender: Any) {
        DispatchQueue.main.async {
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
           //print("Button capture")
           let imagePicker = UIImagePickerController()
           imagePicker.delegate = self
           imagePicker.sourceType = .savedPhotosAlbum
           imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
    }
    /*
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            let screenSize = scanView.bounds.size
            if let touchPoint = touches.first {
                let x = touchPoint.location(in: scanView).y / screenSize.height
                let y = 1.0 - touchPoint.location(in: scanView).x / screenSize.width
                let focusPoint = CGPoint(x: x, y: y)
                
                if let device = AVCaptureDevice.default(for: AVMediaType.video) {
                    do {
                        try device.lockForConfiguration()

                        device.focusPointOfInterest = focusPoint
                        //device.focusMode = .continuousAutoFocus
                        device.focusMode = .autoFocus
                        //device.focusMode = .locked
                        device.exposurePointOfInterest = focusPoint
                        device.exposureMode = AVCaptureDevice.ExposureMode.continuousAutoExposure
                        device.unlockForConfiguration()
                    }
                    catch {
                        // just ignore
                    }
                }
            }
        } */
    func toggleFlash() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }

        do {
            try device.lockForConfiguration()

            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }

            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    func addScanner() {
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()

        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
            metadataOutput.rectOfInterest = convertRectOfInterest(rect: cameraView.frame) //imageView.frame
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        //previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        cameraView.layer.addSublayer(previewLayer)
    }
    override func viewDidLayoutSubviews() {
        if !Constants.isSimulator {
        previewLayer.frame = cameraView.bounds
            previewLayer.bounds = CGRect(x: 0, y: 0, width: cameraView.frame.size.width, height: cameraView.frame.size.height) }
    }
    
    func convertRectOfInterest(rect: CGRect) -> CGRect {
        let screenRect = self.view.frame
        let screenWidth = screenRect.width
        let screenHeight = screenRect.height
        let newX = 1 / (screenWidth / rect.minX)
        let newY = 1 / (screenHeight / rect.minY)
        let newWidth = 1 / (screenWidth / rect.width)
        let newHeight = 1 / (screenHeight / rect.height)
        return CGRect(x: newX, y: newY, width: newWidth, height: newHeight)
    }

    func failed() {
        let ac = UIAlertController(title: Language.getText(with: LanguageConstants.lbl_scan_not_supported_title) ?? Constants.lbl_scan_not_supported_title, message: Language.getText(with: LanguageConstants.lbl_scan_not_supported_message) ?? Constants.lbl_scan_not_supported_message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: { (index) in
            DispatchQueue.main.async {
               // self.dismiss(animated: true) {
                    self.captureSession.startRunning()
                //}
            }
        }))
        present(ac, animated: true)
        self.captureSession.stopRunning()
        captureSession = nil
    }
    func presentPayConfirm(name: String, mobile : String , extra: String...) {
        DispatchQueue.main.async {
            if let vc = getPayConfirmViewController() as? PayConfirmViewController {
                vc.name = name
                vc.mobile = mobile
                vc.extra = extra
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    func success(msg: String) {
        let component = msg.split(separator: "&")
        let upi = String(component[0]).split(separator: "=")
        let name = String(component[1]).split(separator: "=")
        var payeeName =  name[1].replacingOccurrences(of: "%20", with: " ")
        var mc :String?
        var mode :String?
        var purpose :String?

        if component.count <= 5 {
            if component.count == 5{
                mc = String(component[2])
                mode = String(component[3])
                purpose = String(component[4])
            }else{
                mc = ""
                mode = ""
                purpose = ""
            }
            presentPayConfirm(name: payeeName, mobile: String(upi[1]) , extra: mc ?? "" ,mode ?? "" , purpose ?? "")
        } else {
            invalid()
        }
        return
        let ac = UIAlertController(title: "Data", message: msg, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: { (index) in
            DispatchQueue.main.async {
                //self.dismiss(animated: true) {
                    self.present(getPayConfirmViewController(), animated: true, completion: nil)
               // }
            }
        }))
        present(ac, animated: true)
        //captureSession = nil
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
        qrImageView.blink()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }

        //dismiss(animated: true)
    }
    func invalid() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1.3) { [weak self] in
                            guard let self = self else { return print("gotchya!") }
                self.borderView.image = UIImage.init(named: "scanRed")
                self.qrImageView.image = UIImage.init(named: "QRred")
                self.responseLabel.text = "Wrong QR, Try again."
                    }
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            UIView.animate(withDuration: 1.3) { [weak self] in
                guard let self = self else { return print("gotchya!") }
                self.viewWillAppear(true)
                self.responseLabel.text = ""
                self.borderView.image = UIImage.init(named: "Scan")
                self.qrImageView.image = UIImage.init(named: "qr_logo")
                        }
                    }
        }
    }
    func found(code: String) {
        print(code)
        
        if false  {
            invalid()
        } else {
            success(msg: code) }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}

extension ScannerViewController : UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let qrcodeImg = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage {
                let detector:CIDetector=CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy:CIDetectorAccuracyHigh])!
                let ciImage:CIImage=CIImage(image:qrcodeImg)!
                var qrCodeLink=""
      
                let features=detector.features(in: ciImage)
                for feature in features as! [CIQRCodeFeature] {
                    qrCodeLink += feature.messageString!
                }
                
                if qrCodeLink=="" {
                    //print("nothing")
                    failed()
                }else{
                    DispatchQueue.main.async {
                        self.found(code: qrCodeLink)
                    }
                    //print("message: \(qrCodeLink)")
                }
            }
            else{
               print("Something went wrong")
            }
           self.dismiss(animated: true, completion: nil)
    }
}

extension ScannerViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if recents.count == 0 {
            self.viewNoRecents.isHidden = false
        } else {
            self.viewNoRecents.isHidden = true
        }
        return recents.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: "RecentCell", for: indexPath) as! RecentCell
        let item = recents[indexPath.row]
        cell.text = item.toName
        
        cell.lblNameTag.text = item.toName?.first?.uppercased() ?? "C"
        if indexPath.row % 2 == 0 {
            cell.lblNameTag.backgroundColor = .systemIndigo
        } else {
            cell.lblNameTag.backgroundColor = .systemGreen
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        DispatchQueue.main.async {
            let item    = self.recents[indexPath.row]
            let vc      = getPayConfirmViewController() as! PayConfirmViewController
            vc.item     = item
            self.present(vc, animated: true, completion: nil)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
                let marginsAndInsets = inset * 2 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
                let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
                let marginsAndInsetsForColumn = inset * 2 + collectionView.safeAreaInsets.top + collectionView.safeAreaInsets.bottom + minimumLineSpacing * CGFloat(cellsPerColumn - 1)
                let itemHeight = ((collectionView.bounds.size.height - marginsAndInsetsForColumn ) / CGFloat(cellsPerColumn)).rounded(.down)
                return CGSize(width: itemWidth, height: 85)
        
           // return CGSize(width: 320, height: 90)
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
        super.viewWillTransition(to: size, with: coordinator)
    }
    
}
extension ScannerViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.async {
            self.view.endEditing(true)
            self.present(getContactsViewController(), animated: true, completion: nil)
        }
    }
}

/*
 func createScanningIndicator() {
    
    let height: CGFloat = 15
    let opacity: Float = 0.4
    let topColor = UIColor.green.withAlphaComponent(0)
    let bottomColor = UIColor.green

    let layer = CAGradientLayer()
     layer.cornerRadius = 8
    layer.colors = [topColor.cgColor, bottomColor.cgColor]
    layer.opacity = opacity
    
    let squareWidth = view.frame.width * 0.6
    let xOffset = view.frame.width * 0.2
    let yOffset = view.frame.midY - (squareWidth / 2)
     layer.frame = CGRect(x: borderView.frame.origin.x + 20, y: borderView.frame.origin.y + (borderView.frame.height / 2) - 20, width: borderView.frame.width, height: borderView.frame.height) // borderView.frame // CGRect(x: xOffset, y: yOffset, width: squareWidth, height: height)
     layer.bounds = borderView.bounds
    self.view.layer.addSublayer(layer)// insertSublayer(layer, at: 0)

    let initialYPosition = layer.position.y
     let finalYPosition = borderView.frame.origin.y - 20//+ height // initialYPosition + squareWidth - height
     let duration: CFTimeInterval = 1.5

    let animation = CABasicAnimation(keyPath: "position.y")
    animation.fromValue = finalYPosition as NSNumber// initialYPosition as NSNumber
    animation.toValue =  initialYPosition as NSNumber// finalYPosition as NSNumber
    animation.duration = duration
     animation.repeatCount = .infinity
     animation.autoreverses = true
    animation.isRemovedOnCompletion = false
    
    layer.add(animation, forKey: nil)
}
 func createScanningFrame() {
             
     let lineLength: CGFloat = 15
     let squareWidth = view.frame.width * 0.6
     let topLeftPosX = view.frame.width * 0.2
     let topLeftPosY = view.frame.midY - (squareWidth / 2)
     let btmLeftPosY = view.frame.midY + (squareWidth / 2)
     let btmRightPosX = view.frame.midX + (squareWidth / 2)
     let topRightPosX = view.frame.width * 0.8
     
     let path = UIBezierPath()
     
     //top left
     path.move(to: CGPoint(x: topLeftPosX, y: topLeftPosY + lineLength))
     path.addLine(to: CGPoint(x: topLeftPosX, y: topLeftPosY))
     path.addLine(to: CGPoint(x: topLeftPosX + lineLength, y: topLeftPosY))

     //bottom left
     path.move(to: CGPoint(x: topLeftPosX, y: btmLeftPosY - lineLength))
     path.addLine(to: CGPoint(x: topLeftPosX, y: btmLeftPosY))
     path.addLine(to: CGPoint(x: topLeftPosX + lineLength, y: btmLeftPosY))

     //bottom right
     path.move(to: CGPoint(x: btmRightPosX - lineLength, y: btmLeftPosY))
     path.addLine(to: CGPoint(x: btmRightPosX, y: btmLeftPosY))
     path.addLine(to: CGPoint(x: btmRightPosX, y: btmLeftPosY - lineLength))

     //top right
     path.move(to: CGPoint(x: topRightPosX, y: topLeftPosY + lineLength))
     path.addLine(to: CGPoint(x: topRightPosX, y: topLeftPosY))
     path.addLine(to: CGPoint(x: topRightPosX - lineLength, y: topLeftPosY))
     
     let shape = CAShapeLayer()
     shape.path = path.cgPath
     shape.strokeColor = UIColor.white.cgColor
     shape.lineWidth = 3
     shape.fillColor = UIColor.clear.cgColor
     
     self.view.layer.insertSublayer(shape, at: 0)
 }
 func addBackButton() {
     let button = UIButton(frame: CGRect(x: 10, y: 10, width: 32, height: 32))
     button.backgroundColor = .lightGray
     
     button.cornerRadius = 16
     if let image = UIImage.init(systemName: "xmark") {
         button.setImage(image, for: .normal)
         button.tintColor = .white
         button.contentVerticalAlignment = .fill
         button.contentHorizontalAlignment = .fill
         button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
         //button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
     } else {
         button.setTitle("Back", for: .normal)
     }
       button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)

       self.view.addSubview(button)
 }
 @objc func buttonAction(sender: UIButton!) {
     DispatchQueue.main.async {
         //self.dismissDetail()
         if self.navigationController != nil {
             self.navigationController?.dismiss(animated: true, completion: nil)
         } else {
             self.dismiss(animated: true, completion: nil) }
     }
 }
 func addSelectImage() {
     let selectImageButton = UIButton(frame: CGRect(x: UIScreen.main.bounds.width - 50, y: borderView.frame.midY + 60, width: 32, height: 32))
     selectImageButton.backgroundColor = .init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
     
     selectImageButton.cornerRadius = 8
     if let image = UIImage.init(systemName: "arrow.up.doc.fill") {
         selectImageButton.setImage(image, for: .normal)
         selectImageButton.tintColor = .white
         selectImageButton.contentVerticalAlignment = .fill
         selectImageButton.contentHorizontalAlignment = .fill
         selectImageButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
         //button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
     } else {
         selectImageButton.setTitle("T", for: .normal)
     }
     selectImageButton.addTarget(self, action: #selector(selectImageAction), for: .touchUpInside)
       self.view.addSubview(selectImageButton)
 }
 @objc func selectImageAction(sender: UIButton!) {
     if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
        print("Button capture")
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = false
        present(imagePicker, animated: true, completion: nil)
    }
 }

 func addTorchButton() {
     let torchButton = UIButton(frame: CGRect(x: UIScreen.main.bounds.width - 50, y: borderView.frame.midY, width: 32, height: 32))
     torchButton.backgroundColor = .init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
     
     torchButton.cornerRadius = 8
     if let image = UIImage.init(systemName: "flashlight.on.fill") {
         torchButton.setImage(image, for: .normal)
         torchButton.tintColor = .white
         torchButton.contentVerticalAlignment = .fill
         torchButton.contentHorizontalAlignment = .fill
         torchButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
         //button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
     } else {
         torchButton.setTitle("T", for: .normal)
     }
     torchButton.addTarget(self, action: #selector(torchButtonAction), for: .touchUpInside)

       self.view.addSubview(torchButton)
 }
 @objc func torchButtonAction(sender: UIButton!) {
     toggleFlash()
 }
 */
