//
//  MakeRequestViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 22/02/21.
//

import UIKit

class IncomingRequestViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var lblAlertTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    var requests = [Transactions]()
    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular, text: Language.getText(with: LanguageConstants.lbl_request_title))
        
        lblAlertTitle.setup(text: Language.getText(with: LanguageConstants.lbl_decline_alert_title))
        lblName.setup(fontSize: 20, fontType: .semiBold, textColor: .customRed)
        lblMessage.setup()
        btnOk.setup(textColor: .customWhite, bgColor: .taleColor, text: Language.getText(with: LanguageConstants.btn_decline_alert_ok))
        
        if let recent = CoredataManager.getSpecificRecord(Transactions.self, key: "type", value: "R") {
            self.requests = recent
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func closeButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.alertView.isHidden = true
        }
    }
    @IBAction func okButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.alertView.isHidden = true
        }
    }
}

extension IncomingRequestViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.requests.count == 0 || Constants.historyEmptyTest {
            //self.ViewHeader.isHidden = true
            tableView.setEmptyMessage(UIImage(named: "historyNotFound")!)
            return 0
        } else {
            tableView.restore()
            //self.ViewHeader.isHidden = false
            return self.requests.count
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RequestsCell", for: NSIndexPath(row: 0, section: 0) as IndexPath) as! RequestsCell
        let item = self.requests[indexPath.row]
        cell.lblRequestedFrom.text = "Requested by \(item.toName ?? "")"
        let time = item.addedAt?.getCurrentDate(format: "dd MMM yyyy, hh:mma").0
        cell.lblTime.text = time
        if let time = getMinutesDifferenceFromTwoDates(start: Date(), end: item.expiry ?? Date()) {
            cell.lblExpiry.text = "Expires in \(time)"
        } else {
            cell.lblExpiry.text = "Expired"
        }
        cell.lblAmount.text = "₹ \(item.amount)"
        
        return cell
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.requests[indexPath.row]
        let vc = getRequestDetailsViewController() as! RequestDetailsViewController
        vc.transaction = item
        self.present(vc, animated: true, completion: nil)
    }
}

extension IncomingRequestViewController : ButtonActionDelegate {
    func acceptButtonTapped(indexPath: IndexPath) {
        DispatchQueue.main.async {
            let vc = getPayConfirmViewController() as! PayConfirmViewController
            vc.isFromPendingRequest = true
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func rejectButtonTapped(indexPath: IndexPath) {
        DispatchQueue.main.async {
            //let vc = self.parent as! RequestViewController
            //vc.
            self.alertView.isHidden = false
            //notImplementedYetAlert(base: self)
        }
    }
    
    
}

extension IncomingRequestViewController{
    
    func incomingRequest(){
        
    }
    
    
    
}
