//
//  TicketsViewController.swift
//  CSCPAY
//
//  Created by Aman Pandey on 26/08/21.
//

import UIKit

class TicketsViewController: UIViewController , DetailButtonDelegate {
    
    

    @IBOutlet weak var ticketsTableView: UITableView!
    @IBOutlet weak var filtersButton: UIButton!
    @IBOutlet weak var openButton: UIButton!
    @IBOutlet weak var closedButton: UIButton!
    @IBOutlet weak var filterView : UIView!
    @IBOutlet weak var openedView : UIView!
    @IBOutlet weak var closedView : UIView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var buttonStackView: UIStackView!
    
    var selectedIndex : IndexPath?
    
    
    var cityModelArr = [CityModel]()
    
    var disputeList = [ListofCustDispute]()
    var disputeObj : DisputeList?

    
    let dropDown = MakeDropDown()
    var dropDownRowHeight: CGFloat = 50
    
    var month = ["Last Month" , "Last 3 Months" , "Last 6 Months"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        dropDown.makeDropDownDataSourceProtocol = self
        populateCityModelArray()
        ticketList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setUpDropDown()
    }
    
    
    func populateCityModelArray(){
        let cityModel1 = CityModel(isOpened: true)
        let cityModel2 = CityModel(isOpened: true )
        let cityModel3 = CityModel(isOpened: true )
        let cityModel4 = CityModel(isOpened: true )
        let cityModel5 = CityModel(isOpened: false)
        let cityModel6 = CityModel(isOpened: true)
        let cityModel7 = CityModel(isOpened: false)
        let cityModel8 = CityModel(isOpened: false)
        let cityModel9 = CityModel(isOpened: true)
        let cityModel10 = CityModel(isOpened: false)
        
        self.cityModelArr.append(cityModel1)
        self.cityModelArr.append(cityModel2)
        self.cityModelArr.append(cityModel3)
        self.cityModelArr.append(cityModel4)
        self.cityModelArr.append(cityModel5)
        self.cityModelArr.append(cityModel6)
        self.cityModelArr.append(cityModel7)
        self.cityModelArr.append(cityModel8)
        self.cityModelArr.append(cityModel9)
        self.cityModelArr.append(cityModel10)
    }
    
    
    
    
    func setUpDropDown(){
        dropDown.makeDropDownIdentifier = "DROP_DOWN_NEW"
        dropDown.cellReusableIdentifier = "dropDownCell"
        dropDown.makeDropDownDataSourceProtocol = self
        dropDown.setUpDropDown(viewPositionReference: filterView.frame, offset: 0)
        dropDown.nib = UINib(nibName: "DropDownCell", bundle: nil)
        dropDown.setRowHeight(height: self.dropDownRowHeight)
        self.buttonStackView.addSubview(dropDown)
    }
    
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func filterButtonTapped(_ sender: UIButton){
        self.dropDown.showDropDown(height: self.dropDownRowHeight * CGFloat(month.count))
    }
    
    @IBAction func openButtonTapped(_ sender: UIButton){
        if openButton.tag == 0 {
            openButton.tag = 1
            openedView.backgroundColor = .customBlue
            openButton.setTitleColor(.customWhite, for: .normal)
            closedButton.tag = 0
            closedView.backgroundColor = .noColor
            closedButton.setTitleColor(.customBlue, for: .normal)
        } else {
            openButton.tag = 0
            openedView.backgroundColor = .noColor
            openButton.setTitleColor(.customBlue, for: .normal)
            self.ticketsTableView.reloadData()
        }
    }
    
    @IBAction func closedButtonTapped(_ sender: UIButton){
        if closedButton.tag == 0 {
            closedButton.tag = 1
            closedView.backgroundColor = .customBlue
            closedButton.setTitleColor(.customWhite, for: .normal)
            
            openButton.tag = 0
            openedView.backgroundColor = .noColor
            openButton.setTitleColor(.customBlue, for: .normal)
        } else {
            closedButton.tag = 0
            closedView.backgroundColor = .noColor
            closedButton.setTitleColor(.customBlue, for: .normal)
            self.ticketsTableView.reloadData()
        }
    }
    
    
    
}


//MARK: - DropDown
extension TicketsViewController : MakeDropDownDataSourceProtocol{
    
    func getDataToDropDown(cell: UITableViewCell, indexPos: Int, makeDropDownIdentifier: String) {
        if makeDropDownIdentifier == "DROP_DOWN_NEW"{
            let customCell = cell as! DropDownCell
            customCell.monthLabel.text = self.month[indexPos]
            print("test")
        }
    }
    
    func numberOfRows(makeDropDownIdentifier: String) -> Int {
        return self.month.count
    }
    
    func selectItemInDropDown(indexPos: Int, makeDropDownIdentifier: String) {
        let text = month[indexPos]
        self.filtersButton.setTitle(text, for: .normal)
        self.dropDown.hideDropDown()
    }
}

//MARK: - TableView Delegate & DataSource Methods
extension TicketsViewController : UITableViewDelegate , UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
  
    

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard disputeList.count > 0 else {
            self.viewHeader.isHidden = true
            tableView.setEmptyMessage(UIImage(named: "historyNotFound")!)
            return 0
        }
        tableView.restore()
        self.viewHeader.isHidden = false
    return disputeList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard disputeList.count > 0 else{
            return UITableViewCell()
        }
        if disputeList[indexPath.row].status == "O"{
            let cell : OpenTicketCell = ticketsTableView.dequeueReusableCell(withIdentifier: "OpenTicketCell", for: indexPath) as! OpenTicketCell
            cell.delegate = self
            return cell
        }else{
            let cell : ClosedTicketsCell = ticketsTableView.dequeueReusableCell(withIdentifier: "ClosedTicketsCell", for: indexPath) as! ClosedTicketsCell
            cell.delegate = self
            return cell
        }
    }
        
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func detailButtonTapped(indexPath: IndexPath) {
        print(indexPath)
        ticketStatus()
    }
}

//MARK: - Api Calling
extension TicketsViewController {
    
    func ticketList(){
        APIManager.dispute(DisputeList.self, reqAction: "getDisputeList", pspId: "IBL", upiTranRefNo: "860247", ticketNo: "4064", requestType: "A", statusIs: "O", disputeRemark: "test", disputeType: "U008", custRefNo: "031015064387") { res, err in
            print(res)
            print(err)
            DispatchQueue.main.async {
                if err == nil {
                    self.disputeList.removeAll()
                    if let result = res , let list = result.listofCustDispute{
                        for i in list{
                            self.disputeList.append(i)
                        }
                        self.ticketsTableView.reloadData()
                    }
                    
                }
            }
            
        }
    }
    
    
    func ticketStatus(){
        APIManager.dispute(DisputeList.self, reqAction: "getdisputestatus", pspId: "IBL", upiTranRefNo: "860247", ticketNo: "4064") { res, err in
            print(res)
            print(err)
            print("-------")
        }
    }
}



struct CityModel {
    var isOpened: Bool
}
