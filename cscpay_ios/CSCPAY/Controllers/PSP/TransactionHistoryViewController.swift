//
//  TransactionHistoryViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 09/02/21.
//

import UIKit

class TransactionHistoryViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblHistorySubtitle: UILabel!
    @IBOutlet weak var lblDesctiption: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var paidBtn: UIButton!
    @IBOutlet weak var receivedBtn: UIButton!
    @IBOutlet weak var paidView: UIView!
    @IBOutlet weak var receivedView: UIView!
    @IBOutlet weak var ViewHeader: UIView!
   
    
    
    
    var isPaid = false
    var isReceived = false
    var isMerchant = false
    var transactions = [User_Transaction]()
    var transactionHistory : [TransactionHistoryDetails]?
    var his : [Transaction_History]?
    var recent : [Transaction_History]?
    var isFrom : fromScreen = .isPaid
    
    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular,text: Language.getText(with: LanguageConstants.lbl_history_title))
        lblHistorySubtitle.setup(text: Language.getText(with: LanguageConstants.lbl_history_subtitle))
        lblDesctiption.setup(fontSize: 14, textColor: .subtitleColor,text: Language.getText(with: LanguageConstants.lbl_history_description))
        paidBtn.setup(fontSize: 16, fontType: .regular, textColor: .customBlue, bgColor: .clear, text: Language.getText(with: LanguageConstants.btn_paid))
        receivedBtn.setup(fontSize: 16, fontType: .regular, textColor: .customBlue, bgColor: .clear, text: Language.getText(with: LanguageConstants.btn_received))
        ViewHeader.backgroundColor = .bgBlue
        if !isMerchant {
        if let recent = CoredataManager.getALLDetails(Transaction_History.self) {
            self.his = recent
            self.recent = recent
        }
//           if let historyTrns = CoredataManager.getDetails(Transaction_History.self){
//                self.recent = historyTrns
//               self.his = recent
//           }else{
//               receivedHistory()
//           }
            //for check only
            receivedHistory()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    @IBAction func paidBtnTapped(_ sender: Any) {
        if paidBtn.tag == 0 {
            paidBtn.tag = 1
            paidView.backgroundColor = .customBlue
            paidBtn.setTitleColor(.customWhite, for: .normal)
            
            
            isPaid = true //Test
            isReceived = false
          /*
            if let recent = CoredataManager.getSpecificRecord(Transactions.self, key: "type", value: "P") {
                self.transactions = recent
            }
           */
            if let paid = his?.filter({$0.drCrFlag == "D"}){
                self.his = paid
                self.tableView.reloadData()
            }
            receivedBtn.tag = 0
            receivedView.backgroundColor = .noColor
            receivedBtn.setTitleColor(.customBlue, for: .normal)
        } else {
            paidBtn.tag = 0
            paidView.backgroundColor = .noColor
            paidBtn.setTitleColor(.customBlue, for: .normal)
            isPaid = false //Test
//            if let recent = CoredataManager.getALLDetails(Transactions.self) {
//                self.transactions = recent
//            }
            self.his = recent
            self.tableView.reloadData()
        }
    }
    
    @IBAction func receivedBtnTapped(_ sender: Any) {
        if receivedBtn.tag == 0 {
            receivedBtn.tag = 1
            receivedView.backgroundColor = .customBlue
            receivedBtn.setTitleColor(.customWhite, for: .normal)
            isReceived = true //Test
            isPaid = false
            
            paidBtn.tag = 0
            paidView.backgroundColor = .noColor
            paidBtn.setTitleColor(.customBlue, for: .normal)
            /*if let recent = CoredataManager.getSpecificRecord(Transactions.self, key: "type", value: "R") {
                self.transactions = recent
            }
            */
            if let received = his?.filter({$0.drCrFlag == "C"}) {
                self.his = received
                self.tableView.reloadData()
            }
        } else {
            receivedBtn.tag = 0
            receivedView.backgroundColor = .noColor
            receivedBtn.setTitleColor(.customBlue, for: .normal)
            isReceived = false // Test
            his = recent
            self.tableView.reloadData()
        }
        
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension TransactionHistoryViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        //if(section == 0) {
            //return nil //As you do not have to display header in 0th section, return nil
            let reuseIdentifier : String!
        reuseIdentifier = String(format: "FAQHeaderCell")

            let headerView = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath)

            return headerView
        //}
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if let trans = his{
            if trans.count == 0 || Constants.historyEmptyTest{
                return 0
            }

        }
        return 25

        
//        if self.transactionHistory!.count == 0 || Constants.historyEmptyTest {
//            return 0
//        }
//        return 25
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        if self.transactions.count == 0 || Constants.historyEmptyTest {
//            self.ViewHeader.isHidden = true
//            tableView.setEmptyMessage(UIImage(named: "historyNotFound")!)
//            return 0
//        } else {
//            tableView.restore()
//            self.ViewHeader.isHidden = false
//            return self.transactions.count
//        }
        
        guard let count = his?.count else {
            self.ViewHeader.isHidden = true
            tableView.setEmptyMessage(UIImage(named: "historyNotFound")!)
            return 0
        }
        tableView.restore()
        self.ViewHeader.isHidden = false
        return count

        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier : String!
    reuseIdentifier = String(format: "TransactionHistoryCell") // historyCell

        let historyCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath) as? TransactionHistoryCell
        /*
        let item = self.transactions[indexPath.row]
        let isSuccess = item.status == "success" ? true : false
        let isPaid       = item.type == "P" ? true : false
        historyCell?.setupValue(isPaid: isPaid, isSuccess : isSuccess, person: item.toName ?? "Saloni Rao", date: "\(String(describing: item.addedAt ?? Date()))", amount: "\(item.amount)", img: UIImage.init(named: "iaxis")!) // // "05 Sep 2020, 02:05pm"
        */
        
   
        
        
        if let item = self.his{
            let items = item[indexPath.row]
            
            let isSuccess = items.txnStatus == "SUCCESS" ? true : false
            let isPaid    = items.drCrFlag == "D" ? true : false
            historyCell?.setupValue(isPaid: isPaid, isSuccess : isSuccess, person: items.payerName ?? "Saloni Rao", date: items.trnDate ?? "", amount: "\(items.txnAmount ?? "0")", img: UIImage.init(named: "iaxis")!) // // "05 Sep 2020, 02:05pm"
        }
     
        
        /*
        if indexPath.row % 2 == 0 {
            if isPaid {
                historyCell?.setupValue(person: "Saloni Rao", date: "05 Sep 2020, 02:05pm", amount: "250", img: UIImage.init(named: "iaxis")!)
            } else if isReceived {
                historyCell?.setupValue(isPaid:  false,isSuccess: false, person: "Gowtham", date: "05 Sep 2020, 02:05pm", amount: "180", img: UIImage.init(named: "iaxis")!)
            } else {
                    historyCell?.setupValue(person: "Saloni Rao", date: "05 Sep 2020, 02:05pm", amount: "250", img: UIImage.init(named: "iaxis")!)}
        } else {
            if isPaid {
                historyCell?.setupValue(person: "Saloni Rao", date: "05 Sep 2020, 02:05pm", amount: "250", img: UIImage.init(named: "iaxis")!)
            } else if isReceived {
                historyCell?.setupValue(isPaid:  false, person: "Saloni Rao", date: "05 Sep 2020, 02:05pm", amount: "250", img: UIImage.init(named: "iaxis")!)
            } else {

                    historyCell?.setupValue(isPaid:  false,person: "Gowtham", date: "05 Sep 2020, 02:05pm", amount: "180", img: UIImage.init(named: "iaxis")!)
            }
        } */
        if indexPath.row == 3 {
            historyCell?.viewSeperator.backgroundColor = .clear
        }
        return historyCell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = getTransactionDetialsViewController() as! TransactionDetialsViewController
        switch isFrom{
        case .isPaid:
            vc.isFrom = .isPaid
        case .isHistory:
            vc.isFrom = .isHistory
            vc.transactionDetails = self.transactionHistory?[indexPath.row]
            vc.transDetails = self.his?[indexPath.row]
        case .isMerchant:
            vc.isFrom = .isMerchant
        case .isFromReceive:
            vc.isFrom = .isFromReceive
        case .isFailed:
            vc.isFrom = .isFailed
        }
        
//        vc.transaction = self.transactions[indexPath.row]
        
        
      //  vc.transactionDetails = self.transactionHistory![indexPath.row]
        
//        if indexPath.row % 2 == 0 {
//            vc.isFailed = false
//        } else {
//            vc.isFailed = true
//        }
        
        
        self.present(vc, animated: true, completion: nil)
    }
    // MARK:- Delete cells
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if (editingStyle == .delete) {
//            // handle delete (by removing the data from your array and updating the tableview)
//        }
//    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let raiseDespute = UIContextualAction(style: .normal, title: "") { _, _, complete  in
            guard let dispute = self.transactionHistory?[indexPath.row]else{return}
            self.raiseDispute(custRefNo: dispute.custRefNo!, upiTranRefNo: "860247", disputeRemark: "test", disputeType: "U008")
            complete(true)
        }
        
        raiseDespute.image = UIImage(named: "dispute")
        raiseDespute.backgroundColor = .white
        let configuration = UISwipeActionsConfiguration(actions: [raiseDespute])
        configuration.performsFirstActionWithFullSwipe = true
         return configuration
    }
    
}

extension TransactionHistoryViewController{
    func receivedHistory() {
        self.showActivityIndicator()
        APIManager.call(TransactionHistoryResponse.self, urlString: "indus/transact/history", reqAction: "getHistory", pspId: "IBL", extra: "IndusInd" ) { res, err in
            self.hideActivityIndicator()
            
            DispatchQueue.main.async {
                if err == nil{
                    if let result = res , let transactionHistroy = result.transDetails{
                        CoredataManager.cleanEntity(Transaction_History.self)
                        CoredataManager.saveTransactionHistory(transactions: transactionHistroy)
                        
                        //CoredataManager.saveTransactionHistory(transactions: transactionHistroy)
                        
                        self.transactionHistory = transactionHistroy
                        self.his = CoredataManager.getDetails(Transaction_History.self)
                        self.recent = self.his
                        print(self.his ?? "NA")
                        
                        self.tableView.reloadData()
                        print(res)
                    }
                }
            }
            
            
           
            
        }
}
    
    
    
    
    
    func raiseDispute(custRefNo: String , upiTranRefNo: String , disputeRemark: String , disputeType: String ){
        APIManager.dispute(DisputeList.self, reqAction: "raiseDispute", pspId: "IBL", upiTranRefNo: upiTranRefNo, disputeRemark: disputeRemark, disputeType: disputeType, custRefNo: custRefNo) { res, err in
            DispatchQueue.main.async {
                if err == nil{
                    GlobalData.sharedInstance.raiseDispute = res
                    let vc = getChatViewController() as! ChatViewController
                    vc.raiseDispute = res
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
}
/*
 func openAlert() {
     let alertController = UIAlertController(title: "", message: nil, preferredStyle: UIAlertController.Style.actionSheet)

//            let margin:CGFloat = 10.0
//            let rect = CGRect(x: margin, y: margin, width: alertController.view.bounds.size.width - margin * 4.0, height: 120)
//            let customView = UIView(frame: rect)

         //customView.backgroundColor = .red
         //alertController.view.addSubview(customView)

         let somethingAction = UIAlertAction(title: "Something", style: .default, handler: {(alert: UIAlertAction!) in print("something")})

         let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})

         alertController.addAction(somethingAction)
     
     // Accessing alert view backgroundColor :
     alertController.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = UIColor.green
     
     // Accessing buttons tintcolor :
     alertController.view.tintColor = UIColor.white
     
     var height:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 40)
     
         alertController.view.addConstraint(height);
     
     //var bottom:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view, attribute: NSLayoutConstraint.Attribute.bottomMargin, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 0)
     //alertController.view.addConstraint(bottom);
         ///alertController.addAction(cancelAction)

         DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
             self.present(alertController, animated: true, completion:{
                 let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissAlertController))
                         alertController.view.superview?.subviews[0].addGestureRecognizer(tapGesture)
                 
                 DispatchQueue.main.asyncAfter(deadline: .now() + 3.5) {
                     self.dismiss(animated: true, completion: nil)
                 }
             })
         }
 }
 @objc func dismissAlertController()
 {
     self.dismiss(animated: true, completion: nil)
 }
 */
