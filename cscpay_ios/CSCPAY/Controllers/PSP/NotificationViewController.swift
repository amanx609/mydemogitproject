//
//  NotificationViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 19/02/21.
//

import UIKit

class NotificationViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnMarkAsRead: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var read: Bool = false
    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular, text: Language.getText(with: LanguageConstants.lbl_notification_title))
        btnMarkAsRead.setup(fontSize: 15.0, fontType: .semiBold, textColor: .customBlue, bgColor: .clear, text: Language.getText(with: LanguageConstants.btn_mark_as_read))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        self.tableView.tableFooterView = UIView()
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func markAsReadButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.read = true
            self.tableView.reloadData()
        } 
    }
}

extension NotificationViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier : String!
    reuseIdentifier = String(format: "NotificationCell")

        let headerView = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? NotificationCell
        headerView?.setupValues(title: "Request From Anima", msg: "Anima has requested you to pay $50.00 for xyz expenses", profileImg: nil, date: "Yesterday")
        
        if indexPath.row % 2 != 0 || read == true {
            headerView?.contentView.backgroundColor = .white
        } else {
            headerView?.contentView.backgroundColor = hexStringToUIColor(hex: "F8F8F8")
        }
        return headerView ?? UITableViewCell()
    }
    // MARK:- Delete cells
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
    
}
