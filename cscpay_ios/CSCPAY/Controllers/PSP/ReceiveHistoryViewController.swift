//
//  ReceiveHistoryViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 16/06/21.
//

import UIKit

class ReceiveHistoryViewController: UIViewController {

    var requests = [Transactions]()
    override func viewDidLoad() {
        super.viewDidLoad()
        if let recent = CoredataManager.getSpecificRecord(Transactions.self, key: "type", value: "R") {
            self.requests = recent
        }
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension ReceiveHistoryViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        //if(section == 0) {
            //return nil //As you do not have to display header in 0th section, return nil
            let reuseIdentifier : String!
        reuseIdentifier = String(format: "FAQHeaderCell")

            let headerView = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath)

            return headerView
        //}
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if Constants.receiveHistoryEmptyTest {
            return 0
        }
        return 25
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.requests.count == 0 { //} Constants.receiveHistoryEmptyTest {
            tableView.setEmptyMessage(UIImage(named: "historyNotFound")!)
            return 0
        } else {
            tableView.restore()
            return self.requests.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier : String!
    reuseIdentifier = String(format: "RequestHistoryCell") // historyCell

        let historyCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath) as? RequestHistoryCell
        
        let item = self.requests[indexPath.row]
        historyCell?.lblRequestedFrom.text = item.toName
        let date = item.addedAt?.getCurrentDate(format: "dd MMM yyyy, hh:mma") //01 Apr 2020, 02:05pm
        historyCell?.lblTime.text = date?.0 ?? ""
        historyCell?.lblAmount.text = "₹ \(item.amount)"
        if item.amount == 100 {
            print(item.expiry)
        }
        if let time = getMinutesDifferenceFromTwoDates(start: Date(), end: item.expiry ?? Date()) {
            historyCell?.lblExpiry.text = "Expires in \(time)"
        } else {
            historyCell?.lblExpiry.text = "Expired"
        }
        
        return historyCell ?? UITableViewCell()
    }
    func getMinutesDifferenceFromTwoDates(start: Date, end: Date) -> String?
       {
           let diff = Int(end.timeIntervalSince1970 - start.timeIntervalSince1970)

           let hours = diff / 3600
           let minutes = (diff - hours * 3600) / 60
        if hours >= 0 && minutes >= 0 {
            if hours > 0 {
                return "\(hours) hour \(minutes) mins"
            }
            return "\(minutes) mins"
        }
        return nil // (hours * 60) + minutes
       }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = getTransactionDetialsViewController() as! TransactionDetialsViewController
        let item = self.requests[indexPath.row]
        vc.transaction = item
        vc.isFromReceive = true
        
        if indexPath.row % 2 == 0 {
            vc.isFailed = false
        } else {
            vc.isFailed = true
        }
        
        
        self.present(vc, animated: true, completion: nil)
    }
    // MARK:- Delete cells
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
}



    
        
//        (BankList.self,  mobile: "7007587639", reqAction: "getHistory", geoLong: "77.1873", geoLat: "28.4817", location: "Delhi", pspBank: "IndusInd", pspId: "IBL", isCheckDevice : true, saveBank : true) { (res, err, msg) in
//            self.hideActivityIndicator()
//
//            print(res)
//            print(err)
//
//
//
//            DispatchQueue.main.async {
//                if let banks = CoredataManager.getALLDetails(Master_BankList.self, sortBytTime: true, key: "bankName", ascending: true, sortCaseSensitive: true) {
//                    DispatchQueue.main.async {
//                        for name in Constants.Bank_Items{
//                           print(name["title"])
////                            self.popularBankList = banks.filter{$0.bankName, name["title"]}
//                        }
//
//
//
//                    }
//                }
//            }
//        }
    









//reqAction: getHistory  -  get transaction history
//requestType:   QT - Quarterly
//Endpoint:   gw1/indus/transact/history


//Request Format Data:    {"head":{"appName":"CSC Pay","appVer":"1.0","deviceId":"ios11","deviceType":"ios","osVer":"IOS 13.0","refId":"999989487027062115262102","ts":"2021-05-27T02:27:12","reqAction":"getHistory","clientIp":"127.0.0.1","location":"Delhi","geoLat":"19.0911","geoLong":"72.9208","pspId":"IBL","pspData":"ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTBFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiMTAuOTEuMjYuMzQiLA0KCSJibHVldG9vdGhNYWMiOiAiMjI6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUwRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUwRSIsDQoJIm9zIjogIklPUyAxMy4yLjMiLA0KCSJyZWdJZCI6ICJOQSIsDQoJInNlbGVjdGVkU2ltU2xvdCI6ICIwIiwNCgkiZmNtVG9rZW4iOiAiIg0KfQ"},"body":{"pspBank":"IndusInd","mobileNo":"9999894870","virtualAddress":"abhishek@cscindus",  "requestType": "QT"}}
