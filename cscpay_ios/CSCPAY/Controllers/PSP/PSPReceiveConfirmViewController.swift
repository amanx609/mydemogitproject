//
//  ReceiveConfirmViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 21/06/21.
//

import UIKit

class PSPReceiveConfirmViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameTagLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileUpiLabel: UILabel!
    @IBOutlet weak var noteView: UITextView!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var paymentDropdownView: UIView!
    @IBOutlet weak var emojiView: UICollectionView!
    @IBOutlet weak var lblPayFrom: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var imgEmoji: UIImageView!
    @IBOutlet weak var viewBankList: UIView!
    @IBOutlet weak var lblRequestingAmount: UILabel!
    @IBOutlet weak var expiryField: UITextField!
    @IBOutlet weak var lblRequestingName: UILabel!
    @IBOutlet weak var lblRequestUpi: UILabel!
    @IBOutlet weak var addExpiryTimeButton : UIButton!
    @IBOutlet weak var decreaseExpiryTimeButton: UIButton!
    //@IBOutlet weak var lblEmoji: UILabel!
    //@IBOutlet weak var paymentModeDropdown: DropDownTextField!
    @IBOutlet weak var expiryTimeView: UIView!
    

    var isFromRequest : Bool = false
    var isFromPendingRequest : Bool = false
    var titleStr = "Pay"
    var btnTitle = "PROCEED TO PAY"
    var isFromContacts : Bool = false
    var isReceive = true

    //Values
    var name: String?
    var mobile: String?
    var item : Transactions?
    var newTransaction : TransactionHistoryDetails?

    let guestArray : [String]? = ["Pay", "Request"]
    let emojis = ["e1", "e2", "e3", "e4", "e5", "e6" ]
    var currentEmojis = ""
    
    var hours: Int = 0
    var minutes: Int = 0
    var seconds: Int = 0
    var timerIsPaused: Bool = true
    var timer: Timer? = nil
    var expiryType : String?
    
    private let expiryDatePicker = MonthYearPickerView()
    
    private let newMinuteHourPicker = MinuteHourPicker()

    func setupValues() {
        if let names = name {
            self.nameLabel.text = names }
        if let mob = mobile {
            self.mobileUpiLabel.text = mob }
    }

    func setupUI() {
        titleLabel.setup(fontSize: 24, text: titleStr)
        nameLabel.setup(textColor: .customBlack)
        mobileUpiLabel.setup(fontSize: 15, fontType: .semiBold, textColor: .customBlue)
        seperatorView.backgroundColor = .seperatorColor

        amountField.setup(fontSize: 24, fontType: .bold, placeholderText: Language.getText(with: LanguageConstants.txt_amount_placeholder) ?? Constants.txt_amount_placeholder)
        noteView.setup(fontSize: 15, fontType: .regular, textColor: .subtitleColor, bgColor: .customWhite, text: "Make a note")
        lblPayFrom.setup(fontSize: 15, fontType: .regular, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_pay_from) ?? Constants.lbl_pay_from)
        proceedButton.setup(fontType: .semiBold, textColor: .customWhite, bgColor: .taleColor, text:btnTitle)
        if let to = item {
            self.nameLabel.text = to.toName
            self.mobileUpiLabel.text = to.to
//            self.amountField.text = "\(to.amount)"
//            if let note = to.remark {
//                if note.count != 0 {
//                    self.noteView.text = note
//                }
//            }
        }
        
        if let repeatTransaction = newTransaction{
            self.nameLabel.text = repeatTransaction.payeeName
            self.mobileUpiLabel.text = repeatTransaction.payerVirtualAddress
        }
        
        if isReceive {
            self.titleLabel.text = "Receive"
            self.lblPayFrom.text = "Receive In"
        }
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(gesture:)))
        longPress.minimumPressDuration = 0.3
        self.addExpiryTimeButton.addGestureRecognizer(longPress)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupUI()
        setupValues()
        //paymentModeDropdown.dropDownDelegate = self
        //updateDropdown(index : 0)
        //self.titleLabel.text = titleStr
        //self.proceedButton.setTitle(btnTitle, for: .normal)
        //self.noteView.textColor = UIColor.lightGray

        DispatchQueue.main.async {
            if self.isFromPendingRequest {
                self.amountField.text = "200"
                self.amountField.isUserInteractionEnabled = false
                self.noteView.isUserInteractionEnabled = false
            } else if self.isFromRequest {
                //self.titleLabel.text = self.titleStr //"Request"
                //self.proceedButton.setTitle(self.btnTitle, for: .normal)
            } else if self.isFromContacts {
                self.paymentDropdownView.isHidden = false
            }
        }
//        expiryField.inputView    = expiryDatePicker
//        expiryDatePicker.onDateSelected = { (hour: String, minute: String, second: String) in
//            let string = "\(hour) : \(minute) : \(second)" // String(format: "%02d/%d", month, (year % 100))
//            self.expiryField.text = string
//        }
        
        
        expiryField.inputView = newMinuteHourPicker
        newMinuteHourPicker.onDateSelected = {(minute : String) in
            self.expiryType = minute
        }
        
        
        
        }/*
    func updateDropdown(index : Int){
            self.paymentModeDropdown.addDropDownasInput(withArray: guestArray ?? [])
            if let ga = guestArray, ga.count > 0 {
                self.paymentModeDropdown.text = ga[index]
            }

    } */
    override func viewDidLayoutSubviews() {
        //noteView.frame.size.width = noteView.intrinsicContentSize.width
    }

    @IBAction func okButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.alertView.isHidden = true
            let vc = getSuccessViewController() as! SuccessViewController
            vc.transaction = self.item
            vc.isFailed = true
            vc.isFromReceive = true
            self.present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func closeAlertBtnTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.alertView.isHidden = true
        }
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func proceedButtonTapped(_ sender: Any) {
        guard let amount = amountField.text else {
            DispatchQueue.main.async {
                self.responseLabel.text = Language.getText(with: LanguageConstants.alert_enter_amount) ?? Constants.alert_enter_amount
            }
            return
        }
        guard amount.count > 0 else {
            DispatchQueue.main.async {
                self.responseLabel.text = Language.getText(with: LanguageConstants.alert_enter_valid_amount) ??  Constants.alert_enter_valid_amount
            }
            return
        }

        var type = "P"
        if isReceive {
            type = "R"
        }
        
        var expiry = NSCalendar.current.date(byAdding: .minute, value: 30, to: Date(), wrappingComponents: true) ?? Date()
        
        let timeExpiry = expiryField.text
        let component = timeExpiry?.components(separatedBy: " : ")
        if component?.count == 3 {
            let hour = Int(component?[0] ?? "0") ?? 0
            let minute = Int(component?[1] ?? "0") ?? 0
            //let sec = Int(component?[2] ?? "0") ?? 0
            let total = minute + (hour * 60)
            if total >= 1 && total <= 1440 {
                expiry = Date()
                if hour > 0 {
                    let calender = NSCalendar.current
                    expiry = NSCalendar.current.date(byAdding: .hour, value: hour, to: expiry, wrappingComponents: true) ?? Date()
                }
                if minute > 0 {
                    expiry = NSCalendar.current.date(byAdding: .minute, value: minute, to: expiry, wrappingComponents: true) ?? Date()
                }
            }
        }
        var note = ""
        if noteView.textColor == .black {
            note = noteView.text ?? ""
        }
        
       
//       print(utcToLocal(dateStr: "2021-11-08 09:51:02 +0000"))
        let exp = expiry.getLocalTime().1
        print(exp)
        
        let item = CoredataManager.saveTransaction(from: "Gowtham", to: mobile ?? self.mobileUpiLabel.text ?? "nil", type:type, toName: name ?? self.nameLabel.text ?? "nil", amount: Double(amount) ?? 0, remark: note, emoji: self.currentEmojis, status: "success",expiry: exp, isSave: true)
        //let vc = getSuccessViewController() as! SuccessViewController
        //vc.isFromPay = true
        //let vc = getTransactionDetialsViewController() as! TransactionDetialsViewController

        DispatchQueue.main.async {
            self.lblRequestingAmount.text = "₹ \(amount)"
            self.lblRequestingName.text = self.name ?? ""
            self.lblRequestUpi.text = self.mobile ?? ""
            self.alertView.isHidden = false
            self.item = item
        }
        
        if isFromRequest {
            DispatchQueue.main.async {
                self.alertView.isHidden = false
            }
            return
        }

        let vc = getSuccessViewController() as! SuccessViewController
        vc.transaction = item
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func decreaseAmountButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            if let amt = Int(self.amountField.text ?? "0") {
                self.amountField.text = amt <= 99 ? "\(amt)" : "\(amt - 100)"
            } else {
                self.amountField.text = "100"
            }
        }
    }
    @IBAction func increaseAmountButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            if let amt = Int(self.amountField.text ?? "0") {
                self.amountField.text = amt > 99900 ? "\(amt)" : "\(amt + 100)"
            } else {
                self.amountField.text = "100"
            }
        }
    }
    
    @IBAction func increaseTimeButtonTapped(_ sender: Any){
        increaseTime()
    }
    
    @IBAction func decreaseTimeButtonTapped(_ sender : Any){
        decreaseTime()
    }
    
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.viewBankList.isHidden = true
        }
    }
    
    @IBAction func moreAccountListButtonTapped(_ sender: Any) {
        self.viewBankList.isHidden = false
    }
    
    
    @objc func longPress(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began{
            print("LONG TAP")
            newMinuteHourPicker.onDateSelected = {(minute : String) in
                self.expiryType = minute
            }
        }
    }
    
}

extension PSPReceiveConfirmViewController : UITextViewDelegate, UITextFieldDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.subtitleColor {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = Language.getText(with: LanguageConstants.txt_note_placeholder) ?? Constants.txt_note_placeholder
            textView.textColor = UIColor.subtitleColor
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var newText = textView.text!
        newText.removeAll { (character) -> Bool in
            return character == " " || character == "\n"
        }
        guard text.rangeOfCharacter(from: CharacterSet.newlines) == nil else {
                // textView.resignFirstResponder() // uncomment this to close the keyboard when return key is pressed
                return false
            }

        return (newText.count + text.count) <= 100
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string) as String
        if let num = Int(newText), num >= 0 && num <= 100000 {
//            let formatter = NumberFormatter()
//                formatter.numberStyle = NumberFormatter.Style.decimal
//            if let number = Double(textField.text!.replacingOccurrences(of: ",", with: "")) {
//                let result = formatter.string(from: NSNumber(value: number))
//                    textField.text = result!
//            }

            return true
        } else if textField.text?.count ?? 0 <= 1 && newText == "" {
            return true
        } else {
            return false
        }
    }
}

extension PSPReceiveConfirmViewController : DropDownDelegate {
    func dropdownDidChangeValue(textfield: UITextField, title: String, index: Int) {
        /*if textfield == paymentModeDropdown {
            updateDropdown(index : index)
        }*/
    }


}
extension PSPReceiveConfirmViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.emojis.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmojiCell", for: indexPath) as? EmojiCell {
            if let img = UIImage.init(named: "\(self.emojis[indexPath.row])") {
            cell.imgEmoji.image = img
            }
            return cell
        }
        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if let img = UIImage.init(named: "\(self.emojis[indexPath.row])") {
                self.imgEmoji.image = img
                self.currentEmojis  = self.emojis[indexPath.row]
                self.imgEmoji.isHidden = false
            } else {
                self.imgEmoji.image = UIImage()
                self.currentEmojis  = ""
                self.imgEmoji.isHidden = true
            }
            UIView.animate(withDuration: 0.6, animations: {
                self.imgEmoji.frame.origin.x -= 100
            }) {_ in
                UIView.animateKeyframes(withDuration: 0.6, delay: 0.20, options: .beginFromCurrentState, animations: {
                    self.imgEmoji.frame.origin.x += 100
                }, completion: {_ in
                    UIView.animateKeyframes(withDuration: 0.6, delay: 0.25, options: .beginFromCurrentState, animations: {
                        self.imgEmoji.frame.origin.x += 100
                    }) { (_) in
                        UIView.animate(withDuration: 0.6, animations: {
                            self.imgEmoji.frame.origin.x -= 100
                        })
                    }
                })
            }

        }

    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.imgEmoji.image = UIImage()
            self.currentEmojis  = ""
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = (collectionView.bounds.size.width) / CGFloat(6).rounded(.down)
            return CGSize(width: width, height: 40)
    }

}
extension PSPReceiveConfirmViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }


}



extension PSPReceiveConfirmViewController{
    
    func increaseTime(){
        let increaseTime = 30
        let increasehour = 60
        let timeExpiry = expiryField.text
        var textCursorPosition : Int!
        if let selectedRange = expiryField.selectedTextRange {
            let cursorPosition = expiryField.offset(from: expiryField.beginningOfDocument, to: selectedRange.start)
            textCursorPosition = cursorPosition
        }
        let component = timeExpiry?.components(separatedBy: ":")
        if component?.count == 2 {
            var hour = Int(component?[0] ?? "0") ?? 0
            var minute = Int(component?[1] ?? "0") ?? 0
            var total = minute + (hour * 60 )
            if total < 1410{
                
                if expiryType == "Hours"{
//                    if textCursorPosition == 1 || textCursorPosition == 2 || textCursorPosition == 0{
                        total = total + increasehour
//                        expiryField.offset(from: expiryField.beginningOfDocument, to: expiryField.selectedTextRange!.start)
//                        textCursorPosition = 2
//                    }
                }else{
                    total = total + increaseTime
//                    expiryField.offset(from: expiryField.selectedTextRange!.start, to: expiryField.selectedTextRange!.end)
//                    textCursorPosition = 3
                }
                
//                if textCursorPosition == 1 || textCursorPosition == 2 || textCursorPosition == 0{
//                    total = total + increasehour
//                    expiryField.offset(from: expiryField.beginningOfDocument, to: expiryField.selectedTextRange!.start)
//                    textCursorPosition = 2
//                }else{
//                    total = total + increaseTime
//                    expiryField.offset(from: expiryField.selectedTextRange!.start, to: expiryField.selectedTextRange!.end)
//                    textCursorPosition = 3
//                }
                let newHour = total / 60
                let newMinute = total % 60
                
                hour = newHour
                minute = newMinute
                
                
                var string = ""
                string =  "\(newHour):\(newMinute)"
                self.expiryField.text = String(format: "%02d:%02d", newHour,newMinute)
            }
        }else{
            var hour = 0
            var minute = 0
            var total = minute + (hour * 60)
            if total < 1440{
                total = total + increaseTime
                let newHour = total / 60
                let newMinute = total % 60
                hour = newHour
                minute = newMinute
                var string = ""
                string =  "\(newHour):\(newMinute)"
                self.expiryField.text = String(format: "%02d:%02d", newHour,newMinute)
            }
        }
    }
    
 
    
    func decreaseTime(){
        let decreaseMinute = 30
        let decreasehour = 60
        let timeExpiry = expiryField.text
        var textCursorPosition : Int!
        if let selectedRange = expiryField.selectedTextRange {
            let cursorPosition = expiryField.offset(from: expiryField.beginningOfDocument, to: selectedRange.start)
            textCursorPosition = cursorPosition
        }
        let components = timeExpiry?.components(separatedBy: ":")
        if components?.count == 2{
            let hour = Int(components?[0] ?? "0") ?? 0
            let minute = Int(components?[1] ?? "0") ?? 0
            var total = minute + (hour * 60)
            
//            total = total - decreaseMinute
            if total > 0{
                if expiryType == "Hours"{
                    total = total - decreasehour
                }else{
                    total = total - decreaseMinute
                }
                
                
//                if textCursorPosition == 1 || textCursorPosition == 2 || textCursorPosition == 0{
//                    total = total - 60
//                    expiryField.offset(from: expiryField.beginningOfDocument, to: expiryField.selectedTextRange!.start)
//                    textCursorPosition = 2
//                }else{
//                    total = total - 30
//                    expiryField.offset(from: expiryField.selectedTextRange!.start, to: expiryField.selectedTextRange!.end)
//                    textCursorPosition = 3
//                }
                
                let newHour = total / 60
                let newMinute = total  % 60
                let string =  "\(newHour):\(newMinute)"
                self.expiryField.text = String(format: "%02d:%02d", newHour,newMinute)
            }
        }
    }
    
    
//    func utcToLocal(dateStr: String) -> String? {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//
//        if let date = dateFormatter.string(from: dateStr){
//            dateFormatter.timeZone = TimeZone.current
//            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
//            return dateFormatter.string(from: date)
//        }
//        return nil
//    }
    
}





