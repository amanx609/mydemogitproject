//
//  SuccessViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 19/03/21.
//

import UIKit

class SuccessViewController: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var lblGoTo: UILabel!
    
    var counter = 3
    var isFromLink: Bool = false
    var transaction: Transactions?
    var newTransaction : TransactionHistoryDetails?
    var payments : PaymentResponse?
    var msg: String?
    var isFailed = false
    var isFromReceive = false
    var goMerchant = false
    var isPay = false
    var isFrom : fromScreen = .isPaid
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFromLink {
            self.msgLabel.text = "Bank Account Linked Successfully"
            self.lblGoTo.text = "go to home in"
        }
        if let message = msg {
            self.msgLabel.text = message
        }
        let success = UIImage.gifImageWithName("yellow")
        imgView.image = success
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.2) {
            let success = UIImage.gifImageWithName("success")
            self.imgView.image = success
        }

        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    @objc func updateCounter() {
        //example functionality
        if counter > 0 {
            DispatchQueue.main.async {
                self.countLabel.text = "\(self.counter)"
            }
            counter -= 1
        } else {
            if goMerchant {
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.present(getMerchantHomeViewController(), animated: true, completion: nil)
            }
            } else if isFromLink {
                showHomeVC()
            } else {
                showTransactionVC() }
//            if isFromPay {
//                self.showTransactionVC()
//            } else {
//                self.showVC() }
        }
    }
    func showHomeVC() {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.present(getHomeViewController(), animated: true ,completion: nil)
            
            //notImplementedYetAlert(base: self)
        }
        
    }
    func showTransactionVC() {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            let vc = getTransactionDetialsViewController() as! TransactionDetialsViewController
            
//            vc.transactionDetails = self.newTransaction
         //   vc.transaction = self.transaction
            
//            vc.paymentDetails = self.newTransaction
//
//            vc.paymentDetails = self.payments
//            vc.isPay = self.isPay
//
//            vc.isFailed = self.isFailed
//            vc.isFromReceive = self.isFromReceive
            vc.isFrom = self.isFrom
            self.present(vc, animated: true, completion: nil)
        }
        
    }
}

