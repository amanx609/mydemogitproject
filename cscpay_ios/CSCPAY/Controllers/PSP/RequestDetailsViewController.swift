//
//  RequestDetailsViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 25/06/21.
//

import UIKit

class RequestDetailsViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var viewDetail: UITextView!
    @IBOutlet weak var lblAmount: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var viewDetails: UIView!
    @IBOutlet weak var imgEmoji: UIImageView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var viewAmount: UIView!
    @IBOutlet weak var noteViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblRequestTitle: UILabel!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var lblExpiry: UILabel!
    @IBOutlet weak var actionView: UIView!
    
    var transaction : Transactions?
    
    var isFailed = false
    var isFromReceive = false
    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular,text: Language.getText(with: LanguageConstants.lbl_transaction_detail_title))
        lblName.setup(fontSize: 22, fontType: .semiBold, textColor: .titleColor, bgColor: .clear)
        lblMobile.setup(fontType: .semiBold)
        lblAmount.setup(fontSize: 46, fontType: .bold, textColor: .customGreen)
        lblTime.setup(fontSize: 12, textColor: .darkGray)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        if let trans = self.transaction {
            self.lblName.text = trans.toName
            self.lblMobile.text = trans.to
            self.lblAmount.text = "\(trans.amount)"
            let date = trans.addedAt?.getCurrentDate(format: "dd MMM yyyy, hh:mma")
            self.lblTime.text = date?.0 ?? ""
            if trans.remark?.count ?? 0 > 0 {
                self.viewDetail.text = trans.remark
                self.borderView.isHidden = false
                self.noteViewHeightConstraint.isActive = false
            } else {
                self.viewDetail.frame = CGRect.init(x: 0, y: 0, width: 0, height: 0)
            }
            
            if let img = UIImage.init(named: "\(trans.emoji ?? "")") {
                self.imgEmoji.image = img
                self.imgEmoji.isHidden = false
            }
            if let time = getMinutesDifferenceFromTwoDates(start: Date(), end: trans.expiry ?? Date()) {
                self.lblExpiry.text = "Expires in \(time)"
            } else {
                self.lblExpiry.text = "Expired"
                self.actionView.isHidden = true
            }
        }
        if isFailed {
            //self.statusView.backgroundColor = hexStringToUIColor(hex: "FFF1F0")
            //statusLabel.text = "Failed"
            //statusLabel.textColor = hexStringToUIColor(hex: "EA1601")
            //let fail = UIImage.gifImageWithName("failiure")
            statusImageView.image = UIImage.init(named: "failiure")
            self.viewAmount.backgroundColor = hexStringToUIColor(hex: "FFFAFA")
            self.lblAmount.textColor = .customRed
            //statusImageView.tintColor = hexStringToUIColor(hex: "EA1601")
            //amountLabel.textColor = hexStringToUIColor(hex: "EA1601")
        } else {
            //let success = UIImage.gifImageWithName("success1")
            statusImageView.image = UIImage.init(named: "success1")
        }
    }
//    @IBAction func homeButtonTapped(_ sender: Any) {
//        self.present(getHomeViewController(), animated: true, completion: nil)
//    }
    @IBAction func shareButtonTapped(_ sender: Any) {
        let img = self.viewDetails.takeScreenshot()
        shareQRCode(img: img)
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func declineButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.alertView.isHidden = false
        }
    }
    @IBAction func closeButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.alertView.isHidden = true
        }
    }
    @IBAction func okButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.alertView.isHidden = true
        }
    }
    @IBAction func acceptButtonTapped(_ sender: Any) {
        let vc = getPayConfirmViewController() as! PayConfirmViewController
        vc.item = self.transaction
        vc.updateMoney = true
        self.present(vc, animated: true, completion: nil)
    }
    
    private func shareQRCode(img :UIImage?) {
        if let image = img {
            let vc = UIActivityViewController(activityItems: [image], applicationActivities: [])
            present(vc, animated: true)
        }
    }
}
