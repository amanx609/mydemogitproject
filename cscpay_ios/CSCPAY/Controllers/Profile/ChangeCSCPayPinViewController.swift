//
//  ChangeCSCPayPinViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 23/06/21.
//

import UIKit

class ChangeCSCPayPinViewController: UIViewController {

    @IBOutlet weak var pFirst: UITextField!
    @IBOutlet weak var pSecond: UITextField!
    @IBOutlet weak var pThird: UITextField!
    @IBOutlet weak var pFourth: UITextField!
    
    @IBOutlet weak var cpFirst: UITextField!
    @IBOutlet weak var cpSecond: UITextField!
    @IBOutlet weak var cpThird: UITextField!
    @IBOutlet weak var cpFourth: UITextField!
    @IBOutlet weak var lblResponse: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func createMPinBtn(_ sender: Any) {
        DispatchQueue.main.async {
            let vc = getSuccessViewController() as! SuccessViewController
            vc.isFromLink = true
            vc.msg = "CSC PAY Pin Updated Successfully."
            self.present(vc, animated: true, completion: nil)
        }
    }
}
