//
//  ChangeUPIThroughCardViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 23/06/21.
//

import UIKit

class ChangeUPIThroughCardViewController: UIViewController {
    
    
    @IBOutlet weak var first: UITextField!
    @IBOutlet weak var second: UITextField!
    @IBOutlet weak var third: UITextField!
    @IBOutlet weak var fourth: UITextField!
    @IBOutlet weak var fifth: UITextField!
    @IBOutlet weak var sixth: UITextField!
    @IBOutlet weak var expiryMonth: UITextField!
    @IBOutlet weak var expiryYear: UITextField!
    @IBOutlet weak var responseLabel: UILabel!
    
    var bankDetails: BankAccountsList?
    var bankId : Int?
    var otpDict : [String:Any]?
    var isValidMonth = false
    var isValidYear = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
//        initiateMpinSetup()
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func proceedBtnTapped(_ sender: Any) {
        validateCard()
//        self.present(getEnterOTPViewController(), animated: true, completion: nil)
    }
    @IBAction func upiIDBtnTapped(_ sender: Any) {
        self.present(getChangePinViewController(), animated: true, completion: nil)
    }
}

extension ChangeUPIThroughCardViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if (range.length == 1) {
            if textField == sixth {
                fifth?.becomeFirstResponder()
            }
            if textField == fifth {
                fourth?.becomeFirstResponder()
            }
            if textField == fourth {
                third?.becomeFirstResponder()
            }
            if textField == third {
                second?.becomeFirstResponder()
            }
            if textField == second {
                first?.becomeFirstResponder()
            }
            if textField == first {
                first?.resignFirstResponder()
            }
            textField.text? = ""
            return false
        } else if (range.length == 2){
            if textField == expiryYear {
                expiryMonth?.becomeFirstResponder()
            }
            if textField == expiryYear {
                first?.resignFirstResponder()
            }
            textField.text? = ""
            return false
        }else{
            
            //This lines allows the user to delete the number in the textfield.
            if string.isEmpty{
                return true
            }
            //----------------------------------------------------------------
            
            //This lines prevents the users from entering any type of text.
            if Int(string) == nil {
                return false
            }
            //----------------------------------------------------------------
            
            //This lines lets the user copy and paste the One Time Code.
            //For this code to work you need to enable subscript in Strings https://gist.github.com/JCTec/6f6bafba57373f7385619380046822a0
            if string.count == 10 {
                first.text = "\(string[0])"
                second.text = "\(string[1])"
                third.text = "\(string[2])"
                fourth.text = "\(string[3])"
                fifth.text = "\(string[4])"
                sixth.text = "\(string[5])"
                expiryMonth.text = "\(string[6])\(string[7])"
                expiryYear.text = "\(string[8])\(string[9])"
                
                DispatchQueue.main.async {
                    self.dismissKeyboard()
                }
            }
            //----------------------------------------------------------------
            
            //This is where the magic happens. The OS will try to insert manually the code number by number, this lines will insert all the numbers one by one in each TextField as it goes In. (The first one will go in normally and the next to follow will be inserted manually)
            if string.count == 1 {
                if (textField.text?.count ?? 0) == 1 && textField.tag == 0{
                    if (second.text?.count ?? 0) == 1{
                        if (third.text?.count ?? 0) == 1{
                            if (fourth.text?.count ?? 0) == 1{
                                if (fifth.text?.count ?? 0) == 1{
                                    sixth.text = string
                                    DispatchQueue.main.async {
                                        self.dismissKeyboard()
                                        //self.validCode()
                                    }
                                    return false
                                }else{
                                    fifth.text = string
                                    return false
                                }
                            }else{
                                fourth.text = string
                                return false
                            }
                        }else{
                            third.text = string
                            return false
                        }
                    }else{
                        second.text = string
                        return false
                    }
                }
            }
            //----------------------------------------------------------------
            
            
            //This lines of code will ensure you can only insert one number in each UITextField and change the user to next UITextField when function ends.
            guard let textFieldText = textField.text,
                  let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            
            
            if count == 1{
                if textField.tag == 0{
                    DispatchQueue.main.async {
                        self.second.becomeFirstResponder()
                    }
                    
                }else if textField.tag == 1{
                    DispatchQueue.main.async {
                        self.third.becomeFirstResponder()
                    }
                    
                }else if textField.tag == 2{
                    DispatchQueue.main.async {
                        self.fourth.becomeFirstResponder()
                    }
                    
                }else if textField.tag == 3{
                    DispatchQueue.main.async {
                        self.fifth.becomeFirstResponder()
                    }
                    
                }else if textField.tag == 4{
                    DispatchQueue.main.async {
                        self.sixth.becomeFirstResponder()
                    }
                    
                }else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                        self.dismissKeyboard()
                    })
                }
            }else if count == 2{
                guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                    return true
                }
                let updatedText = oldText.replacingCharacters(in: r, with: string)
                
                
                if textField.tag == 10 {
                    if updatedText.count == 2 {
                        let month = Int(updatedText.suffix(2)) ?? 13
                        if month <= 12 { //Prevent user to not enter month more than 12
                            textField.text = "\(updatedText)"
                            DispatchQueue.main.async {
                                self.expiryYear.becomeFirstResponder()
                            }
                        }
                        //self.lblHelper.text = "Expiry date format is MM/YYYY"
                        return false
                    }
                    //self.lblHelper.text = "Expiry date format is MM/YYYY"
                    return true
                }else if textField.tag == 11{
                    if updatedText.count == 2 {
                        textField.text = "\(updatedText)"
                        expDateValidation(dateStr: updatedText)
//                        return true
                    }
                    return false
                }
            }
            return count <= 2
            
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.animateTextField(textField: textField, up: true)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.animateTextField(textField: textField, up: false)
    }
    
    func showAlert(msg: String) {
        DispatchQueue.main.async {
            self.responseLabel.text = msg
        }
    }
    
    func validateCard() {
            self.showAlert(msg: "")
            if self.first.text?.count == 0 && self.second.text?.count == 0 && self.third.text?.count == 0 && self.fourth.text?.count == 0 && self.fifth.text?.count == 0 && self.sixth.text?.count == 0  && self.expiryMonth.text?.count == 0 && self.expiryYear.text?.count == 0{
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_card_number) ?? Constants.alert_enter_valid_card_number)
            }

        guard let one = self.first.text, one.count > 0 else {
            self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_card_number) ?? Constants.alert_enter_valid_card_number)
        return
        }
        guard let two = self.second.text, two.count > 0 else {
            self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_card_number) ?? Constants.alert_enter_valid_card_number)
        return
        }
        guard let three = self.third.text, three.count > 0 else {
            self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_card_number) ?? Constants.alert_enter_valid_card_number)
        return
        }
        guard let four = self.fourth.text, four.count > 0 else {
            self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_card_number) ?? Constants.alert_enter_valid_card_number)
        return
        }
        guard let five = self.fifth.text, five.count > 0 else {
            self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_card_number) ?? Constants.alert_enter_valid_card_number)
        return
        }
        guard let six = self.sixth.text, six.count > 0 else {
            self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_card_number) ?? Constants.alert_enter_valid_card_number)
        return
        }
        
        
        
            guard let expiryMonth = self.expiryMonth.text, expiryMonth.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_expiry_month) ?? Constants.alert_enter_valid_expiry_month)
                return
            }
            guard let expiryYear = self.expiryYear.text, expiryYear.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_expiry_year) ?? Constants.alert_enter_valid_expiry_year)
                return
            }
            if !isValidYear && !isValidMonth{
                showAlert(msg: Constants.alert_enter_valid_expiry_year)
                return
            }
            let lastSixDigit = "\(one)\(two)\(three)\(four)\(five)\(six)"
            let expiryDate = expiryMonth + expiryYear
        initiateMpinSetup(expiryDate: expiryDate, lastSixDebCard: lastSixDigit)
            //self.present(getPageViewController(), animated: true, completion: nil)
            //self.present(getAppPermissionViewController(), animated: true, completion: nil)
            //self.otpValidate(otpHash: self.otpResponse?.body?.otpHash ?? "NA")
            
            
        
    }
    
}


extension ChangeUPIThroughCardViewController{
    
    func initiateMpinSetup(expiryDate: String , lastSixDebCard : String ){
        self.showActivityIndicator()
        if let userAccount = CoredataManager.getDetail(App_User.self) , let userAccountId = userAccount.accountId{
            APIManager.setMpinSetup(MpinResponse.self, category: userAccount.mobile ?? "7007587639", pspId: "IBL", extra: "IndusInd", accountId: Int(userAccountId) ?? 00000, cardType: "O", expiryDate: expiryDate, lastSixDebCard: lastSixDebCard) { (res, err) in
                self.hideActivityIndicator()
                print(res)
                print(err)
                if err == nil {
                    var challangeString : String = "NA"
//                    if let challangeStr =  CoredataManager.getDetail(App_NPCI_Token.self){
//                        challangeString = challangeStr.keyValue!
//                    }else{
//                        challangeString =  UPI.getChallenge()! as String
//                        print(challangeString)
////                        return
//                    }
                    
//                    DispatchQueue.main.async {
//                        let vc = getSuccessViewController() as! SuccessViewController
//                        vc.isFromLink = true
//                        vc.msg        = "UPI PIN Changed Successfully."
//                        self.present(vc, animated: true, completion: nil)
//                    }
                 
                    
                    
                   //TODO: Uncomment mpin conformation
                    
                    self.mpinConformation(challangeString: challangeString)
                }
            }
        }
        
        
        
//        if let account = GlobalData.sharedInstance.addAccountToVPA , let accountId = account.accountId{
//            APIManager.setMpinSetup(MpinResponse.self, category: "7007587639", pspId: "IBL", extra: "IndusInd", accountId: Int(accountId) ?? 00000, cardType: "O", expiryDate: expiryDate, lastSixDebCard: lastSixDebCard) { (res, err) in
//
//                print(res)
//                print(err)
//                if err == nil {
//                    let challangeString =  UPI.getChallenge() as! String
//                    print(challangeString)
//                    self.mpinConformation(challangeString: challangeString)
//                }
//            }
//        }

    }
    
    func mpinConformation(challangeString: String){
        APIManager.call(MpinResponse.self, urlString: "indus/accountmpin", reqAction: "setMpinConf", category: "7007587639", pspId: "IBL", extra: "IndusInd",npciTransId: "INDB9F765CAC738328FFE0539F42180ABCB", otpNpciTranId: "INDB9F765CAC738128FFE0539F42180ABCB", credentialDataValue: challangeString) { (res, err) in
            print(res)
            print(err)
//            if self.registerApp() ?? false{
//                self.present(getEnterOTPViewController(), animated: true, completion: nil)
//            }else{
//                self.showAlert(msg: "false")
//            }
            
            DispatchQueue.main.async {
                
                let vc = getSuccessViewController() as! SuccessViewController
                vc.isFromLink = true
                vc.msg        = "UPI PIN Changed Successfully."
                self.present(vc, animated: true, completion: nil)
                
                
                
//                self.present(getEnterOTPViewController(), animated: true, completion: nil)
            }
            
            
            
        }
    }
    
    
    
}


extension ChangeUPIThroughCardViewController{
    
    func expDateValidation(dateStr:String) {
        let currentYear = Calendar.current.component(.year, from: Date()) % 100   // This will give you current year (i.e. if 2019 then it will be 19)
        let currentMonth = Calendar.current.component(.month, from: Date()) // This will give you current month (i.e if June then it will be 6)
        let enteredYear = Int(dateStr.suffix(2)) ?? 0 // get last two digit from entered string as year
        let enteredMonth = Int(dateStr.prefix(2)) ?? 0 // get first two digit from entered string as month
        print(dateStr) // This is MM/YY Entered by user

        if enteredYear > currentYear {
//            if (1 ... 12).contains(enteredMonth) {
//                print("Entered Date Is Right")
//                self.isValidYear = true
//            } else {
//                print("Entered Date Is Wrong")
//                self.isValidYear = false
//            }
            self.isValidYear = true

        }else if currentYear == enteredYear{
            self.isValidYear = true

        }
//        else if currentYear == enteredYear {
//            if enteredMonth >= currentMonth {
//                if (1 ... 12).contains(enteredMonth) {
//                   print("Entered Date Is Right")
//                } else {
//                   print("Entered Date Is Wrong")
//                }
//            } else {
//                print("Entered Date Is Wrong")
//            }
//        }
        else {
            self.isValidYear = false
            self.showAlert(msg: "Entered Date Is Wrong")
        }

    }
    
    
    func animateTextField(textField: UITextField, up: Bool)
        {
            let movementDistance:CGFloat = -160
            let movementDuration: Double = 0.3

            var movement:CGFloat = 0
            if up
            {
                movement = movementDistance
            }
            else
            {
                movement = -movementDistance
            }
            UIView.beginAnimations("animateTextField", context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            UIView.setAnimationDuration(movementDuration)
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
            UIView.commitAnimations()
        }
}


