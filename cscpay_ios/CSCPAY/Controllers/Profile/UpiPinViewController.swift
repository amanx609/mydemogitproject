//
//  UpiPinViewController.swift
//  CSCPAY
//
//  Created by Aman Pandey on 03/08/21.
//

import UIKit

class UpiPinViewController: UIViewController {

    @IBOutlet weak var reqlbl: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var eyeView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var pFirst : UITextField!
    @IBOutlet weak var pSecond : UITextField!
    @IBOutlet weak var pThird : UITextField!
    @IBOutlet weak var pFourth : UITextField!
    
    var paymentInformation : InitPaymentResponse?
    var pay : Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.addTarget(self, action: #selector(backButtonClicked(_:)), for: .touchUpInside)
    }
    
    
    @objc func backButtonClicked(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func doneButtonTap(_ sender: UIButton) {
        validateUpiPin()
        
    }
    
    
    
    
    
}



extension UpiPinViewController{
    
    func showAlert(msg: String) {
        DispatchQueue.main.async {
            self.reqlbl.text = msg
        }
    }
    
    func validateUpiPin() {
        DispatchQueue.main.async {
            self.showAlert(msg: "")
            if self.pFirst.text?.count == 0 && self.pSecond.text?.count == 0 && self.pThird.text?.count == 0 && self.pFirst.text?.count == 0 {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_otp) ?? Constants.alert_enter_otp)
            }
            guard let one = self.pFirst.text, one.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
            return
            }
            guard let two = self.pSecond.text, two.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
            return
            }
            guard let three = self.pThird.text, three.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
            return
            }
            guard let four = self.pFirst.text, four.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
            return
            }
            let upiPin = "\(one)\(two)\(three)\(four)"
            self.confirmPayment()
            
            
            
        }
    }
}

extension UpiPinViewController : UITextFieldDelegate{
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if (range.length == 1) {
                if textField == pFourth {
                    pThird?.becomeFirstResponder()
                }
                if textField == pThird {
                    pSecond?.becomeFirstResponder()
                }
                if textField == pSecond {
                    pFirst?.becomeFirstResponder()
                }
                if textField == pFirst {
                    pFirst?.resignFirstResponder()
                }
                textField.text? = ""
                return false
            } else {
                
                //This lines allows the user to delete the number in the textfield.
                if string.isEmpty{
                    return true
                }
                //----------------------------------------------------------------
                
                //This lines prevents the users from entering any type of text.
                if Int(string) == nil {
                    return false
                }
                //----------------------------------------------------------------
                
                //This lines lets the user copy and paste the One Time Code.
                //For this code to work you need to enable subscript in Strings https://gist.github.com/JCTec/6f6bafba57373f7385619380046822a0
                if string.count == 4 {
                    pFirst.text = "\(string[0])"
                    pSecond.text = "\(string[1])"
                    pThird.text = "\(string[2])"
                    pFirst.text = "\(string[3])"
                    
                    DispatchQueue.main.async {
                        self.dismissKeyboard()
//                        self.validateOTP()
                    }
                }
                //----------------------------------------------------------------
                
                //This is where the magic happens. The OS will try to insert manually the code number by number, this lines will insert all the numbers one by one in each TextField as it goes In. (The first one will go in normally and the next to follow will be inserted manually)
                if string.count == 1 {
                    if (textField.text?.count ?? 0) == 1 && textField.tag == 0{
                        if (pSecond.text?.count ?? 0) == 1{
                            if (pThird.text?.count ?? 0) == 1{
                                        pFourth.text = string
                                        DispatchQueue.main.async {
                                            self.dismissKeyboard()
                                            //self.validCode()
                                        }
                                        return false
                                    }else{
                                        pThird.text = string
                                        return false
                                    }
                                }else{
                                    pSecond.text = string
                                    return false
                                }
                            }else{
                                pFirst.text = string
                                return false
                            }
                    }
                }
                //----------------------------------------------------------------
                
                
                //This lines of code will ensure you can only insert one number in each UITextField and change the user to next UITextField when function ends.
                guard let textFieldText = textField.text,
                      let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
                }
                let substringToReplace = textFieldText[rangeOfTextToReplace]
                let count = textFieldText.count - substringToReplace.count + string.count
                
                
                if count == 1{
                    if textField.tag == 0{
                        DispatchQueue.main.async {
                            self.pSecond.becomeFirstResponder()
                        }
                        
                    }else if textField.tag == 1{
                        DispatchQueue.main.async {
                            self.pThird.becomeFirstResponder()
                        }
                        
                    }else if textField.tag == 2{
                        DispatchQueue.main.async {
                            self.pFourth.becomeFirstResponder()
                        }
                        
                    }else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                            self.dismissKeyboard()
                        })
                    }
                }
                
                return count <= 1
                //----------------------------------------------------------------
            }
        
        
            public func textFieldDidBeginEditing(_ textField: UITextField) {
                    //moveTextField(textField, moveDistance: -250, up: true)
                
                }

                // Finish Editing The Text Field
            public func textFieldDidEndEditing(_ textField: UITextField) {
              
                   // moveTextField(textField, moveDistance: -250, up: false)
                }

                // Hide the keyboard when the return key pressed
            public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
                    textField.resignFirstResponder()
                    return true
                }
}


extension UpiPinViewController{
    func confirmPayment(){
        APIManager.payment(PaymentResponse.self, reqAction: "confPayment", upiTransRefNo: 91684,  amount: "1.00", expiryTime: 0) { res, err in
            print(res)
            print(err)
            
            if err == nil{
                DispatchQueue.main.async {
                    if let result = res{
                        CoredataManager.saveTransactions(payments: result)
                        
                        let vc = getSuccessViewController() as! SuccessViewController
                        vc.payments = result
                        vc.isPay = true
                        vc.isFrom = .isPaid
                        self.present(vc, animated: true, completion: nil)
                    }
                }
                
            }
            
        }
    }
}

