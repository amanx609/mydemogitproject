//
//  ChatViewController.swift
//  CSCPAY
//
//  Created by Aman Pandey on 25/08/21.
//

import UIKit

class ChatViewController: UIViewController {
    
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var chatTableView: UITableView!
    
    var objChat = Chat()
    var raiseDispute: DisputeList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func backButtonTapped(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

}


extension ChatViewController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiveChatCell", for: indexPath) as! ReceiveChatCell
            if let disputeData = raiseDispute{
                cell.showMessage(text: "Hello  \(disputeData.disputeStatus)")
            }
            
//            cell.showOutgoingMessage(text: "An arbitrary text which we use to demonstrate how our label sizes' calculation works.")
            cell.subviews[0].backgroundColor = .blue
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SendChatCell", for: indexPath) as! SendChatCell
            cell.subviews[0].backgroundColor = .red
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


