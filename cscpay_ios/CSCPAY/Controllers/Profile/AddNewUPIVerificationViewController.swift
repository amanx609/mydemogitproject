//
//  AddNewUPIVerificationViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 19/02/21.
//

import UIKit

class AddNewUPIVerificationViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblOTPSent: UILabel!
    @IBOutlet weak var first: UITextField!
    @IBOutlet weak var second: UITextField!
    @IBOutlet weak var third: UITextField!
    @IBOutlet weak var fourth: UITextField!
    @IBOutlet weak var fifth: UITextField!
    @IBOutlet weak var sixth: UITextField!
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var resendOTPLabel: UILabel!
    @IBOutlet weak var btnVerify: UIButton!
    
    var mobile      : String?
    var otpResponse : OTPResponse?
    var resendTimer : Timer?
    var count = 60  // 60sec

    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular,text: Language.getText(with: LanguageConstants.lbl_add_new_verification_title))
        lblOTPSent.setup(textColor: .subtitleColor,text: (Language.getText(with: LanguageConstants.lbl_otp_sent) ?? Constants.otpSentHint) + (mobile?.suffix(2) ?? "##"))
        btnVerify.setup(fontType: .semiBold, textColor: .customWhite, bgColor: .taleColor, text: Language.getText(with: LanguageConstants.btn_verify))
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        setupUI()
        setupResend()
    }
    func setupResend() {
        self.resendOTPLabel.halfTextColorChange(fullText: Language.getText(with: LanguageConstants.lbl_resend_otp) ?? Constants.otpResend, changeText: Language.getText(with: LanguageConstants.btn_resend_otp) ?? Constants.otpResendBtn, color: .lightBlue, textSize: 11)
        self.resendOTPLabel.isUserInteractionEnabled = true
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnLabel(_ :)))
        tapgesture.numberOfTapsRequired = 1
        self.resendOTPLabel.addGestureRecognizer(tapgesture)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.first.becomeFirstResponder()
        }
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    //MARK:- tappedOnLabel
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = self.resendOTPLabel.text else { return }
        let termsRange = (text as NSString).range(of: Language.getText(with: LanguageConstants.btn_resend_otp) ?? Constants.otpResendBtn)
        if gesture.didTapAttributedTextInLabel(label: resendOTPLabel, inRange: termsRange) {
            //print("Tapped Resend Button")
            self.otpResend()
        }
    }
    func showAlert(msg: String) {
        DispatchQueue.main.async {
            self.responseLabel.text = msg
        }
    }
    func validateOTP() {
        DispatchQueue.main.async {
            self.showAlert(msg: "")
            if self.first.text?.count == 0 && self.second.text?.count == 0 && self.third.text?.count == 0 && self.fourth.text?.count == 0 && self.fifth.text?.count == 0 && self.sixth.text?.count == 0 {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_otp) ?? Constants.alert_enter_otp)
            }
            guard let one = self.first.text, one.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
            return
            }
            guard let two = self.second.text, two.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
            return
            }
            guard let three = self.third.text, three.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
            return
            }
            guard let four = self.fourth.text, four.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
            return
            }
            guard let five = self.fifth.text, five.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
            return
            }
            guard let six = self.sixth.text, six.count > 0 else {
                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
            return
            }
            self.present(getHomeViewController(), animated: true, completion: nil)
            //self.otpValidate(otpHash: self.otpResponse?.body?.otpHash ?? "NA")
            /*
            let otpHash = "\(one)\(two)\(three)\(four)\(five)\(six)\(self.mobile ?? "")\(self.otpResponse?.head?.refId ?? "")".sha256() as? String
            if otpHash != (self.otpResponse?.body?.otpHash ?? "NA") {                self.showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_otp) ?? Constants.alert_enter_valid_otp)
            } else {
                self.otpValidate(otpHash: otpHash ?? "NA") }  */
        }
    }
    
    @IBAction func verifyBtnTapped(_ sender: UIButton) {
        validateOTP()
    }
    func otpResend() {
        self.showActivityIndicator()
        APIManager.otpRequest(OTPResponse.self, mobile: mobile ?? "NA", reqAction: "resend", refId:otpResponse?.head?.refId ?? "NA", resHash: otpResponse?.body?.resHash ?? "NA") { (result, err) in
            self.hideActivityIndicator()
            DispatchQueue.main.async {
                if err != nil {
                    if let err = err as? RuntimeError {
                        self.responseLabel.text = err.message
                    } else {
                        self.responseLabel.text = err?.localizedDescription ?? Language.getText(with: LanguageConstants.general_error_generic) ?? Constants.SOMETHING_WRONG
                    }
                } else {
                    //Success goes here
                    self.resendTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
                }
            }
        }
    }
    @objc func update() {
        if(count > 0) {
            count = count - 1
            self.resendOTPLabel.isUserInteractionEnabled = false
            self.resendOTPLabel.text    = "\(Language.getText(with: LanguageConstants.btn_resent_count) ?? Constants.otpResendCount) \(self.count)s"
        }
        else {
            resendTimer?.invalidate()
            count = 60
            DispatchQueue.main.async {
                self.setupResend()
            }
        }
    }
    func otpValidate(otpHash: String) {
        self.showActivityIndicator()
        APIManager.otpRequest(OTPResponse.self, mobile: mobile ?? "NA", reqAction: "validate", refId:otpResponse?.head?.refId ?? "NA", otpHash: otpHash) { (res1, err) in
            self.hideActivityIndicator()
            DispatchQueue.main.async {
                if err != nil {
                    if let err = err as? RuntimeError {
                        self.responseLabel.text = err.message
                    } else {
                        self.responseLabel.text = err?.localizedDescription ?? Language.getText(with: LanguageConstants.general_error_generic) ?? Constants.SOMETHING_WRONG
                    }
                } else {
                    //Success goes here
                    //self.present(getPageViewController(), animated: true, completion: nil)
                }
            }
        }
    }
    }
    extension AddNewUPIVerificationViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if (range.length == 1) {
            if textField == sixth {
                fifth?.becomeFirstResponder()
            }
            if textField == fifth {
                fourth?.becomeFirstResponder()
            }
            if textField == fourth {
                third?.becomeFirstResponder()
            }
            if textField == third {
                second?.becomeFirstResponder()
            }
            if textField == second {
                first?.becomeFirstResponder()
            }
            if textField == first {
                first?.resignFirstResponder()
            }
            textField.text? = ""
            return false
    } else {

        //This lines allows the user to delete the number in the textfield.
        if string.isEmpty{
            return true
        }
        //----------------------------------------------------------------

        //This lines prevents the users from entering any type of text.
        if Int(string) == nil {
            return false
        }
        //----------------------------------------------------------------

        //This lines lets the user copy and paste the One Time Code.
        //For this code to work you need to enable subscript in Strings https://gist.github.com/JCTec/6f6bafba57373f7385619380046822a0
        if string.count == 6 {
            first.text = "\(string[0])"
            second.text = "\(string[1])"
            third.text = "\(string[2])"
            fourth.text = "\(string[3])"
            fifth.text = "\(string[4])"
            sixth.text = "\(string[5])"

            DispatchQueue.main.async {
                self.dismissKeyboard()
                //self.validCode()
            }
        }
        //----------------------------------------------------------------

        //This is where the magic happens. The OS will try to insert manually the code number by number, this lines will insert all the numbers one by one in each TextField as it goes In. (The first one will go in normally and the next to follow will be inserted manually)
        if string.count == 1 {
            if (textField.text?.count ?? 0) == 1 && textField.tag == 0{
                if (second.text?.count ?? 0) == 1{
                    if (third.text?.count ?? 0) == 1{
                        if (fourth.text?.count ?? 0) == 1{
                            if (fifth.text?.count ?? 0) == 1{
                                sixth.text = string
                                DispatchQueue.main.async {
                                    self.dismissKeyboard()
                                    self.validateOTP()
                                }
                                return false
                            }else{
                                fifth.text = string
                                return false
                            }
                        }else{
                            fourth.text = string
                            return false
                        }
                    }else{
                        third.text = string
                        return false
                    }
                }else{
                    second.text = string
                    return false
                }
            }
        }
        //----------------------------------------------------------------


        //This lines of code will ensure you can only insert one number in each UITextField and change the user to next UITextField when function ends.
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count


        if count == 1{
            if textField.tag == 0{
                DispatchQueue.main.async {
                    self.second.becomeFirstResponder()
                }

            }else if textField.tag == 1{
                DispatchQueue.main.async {
                    self.third.becomeFirstResponder()
                }

            }else if textField.tag == 2{
                DispatchQueue.main.async {
                    self.fourth.becomeFirstResponder()
                }

            }else if textField.tag == 3{
                DispatchQueue.main.async {
                    self.fifth.becomeFirstResponder()
                }

            }else if textField.tag == 4{
                DispatchQueue.main.async {
                    self.sixth.becomeFirstResponder()
                }

            }else {
                DispatchQueue.main.async {
                    self.dismissKeyboard()
                    self.validateOTP()
                }
            }
        }

        return count <= 1
        //----------------------------------------------------------------
    }
    }

    func animateTextField(textField: UITextField, up: Bool)
        {
            let movementDistance:CGFloat = -160
            let movementDuration: Double = 0.3

            var movement:CGFloat = 0
            if up
            {
                movement = movementDistance
            }
            else
            {
                movement = -movementDistance
            }
            UIView.beginAnimations("animateTextField", context: nil)
            UIView.setAnimationBeginsFromCurrentState(true)
            UIView.setAnimationDuration(movementDuration)
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
            UIView.commitAnimations()
        }
    public func textFieldDidBeginEditing(_ textField: UITextField) {
            //moveTextField(textField, moveDistance: -250, up: true)
        self.animateTextField(textField: textField, up:true)
        }

        // Finish Editing The Text Field
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.animateTextField(textField: textField, up:false)
           // moveTextField(textField, moveDistance: -250, up: false)
        }

        // Hide the keyboard when the return key pressed
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }

        // Move the text field in a pretty animation!
        func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
            let moveDuration = 0.3
            let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
            UIView.animate(withDuration: moveDuration, animations: {
            }) { _ in
                self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
            }
        }
    }
