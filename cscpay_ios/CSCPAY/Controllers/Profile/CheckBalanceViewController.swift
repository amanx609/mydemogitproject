//
//  CheckBalanceViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 08/04/21.
//

import UIKit

class CheckBalanceViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBalanceMessage: UILabel!
    @IBOutlet weak var lblBankMessageBody: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblAccountNumber: UILabel!
    @IBOutlet weak var lblAccountType: UILabel!
    @IBOutlet weak var imgBank: UIImageView!
    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular,text: Language.getText(with: LanguageConstants.lbl_balance_title))
        lblBalanceMessage.setup(fontSize: 18, fontType: .semiBold, textColor: .customBlack)
        lblBankMessageBody.setup(fontSize: 18, fontType: .regular, textColor: .customBlack)
        lblAmount.setup(fontSize: 30, fontType: .bold, textColor: .customBlack)
        lblAccountNumber.setup(fontSize: 15, fontType: .regular)
        lblAccountType.setup(fontSize: 13, textColor: .subtitleColor)
        
        if let balanceModel = GlobalData.sharedInstance.confirmBalance {
            self.lblAmount.text = balanceModel.accountInfo?.accountBalance ?? "0"
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//            self.present(getHomeViewController(), animated: true, completion: nil)
//        }
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func homeBtnTapped(_ sender: Any) {
        self.present(getHomeViewController(), animated: true, completion: nil)
    }
}
