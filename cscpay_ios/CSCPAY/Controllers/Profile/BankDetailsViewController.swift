//
//  BankDetailsViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 22/02/21.
//

import UIKit
import CoreData

class BankDetailsViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBank: UILabel!
    @IBOutlet weak var lblAccountType: UILabel!
    @IBOutlet weak var lblIsPrimary: UILabel!
    @IBOutlet weak var lblAccountInfo: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblUPI: UILabel!
    @IBOutlet weak var lblUpiId: UILabel!
    @IBOutlet weak var lblViewBalance: UILabel!
    @IBOutlet weak var lblForgetUpiPin: UILabel!
    @IBOutlet weak var imgCheckmark: UIImageView!
    @IBOutlet weak var removeAccountView: UIView!
    @IBOutlet weak var changeUPILabel: UILabel!
    @IBOutlet weak var removeAccountLabel: UILabel!
    @IBOutlet weak var delinkAccountView: UIView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var primaryAccountButton: UIButton!
    @IBOutlet weak var createUpiView: UIView!
    @IBOutlet weak var bankIcon: UIImageView!
    @IBOutlet weak var apLbl: UILabel!
    
    var bankDetails : Account_List?
    var userDetails : App_User?
    var appStage = ""
    
    

    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular,text: Language.getText(with: LanguageConstants.lbl_payment_method_subtitle))
        lblBank.setup(fontSize: 15, fontType: .regular, textColor: .titleColor)
        lblAccountType.setup(fontSize: 13, fontType: .regular, textColor: .subtitleColor)
        lblIsPrimary.setup(fontSize: 15, fontType: .regular, textColor: .titleColor)
        lblAccountInfo.setup(fontType: .semiBold, text: Language.getText(with: LanguageConstants.lbl_account_information))
        lblUsername.setup()
        lblMobile.setup()
        lblUPI.setup(fontType: .semiBold, text: Language.getText(with: LanguageConstants.lbl_upi_id))
        lblUpiId.setup()
        lblViewBalance.setup(fontSize: 15, fontType: .semiBold, text: Language.getText(with: LanguageConstants.lbl_view_balance))
        lblForgetUpiPin.setup(fontSize: 15, fontType: .semiBold, text: Language.getText(with: LanguageConstants.lbl_forget_upi_pin))
        let dismissView = UITapGestureRecognizer(target: self, action: #selector(tapViewWhenTappedAround))
        view.addGestureRecognizer(dismissView)
        let createUpiID = UITapGestureRecognizer(target: self, action: #selector(tapOnCreateUpiLabel))
        createUpiView.addGestureRecognizer(createUpiID)
        userDetails = CoredataManager.getDetail(App_User.self)
        appStage = CoredataManager.getDetail(App_Config.self)?.appStage ?? "0"
        
    }
    
    func setValues(){
        if let user = CoredataManager.getDetail(App_User.self) {
            if let mob = user.mobile {
                self.lblMobile.text =  mob
            }
            if let name = user.name {
                self.lblUsername.text = name
            }
            
            if appStage == "4"{
                createUpiView.isHidden = false
                lblUpiId.text = "-"
            }else{
                createUpiView.isHidden = true
                if let upiId = user.virtualAddress{
                    self.lblUpiId.text = upiId
                }
            }

            //TODO: Bank Name
            if CoredataManager.getDetails(Master_BankList.self)!.contains(where: {$0.bankCode == bankDetails?.bankCode}) {
                //BANK NAME
            }
            
            if let bankName = CoredataManager.getDetails(Master_BankList.self)?.filter({$0.bankCode == bankDetails?.bankCode}){
                bankIcon.image = UIImage(named: bankName[0].bankCode ?? "bankPlaceholder")
                if let bankDetails = bankDetails{
                    lblBank.text = "\(bankName[0].bankName ?? "icici")" + " \(bankDetails.maskedAccountNumber ?? "1234")"
                }
            }
            
            
            if (bankDetails?.isPrimary != nil && bankDetails?.isPrimary == "T"){
                    self.imgCheckmark.image = UIImage.init(systemName: "checkmark.square.fill")
                primaryAccountButton.isUserInteractionEnabled = false
                
                } else {
                    self.imgCheckmark.image =  UIImage.init(systemName: "square")
                    primaryAccountButton.isUserInteractionEnabled = true
                }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setupUI()
//        setValues()
//        setUpLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupUI()
        setValues()
        setUpLabel()

    }
    
    @IBAction func optionBtnTapped(_ sender: Any) {
        UIView.animate(withDuration: 1.0, delay: 0.4, options: .curveEaseOut, animations: {
            self.removeAccountView.alpha = 1.0
            self.removeAccountView.isHidden = false
        }, completion: nil)
        
//        self.present(getChangeUPIThroughCardViewController(), animated: true, completion: nil)
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func viewBalanceButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.checkBalanceInitiate()
            //self.present(getCheckBalanceViewController(), animated: true, completion: nil)
        }
        
    }
    @IBAction func primeAccountBtnTapped(_ sender: Any) {
        DispatchQueue.main.async {
            if self.imgCheckmark.image == UIImage.init(systemName: "checkmark.square.fill") {
                self.imgCheckmark.image = UIImage.init(systemName: "square")
            } else {
                self.imgCheckmark.image = UIImage.init(systemName: "checkmark.square.fill")
                self.setDefaultAccount(action: "setDefault")
                
            }
        }
    }
    func setDefaultAccount(action : String) {
        self.showActivityIndicator()
        APIManager.call(SetDefaultAccount.self, urlString: "indus/account", reqAction : action, category: "IndusInd", pspId: "IBL", extra: userDetails?.virtualAddress) { (res, err) in
            
            self.hideActivityIndicator()
            print(res)
            print(err)
            print("Done**************")
            
            if err != nil{
                if let err = err as? RuntimeError {
                    print(err.message)
                } else {
                    print(err?.localizedDescription ?? Language.getText(with: LanguageConstants.general_error_generic) ?? Constants.SOMETHING_WRONG)
                }
            }else{
            
            switch action {
            case "removeAccount":
                print("removeAccount********")
                DispatchQueue.main.async {
                    guard let primaryAccount = CoredataManager.getDetail(Account_List.self) else {return}
                    if self.bankDetails?.accId == primaryAccount.accId{
                        CoredataManager.deleteDetail(myData: self.bankDetails!)
                    }
                    self.dismiss(animated: true, completion: nil)

                }
            case "setDefault":
                GlobalData.sharedInstance.setDefaultAccount = res
                self.bankDetails?.isPrimary = "T"
                DispatchQueue.main.async {
                    if let  primaryAccount =  CoredataManager.getSpecificRecord(Account_List.self, key: "isPrimary", value: "T"){
                        for account in primaryAccount{
                            if self.bankDetails?.bankCode == account.bankCode{
                                CoredataManager.updateDetailsBank(Account_List.self, key: "isPrimary", value: "T")
                            }else{
                                CoredataManager.updateDetailsBank(Account_List.self, key: "isPrimary", value: "F")
                            }
                        }
                    }
                }
                break
            default:
                print("Done********")
            }
         }
        }
    }
    
    func checkBalance() {
        self.showActivityIndicator()
        APIManager.call(ConfirmBalance.self, urlString: "indus/transact/balance", reqAction : "confBalance", category: "IndusInd", pspId: "IBL", extra: userDetails?.virtualAddress) { (res, err) in
            
            self.hideActivityIndicator()
            //self.refId = msg ?? "NA"
            print(res)
            print(err)
            print("Done**************")
            GlobalData.sharedInstance.confirmBalance = res
            DispatchQueue.main.async {
                self.present(getCheckBalanceViewController(), animated: true, completion: nil)
            }
        }
    }
    func checkBalanceInitiate() {
        self.showActivityIndicator()
        APIManager.call(InitiateBalance.self, urlString: "indus/transact/balance", reqAction : "initBalance", category: "IndusInd", pspId: "IBL", extra: userDetails?.virtualAddress) { (res, err) in
            
            self.hideActivityIndicator()
            //self.refId = msg ?? "NA"
            print(res)
            print(err)
            print("Done**************")
            self.checkBalance()
            
        }
    }
    
    
    func setUpLabel() {
        self.changeUPILabel.isUserInteractionEnabled = true
        self.removeAccountLabel.isUserInteractionEnabled = true
        let changeUPILabelGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnLabel(_ :)))
        changeUPILabelGesture.numberOfTapsRequired = 1
        self.changeUPILabel.addGestureRecognizer(changeUPILabelGesture)
        let removeAccountGesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnLabelRemoveAccount(_:)))
        self.removeAccountLabel.addGestureRecognizer(removeAccountGesture)
        print(userDetails)
    }
    
    
    
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        UIView.animate(withDuration: 1.2, delay: 0.4, options: .curveEaseInOut, animations: {
            self.removeAccountView.alpha = 0.0
            self.removeAccountView.isHidden = true
        }, completion: { vc in
        self.present(getChangeUPIThroughCardViewController(), animated: true, completion: nil)
        })
    }
    
    @objc func tapOnCreateUpiLabel(_ gesture : UITapGestureRecognizer){
//        createUpiView.isHidden = false
        self.present(getSetMPinViewController(), animated: true, completion: nil)
    }
    
    
    @objc func tappedOnLabelRemoveAccount(_ gesture : UITapGestureRecognizer){
        UIView.animate(withDuration: 1.0, delay: 0.4, options: .curveEaseInOut, animations: {
            self.removeAccountView.alpha = 0.0
            self.removeAccountView.isHidden = true
        } , completion: { _ in
                self.delinkAccountView.alpha = 1.0
                self.delinkAccountView.isHidden = false
                self.buttonSetUp(cancel: false)
        })
    }
    
    
    @objc func tapViewWhenTappedAround(_ gesture : UITapGestureRecognizer){
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseInOut, animations: {
            self.removeAccountView.alpha = 0.0
            self.removeAccountView.isHidden = true
        }, completion: nil)
    }
    
    
    @IBAction func cancelButtonTap(_ sender : UIButton){
        buttonSetUp(cancel: true)
    }
    
    
    @IBAction func okButtonTap(_ sender : UIButton){
        buttonSetUp(cancel: false)
    }
    
    @IBAction func forgetUpiButtonTapped(_ sender: Any) {
//        self.setDefaultAccount(action: "removeAccount")
        //notImplementedYetAlert(base: self)
    }
    
    func buttonSetUp(cancel : Bool = false){
        cancelButton.layer.cornerRadius = 10
        okButton.layer.cornerRadius = 10
        if cancel{
            cancelButton.setTitleColor(.white, for: .normal)
            cancelButton.backgroundColor = .customBlue
            okButton.setTitleColor(.customBlue, for: .normal)
            okButton.backgroundColor = .white
            okButton.layer.borderWidth = 1
            okButton.layer.borderColor = UIColor.customBlue.cgColor
            delinkAccountView.isHidden = cancel
        }else{
            okButton.setTitleColor(.white, for: .normal)
            okButton.backgroundColor = .customBlue
            okButton.addTarget(self, action: #selector(accountDisabledOkButtonTap(_:)), for: .touchUpInside)
            cancelButton.setTitleColor(.customBlue, for: .normal)
            cancelButton.backgroundColor = .white
            cancelButton.layer.borderWidth = 1
            cancelButton.layer.borderColor = UIColor.customBlue.cgColor
            delinkAccountView.isHidden = cancel
        }
    }
    
    @objc func accountDisabledOkButtonTap(_ sender: UIButton){
        delinkAccountView.isHidden = true
        self.setDefaultAccount(action: "removeAccount")
    }
    
    
}
