//
//  ChangePinViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 23/06/21.
//

import UIKit

class ChangePinViewController: UIViewController {
    
    @IBOutlet weak var pFirst: UITextField!
    @IBOutlet weak var pSecond: UITextField!
    @IBOutlet weak var pThird: UITextField!
    @IBOutlet weak var pFourth: UITextField!
    
    @IBOutlet weak var cpFirst: UITextField!
    @IBOutlet weak var cpSecond: UITextField!
    @IBOutlet weak var cpThird: UITextField!
    @IBOutlet weak var cpFourth: UITextField!
    @IBOutlet weak var lblResponse: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createUPIPinBtn(_ sender: Any) {
        DispatchQueue.main.async {
            let vc = getSuccessViewController() as! SuccessViewController
            vc.isFromLink = true
            vc.msg        = "UPI PIN Changed Successfully."
            self.present(vc, animated: true, completion: nil)
        }
    }
}
