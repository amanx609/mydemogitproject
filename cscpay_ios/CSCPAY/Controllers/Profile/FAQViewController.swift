//
//  FAQViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 08/03/21.
//

import UIKit

class FAQViewController: UIViewController {
    
    let others = ["CSCPay Guide", "Using QR", "Complete Your Profile", "Learn Payments on CSCPay", "Reporting fraudulent activity", "Account setting", "Bank Account and UPI", "Money Transfer", "Contact your Bank"]
    let important = ["My account and KYC", "Bank Account, UPI & Debit/Credit cards", "Money Transfer", "Contact your Bank"]

    @IBOutlet weak var tableview: UITableView!
    //@IBOutlet weak var tableView2: UITableView!
    //@IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var ticketListButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.tableFooterView = UIView()
        //self.tableView2.tableFooterView = UIView()
        getDisputeList()
    }
//    override func viewWillLayoutSubviews() {
//        super.updateViewConstraints()
//        self.tableHeight?.constant = self.tableView2.contentSize.height
//    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func viewAllTransactionButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.present(getTransactionHistoryViewController(), animated: true, completion: nil)
        }
    }
    
    @IBAction func ticketListButtonTapped(_ sender: UIButton) {
        let vc = getTicketsViewController() as! TicketsViewController
        vc.disputeObj = GlobalData.sharedInstance.disputeList
        present(getTicketsViewController(), animated: true, completion: nil)
    }
    
    
    
    func getDisputeList() {
        APIManager.call(DisputeList.self, urlString: "indus/support", reqAction : "getDisputeList",category: "IndusInd", pspId: "IBL", extra: "dispute") { (res, err) in
            self.hideActivityIndicator()
            
            GlobalData.sharedInstance.disputeList = res
            //self.refId = msg ?? "NA"
            print(res)
            print(err)
            print("Done**************")
        }
    }
}
extension FAQViewController : UITableViewDelegate, UITableViewDataSource {
    /*
     func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
         let footerView = tableView.dequeueReusableCell(withIdentifier: "FAQFooterCell") as! FAQFooterCell
         return footerView
     }

     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
         return 50
     }
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2 {
            return 0
        }
        return UITableView.automaticDimension
        }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    } /*
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
        if tableView == tableview {
            if section == 2 {
                return 55 // UITableView.automaticDimension
            } else {
             if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                return 75.0
              } else {
                return 41.0
              }
            }
        } else {
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if tableView == tableview {
            if section == 2 {
                let reuseIdentifier : String!
                reuseIdentifier = String(format: "FAQFooterCell")

                let expandCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath) as? FAQFooterCell
                
                return expandCell
            }
            
            let reuseIdentifier : String!
            reuseIdentifier = String(format: "FAQSectionCell")

            let expandCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath) as? FAQSectionCell
            expandCell?.titleLabel.textColor = .black
        expandCell?.titleLabel.text = section == 0 ? "Other help topics" : "Important"
            expandCell?.contentView.backgroundColor = hexStringToUIColor(hex: "F6F6F6")
            return expandCell
        } else {
            return UITableViewCell()
        }
        
    } */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return others.count
//        if tableView == tableView2 {
//            return 1
//        } else {
//            if section == 2 {
//                return 1
//            } else if section == 0 {
//                return others.count
//            } else  {
//                return important.count
//            }
//        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /* if tableView == tableView2 {
            let reuseIdentifier : String!
            reuseIdentifier = String(format: "historyCell")

            guard let expandCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath) as? historyCell else { return UITableViewCell() }
            return expandCell
            
        } else {
            if indexPath.section == 2 {
    //            let reuseIdentifier : String!
    //            reuseIdentifier = String(format: "FAQFooterCell")
    //
    //            let expandCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath) as? FAQFooterCell
    //            return expandCell ?? UITableViewCell()
                return UITableViewCell()
            } else {
         */
                let reuseIdentifier : String!
                reuseIdentifier = String(format: "FAQCell")

                let expandCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath) as? FAQCell
                var item : String = "Null"
                //if indexPath.section == 0 {
                    item = others[indexPath.row]
//                } else {
//                    item = important[indexPath.row]
//                }
                expandCell?.titleLabel.text = item

                return expandCell ?? UITableViewCell()
         //   }
        //}
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableview {
            let cell = tableview.cellForRow(at: indexPath)! as! FAQCell //.textLabel!.text!
            let vc = getCSCPayGuideController() as! CSCPayGuideController
            vc.titleText = cell.titleLabel.text ?? ""
            self.present(vc, animated: true, completion: nil)
        } else {
            notImplementedYetAlert(base: self) }
    }
    
}

final class ContentSizedTableView: UITableView {
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}
