//
//  EnterOTPViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 23/06/21.
//

import UIKit

class EnterOTPViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func doneButtonTapped(_ sender: Any) {
        self.present(getAtmPinViewController(), animated: true, completion: nil)
    }
    

}
