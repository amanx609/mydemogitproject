//
//  AddNewUPIViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 19/02/21.
//

import UIKit

class AddNewUPIViewController: UIViewController {
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var statusImg: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var upiField: UITextField!
    //@IBOutlet weak var verifierView: UIView!
    //@IBOutlet weak var nameLabel: IuFloatingTextFiledPlaceHolder!
    //@IBOutlet weak var accountHolderNameLabel: UILabel!
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var btnProceed: UIButton!
    
    var isFirst: Bool = false
    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular,text: Language.getText(with: LanguageConstants.lbl_add_new_upi_title))
        upiField.setup(textColor: .customBlack, placeholderText: Language.getText(with: LanguageConstants.txt_upi_placeholder))
        //nameLabel.setup(fontType:.semiBold ,textColor:.customBlack, placeholderText: nil)
        btnVerify.setup(fontSize: 12, fontType: .bold, textColor: .customBlue, text: Language.getText(with: LanguageConstants.btn_upi_verify))
        btnProceed.setup(fontType: .semiBold, textColor: .customWhite, bgColor: .taleColor, text: Language.getText(with: LanguageConstants.btn_upi_proceed))
//        if let _ = CoredataManager.updateDetail(App_Config.self, key: "appStage", value: "4", isSave: true) {}
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupUI()
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func showAlert(msg: String) {
        DispatchQueue.main.async {
            self.responseLabel.text = msg
        }
    }
    func checkValues() -> Bool {
        showAlert(msg: "")
        var upis = ""
        guard let upi = upiField.text, upi.count > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_upi) ?? Constants.alert_enter_upi)
            return false
        }
        upis = upi + "@indus"
        if !upis.isValidUPI {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_valid_upi) ?? Constants.alert_enter_valid_upi)
            return false
        }
        return true
    }
//    @IBAction func verifyButtonTapped(_ sender: Any) {
//        if checkValues() {
//            DispatchQueue.main.async {
//                //self.verifierView.isHidden = false
//            }
//        }
//    }
    
    @IBAction func proceedButtonTapped(_ sender: Any) {
        if checkValues() {
            if isFirst {
                isFirst = false
                DispatchQueue.main.async {
                    self.borderView.layer.borderColor = UIColor.customGreen.cgColor
                    self.statusImg.image = UIImage.init(named: "success1")
                    
                    if var userInfo = UserDefaults.standard.value(forKey: Constants.User_Info) as? [String: String] {
                        if let upi2 = userInfo["upi2"] {
                            self.showAlert(msg: "Cannot create more than 2 UPI ID.")
                            return
                        } else if let upi1 = userInfo["upi"] {
                            userInfo["upi2"] = "\(self.upiField.text ?? "12345")@indus"
                            UserDefaults.standard.setValue(userInfo, forKey: Constants.User_Info)
                        } else {
                        userInfo["upi"] = "\(self.upiField.text ?? "12345")@indus"
                        UserDefaults.standard.setValue(userInfo, forKey: Constants.User_Info)
                        }
                    } else {
                        let user = ["upi" : "\(self.upiField.text ?? "12345")@indus" ]
                        UserDefaults.standard.setValue(user, forKey: Constants.User_Info)
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                        self.present(getSetMPinViewController(), animated: true, completion: nil)
                    })
                }
            } else {
                isFirst = true
                DispatchQueue.main.async {
                    self.borderView.layer.borderColor = UIColor.customRed.cgColor
                    self.statusImg.image = UIImage.init(named: "failiure")
                    self.responseLabel.text = "Username already exist. Try something new."
                    //self.present(getAddNewUPIVerificationViewController(), animated: true, completion: nil)
                }
            }
            
        }
    }
    
}

extension AddNewUPIViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.async {
        self.responseLabel.text = ""
        self.statusImg.image = UIImage()
        self.borderView.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
}
