//
//  ManageUPIViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 19/02/21.
//

import UIKit

class ManageUPIViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewUpi1: UIView!
    @IBOutlet weak var lblupi1: UILabel!
    @IBOutlet weak var lblPrimaryId1: UILabel!
    @IBOutlet weak var imgPrimary1: UIImageView!
    @IBOutlet weak var lblSetPrimary: UILabel!
    @IBOutlet weak var btnSetPrimary1: UIButton!
    
    @IBOutlet weak var viewUpi2: UIView!
    @IBOutlet weak var lblupi2: UILabel!
    @IBOutlet weak var lblPrimaryId2: UILabel!
    @IBOutlet weak var imgPrimary2: UIImageView!
    @IBOutlet weak var lblSetPrimary2: UILabel!
    @IBOutlet weak var btnSetPrimary2: UIButton!
    
    @IBOutlet weak var viewAddNew: UIView!
    @IBOutlet weak var lblAddNewUpi: UILabel!
    @IBOutlet weak var btnDeleteUpi1: UIButton!
    @IBOutlet weak var btnDeleteUpi2: UIButton!
    
    @IBOutlet weak var upiTableView: UITableView!
    
    
    var upiAdrress : App_User?
    var selectedIndex : IndexPath?
    var upiIds = ["amanx609" , "goutham"]
    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular,text: Language.getText(with: LanguageConstants.lbl_manage_upi_title))
        lblupi1.setup(fontSize: 16, fontType: .semiBold, textColor: .titleColor)
        lblPrimaryId1.setup(fontSize: 13, fontType: .regular, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_primary_id))
        lblSetPrimary.setup(fontSize: 15, fontType: .regular, textColor: .darkGray, text: Language.getText(with: LanguageConstants.lbl_set_primary))
        lblupi2.setup(fontSize: 16, fontType: .semiBold, textColor: .titleColor)
        lblPrimaryId2.setup(fontSize: 13, fontType: .regular, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_primary_id))
        lblSetPrimary2.setup(fontSize: 15, fontType: .regular, textColor: .darkGray, text: Language.getText(with: LanguageConstants.lbl_set_primary))
        lblAddNewUpi.setup(fontSize: 15, fontType: .semiBold, textColor: .customBlue, text: Language.getText(with: LanguageConstants.lbl_add_new_upi))
        
        if let upiAddressData = CoredataManager.getDetail(App_User.self){
            upiAdrress = upiAddressData
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func addNewUPIButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.present(getAddNewUPIViewController(), animated: true, completion: nil)
        }
    }
}

extension ManageUPIViewController : UITableViewDelegate{
    
}
extension ManageUPIViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return upiIds.count + 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == upiIds.count{
            let addBankCell = tableView.dequeueReusableCell(withIdentifier: "AddNewUpiIdCell", for:indexPath) as? AddNewUpiIdCell
            addBankCell?.setupValues()
            return addBankCell ?? UITableViewCell()
        }else{
            let upiAddressCell = tableView.dequeueReusableCell(withIdentifier: "UpiAddressCell", for:indexPath) as? UpiAddressCell
            upiAddressCell?.deleteDelegate = self
            switch indexPath.row{
            case 0:
                upiAddressCell?.setupValues(title: upiAdrress!.virtualAddress ?? "aman@cscpay", acType: "Saving", isPrimary: true, icon: UIImage(named: "ICICI"))
            default:
                upiAddressCell?.setupValues(title: upiAdrress!.virtualAddress ?? "aman@cscpay", acType: "Saving", isPrimary: false, icon: UIImage(named: "ICICI"))
            }
            return upiAddressCell ?? UITableViewCell()
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if indexPath.row == self.upiIds.count{
                self.present(getAddNewUPIViewController(), animated: true, completion: nil)
            }else{
                if let cell = tableView.cellForRow(at: indexPath) as? UpiAddressCell{
                    cell.setupValues(title: self.upiAdrress!.virtualAddress ?? "aman@cscpay", acType: "Saving", isPrimary: true ,  icon: UIImage(named: "ICICI"))
                }
            }
        }
    }
    
    
}

extension ManageUPIViewController : DeleteButtonDelegate{
    func deleteButtonTapped(indexPath: IndexPath) {
        self.selectedIndex = indexPath
        upiTableView.beginUpdates()
        upiIds.remove(at: indexPath.row)
        upiTableView.deleteRows(at: [indexPath], with: .none)
        upiTableView.endUpdates()
    }
}
