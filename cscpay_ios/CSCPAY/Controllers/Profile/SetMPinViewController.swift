//
//  SetMPinViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 22/06/21.
//

import UIKit

class SetMPinViewController: UIViewController {

    @IBOutlet weak var pFirst: UITextField!
    @IBOutlet weak var pSecond: UITextField!
    @IBOutlet weak var pThird: UITextField!
    @IBOutlet weak var pFourth: UITextField!
    
    @IBOutlet weak var cpFirst: UITextField!
    @IBOutlet weak var cpSecond: UITextField!
    @IBOutlet weak var cpThird: UITextField!
    @IBOutlet weak var cpFourth: UITextField!
    @IBOutlet weak var lblResponse: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func createMPinBtn(_ sender: Any) {
        DispatchQueue.main.async {
            self.checkValues()
        }
    }
    
    func checkValues() {
        var enterPin = ""
        var confirmPin = ""
        showAlert(msg: "")
        guard let pfirst = pFirst.text, pfirst.count > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_name) ?? Constants.alert_enter_name)
            return
        }
        guard let pSecond = pSecond.text, pSecond.count > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_name) ?? Constants.alert_enter_name)
            return
        }
        guard let pThird = pThird.text, pThird.count > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_name) ?? Constants.alert_enter_name)
            return
        }
        guard let pFourth = pFourth.text, pFourth.count > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_name) ?? Constants.alert_enter_name)
            return
        }
        
        guard let cpFirst = cpFirst.text, cpFirst.count > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_name) ?? Constants.alert_enter_name)
            return
        }
        guard let cpSecond = cpSecond.text, cpSecond.count > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_name) ?? Constants.alert_enter_name)
            return
        }
        guard let cpThird = cpThird.text, cpThird.count > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_name) ?? Constants.alert_enter_name)
            return
        }
        guard let cpFourth = cpFourth.text, cpFourth.count > 0 else {
            showAlert(msg: Language.getText(with: LanguageConstants.alert_enter_name) ?? Constants.alert_enter_name)
            return
        }
        enterPin = "\(pfirst)\(pSecond)\(pThird)\(pFourth)"
        confirmPin = "\(cpFirst)\(cpSecond)\(cpThird)\(cpFourth)"
        if enterPin == confirmPin{
           let vc = getSuccessViewController() as! SuccessViewController
           vc.isFromLink = true
           self.present(vc, animated: true, completion: nil)
        }else{
            showAlert(msg: "Password not match")
            return
        }
    }
    
    func showAlert(msg: String) {
        DispatchQueue.main.async {
            self.lblResponse.text = msg
        }
    }
}

