//
//  PaymentMethodsViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 22/02/21.
//

import UIKit

class PaymentMethodsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var selectedBankAccountList : [Account_List]?
    var userDetails : App_User?
    var masterBankList : [Master_BankList]?
    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular,text: Language.getText(with: LanguageConstants.lbl_payment_method_title))
        self.tableView.tableFooterView = UIView()
        print(userDetails)
        masterBankList = CoredataManager.getDetails(Master_BankList.self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        selectedBankAccountList = CoredataManager.getDetails(Account_List.self)
        userDetails = CoredataManager.getDetail(App_User.self)
        masterBankList = CoredataManager.getDetails(Master_BankList.self)
        tableView.reloadData()
    }
    
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension PaymentMethodsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CoredataManager.returnBankCount() + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {


        if indexPath.row == CoredataManager.returnBankCount(){
            let addBankCell = tableView.dequeueReusableCell(withIdentifier: "AddNewBankCell", for:indexPath) as? AddNewBankCell
            addBankCell?.setupValues()
            return addBankCell ?? UITableViewCell()
        }else{
            let bankCell = tableView.dequeueReusableCell(withIdentifier: "PaymentMethodCell", for:indexPath) as? PaymentMethodCell
            var primary : Bool = false
//             let bank = masterBankList?.filter({$0.bankCode == selectedBankAccountList![indexPath.row].bankCode})
//            if indexPath.row == 0 {
//                    bankCell?.setupValues(title: bank![indexPath.row].bankName ?? "ICICI BANK", acType: "Saving Account", isPrimary: true , icon: bank![indexPath.row].bankCode)
//                    CoredataManager.updateDetail(Account_List.self, key: "isPrimary", value: "T")
//                }else{
//                    if selectedBankAccountList?[indexPath.row].isPrimary == "T"{ primary = true}else{primary = false}
//                    bankCell?.setupValues(title: bank![indexPath.row].bankName ?? "ICICI BANK", acType: "Saving Account", isPrimary: primary , icon: bank![indexPath.row].bankCode)
//                }
            
            
            
            if CoredataManager.returnBankCount() == 1 {
                bankCell?.setupValues(title: selectedBankAccountList![indexPath.row].bankCode ?? "ICICI BANK", acType: "Saving Account", isPrimary: true , icon: selectedBankAccountList![indexPath.row].bankCode)
                CoredataManager.updateDetail(Account_List.self, key: "isPrimary", value: "T")
            }else{
                if selectedBankAccountList?[indexPath.row].isPrimary == "T"{ primary = true}else{primary = false}
                bankCell?.setupValues(title: selectedBankAccountList![indexPath.row].bankCode ?? "ICICI BANK", acType: "Saving Account", isPrimary: primary , icon: selectedBankAccountList![indexPath.row].bankCode)
            }
            return bankCell ?? UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if indexPath.row == CoredataManager.returnBankCount(){
                self.present(getSelectYourBankViewController(), animated: true, completion: nil)
            } else {
                let vc = getBankDetailsViewController() as! BankDetailsViewController
                vc.bankDetails = self.selectedBankAccountList?[indexPath.row]
                self.present(vc, animated: true, completion: nil) }
        }
    }
}


extension PaymentMethodsViewController{
    
    
    
}
