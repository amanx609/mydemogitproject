//
//  SettingsViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 12/01/21.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var usernamelabel: UILabel!
    @IBOutlet weak var mobileField: UILabel!
    @IBOutlet weak var upiLabel: UILabel!
    @IBOutlet weak var viewMenu: UIView!
    @IBOutlet weak var lblBankAccountMenu: UILabel!
    @IBOutlet weak var lblBankAccountSubmenu: UILabel!
    @IBOutlet weak var lblUpiMenu: UILabel!
    @IBOutlet weak var lblUpiSubmenu: UILabel!
    @IBOutlet weak var lblTrendsMenu: UILabel!
    @IBOutlet weak var lblTrendsSubmenu: UILabel!
    @IBOutlet weak var lblSupportMenu: UILabel!
    @IBOutlet weak var lblSupportSubmenu: UILabel!
    @IBOutlet weak var lblNotificationsMenu: UILabel!
    @IBOutlet weak var lblNotificationsSubmenu: UILabel!
    @IBOutlet weak var lblLogoutMenu: UILabel!
    @IBOutlet weak var lblLogoutSubmenu: UILabel!
    @IBOutlet weak var lblCloseAcMenu: UILabel!
    @IBOutlet weak var lblCloseAcSubmenu: UILabel!
    @IBOutlet weak var lblDefault: UILabel!
    @IBOutlet weak var switchBusiness: UISwitch!
    @IBOutlet weak var notificationSwitch: UISwitch!
    @IBOutlet weak var lblChangePinMenu: UILabel!
    @IBOutlet weak var lblChangePinSubMenu: UILabel!
    
    var isMerchant = false
    
    func setupUI() {
        lblTitle.setup(fontSize: 24, fontType: .regular,text: Language.getText(with: LanguageConstants.lbl_profile_title))
        usernamelabel.setup(fontSize: 18, fontType: .bold)
        mobileField.setup(fontSize: 14)
        upiLabel.setup(fontSize: 14)
        lblDefault.setup(fontType: .semiBold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_default_home))
        lblBankAccountMenu.setup(fontType: .semiBold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_bank_account_menu))
        lblBankAccountSubmenu.setup(fontSize: 12, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_bank_account_submenu))
        lblUpiMenu.setup(fontType: .semiBold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_upi_menu))
        lblUpiSubmenu.setup(fontSize: 12, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_upi_submenu))
        lblTrendsMenu.setup(fontType: .semiBold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_trends_menu))
        lblTrendsSubmenu.setup(fontSize: 12, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_trends_submenu))
        lblSupportMenu.setup(fontType: .semiBold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_support_menu))
        lblSupportSubmenu.setup(fontSize: 12, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_support_submenu))
        lblNotificationsMenu.setup(fontType: .semiBold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_notifications_menu))
        lblNotificationsSubmenu.setup(fontSize: 12, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_notifications_submenu))
        lblLogoutMenu.setup(fontType: .semiBold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_logout_menu))
        lblLogoutSubmenu.setup(fontSize: 12, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_logout_submenu))
        lblCloseAcMenu.setup(fontType: .semiBold, textColor: .customBlack, text: Language.getText(with: LanguageConstants.lbl_close_account_menu))
        lblCloseAcSubmenu.setup(fontSize: 12, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_close_account_submenu))
//        lblChangePinMenu.setup(fontSize: 12, textColor: .subtitleColor, text: Language.getText(with: LanguageConstants.lbl_change_pin_menu))
        lblChangePinMenu.setup(fontType: .semiBold, textColor: .customBlack, text:  Language.getText(with: LanguageConstants.lbl_change_pin_menu))
        viewMenu.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        if let img = UD.SharedManager.getImageFromUserDefault(key:Constants.User_Image) {
            self.imgProfile.image = img
        }
        
        if let user = CoredataManager.getDetail(App_User.self) {
            if let name = user.vpaName {
                self.usernamelabel.text = name
            }
            if let mob = user.mobile {
                self.mobileField.text = mob
            }
            if let vpa = user.virtualAddress {
                self.upiLabel.text = vpa
            }
            if user.isMerchant {
                self.lblDefault.text = "Switch to Merchant Profile"
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func profilePicBtnTapped(_ sender: Any) {
        showAlert()
        
    }
    @IBAction func editDetialsButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            let vc = getUserDetialsViewController() as! UserDetialsViewController
            vc.isEdit = true
            self.present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func becomeMerchantButtonTapped(_ sender: Any) {
        if isMerchant {
            switchBusiness.isOn = true
            switchBusiness.onTintColor = .customBlue
            self.present(getMerchantHomeViewController(), animated: true, completion: nil)
        } else {
            if let userInfo = UserDefaults.standard.value(forKey: Constants.User_Info) as? [String: String] {
                if let _ = userInfo["merchant"]  {
                    switchBusiness.isOn = true
                    switchBusiness.onTintColor = .customBlue
                    //Merchant already available
                    self.present(getMerchantHomeViewController(), animated: true, completion: nil)
                }else {
                    self.present(getGeneralDetailsViewController(), animated: true, completion: nil)
                }
            }
        }
//        self.present(getGeneralDetailsViewController(), animated: true, completion: nil)
    }
    
    
    @IBAction func switchBusinessAccount(_ sender: UISwitch) {
        if isMerchant{
            switchBusiness.isOn = true
            switchBusiness.onTintColor = .customBlue
            self.present(getMerchantHomeViewController(), animated: true, completion: nil)
        }else{
            if let userInfo = UserDefaults.standard.value(forKey: Constants.User_Info) as? [String:String]{
                if let _ = userInfo["merchant"]{
                    switchBusiness.isOn = true
                    switchBusiness.onTintColor = .customBlue
                    self.present(getMerchantHomeViewController(), animated: true, completion: nil)
                }
            }
            self.present(getGeneralDetailsViewController(), animated: true, completion: nil)
        }
    }
    
    
    @IBAction func paymentMethodButtonTapped(_ sender: Any) {
        self.present(getPaymentMethodsViewController(), animated: true, completion: nil)
    }
    @IBAction func upiAddressButtonTapped(_ sender: Any) {
        self.present(getManageUPIViewController(), animated: true, completion: nil)
    }
    @IBAction func changePinButtonTapped(_ sender: Any) {
        self.present(getChangeCSCPayPinViewController(), animated: true, completion: nil)
    }
    @IBAction func helpAndFeedbackButtonTapped(_ sender: Any) {
        notImplementedYetAlert(base: self)
        //self.present(getHelp_FeedbackViewController(), animated: true, completion: nil)
    }
    @IBAction func faqButtonTapped(_ sender: Any) {
        self.present(getFAQViewController(), animated: true, completion: nil)
    }
    @IBAction func logoutButtonTapped(_ sender: Any) {
        notImplementedYetAlert(base: self)
    }
    @IBAction func closeAccountButtonTapped(_ sender: Any) {
        self.present(getOTPViewController(), animated: true, completion: nil)
//        notImplementedYetAlert(base: self)
    }
}

extension SettingsViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showAlert() {
        let alert = UIAlertController(title: "", message: Constants.alert_choose_image, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: Constants.alert_open_gallery, style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: Constants.alert_capture_image, style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: Constants.alert_remove_image, style: .default, handler: { (action: UIAlertAction) in
            self.imgProfile.image = UIImage(named: "avatarPlaceholder")
            UD.SharedManager.removeValue(key: Constants.User_Image)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //get image from source type
    func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            //imagePickerController.cameraViewTransform = imagePickerController.cameraViewTransform.scaledBy(x: -1, y: 1)
            if sourceType == .camera {
                imagePickerController.cameraDevice = UIImagePickerController.CameraDevice.front }
            imagePickerController.allowsEditing = true
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.editedImage.rawValue)] as! UIImage
        let afterResizeImg = image.resize(to: imgProfile.frame.size)
        self.imgProfile.image = afterResizeImg //.
        //self.imgAvatar.cornerRadius = imgProfile.frame.size.height / 2
        UD.SharedManager.saveImageInUserDefault(img:afterResizeImg, key:Constants.User_Image)
        self.dismiss(animated: true, completion: nil)
    }
}


/* @IBAction func mobileButtonTapped(_ sender: Any) {
    self.present(getMobileNumberViewController(), animated: true, completion: nil)
}
@IBAction func languageButtonTapped(_ sender: Any) {
    self.present(getChooseLanguageViewController(), animated: true, completion: nil)
} */


/*
if let userInfo = UserDefaults.standard.value(forKey: Constants.User_Info) as? [String: String] {
    if let name = userInfo["name"] {
        self.usernamelabel.text = name
    }
    if let mob = userInfo["mobile"] {
        self.mobileField.text = mob
    }
}
if let userInfo = UserDefaults.standard.value(forKey: Constants.User_Info) as? [String: String] {
    if let upi2 = userInfo["upi2"] {
        if let upi1 = userInfo["upi"] {
            self.upiLabel.text = upi1 + "\n" + upi2
        }
    } else {
    if let upi1 = userInfo["upi"] {
        self.upiLabel.text = upi1
    } }
} else {
    self.upiLabel.text = "UPI not available."
} */
