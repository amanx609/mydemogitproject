//
//  FAQViewController.swift
//  CSC PAY
//
//  Created by Gowtham on 19/02/21.
//

import UIKit

class CSCPayGuideController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var selectedCell:IndexPath?
    var titleText : String = "CSC Pay Guide"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = titleText
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 50;
        self.tableView.reloadData()
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension CSCPayGuideController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier : String!
    reuseIdentifier = String(format: "ExpandCell")

        let expandCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath) as? ExpandCell
        expandCell?.textView.halfTextColorChange(fullText: Constants.Pay_Guide_Items[0]["message"] as! String , changeText: Constants.Pay_Guide_Items[0]["change"] as! String , color: hexStringToUIColor(hex: "1160DA"), textSize: 14)
        
        expandCell?.titleLabel.text = Constants.Pay_Guide_Items[0]["title"] as! String
        //expandCell?.textView.text = Constants.Pay_Guide_Items[0]["message"] as! String
        if let selectedCell = selectedCell, selectedCell == indexPath {
            expandCell?.textView.isHidden = false
            
        } else {
            expandCell?.textView.isHidden = true
        }

        return expandCell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedCell == indexPath {
            selectedCell = nil
        } else {
            selectedCell = indexPath }
        self.tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {

            let reuseIdentifier : String!
        reuseIdentifier = String(format: "GuideHeaderCell")

            let headerView = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: NSIndexPath(row: 0, section: 0) as IndexPath) as! GuideHeaderCell
        headerView.titleLabel.text = "Simple steps to help you understand how you can send and receive money in a hassle free manner using QR code"

            return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
}

class ExpandCell : UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()        // Initialization code
        textView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        textView.sizeToFit()
        textView.layoutIfNeeded()
    }
}

