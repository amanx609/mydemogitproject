//
//  AtmPinViewController.swift
//  CSCPAY
//
//  Created by Gowtham on 23/06/21.
//

import UIKit

class AtmPinViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func doneButtonTapped(_ sender: Any) {
        self.present(getSetUPIPinViewController(), animated: true, completion: nil)
    }
}
