//
//  Extension.swift
//  CSC PAY
//
//  Created by Abhishek Ranjan on 06/01/21.
//

import Foundation
import UIKit
import CommonCrypto
import CryptoKit
import CoreImage
import LocalAuthentication

//MARK: - Activity Indicators
extension UIViewController {
    func showActivityIndicator() {
        DispatchQueue.main.async {
            let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 55, height: 55))
            //activityIndicator.borderWidth = 1.0
            self.view.isUserInteractionEnabled = false
            activityIndicator.borderColor = .lightGray
            activityIndicator.backgroundColor = .white// UIColor(red:0.16, green:0.17, blue:0.21, alpha:1)
            activityIndicator.layer.cornerRadius = 5 // 27.5
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.style = .medium // .UIActivityIndicatorView.Style.large
            //self.view.alpha = 0.5
            activityIndicator.startAnimating()
            //UIApplication.shared.beginIgnoringInteractionEvents()

            activityIndicator.tag = 100 // 100 for example

            // before adding it, you need to check if it is already has been added:
            for subview in self.view.subviews {
                if subview.tag == 100 {
                    print("already added")
                    return
                }
            }
            self.view.addBlurEffect()
            self.view.addSubview(activityIndicator)
        }
    }

    func hideActivityIndicator() {
        DispatchQueue.main.async {
            let activityIndicator = self.view.viewWithTag(100) as? UIActivityIndicatorView
            activityIndicator?.stopAnimating()
            self.view.removeBlurEffect()

            // I think you forgot to remove it?
            self.view.isUserInteractionEnabled = true
            self.view.alpha = 1.0
            activityIndicator?.removeFromSuperview()
            //UIApplication.shared.endIgnoringInteractionEvents()
        }
        
    }
}
extension UIView {
  /// Remove UIBlurEffect from UIView
  func removeBlurEffect() {
    let blurredEffectViews = self.subviews.filter{$0 is UIVisualEffectView}
        blurredEffectViews.forEach{ blurView in
        blurView.removeFromSuperview()
    }
  }
    func addBlurEffect(effect :UIBlurEffect.Style = .dark, alphas : CGFloat = 0.4) {
        let blurEffect = UIBlurEffect(style: effect) // UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = alphas
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
}

extension UIImage {
    func resize(to targetSize: CGSize) -> UIImage? {
        let newSize = CGSize(width: 90, height: 90)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height + 1)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
     func resizeImage(image: UIImage, newHeight: CGFloat) -> UIImage {
            let scale = newHeight / image.size.height
            let newWidth = image.size.width * scale
            UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
            image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return newImage!
        }
}

extension UIView {
  func addDashedBorder(_ color: UIColor = hexStringToUIColor(hex: "D7DAE6") , withWidth width: CGFloat = 2, cornerRadius: CGFloat = 10, dashPattern: [NSNumber] = [3,6]) {

    let shapeLayer = CAShapeLayer()

    shapeLayer.bounds = bounds
    shapeLayer.position = CGPoint(x: bounds.width/2, y: bounds.height/2)
    shapeLayer.fillColor = nil
    shapeLayer.strokeColor = color.cgColor
    shapeLayer.lineWidth = width
    shapeLayer.lineJoin = CAShapeLayerLineJoin.round // Updated in swift 4.2
    shapeLayer.lineDashPattern = dashPattern
    shapeLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath

    self.layer.addSublayer(shapeLayer)
  }
}

extension CIImage {
    /// Inverts the colors and creates a transparent image by converting the mask to alpha.
    /// Input image should be black and white.
    var transparent: CIImage? {
        return inverted?.blackTransparent
    }

    /// Inverts the colors.
    var inverted: CIImage? {
        guard let invertedColorFilter = CIFilter(name: "CIColorInvert") else { return nil }

        invertedColorFilter.setValue(self, forKey: "inputImage")
        return invertedColorFilter.outputImage
    }

    /// Converts all black to transparent.
    var blackTransparent: CIImage? {
        guard let blackTransparentFilter = CIFilter(name: "CIMaskToAlpha") else { return nil }
        blackTransparentFilter.setValue(self, forKey: "inputImage")
        return blackTransparentFilter.outputImage
    }

    /// Applies the given color as a tint color.
    func tinted(using color: UIColor) -> CIImage?
    {
        guard
            let transparentQRImage = transparent,
            let filter = CIFilter(name: "CIMultiplyCompositing"),
            let colorFilter = CIFilter(name: "CIConstantColorGenerator") else { return nil }

        let ciColor = CIColor(color: color)
        colorFilter.setValue(ciColor, forKey: kCIInputColorKey)
        let colorImage = colorFilter.outputImage

        filter.setValue(colorImage, forKey: kCIInputImageKey)
        filter.setValue(transparentQRImage, forKey: kCIInputBackgroundImageKey)

        return filter.outputImage!
    }
}

extension CIImage {

    /// Combines the current image with the given image centered.
    func combined(with image: CIImage) -> CIImage? {
        guard let combinedFilter = CIFilter(name: "CISourceOverCompositing") else { return nil }
        let centerTransform = CGAffineTransform(translationX: extent.midX - (image.extent.size.width / 2), y: extent.midY - (image.extent.size.height / 2))
        combinedFilter.setValue(image.transformed(by: centerTransform), forKey: "inputImage")
        combinedFilter.setValue(self, forKey: "inputBackgroundImage")
        return combinedFilter.outputImage!
    }
}

extension URL {

    /// Creates a QR code for the current URL in the given color.
    func qrImage(using color: UIColor, logo: UIImage? = nil) -> CIImage? {
        let tintedQRImage = qrImage?.tinted(using: color)

        guard let logo = logo?.cgImage else {
            return tintedQRImage
        }

        return tintedQRImage?.combined(with: CIImage(cgImage: logo))
    }

    // Returns a black and white QR code for this URL.
    var qrImage: CIImage? {
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return nil }
        let qrData = absoluteString.data(using: String.Encoding.ascii)
        qrFilter.setValue(qrData, forKey: "inputMessage")

        let qrTransform = CGAffineTransform(scaleX: 10, y: 10)
        return qrFilter.outputImage?.transformed(by: qrTransform)
    }
}

extension UILabel {
    func halfTextColorChange (fullText : String , changeText : String, color: UIColor, textSize : CGFloat ) {
        
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attrs = [NSAttributedString.Key.font :UIFont.size(16, fontType: .regular)] // UIFont (name: "Nunito-Regular", size: 16)]
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
        attribute.addAttributes(attrs as [NSAttributedString.Key : Any], range: range)
        self.attributedText = attribute
    }
    func halfTextColorChange2 (fullText : String , changeText : String, color: UIColor, textSize : CGFloat , extra:Bool = false, extraText: String = "", extraTextColor: UIColor = .systemBlue, extraTextSize: CGFloat = 14) {
        
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: textSize)]
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
        
        if extra {
            let rangeTitle = (strNumber).range(of: extraText)
            let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: extraTextSize)]
            //let attribute1 = NSMutableAttributedString.init(string: fullText)
            attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: extraTextColor , range: rangeTitle)
            attribute.addAttributes(attrs, range: rangeTitle)
        }
        
        attribute.addAttributes(attrs, range: range)
        self.attributedText = attribute
    }
}
extension UITextView {
    func halfTextColorChange (fullText : String , changeText : String, color: UIColor, textSize : CGFloat = 20 ) {
        
        let strNumber: NSString = fullText as NSString
        let range2 = (strNumber).range(of: strNumber as String)
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        
        let range = (strNumber).range(of: changeText)
        let attrs = [NSAttributedString.Key.font : UIFont (name: "Nunito-Regular", size: textSize)]
        let attribute = NSMutableAttributedString.init(string: fullText, attributes: [.paragraphStyle: paragraph])
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
        attribute.addAttributes(attrs as [NSAttributedString.Key : Any], range: range2)
        attribute.addAttributes(attrs as [NSAttributedString.Key : Any], range: range)
        self.attributedText = attribute
    }
}

extension UITapGestureRecognizer {

    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        //let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                              //(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)

        //let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
                                                        // locationOfTouchInLabel.y - textContainerOffset.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }

}

extension UIButton {
    func underline() {
        guard let title = self.titleLabel else { return }
        guard let tittleText = title.text else { return }
        let attributedString = NSMutableAttributedString(string: (tittleText))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: (tittleText.count)))
        self.setAttributedTitle(attributedString, for: .normal)
    }
}

@IBDesignable
class DesignableView: UIView {
}

@IBDesignable
class DesignableButton: UIButton {
}

@IBDesignable
class DesignableLabel: UILabel {
}


extension UIView {
  
  @IBInspectable
  var cornerRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set {
      layer.cornerRadius = newValue
    }
  }
  
  @IBInspectable
  var borderWidth: CGFloat {
    get {
      return layer.borderWidth
    }
    set {
      layer.borderWidth = newValue
    }
  }
  
  @IBInspectable
  var borderColor: UIColor? {
    get {
      if let color = layer.borderColor {
        return UIColor(cgColor: color)
      }
      return nil
    }
    set {
      if let color = newValue {
        layer.borderColor = color.cgColor
      } else {
        layer.borderColor = nil
      }
    }
  }
  
  @IBInspectable
  var shadowRadius: CGFloat {
    get {
      return layer.shadowRadius
    }
    set {
      layer.shadowRadius = newValue
    }
  }
  
  @IBInspectable
  var shadowOpacity: Float {
    get {
      return layer.shadowOpacity
    }
    set {
      layer.shadowOpacity = newValue
    }
  }
  
  @IBInspectable
  var shadowOffset: CGSize {
    get {
      return layer.shadowOffset
    }
    set {
      layer.shadowOffset = newValue
    }
  }
  
  @IBInspectable
  var shadowColor: UIColor? {
    get {
      if let color = layer.shadowColor {
        return UIColor(cgColor: color)
      }
      return nil
    }
    set {
      if let color = newValue {
        layer.shadowColor = color.cgColor
      } else {
        layer.shadowColor = nil
      }
    }
  }
    //MARK: - Set corner radius only specific corner
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        
                let shapeLayer = CAShapeLayer()
                shapeLayer.position = self.center
                self.layer.masksToBounds = true
                self.clipsToBounds = true
                let bezirePath = UIBezierPath(roundedRect: self.bounds,
                                              byRoundingCorners: corners,
                                              cornerRadii: CGSize(width: radius, height: radius))
                shapeLayer.bounds = frame
                shapeLayer.path = bezirePath.cgPath

                self.layer.mask = shapeLayer
        }
    //MARK: - Add animation while unhide the view
    func addAnimation(duration : Double = 0.8) {
        let transition1: CATransition = CATransition()
        let timeFunc1 : CAMediaTimingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
            transition1.duration = duration
            transition1.timingFunction = timeFunc1
            transition1.type = CATransitionType.push
            transition1.subtype = CATransitionSubtype.fromTop
        self.layer.add(transition1, forKey: kCATransition)
        self.isHidden = false
    }
    //MARK: - Hide animation while hide view
    func hideAnimation() {
        let transition1: CATransition = CATransition()
        let timeFunc1 : CAMediaTimingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        transition1.duration = 0.8
            transition1.timingFunction = timeFunc1
        transition1.type = CATransitionType.fade
            transition1.subtype = CATransitionSubtype.fromBottom
        self.layer.add(transition1, forKey: kCATransition)
        self.isHidden = true
    }
}

//MARK: - GradientView in IBDesignable
@IBDesignable
final class GradientView: UIView {

    @IBInspectable var firstColor: UIColor = .clear { didSet { updateView() } }
    @IBInspectable var secondColor: UIColor = .clear { didSet { updateView() } }

    @IBInspectable var startPoint: CGPoint = CGPoint(x: 0, y: 0) { didSet { updateView() } }
    @IBInspectable var endPoint: CGPoint = CGPoint(x: 1, y: 1) { didSet { updateView() } }

    override class var layerClass: AnyClass { get { CAGradientLayer.self } }

    override func layoutSubviews() {
        super.layoutSubviews()
        updateView()
        layer.frame = bounds
    }

    private func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstColor, secondColor].map {$0.cgColor}
        layer.startPoint = startPoint
        layer.endPoint = endPoint
    }
}

//MARK: - Shake UIView / Button / Label
public extension UIView {

    func shake(count : Float = 3,for duration : TimeInterval = 0.4,withTranslation translation : Float = 12) {

        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.repeatCount = count
        animation.duration = duration/TimeInterval(animation.repeatCount)
        animation.autoreverses = true
        animation.values = [translation, -translation]
        layer.add(animation, forKey: "shake")
    }
}

//MARK: - Hide keyboard tap around
extension UIViewController {
    func screenRecordingDisable() {
        if UIScreen.main.isCaptured {
            self.presentAlert(title: "Screen recording detected.", msg: "Please switch off screen recording to continue further.")
        }
    }
    
    //MARK:- Alert for taking screenshot
    func screenshotDisable() {
        NotificationCenter.default.addObserver(self, selector: #selector(didTakeScreenshot(notification:)), name: UIApplication.userDidTakeScreenshotNotification, object: nil)
    }
    @objc func didTakeScreenshot(notification:Notification) -> Void {
        print("Screen Shot Taken")
        self.presentAlert()
    }

    @objc func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:    #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        //view.endEditing(true)
        DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.view.endEditing(true)
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                })
            }
    }
    

    
    //MARK: - Block using Common Alert
    func presentAlert(title: String = "Error", msg: String = "We found some security issue with this device. So you are not allowed to access this app.\n") {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
            let blockView = UIViewController()
            blockView.view.frame = UIScreen.main.bounds
            blockView.view.backgroundColor = .white
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
            //blockView.present(alert, animated: true, completion: nil)
    }
}
//MARK: -  HEX TO BIN , BIN TO DEC, ETC.,
extension StringProtocol {
    func dropping<S: StringProtocol>(prefix: S) -> SubSequence { hasPrefix(prefix) ? dropFirst(prefix.count) : self[...] }
    var hexaToDecimal: Int { Int(dropping(prefix: "0x"), radix: 16) ?? 0 }
    var hexaToBinary: String { .init(hexaToDecimal, radix: 2) }
    var decimalToHexa: String { .init(Int(self) ?? 0, radix: 16) }
    var decimalToBinary: String { .init(Int(self) ?? 0, radix: 2) }
    var binaryToDecimal: Int { Int(dropping(prefix: "0b"), radix: 2) ?? 0 }
    var binaryToHexa: String { .init(binaryToDecimal, radix: 16) }
}
extension BinaryInteger {
    var binary: String { .init(self, radix: 2) }
    var hexa: String { .init(self, radix: 16) }
}

extension String {
    var hexaToBinary: String {
        return hexaToBytes.map {
            let binary = String($0, radix: 2)
            return repeatElement("0", count: 8-binary.count) + binary
        }.joined()
    }

    var hexaToBytes: [UInt8] {
        var start = startIndex
        return stride(from: 0, to: count, by: 2).compactMap { _ in
            let end = index(after: start)
            defer { start = index(after: end) }
            return UInt8(self[start...end], radix: 16)
        }
    }
}
public extension Data {
   init?(hexString: String) {
     let len = hexString.count / 2
     var data = Data(capacity: len)
     var i = hexString.startIndex
     for _ in 0..<len {
       let j = hexString.index(i, offsetBy: 2)
       let bytes = hexString[i..<j]
       if var num = UInt8(bytes, radix: 16) {
         data.append(&num, count: 1)
       } else {
         return nil
       }
       i = j
     }
     self = data
   }
   /// Hexadecimal string representation of `Data` object.
   var hexadecimal: String {
       return map { String(format: "%02x", $0) }
           .joined()
   }
}

extension Data {
  /// A hexadecimal string representation of the bytes.
  func hexEncodedString() -> String {
    let hexDigits = Array("0123456789abcdef".utf16)
    var hexChars = [UTF16.CodeUnit]()
    hexChars.reserveCapacity(count * 2)

    for byte in self {
      let (index1, index2) = Int(byte).quotientAndRemainder(dividingBy: 16)
      hexChars.append(hexDigits[index1])
      hexChars.append(hexDigits[index2])
    }

    return String(utf16CodeUnits: hexChars, count: hexChars.count)
  }
}


extension String {
    //MARK: -  Check valid mobile number
    var isValidContact: Bool {
        let phoneNumberRegex = "^[6-9]\\d{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        let isValidPhone = phoneTest.evaluate(with: self)
        return isValidPhone
    }
    //MARK: -  Check valid mobile number
    var isValidUPI: Bool {
        let phoneNumberRegex = "[a-zA-Z0-9_]{3,}@[a-zA-Z]{3,}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        let isValidPhone = phoneTest.evaluate(with: self)
        return isValidPhone
    }
    //MARK: -  Check valid user name
    func isValidName() -> Bool {
        return self.range(of: "^(?![\\. ])[a-zA-Z\\. ]+(?<! )$", options: .regularExpression) != nil // ^\\A\\w{3,25}\\z"
    }
    
//MARK:- Validate Pan Card
    func validatePANCardNumber() -> Bool{
            let regularExpression = "[A-Z]{5}[0-9]{4}[A-Z]{1}"
            let panCardValidation = NSPredicate(format : "SELF MATCHES %@", regularExpression)
            return panCardValidation.evaluate(with: self)
        }
    
    
    
        //MARK: Validate GST Number
    
    
    func validateGSTnumber() -> Bool{       
        let gstnumberRegEx = "^\\d{2}\\D{5}\\d{4}\\D\\dZ\\w"
        let range = self.range(of: gstnumberRegEx, options:.regularExpression)
        let result = range != nil ? true : false
        return result
    }
    
    
    
    
    
    //MARK: -  Check valid email
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
//        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
//        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let range = self.range(of: emailRegEx, options:.regularExpression)
        let result = range != nil ? true : false
        return result
    }
    //MARK: -  Base64 encoding a string
    func base64Encoded() -> String? {
        if let data = self.data(using: .utf8) {
            return data.base64EncodedString()
        }
        return nil
    }

    //MARK: - Base64 decoding a string
    func base64Decoded() -> String? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
    func base64Decode() -> Data? {
        return Data(base64Encoded: self, options: .ignoreUnknownCharacters)
    }
    //MARK: - Subscript string using range
    subscript(_ range: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: max(0, range.lowerBound))
        let end = index(start, offsetBy: min(self.count - range.lowerBound,
                                             range.upperBound - range.lowerBound))
        return String(self[start..<end])
    }

    subscript(_ range: CountablePartialRangeFrom<Int>) -> String {
        let start = index(startIndex, offsetBy: max(0, range.lowerBound))
         return String(self[start...])
    }
    //MARK: - Base64 decode
   func fromBase64() -> String? {
           guard let data = Data(base64Encoded: self) else {
                   return nil
           }
           return String(data: data, encoding: .utf8)
   }

    //MARK: - Base64 encode
   func toBase64() -> String {
           return Data(self.utf8).base64EncodedString()
   }
    //MARK: - String to HexEncodedString
    func toHexEncodedString(uppercase: Bool = true, prefix: String = "", separator: String = "") -> String {
        return unicodeScalars.map { prefix + .init($0.value, radix: 16, uppercase: uppercase) } .joined(separator: separator)
    }
    
    //MARK: - SHA256
    func sha256(_ outForm : String = "HEX") -> Any {
        guard let stringData = self.data(using: String.Encoding.utf8) else { return "" }
        let sha = sha256digest(input: stringData as NSData)
        if outForm == "B64" {
            return sha.base64EncodedString(options: [])
        } else if outForm == "HEX" {
            var bytes = [UInt8](repeating: 0, count: sha.length)
            sha.getBytes(&bytes, length: sha.length)

            var hexString = ""
            for byte in bytes {
                hexString += String(format:"%02x", UInt8(byte))
            }

            return hexString
        } else {
            return sha
        }
    }
    private func sha256digest(input : NSData) -> NSData {
        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
        var hash = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }

}
extension Date {
    //MARK: - Get date using format "YYYYMMdd"
    func getCurrentDate(format : String = "yyyyMMdd") -> (String, Date) {
        let dateformat = DateFormatter()
        dateformat.timeZone = (NSTimeZone(name: "IST")! as TimeZone)
        dateformat.dateFormat = format
        let str = dateformat.string(from: self)
        return (str, self)
    }
    
    
    func getLocalTime() -> (String , Date){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let str = dateFormatter.string(from: self)
        
         
         if let date = dateFormatter.date(from: str) {
             dateFormatter.timeZone = TimeZone.current
             dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//             return dateFormatter.string(from: date)
             print(date)
             return (str , dateFormatter.date(from: str)!)
         }
        return (str , self)
    }
    
    func utcToLocal(dateStr: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let date = dateFormatter.date(from: dateStr){
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
    
    
}
extension Data {
    //MARK: - Data to HEX Encode
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }
    //MARK: - Hex Encode
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
}
//MARK: - Build App Version
extension Bundle {
    var versionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }

    var buildNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }

    var bundleName: String? {
        return infoDictionary?["CFBundleName"] as? String
    }
}

//MARK: - Insert Elements in array while mapping
extension Array where Element: Hashable {
    func distinct() -> Array<Element> {
        var set = Set<Element>()
        return filter {
            guard !set.contains($0) else { return false }
            set.insert($0)
            return true
        }
    }
}

//MARK: - Set Max Length to the textfield
private var kAssociationKeyMaxLength: Int = 0

    extension UITextField {

        @IBInspectable var maxLength: Int {
            get {
                if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                    return length
                } else {
                    return Int.max
                }
            }
            set {
                objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
                self.addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
            }
        }

//The method is used to cancel the check when use Chinese Pinyin input method.
        //Becuase the alphabet also appears in the textfield when inputting, we should cancel the check.
        func isInputMethod() -> Bool {
            if let positionRange = self.markedTextRange {
                if let _ = self.position(from: positionRange.start, offset: 0) {
                    return true
                }
            }
            return false
        }


        @objc func checkMaxLength(textField: UITextField) {

            guard !self.isInputMethod(), let prospectiveText = self.text,
                prospectiveText.count > maxLength
                else {
                    return
            }

            let selection = selectedTextRange
            let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
            text = prospectiveText.substring(to: maxCharIndex)
            selectedTextRange = selection
        }



    }
//MARK: - Substring extension
extension String {

    var length: Int {
        return count
    }

    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }

    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }

    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }

//    subscript (r: Range<Int>) -> String {
//        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
//                                            upper: min(length, max(0, r.upperBound))))
//        let start = index(startIndex, offsetBy: range.lowerBound)
//        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
//        return String(self[start ..< end])
//    }
}

extension UIViewController {

    func presentDetail(_ viewControllerToPresent: UIViewController, subType : CATransitionSubtype = .fromRight) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = subType
        self.view.window!.layer.add(transition, forKey: kCATransition)

        present(viewControllerToPresent, animated: false)
    }

    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window!.layer.add(transition, forKey: kCATransition)

        dismiss(animated: false)
    }
    
    //MARK: - Local Auth
    func authenticationWithTouchID(completion: (() -> Void)?) {
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = Constants.alert_enter_passcode

        var authorizationError: NSError?
        let reason = Constants.alert_unlock_cscapp

        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: &authorizationError) {
            
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) { success, evaluateError in
                
                if success {
                    DispatchQueue.main.async() {
                        self.view.removeBlurEffect()
                        //self.checkItsFirstTime()
                        }
                    completion!()
                    /*let alert = UIAlertController(title: "Success", message: "Authenticated succesfully!", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in
                            self.view.removeBlurEffect()
                        }))
                        self.present(alert, animated: true, completion: nil) */
                    //}
                    
                } else {
                    DispatchQueue.main.async() {
                        self.view.addBlurEffect()
                        //self.checkItsFirstTime()
                        }
                    // Failed to authenticate
                    guard let error = evaluateError else {
                        return
                    }
                    print(error)
                    DispatchQueue.main.async() {
                        self.presentAlertWithTitle(title: "Unlock To Continue", message: Constants.alert_use_passcode, options: "Retry") { (option) in
                        //print("option: \(option)")
                            self.authenticationWithTouchID(completion: nil)
                    }
                    }
                }
            }
        } else {
            
            guard let error = authorizationError else {
                return
            }
            print(error)
        }
    }
    
    
    
}

extension UIView {

    func blink(duration : Double = 0.8, repeatCount:Float = Float.infinity, autoRevers : Bool = true, visible: Bool = false) {

        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = duration
        flash.fromValue = 0.0 // 1
        flash.toValue = 1.0 //0.1
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        flash.autoreverses = autoRevers
        flash.repeatCount = repeatCount
        layer.add(flash, forKey: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
            self.alpha = 1.0
        }
    }
    
    func takeScreenshot() -> UIImage? {
        var screenshotImage :UIImage?
        let layer = self.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale)
        self.drawHierarchy(in: layer.bounds, afterScreenUpdates: true)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return screenshotImage
    }
}

extension UIViewController {
    func embed(_ viewController:UIViewController, inView view:UIView){
        viewController.willMove(toParent: self)
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.subviews.forEach { $0.removeFromSuperview() }
        self.removeChild()
        view.addSubview(viewController.view)
        self.addChild(viewController)
        viewController.didMove(toParent: self)
    }
    func removeChild() {
        self.children.forEach {
            $0.willMove(toParent: nil)
            $0.view.removeFromSuperview()
            $0.removeFromParent()
        }
    }
}
//MARK:- Setup tableview empty messages
extension UITableView {

    func setEmptyMessage(_ img: UIImage) {
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width - 32, height: self.bounds.size.width - 32))
        imgView.image = img
        imgView.contentMode = .scaleAspectFit
        
        self.backgroundView = imgView
        self.separatorStyle = .none
    }

    func restore(seperator : UITableViewCell.SeparatorStyle = .none) {
        self.backgroundView = nil
        self.separatorStyle = seperator
    }
}

extension UIViewController {

    func presentAlertWithTitle(title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    func dycryptedString(data: String) {
        let creds = CoredataManager.getDetail(App_User.self)
        let dec = AppSecure.tokenDecryptWithGCM(data, aesKey: creds?.aesKey ?? "")
        do {
        if Constants.printResponse {
            //print(dec)
            let json = try JSONSerialization.jsonObject(with: Data(dec.utf8), options: .allowFragments)
            print("Encrypted Data: \(data)")
            print("Non Encrypted Data: \(json)")
        }
            
        }
        catch {
            print("we have an error \(error)")
            
        }
    }
}

extension UITextField {

   func addInputViewDatePicker(target: Any, selector: Selector) {

    let screenWidth = UIScreen.main.bounds.width

    //Add DatePicker as inputView
    let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
    datePicker.datePickerMode = .date
    datePicker.maximumDate = Calendar.current.date(byAdding: .day, value: -1, to: Date())

    datePicker.locale = Locale.init(identifier: "en_gb")
    self.inputView = datePicker

    //Add Tool Bar as input AccessoryView
    let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
    let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let cancelBarButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
    let doneBarButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector)
    toolBar.setItems([cancelBarButton, flexibleSpace, doneBarButton], animated: false)

    self.inputAccessoryView = toolBar
 }

   @objc func cancelPressed() {
     self.resignFirstResponder()
   }
    
    
    
    
    
    
    
    
    
}

extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var seen: Set<Iterator.Element> = []
        return filter { seen.insert($0).inserted }
    }
}

extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()

        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }

    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
    
    
//    func uniqueArray() -> [Element]{
//        
//        var tempArray = [Element]()
//        var j = 0
//        if self.count == 0 || self.count == 1{
//            return self
//        }
//        
//        
//        for i in 0..<self.count{
//            if self[i] != self[i + 1]{
//                tempArray[j] = self[i]
//            }
//            tempArray[j + 1] = self[count - 1]
//            j += 1
//        }        
//        return tempArray
//    }
    
    
    
}
