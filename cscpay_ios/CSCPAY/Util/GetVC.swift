//
//  GetVC.swift
//  CSC PAY
//
//  Created by Gowtham on 05/02/21.
//

import Foundation
import UIKit


//MARK: - Main
let storyboard = UIStoryboard(name: "Main", bundle: nil)
func getSplashViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
    return controller
}
func getOTPViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
    return controller
}
func getOTPVerificationViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "OTPVerificationViewController") as! OTPVerificationViewController
    return controller
}
func getAppPermissionViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "AppPermissionViewController") as! AppPermissionViewController
    return controller
}
func getUserDetialsViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "UserDetialsViewController") as! UserDetialsViewController
    return controller
}
func getPageViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "PageViewController") as! PageViewController
    return controller
}
func getLinkBankAccountViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "LinkBankAccountViewController") as! LinkBankAccountViewController
    return controller
}
func getSelectYourBankViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "SelectYourBankViewController") as! SelectYourBankViewController
    return controller
}
func getSelectBankAccountViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "SelectBankAccountViewController") as! SelectBankAccountViewController
    return controller
}

//MARK: - PROFILE

let profileStoryboard = UIStoryboard(name: "Profile", bundle: nil)

func getSettingsViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
    return controller
}
func getPaymentMethodsViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "PaymentMethodsViewController") as! PaymentMethodsViewController
    return controller
}
func getBankDetailsViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "BankDetailsViewController") as! BankDetailsViewController
    return controller
}
func getCheckBalanceViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "CheckBalanceViewController") as! CheckBalanceViewController
    return controller
}
func getManageUPIViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "ManageUPIViewController") as! ManageUPIViewController
    return controller
}
func getAddNewUPIViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "AddNewUPIViewController") as! AddNewUPIViewController
    return controller
}
func getAddNewUPIVerificationViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "AddNewUPIVerificationViewController") as! AddNewUPIVerificationViewController
    return controller
}
func getHelp_FeedbackViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "Help_FeedbackViewController") as! Help_FeedbackViewController
    return controller
}
func getFAQViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
    return controller
}
func getCSCPayGuideController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "CSCPayGuideController") as! CSCPayGuideController
    return controller
}
func getSetMPinViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "SetMPinViewController") as! SetMPinViewController
    return controller
}
func getSetUPIPinViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "SetUPIPinViewController") as! SetUPIPinViewController
    return controller
}
func getAtmPinViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "AtmPinViewController") as! AtmPinViewController
    return controller
}
func getEnterOTPViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "EnterOTPViewController") as! EnterOTPViewController
    return controller
}
func getChangeUPIThroughCardViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "ChangeUPIThroughCardViewController") as! ChangeUPIThroughCardViewController
    return controller
}
func getChangePinViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "ChangePinViewController") as! ChangePinViewController
    return controller
}
func getChangeCSCPayPinViewController() -> UIViewController {
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "ChangeCSCPayPinViewController") as! ChangeCSCPayPinViewController
    return controller
}

func getUpiPinViewController() -> UIViewController{
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "UpiPinViewController") as! UpiPinViewController
    return controller
}

func getChatViewController() -> UIViewController{
    let controller: UIViewController = profileStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
    return controller
}


//MARK: - PSP
let pspStoryboard = UIStoryboard(name: "PSP", bundle: nil)

func getHomeViewController() -> UIViewController {
    let controller: UIViewController = pspStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
    return controller
}
func getNotificationViewController() -> UIViewController {
    let controller: UIViewController = pspStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
    return controller
}
func getScannerViewController() -> UIViewController {
    let controller: UIViewController = pspStoryboard.instantiateViewController(withIdentifier: "ScannerViewController") as! ScannerViewController
    return controller
}
func getGenerateQRViewController() -> UIViewController {
    let controller: UIViewController = pspStoryboard.instantiateViewController(withIdentifier: "GenerateQRViewController") as! GenerateQRViewController
    return controller
}
func getContactsViewController() -> UIViewController {
    let controller: UIViewController = pspStoryboard.instantiateViewController(withIdentifier: "ContactsViewController") as! ContactsViewController
    return controller
}
func getPayConfirmViewController() -> UIViewController {
    let controller: UIViewController = pspStoryboard.instantiateViewController(withIdentifier: "PayConfirmViewController") as! PayConfirmViewController
    return controller
}
func getPSPReceiveConfirmViewController() -> UIViewController {
    let controller: UIViewController = pspStoryboard.instantiateViewController(withIdentifier: "PSPReceiveConfirmViewController") as! PSPReceiveConfirmViewController
    return controller
}
func getSuccessViewController() -> UIViewController {
    let controller: UIViewController = pspStoryboard.instantiateViewController(withIdentifier: "SuccessViewController") as! SuccessViewController
    return controller
}
func getIncomingRequestViewController() -> UIViewController {
    let controller: UIViewController = pspStoryboard.instantiateViewController(withIdentifier: "IncomingRequestViewController") as! IncomingRequestViewController
    return controller
}
func getTransactionHistoryViewController() -> UIViewController {
    let controller: UIViewController = pspStoryboard.instantiateViewController(withIdentifier: "TransactionHistoryViewController") as! TransactionHistoryViewController
    return controller
}
func getTransactionDetialsViewController() -> UIViewController {
    let controller: UIViewController = pspStoryboard.instantiateViewController(withIdentifier: "TransactionDetialsViewController") as! TransactionDetialsViewController
    return controller
}
func getRequestDetailsViewController() -> UIViewController {
    let controller: UIViewController = pspStoryboard.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
    return controller
}
func getReceiveHistoryViewController() -> UIViewController {
    let controller: UIViewController = pspStoryboard.instantiateViewController(withIdentifier: "ReceiveHistoryViewController") as! ReceiveHistoryViewController
    return controller
}

func getTicketsViewController() -> UIViewController{
    let controller: UIViewController = pspStoryboard.instantiateViewController(withIdentifier: "TicketsViewController") as! TicketsViewController
    return controller
}



//MARK: - Merchant
let merchantStoryboard = UIStoryboard(name: "Merchant", bundle: nil)

func getMerchantSuccessViewController() -> UIViewController{
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "MerchantSucessVC") as! MerchantSucessVC
    return controller
}

func getMerchantPendingViewController() -> UIViewController{
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "PendingDocumentsVC") as! PendingDocumentsVC
    return controller
}

func getMerchantUploadDocsViewController() -> UIViewController{
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "UploadDocsVC") as! UploadDocsVC
    return controller
}

func getPanCardViewController() -> UIViewController{
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "PANCheckVC") as! PANCheckVC
    return controller
}


func getMerchantOnBoardingViewController() -> UIViewController{
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "MerchantOnBoardingVC") as! MerchantOnBoardingVC
    return controller
}


func getChooseYourOptionViewController() -> UIViewController{
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "ChooseYourOptionVC") as! ChooseYourOptionVC
    return controller
}


func getCommonFieldsViewController() -> UIViewController{
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "CommonFeildVC") as! CommonFeildVC
    return controller

}

func getSetMerchantPinViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "SetMerchantPinViewController") as! SetMerchantPinViewController
    return controller

}

func getCheckMerchantPinViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "CheckMerchantPinViewController") as! CheckMerchantPinViewController
    return controller

}


func getGeneralDetailsViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "GeneralDetailsViewController") as! GeneralDetailsViewController
    return controller
}
func getAddressDetailsViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "AddressDetailsViewController") as! AddressDetailsViewController
    return controller
}
func getPanCardAndBankViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "PanCardAndBankViewController") as! PanCardAndBankViewController
    return controller
}
func getSummaryViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "SummaryViewController") as! SummaryViewController
    return controller
}
func getMerchantHomeViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "MerchantHomeViewController") as! MerchantHomeViewController
    return controller
}
func getMerchantGeneralDetailsViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "MerchantGeneralDetailsViewController") as! MerchantGeneralDetailsViewController
    return controller
}
func getBusinessTrendsViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "BusinessTrendsViewController") as! BusinessTrendsViewController
    return controller
}
func getSettlementsViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "SettlementsViewController") as! SettlementsViewController
    return controller
}
func getSettlementViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "SettlementViewController") as! SettlementViewController
    return controller
}
func getMerchantRequestViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "MerchantRequestViewController") as! MerchantRequestViewController
    return controller
}
func getMyQRViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "MyQRViewController") as! MyQRViewController
    return controller
}
func getReceiveConfirmViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "ReceiveConfirmViewController") as! ReceiveConfirmViewController
    return controller
}
func getGenerateMerchantQRViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "GenerateMerchantQRViewController") as! GenerateMerchantQRViewController
    return controller
}
func getMerchantHistoryViewController() -> UIViewController {
    let controller: UIViewController = merchantStoryboard.instantiateViewController(withIdentifier: "MerchantHistoryViewController") as! MerchantHistoryViewController
    return controller
}

/*

func getMobileNumberViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "MobileNumberViewController") as! MobileNumberViewController
    return controller
}
func getChooseLanguageViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "ChooseLanguageViewController") as! ChooseLanguageViewController
    return controller
}


func getHomeViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
    return controller
}
func getMakeRequestViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "MakeRequestViewController") as! MakeRequestViewController
    return controller
}





func getFAQHistoryViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "FAQHistoryViewController") as! FAQHistoryViewController
    return controller
}

func getSpendTrendsViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "SpendTrendsViewController") as! SpendTrendsViewController
    return controller
}
func getPayViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "PayViewController") as! PayViewController
    return controller
}
func getToAccountViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "ToAccountViewController") as! ToAccountViewController
    return controller
}
func getRequestViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "RequestViewController") as! RequestViewController
    return controller
}


func getUPIPayViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "UPIPayViewController") as! UPIPayViewController
    return controller
}

func getFavouriteViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "FavouriteViewController") as! FavouriteViewController
    return controller
}
func getFavouriteTableViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "FavouriteTableViewController") as! FavouriteTableViewController
    return controller
}
func getPayeeInfoViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "PayeeInfoViewController") as! PayeeInfoViewController
    return controller
}
func getRequestHistoryViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "RequestHistoryViewController") as! RequestHistoryViewController
    return controller
}


func getConfirmationViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "ConfirmationViewController") as! ConfirmationViewController
    return controller
}
func getFindIFSCViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "FindIFSCViewController") as! FindIFSCViewController
    return controller
}
func getSelectBranchViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "SelectBranchViewController") as! SelectBranchViewController
    return controller
}
func getIFSCConfirmViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "IFSCConfirmViewController") as! IFSCConfirmViewController
    return controller
}






func getPayToContactsViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "PayToContactsViewController") as! PayToContactsViewController
    return controller
}

//Merchant
func getGeneralDetailsViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "GeneralDetailsViewController") as! GeneralDetailsViewController
    return controller
}

func getMerchantGeneralDetailsViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "MerchantGeneralDetailsViewController") as! MerchantGeneralDetailsViewController
    return controller
}
func getSummaryViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "SummaryViewController") as! SummaryViewController
    return controller
}
func getMerchantHomeViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "MerchantHomeViewController") as! MerchantHomeViewController
    return controller
}

func getBusinessTrendsViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "BusinessTrendsViewController") as! BusinessTrendsViewController
    return controller
}
func getSettlementsViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "SettlementsViewController") as! SettlementsViewController
    return controller
}
func getMerchantRequestViewController() -> UIViewController {
    let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "MerchantRequestViewController") as! MerchantRequestViewController
    return controller
}

*/
