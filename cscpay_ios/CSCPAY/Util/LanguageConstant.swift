//
//  LanguageConstant.swift
//  CSCPAY
//
//  Created by Gowtham on 27/05/21.
//

import Foundation

class LanguageConstants {
    //OTP Screen - 1
    static let lbl_otp_title    = "lbl_otp_title" //: "Mobile Verification"
    static let lbl_otp_description   = "lbl_otp_description" //: "Please enter your mobile number linked to your bank account"
    static let txt_mobile_placeholder   = "txt_mobile_placeholder" //: "Mobile number"
    static let btn_send_otp = "btn_send_otp" //: "SEND OTP"
    //Alert
    static let alert_enter_mobile   = "alert_enter_mobile" //: "Please enter mobile number"
    static let alert_enter_valid_mobile  = "alert_enter_valid_mobile" //: "Please enter valid mobile number"
    static let alert_enable_internet    = "alert_enable_internet" //: "Please enable internet connection"
    static let general_error_generic =     "general_error_generic" //: "Something went wrong, please try again later."
    //OTP Verification Screen - 2
    static let lbl_verification_title   = "lbl_verification_title" //: "Verification Code"
    static let lbl_otp_sent = "lbl_otp_sent" //: "Please enter the verification code sent to +91 ********"
    static let btn_verify   = "btn_verify" //: "VERIFY"
    static let lbl_resend_otp   = "lbl_resend_otp" //: "Don't receive the OTP? RESEND"
    static let btn_resend_otp   = "btn_resend_otp" //: "RESEND"
    static let btn_resent_count = "btn_resent_count" //: ""Resend available in"
    //Alert
    static let alert_enter_otp  = "alert_enter_otp" //: "Please enter OTP"
    static let alert_enter_valid_otp  = "alert_enter_valid_otp" //: "Please enter valid OTP"
    //Incoming Requests
    static let lbl_request_title   = "lbl_request_title" //: "Requests"
    static let lbl_requestedBy  = "lbl_requestedBy" //: "Requested by"
    static let lbl_expiry   = "lbl_expiry"  //: "Expires in"
    static let lbl_expiry_type   = "lbl_expiry_type" //: "mins"
    static let lbl_currency = "lbl_currency" //: "₹"
    //Decline Alert
    static let lbl_decline_alert_title  = "lbl_decline_alert_title" //: "Are you sure you want to decline the request made by"
    static let btn_decline_alert_ok = "btn_decline_alert_ok" //: "OK"
    //Transaction History
    static let lbl_history_title   = "lbl_history_title" //: "Transaction History"
    static let lbl_history_subtitle   = "lbl_history_subtitle" //: "Payments History"
    static let lbl_history_description   = "lbl_history_description" //: "See your recent payments"
    static let btn_paid = "btn_paid" //: "Paid"
    static let btn_received = "btn_received" //: "Received"
    //Tableview cell
    static let lbl_paid_to  = "lbl_paid_to"
    static let lbl_received_from    = "lbl_received_from"
    //Notifications
    static let lbl_notification_title   = "lbl_notification_title" //: "Notifications"
    static let btn_mark_as_read   = "btn_mark_as_read" //: "Mark as Read"
    //Scan and PAy
    static let lbl_scan_pay_title   = "lbl_scan_pay_title" //: "Scan & Pay"
    static let lbl_enter_mobile_placeholder   = "lbl_enter_mobile_placeholder" //: "Enter Mobile number or UPI ID"
    static let lbl_scan_not_supported_title = "lbl_scan_not_supported_title" //: "Scanning not supported"
    static let lbl_scan_not_supported_message = "lbl_scan_not_supported_message"//: "Your device does not support scanning a code from an item. Please use a device with a camera."
    //QR Generator
    static let lbl_Qr_title = "lbl_Qr_title" //: "My QR"
    static let lbl_Qr_Subtitle  = "lbl_Qr_Subtitle" //: "Place the QR Code inside the area"
    static let lbl_Qr_Description   = "lbl_Qr_Description" //: "Scanning will start automatically"
    //Pay Confirmation
    static let txt_amount_placeholder    = "txt_amount_placeholder" //: "0"
    static let txt_note_placeholder = "txt_note_placeholder" //: "Make a note"
    static let lbl_pay_from = "lbl_pay_from" //: "Pay from"
    static let btn_proceed_to_pay  = "btn_proceed_to_pay" //: "PROCEED TO PAY"
    //Alert
    static let alert_enter_amount  = "alert_enter_amount" //: "Please enter amount"
    static let alert_enter_valid_amount  = "alert_enter_valid_amount" //: "Please enter valid amount"
    //Transaction Details
    static let lbl_transaction_detail_title = "lbl_transaction_detail_title" //: "Transaction Details"
    static let lbl_transaction_ref_id = "lbl_transaction_ref_id" //: "CSC PAY Ref ID :"
    static let lbl_transaction_id = "lbl_transaction_id" //: "Order ID :"
    static let btn_repeat_transacation   = "btn_repeat_transacation" //: "REPEAT TRANSACTION"
    static let btn_back_to_home   = "btn_back_to_home" //: "Home"
   
    //Profile Settings
    static let lbl_profile_title = "lbl_profile_title" //: "Settings"
    static let lbl_default_home = "lbl_default_home" //: "Default Home"
    static let lbl_bank_account_menu = "lbl_bank_account_menu" //: "Bank Accounts"
    static let lbl_bank_account_submenu = "lbl_bank_account_submenu" //: "Bank accounts & cards"
    static let lbl_upi_menu = "lbl_upi_menu" //: "UPI Address"
    static let lbl_upi_submenu  = "lbl_upi_submenu" //: "Check UPI address"
    static let lbl_trends_menu  = "lbl_trends_menu" //: "Spend Trends"
    static let lbl_trends_submenu = "lbl_trends_submenu" //: "Study your expenses"
    static let lbl_support_menu = "lbl_support_menu" //: "Support"
    static let lbl_support_submenu = "lbl_support_submenu" //: "Seek support"
    static let lbl_notifications_menu   = "lbl_notifications_menu" //: "Notifications"
    static let lbl_notifications_submenu = "lbl_notifications_submenu" //: "Turn notifications on/off"
    static let lbl_logout_menu  = "lbl_logout_menu" //: "Logout"
    static let lbl_logout_submenu = "lbl_logout_submenu" //: "Logout from CSC Pay"
    static let lbl_close_account_menu   = "lbl_close_account_menu" //: "Close Account"
    static let lbl_close_account_submenu = "lbl_close_account_submenu" //: "Deactivate from CSC Pay"
    static let lbl_change_pin_menu = "lbl_change_pin_menu"
    static let lbl_change_pin_submenu = "lbl_change_pin_submenu"
    //Payment Method
    static let lbl_payment_method_title = "lbl_payment_method_title" //: "Payment Methods"
    static let lbl_primary_account  = "lbl_primary_account" //: "Primary Account"
    static let btn_add_bank_account = "btn_add_bank_account" //: "ADD BANK ACCOUNT"
    //Bank Detials
    static let lbl_payment_method_subtitle = "lbl_payment_method_subtitle" //: "Payment Methods"
    static let lbl_account_information = "lbl_account_information" //: "Account Information"
    static let lbl_upi_id = "lbl_upi_id" //: "UPI ID"
    static let lbl_view_balance = "lbl_view_balance" //: "View Balance"
    static let lbl_forget_upi_pin = "lbl_forget_upi_pin" //: "Forget UPI PIN?"
    //Balance
    static let lbl_balance_title = "lbl_balance_title" //: "Balance"
    //Manage UPI Address
    static let lbl_manage_upi_title = "lbl_manage_upi_title" //: "UPI Address"
    static let lbl_primary_id = "lbl_primary_id" //: "Primary UPI ID"
    static let lbl_set_primary = "lbl_set_primary" //: "Set as Primary Account"
    static let lbl_add_new_upi = "lbl_add_new_upi" //: "ADD NEW UPI ID"
    //Add new UPI Address
    static let lbl_add_new_upi_title = "lbl_add_new_upi_title" //: "Add New UPI Address"
    static let txt_upi_placeholder = "txt_upi_placeholder" //: "Enter UPI address"
    static let btn_upi_verify = "btn_upi_verify" //: "Verify"
    static let btn_upi_proceed = "btn_upi_proceed" //: "PROCEED"
    static let alert_enter_upi = "alert_enter_upi" //: "Please enter UPI address"
    static let alert_enter_valid_upi = "alert_enter_valid_upi" //: "Please enter valid UPI address."
    //User General Details
    static let lbl_user_details_title = "lbl_user_details_title" //: "General Details"
    static let lbl_user_detials_subtitle = "lbl_user_detials_subtitle" //: "Fill your details to help us know you better"
    static let txt_name_placeholder = "txt_name_placeholder" //: "Name"
    static let lbl_user_details_gender = "lbl_user_details_gender" //: "Gender"
    static let lbl_male = "lbl_male" //: "Male"
    static let lbl_female   = "lbl_female" //: "Female"
    static let lbl_other_gender = "lbl_other_gender" //: "Others"
    static let lbl_user_details_dob = "lbl_user_details_dob" //: "Date of Birth"
    static let txt_dob_placehloder = "txt_dob_placehloder" //: "DD/MM/YYYY"
    static let txt_emil_placehloder = "txt_emil_placehloder" //: "Email"
    static let lbl_otp_sent_to_email =  "lbl_otp_sent_to_email" //: "The OTP has been sent to your email"
    static let txt_enter_otp_placeholder = "txt_enter_otp_placeholder" //: "Enter OTP"
    static let btn_user_proceed = "btn_user_proceed" //: "PROCEED"
    //Alert
    static let alert_enter_name = "alert_enter_name" //: "Enter your name"
    static let alert_select_gender = "alert_select_gender" //: "Select your gender"
    static let alert_enter_dob = "alert_enter_dob" //: "Enter your DOB"
    static let alert_enter_email = "alert_enter_email" //: "Enter your email"
    static let alert_enter_valid_email = "alert_enter_valid_email" //: "Enter valid email"
    static let alert_enter_user_otp = "alert_enter_user_otp" //: "Enter OTP that you've received"
    
    //Add New UPI Verification
    static let lbl_add_new_verification_title = "lbl_add_new_verification_title"  //: "Verification Code"
    //App Permission
    static let lbl_app_permission_title = "lbl_app_permission_title" //: "App Permission"
    static let lbl_app_permission_subtitle = "lbl_app_permission_subtitle" //: "Permissions are necessary for all the features and functions to work properly."
    static let lbl_location = "lbl_location" //: "Location"
    static let lbl_location_info = "lbl_location_info" //: "To be enabled for making UPI transaction"
    static let lbl_contacts = "lbl_contacts" //: "Contacts"
    static let lbl_contacts_info = "lbl_contacts_info" //: "To be enabled to browse through your connections"
    static let lbl_camera = "lbl_camera" //: "Camera"
    static let lbl_camera_info = "lbl_camera_info" //: "To be enabled to scan or capture QR code"
    static let lbl_gallery = "lbl_gallery" //: "Gallery"
    static let lbl_gallery_info = "lbl_gallery_info" //: "To be enabled to load an already captured QR"
    static let btn_allow_to_proceed = "btn_allow_to_proceed" //: "PROCEED"
    //Select the Bank Account
    static let lbl_select_account_title = "lbl_select_account_title" //: "Select the bank account"
    static let lbl_select_account_subtitle = "lbl_select_account_subtitle" //: "Get your debit or ATM card ready to enter some deatils. Learn more"
    static let btn_select_account_start = "btn_select_account_start" //: "START"
    //Register Merchant
    
    static let alert_enter_business_name        = "alert_enter_business_name" //: "Please enter registered business name."
    static let alert_enter_shop_name        = "alert_enter_shop_name" //:"Please enter shop name."
    static let alert_choose_business_type        = "alert_choose_business_type" //:"Please choose Type of Business."
    static let alert_enter_merchant_category        = "alert_enter_merchant_category" //: "Please enter merchant category."
    static let alert_register_ID = "alert_enter_registered_id"
    
    static let alert_enter_owner_name        = "alert_enter_owner_name" //: "Please enter business owner name."
    static let alert_capture_image        = "alert_capture_image" //: "Capture photo"
    
    static let alert_pan_details  = "alert_pan_details"
   
    static let alert_enter_shop_number        = "alert_enter_shop_number" //:"Please enter shop number."
    static let alert_enter_area        = "alert_enter_area" //: "Please enter shop address."
    static let alert_choose_city        = "alert_choose_city" //: "Please choose your city."
    static let alert_choose_state       = "alert_choose_state" //: "Please choose your state."
    static let alert_enter_pincode        = "alert_enter_pincode" //: "Please enter pincode."
    //Change UPI through Card
    static let alert_enter_valid_expiry_month = "alert_enter_valid_expiry_month"
    static let alert_enter_valid_expiry_year  = "alert_enter_valid_expiry_year"
    static let alert_enter_valid_card_number =  "alert_enter_valid_card_number"
    
    //Create UPI ID
    
    static let lbl_primary_upi = "lbl_primary_upi"
    
    
    
    
 }


//lblSubscriptionNow.text = Language.getText(with: LanguageConstants.sign_up_5_title)
class Language {
    
    private static var dict: [String: Any]?
    
    private init() { }
    
     static func loadLanguage()  {
        guard let text = UserDefaults.standard.string(forKey: "language") else {
            return
        }
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]
                if let json = json {
                    Language.dict = json
                }
            } catch {
                print("Something went wrong")
            }
        }
    }
    /**
     Provides text corresponding to provided key in current local
     */
    static func getText(with key: String) -> String? {
        if (Language.dict == nil) {
            Language.loadLanguage()
        }
        if let dict = Language.dict {
            if let value = dict[key] {
                return value as? String
            }
        }
        return nil
    }
    
    static func getGenricError() -> String? {
        return Language.getText(with:LanguageConstants.general_error_generic)
    }

}

/*
 {"head":{"appName":"CSC Pay","deviceId":"ios11","refId":"999989487011052106092101","ts":"2021-05-24T04:44:52","resAction":"label","respUID":"ada21f97-bc1c-11eb-8f4d-482ae31bab14"},"body":{"dat":[{"lblTitle":"lblgeneralDetailsTitleLabel","lblValue":"General Details"},{"lblTitle":"lblregisteredBusinessNamePlaceholder","lblValue":"Registered Business Name"},{"lblTitle":"lblregisteredBusinessNameExample","lblValue":"E.g. A-B-C Pvt. Ltd"},{"lblTitle":"lblshopNamePlaceholder","lblValue":"Shop Name"},{"lblTitle":"lblshopNameExample","lblValue":"Name your customers will see"},{"lblTitle":"lbltypeOfBusinessPlaceholder","lblValue":"Type of Business"},{"lblTitle":"lblmerchantCategoryPlaceholder","lblValue":"Merchant Category"},{"lblTitle":"lblbusinessOwnerNamePlaceholder","lblValue":"Business Owner Name"},{"lblTitle":"lblbusinessOwnerNameExample","lblValue":"Name of Owner, Director or Partner"},{"lblTitle":"lblcaptureShopPhoto","lblValue":"Capture Shop Photo"},{"lblTitle":"lblnextButtonText","lblValue":"NEXT"},{"lblTitle":"lbladdressDetailsTitleLabel","lblValue":"Address Details"},{"lblTitle":"lblshopNumberAndAddressPlaceholder","lblValue":"Shop Number and Street Name"},{"lblTitle":"lblareaPlaceholder","lblValue":"Area\/Locality\/Village"},{"lblTitle":"lblcityPlaceholer","lblValue":"City"},{"lblTitle":"lblstatePlaceholder","lblValue":"State"},{"lblTitle":"lblpincodePlaceholder","lblValue":"PIN Code"},{"lblTitle":"lblpanAndBankDetailsTitleLabel","lblValue":"PAN & Bank Details"},{"lblTitle":"lblpanCardNumberPlaceholder","lblValue":"PAN Card Number"},{"lblTitle":"lbluploadPancardPhotoPlaceholder","lblValue":"Upload Pan Card Photo"},{"lblTitle":"lblgstinPlaceholder","lblValue":"GSTIN (optional)"},{"lblTitle":"lblbankAccountNumberPlaceholder","lblValue":"Bank Account Number"},{"lblTitle":"lblconfirmBankAccountNumberPlaceholder","lblValue":"Confirm Bank Account Number"},{"lblTitle":"lblifscCodePlaceholder","lblValue":"IFSC Code"},{"lblTitle":"lblfindIFSCButtonText","lblValue":"Find IFSC"},{"lblTitle":"lblsummaryTitleLabel","lblValue":"Summary"},{"lblTitle":"lblsummarySubtitleLabel","lblValue":"Please verify the information entered by you"},{"lblTitle":"lblverifyButtonText","lblValue":"VERIFY"},{"lblTitle":"lblregistrationSuccessMessage","lblValue":"Registration Successful!"},{"lblTitle":"lblwelcomeMessageForMerchant","lblValue":"Welcome to the application, start the experience and wish you the best moments"},{"lblTitle":"lbldownloadQrButtonText","lblValue":"Download QR"},{"lblTitle":"lblshareQrButtonText","lblValue":"Share QR"},{"lblTitle":"lblrequestQrStickerButtonText","lblValue":"Request QR Sticker"},{"lblTitle":"lblproceedToHomepageButtonText","lblValue":"proceed to homepage"},{"lblTitle":"lblmtWelcomeLabel","lblValue":"Namaste, "},{"lblTitle":"lblrequestMoneyFromHomeLabel","lblValue":"Request money from your customers via UPI and Mobile Number"},{"lblTitle":"lblmtShowTransactionHistoryLabel","lblValue":"Show Transaction History"},{"lblTitle":"lblmtSeeYourTransactionLabel","lblValue":"See your transaction and settlement history"},{"lblTitle":"lblmtCheckBusinessTrendsLabel","lblValue":"Check Business Trends"},{"lblTitle":"lblmtSettlementLabel","lblValue":"All the payments will be settled within 24 hours. Check settled payments"},{"lblTitle":"lblgraphCategories","lblValue":"Transactions,Settlements"},{"lblTitle":"lblmtBusinessTrendsTitleLabel","lblValue":"Business Trends"},{"lblTitle":"lblmtBusinessTrendsCategories","lblValue":"Weekly,Monthly"},{"lblTitle":"lbldateComponentsForBusinessTrends","lblValue":"From,To"},{"lblTitle":"lbllast7DaysTransactionVolumeLabel","lblValue":"Last 7 day transaction volumes"},{"lblTitle":"lbllast7DaysTransactionVolumeXaxis","lblValue":"S,M,T,W,T,F,S"},{"lblTitle":"lbllast7DaysTransactionLabel","lblValue":"Last 7 day transaction"},{"lblTitle":"lbllast7DaysTransactionXaxis","lblValue":"S,M,T,W,T,F,S"},{"lblTitle":"lblmerchantDetailsCategories","lblValue":"General Details,Address Details,PAN & Bank Details"},{"lblTitle":"lblsettlementsTitleLabel","lblValue":"Settlements"},{"lblTitle":"lblmtSummaryLabel","lblValue":"Summary"},{"lblTitle":"lbltodaySettlementsLael","lblValue":"DailySettlement "},{"lblTitle":"lbltotalSettlementsLabel","lblValue":"Total Settlements"},{"lblTitle":"lblsettlementsLabel","lblValue":"Settlements"},{"lblTitle":"lblseeYourRecentSettlementLabel","lblValue":"See your recent settlements"},{"lblTitle":"lblmtSelectDateButtonText","lblValue":"Select Date"}],"version":"3","meta":"merchant"}}
 */
