//
//  MonthAndYearPickerView.swift
//  coffeeclub-ios
//
//  Created by Gowtham on 21/05/21.
//  Copyright © 2021 Coffee Club. All rights reserved.
//

import Foundation
import UIKit

class MonthYearPickerView: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var hour = [String]()
    var minute = [String]()
    var second = [String]()
    
    var onDateSelected: ((_ hour: String, _ minute: String, _ second: String) -> Void)?
    
    var onHourMinuteSelected : ()?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonSetup()
    }
    
    func commonSetup() {
        self.hour = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"]
        self.minute = ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"]
        self.second = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"]
        
        delegate = self
        dataSource = self
    }
    
    
    func hourMinuteSetup(){
        
    }
    
    // Mark: UIPicker Delegate / Data Source
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return "\(hour[row])"
        case 1:
            return "\(minute[row])"
        case 2:
            return "\(second[row])"
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return hour.count
        case 1:
            return minute.count
        case 2:
            return second.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let hour = self.hour[selectedRow(inComponent: 0)]
        let minute = self.minute[selectedRow(inComponent: 1)]
        let second = self.second[selectedRow(inComponent: 2)]
        
        
        if let block = onDateSelected {
            block(hour, minute, second)
        }
        
    }
    
}




class MinuteHourPicker: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {

    var selctedType = ["Hours" , "Minute"]
    var onDateSelected: ((_ minHr: String) -> Void)?
    var hour = [String]()
    var minute = [String]()

   
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonSetup()
    }
    
    func commonSetup() {
        self.hour = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"]
        self.minute = ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"]
        delegate = self
        dataSource = self
    }
    
    
    
    
    // Mark: UIPicker Delegate / Data Source
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return selctedType[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return selctedType.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let select = selctedType[row]
        if let block = onDateSelected {
            block(select)
        }
        
    }
}



enum PickerType {
    case State , District , BusinessN , BUsinessT
}

class StateCity: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {

    var selctedType : State?
    var onStateSelected: ((_ selectedState: String , _ district: [String]) -> Void)?
    var onDistrictSelected: ((_ selectDistrict: String) -> Void)?
    var state : [StateResponse]?
    var allState = [String]()
    var allDistrict = [String]()
    var pickerType : PickerType = .State
    var selectedState : String?
    var businessNature : [BusinessNature]?

   
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonSetup()
    }
    
    func commonSetup() {
        self.selctedType = APIManager.loadAllJson(State.self , filename: "StateDistrict")
        let states = selctedType?.map({$0.state})
//        allState.removeAll()
        
            for i in 0..<states!.count{
                if !allState.contains((selctedType![i].state)){
                    allState.append(selctedType![i].state)
                }
            }
        
        
//        print(Set(states!))
        delegate = self
        dataSource = self
    }

    
    
    // Mark: UIPicker Delegate / Data Source
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return allState[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return allState.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let select = allState[row]
        if let block = onStateSelected {
            selectedState = select
            allDistrict.removeAll()
            let states = selctedType?.map({$0.state})
            for i in 0..<states!.count{
                let dis = selctedType?.filter({$0.state == selectedState})
                if i < dis!.count{
                    allDistrict.append(dis![i].district)
                }
            }
            block(select , allDistrict)
        }
    }
}











class CityPicker: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {

    var selctedType : State?
    var onCitySelect: ((_ citySelect: String) -> Void)?
    var state : [StateResponse]?
    var allDistrict = [String]()
    var selectedState : String?

    

   
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonSetup()
    }
    
    func commonSetup() {
        self.selctedType = APIManager.loadAllJson(State.self , filename: "StateDistrict")
        let states = selctedType?.map({$0.state})
        allDistrict.removeAll()
        for i in 0..<states!.count{
            let dis = selctedType?.filter({$0.state == selectedState})
            if i < dis!.count{
                print(dis![i].district)
                allDistrict.append(dis![i].district)
            }
        }
        delegate = self
        dataSource = self
    }

    
    
    // Mark: UIPicker Delegate / Data Source
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return allDistrict[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return allDistrict.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let select = allDistrict[row]
        if let block = onCitySelect {
            block(select)
        }
    }
}












extension Sequence where Element: Hashable {
    func uniqued() -> [Element] {
        var set = Set<Element>()
        return filter { set.insert($0).inserted }
    }
}
