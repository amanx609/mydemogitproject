//
//  DropDown.swift
//  VLE Survey
//
//  Created by Gowtham on 12/10/20.
//  Copyright © 2020 CSC eGovernance. All rights reserved.
//

import Foundation
import UIKit

class DropDownTextField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 26)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}

public protocol DropDownDelegate: class {
    func dropdownDidChangeValue(textfield: UITextField, title: String, index: Int)
}

var AssociatedObjectHandle: UInt8 = 0
private var maxLengths = [UITextField: Int]()
var maxLength = 100

extension UITextField: UIPickerViewDataSource, UIPickerViewDelegate {
    // MARK:- associat key
    private struct xy_associatedKeys {
        static var originalDataBlockKey = "xy_originalDataBlockKey"
        static var newDataBlockKey = "xy_newDataBlockKey"
    }
    
    
    public static var _pickerArray = [String: [String]]()
    public static var _pickerView = [String: UIPickerView]()
    
    // Change editing selector for setting max length
    @objc func limitLength(textField: UITextField) {
        guard let prospectiveText = textField.text,
            prospectiveText.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        text = prospectiveText.substring(to: maxCharIndex)
        selectedTextRange = selection
    }
    
    // Extend Text field to set left padding
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    // Extend Text field to set right padding
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    
    var dropDownDelegate: DropDownDelegate? {
        get {
            return objc_getAssociatedObject(self, &AssociatedObjectHandle) as? DropDownDelegate
        }
        set {
            objc_setAssociatedObject(self, &AssociatedObjectHandle, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var pickerArray:[String] {
        get {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            return UITextField._pickerArray[tmpAddress] ?? []
            
        }
        set(newValue) {
            
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            UITextField._pickerArray[tmpAddress] = newValue
        }
    }
    
    var pickerView: UIPickerView {
        get {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            return UITextField._pickerView[tmpAddress] ?? UIPickerView()
            
        }
        set(newValue) {
            
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            UITextField._pickerView[tmpAddress] = newValue
        }
    }
    
    
    func addDropDownasInput(withArray: [String]) {
        self.pickerArray = withArray
        self.pickerView =  UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = 3814
        self.inputView = pickerView
        let toolBar = UIToolbar.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: UIScreen.main.bounds.size.width, height: 40)))
        
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 44, height: 44)
        menuBtn.bounds = CGRect( x: 0, y: 0, width: 40, height: 40);
        menuBtn.imageView?.contentMode = .scaleAspectFit
        menuBtn.setImage(UIImage.init(systemName: "arrowtriangle.down.circle")?.withTintColor(.blue), for: .normal)
        // menuBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        //
        menuBtn.addTarget(self, action: #selector(UITextField.donePressed), for: .touchUpInside)
        let doneButton = UIBarButtonItem(customView: menuBtn)
        //doneButton.customView?.trailingAnchor leftAnchor.constraint(equalToConstant: 24).isActive = true
        
        
        //let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(UITextField.donePressed))
        //let doneButton = UIBarButtonItem(image: UIImage(named: "downArrow-1", in: DBUPI_Intractor.sharedInteractor.bundle, compatibleWith: nil), style: .done, target: self, action: #selector(UITextField.donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: true)
        toolBar.tintColor = hexStringToUIColor(hex: "1C7FE5")
        toolBar.backgroundColor = hexStringToUIColor(hex: "F0F0F2")
        //toolBar.barStyle = UIBarStyle. blackTranslucent
        self.inputAccessoryView = toolBar
    }
    
    @objc func donePressed() {
        resignFirstResponder()
        /* let index = self.pickerView.selectedRow(inComponent: 0)
         if( pickerArray.count > 0){
         self.text = pickerArray[index]
         self.dropDownDelegate?.dropdownDidChangeValue(textfield: self, title: pickerArray[index], index: index)
         }else{
         self.dropDownDelegate?.dropdownDidChangeValue(textfield: self, title: "", index: -1)
         }*/
    }
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArray[row]
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if( pickerArray.count > 0){
            self.text = pickerArray[row]
            self.dropDownDelegate?.dropdownDidChangeValue(textfield: self, title: pickerArray[row], index: row)
        }else{
            self.dropDownDelegate?.dropdownDidChangeValue(textfield: self, title: "", index: -1)
            self.text = ""
        }
        
    }
    
}
