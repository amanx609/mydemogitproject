//
//  Userdefault.swift
//  VLE Survey
//
//  Created by Gowtham on 01/11/20.
//  Copyright © 2020 CSC eGovernance. All rights reserved.
//

import Foundation
import UIKit

enum UDTypes {
    case string
    case bool
    case int
    case stringArray
    case object
    case anyObject
}

class UD : NSObject {
    
    static let SharedManager = UD()
    let ud = UserDefaults.standard
    private override init () {
        super.init()
    }
    func saveToUserDefaults(key: String, value : Any?) {
        guard let val = value else {
            return
        }
        ud.set(val, forKey: key)
    }
    
    func saveImageInUserDefault(img:UIImage?, key:String) {
        guard let image = img else {
            return
        }
        ud.set(image.pngData(), forKey: key)
        ud.synchronize()
    }

    func getImageFromUserDefault(key:String) -> UIImage? {
        let imageData = UserDefaults.standard.object(forKey: key) as? Data
        var image: UIImage? = nil
        if let imageData = imageData {
            image = UIImage(data: imageData)
        }
        return image
    }
    func removeValue(key: String) {
        ud.removeObject(forKey: key)
        ud.synchronize()
    }
    func getValues(key : String, type : UDTypes ) -> Any? {
        switch type {
        case .string:
            return ud.string(forKey: key)
        case .bool:
            return ud.bool(forKey: key)
        case .int :
            return ud.integer(forKey: key)
        case .stringArray :
            return ud.stringArray(forKey: key)
        case .object :
            return ud.object(forKey: key)
        case .anyObject:
            return ud.object(forKey: key)
        }
        
    }
    func getBoolean(key : String) -> Bool {
        return ud.bool(forKey: key)
    }

    
    
    
}
