//
//  Reference.swift
//  CSCPAY
//
//  Created by Gowtham on 24/05/21.
//

import Foundation

/*  OTP Sent
 {
     body =     {
         msgRes = "HS24391261030144039563951 Message submitted successfully";
         otpHash = 2b79d4ae9a86be8cb47f202b15918802e11f1b5b2310daac18aec58eaf034597;
         resCode = 000;
         resHash = "uyp3A8BVk0MbrVO4+NyUS5ee95HNl3zGty7pBKk+ref8pxfVtgrS67tOqFyMq7PdV9tLREOXM0hOk2CAGCrbiEuJ++BAIacNpaGfnClsqq6BR0PalSr1QmhHm96l/azjaQ7raFAhRHQGeVV2ZCqS9Jg6L8qXdfCn2uOqf8Y0LXEE0Dla6oOUtX/zFW0iufjeKQw9ZsWy58t1YjXPfWQ2Aw==";
         resMsg = "OTP sent to mobile number 74-xxx-xxx-73";
         respCode = "09daee1f-d983-11eb-8b35-1e00be0004e4";
         verifyCall = N;
     };
     cred =     {
         aesKey = 9e96389e1ef6351f2186bab8b3588367da8bb12343f4aa60;
         deviceId = "A5C8D1D2-9E2F-4A7A-B1D0-F46637DC4CC2";
         hashToken = "YRrnuyEaAoOB2RN5oG4XujRKyL0hpxI1FM5nTw55G+bv3FSuZmBlmPT+au6WkS6lsKI/8EwA/G/L7dDWFkptH8aB4mWAvSGXpwLadWDYrDCb8ED7Sud6NYNbZ6OMp6uWm8mEe4IZy8eZJ/j0NQ+cMLh0Gxl4R5SBHBzA/PiMWVMD/ovDj1v8ArK3Zqmh5G0USr/9+09EL4avAY6B0Bg2nDgUaRzUYcVQjFqrh3LJqG16Z7dYiYq5G7m3B1i23QAn9mZkOUNLE7tSvBpVT+Ndl0F/I5zuYM7qJ2vOoe4Z61AWI8c=";
         hmac = 37ae5441306060dc37374232baab011af46677abe1be576de36de88701e8cb12;
     };
     head =     {
         appName = "CSC Pay";
         appVer = "1.0";
         bluetoothMac = na;
         clientIp = "55.226.554.10";
         deviceId = "A5C8D1D2-9E2F-4A7A-B1D0-F46637DC4CC2";
         deviceType = iOS;
         geoLat = NA;
         geoLong = NA;
         location = na;
         osVer = "14.4";
         refId = 741861297320210630144040;
         reqAction = generate;
         simId = NA;
         ts = "2021-06-30T14:40:39";
         wifiMac = na;
     };
 }
 
 
 OTP Validate
 {
     body =     {
         resCode = 000;
         resStatus = Success;
         respCode = "099476ac-d984-11eb-8b35-1e00be0004e4";
     };
     cred =     {
         aesKey = fc585a34341be985dae144e3559cd08dc9d9955d7d9a7554;
         deviceId = "A5C8D1D2-9E2F-4A7A-B1D0-F46637DC4CC2";
         hashToken = "93k82WpVWK54gbCbwWVORPQs2k7khKU0u14uDBY1oxrO5xrsjdqoV67CNTcj8KGD86aiV0KPtq0VVyGmYpBcv9+YcbZEC/4gPqF6wzyBy44CHvotM7LkcC6AxUNAh1ru0mLAKUzVlf10GaZoFRRVeorgo4xTgUyM0nZ6ApLFrrqx84E0SzpkF/Fgc81UKIA4YhkkuqXF2CHowLth9d++Qqh38hoZO2lU20curRhIJ9zDEjYpWKTDv6s+rJtJKgF1n54oSTZaOroncR8lq7EH++QzfB1vGUEcN4/rO0k8w/tR/b0=";
         hmac = 37d36b4f1f3942f33a1227398f98f6ced885341b01e2b3b8681dad87f7f7ed13;
     };
     head =     {
         appName = "CSC Pay";
         appVer = "1.0";
         bluetoothMac = na;
         clientIp = "55.226.554.10";
         deviceId = "A5C8D1D2-9E2F-4A7A-B1D0-F46637DC4CC2";
         deviceType = iOS;
         geoLat = NA;
         geoLong = NA;
         location = na;
         osVer = "14.4";
         refId = 741861297320210630144040;
         reqAction = validate;
         simId = NA;
         ts = "2021-06-30T14:47:48";
         wifiMac = na;
     };
 }
 
 
 // Config Label
 {
     body =     {
         androidGrace = "2021-05-21 16:01:48";
         androidVer = 1;
         apiEndpoint = 3;
         appLabel = 3;
         appSalt = abcd;
         bankMaster = 3;
         faqData = 3;
         iosGrace = "2021-05-21 16:02:01";
         iosVer = 1;
         lastUpdated = "2021-05-31 03:45:40";
         masterDelay = 7;
         masterNextDate = "2021-05-28 16:02:18";
         merchantBank = "<null>";
         merchantVersion = "<null>";
         otpReValidate = Y;
         otpValidity = 60;
         pspBank =         (
                         {
                 bankCode = INDB;
                 bankName = "IndusInd Bank Limited";
                 bankStatus = Active;
                 bankVA = "@cscindus";
             },
                         {
                 bankCode = ICIC;
                 bankName = "ICICI Bank Limited";
                 bankStatus = InActive;
                 bankVA = "@cscicici";
             }
         );
         pspVersion = 3;
     };
     head =     {
         appName = "CSC Pay";
         deviceId = "A5C8D1D2-9E2F-4A7A-B1D0-F46637DC4CC2";
         refId = 999989487020210630123452;
         resAction = config;
         respUID = "ef9c9972-d971-11eb-8b35-1e00be0004e4";
         ts = "2021-06-30T12:38:13";
     };
 }

 //URL
 {
     body =     {
         dat =         (
                         {
                 actionName = genOTP;
                 activeStatus = 2;
                 endUrl = "https://devupi.csccloud.in/gw1/start/otp";
             },
                         {
                 actionName = valOTP;
                 activeStatus = 2;
                 endUrl = "https://devupi.csccloud.in/gw1/start/otp";
             }
         );
         meta = na;
         version = 3;
     };
     head =     {
         appName = "CSC Pay";
         deviceId = "61A909E7-1701-4C8C-BD47-5003BEE9AE66";
         refId = 741861297320210525111111;
         resAction = url;
         respUID = "d33721c9-bd1b-11eb-8b35-1e00be0004e4";
         ts = "2021-05-25T11:11:17";
     };
 }
 
 //Bank
 {
     body =     {
         dat =         (
                         {
                 activeStatus = 2;
                 bankCode = IBLB;
                 bankName = "IndusInd Bank";
                 sno = 1;
             },
                         {
                 activeStatus = 2;
                 bankCode = ICIC;
                 bankName = "ICICI Bank";
                 sno = 2;
             }
         );
         meta = na;
         version = 3;
     };
     head =     {
         appName = "CSC Pay";
         deviceId = "61A909E7-1701-4C8C-BD47-5003BEE9AE66";
         refId = 741861297320210525111217;
         resAction = bank;
         respUID = "fb66773d-bd1b-11eb-8b35-1e00be0004e4";
         ts = "2021-05-25T11:12:24";
     };
 }
 // FAQ - General
 {
     body =     {
         dat =         (
                         {
                 answer = "signup online";
                 category = General;
                 faqRating = "<null>";
                 lastUpdated = "2021-05-24 04:20:32";
                 question = "How can i Register";
             },
                         {
                 answer = "yes you can try if that is available";
                 category = General;
                 faqRating = "<null>";
                 lastUpdated = "2021-05-24 04:20:37";
                 question = "can i get a choice to set my ID";
             }
         );
         meta = General;
         version = 3;
     };
     head =     {
         appName = "CSC Pay";
         deviceId = "61A909E7-1701-4C8C-BD47-5003BEE9AE66";
         refId = 741861297320210525111414;
         resAction = faq;
         respUID = "46969072-bd1c-11eb-8b35-1e00be0004e4";
         ts = "2021-05-25T11:14:30";
     };
 }
 
 // PSPBank
 
 {
     body =     {
         dat =         (
                         {
                 activeStatus = 2;
                 bankCode = INDB;
                 bankName = "IndusInd Bank";
                 bankVPA = "@cscindus";
                 commonData =                 (
                     androidId,
                     appName,
                     appVersionCode,
                     appVersionName,
                     wifiMac,
                     bluetoothMac,
                     capability,
                     deviceId,
                     deviceType,
                     geoCode,
                     location,
                     ip,
                     mobileNo,
                     relayButton,
                     simId,
                     os,
                     regId,
                     selectedSimSlot,
                     fcmToken
                 );
                 merchantModule =                 (
                     VA,
                     MA
                 );
                 npciToken = Y;
                 pspModule =                 (
                     VA,
                     AC,
                     MI
                 );
                 sno = 1;
             },
                         {
                 activeStatus = 2;
                 bankCode = ICIC;
                 bankName = "ICICI Bank";
                 bankVPA = "@cscicici";
                 commonData =                 (
                     androidId,
                     appName,
                     appVersionCode,
                     appVersionName,
                     wifiMac,
                     bluetoothMac,
                     capability,
                     deviceId,
                     deviceType,
                     geoCode,
                     location,
                     ip,
                     mobileNo,
                     relayButton,
                     simId,
                     os,
                     regId,
                     selectedSimSlot,
                     fcmToken
                 );
                 merchantModule =                 (
                     VA,
                     MA
                 );
                 npciToken = Y;
                 pspModule =                 (
                     VA,
                     AC,
                     MI
                 );
                 sno = 2;
             }
         );
         meta = na;
         version = 3;
     };
     head =     {
         appName = "CSC Pay";
         deviceId = "A5C8D1D2-9E2F-4A7A-B1D0-F46637DC4CC2";
         refId = 741861297320210630201459;
         resAction = pspBank;
         respUID = "cb958672-d9b1-11eb-8b35-1e00be0004e4";
         ts = "2021-06-30T20:15:21";
     };
 }
 
 // PSP Data
let ip = getIPAddress() ?? ""
let params = [
    "androidId": getUUID() ?? "na",
    "appName": "in.cscpay.upi",
    "appVersionCode": "1.0",
    "appVersionName": "Pay UAT 1",
    "wifiMac": "na",
    "bluetoothMac": "na",
    "capability":"5200000200010004000639292929292",
    "deviceId": getUUID() ?? "na",
    "deviceType": "MOB",
    "geoCode": "28.4817,77.1873",
    "location": "Delhi",
    "ip": ip,
    "mobileNo": "7418612973",
    "relayButton": "NEZGNzAyNkVENzE4ODZDODg2QTlBNTBF",
    "simId": "4FF7026ED71886C886A9A50E",
    "os": "IOS 13.2.3",
    "regId": "NA",
    "selectedSimSlot": "0",
    "fcmToken": ""
]
 
 //Bank List info
 
 {
     bankMasterList =     (
                 {
             bankCode = SIBL;
             bankId = 0;
             bankName = "South Indian Bank";
             ifsc = SIBL;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = MUBL0000001;
             bankId = 0;
             bankName = "THE MUNICIPAL CO-OP BANK LTD";
             ifsc = MUBL0000001;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = UTIB;
             bankId = 0;
             bankName = "Axis bank_JusPay";
             ifsc = UTIB;
             iin = 246893;
             statusCode = 0;
         },
                 {
             bankCode = CORP;
             bankId = 0;
             bankName = "CORPORATION BANK";
             ifsc = CORP;
             iin = 607184;
             statusCode = 0;
         },
                 {
             bankCode = JIOB;
             bankId = 0;
             bankName = "JIO Payments Bank";
             ifsc = JIOB;
             iin = 123009;
             statusCode = 0;
         },
                 {
             bankCode = POOL;
             bankId = 0;
             bankName = "POOJITHA BANK";
             ifsc = POOL;
             iin = 123456;
             statusCode = 0;
         },
                 {
             bankCode = SYNB;
             bankId = 0;
             bankName = "Syndicate Bank";
             ifsc = SYNB;
             iin = 508508;
             statusCode = 0;
         },
                 {
             bankCode = VSBL;
             bankId = 0;
             bankName = "The Vishweshwar Sahakari Bank Ltd";
             ifsc = VSBL;
             iin = 607103;
             statusCode = 0;
         },
                 {
             bankCode = HCBL;
             bankId = 0;
             bankName = "The Hasti Co-operative Bank Ltd";
             ifsc = HCBL;
             iin = 607621;
             statusCode = 0;
         },
                 {
             bankCode = MCBL;
             bankId = 0;
             bankName = "The Mahanagar Co.Op. Bank Ltd";
             ifsc = MCBL;
             iin = 607320;
             statusCode = 0;
         },
                 {
             bankCode = SBIN0RRAPGB;
             bankId = 0;
             bankName = "Andhra Pradesh Grameena Vikas Bank";
             ifsc = SBIN0RRAPGB;
             iin = 607198;
             statusCode = 0;
         },
                 {
             bankCode = SBIN0RRCHGB;
             bankId = 0;
             bankName = "CHATISGARH R G BANK";
             ifsc = SBIN0RRCHGB;
             iin = 607214;
             statusCode = 0;
         },
                 {
             bankCode = KLGB;
             bankId = 0;
             bankName = "Kerala Gramin Bank";
             ifsc = KLGB;
             iin = 607476;
             statusCode = 0;
         },
                 {
             bankCode = PKGB;
             bankId = 0;
             bankName = "Pragathi Krishna Gramin Bank";
             ifsc = PKGB;
             iin = 607389;
             statusCode = 0;
         },
                 {
             bankCode = SBIN0RRPUGB;
             bankId = 0;
             bankName = "Purvanchal Bank";
             ifsc = SBIN0RRPUGB;
             iin = 607212;
             statusCode = 0;
         },
                 {
             bankCode = ICIC00HSELW;
             bankId = 0;
             bankName = "Hutatma Sahakari Bank Ltd";
             ifsc = ICIC00HSELW;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = INDB;
             bankId = 0;
             bankName = "IndusInd Bank";
             ifsc = INDB;
             iin = 612355;
             statusCode = 0;
         },
                 {
             bankCode = AABH;
             bankId = 0;
             bankName = MYSIMI;
             ifsc = AABH;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = YESB0KNB006;
             bankId = 0;
             bankName = "Shree Kadi Nagarik Sahakari Bank Ltd";
             ifsc = YESB0KNB006;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = SURY;
             bankId = 0;
             bankName = "suryodaya small finance bak";
             ifsc = SURY;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = GSCB0ADC001;
             bankId = 0;
             bankName = "The Ahmedabad District Co-operative Bank Ltd";
             ifsc = GSCB0ADC001;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = GSCB0BRD001;
             bankId = 0;
             bankName = "The Baroda Central Co-Operative Bank Ltd";
             ifsc = GSCB0BRD001;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = MUBL;
             bankId = 0;
             bankName = "THE MUNICIPAL CO-OP BANK LTD";
             ifsc = MUBL;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = NTBL0DEL080;
             bankId = 0;
             bankName = "The Nainital Bank Limited";
             ifsc = NTBL0DEL080;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = UBIN0556688;
             bankId = 0;
             bankName = "union bank of india 2";
             ifsc = UBIN0556688;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = BARB;
             bankId = 0;
             bankName = "Bank of baroda";
             ifsc = BARB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = BACB;
             bankId = 0;
             bankName = "Bassein Catholic Co-Op Bank Ltd";
             ifsc = BACB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = CITI0RTGSMI;
             bankId = 0;
             bankName = "Citi bank NA";
             ifsc = CITI0RTGSMI;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = DEUT;
             bankId = 0;
             bankName = "Deutsche Bank AG";
             ifsc = DEUT;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = AABG;
             bankId = 0;
             bankName = MyCM;
             ifsc = AABG;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = TMBL;
             bankId = 0;
             bankName = "Tamilnad Mercantile Bank Ltd.";
             ifsc = TMBL;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = TSAB;
             bankId = 0;
             bankName = "Telangana State Co Operative Apex Bank Ltd";
             ifsc = TSAB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = GSB0ADC001;
             bankId = 0;
             bankName = "The Ahmedabad District Co-operative Bank Ltd";
             ifsc = GSB0ADC001;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = APBL;
             bankId = 0;
             bankName = "The Andhra Pradesh state cooperative Bank Ltd";
             ifsc = APBL;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = COSB;
             bankId = 0;
             bankName = "THE COSMOS CO-OPERATIVE BANK LTD";
             ifsc = COSB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = VARA;
             bankId = 0;
             bankName = "The Varachha co-op. Bank";
             ifsc = VARA;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = YESB0MSB002;
             bankId = 0;
             bankName = "Udaipur Mahila Samridhi urban Co-Op Bank";
             ifsc = YESB0MSB002;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = UJVN;
             bankId = 0;
             bankName = "Ujjivan Small Finance Bank Limited";
             ifsc = UJVN;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = UBIN;
             bankId = 0;
             bankName = "Union Bank of India";
             ifsc = UBIN;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = VIJB;
             bankId = 0;
             bankName = "Vijaya Bank";
             ifsc = VIJB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = YESB;
             bankId = 0;
             bankName = "Yes Bank";
             ifsc = YESB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = CITB;
             bankId = 0;
             bankName = "Citi bank Retail";
             ifsc = CITB;
             iin = 532662;
             statusCode = 0;
         },
                 {
             bankCode = ASBL;
             bankId = 0;
             bankName = "Apna Sahakari Bank Ltd";
             ifsc = ASBL;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = UTBI0RRBAGB;
             bankId = 0;
             bankName = "Assam Gramin VIkash Bank";
             ifsc = UTBI0RRBAGB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = UTIB0000248;
             bankId = 0;
             bankName = "Axis bank_JusPay";
             ifsc = UTIB0000248;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = BANK;
             bankId = 0;
             bankName = Bank1;
             ifsc = BANK;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = BARB0BRGBXX;
             bankId = 0;
             bankName = "Baroda Rajasthan Kshetriya Gramin Bank";
             ifsc = BARB0BRGBXX;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = BILL;
             bankId = 0;
             bankName = BillDesk;
             ifsc = BILL;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = BILN;
             bankId = 0;
             bankName = BillDesk2;
             ifsc = BILN;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = CSBK;
             bankId = 0;
             bankName = CSBK;
             ifsc = CSBK;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = DCBN;
             bankId = 0;
             bankName = "DCB Bank";
             ifsc = DCBN;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = FDRL;
             bankId = 0;
             bankName = FEDE;
             ifsc = FDRL;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = FDRN;
             bankId = 0;
             bankName = FEDEPSP;
             ifsc = FDRN;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = PJSB;
             bankId = 0;
             bankName = GPPB;
             ifsc = PJSB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = HCEB;
             bankId = 0;
             bankName = "HCE Secure IT Serv";
             ifsc = HCEB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = HDFN;
             bankId = 0;
             bankName = HDFC;
             ifsc = HDFN;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = HSBD;
             bankId = 0;
             bankName = "HSBC Bank_test";
             ifsc = HSBD;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = IDFP;
             bankId = 0;
             bankName = "IDFC_NEW";
             ifsc = IDFP;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = JJSB;
             bankId = 0;
             bankName = "JALGAON JANATA SAHKARI BANK LTD";
             ifsc = JJSB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = JAKA;
             bankId = 0;
             bankName = "Jammu and Kashmir ltd";
             ifsc = JAKA;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = JUSP;
             bankId = 0;
             bankName = JusPay;
             ifsc = JUSP;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = KJSB;
             bankId = 0;
             bankName = "Kalyan Janata Sahakari bank";
             ifsc = KJSB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = KUMR;
             bankId = 0;
             bankName = "Kumaran Systems Pvt. Ltd";
             ifsc = KUMR;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = MSNU;
             bankId = 0;
             bankName = "Mehsana Urban";
             ifsc = MSNU;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = AABA;
             bankId = 0;
             bankName = Mybank;
             ifsc = AABA;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = AABE0101101;
             bankId = 0;
             bankName = Mybene;
             ifsc = AABE0101101;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = PMCB;
             bankId = 0;
             bankName = PMCB;
             ifsc = PMCB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = RNSB;
             bankId = 0;
             bankName = "Rajkot Nagarik";
             ifsc = RNSB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = RAZR;
             bankId = 0;
             bankName = RAZORPAY;
             ifsc = RAZR;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = RBLB;
             bankId = 0;
             bankName = "RBL New";
             ifsc = RBLB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = SRCB;
             bankId = 0;
             bankName = "saraswat bank";
             ifsc = SRCB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = SEQU;
             bankId = 0;
             bankName = "Sequro Technolgy";
             ifsc = SEQU;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = SCBL;
             bankId = 0;
             bankName = "Standard Chartered";
             ifsc = SCBL;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = SYND;
             bankId = 0;
             bankName = Syndicate;
             ifsc = SYND;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = TECH;
             bankId = 0;
             bankName = "TECH TEAM";
             ifsc = TECH;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = TBSB;
             bankId = 0;
             bankName = "Thane Bharat Sahakar";
             ifsc = TBSB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = PUBB;
             bankId = 0;
             bankName = "Thumbworks Technolog";
             ifsc = PUBB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = TJSB;
             bankId = 0;
             bankName = TJSB;
             ifsc = TJSB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = UTBI0RRBTGB;
             bankId = 0;
             bankName = "Tripura Gramin Bank";
             ifsc = UTBI0RRBTGB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = UCBA;
             bankId = 0;
             bankName = UCO;
             ifsc = UCBA;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = UTBI;
             bankId = 0;
             bankName = "United Bank of India";
             ifsc = UTBI;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = VVSB;
             bankId = 0;
             bankName = "Vasai Vikas sahakari bank ltd";
             ifsc = VVSB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = YESB0YBLUPI;
             bankId = 0;
             bankName = "Yes Bank YBL";
             ifsc = YESB0YBLUPI;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = YUKT;
             bankId = 0;
             bankName = Yuktisolution;
             ifsc = YUKT;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = AABE;
             bankId = 0;
             bankName = Mybene;
             ifsc = AABE;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = ABHY;
             bankId = 0;
             bankName = "Abhyudaya Co-operative Bank";
             ifsc = ABHY;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = HDFC0CADARS;
             bankId = 0;
             bankName = "Adarsh Co-operative Bank Ltd";
             ifsc = HDFC0CADARS;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = ABPB;
             bankId = 0;
             bankName = "Aditya Birla Idea";
             ifsc = ABPB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = AIRP;
             bankId = 0;
             bankName = "AIRTEL PAYMENTS BANK";
             ifsc = AIRP;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = ALLA;
             bankId = 0;
             bankName = "allahabad bank";
             ifsc = ALLA;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = ALLA0AU1;
             bankId = 0;
             bankName = "Allahabad UP Gramin Bank";
             ifsc = ALLA0AU1;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = ANDB;
             bankId = 0;
             bankName = ANDB;
             ifsc = ANDB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = APGB;
             bankId = 0;
             bankName = "Andhra Pragathi Grameena Bank";
             ifsc = APGB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = APMC;
             bankId = 0;
             bankName = "A P MAHESH CO-OP URBAN BANK LTD.";
             ifsc = APMC;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = AXIS;
             bankId = 0;
             bankName = AXIS;
             ifsc = AXIS;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = BDBL;
             bankId = 0;
             bankName = "Bandhan Bank";
             ifsc = BDBL;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = BKID;
             bankId = 0;
             bankName = "Bank of India";
             ifsc = BKID;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = MAHB;
             bankId = 0;
             bankName = "Bank of Maharashtra";
             ifsc = MAHB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = HDFC0CBHLUB;
             bankId = 0;
             bankName = "BHILWARA URBAN CO-OPERATIVE BANK LTD";
             ifsc = HDFC0CBHLUB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = JPAY;
             bankId = 0;
             bankName = BHIM;
             ifsc = JPAY;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = UCBA0RRBBKG;
             bankId = 0;
             bankName = "BIhar Gramin Bank";
             ifsc = UCBA0RRBBKG;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = CNRB;
             bankId = 0;
             bankName = Canara;
             ifsc = CNRB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = CANA;
             bankId = 0;
             bankName = Canara;
             ifsc = CANA;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = CBIN;
             bankId = 0;
             bankName = "Central Bank of India";
             ifsc = CBIN;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = ANDB0CGGBHO;
             bankId = 0;
             bankName = "Chaitanya Godavari Grameena Bank";
             ifsc = ANDB0CGGBHO;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = CIUB;
             bankId = 0;
             bankName = "CITY UNION BANK";
             ifsc = CIUB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = MAHB000CB01;
             bankId = 0;
             bankName = "COASTAL LOCAL AREA BANK LTD";
             ifsc = MAHB000CB01;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = DBSS;
             bankId = 0;
             bankName = "DBS BANK LTD";
             ifsc = DBSS;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = DCBL;
             bankId = 0;
             bankName = DCB;
             ifsc = DCBL;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = BKDN;
             bankId = 0;
             bankName = "Dena Bank";
             ifsc = BKDN;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = DEUT0784PBC;
             bankId = 0;
             bankName = "DEUTSCHE BANK AG";
             ifsc = DEUT0784PBC;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = DLXB;
             bankId = 0;
             bankName = "Dhanlaxmi Bank";
             ifsc = DLXB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = DNSB;
             bankId = 0;
             bankName = "Dombivli Nagari bank";
             ifsc = DNSB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = ESFB;
             bankId = 0;
             bankName = "Equitas Small Financ";
             ifsc = ESFB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = ESMF;
             bankId = 0;
             bankName = "ESAF SMALL FINANCE BANK LIMITED";
             ifsc = ESMF;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = FINO;
             bankId = 0;
             bankName = "Fino Payments Bank";
             ifsc = FINO;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = GSCB;
             bankId = 0;
             bankName = "Gujarat State Coop";
             ifsc = GSCB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = HDFC;
             bankId = 0;
             bankName = HDFC;
             ifsc = HDFC;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = HDFB;
             bankId = 0;
             bankName = "HDFC razorpay";
             ifsc = HDFB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = PUNB0HPGB04;
             bankId = 0;
             bankName = "Himachal Pradesh Gramin Bank";
             ifsc = PUNB0HPGB04;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = HSBC;
             bankId = 0;
             bankName = HSBC;
             ifsc = HSBC;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = ICIC00HSBLW;
             bankId = 0;
             bankName = "Hutatma Sahakari Bank Ltd";
             ifsc = ICIC00HSBLW;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = ICIC;
             bankId = 0;
             bankName = ICICI;
             ifsc = ICIC;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = IBKL;
             bankId = 0;
             bankName = IDBI;
             ifsc = IBKL;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = IDFB;
             bankId = 0;
             bankName = IDFC;
             ifsc = IDFB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = IDIB;
             bankId = 0;
             bankName = "Indian Bank";
             ifsc = IDIB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = IPOS;
             bankId = 0;
             bankName = "India Post Payments Bank Limited";
             ifsc = IPOS;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = IOBA;
             bankId = 0;
             bankName = IOB;
             ifsc = IOBA;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = JANA;
             bankId = 0;
             bankName = "Janaseva Sahakari Bank Ltd Pune";
             ifsc = JANA;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = JSFB;
             bankId = 0;
             bankName = "Jana Small Finance Bank";
             ifsc = JSFB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = JSBP;
             bankId = 0;
             bankName = "Janata Sahakari bank";
             ifsc = JSBP;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = JIOP;
             bankId = 0;
             bankName = "JIO Payments Bank";
             ifsc = JIOP;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = JAKA0GRAMEN;
             bankId = 0;
             bankName = "J&K GRAMEEN BANK";
             ifsc = JAKA0GRAMEN;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = KAIJ;
             bankId = 0;
             bankName = "Kallappanna Awade Ic";
             ifsc = KAIJ;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = KARB;
             bankId = 0;
             bankName = KARNATAKABANK;
             ifsc = KARB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = KVGB;
             bankId = 0;
             bankName = "Karnataka Vikas Grameena Bank";
             ifsc = KVGB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = UBIN0RRBKGS;
             bankId = 0;
             bankName = "Kashi Gomti Samyut Gramin Bank";
             ifsc = UBIN0RRBKGS;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = KKBK;
             bankId = 0;
             bankName = "Kotak Mahindra Bank Limited";
             ifsc = KKBK;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = KVBL;
             bankId = 0;
             bankName = KVB;
             ifsc = KVBL;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = ZZZZ;
             bankId = 0;
             bankName = Lenz1;
             ifsc = ZZZZ;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = LAVB;
             bankId = 0;
             bankName = LVB;
             ifsc = LAVB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = PUNB0MBGB06;
             bankId = 0;
             bankName = "Madhya Bihar Gramin Bank";
             ifsc = PUNB0MBGB06;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = MAHG;
             bankId = 0;
             bankName = "MAHARASHTRA GRAMIN BANK";
             ifsc = MAHG;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = MSCI;
             bankId = 0;
             bankName = "Maharashtra State Cooperative Bank Ltd";
             ifsc = MSCI;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = UTBI0RRBMRB;
             bankId = 0;
             bankName = "MANIPUR RURAL BANK";
             ifsc = UTBI0RRBMRB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = IBKL0101MCB;
             bankId = 0;
             bankName = "Maratha Cooperative Bank";
             ifsc = IBKL0101MCB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = AABD;
             bankId = 0;
             bankName = Mybank;
             ifsc = AABD;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = TEST;
             bankId = 0;
             bankName = MyCM;
             ifsc = TEST;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = AABC;
             bankId = 0;
             bankName = Mypsp;
             ifsc = AABC;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = AABF;
             bankId = 0;
             bankName = Mypsp2;
             ifsc = AABF;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = NKGS;
             bankId = 0;
             bankName = "NKGSB CO-Op. Bank Ltd.";
             ifsc = NKGS;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = ORBC;
             bankId = 0;
             bankName = "Oriental Bank of Commerce";
             ifsc = ORBC;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = UCBA0RRBPBG;
             bankId = 0;
             bankName = "Paschim Banga Gramin Bank";
             ifsc = UCBA0RRBPBG;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = PYTM;
             bankId = 0;
             bankName = "PAYTM PAYMENTS BANK";
             ifsc = PYTM;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = PATM;
             bankId = 0;
             bankName = "PAYTM PAYMENTS BANK_New";
             ifsc = PATM;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = PUNB;
             bankId = 0;
             bankName = "PNB migration";
             ifsc = PUNB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = PUNB0123456;
             bankId = 0;
             bankName = "PNB (Wallnut)";
             ifsc = PUNB0123456;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = PRTH;
             bankId = 0;
             bankName = "Prathama Bank";
             ifsc = PRTH;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = PSIB;
             bankId = 0;
             bankName = PSB;
             ifsc = PSIB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = AAMB;
             bankId = 0;
             bankName = "PSP_ONE_TEST";
             ifsc = AAMB;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = PUNB0PGB003;
             bankId = 0;
             bankName = "Punjab Gramin Bank";
             ifsc = PUNB0PGB003;
             iin = 0;
             statusCode = 0;
         },
                 {
             bankCode = IBKL0041SCB;
             bankId = 0;
             bankName = "Samruddhi Co-op bank ltd";
             ifsc = IBKL0041SCB;
             iin = 0;
             statusCode = 0;
         }
     );
     status = S;
     statusDesc = "Bank list response seccessful.";
 }
 
 //MARK :- Secret Questions
 {
     secQuesList =     (
                 {
             quesId = 0;
             quesName = "What was your childhood nickname?";
         },
                 {
             quesId = 101;
             quesName = "What is the name of your favorite childhood friend?";
         },
                 {
             quesId = 103;
             quesName = "What is your favorite team?";
         },
                 {
             quesId = 104;
             quesName = "What is your favorite movie?";
         },
                 {
             quesId = 109;
             quesName = "In what town was your first job?";
         }
     );
     status = S;
     statusDesc = "Security Question list response seccessful.";
 }
 
 // Check Device
 {
     "check_root_detection" = 0;
     devMsgTxt = "INDBUATBNK xzkhDbQlvPgq1I2RYyYJNJJ1hYdGdxRZ3JUDSkrTE85M6IOeH8bHfLhjpuEtEvKKAuUuCzATSmEGM+UblQ04rtcLJ7KljLpYfAQibOgyWow=";
     deviceStatus = DN;
     isMerchant = 0;
     listKeysFlag = IN;
     reVerify = N;
     regLevel = "SEND_SMS";
     simStatus = SNN;
     smsGateWayNo = 9223071030;
     smsStatus = SN;
     status = S;
     statusDesc = SUCCESS;
     userMsg = "Unable to fetch your mobile number for registration. ";
 }
 
 //Get bank account list
 
 {
     accountList =     (
                 {
             accId = 45116;
             accountName = "ABHISHEK AMITABH BACCHAN";
             accountType = SAVINGS;
             aeba = Y;
             atmdLength = 0;
             bankCode = INDB;
             bankId = 0;
             crdLength = 6;
             crdType = NUM;
             formatType = FORMAT1;
             ifscCode = INDB0000018;
             maskedAccountNumber = XXXXXX0416;
             mpinFlag = Y;
             otpdLength = 6;
             otpdType = NUM;
             uPinLength = 0;
         }
     );
     isMerchant = 0;
     pspRefNo = INDBTG864QYAT127J22X9C96T6FWF06CJK2;
     pspRespRefNo = 41269;
     status = S;
     statusDesc = "Account has been fetched successfully";
     userInfo =     {
         defVPAStatus = 0;
         isMerchant = 0;
         name = samreen;
         showMerchant = 0;
         virtualAddress = "919930465134@indus";
     };
     vpaSuggestion =     (
         "9930465134@indus",
         "samreen6219@indus",
         "samreen5692@indus"
     );
 }
 
 //List keys Npci
 {
     listKeys =     (
                 {
             code = NPCI;
             keyValue = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuMKxWfy0WcPp98muBWa6yhpmb6ZGZGSKHRIOv05UlIN5TbUPl6yEerh7Wj0+JyKfsOntRdAVhkLJGRoHwH6gEEeFNHge7kPea/B33cQAbqa39mnP5F1aaZT3tjJnKrfI1Wum0crdb7dAMzft4JILOEa+s3Uh7OdYEl/Xp7EisdSoJ345Cj0LTfLZEQzRdVGovXZrfLByJysH11V9tDrIVv75C/3UndwjHt3NrqzNBoUMh5VZRFkcwuebUAkhIed5gvoysJwd0yYGrAUXNrXJJDTAj5diCuasWyfWZR9lsX5l14hdxF+lqadR/pgII53DW5oEy2LMXgvt2u/qmSml8wIDAQAB";
             ki = 20150822;
             owner = NPCI;
             type = PKI;
         }
     );
     refId = INDF2765696E648408DB8B7955313DEB01E;
     refUrl = "http://www.indusind.com";
     reqMsgId = IND47219276D378483A92C9DBC6F3C99CD6;
     status = SUCCESS;
     txnId = INDUS92EEED859B3247fOzBAHm4ZXsQbxB1;
     xmlResp = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48bnMyOlJlc3BMaXN0S2V5cyB4bWxuczpuczI9Imh0dHA6Ly9ucGNpLm9yZy91cGkvc2NoZW1hLyIgeG1sbnM6bnMzPSJodHRwOi8vbnBjaS5vcmcvY20vc2NoZW1hLyI+PEhlYWQgbXNnSWQ9IjFHUkRwZWdCYkE2OXpQY2NEdkVBIiBvcmdJZD0iTlBDSSIgdHM9IjIwMjAtMTEtMDdUMTA6Mzc6NDYrMDU6MzAiIHZlcj0iMi4wIi8+PFJlc3AgcmVxTXNnSWQ9IklORDQ3MjE5Mjc2RDM3ODQ4M0E5MkM5REJDNkYzQzk5Q0Q2IiByZXN1bHQ9IlNVQ0NFU1MiLz48VHhuIGlkPSJJTkRVUzkyRUVFRDg1OUIzMjQ3Zk96QkFIbTRaWHNRYnhCMSIgbm90ZT0iTGlzdCBLZXlzIiByZWZJZD0iSU5ERjI3NjU2OTZFNjQ4NDA4REI4Qjc5NTUzMTNERUIwMUUiIHJlZlVybD0iaHR0cDovL3d3dy5pbmR1c2luZC5jb20iIHRzPSIyMDIwLTExLTA3VDEwOjM0OjMxKzA1OjMwIiB0eXBlPSJMaXN0S2V5cyIvPjxrZXlMaXN0PjxrZXkgY29kZT0iTlBDSSIga2k9IjIwMTUwODIyIiBvd25lcj0iTlBDSSIgdHlwZT0iUEtJIj48a2V5VmFsdWUgeG1sbnM6eHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4c2k6dHlwZT0ieHM6c3RyaW5nIj5NSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQXVNS3hXZnkwV2NQcDk4bXVCV2E2eWhwbWI2WkdaR1NLSFJJT3YwNVVsSU41VGJVUGw2eUVlcmg3V2owK0p5S2ZzT250UmRBVmhrTEpHUm9Id0g2Z0VFZUZOSGdlN2tQZWEvQjMzY1FBYnFhMzltblA1RjFhYVpUM3RqSm5LcmZJMVd1bTBjcmRiN2RBTXpmdDRKSUxPRWErczNVaDdPZFlFbC9YcDdFaXNkU29KMzQ1Q2owTFRmTFpFUXpSZFZHb3ZYWnJmTEJ5SnlzSDExVjl0RHJJVnY3NUMvM1VuZHdqSHQzTnJxek5Cb1VNaDVWWlJGa2N3dWViVUFraEllZDVndm95c0p3ZDB5WUdyQVVYTnJYSkpEVEFqNWRpQ3Vhc1d5ZldaUjlsc1g1bDE0aGR4RitscWFkUi9wZ0lJNTNEVzVvRXkyTE1YZ3Z0MnUvcW1TbWw4d0lEQVFBQjwva2V5VmFsdWU+PC9rZXk+PC9rZXlMaXN0PjxTaWduYXR1cmUgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxTaWduZWRJbmZvPjxDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvVFIvMjAwMS9SRUMteG1sLWMxNG4tMjAwMTAzMTUiLz48U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxkc2lnLW1vcmUjcnNhLXNoYTI1NiIvPjxSZWZlcmVuY2UgVVJJPSIiPjxUcmFuc2Zvcm1zPjxUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjwvVHJhbnNmb3Jtcz48RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxlbmMjc2hhMjU2Ii8+PERpZ2VzdFZhbHVlPlJBMEZxUWZTclMzcC92NzRPV25BMUZnRmpNbDRqcmhZVW1KU25yZnZtWTg9PC9EaWdlc3RWYWx1ZT48L1JlZmVyZW5jZT48L1NpZ25lZEluZm8+PFNpZ25hdHVyZVZhbHVlPk1oL0h6YTcrZFJBSkhsbjVPemRvUTRnUDJ5WXlIUGNXcUVIMzVCQnBuU3pIT1ZiUU93emRPVUhFRUt1Um1ZMkhPUmdOQ0N3ZUhkY1IKZnkwQVNxb3RUMkRwVlc4YjNOMHNTYS8zRDc2Y2pBQ1NmKzFGNVhiem1uOXhEWWt2UWpFamRrMktRbm9tb24zZWRqbVdIZlNXYzlaSQpQWTZWeWR0NzdvaFp3N3o0blJrUktIdzNEYXp6bktKN2R6ZUp0TExmNzdlRG1DVU9wVE16S2hYckJoL21CL2UyY0xZM3Yzc3QzUUZ0CmFDOFlldEczMklOd2xIWXVPSTVNd0toOGFJeGpVTmdDdW0vS0dFaldibzBrYUhCbWNCbGpOb2FHM3l2SjBVcEPFNpZ25hdHVyZVZhbHVlPk1oL0h6YTcrZFJBSkhsbjVPemRvUTRnUDJ5WXlIUGNXcUVIMzVCQnBuU3pIT1ZiUU93emRPVUhFRUt1Um1ZMkhPUmdOQ0N3ZUhkY1IKZnkwQVNxb3RUMkRwVlc4YjNOMHNTYS8zRDc2Y2pBQ1NmKzFGNVhiem1uOXhEWWt2UWpFamRrMktRbm9tb24zZWRqbVdIZlNXYzlaSQpQWTZWeWR0NzdvaFp3N3o0blJrUktIdzNEYXp6bktKN2R6ZUp0TExmNzdlRG1DVU9wVE16S2hYckJoL21CL2UyY0xZM3Yzc3QzUUZ0CmFDOFlldEczMklOd2xIWXVPSTVNd0toOGFJeGpVTmdDdW0vS0dFaldibzBrYUhCbWNCbGpOb2FHM3l2SjBVcEkrQ3ZGa0NnZ2xvdjgKMit0SkFlZ3U1WXYxRUtpU0h2SHNkcGN6WXdHaXlTbVB6ejRMWUE9PTwvU2lnbmF0dXJlVmFsdWU+PEtleUluZm8+PEtleVZhbHVlPjxSU0FLZXlWYWx1ZT48TW9kdWx1cz51TUt4V2Z5MFdjUHA5OG11QldhNnlocG1iNlpHWkdTS0hSSU92MDVVbElONVRiVVBsNnlFZXJoN1dqMCtKeUtmc09udFJkQVZoa0xKCkdSb0h3SDZnRUVlRk5IZ2U3a1BlYS9CMzNjUUFicWEzOW1uUDVGMWFhWlQzdGpKbktyZkkxV3VtMGNyZGI3ZEFNemZ0NEpJTE9FYSsKczNVaDdPZFlFbC9YcDdFaXNkU29KMzQ1Q2owTFRmTFpFUXpSZFZHb3ZYWnJmTEJ5SnlzSDExVjl0RHJJVnY3NUMvM1VuZHdqSHQzTgpycXpOQm9VTWg1VlpSRmtjd3VlYlVBa2hJZWQ1Z3ZveXNKd2QweVlHckFVWE5yWEpKRFRBajVkaUN1YXNXeWZXWlI5bHNYNWwxNGhkCnhGK2xxYWRSL3BnSUk1M0RXNW9FeTJMTVhndnQydS9xbVNtbDh3PT08L01vZHVsdXM+PEV4cG9uZW50PkFRQUI8L0V4cG9uZW50PjwvUlNBS2V5VmFsdWU+PC9LZXlWYWx1ZT48L0tleUluZm8+PC9TaWduYXR1cmU+PC9uczI6UmVzcExpc3RLZXlzPg==";
 }
 
 //Check VPA
 {
     pspRefNo = INDB6JCHP0AW35H6P6CR10R8PPQPC2;
     regLevel = "REGISTER_VPA";
     resHash = 571d1562ec18cacf7d0f35ab7e3b5807e6d672393f49c7f5ee4bcab24298ba45;
     status = VN;
     statusDesc = "VPA is available.";
     virtualAddress = "7418612973@cscindus";
 }
 
 // Register VPA
 
 {
     customerId = 9570;
     isMerchant = 0;
     merchantFlag = 0;
     pspRefNo = INDBACS8LDZ3PWA2Q6ZX6ZODDC9HJ45L3D1;
     pspRespRefNo = 41269;
     regDate = "2020-11-05 12:12:16";
     resHash = 39c451b00aacef0b66ce970e47cc725992a93c3ed2612c3686068f18882c2766;
     status = S;
     statusDesc = "Congratulations! Your UPI registration is successful.";
     userInfo =     {
         accountId = 28834;
         customerId = 9570;
         defVPAStatus = 0;
         email = "samreennew@gmail.com";
         isMerchant = 0;
         mobileNo = 919930465134;
         name = samreen; // vpaName
         regDate = "2020-11-05 12:12:16";
         showMerchant = 0;
         virtualAddress = "samreennew@indus";
     };
     virtualAddress = "samreennew@indus";
 }
 
 
 //Add Account to VPA
 {
     accountId = 28836;
     aepsAccountId = 28836;
     isMerchant = 0;
     merchantFlag = 0;
     pspRefNo = INDB69Y6L7UOK3YR6RH52H8TOU09AS4X0U2;
     resHash = 1b616e35c89d3bf6c3f55602b747539d462fceae54b8bf440a8fa5d0b949797a;
     status = S;
     statusDesc = "Accounts added successfully";
     userInfo =     {
         accountId = 28836;
         defVPAStatus = 0;
         isMerchant = 0;
         showMerchant = 0;
     };
 }
 
 //getDisputeList
 
 {
     listofCustDispute =     (
                 {
             amount = "1.00";
             crtDate = "Nov 3, 2020 6:42:55 PM";
             custRefNo = 030814064237;
             custid = 0;
             fvaddr = "samreen@indus";
             pgMeTrnRefNo = 856975;
             reasonDesc = "Goods/services are not provided for approved transaction";
             reasonId = 0;
             responseDesc = test;
             status = O;
             storeId = 0;
             targetDate = "Nov 10, 2020 6:42:55 PM";
             ticketNo = 4060;
         }
     );
     pspRefNo = INDB275129DCBABC4FFF88C4053C99;
     status = S;
     statusDesc = "Dispute status list fetched successfully";
     upiTranRefNo = 0;
 }
 
 // Confirm balance check
 {
     accountId = 0;
     accountInfo =     {
         accId = 28063;
         accountBalance = "225847.20";
         accountName = "ABHISHEK AMITABH BACCHAN";
         bankId = 0;
         ifscCode = INDB0000018;
         ledgerBalance = "227249.43";
         maskedAccountNumber = XXXXXX0416;
         uPinLength = 0;
     };
     npciTransId = INDBB334F4F3EFAB095AE0539F42180A086;
     pspRefNo = INDB3BBDF7B8D7F64BC4941A3379D4;
     status = S;
     statusDesc = "Balance Enquiry request success.";
 }
 
*/




/*
Check VPA



["body": ["mobileNo": "7007587639", "virtualAddress": "919930465134@indus", "pspBank": "IndusInd", "vpaMode": "R"], "head": ["location": "Delhi", "appVer": "1.0", "deviceType": "iOS", "reqAction": "checkVPA", "geoLat": "72.223", "appName": "CSC Pay", "pspData": "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTFFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiNDU6MjI6NjU6Q0U6MDA6QzEiLA0KCSJibHVldG9vdGhNYWMiOiAiMjc6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUxRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjg5OTE3NTIxNzAwNjA3NDU0MjUiLA0KCSJvcyI6ICJJT1MgMTMuMi4zIiwNCgkicmVnSWQiOiAiTkEiLA0KCSJzZWxlY3RlZFNpbVNsb3QiOiAiMCIsDQoJImZjbVRva2VuIjogIiINCn0", "geoLong": "77.223", "wifiMac": "02:00:00:00:00:00", "osVer": "14.6", "ts": "2021-07-26T14:31:55", "simId": "7007587639700758760", "bluetoothMac": "02:00:00:00:00:00", "clientIp": "192.168.43.144", "refId": "700758763920210726143155", "deviceId": "DAD9A677-E58E-48D8-B396-706E3610ADBA", "pspId": "IBL"]]
{
    pspRefNo = INDB6JCHP0AW35H6P6CR10R8PPQPC2;
    regLevel = "REGISTER_VPA";
    resHash = 110719aac056e1f74796944244858891218ab3147aa6a1dd3ab7f49ffcba9548;
    status = VN;
    statusDesc = "VPA is available.";
    virtualAddress = "919930465134@indus";
}
Optional(CSCPAY.CheckVPA)
nil
Done**************


Registration

["body": ["pspRespRefNo": "41269", "accId": "45117", "secretAnswer": "test", "accountStatus": "S", "mobileNo": "7007587639", "pspBank": "IndusInd", "customerName": "7007587639", "virtualAddress": "9999894870@cscindus", "bankCode": "INDB", "quesId": "1", "email": "gowtham@gmail.com"], "head": ["ts": "2021-07-26T14:33:3", "refId": "70075876392021072614333", "simId": "7007587639700758760", "geoLat": "72.223", "appVer": "1.0", "deviceType": "iOS", "pspId": "IBL", "wifiMac": "02:00:00:00:00:00", "reqAction": "registerVPA", "location": "Delhi", "appName": "CSC Pay", "osVer": "14.6", "bluetoothMac": "02:00:00:00:00:00", "geoLong": "77.223", "pspData": "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTFFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiNDU6MjI6NjU6Q0U6MDA6QzEiLA0KCSJibHVldG9vdGhNYWMiOiAiMjc6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUxRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjg5OTE3NTIxNzAwNjA3NDU0MjUiLA0KCSJvcyI6ICJJT1MgMTMuMi4zIiwNCgkicmVnSWQiOiAiTkEiLA0KCSJzZWxlY3RlZFNpbVNsb3QiOiAiMCIsDQoJImZjbVRva2VuIjogIiINCn0", "clientIp": "192.168.43.144", "deviceId": "DAD9A677-E58E-48D8-B396-706E3610ADBA"]]
{
    customerId = 9570;
    isMerchant = 0;
    merchantFlag = 0;
    pspRefNo = INDBACS8LDZ3PWA2Q6ZX6ZODDC9HJ45L3D1;
    pspRespRefNo = 41269;
    regDate = "2020-11-05 12:12:16";
    resHash = c3e6582d25cc399e723b5fa8dd23cb9a85bb97604436506b95961b58d11bcced;
    status = S;
    statusDesc = "Congratulations! Your UPI registration is successful.";
    userInfo =     {
        accountId = 28834;
        customerId = 9570;
        defVPAStatus = 0;
        email = "samreennew@gmail.com";
        isMerchant = 0;
        mobileNo = 919930465134;
        name = samreen;
        regDate = "2020-11-05 12:12:16";
        showMerchant = 0;
        virtualAddress = "samreennew@indus";
    };
    virtualAddress = "samreennew@indus";
}




Get bank account list




["body": ["pspBank": "IndusInd", "customerName": "7007587639", "mobileNo": "7007587639", "bankCode": "INDB", "regType": "R"], "head": ["pspData": "ew0KCSJhbmRyb2lkSWQiOiAiNEZGNzAyNkVENzE4ODZDODg2QTlBNTFFIiwNCgkiYXBwTmFtZSI6ICJpbi5jc2NwYXkudXBpIiwNCgkiYXBwVmVyc2lvbkNvZGUiOiAiMSIsDQoJImFwcFZlcnNpb25OYW1lIjogIlBheSBVQVQgMSIsDQoJIndpZmlNYWMiOiAiNDU6MjI6NjU6Q0U6MDA6QzEiLA0KCSJibHVldG9vdGhNYWMiOiAiMjc6MjI6NjU6Q0U6RkQ6QzEiLA0KCSJjYXBhYmlsaXR5IjoiNTIwMDAwMDIwMDAxMDAwNDAwMDYzOTI5MjkyOTI5MiIsDQoJImRldmljZUlkIjogIjRGRjcwMjZFRDcxODg2Qzg4NkE5QTUxRSIsDQoJImRldmljZVR5cGUiOiAiTU9CIiwNCgkiZ2VvQ29kZSI6ICIxOS4wOTExLDcyLjkyMDgiLA0KCSJsb2NhdGlvbiI6ICJNdW1iYWkiLA0KCSJpcCI6ICIxMC45MS4yNi4zNCIsDQoJIm1vYmlsZU5vIjogIjk5OTk4OTQ4NzAiLA0KCSJyZWxheUJ1dHRvbiI6ICJORVpHTnpBeU5rVkVOekU0T0RaRE9EZzJRVGxCTlRCRiIsDQoJInNpbUlkIjogIjg5OTE3NTIxNzAwNjA3NDU0MjUiLA0KCSJvcyI6ICJJT1MgMTMuMi4zIiwNCgkicmVnSWQiOiAiTkEiLA0KCSJzZWxlY3RlZFNpbVNsb3QiOiAiMCIsDQoJImZjbVRva2VuIjogIiINCn0", "bluetoothMac": "02:00:00:00:00:00", "appName": "CSC Pay", "deviceType": "iOS", "clientIp": "192.168.43.144", "pspId": "IBL", "simId": "7007587639700758760", "geoLong": "77.223", "wifiMac": "02:00:00:00:00:00", "refId": "70075876392021072616534", "location": "Delhi", "deviceId": "DAD9A677-E58E-48D8-B396-706E3610ADBA", "reqAction": "getBankAccountList", "appVer": "1.0", "geoLat": "72.223", "ts": "2021-07-26T16:5:34", "osVer": "14.6"]]
{
    accountList =     (
                {
            accId = 45116;
            accountName = "ABHISHEK RANJAN";
            accountType = SAVINGS;
            aeba = Y;
            atmdLength = 0;
            bankCode = INDB;
            bankId = 0;
            crdLength = 6;
            crdType = NUM;
            formatType = FORMAT1;
            ifscCode = INDB0000018;
            maskedAccountNumber = XXXXXX7800;
            mpinFlag = Y;
            otpdLength = 6;
            otpdType = NUM;
            uPinLength = 0;
        }
    );
    isMerchant = 0;
    pspRefNo = INDBTG864QYAT127J22X9C96T6FWF06CJK2;
    pspRespRefNo = 41269;
    resHash = 34c695f8a7630d17ede81ed5893e6046460560bf9d912b97601580fcbffe9486;
    status = S;
    statusDesc = "Account has been fetched successfully";
    userInfo =     {
        defVPAStatus = 0;
        isMerchant = 0;
        name = samreen;
        showMerchant = 0;
        virtualAddress = "919930465134@indus";
    };
    vpaSuggestion =     (
        "9999894870@cscindus",
        "abhishek@cscindus",
        "abhishekranjan@cscindus"
    );
}


Account added to VPA Response



{
    accountId = 28836;
    aepsAccountId = 28836;
    isMerchant = 0;
    merchantFlag = 0;
    pspRefNo = INDB69Y6L7UOK3YR6RH52H8TOU09AS4X0U2;
    resHash = fbdbca25f18571defd3c6b089e04bf70a11130ce7209b6c1bfd9cc0ba0de16e3;
    status = S;
    statusDesc = "Accounts added successfully";
    userInfo =     {
        accountId = 28836;
        defVPAStatus = 0;
        isMerchant = 0;
        showMerchant = 0;
    };
}

Bank Account List Response

{
    accountList =     (
                {
            accId = 45116;
            accountName = "ABHISHEK RANJAN";
            accountType = SAVINGS;
            aeba = Y;
            atmdLength = 0;
            bankCode = INDB;
            bankId = 0;
            crdLength = 6;
            crdType = NUM;
            formatType = FORMAT1;
            ifscCode = INDB0000018;
            maskedAccountNumber = XXXXXX7800;
            mpinFlag = Y;
            otpdLength = 6;
            otpdType = NUM;
            uPinLength = 0;
        }
    );
    isMerchant = 0;
    pspRefNo = INDBTG864QYAT127J22X9C96T6FWF06CJK2;
    pspRespRefNo = 41269;
    resHash = 72a8e856b05040b0abcc0e5ab815b7e377bf86f9aaf93da02107227656cb63f5;
    status = S;
    statusDesc = "Account has been fetched successfully";
    userInfo =     {
        defVPAStatus = 0;
        isMerchant = 0;
        name = samreen;
        showMerchant = 0;
        virtualAddress = "919930465134@indus";
    };
    vpaSuggestion =     (
        "9999894870@cscindus",
        "abhishek@cscindus",
        "abhishekranjan@cscindus"
    );
}

Mpin Response


{
    cred =     {
        atmCrdLength = 0;
        credentialDataLength = 6;
        credentialDataType = NUM;
        formatType = FORMAT1;
        otpCrdLength = 6;
        otpCrdType = NUM;
    };
    deviceId = 866085039927946;
    mobileNo = 919930465134;
    npciTranId = INDB9F765CAC738328FFE0539F42180ABCB;
    otpNpciTranId = INDB9F765CAC738128FFE0539F42180ABCB;
    payerType =     {
        defVPAStatus = 0;
        isMerchant = 0;
        name = "ABHISHEK AMITABH BACCHAN";
        payerBankName = "IndusInd Bank";
        showMerchant = 0;
        virtualAddress = "sam1996@indus";
    };
    pspRefNo = INDBDA76DF5BB20E4A8A84FC5524A1;
    resHash = 1cd761eec91752e3a916fd6a3c923ae777ba61ed6c88f7d4975266a390f36237;
    status = S;
    statusDesc = "OTP request success.";
    tranactionNote = "Enter 6 digits UPI PIN";
    upiTranRefNo = 0;
    virtualAddress = "sam1996@indus";
}


Default Bank Account Response

{
    pspRefNo = INDBH1GSHWQL3LT0;
    recoveryFlag = 0;
    status = S;
    statusDesc = "Your preferred account is added/configured successfully";
    updateFlag = 0;
}
*/
