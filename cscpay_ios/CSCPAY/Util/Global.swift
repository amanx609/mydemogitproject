//
//  Global.swift
//  CSCPAY
//
//  Created by Gowtham on 27/05/21.
//

import Foundation
import UIKit

//MARK: - Get Colors

extension UIColor {
    public class var taleColor : UIColor {
        return hexStringToUIColor(hex: "1B8BBA")
    }
    public class var customBlack : UIColor {
        return .black
    }
    
    
    
    public class var labelDescColor : UIColor {
        return hexStringToUIColor(hex: "BCBCBC")
    }
    
    public class var greyColor : UIColor {
        return hexStringToUIColor(hex: "5B5B5B")
    }
    
    public class var skyBlueTitle : UIColor {
        return hexStringToUIColor(hex: "1B8BBA")
    }
    
    
    public class var titleColor : UIColor {
        return hexStringToUIColor(hex: "04152F") // Black & Ink Blue
    }
    public class var subtitleColor : UIColor {
        return hexStringToUIColor(hex: "959595") // gray light F6F6F6
    }
    public class var subtitleColor2 : UIColor {
        return hexStringToUIColor(hex: "17749C") // gray light F6F6F6
    }
    public class var lightGreyColor : UIColor {
        return hexStringToUIColor(hex: "F6F6F6") // gray light
    }
    public class var darkGray : UIColor {
        return hexStringToUIColor(hex: "434343")
    }
    public class var customWhite : UIColor {
        return .white // gray
    }
    public class var lightBlue : UIColor {
        return hexStringToUIColor(hex: "6B99E0")
    }
    public class var customBlue : UIColor {
        return hexStringToUIColor(hex: "1B8BBA") // 28A9E0
    }
    public class var bgBlue : UIColor {
        return hexStringToUIColor(hex: "F6FBFE")
    }
    public class var seperatorColor : UIColor {
        return hexStringToUIColor(hex: "D8D8D8") // gray EF425A
    }
    public class var customRed : UIColor {
        return hexStringToUIColor(hex: "EF425A")
    }
    public class var customGreen : UIColor {
        return hexStringToUIColor(hex: "09B55C") //64BC26
    }
    public class var noColor : UIColor {
        return .clear
    }
    public class var onBoardingBlack : UIColor{
        return hexStringToUIColor(hex: "525252")
    }
    
    public class var msmseYesColor : UIColor {
        return hexStringToUIColor(hex: "F6FBFE")
    }
    
    public class var msmseNoColor : UIColor{
        return hexStringToUIColor(hex: "F8F8F8")
    }
    
    
    
    
}

extension UIFont {
    //let font = UIFont.size(16)
    class func size(_ size: CGFloat = 16, fontType: Fonts = .regular) -> UIFont {
            return UIFont.init(name: "Nunito-\(fontType)", size: size) ?? UIFont.systemFont(ofSize: size)
        }
}
extension UILabel{
    var defaultFont: UIFont? {
        get { return self.font }
        set { self.font = newValue }
    }
    func setup(fontSize: CGFloat = 16, fontType:Fonts = .regular, textColor : UIColor = .titleColor, bgColor: UIColor = .clear, text : String? = nil) {
        self.font            = .size(fontSize, fontType: fontType)
        self.textColor       = textColor
        self.backgroundColor = bgColor
        if let txt = text {
            self.text = txt.capitalizingFirstLetter()
        }
    }
}
extension UIButton {
    func setup(fontSize: CGFloat = 16, fontType:Fonts = .regular, textColor : UIColor = .taleColor, bgColor: UIColor = .clear,  text : String? = nil) {
        self.titleLabel?.font            = .size(fontSize, fontType: fontType)
        self.setTitleColor(textColor, for: .normal)
        self.backgroundColor = bgColor
        if let txt = text {
            self.setTitle(txt, for: .normal)
        }
    }
}
extension UITextField {
    func setup(fontSize: CGFloat = 16, fontType:Fonts = .regular, textColor : UIColor = .titleColor, bgColor: UIColor = .clear,  text : String? = nil, placeholderText: String?) {
        self.font            = .size(fontSize, fontType: fontType)
        self.textColor       = textColor
        self.backgroundColor = bgColor
        if let txt = text {
            self.text = txt
        }
        if let pht = placeholderText {
            self.placeholder = pht
        }
    }
}
extension UITextView {
    func setup(fontSize: CGFloat = 16, fontType:Fonts = .regular, textColor : UIColor = .titleColor, bgColor: UIColor = .clear,  text : String? = nil) {
        self.font            = .size(fontSize, fontType: fontType)
        self.textColor       = textColor
        self.backgroundColor = bgColor
        if let txt = text {
            self.text = txt
        }
    }
}



//MARK: - Get Font
enum Fonts : CustomStringConvertible {
    case regular
    case italic
    case bold
    case light
    case semiBold
    case extraBold
    
    var description : String {
        switch self {
        // Use Internationalization, as appropriate.
        case .regular:  return "Regular"
        case .italic:   return "Italic"
        case .bold:     return "Bold"
        case .light:    return "Light"
        case .semiBold: return "SemiBold"
        case .extraBold:return "ExtraBold"
        }
      }
}

//extension String {
//      func capitalizeFirstLetter() -> String {
//           return self.prefix(1).capitalized + dropFirst()
//      }
//    
//    
//    
//    
// }
extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
