//
//  CommonFucntions.swift
//  CSC PAY
//
//  Created by Abhishek Ranjan on 06/01/21.
//

import Foundation
import UIKit
import ContactsUI

//MARK: - Get last 7 days
func getlast7days(day : Int) -> [String] {
    switch day {
    case 1:
        return ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    case 2:
        return [ "Tue", "Wed", "Thu", "Fri", "Sat", "Sun", "Mon"]
    case 3:
        return ["Wed", "Thu", "Fri", "Sat", "Sun", "Mon", "Tue"]
    case 4:
        return ["Thu", "Fri", "Sat", "Sun", "Mon", "Tue", "Wed"]
    case 5:
        return ["Fri", "Sat", "Sun", "Mon", "Tue", "Wed", "Thu"]
    case 6:
        return ["Sat", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri"]
    case 7:
        return ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
    default:
        return [String]()
    }
}

//MARK: - Call using number
func callNumber(phoneNumber: String) {
    let formattedNumber = phoneNumber.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")

    if let url = NSURL(string: ("tel:" + (formattedNumber))) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url as URL)
        }
    }
}
//MARK: - Converting hex string to UIColor
func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }

    if ((cString.count) != 6) {
        return UIColor.gray
    }

    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
//MARK: - Hex to String
func hex(from string: String) -> Data {
    .init(stride(from: 0, to: string.count, by: 2).map {
        string[string.index(string.startIndex, offsetBy: $0) ... string.index(string.startIndex, offsetBy: $0 + 1)]
    }.map {
        UInt8($0, radix: 16)!
    })
}
//MARK: - Convert Dictionary to String
func dictToString(dic: [String: String]) -> String {
    let cookieHeader = dic.map { $0.0 + "=" + $0.1 }.joined(separator: "|")
    return cookieHeader
}
//MARK: - Convert JsonString to Models
func getTypes<T : Decodable>(of type: T.Type, from jsonString:String) throws -> T {
        let jsonData = Data(jsonString.utf8)
        let endResult = try JSONDecoder().decode(T.self, from: jsonData)
        return endResult
}
//MARK: - Convert JsonString to Model
func getType<T : Decodable>(of type: T.Type, from jsonString:String) throws -> [T] {
        let jsonData = Data(jsonString.utf8)
        let endResult = try JSONDecoder().decode([T].self, from: jsonData)
        return endResult
}
//MARK: - Change Button State
func changeButtonsState(btn : UIButton, otherBtns : [UIButton]) {
    DispatchQueue.main.async {
        if btn.backgroundColor == UIColor.clear {
            btn.backgroundColor = hexStringToUIColor(hex: "1160DA")
            btn.setTitleColor(.white, for: .normal)
        } else {
            btn.backgroundColor = UIColor.clear
            btn.setTitleColor(hexStringToUIColor(hex: "04152F"), for: .normal)
        }
        for otherBtn in otherBtns {
            otherBtn.backgroundColor = .clear
            otherBtn.setTitleColor(hexStringToUIColor(hex: "04152F"), for: .normal)
        }
    }
}
//MARK: - Change UIView Alpha
func changeViewsAlpha(view : UIView, otherViews : [UIView]) {
    DispatchQueue.main.async {
        if view.alpha == 0 {
        view.alpha = 1
        } else {
            view.alpha = 0
        }
        for otherView in otherViews {
            otherView.alpha = 0
        }
    }
}

//MARK: - Get Contacts
func getContactFromCNContact() -> [CNContact]? {

    let contactStore = CNContactStore()
    let keysToFetch = [
        CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
        CNContactPhoneNumbersKey,
        CNContactEmailAddressesKey,
        CNContactThumbnailImageDataKey,
        CNContactGivenNameKey,
        CNContactMiddleNameKey,
        CNContactFamilyNameKey,
        CNContactThumbnailImageDataKey
        ] as [Any]

    //Get all the containers
    var allContainers: [CNContainer] = []
    do {
        allContainers = try contactStore.containers(matching: nil)
    } catch {
        print("Error fetching containers")
    }

    var results : [CNContact] = []

    // Iterate all containers and append their contacts to our results array
    for container in allContainers {

        let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)

        do {
            let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
            results.append(contentsOf: containerResults)

        } catch {
            print("Error fetching results for container")
        }
    }

    return results
}

//MARK: - Common Alert
func notImplementedYetAlert(base: UIViewController,title: String = "Error", msg :String = "Not implemented yet.") {
    DispatchQueue.main.async {
        let alert = UIAlertController(title:title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        base.present(alert, animated: true, completion: nil)
    }
}

//MARK: - Get IP / Mac Address
func getIPAddress() -> String? {
    var address : String?

    // Get list of all interfaces on the local machine:
    var ifaddr : UnsafeMutablePointer<ifaddrs>?
    guard getifaddrs(&ifaddr) == 0 else { return nil }
    guard let firstAddr = ifaddr else { return nil }

    // For each interface ...
    for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
        let interface = ifptr.pointee

        // Check for IPv4 or IPv6 interface:
        let addrFamily = interface.ifa_addr.pointee.sa_family
        if addrFamily == UInt8(AF_INET) { //|| addrFamily == UInt8(AF_INET6) {

            // Check interface name:
            let name = String(cString: interface.ifa_name)
            if  name == "en0" || name == "pdp_ip0" { //For wifi name == "en0" ||

                // Convert interface address to a human readable string:
                var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                            &hostname, socklen_t(hostname.count),
                            nil, socklen_t(0), NI_NUMERICHOST)
                address = String(cString: hostname)
            }
        }
    }
    freeifaddrs(ifaddr)
    
    if Constants.isSimulator {
        return "10.91.26.36"
    }

    return address
}

//MARK: - Get CPU Info
func hostCPULoadInfo() -> host_cpu_load_info? {
    let HOST_CPU_LOAD_INFO_COUNT = MemoryLayout<host_cpu_load_info>.stride/MemoryLayout<integer_t>.stride
    var size = mach_msg_type_number_t(HOST_CPU_LOAD_INFO_COUNT)
    var cpuLoadInfo = host_cpu_load_info()

    let result = withUnsafeMutablePointer(to: &cpuLoadInfo) {
        $0.withMemoryRebound(to: integer_t.self, capacity: HOST_CPU_LOAD_INFO_COUNT) {
            host_statistics(mach_host_self(), HOST_CPU_LOAD_INFO, $0, &size)
        }
    }
    if result != KERN_SUCCESS{
        print("Error  - \(#file): \(#function) - kern_result_t = \(result)")
        return nil
    }
    return cpuLoadInfo
}

//MARK :- Userdefaults
func linkBank() {
    let bLink = UserDefaults.standard.integer(forKey: "linkedBank")
    UserDefaults.standard.set(bLink + 1, forKey: "linkedBank")
    UserDefaults.standard.synchronize()
}
func linkBankCount() -> Int {
    let bLink = UserDefaults.standard.integer(forKey: "linkedBank")
    return bLink
}
func removeLinkedBank() {
    let bLink = UserDefaults.standard.removeObject(forKey: "linkedBank")
    UserDefaults.standard.synchronize()
}
//MARK: - Get Difference in Minutes from two dates
func getMinutesDifferenceFromTwoDates(start: Date, end: Date) -> String?
   {
       let diff = Int(end.timeIntervalSince1970 - start.timeIntervalSince1970)

       let hours = diff / 3600
       let minutes = (diff - hours * 3600) / 60
    if hours >= 0 && minutes >= 0 {
        if hours > 0 {
            return "\(hours) hour \(minutes) mins"
        }
        return "\(minutes) mins"
    }
    return nil // (hours * 60) + minutes
   }

// MARK:- Create CGImage
func cgImage(from ciImage: CIImage) -> CGImage? {
    let context = CIContext(options: nil)
    return context.createCGImage(ciImage, from: ciImage.extent)
}
// MARK:- Convert Any type to String
func toString(_ value: Any?) -> String {
  return String(describing: value ?? "")
}
