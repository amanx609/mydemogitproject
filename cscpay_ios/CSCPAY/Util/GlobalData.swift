//
//  Globals.swift
//  CSCPAY
//
//  Created by Gowtham on 30/06/21.
//

import Foundation
import  UIKit

final class GlobalData: NSObject {
   static let sharedInstance = GlobalData()
    
    var config : ConfigResponse?
    var checkDevice : CheckDevice?
    var otpResp : OTPResponse?
    var otpValid : OTPResponse?
    var bankAccounts : BankAccountsList?
    var checkVPA : CheckVPA?
    var registerVPA : RegisterVPA?
    var addAccountToVPA : AddAccountToVPA?
    var setDefaultAccount : SetDefaultAccount?
    var disputeList : DisputeList?
    var confirmBalance : ConfirmBalance?
    var setToken : GetTokenResponse?
    var raiseDispute : DisputeList?
    var merchantConfig : MerchantConfigResponse?
    var merchantRegistrationResponse : MerchantRegistrationModel?
    
    

   private override init() { }

   func foo() { }
}
