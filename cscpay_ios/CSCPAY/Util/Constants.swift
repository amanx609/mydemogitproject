//
//  Constants.swift
//  CSC PAY
//
//  Created by Abhishek Ranjan on 06/01/21.
//

import Foundation

class Constants {
    
    public static var geolat = "72.223"
    public static var geolong = "77.221"
    public static var location = "Delhi"
    
    public static var byPass = false
    public static var authenticationStatus = "Not Done"
    public static let forMerchant = true
    public static var isSimulator = true
    public static let presentVCForTest = false
    public static let vc = getPanCardAndBankViewController()
    public static let historyEmptyTest = false
    public static let receiveHistoryEmptyTest = false
    public static let BASE_URL = "https://devupi.csccloud.in/gw1/"
    public static let printResponse = true
    public static let api_off = false
    public static var uploadDocData = CompanyDocModel(companies: nil)
    
    //Alerts
    //Local auth
    public static let alert_enter_passcode      = "Enter your Passcode"
    public static let alert_unlock_cscapp       = "Unlock CSC APP"
    public static let alert_use_passcode        = "Please use your Face ID or enter device's passcode to access your VLE account"
    //OTP
    public static let alert_enter_mobile        = "Please enter mobile number"
    public static let alert_enter_valid_mobile  = "Please enter valid mobile number"
    public static let alert_enable_internet     = "Please enable internet connection"
    public static let SOMETHING_WRONG     = "Something went wrong, please try again later."
    //Verifcation
    public static let alert_enter_otp       = "Please enter OTP"
    public static let alert_enter_valid_otp  = "Please enter valid OTP"
    
    public static let alert_enter_upi        = "Please enter UPI address"
    public static let alert_enter_valid_upi  = "Please enter valid UPI address"
    
    public static let alert_enter_amount  = "Please enter amount"
    public static let alert_enter_valid_amount  = "Please enter valid amount"
    
    public static let alert_enter_account_number  = "Please enter account number"
    public static let alert_enter_valid_account_number  = "Please enter valid account number"
    public static let alert_enter_re_account_number  = "Please enter Re-enter account number"
    public static let alert_enter_valid_re_account_number  = "Please enter valid Re-enter account number"
    public static let alert_enter_ifsc_code  = "Please enter IFSC Code"
    public static let alert_enter_valid_ifsc_code  = "Please enter valid IFSC Code"
    public static let alert_receipient_name  = "Please enter Receipient name"
    public static let alert_register_ID    = "Please enter registered id"
    public static let alert_pan_details   = "Please enter your PAN No."
    
    
    
    //Location Alert
    public static let LOCATION_ALERT = "locationForRegister"
    public static let alert_enable_location_register = "Location Permission Required.\n\nAs a CSC user, you have to operate from a designated location you specified during the registration. Your location information is required to validate that you are operating from the declared location.\n\n*Your location is not shared with anyone, its only used for validation purpose during the activation."
    public static let alert_enable_location_register_title = "Location Permission Required."
    public static let alert_enable_location_register_change = "*Your location is not shared with anyone, its only used for validation purpose during the activation."
    public static let RegisterWithOtp = "AlreadyRegWithOTP"
    
    
    //Bank Items
    public static let Bank_Items : [[String:Any]] = [["title":"HDFC Bank", "icon":"hdfc"], ["title":"Axis Bank", "icon":"axis"], ["title":"ICICI Bank", "icon":"icici"], ["title":"IDBI Bank", "icon":"idbi"], ["title":"Union Bank", "icon":"union"], ["title":"PNB Bank", "icon":"pnb"], ["title":"SBI Bank", "icon":"sbi"], ["title":"Canara Bank", "icon":"canara"] ]
    
    
    public static let popularBanks : [[String:Any]] =
    [["title":"HDFC", "icon":"hdfc"], ["title":"AXIS", "icon":"axis"], ["title":"ICIC", "icon":"icici"], ["title":"IBKL", "icon":"idbi"], ["title":"UBIN", "icon":"union"], ["title":"ALLA", "icon":"pnb"], ["title":"BARB", "icon":"sbi"], ["title":"CANA", "icon":"canara"] ]
    
    
    public static let alert_enter_business_name        = "Please enter registered business name."
    public static let alert_enter_shop_name        = "Please enter shop name."
    public static let alert_choose_business_type        = "Please choose Type of Business."
    public static let alert_enter_merchant_category        = "Please enter merchant category."
    public static let alert_enter_owner_name        = "Please enter business owner name."
    public static let alert_capture_image        = "Capture photo"
    
    public static let alert_enter_shop_number        = "Please enter shop number."
    public static let alert_enter_area        = "Please enter shop address."
    public static let alert_choose_city        = "Please choose your city."
    public static let alert_choose_state       = "Please choose your state."
    public static let alert_enter_pincode        = "Please enter valid pincode."
    
    //Camera alert
    public static let alert_open_gallery    = "Open Gallery"
    public static let alert_choose_image    = "Choose Image"
    public static let alert_capture_image2   = "Capture Image"
    public static let alert_remove_image    = "Remove Image"
    
    // Home bottom illustrations
    public static let Illustrations : [String] = ["BottomIllustration", "BottomIllustration_2", "BottomIllustration_3", "BottomIllustration_4", "BottomIllustration_5"]
    //Guide items
    public static let Pay_Guide_Items : [[String:Any]] = [["title":"Make your first Payment", "message":
"""
\u{2022} To make your first payment, you must have to add a bank account.
\u{2022} While adding your bank account on CSC Pay, make sure that:
- The mobile number you have used while registering is same as the mobile number linked with bank account.
- The mobile number as sufficient balance to send a verification SMS.
- You have to mobile network and internet connectivity.
\u{2022} Once the account is link set your UPI PIN

Note: if you already have a UPI PIN, just tap and link.
"""
   , "change":"Note: if you already have a UPI PIN, just tap and link." ], ["title":"hdfc", "message":"hdfc"] ]
    
    //MARK:- OTP Verification Screen - 03
    public static let otpSentHint = "Please enter the verification code sent to +91********"
    public static let otpResend = "Don't receive the OTP?  RESEND"
    public static let otpResendBtn = "RESEND"
    public static let otpResendCount = "Resend available in"
    //Home Screen
    public static let contactCount  = 4
    //Requests screen
    public static let lbl_requestedBy = "Requested by"
    public static let lbl_expiry = "Expires in"
    public static let lbl_expiry_type = "mins"
    public static let lbl_currency = "₹"
    //Transaction history
    public static let lbl_paid_to   = "Paid To"
    public static let lbl_received_from   = "Received From"
    //Scan and Pay
    public static let lbl_scan_not_supported_title   = "Scanning not supported"
    public static let lbl_scan_not_supported_message    = "Your device does not support scanning a code from an item. Please use a device with a camera."
    //PAy Confirm
    public static let txt_amount_placeholder = "0"
    public static let txt_note_placeholder  = "Make a note"
    public static let lbl_pay_from = "Pay from"
    public static let btn_proceed_to_pay    = "PROCEED TO PAY"
    //Transaction Details
    //static let lbl_transaction_detail_title = "Transaction Detials"
    //Payment method
    public static let lbl_primary_account = "Primary Account"
    //User Details
    public static let alert_enter_name = "Enter your name"
    public static let alert_enter_valid_name = "Name can be alphanumeric and first letter should be alphabet."
    public static let alert_select_gender = "Select your gender"
    public static let alert_enter_dob = "Enter your DOB"
    public static let alert_enter_email = "Enter your email"
    static let alert_enter_valid_email = "Enter valid email"
    public static let alert_enter_user_otp = "Enter OTP that you've received"
    public static let alert_verify_email = "Verify email to proceed."
    
    public static let alert_enter_valid_dob = "Enter valid DOB"
    
    //Change UPI through card
    
    public static let alert_enter_valid_expiry_month = "Enter valid expiry month"
    public static let alert_enter_valid_expiry_year = "Enter valid expiry year"
    public static let alert_enter_valid_card_number = "Enter valid card number"
    public static let lbl_primary_upiID = "Primary UPI ID"

    //Userdefaults
    public static let User_Image = "UserImg"
    public static let User_Info = "UserInfo"
    public static let Master_Changes = "MasterChanges"
    
    public static let alert_enter_valid_pancard = "Enter Valid Pan Card Number"
}

//MARK: - Status enum
enum Status {
    case fine
    case used
    case current
    case illegal
    case simulator
    case notInUse
}

/*
 
 {
   "mobileVerificationTitleLabel" : "Mobile Verification",
   "mobileVerificationSubtitleLabel" : "Please enter your mobile number linked to your bank account",
   "enterMobileNumberFieldPlaceholder" : "Mobile number",
   "sendOTPButtonTitle" : "SEND OTP",
   
   "otpVerificationTitleLabel" : "Verification Code",
   "otpVerificationSubtitleLabel" : "Please enter the verification code sent to ",
   "dontReceivedOTPLabel" : "Don’t receive the OTP? <RESEND>",
   "verifyButtonTitle" : "VERIFY",
   
   "linkYourBankAccountTitle" : "Link your bank account",
   "recommendedLabel" : "Recommended",
   "linkBankAccountDescription" : "You can add your bank account, cards and UPI IDs",
   "linkNowButtonText" : "Link Now",
   "becomeAmerchantLabel" : "Become a Merchant",
   "becomeAmerchantDescription" : "Get All-in-One-QR instantly & start accepting payments",
   "inviteYourFriendsLabel" : "Invite your Friends",
   "inviteYourFriendsDescription" : "*********Not fixed yet******",
   "settingsButtonText" : "Settings",
   "helpButtonText" : "Help"
 
   "welcomeUserText" : "Namaste,",
   "searchPlaceholderText" : "Search here...",
   "makePaymentButtonText" : "Make a Payment",
   "requestPaymentButtonText" : "Request Payments",
   "showTransactionHistoryLabel" : "Show Transaction History",
   "showTransactionHistoryDescription" : "See your payment and received history",
   "checkSpendTrendsLabel" : "Check Spend Trends",
   "findPeopleToPayLabel" : "People who you’ve recently paid will show up here. Find people to pay",
   "payToContactsLabel" : "Pay to Contacts",
   "morePlusLabel" : "More",
   
   "notificationTitleLabel" : "Notifications",
   "markAsRead" : "Mark as Read",
   "scanAndPayTitleLabel" : "Scan & Pay",
   "enterMobileOrUpiPlaceholder" : "Enter Mobile Number or UPI ID",
   "recentContactsLabel" : "Recents",
   
   "myQrTitleLabel" : "My QR",
   "placeQRLabel" : "Place the QR Code inside the area",
   "scanWillStartAutomaticallyLabel" : "Scanning will start automatically",
   "shareButtonText" : "SHARE",
   "enterMobileOrUpiPlaceholder2" : "Enter Mobile Number or UPI ID",
   "recentContactsLabel2" : "Recents",
   "poweredByLabel" : "Powered by",
   
   "payTitleLabel" : "Pay",
   "transferOnLabel" : "Transfer on ",
   "addAmessagePlaceholder" : "Add a message",
   "payFromLabel" : "Pay from",
   "proceedToPayButtonText" : "Proceed to pay",
   
   "transactionHistoryTitleLabel" : "Transaction History",
   "summaryLabel" : "Summary",
   "paymentMadeLabel". : "Payments Made",
   "paymentReceivedLabel" : "Payments Received",
   "paymentHistoryLabel" : "Payment History",
   "seeYourRecentPaymentsLabel" : "See your recent payments",
   "paidButtonText" : "Paid",
   "receivedButtonText" : "Received",
   "referenceLabel" : "Ref",
   "orderIdLabel" : "Order ID",
 
   "noTransactionFoundLabel" : "No transactions found",
   "selectYourBankTitleLabel" : "Select Your Bank",
   "searchPlaceholderText2" : "Search here...",
   "popularBanksLabel" : "Popular Banks",
   "allBanksLabel" : "All Banks",
   
   
   "fetchingAccountDetailsLabel" : "Fetching the account details linked with",
   "fetchingAccountDetailsChargesLabel" : "Make sure the number is linked to your bank and the SIM is on your device. Standard carrier charges apply."
   "okButtonText" : "OK",
   "findingAccountLabel" : "Finding Accounts",
   "sentSMSforVerificationLabel" : "Sent SMS for verification",
   "verifiedNumberWithBank" : "Verified number with bank",
   "findingBankAccountsLabel" : "Finding bank accounts",
   
   "selectTheBankAccountTitleLabel" : "Select the bank account",
   "selectTheBankAccountSubtitleLabel" : "Get your debit or ATM card ready to enter some deatils. <Learn more>",
   "savingsAccountLabel" : "Savings Account",
   "currentAccountLabel" : "Current Account",
   "termsOfServicesAndPrivacyNoticeLabel" : "By continuing, you agree to the CSC Pay <Terms of Services>. The <Privacy Notice> describes how your data is handled.",
   "startButtonText" : "START",
 
   "settingsTitleLabel" : "Settings",
   "becomeAmerchantLabel2" : "Become a Merchant",
   "becomeAmerchantDescription2" : "Get All-in-One-QR instantly & start accepting payments",
   "manageBankAccountsLabel" : "Manage bank accounts",
   "paymentsMethodLabel" : "Payment methods",
   "bankAccountAndCardsLabel" : "Bank accounts & cards",
   "upiAddressLabel" : "UPI address",
   "checkUpiAddressLabel" : "Check UPI address",
   "manageProfileLabel" : "Manage profile",
   "languageLabel" : "Language",
   "helpAndFeedbackLabel" : "Help & feedback",
   "checkForHelpLabel" : "Check for help",
   "supportLabel" : "Support",
   "seekSupportLabel" : "Seek support",
   "privacyAndSecurityLabel" : "Privacy & security",
   "notificationsLabel" : "Notifications",
   "turnNotificationLabel" : "Turn notifications on/off",
   "logoutLabel" : "Logout",
   "logoutFromCSCPayLabel" : "Logout from CSC Pay",
 
   "paymentsMethodsTitleLabel" : "Payment Methods",
   "addBankAccountLabel" : "Add Bank Account",
   "paymentsAccountsLabel" : "Primary Account",
   "accountInformationLabel" : "Account Information",
   "upiIDLabel" : "UPI ID",
   "viewBalanceLabel" : "View Balance",
   "forgetUpiPinLabel" : "Forget UPI PIN?",
   
   "balanceTitleLabel" : "Balance",
   "bankBalanceFetchedInfo" : "Bank balance fetched successfully",
   "yourAccountBalanceLabel" : "Your Savings A/C Balance",
   
   "upiAddressTitleLabel" : "UPI Address",
   "upiAddressPlaceholder" : "UPI Address",
   "addNewUpiIdLabel" : "Add new upi id",
   "addNewUpiAddressTitleLabel" : "Add New UPI Address",
   "enterUpiAddressPlaceholder" : "Enter UPI Address",
   "verifyButtonText" : "Verify",
   "accountHolderNamePlaceholder" : "Account Holder Name",
   "proceedButtonText" : "PROCEED",
   
   "verificationTitleLabel" : "Verification Code",
   "pleaseEnterVerificationCodeLabel" : "Please enter the verification code",
   "dontReceivedOTPLabel" : "Don’t receive the OTP? <RESEND>",
   "verifyButtonText" : "VERIFY",
   
   "spendTrendsTitleLabel" : "Spend Trends",
   "spendTrendCategories" : "Daily,Weekly,Monthly",
   "spendTrendSubCategories" : "Expenses,Income",
   "spendTrendSubCategoriesItems" : "Transfer & cash,Entertainment,Shopping",
   
   "transactionDetailsTitleLabel" : "Transaction Details",
   "repeatTransactionButtonText" : "REPEAT TRANSACTION",
   
   "requestMoneyTitleLabel" : "Request Money",
   "requestMoneyCategories" : "Make Request,Pending Request",
   "requestMoneySubCategories" : "UPI ID / Mobile Number,History",
   "inviteButtonText" : "Invite",
   "historyExpiresText" : "Expires in",
   
   "requestTitleLabel" : "Request",
   "requestOnLabel" : "Request on",
   "receiveMoneyInLabel" : "Receive money in",
   "proceedToRequestButtonText": "PROCEED TO REQUEST",
   
   "youAreRequestingLabel" : "You are requesting",
   "fromLabel" : "From",
   
   "congratsMessage" : "Congratulations!",
   "requestSentInfo" : "Request Sent Successfully to",
   "goToHomeText" : "go to home in "
 
   "declineRequestAlertTitle" : "Are you sure you want to decline the request made by",
   
   "helpAndFeedbackTitleLabel" : "Help and Feedback",
   "viewAllTransactionButtonText" : "View all transactions",
   "otherHelpTopicsLabel" : "Other help topics",
   "importantLabel" : "Important",
   "doNotShareInfo" : "Please do not share any confidential information like UPI PIN, OTP, CVV",
   "chatTitleLabel" : "Chat"
 
 }
 //For merchant Contents
 {
   "generalDetailsTitleLabel" : "General Details",
   "registeredBusinessNamePlaceholder" : "Registered Business Name",
   "registeredBusinessNameExample" : "E.g. A-B-C Pvt. Ltd",
   "shopNamePlaceholder" : "Shop Name",
   "shopNameExample" : "Name your customers will see",
   "typeOfBusinessPlaceholder" : "Type of Business",
   "merchantCategoryPlaceholder" : "Merchant Category",
   "businessOwnerNamePlaceholder" : "Business Owner Name",
   "businessOwnerNameExample" : "Name of Owner, Director or Partner",
   "captureShopPhoto" : "Capture Shop Photo",
   "nextButtonText" : "NEXT",
   
   "addressDetailsTitleLabel" : "Address Details",
   "shopNumberAndAddressPlaceholder" : "Shop Number and Street Name",
   "areaPlaceholder" : "Area/Locality/Village",
   "cityPlaceholer" : "City",
   "statePlaceholder" : "State",
   "pincodePlaceholder" : "PIN Code",
   
   "panAndBankDetailsTitleLabel" : "PAN & Bank Details",
   "panCardNumberPlaceholder" : "PAN Card Number",
   "uploadPancardPhotoPlaceholder" : "Upload Pan Card Photo",
   "gstinPlaceholder" : "GSTIN (optional)",
   "bankAccountNumberPlaceholder" : "Bank Account Number",
   "confirmBankAccountNumberPlaceholder" : "Confirm Bank Account Number",
   "ifscCodePlaceholder" : "IFSC Code",
   "findIFSCButtonText" : "Find IFSC",
   
   "summaryTitleLabel" : "Summary",
   "summarySubtitleLabel" : "Please verify the information entered by you",
   "verifyButtonText" : "VERIFY",
   
   "registrationSuccessMessage" : "Registration Successful!",
   "welcomeMessageForMerchant" : "Welcome to the application, start the experience and wish you the best moments",
   
   
   "downloadQrButtonText". : "Download QR",
   "shareQrButtonText" : "Share QR",
   "requestQrStickerButtonText" : "Request QR Sticker",
   "proceedToHomepageButtonText" : "proceed to homepage",
   
   "mtWelcomeLabel" : "Namaste, ",
   "requestMoneyFromHomeLabel" : "Request money from your customers via UPI and Mobile Number,
   "mtShowTransactionHistoryLabel" : "Show Transaction History",
   "mtSeeYourTransactionLabel" : "See your transaction and settlement history",
   "mtCheckBusinessTrendsLabel" : "Check Business Trends",
   "mtSettlementLabel" : "All the payments will be settled within 24 hours. Check settled payments",
   "graphCategories" : "Transactions,Settlements",
   "mtBusinessTrendsTitleLabel" : "Business Trends",
   "mtBusinessTrendsCategories" : "Weekly,Monthly",
   "dateComponentsForBusinessTrends" : "From,To",
   "last7DaysTransactionVolumeLabel" : "Last 7 day transaction volumes",
   "last7DaysTransactionVolumeXaxis" : "S,M,T,W,T,F,S",
   "last7DaysTransactionLabel" : "Last 7 day transaction",
   "last7DaysTransactionXaxis" : "S,M,T,W,T,F,S",
   
   "merchantDetailsCategories" : "General Details,Address Details,PAN & Bank Details",
   
   "settlementsTitleLabel" : "Settlements",
   "mtSummaryLabel" : "Summary",
   "todaySettlementsLael" : "Today’s Settlements",
   "totalSettlementsLabel" : "Total Settlements",
   "settlementsLabel" : "Settlements",
   "seeYourRecentSettlementLabel" : "See your recent settlements",
   "mtSelectDateButtonText" : "Select Date"
   
   
 }
 
 */



/*
 func bb() {
     let arr = [8, 1, 8]

     let k = arr[0]
     let occupied = arr.dropFirst()
     let row = Int(k / 2)

     var seats = [Bool]()
     var x = 0


     for i in 0..<row {
         var full_seat = false
         for j in 1...2 {
             if (occupied.contains(x+1))  {
                 full_seat = true
             } else {
                 full_seat = false
             }
             seats.insert(full_seat, at: x)
             x += 1
         }
     }
     
     var seating = 0
     for (index, seat) in seats.enumerated() {
         var i = 0
         if (index == 0 || (index % 2 == 0)) && !seat {
             
             for (index2, seat2) in seats.enumerated() {
                 if index2 <= index {
                     continue
                 }
                 if i == 2 {
                     break
                 }
                 if !seat2 {
                     seating += 1
                 }
                 i += 1
             }
         } else if  index != 0 && (index % 2 != 0) && !seat {
             for (index2, seat2) in seats.enumerated() {
                 if index2 <= index {
                     continue
                 }
                 if i == 0 {
                     i += 1
                     continue
                 }
                 if i == 2 {
                     break
                 }
                 if !seat2 {
                     seating += 1
                 }
                 i += 1
             }
         }
     }
     
     print(seating)
 }
 func aa(){
     let str = "1abcbbbaa"
     let k = Int(String(str.prefix(1))) ?? 0
     let strX = str.dropFirst()

     var output = ""
     //var outputStrCollection = [String]()


     for (startIndex, _) in strX.enumerated() {
       var opt = ""
       var sets : Set<String> = []
       //print("index \(index), element \(char)\n")
       //strCollection.append(String(char))

         for (startIndex2, char2) in strX.enumerated() {
           if startIndex2 == startIndex {
             if opt == ""  { opt = String(char2)
                 sets.insert(String(char2))
             }
             continue
           }
             if startIndex2 < startIndex {
               continue
             }
           sets.insert(String(char2))
           if sets.count > k {
             break
           } else {
           opt += String(char2) }
         }
         if output.count == opt.count {
           //outputStrCollection.append(opt)
         } else if output.count < opt.count {
           //outputStrCollection = [String]()
             //sets = []
           output = opt
         }
     }


     print("Output : \(output)")
     print(strX)
     print("value is \(k)")
 }
 
 
 
 var firstArr = [String]()
var secondArr = [String]()
var thirdArr = [String]()
var fourthArr = [String]()
var fifthArr = [String]()
var sixthArr = [String]()

for (index, element) in strArr.enumerated() {
if element == "<>" {
  continue
}
if index == 0 {
  firstArr.append(element)
} else if index == 1 {
  secondArr.append(element)
} else if index == 2 {
  thirdArr.append(element)
} else if index == 3 {
  fourthArr.append(element)
} else if index == 4 {
  fifthArr.append(element)
} if index == 5 {
  sixthArr.append(element)
}

print(index, element)

}
print(firstArr)
print(secondArr)
print(thirdArr)
print(fourthArr)
print(fifthArr)
print(sixthArr)
 */


/*
 {
     body =     {
         androidGrace = "2021-05-21 16:01:48";
         androidVer = 1;
         apiEndpoint = 3;
         appLabel = 3;
         appSalt = abcd;
         bankMaster = 3;
         faqData = 3;
         iosGrace = "2021-05-21 16:02:01";
         iosVer = 1;
         lastUpdated = "2021-05-31 03:45:40";
         masterDelay = 7;
         masterNextDate = "2021-05-28 16:02:18";
         merchantBank = "<null>";
         merchantVersion = "<null>";
         otpReValidate = Y;
         otpValidity = 60;
         pspBank =         (
                         {
                 bankCode = INDB;
                 bankName = "IndusInd Bank Limited";
                 bankStatus = Active;
                 bankVA = "@cscindus";
             },
                         {
                 bankCode = ICIC;
                 bankName = "ICICI Bank Limited";
                 bankStatus = InActive;
                 bankVA = "@cscicici";
             }
         );
         pspVersion = 3;
     };
     head =     {
         appName = "CSC Pay";
         deviceId = "61A909E7-1701-4C8C-BD47-5003BEE9AE66";
         refId = 999989487020210604161650;
         resAction = config;
         respUID = "2e5bdb91-c522-11eb-8b35-1e00be0004e4";
         ts = "2021-06-04T16:16:56";
     };
 }
 */
