//
//  DeviceID.swift
//  VLE Survey
//
//  Created by Gowtham on 09/10/20.
//  Copyright © 2020 CSC eGovernance. All rights reserved.
//

import Foundation
import UIKit

/// Creates a new unique user identifier or retrieves the last one created
func getUUID() -> String? {

    // create a keychain helper instance
    let keychain = KeychainAccess()

    // this is the key we'll use to store the uuid in the keychain
    let uuidKey = "com.myorg.myappid.unique_uuid"

    // check if we already have a uuid stored, if so return it
    if let uuid = try? keychain.queryKeychainData(itemKey: uuidKey), uuid != "" && uuid != nil {
        return uuid
    }

    // generate a new id
    guard let newId = UIDevice.current.identifierForVendor?.uuidString else {
        return nil
    }

    // store new identifier in keychain
    try? keychain.addKeychainData(itemKey: uuidKey, itemValue: newId)

    // return new id
    return newId
}

class KeychainAccess {

    func addKeychainData(itemKey: String, itemValue: String) throws {
        guard let valueData = itemValue.data(using: .utf8) else {
            print("Keychain: Unable to store data, invalid input - key: \(itemKey), value: \(itemValue)")
            return
        }

        //delete old value if stored first
        do {
            try deleteKeychainData(itemKey: itemKey)
        } catch {
            print("Keychain: nothing to delete...")
        }

        let queryAdd: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject,
            kSecValueData as String: valueData as AnyObject,
            kSecAttrAccessible as String: kSecAttrAccessibleWhenUnlocked
        ]
        let resultCode: OSStatus = SecItemAdd(queryAdd as CFDictionary, nil)

        if resultCode != 0 {
            print("Keychain: value not added - Error: \(resultCode)")
        } else {
            print("Keychain: value added successfully")
        }
    }

    func deleteKeychainData(itemKey: String) throws {
        let queryDelete: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject
        ]

        let resultCodeDelete = SecItemDelete(queryDelete as CFDictionary)

        if resultCodeDelete != 0 {
            print("Keychain: unable to delete from keychain: \(resultCodeDelete)")
        } else {
            print("Keychain: successfully deleted item")
        }
    }

    func queryKeychainData (itemKey: String) throws -> String? {
        let queryLoad: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject,
            kSecReturnData as String: kCFBooleanTrue,
            kSecMatchLimit as String: kSecMatchLimitOne
        ]
        var result: AnyObject?
        let resultCodeLoad = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(queryLoad as CFDictionary, UnsafeMutablePointer($0))
        }

        if resultCodeLoad != 0 {
            print("Keychain: unable to load data - \(resultCodeLoad)")
            return nil
        }

        guard let resultVal = result as? NSData, let keyValue = NSString(data: resultVal as Data, encoding: String.Encoding.utf8.rawValue) as String? else {
            print("Keychain: error parsing keychain result - \(resultCodeLoad)")
            return nil
        }
        return keyValue
    }
}

func registerApp() -> Bool?{
    guard let hmac = APIManager.RegisterAppServices()else{
        return false
    }
    let result = UPI.registerAppHmac(withHmac: hmac)
    return result
}

func getToken(status : Bool = false) {
    APIManager.call(GetTokenResponse.self, urlString: "indus/apptoken", reqAction : "getToken",category: "IndusInd", pspId: "IBL", extra: "GET_TOKEN") { (res, err) in
        if err == nil{
            if !status{
                if let response = res , let list = response.listKeys{
                    print(response)
                    GlobalData.sharedInstance.setToken = res
                    CoredataManager.saveNpciTokenListKey(getNpciData: response)
                }
            }else{
                let localNpciData =  APIManager.loadAllJson(GetTokenResponse.self, filename: "npci")
                print(localNpciData)
                GlobalData.sharedInstance.setToken = res
                CoredataManager.saveNpciTokenListKey(getNpciData: localNpciData!)
            }
        }else{
           if let npciData = CoredataManager.getDetail(NPCI_Data.self){
               if let listKey = CoredataManager.getDetail(List_Key.self){
                   print(npciData , listKey)
               }
           }else{
               let localNpciData =  APIManager.loadAllJson(GetTokenResponse.self, filename: "npci")
               CoredataManager.saveNpciTokenListKey(getNpciData: localNpciData!)
           }
        }
    }
}
