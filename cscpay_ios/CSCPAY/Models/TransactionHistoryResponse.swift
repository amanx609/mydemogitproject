//
//  TransactionHistoryResponse.swift
//  CSCPAY
//
//  Created by Aman Pandey on 02/08/21.
//

import Foundation


struct TransactionHistoryResponse : Decodable{
    let encKey :String?
    let isMerchant : Bool?
    let pspRefNo : String?
    let resHash : String?
    let status : String?
    let statusDesc : String?
    let transDetails :[TransactionHistoryDetails]?
    
    enum CodingKeys : String , CodingKey{
        case encKey , pspRefNo , resHash , status , statusDesc , isMerchant , transDetails
    }
    
}

struct TransactionHistoryDetails : Decodable{
    let custRefNo : String?
    let drCrFlag : String?
    let payType : String?
    let payeeAccountNo : String?
    let payeeBankCode : String?
    let payeeIfsc : String?
    let payeeMaskAccountNo : String?
    let payeeName : String?
    let payeeVirtualAddress: String?
    let payerAccountNo : String?
    let payerBankCode : String?
    let payerIfsc : String?
    let payerName : String?
    let payerVirtualAddress : String?
    let paymentmethod : String?
    let reason_desc : String?
    let  refFlag : String?
    let trnDate : String?
    let trnRefNo : Int?
    let txnAmount : String?
    let txnNote : String?
    let txnStatus : String?
    let txnStatusDesc : String?
    let txnType : String?
    
    enum CodingKeys: String, CodingKey {
        case custRefNo  ,trnRefNo
        case drCrFlag , payType , payeeAccountNo , payeeBankCode , payeeIfsc , payeeMaskAccountNo , payeeName , payeeVirtualAddress , payerAccountNo , payerBankCode , payerIfsc , payerName , payerVirtualAddress , paymentmethod , reason_desc , refFlag , trnDate , txnAmount , txnNote , txnStatusDesc ,txnStatus , txnType
    } 
}
