//
//  ApiResponse.swift
//  CSCPAY
//
//  Created by Gowtham on 21/05/21.
//

import Foundation

struct APIResponse : Decodable
{
     enum CodingKeys : String, CodingKey {
        case resCode = "resCode"
        case resMsg = "resMsg"
        case resData = "resData"
    }
    var resCode : String?
    var resMsg : String?
    var resData : String?
}

struct RuntimeError: Error {
    let message: String
    var code : String

    init(_ code: String, _ message: String) {
        self.code    = code
        self.message = message
    }

    public var localizedDescription: String {
        return message
    }
}
