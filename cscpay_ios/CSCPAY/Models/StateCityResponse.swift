//
//  StateCityRespoonse.swift
//  CSCPAY
//
//  Created by Aman Pandey on 18/08/21.
//

import Foundation
struct StateResponse: Decodable {
    let sno, state, stateShort, district: String
    let activeStatus: String

    enum CodingKeys: String, CodingKey {
        case sno, state
        case stateShort = "state_short"
        case district
        case activeStatus = "active_status"
    }
}

typealias State = [StateResponse]
