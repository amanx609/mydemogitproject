//
//  File.swift
//  CSCPAY
//
//  Created by Gowtham on 04/06/21.
//

import Foundation
import UIKit

class ConfigResponse : Decodable {
    private enum CodingKeys : String, CodingKey {
        case head = "head"
        case body = "body"
    }
    var head : ConfigHead?
    var body : ConfigBody?
}

class ConfigHead : Decodable {
    private enum CodingKeys : String, CodingKey {
        case appName = "appName"
        case deviceId = "deviceId"
        case refId = "refId"
        case resAction = "resAction"
        case respUID = "respUID"
        case ts = "ts"
    }
    var appName : String?
    var deviceId : String?
    var refId : String?
    var resAction : String?
    var respUID : String?
    var ts : String?
}
class ConfigBody : Decodable {
    private enum CodingKeys : String, CodingKey {
        case androidGrace = "androidGrace"
        case androidVer = "androidVer"
        case apiEndpoint = "apiEndpoint"
        case appLabel = "appLabel"
        case appSalt = "appSalt"
        case bankMaster = "bankMaster"
        case faqData = "faqData"
        case iosGrace = "iosGrace"
        case iosVer = "iosVer"
        case lastUpdated = "lastUpdated"
        case masterDelay = "masterDelay"
        case masterNextDate = "masterNextDate"
        case merchantBank = "merchantBank"
        case merchantVersion = "merchantVersion"
        case otpReValidate = "otpReValidate"
        case otpValidity = "otpValidity"
        case pspVersion = "pspVersion"
        case pspBank = "pspBank"
    }
    var androidGrace : String?
    var androidVer : String?
    var apiEndpoint : String?
    var appLabel : String?
    var appSalt : String?
    var bankMaster : String?
    var faqData: String?
    var iosGrace : String?
    var iosVer : String?
    var lastUpdated : String?
    var masterDelay : String?
    var masterNextDate : String?
    var merchantBank : String?
    var merchantVersion: String?
    var otpReValidate: String?
    var otpValidity:String?
    var pspVersion: String?
    var pspBank:[PSPBank]?
}

class PSPBank : Decodable {
    private enum CodingKeys : String, CodingKey {
        case bankCode = "bankCode"
        case bankName = "bankName"
        case bankStatus = "bankStatus"
        case bankVA = "bankVA"
    }
    var bankCode : String?
    var bankName : String?
    var bankStatus : String?
    var bankVA : String?
}





//{
//    body :     {
//        androidGrace : "2021-05-21 16:01:48";
//        androidVer : 1;
//        apiEndpoint : 3;
//        appLabel : 3;
//        appSalt : abcd;
//        faqData : 3;
//        iosGrace : "2021-05-21 16:02:01";
//        iosVer : 1;
//        lastUpdated : "2021-12-19 17:13:43";
//        merchantBank : indus;
//        merchantVersion : 14;
//        otpReAuth : 30;
//        otpValidity : 60;
//    };
//    head :     {
//        appName : "CSC Pay";
//        appVer : "1.0";
//        bluetoothMac : "02:00:00:00:00:00";
//        clientIp : "192.168.2.12";
//        deviceId : "F379D777-BE8F-40F3-8187-6081BDBDEAED";
//        deviceType : iOS;
//        geoLat : "72.223";
//        geoLong : "77.221";
//        location : Delhi;
//        osVer : "15.1";
//        refId : 987654321020211220204749;
//        reqAction : config;
//        simId : 9876543210987654320;
//        ts : "2021-12-20T20:47:49";
//        wifiMac : "02:00:00:00:00:00";
//    };
//}
