//
//  BankInfo.swift
//  CSCPAY
//
//  Created by Gowtham on 01/07/21.
//

import Foundation

class BankList : Decodable {
    private enum CodingKeys : String, CodingKey {
        case bankMasterList = "bankMasterList"
        case status = "status"
        case statusDesc = "statusDesc"
    }
    var bankMasterList : [BankInfo]?
    var status: String?
    var statusDesc : String?
}

class BankInfo : Decodable {
    private enum CodingKeys : String, CodingKey {
        case bankCode = "bankCode"
        case bankId = "bankId"
        case bankName = "bankName"
        case ifsc = "ifsc"
        case iin = "iin"
        case statusCode = "statusCode"
    }
    var bankCode : String?
    var bankId: Int?
    var bankName : String?
    var ifsc : String?
    var iin: Int?
    var statusCode : Int?
}

class secQuesList : Decodable {
    private enum CodingKeys : String, CodingKey {
        case secQuesList = "secQuesList"
        case status = "status"
        case statusDesc = "statusDesc"
    }
    var secQuesList : [QuestionInfo]?
    var status: String?
    var statusDesc : String?
}

class QuestionInfo : Decodable {
    private enum CodingKeys : String, CodingKey {
        case quesId = "quesId"
        case quesName = "quesName"
    }
    var quesId : Int?
    var quesName: String?
}
