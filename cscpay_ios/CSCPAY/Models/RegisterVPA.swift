//
//  RegisterVPA.swift
//  CSCPAY
//
//  Created by Gowtham on 07/07/21.
//

import Foundation

class RegisterVPA : Decodable {
    private enum CodingKeys : String, CodingKey {
        case customerId = "customerId"
        case isMerchant = "isMerchant"
        case merchantFlag = "merchantFlag"
        case pspRefNo = "pspRefNo"
        case pspRespRefNo = "pspRespRefNo"
        case regDate = "regDate"
        case resHash = "resHash"
        case status = "status"
        case statusDesc = "statusDesc"
        case userInfo = "userInfo"
        case virtualAddress = "virtualAddress"
    }
    var customerId : String?
    var isMerchant : Bool?
    var merchantFlag : Bool?
    var pspRefNo : String?
    var pspRespRefNo : String?
    var regDate : String?
    var resHash : String?
    var status : String?
    var statusDesc : String?
    var userInfo : UserInfo?
    var virtualAddress : String?
} 
class UserInfo : Decodable {
    private enum CodingKeys : String, CodingKey {
        case accountId = "accountId"
        case customerId = "customerId"
        case defVPAStatus = "defVPAStatus"
        case email = "email"
        case isMerchant = "isMerchant"
        case mobileNo = "mobileNo"
        case name = "name"
        case regDate = "regDate"
        case showMerchant = "showMerchant"
        case virtualAddress = "virtualAddress"
    }
    var accountId : String?
    var customerId : String?
    var defVPAStatus : Bool?
    var email : String?
    var isMerchant : Bool?
    var mobileNo : String?
    var name : String?
    var regDate : String?
    var showMerchant : Bool?
    var virtualAddress : String?
}
