//
//  CommonModel.swift
//  CSCPAY
//
//  Created by Aman Pandey on 01/12/21.
//

import Foundation

struct CommonModel{
    var mobileNo: String?
    var  legalName: String?
    var  bizName: String?
    var  bizGroup: String?
    var  bizDoReg: String?
    var bizNature: String?
    var  bizCategory: String?
    var  mcCode: String?
    var  address: String?
    var  city: String?
    var  stateCode: String?
    var  stateName: String?
    var  districtCode: String?
    var  districtName: String?
    var  authPerson: String?
    var  authContact: String?
    var  authKycType: String?
    var  authKycId: String?
    var  email: String?
    var  panNo: String?
    var  ifscCode: String?
    var  accountNo: String?
    var  gstin: String?
    var  registerId: String?
    var  docCount: String?
    var  docList: String?
    var  cscId: String?
    var  activeStatus : Int?
    var  pinCode : String?
    var  fcmDeviceId: String?
    
    var seqKey: String?
    var bizId: String?
    var docId: Int?
    var docType: String?
    var docName: String?
    var docMime: String?
    var docSize: Int?
    var docData: String?
    var bizHash: String?
    var checkSum : String?
}
  
       
       
       
       
