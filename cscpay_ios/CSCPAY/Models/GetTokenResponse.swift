//
//  GetTokenResponse.swift
//  CSCPAY
//
//  Created by Aman Pandey on 20/07/21.
//

import Foundation

class GetTokenResponse : Decodable{
    private enum CodingKeys : String , CodingKey{
        case listKeys           = "listKeys"
        case refId              = "refId"
        case refUrl             = "refUrl"
        case reqMsgId           = "reqMsgId"
        case status             = "status"
        case txnId              = "txnId"
        case xmlResp            = "xmlResp"
    }
    var listKeys : [ListKeys]?
    var refId : String?
    var refUrl : String?
    var reqMsgId : String?
    var status : String?
    var txnId  : String?
    var xmlResp : String?
    
    
}

class ListKeys : Decodable{
    private enum CodingKeys : String , CodingKey{
        case code      = "code"
        case keyValue  = "keyValue"
        case ki        = "ki"
        case owner     = "owner"
        case type      = "type"
    }
    var code : String?
    var keyValue : String?
    var ki : Int?
    var owner : String?
    var type : String?
}





//{
//    listKeys =     (
//                {
//            code = NPCI;
//            keyValue = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuMKxWfy0WcPp98muBWa6yhpmb6ZGZGSKHRIOv05UlIN5TbUPl6yEerh7Wj0+JyKfsOntRdAVhkLJGRoHwH6gEEeFNHge7kPea/B33cQAbqa39mnP5F1aaZT3tjJnKrfI1Wum0crdb7dAMzft4JILOEa+s3Uh7OdYEl/Xp7EisdSoJ345Cj0LTfLZEQzRdVGovXZrfLByJysH11V9tDrIVv75C/3UndwjHt3NrqzNBoUMh5VZRFkcwuebUAkhIed5gvoysJwd0yYGrAUXNrXJJDTAj5diCuasWyfWZR9lsX5l14hdxF+lqadR/pgII53DW5oEy2LMXgvt2u/qmSml8wIDAQAB";
//            ki = 20150822;
//            owner = NPCI;
//            type = PKI;
//        }
//    );
//    refId = INDF2765696E648408DB8B7955313DEB01E;
//    refUrl = "http://www.indusind.com";
//    reqMsgId = IND47219276D378483A92C9DBC6F3C99CD6;
//    status = SUCCESS;
//    txnId = INDUS92EEED859B3247fOzBAHm4ZXsQbxB1;
//    xmlResp = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48bnMyOlJlc3BMaXN0S2V5cyB4bWxuczpuczI9Imh0dHA6Ly9ucGNpLm9yZy91cGkvc2NoZW1hLyIgeG1sbnM6bnMzPSJodHRwOi8vbnBjaS5vcmcvY20vc2NoZW1hLyI+PEhlYWQgbXNnSWQ9IjFHUkRwZWdCYkE2OXpQY2NEdkVBIiBvcmdJZD0iTlBDSSIgdHM9IjIwMjAtMTEtMDdUMTA6Mzc6NDYrMDU6MzAiIHZlcj0iMi4wIi8+PFJlc3AgcmVxTXNnSWQ9IklORDQ3MjE5Mjc2RDM3ODQ4M0E5MkM5REJDNkYzQzk5Q0Q2IiByZXN1bHQ9IlNVQ0NFU1MiLz48VHhuIGlkPSJJTkRVUzkyRUVFRDg1OUIzMjQ3Zk96QkFIbTRaWHNRYnhCMSIgbm90ZT0iTGlzdCBLZXlzIiByZWZJZD0iSU5ERjI3NjU2OTZFNjQ4NDA4REI4Qjc5NTUzMTNERUIwMUUiIHJlZlVybD0iaHR0cDovL3d3dy5pbmR1c2luZC5jb20iIHRzPSIyMDIwLTExLTA3VDEwOjM0OjMxKzA1OjMwIiB0eXBlPSJMaXN0S2V5cyIvPjxrZXlMaXN0PjxrZXkgY29kZT0iTlBDSSIga2k9IjIwMTUwODIyIiBvd25lcj0iTlBDSSIgdHlwZT0iUEtJIj48a2V5VmFsdWUgeG1sbnM6eHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4c2k6dHlwZT0ieHM6c3RyaW5nIj5NSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQXVNS3hXZnkwV2NQcDk4bXVCV2E2eWhwbWI2WkdaR1NLSFJJT3YwNVVsSU41VGJVUGw2eUVlcmg3V2owK0p5S2ZzT250UmRBVmhrTEpHUm9Id0g2Z0VFZUZOSGdlN2tQZWEvQjMzY1FBYnFhMzltblA1RjFhYVpUM3RqSm5LcmZJMVd1bTBjcmRiN2RBTXpmdDRKSUxPRWErczNVaDdPZFlFbC9YcDdFaXNkU29KMzQ1Q2owTFRmTFpFUXpSZFZHb3ZYWnJmTEJ5SnlzSDExVjl0RHJJVnY3NUMvM1VuZHdqSHQzTnJxek5Cb1VNaDVWWlJGa2N3dWViVUFraEllZDVndm95c0p3ZDB5WUdyQVVYTnJYSkpEVEFqNWRpQ3Vhc1d5ZldaUjlsc1g1bDE0aGR4RitscWFkUi9wZ0lJNTNEVzVvRXkyTE1YZ3Z0MnUvcW1TbWw4d0lEQVFBQjwva2V5VmFsdWU+PC9rZXk+PC9rZXlMaXN0PjxTaWduYXR1cmUgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxTaWduZWRJbmZvPjxDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvVFIvMjAwMS9SRUMteG1sLWMxNG4tMjAwMTAzMTUiLz48U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxkc2lnLW1vcmUjcnNhLXNoYTI1NiIvPjxSZWZlcmVuY2UgVVJJPSIiPjxUcmFuc2Zvcm1zPjxUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjwvVHJhbnNmb3Jtcz48RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxlbmMjc2hhMjU2Ii8+PERpZ2VzdFZhbHVlPlJBMEZxUWZTclMzcC92NzRPV25BMUZnRmpNbDRqcmhZVW1KU25yZnZtWTg9PC9EaWdlc3RWYWx1ZT48L1JlZmVyZW5jZT48L1NpZ25lZEluZm8+PFNpZ25hdHVyZVZhbHVlPk1oL0h6YTcrZFJBSkhsbjVPemRvUTRnUDJ5WXlIUGNXcUVIMzVCQnBuU3pIT1ZiUU93emRPVUhFRUt1Um1ZMkhPUmdOQ0N3ZUhkY1IKZnkwQVNxb3RUMkRwVlc4YjNOMHNTYS8zRDc2Y2pBQ1NmKzFGNVhiem1uOXhEWWt2UWpFamRrMktRbm9tb24zZWRqbVdIZlNXYzlaSQpQWTZWeWR0NzdvaFp3N3o0blJrUktIdzNEYXp6bktKN2R6ZUp0TExmNzdlRG1DVU9wVE16S2hYckJoL21CL2UyY0xZM3Yzc3QzUUZ0CmFDOFlldEczMklOd2xIWXVPSTVNd0toOGFJeGpVTmdDdW0vS0dFaldibzBrYUhCbWNCbGpOb2FHM3l2SjBVcEPFNpZ25hdHVyZVZhbHVlPk1oL0h6YTcrZFJBSkhsbjVPemRvUTRnUDJ5WXlIUGNXcUVIMzVCQnBuU3pIT1ZiUU93emRPVUhFRUt1Um1ZMkhPUmdOQ0N3ZUhkY1IKZnkwQVNxb3RUMkRwVlc4YjNOMHNTYS8zRDc2Y2pBQ1NmKzFGNVhiem1uOXhEWWt2UWpFamRrMktRbm9tb24zZWRqbVdIZlNXYzlaSQpQWTZWeWR0NzdvaFp3N3o0blJrUktIdzNEYXp6bktKN2R6ZUp0TExmNzdlRG1DVU9wVE16S2hYckJoL21CL2UyY0xZM3Yzc3QzUUZ0CmFDOFlldEczMklOd2xIWXVPSTVNd0toOGFJeGpVTmdDdW0vS0dFaldibzBrYUhCbWNCbGpOb2FHM3l2SjBVcEkrQ3ZGa0NnZ2xvdjgKMit0SkFlZ3U1WXYxRUtpU0h2SHNkcGN6WXdHaXlTbVB6ejRMWUE9PTwvU2lnbmF0dXJlVmFsdWU+PEtleUluZm8+PEtleVZhbHVlPjxSU0FLZXlWYWx1ZT48TW9kdWx1cz51TUt4V2Z5MFdjUHA5OG11QldhNnlocG1iNlpHWkdTS0hSSU92MDVVbElONVRiVVBsNnlFZXJoN1dqMCtKeUtmc09udFJkQVZoa0xKCkdSb0h3SDZnRUVlRk5IZ2U3a1BlYS9CMzNjUUFicWEzOW1uUDVGMWFhWlQzdGpKbktyZkkxV3VtMGNyZGI3ZEFNemZ0NEpJTE9FYSsKczNVaDdPZFlFbC9YcDdFaXNkU29KMzQ1Q2owTFRmTFpFUXpSZFZHb3ZYWnJmTEJ5SnlzSDExVjl0RHJJVnY3NUMvM1VuZHdqSHQzTgpycXpOQm9VTWg1VlpSRmtjd3VlYlVBa2hJZWQ1Z3ZveXNKd2QweVlHckFVWE5yWEpKRFRBajVkaUN1YXNXeWZXWlI5bHNYNWwxNGhkCnhGK2xxYWRSL3BnSUk1M0RXNW9FeTJMTVhndnQydS9xbVNtbDh3PT08L01vZHVsdXM+PEV4cG9uZW50PkFRQUI8L0V4cG9uZW50PjwvUlNBS2V5VmFsdWU+PC9LZXlWYWx1ZT48L0tleUluZm8+PC9TaWduYXR1cmU+PC9uczI6UmVzcExpc3RLZXlzPg==";
//}
