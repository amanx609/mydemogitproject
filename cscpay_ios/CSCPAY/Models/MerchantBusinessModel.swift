//
//  MerchantBusinessModel.swift
//  CSCPAY
//
//  Created by Aman Pandey on 29/11/21.
//

import Foundation




struct BusinessCategory{
    let businessCategoryName : String
    let businessCategoryCode : Int
}

//struct BusinessType{
//    let businessTypeName: String
//    let businessTypeCode : Int
//
//}


struct BusinessNature : Decodable{
    let mcName , mcCode : String
    enum CodingKeys: String, CodingKey {
        case mcName = "mcName"
        case mcCode = "mcCode"
    }
}


struct BusinessType : Decodable{
    let name , code : String
    
    enum CodingKeys : String , CodingKey{
        case name = "name"
        case code = "code"
    }
}



typealias BusinessN = [BusinessNature]
typealias BusinessT = [BusinessType]


