//
//  MpinResponse.swift
//  CSCPAY
//
//  Created by Aman Pandey on 15/07/21.
//

import Foundation

class MpinResponse : Decodable {
    private enum CodingKeys : String, CodingKey {
        case head = "head"
        case body = "body"
        case cred = "cred"
    }
    var head : Head?
    var body : Body?
    var cred : Cred?
}

