//
//  Dispute.swift
//  CSCPAY
//
//  Created by Gowtham on 08/07/21.
//

import Foundation

// Get Dispute List
class DisputeList : Decodable {
    private enum CodingKeys : String, CodingKey {
        case listofCustDispute = "listofCustDispute"
        case pspRefNo = "pspRefNo"
        case status = "status"
        case statusDesc = "statusDesc"
        case upiTranRefNo = "upiTranRefNo"
        case resolveDate = "resolveDate"
        case ticketNo = "ticketNo"
        case disputeRemark = "disputeRemark"
        case disputeStatus = "disputeStatus"
    }
    var listofCustDispute : [ListofCustDispute]?
    var pspRefNo : String?
    var status : String?
    var statusDesc : String?
    var upiTranRefNo : Int?
    var resolveDate : String?
    var ticketNo : String?
    var disputeRemark: String?
    var disputeStatus: String?
}
class ListofCustDispute : Decodable {
    private enum CodingKeys : String, CodingKey {
        case amount = "amount"
        case crtDate = "crtDate"
        case custRefNo = "custRefNo"
        case custid = "custid"
        case fvaddr = "fvaddr"
        case pgMeTrnRefNo = "pgMeTrnRefNo"
        case reasonDesc = "reasonDesc"
        case reasonId = "reasonId"
        case responseDesc = "responseDesc"
        case status = "status"
        case storeId = "storeId"
        case targetDate = "targetDate"
        case ticketNo = "ticketNo"
    }
    var amount : String?
    var crtDate : String?
    var custRefNo : String?
    var custid : Int?
    var fvaddr : String?
    var pgMeTrnRefNo : Int?
    var reasonDesc : String?
    var reasonId : Int?
    var responseDesc : String?
    var status : String?
    var storeId : Int?
    var targetDate : String?
    var ticketNo : Int?
}

