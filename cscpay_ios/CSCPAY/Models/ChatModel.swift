//
//  ChatModel.swift
//  CSCPAY
//
//  Created by Aman Pandey on 25/08/21.
//

import Foundation


class Chat{
    var type: String?
    var message: Message?
}

class Message {
    var message: String?
    var options: [Option]?
}
class Option{
    var option: String?
    var key: String?
    var type: String?
}


class Invited{
    var invited: Bool?
}



//override var intrinsicContentSize: CGSize {
//   setNeedsLayout()
//   layoutIfNeeded()
//   let height = min(contentSize.height, maxHeight)
//   return CGSize(width: contentSize.width, height: height)
//}


