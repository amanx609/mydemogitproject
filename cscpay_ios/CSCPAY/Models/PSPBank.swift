//
//  PSPBank.swift
//  CSCPAY
//
//  Created by Gowtham on 07/07/21.
//

import Foundation

class PSPBankResponse : Decodable {
    private enum CodingKeys : String, CodingKey {
        case head = "head"
        case body = "body"
    }
    var head : PSPBankHead?
    var body : PSPBankBody?
}

class PSPBankHead : Decodable {
    private enum CodingKeys : String, CodingKey {
        case appName = "appName"
        case deviceId = "deviceId"
        case refId = "refId"
        case resAction = "resAction"
        case respUID = "respUID"
        case ts = "ts"
    }
    var appName : String?
    var deviceId : String?
    var refId : String?
    var resAction : String?
    var respUID : String?
    var ts : String?
}
class PSPBankBody : Decodable {
    private enum CodingKeys : String, CodingKey {
        case dat = "dat"
        case meta = "meta"
        case version = "version"
    }
    var dat : [PSPBankData]?
    var meta :String?
    var version: String?
}

class PSPBankData : Decodable {
    private enum CodingKeys : String, CodingKey {
        case activeStatus = "activeStatus"
        case bankCode = "bankCode"
        case bankName = "bankName"
        case bankVPA = "bankVPA"
        case commonData = "commonData"
        case merchantModule = "merchantModule"
        case npciToken = "npciToken"
        case pspModule = "pspModule"
        case sno = "sno"
    }
    var activeStatus : String?
    var bankCode : String?
    var bankName : String?
    var bankVPA : String?
    var commonData : [String]?
    var merchantModule : [String]?
    var npciToken: String?
    var pspModule: [String]?
    var sno : String?
}
