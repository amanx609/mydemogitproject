//
//  App_Cred+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Gowtham on 06/07/21.
//
//

import Foundation
import CoreData


extension App_Cred {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<App_Cred> {
        return NSFetchRequest<App_Cred>(entityName: "App_Cred")
    }

    @NSManaged public var aes: String?
    @NSManaged public var deviceId: String?
    @NSManaged public var hashToken: String?
    @NSManaged public var hmac: String?

}

extension App_Cred : Identifiable {

}
