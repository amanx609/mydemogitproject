//
//  App_Config_pspBank+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Gowtham on 05/07/21.
//
//

import Foundation
import CoreData


extension App_Config_pspBank {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<App_Config_pspBank> {
        return NSFetchRequest<App_Config_pspBank>(entityName: "App_Config_pspBank")
    }

    @NSManaged public var bankCode: String?
    @NSManaged public var bankName: String?
    @NSManaged public var bankStatus: String?
    @NSManaged public var bankVA: String?
    @NSManaged public var pspVersion: String?

}

extension App_Config_pspBank : Identifiable {

}
