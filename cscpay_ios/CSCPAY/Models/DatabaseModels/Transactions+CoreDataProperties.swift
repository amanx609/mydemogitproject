//
//  Transactions+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Gowtham on 22/06/21.
//
//

import Foundation
import CoreData


extension Transactions {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Transactions> {
        return NSFetchRequest<Transactions>(entityName: "Transactions")
    }

    @NSManaged public var addedAt: Date?
    @NSManaged public var amount: Double
    @NSManaged public var emoji: String?
    @NSManaged public var from: String?
    @NSManaged public var id: Int64
    @NSManaged public var remark: String?
    @NSManaged public var status: String?
    @NSManaged public var to: String?
    @NSManaged public var toName: String?
    @NSManaged public var type: String?
    @NSManaged public var expiry: Date?

}

extension Transactions : Identifiable {

}
