//
//  MerchantConfigResponse.swift
//  CSCPAY
//
//  Created by Aman Pandey on 20/12/21.
//

import Foundation

class MerchantConfigResponse : Decodable {
    private enum CodingKeys : String, CodingKey {
        case head = "head"
        case body = "body"
    }
    var head : MerchantConfigHead?
    var body : MerchantConfigBody?
}

class MerchantConfigHead : Decodable {
    private enum CodingKeys : String, CodingKey {
        case appName = "appName"
        case deviceId = "deviceId"
        case refId = "refId"
        case resAction = "resAction"
        case respUID = "respUID"
        case ts = "ts"
    }
    var appName : String?
    var deviceId : String?
    var refId : String?
    var resAction : String?
    var respUID : String?
    var ts : String?
}
class MerchantConfigBody : Decodable {
    private enum CodingKeys : String, CodingKey {
        case androidGrace = "androidGrace"
        case androidVer = "androidVer"
        case apiEndpoint = "apiEndpoint"
        case appLabel = "appLabel"
        case appSalt = "appSalt"
        case faqData = "faqData"
        case iosGrace = "iosGrace"
        case iosVer = "iosVer"
        case lastUpdated = "lastUpdated"
        case merchantBank = "merchantBank"
        case merchantVersion = "merchantVersion"
        case otpReAuth = "otpReAuth"
        case otpValidity = "otpValidity"
    }
    var androidGrace : String?
    var androidVer : String?
    var apiEndpoint : String?
    var appLabel : String?
    var appSalt : String?
    var faqData: String?
    var iosGrace : String?
    var iosVer : String?
    var lastUpdated : String?
    var merchantBank : String?
    var merchantVersion: String?
    var otpReAuth: String?
    var otpValidity:String?
}



class BankIfscResponse : Decodable{
    var bankBody : BankIfscBody?
    var bankHead : Head?

    private enum CodingKeys : String, CodingKey {
        case bankBody = "body"
        case bankHead = "head"
    }
    
}


class BankIfscBody: Decodable{
    var bankName : String?
    var branchAddress : String?
    var branchCity : String?
    var branchName : String?
    var district : String?
    var ifscCode : String?
    var micrCode : String?
    var state : String?

    private enum CodingKeys : String, CodingKey {
        case bankName = "bankName"
        case branchAddress
        case branchCity
        case branchName
        case district
        case ifscCode
        case micrCode
        case state
    }
}
