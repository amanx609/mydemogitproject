//
//  App_Merchnat+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Aman Pandey on 23/07/21.
//
//

import Foundation
import CoreData


extension App_Merchnat {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<App_Merchnat> {
        return NSFetchRequest<App_Merchnat>(entityName: "App_Merchnat")
    }

    @NSManaged public var pgMerchantId: String?
    @NSManaged public var merchantName: String?
    @NSManaged public var merCategoryCode: String?
    @NSManaged public var panNo: String?
    @NSManaged public var gstin: String?
    @NSManaged public var udyogId: String?
    @NSManaged public var cityName: String?
    @NSManaged public var stateName: String?
    @NSManaged public var merchantType: String?
    @NSManaged public var integrationType: String?
    @NSManaged public var settleType: String?
    @NSManaged public var accountNo: Int64
    @NSManaged public var ifscCode: String?
    @NSManaged public var bankCode: String?
    @NSManaged public var legalName: String?
    @NSManaged public var virtualAddress: String?
    @NSManaged public var terminalId: String?

}

extension App_Merchnat : Identifiable {

}
