//
//  Merchant_Registration.swift
//  CSCPAY
//
//  Created by Aman Pandey on 15/12/21.
//

import Foundation


// MARK: - Welcome
struct MerchantRegistrationModel: Decodable {
    var cred : MerchantCred?
    var body : MerchantBody
    var head : MerchantHead
    
    enum CodingKeys: String, CodingKey {
        case head = "head"
        case body = "body"
        case cred = "cred"
    }
}

// MARK: - Body
struct MerchantBody: Decodable {
    var activeStatus: String?
    var legalName : String?
    var mobileNo: String?
    var panNo: String?
    var bizCategory: String?
    var mcCode : String?
    var bizId : String?
    var vpaId : String?
    var seqKey:String?
    var docId:Int?
    var docType:String?
    var docName:String?
    var docMime:String?
    var docSize:Int?
    var docData:String?
    var docCount : String?
    var docList : String?

    var bizHash:String?
    var bizName : String?
    var bizgroup : String?
    var bizDoReg : String?
    var address : String?
    var city : String?
    var stateCode : String?
    var stateName : String?
    var districtCode : String?
    var districtName : String?
    var authPerson : String?
    var authContact : String?
    var authKycType : String?
    var authKycId : String?
    var email : String?
    var ifscCode : String?
    var accountNo : String?
    var gstin : String?
    var registerId : String?
    var cscId : String?
    var fcmDeviceId : String?
    var pincode : String?
    var checkSum : String?

    
    enum CodingKeys: String, CodingKey {
        case activeStatus , legalName , panNo , mobileNo , bizCategory , mcCode , bizId , vpaId
        case seqKey , docId , docType , docName , docMime , docSize , docData , docCount , docList
        case bizHash , bizgroup , bizName , bizDoReg
        case address ,city ,stateCode , stateName , districtCode , districtName
        case authPerson , authContact , authKycType , authKycId
        case email , ifscCode, accountNo , gstin , registerId , cscId
        case fcmDeviceId , pincode , checkSum
    }
}

// MARK: - Head
struct MerchantHead: Decodable {
    var appName, deviceID: String?
    var refID: String?
    var resAction, ts: String?

    enum CodingKeys: String, CodingKey {
        case appName
        case deviceID = "deviceId"
        case refID = "refId"
        case resAction, ts
    }
}

class MerchantCred : Decodable {
    private enum CodingKeys : String, CodingKey {
        case aesKey = "aesKey"
        case hashToken = "hashToken"
        case hmac = "hmac"
    }
    var aesKey : String?
    var hashToken : String?
    var hmac : String?
}
