//
//  App_User+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Gowtham on 08/07/21.
//
//

import Foundation
import CoreData


extension App_User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<App_User> {
        return NSFetchRequest<App_User>(entityName: "App_User")
    }
    @NSManaged public var appPin: String?
    @NSManaged public var aesKey: String?
    @NSManaged public var appName: String?
    @NSManaged public var appVer: String?
    @NSManaged public var bluetoothMac: String?
    @NSManaged public var clientIp: String?
    @NSManaged public var deviceId: String?
    @NSManaged public var deviceType: String?
    @NSManaged public var dob: String?
    @NSManaged public var gender: String?
    @NSManaged public var geoLat: String?
    @NSManaged public var geoLong: String?
    @NSManaged public var hashToken: String?
    @NSManaged public var hmac: String?
    @NSManaged public var location: String?
    @NSManaged public var mail: String?
    @NSManaged public var mobile: String?
    @NSManaged public var name: String?
    @NSManaged public var osVer: String?
    @NSManaged public var refId: String?
    @NSManaged public var simId: String?
    @NSManaged public var ts: String?
    @NSManaged public var wifiMac: String?
    @NSManaged public var accountId: String?
    @NSManaged public var customerId: String?
    @NSManaged public var defVPAStatus: Bool
    @NSManaged public var email: String?
    @NSManaged public var isMerchant: Bool
    @NSManaged public var mobileNo: String?
    @NSManaged public var vpaName: String?
    @NSManaged public var regDate: String?
    @NSManaged public var showMerchant: Bool
    @NSManaged public var virtualAddress: String?
    
    
    
    
    
    

}

extension App_User : Identifiable {

}
