//
//  App_Config+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Gowtham on 06/07/21.
//
//

import Foundation
import CoreData


extension App_Config {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<App_Config> {
        return NSFetchRequest<App_Config>(entityName: "App_Config")
    }

    @NSManaged public var androidGrace: String?
    @NSManaged public var androidVer: String?
    @NSManaged public var apiEndpoint: String?
    @NSManaged public var appLabel: String?
    @NSManaged public var appSalt: String?
    @NSManaged public var bankMaster: String?
    @NSManaged public var deviceId: String?
    @NSManaged public var faqData: String?
    @NSManaged public var iosGrace: String?
    @NSManaged public var iosVer: String?
    @NSManaged public var lastUpdated: String?
    @NSManaged public var masterDelay: String?
    @NSManaged public var masterNextDate: String?
    @NSManaged public var merchantBank: String?
    @NSManaged public var merchantVersion: String?
    @NSManaged public var otpReValidate: String?
    @NSManaged public var otpValidity: String?
    @NSManaged public var pspBank: String?
    @NSManaged public var pspVersion: String?
    @NSManaged public var refId: String?
    @NSManaged public var respUID: String?
    @NSManaged public var ts: String?
    @NSManaged public var appStage: String?

}

extension App_Config : Identifiable {

}
