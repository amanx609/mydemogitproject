//
//  MerchantVPAResponse.swift
//  CSCPAY
//
//  Created by Aman Pandey on 20/12/21.
//

import Foundation

// MARK: - Welcome
struct MerchantVPAResponse: Decodable {
    let body: MerVPABody
    let head: MerchantHead
}

// MARK: - Body
struct MerVPABody: Decodable {
    let accountNo, activeStatus, address, authContact: String
    let authKycID, authKycType, authPerson, bizCategory: String
    let bizDoReg, bizGroup, bizID, bizName: String
    let city, cscID, districtCode, districtName: String
    let docCount, docList, email, gstConsentFlag: String
    let gstin, ifscCode, integrationType, legalName: String
    let mcCode, merchantID, merchantKey, merchantType: String
    let mobileNo, panNo, proVer, qrData: String
    let refID, registerID, reqType: String
    let requestUrl1, requestUrl2: String
    let seqKey, settleType, shardHint, stateCode: String
    let stateName, statusDesc, subMerchantID, terminalID: String
    let userShard, userShardLVM, virtualAddress, vpaID: String

    enum CodingKeys: String, CodingKey {
        case accountNo, activeStatus
        case address = "  address"
        case authContact
        case authKycID = "authKycId"
        case authKycType, authPerson, bizCategory
        case bizDoReg = " bizDoReg"
        case bizGroup = " bizGroup"
        case bizID = "    bizId"
        case bizName = "  bizName"
        case city = "     city"
        case cscID = "    cscId"
        case districtCode, districtName
        case docCount = " docCount"
        case docList = "  docList"
        case email = "    email"
        case gstConsentFlag
        case gstin = "    gstin"
        case ifscCode = " ifscCode"
        case integrationType, legalName
        case mcCode = "   mcCode"
        case merchantID = "merchantId"
        case merchantKey, merchantType
        case mobileNo = " mobileNo"
        case panNo = "    panNo"
        case proVer = "   proVer"
        case qrData = "   qrData"
        case refID = "    refId"
        case registerID = "registerId"
        case reqType = "  reqType"
        case requestUrl1, requestUrl2
        case seqKey = "   seqKey"
        case settleType, shardHint, stateCode, stateName, statusDesc
        case subMerchantID = "subMerchantId"
        case terminalID = "terminalId"
        case userShard, userShardLVM, virtualAddress
        case vpaID = "    vpaId"
    }
}

// MARK: - Head
//struct Head: Codable {
//    let appName, deviceID, refID, resAction: String
//    let ts: String
//
//    enum CodingKeys: String, CodingKey {
//        case appName
//        case deviceID = " deviceId"
//        case refID = "    refId"
//        case resAction
//        case ts = "       ts"
//    }
//}
