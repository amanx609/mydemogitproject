//
//  Merchant_Details+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Aman Pandey on 20/12/21.
//
//

import Foundation
import CoreData


extension Merchant_Details {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Merchant_Details> {
        return NSFetchRequest<Merchant_Details>(entityName: "Merchant_Details")
    }

    @NSManaged public var merchantDetails: String?

}

extension Merchant_Details : Identifiable {

}
