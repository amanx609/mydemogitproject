//
//  Master_BankList+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Gowtham on 02/07/21.
//
//

import Foundation
import CoreData


extension Master_BankList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Master_BankList> {
        return NSFetchRequest<Master_BankList>(entityName: "Master_BankList")
    }

    @NSManaged public var bankCode: String?
    @NSManaged public var bankId: Int64
    @NSManaged public var bankName: String?
    @NSManaged public var ifsc: String?
    @NSManaged public var iin: Int64
    @NSManaged public var statusCode: Int64

}

extension Master_BankList : Identifiable {

}
