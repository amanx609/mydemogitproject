//
//  CheckVPA.swift
//  CSCPAY
//
//  Created by Gowtham on 07/07/21.
//

import Foundation

class CheckVPA : Decodable {
    private enum CodingKeys : String, CodingKey {
        case pspRefNo = "pspRefNo"
        case regLevel = "regLevel"
        case resHash = "resHash"
        case status = "status"
        case statusDesc = "statusDesc"
        case virtualAddress = "virtualAddress"
    }
    var pspRefNo : String?
    var regLevel : String?
    var resHash : String?
    var status : String?
    var statusDesc : String?
    var virtualAddress : String?
}

