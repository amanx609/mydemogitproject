//
//  BankAccountsList.swift
//  CSCPAY
//
//  Created by Gowtham on 07/07/21.
//

import Foundation

class BankAccountsList : Decodable {
    private enum CodingKeys : String, CodingKey {
        case accountList = "accountList"
        case isMerchant = "isMerchant"
        case pspRefNo = "pspRefNo"
        case pspRespRefNo = "pspRespRefNo"
        case status = "status"
        case statusDesc = "statusDesc"
        case userInfo = "userInfo"
        case vpaSuggestion = "vpaSuggestion"
        case resHash = "resHash"
    }
    var accountList : [AccountList]?
    var isMerchant : Bool?
    var pspRefNo : String?
    var pspRespRefNo : String?
    var status : String?
    var statusDesc : String?
    var userInfo : BankAccountUserInfo?
    var vpaSuggestion : [String]?
    var resHash: String?
}

class AccountList : Decodable {
    private enum CodingKeys : String, CodingKey {
        case accId = "accId"
        case accountName = "accountName"
        case accountType = "accountType"
        case aeba = "aeba"
        case atmdLength = "atmdLength"
        case bankCode = "bankCode"
        case bankId = "bankId"
        case crdLength = "crdLength"
        case crdType = "crdType"
        case formatType = "formatType"
        case ifscCode = "ifscCode"
        case maskedAccountNumber = "maskedAccountNumber"
        case mpinFlag = "mpinFlag"
        case otpdLength = "otpdLength"
        case otpdType = "otpdType"
        case uPinLength = "uPinLength"
    }
    var accId : Int?
    var accountName : String?
    var accountType : String?
    var aeba : String?
    var atmdLength : Int?
    var bankCode : String?
    var bankId : Int?
    var crdLength : Int?
    var crdType : String?
    var formatType : String?
    var ifscCode : String?
    var maskedAccountNumber : String?
    var mpinFlag : String?
    var otpdLength : Int?
    var otpdType : String?
    var uPinLength : Int?
}

class BankAccountUserInfo : Decodable {
    private enum CodingKeys : String, CodingKey {
        case defVPAStatus = "defVPAStatus"
        case isMerchant = "isMerchant"
        case name = "name"
        case showMerchant = "showMerchant"
        case virtualAddress = "virtualAddress"
    }
    var defVPAStatus : Bool?
    var isMerchant : Bool?
    var name : String?
    var showMerchant : Bool?
    var virtualAddress : String?
}

