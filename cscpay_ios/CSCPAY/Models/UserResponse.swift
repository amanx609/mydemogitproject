//
//  UserDetials.swift
//  CSCPAY
//
//  Created by Gowtham on 30/06/21.
//

import Foundation

class UserResponse : Decodable {
    private enum CodingKeys : String, CodingKey {
        case head = "head"
        case body = "body"
        case info = "info"
        case vpa = "vpa"
    }
    var head : UserHead?
    var body : UserBody?
    var info : [String]?
    var vpa : [String]?
}

class UserHead : Decodable {
    private enum CodingKeys : String, CodingKey {
        case appName = "appName"
        case deviceId = "deviceId"
        case refId = "refId"
        case resAction = "resAction"
        //case respUID = "respUID"
        case ts = "ts"
    }
    var appName : String?
    var deviceId : String?
    var refId : String?
    var resAction : String?
   // var respUID : String?
    var ts : String?
}
class UserBody : Decodable {
    private enum CodingKeys : String, CodingKey {
        case mobileNo = "mobileNo"
        case userType = "userType"
        case userStatus = "userStatus"
    }
    var mobileNo : String?
    var userType : String?
    var userStatus : String?
}
