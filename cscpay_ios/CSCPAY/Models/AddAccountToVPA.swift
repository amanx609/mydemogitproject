//
//  AddAccountToVPA.swift
//  CSCPAY
//
//  Created by Gowtham on 08/07/21.
//

import Foundation

class AddAccountToVPA : Decodable {
    private enum CodingKeys : String, CodingKey {
        case accountId = "accountId"
        case aepsAccountId = "aepsAccountId"
        case isMerchant = "isMerchant"
        case merchantFlag = "merchantFlag"
        case pspRefNo = "pspRefNo"
        case resHash = "resHash"
        case status = "status"
        case statusDesc = "statusDesc"
        case userInfo = "userInfo"
    }
    var accountId : String?
    var aepsAccountId : String?
    var isMerchant : Bool?
    var merchantFlag : Bool?
    var pspRefNo : String?
    var resHash : String?
    var status : String?
    var statusDesc : String?
    var userInfo : AddAccountToVPAUserInfo?
}
class AddAccountToVPAUserInfo : Decodable {
    private enum CodingKeys : String, CodingKey {
        case accountId = "accountId"
        case defVPAStatus = "defVPAStatus"
        case isMerchant = "isMerchant"
        case showMerchant = "showMerchant"
    }
    var accountId : String?
    var defVPAStatus : Bool?
    var isMerchant : Bool?
    var showMerchant : Bool?
}

// Default Account
class SetDefaultAccount : Decodable {
    private enum CodingKeys : String, CodingKey {
        case pspRefNo = "pspRefNo"
        case recoveryFlag = "recoveryFlag"
        case status = "status"
        case statusDesc = "statusDesc"
        case updateFlag = "updateFlag"
    }
    var pspRefNo : String?
    var recoveryFlag : Int?
    var status : String?
    var statusDesc : String?
    var updateFlag : Int?
}
