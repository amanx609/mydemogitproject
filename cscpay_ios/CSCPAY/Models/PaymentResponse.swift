//
//  PaymentResponse.swift
//  CSCPAY
//
//  Created by Aman Pandey on 04/08/21.
//

import Foundation

struct PaymentResponse : Decodable{
    
    enum CodingKeys : String , CodingKey{
        case amount  , custRefNo , npciTransId
        case payeeVPA , payerVPA , pspRefNo , refId , responseCode
        case status , statusDesc , txnAuthDate , upiTransRefNo , approvalNumber
    }
    
    var custRefNo : String?
    var responseCode : String?
    var upiTransRefNo : Int?
    var approvalNumber : String?
    var amount : String?
    var npciTransId : String?
    var payeeVPA : String?
    var payerVPA : String?
    var pspRefNo : String?
    var refId : String?
    var status : String?
    var statusDesc : String?
    var txnAuthDate : String?
 
}
