//
//  CheckBalance.swift
//  CSCPAY
//
//  Created by Gowtham on 08/07/21.
//

import Foundation

class ConfirmBalance : Decodable {
    private enum CodingKeys : String, CodingKey {
        case accountId = "accountId"
        case accountInfo = "accountInfo"
        case npciTransId = "npciTransId"
        case pspRefNo = "pspRefNo"
        case status = "status"
        case statusDesc = "statusDesc"
    }
    var accountId : Int?
    var accountInfo : AccountInfo?
    var npciTransId : String?
    var pspRefNo : String?
    var status : String?
    var statusDesc : String?
}
class AccountInfo : Decodable {
    private enum CodingKeys : String, CodingKey {
        case accId = "accId"
        case accountBalance = "accountBalance"
        case accountName = "accountName"
        case bankId = "bankId"
        case ifscCode = "ifscCode"
        case ledgerBalance = "ledgerBalance"
        case maskedAccountNumber = "maskedAccountNumber"
        case uPinLength = "uPinLength"
    }
    var accId : Int?
    var accountBalance : String?
    var accountName : String?
    var bankId : Int?
    var ifscCode : String?
    var ledgerBalance : String?
    var maskedAccountNumber : String?
    var uPinLength : Int?
}


class InitiateBalance: Decodable {
    let cred: BalCred?
    let deviceId, mobileNo: String?
    let npciTranID: String?
    let payerType: PayerType?
    let pspRefNo, resHash, status, statusDesc: String?
    let tranactionNote: String?
    let upiTranRefNo: Int?
    let virtualAddress: String?
enum CodingKeys: String, CodingKey {
    case cred
    case deviceId = "deviceId"
    case mobileNo = "mobileNo "
    case npciTranID = "npciTranId"
    case payerType, pspRefNo, resHash, status, statusDesc, tranactionNote, upiTranRefNo, virtualAddress
}
}
// MARK: - Cred
class BalCred: Decodable {
    let atmCrdLength, credentialDataLength: Int?
    let credentialDataType: String?
    let otpCrdLength: Int?
}
// MARK: - PayerType
struct PayerType: Decodable {
    let accountNo: String?
    let isMerchant: Bool?
    let name, payerBankName: String?
    let defVPAStatus, showMerchant: Bool?
    let virtualAddress: String?
}


