//
//  InitPaymentResponse.swift
//  CSCPAY
//
//  Created by Aman Pandey on 03/08/21.
//


import Foundation

// MARK: - Welcome
struct InitPaymentResponse: Decodable {
    let amount: String?
    let cred: InitCred
    let payeeType: PayeeType
    let payerType: PayerType
    let pspRefNo, refID: String?
    let refURL: String?
    let resHash, status, statusDesc, tranactionNote: String?
    let upiTranRefNo: Int?
    let virtualAddress: String?

    enum CodingKeys: String, CodingKey {
        case amount, cred, payeeType, payerType, pspRefNo
        case refID = "refId"
        case refURL = "refUrl"
        case resHash, status, statusDesc, tranactionNote, upiTranRefNo, virtualAddress
    }
}

// MARK: - PayeeType
struct PayeeType: Decodable {
    let name, virtualAddress: String?
}
struct InitCred: Codable {
    let atmCrdLength, credentialDataLength: Int?
    let credentialDataType: String?
    let otpCrdLength: Int?
}


