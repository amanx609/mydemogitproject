//
//  UserBankDataModel.swift
//  CSCPAY
//
//  Created by Aman Pandey on 26/07/21.
//

import Foundation

// MARK: - P2P User Bank Details

struct UserBankDataModel: Decodable {
    let customerID, isMerchant, merchantFlag: Int?
    let pspRefNo: String?
    let pspRespRefNo: Int?
    let regDate, resHash, status, statusDesc: String?
    let userInfo: UserInfo?
    let virtualAddress: String?

    enum CodingKeys: String, CodingKey {
        case customerID = "customerId"
        case isMerchant, merchantFlag, pspRefNo, pspRespRefNo, regDate, resHash, status, statusDesc, userInfo, virtualAddress
    }
}



