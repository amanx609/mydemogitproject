//
//  MerchantResponse.swift
//  CSCPAY
//
//  Created by Aman Pandey on 13/07/21.
//

import Foundation

class MerchantResponse : Decodable {
    private enum CodingKeys : String, CodingKey {
        case head = "head"
        case body = "body"
        case cred = "cred"
    }
    var head : Head?
    var body : Body?
    var cred : Cred?
}

/*
{"head":{"refId":"74186129732021052112116","geoLat":"NA","ts":"2021-05-21T12:12:04","clientIp":"55.226.554.10","deviceId":"3A324B67-0805-4A5E-8803-8BCF66C30D78","osVer":"14.4","deviceType":"iOS","bluetoothMac":"na","geoLong":"NA","appName":"CSC Pay","wifiMac":"na","location":"na","appVer":"1.0","simId":"NA","reqAction":"generate"},"body":{"resCode":"000","msgRes":"HS19685229021121204383180 Message submitted successfully","resMsg":"OTP sent to mobile number 74-xxx-xxx-73","resHash":"9llPxA+GQBZegG83gKcONYEOPpksVS7rqgVF+8SXY5sqeTb9u5xGPcKVYkrv0YIIP2iUkMSHjRlzRF4q\/2rAYn9xIVMpmFSJX+uaF\/VsqaUR3enFpiUM04oLp1DQ2TGQu3y+nA2QFZUXJ70B4nhNuzEETIymCIKreui3pY6ADsMNcutuWpbSG8z4pDf0XNzoGW9EvJ25DlhvQDvgCRpyDA==","otpHash":"3ece3cae0b0305316ae78f98c371f42b9a0b3f2201ad0c1605fa46fcb70ee4fb","verifyCall":"N","respCode":"38b09232-00b4-40d9-8334-462687ab0d7b"},"cred":{"aesKey":"91586e703c69f7784368216bb952668bcf1e3d745152ca55","deviceId":"3A324B67-0805-4A5E-8803-8BCF66C30D78","hashToken":"gqTeUEd6JmGf1J1hON96Kqxu6j9cWmVGxHDnDiCLeVA5foG8XA9dhHdmAvVmSdlZh4nRzSqPxQ3sslRCECAHg+X7DJSMd5fSUjqnVksmybETvT5yALBlfd7hSeh0K3ljpzqYnGKN6rbDEw8Toy2lHzY1ORk80wLy4SZWnrekG1qHa6xETarBnL3VxYg6m1HHhr424e30AOW65KcED\/sAaFEv5aQTGmhVumplyuqbq5SMfzcQZ0sDEPzoRA08WGndMLnANZOSAb93O7JJbRPrtCSPcejTdSTGYXAkCmmyyrau4CA=","hmac":"2e6cc2ab2d28a3c6190c16968bc41b74a995273fb19007a3e5627b1d106543c4"}}
*/
