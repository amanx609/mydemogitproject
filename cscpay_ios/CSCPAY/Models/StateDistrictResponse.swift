//
//  StateDistrictResponse.swift
//  CSCPAY
//
//  Created by Aman Pandey on 06/12/21.
//

import Foundation

struct StatesResponse: Decodable{
    var states : [StateDistrictResponse]?
    
    enum CodingKeys : String , CodingKey{
        case states
    }
}


struct StateDistrictResponse : Decodable{
    var stateName : String?
    var stateCode : String?
    var districts : [DistrictResponse]?
    
    enum CodingKeys : String , CodingKey{
        case stateName
        case stateCode
        case districts
    }
}

struct DistrictResponse:Decodable{
    var districtCode : String
    var districtName : String
    enum CodingKeys : String , CodingKey{
        case districtCode
        case districtName
    }
}


typealias StateList = [StateDistrictResponse]
