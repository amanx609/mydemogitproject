//
//  CheckDeviceResponse.swift
//  CSCPAY
//
//  Created by Gowtham on 01/07/21.
//

import Foundation

class CheckDevice : Decodable {
    private enum CodingKeys : String, CodingKey {
        case check_root_detection = "check_root_detection"
        case devMsgTxt = "devMsgTxt"
        case deviceStatus = "deviceStatus"
        case isMerchant = "isMerchant"
        case listKeysFlag = "listKeysFlag"
        case reVerify = "reVerify"
        case regLevel = "regLevel"
        case simStatus = "simStatus"
        case smsGateWayNo = "smsGateWayNo"
        case smsStatus = "smsStatus"
        case status = "status"
        case statusDesc = "statusDesc"
        case userMsg = "userMsg"
    }
    var check_root_detection : Bool?
    var devMsgTxt : String?
    var deviceStatus : String?
    var isMerchant : Bool?
    var listKeysFlag : String?
    var reVerify : String?
    var regLevel : String?
    var simStatus : String?
    var smsGateWayNo : String?
    var smsStatus : String?
    var status : String?
    var statusDesc : String?
    var userMsg : String?
}
