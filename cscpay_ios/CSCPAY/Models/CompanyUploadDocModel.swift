//
//  CompanyUploadDocModel.swift
//  CSCPAY
//
//  Created by Aman Pandey on 16/12/21.
//

import Foundation

import Foundation

struct CompanyDocModel: Codable {
    var companies: [Company]?
}

// MARK: - Company
struct Company: Codable {
    var type: String?
    var docs: [Doc]?
}

// MARK: - Doc
struct Doc: Codable {
    var docType: DocType?
    var doc: [String]?
    var upload: Bool?
}

enum DocType: String, Codable {
    case bankAccount = "Bank Details"
    case companyProof = "Company Proof"
    case kyc = "KYC"
    case pan = "PAN"
}
