//
//  App_UserBankAccount_Details+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Aman Pandey on 26/07/21.
//
//

import Foundation
import CoreData


extension App_UserBankAccount_Details {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<App_UserBankAccount_Details> {
        return NSFetchRequest<App_UserBankAccount_Details>(entityName: "App_UserBankAccount_Details")
    }

    @NSManaged public var isMerchant: Int64
    @NSManaged public var pspRefNo: String?
    @NSManaged public var pspRespRefNo: String?
    @NSManaged public var resHash: String?
    @NSManaged public var status: String?

}

extension App_UserBankAccount_Details : Identifiable {

}
