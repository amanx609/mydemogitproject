//
//  Transaction_History+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Aman Pandey on 08/08/21.
//
//

import Foundation
import CoreData


extension Transaction_History {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Transaction_History> {
        return NSFetchRequest<Transaction_History>(entityName: "Transaction_History")
    }

    @NSManaged public var custRefNo: String?
    @NSManaged public var drCrFlag: String?
    @NSManaged public var payeeAccountNo: String?
    @NSManaged public var payeeBankCode: String?
    @NSManaged public var payeeIfsc: String?
    @NSManaged public var payeeMaskAccountNo: String?
    @NSManaged public var payeeName: String?
    @NSManaged public var payeeVirtualAddress: String?
    @NSManaged public var payerAccountNo: String?
    @NSManaged public var payerBankCode: String?
    @NSManaged public var payerIfsc: String?
    @NSManaged public var payerName: String?
    @NSManaged public var payerVirtualAddress: String?
    @NSManaged public var paymentmethod: String?
    @NSManaged public var payType: String?
    @NSManaged public var reason_desc: String?
    @NSManaged public var refFlag: String?
    @NSManaged public var trnDate: String?
    @NSManaged public var trnRefNo: Int64
    @NSManaged public var txnAmount: String?
    @NSManaged public var txnNote: String?
    @NSManaged public var txnStatus: String?
    @NSManaged public var txnStatusDesc: String?
    @NSManaged public var txnType: String?
    @NSManaged public var transactionResponse: Transaction_Response?

}

extension Transaction_History : Identifiable {

}
