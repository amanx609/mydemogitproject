//
//  App_User_Transactions+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Aman Pandey on 04/08/21.
//
//

import Foundation
import CoreData


extension App_User_Transactions {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<App_User_Transactions> {
        return NSFetchRequest<App_User_Transactions>(entityName: "App_User_Transactions")
    }

    @NSManaged public var amount: String?
    @NSManaged public var approvalNumber: String?
    @NSManaged public var custRefNo: String?
    @NSManaged public var npciTransId: String?
    @NSManaged public var payeeVPA: String?
    @NSManaged public var payerVPA: String?
    @NSManaged public var pspRefNo: String?
    @NSManaged public var refId: String?
    @NSManaged public var responseCode: String?
    @NSManaged public var status: String?
    @NSManaged public var statusDesc: String?
    @NSManaged public var txnAuthDate: String?
    @NSManaged public var upiTransRefNo: Int64

}

extension App_User_Transactions : Identifiable {

}
