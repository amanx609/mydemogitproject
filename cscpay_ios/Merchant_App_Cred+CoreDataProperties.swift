//
//  Merchant_App_Cred+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Aman Pandey on 20/12/21.
//
//

import Foundation
import CoreData


extension Merchant_App_Cred {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Merchant_App_Cred> {
        return NSFetchRequest<Merchant_App_Cred>(entityName: "Merchant_App_Cred")
    }

    @NSManaged public var androidGrace: String?
    @NSManaged public var androidVer: String?
    @NSManaged public var apiEndpoint: String?
    @NSManaged public var appLabel: String?
    @NSManaged public var appSalt: String?
    @NSManaged public var faqData: String?
    @NSManaged public var iosGrace: String?
    @NSManaged public var iosVer: String?
    @NSManaged public var lastUpdated: String?
    @NSManaged public var merchantBank: String?
    @NSManaged public var merchantVersion: String?
    @NSManaged public var otpReAuth: String?
    @NSManaged public var otpValidity: String?
    @NSManaged public var refId: String?
    @NSManaged public var appStage: String?
    @NSManaged public var respUID: String?
    @NSManaged public var ts: String?
    @NSManaged public var deviceId: String?
    @NSManaged public var appName: String?

}

extension Merchant_App_Cred : Identifiable {

}
