//
//  List_Key+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Aman Pandey on 09/09/21.
//
//

import Foundation
import CoreData


extension List_Key {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<List_Key> {
        return NSFetchRequest<List_Key>(entityName: "List_Key")
    }

    @NSManaged public var owner: String?
    @NSManaged public var code: String?
    @NSManaged public var keyValue: String?
    @NSManaged public var ki: Int64
    @NSManaged public var type: String?
    @NSManaged public var relationshipNpciData: NPCI_Data?

}

extension List_Key : Identifiable {

}
