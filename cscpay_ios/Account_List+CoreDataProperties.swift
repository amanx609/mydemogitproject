//
//  Account_List+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Aman Pandey on 29/07/21.
//
//

import Foundation
import CoreData


extension Account_List {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Account_List> {
        return NSFetchRequest<Account_List>(entityName: "Account_List")
    }

    @NSManaged public var accId: Int64
    @NSManaged public var accountType: String?
    @NSManaged public var bankCode: String?
    @NSManaged public var bankId: Int64
    @NSManaged public var ifscCode: String?
    @NSManaged public var maskedAccountNumber: String?
    @NSManaged public var mpinFlag: String?
    @NSManaged public var isPrimary: String?
    @NSManaged public var bankImage: Data?

}

extension Account_List : Identifiable {

}
