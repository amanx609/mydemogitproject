//
//  App_NPCI_Token+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Aman Pandey on 06/09/21.
//
//

import Foundation
import CoreData


extension App_NPCI_Token {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<App_NPCI_Token> {
        return NSFetchRequest<App_NPCI_Token>(entityName: "App_NPCI_Token")
    }

    @NSManaged public var code: String?
    @NSManaged public var keyValue: String?
    @NSManaged public var ki: String?
    @NSManaged public var owner: String?
    @NSManaged public var type: String?

}

extension App_NPCI_Token : Identifiable {

}
