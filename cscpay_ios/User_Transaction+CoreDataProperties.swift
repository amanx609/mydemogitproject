//
//  User_Transaction+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Aman Pandey on 02/08/21.
//
//

import Foundation
import CoreData


extension User_Transaction {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User_Transaction> {
        return NSFetchRequest<User_Transaction>(entityName: "User_Transaction")
    }

    @NSManaged public var custRefNo: String?
    @NSManaged public var drCrFlag: String?
    @NSManaged public var payType: String?
    @NSManaged public var payeeAccountNo: String?
    @NSManaged public var payeeBankCode: String?
    @NSManaged public var payeeIfsc: String?
    @NSManaged public var payeeMaskAccountNo: String?
    @NSManaged public var payeeName: String?
    @NSManaged public var payeeVirtualAddress: String?
    @NSManaged public var payerAccountNo: String?
    @NSManaged public var payerBankCode: String?
    @NSManaged public var payerIfsc: String?
    @NSManaged public var payerName: String?
    @NSManaged public var payerVirtualAddress: String?
    @NSManaged public var paymentmethod: String?
    @NSManaged public var reason_desc: String?
    @NSManaged public var refFlag: String?
    @NSManaged public var trnDate: String?
    @NSManaged public var trnRefNo: Int64
    @NSManaged public var txnAmount: String?
    @NSManaged public var txnNote: String?
    @NSManaged public var txnStatus: String?
    @NSManaged public var txnStatusDesc: String?

}

extension User_Transaction : Identifiable {

}
