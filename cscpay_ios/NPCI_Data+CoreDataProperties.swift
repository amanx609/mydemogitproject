//
//  NPCI_Data+CoreDataProperties.swift
//  CSCPAY
//
//  Created by Aman Pandey on 06/09/21.
//
//

import Foundation
import CoreData


extension NPCI_Data {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NPCI_Data> {
        return NSFetchRequest<NPCI_Data>(entityName: "NPCI_Data")
    }

    @NSManaged public var refId: String?
    @NSManaged public var refUrl: String?
    @NSManaged public var reqMsgId: String?
    @NSManaged public var status: String?
    @NSManaged public var txnId: String?
    @NSManaged public var xmlResp: String?
    @NSManaged public var relationListKey: NSSet?

}

// MARK: Generated accessors for relationListKey
extension NPCI_Data {

    @objc(addRelationListKeyObject:)
    @NSManaged public func addToRelationListKey(_ value: List_Key)

    @objc(removeRelationListKeyObject:)
    @NSManaged public func removeFromRelationListKey(_ value: List_Key)

    @objc(addRelationListKey:)
    @NSManaged public func addToRelationListKey(_ values: NSSet)

    @objc(removeRelationListKey:)
    @NSManaged public func removeFromRelationListKey(_ values: NSSet)

}

extension NPCI_Data : Identifiable {

}
